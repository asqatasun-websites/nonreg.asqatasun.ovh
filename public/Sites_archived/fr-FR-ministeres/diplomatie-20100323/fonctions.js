// FICHER FONCTIONS.JS


/***************************************/
// script centrage_site()
/*
	- se declenche des que la taille de la fenetre est redimensionnee
	- si la nouvelle largeur < 900 px
	- alors modifie les proprietes de l'element #site
	- pour les mettre a zero
*/

window.onload = centrage_site;
window.onresize = centrage_site;

function centrage_site() {
	var largeur = lit_largeur() ;	// largeur de l'ecran
	var limite = donne_limite() ;	// largeur minimale
	
	if (largeur < limite) {		supprime_proprietes() ; }
	else {	retablit_proprietes() ; }
}

// lire_largeur() : recupere la largeur de l'ecran
function lit_largeur() {
	var largeur ;
	if (document.body.offsetWidth) {largeur = document.body.offsetWidth ; }
	else {largeur = window.innerWidth ; }
	return largeur ;}
	
// donne_limite() : definit la largeur minimale pour recentrer le site 
function donne_limite() {
	var limite ;
	if (document.body.offsetWidth) {limite = 1000 ;}
	else  {limite = 1020 ;}
	return limite ;}
	
// supprime_proprietes() : supprime les proprietes de centrage css
function supprime_proprietes() {
	document.getElementById("site").style.left = "0" ;
	document.getElementById("site").style.marginLeft = "0" ;}
	
// retablit_proprietes() : retablit les proprietes de centrage css
function retablit_proprietes() {
	document.getElementById("site").style.left = "50%" ;
	document.getElementById("site").style.marginLeft = "-500px" ;}

/***************************************/
//  script redirection_choix(id rang)
/*
	- recupere la valeur choisie par le <select>
	- dans l'un des 3 menus deroulants de navigation
	 (la valeur choisie est une url relative)
	- redirige la page vers l'url correspondante
*/
function redirection_choix(rang) {
 for(i=0;i<document.forms[rang].url_rubrique.length;++i)
  if(document.forms[rang].url_rubrique.options[i].selected == true)
   window.location.href = document.forms[rang].url_rubrique.options[i].value;}
/***************************************/


/***************************************/
// script equilibre_colonne_gauche
/* 
	Permet de redimensionner la hauteur du cache et de la colonne gauche 
	en fonction de la hauteur du site
*/
function equilibre_colonnes() {
	// on identifie les 3 divs qu'il faut �quilibrer
	var Contenu = document.getElementById("contenu");
	var ColLeft = document.getElementById("colonne_gauche")
	var ColRigh = document.getElementById("colonne_droite")	
	// on trouve leurs hauteurs
	var hauteurContenu = ((Contenu)? Contenu.offsetHeight:0);
	var hauteurColLeft = ((ColLeft)? ColLeft.offsetHeight:0);
	var hauteurColRigh = ((ColRigh)? ColRigh.offsetHeight:0);
	// on trouve la plus haute des trois
	var hauteur = Math.max(hauteurContenu, hauteurColLeft);
	if (ColRigh) {var hauteur = Math.max(hauteur, hauteurColRigh); }
	// on ajoute une petit marge...
	hauteur = hauteur + 20;
	// on alerte le d�buggueur...
	// alert('Colonne gauche : ' + hauteurColLeft + '\nContenu : ' + hauteurContenu + '\nColonne droite : ' + hauteurColRigh + '\nOn met ces 3 divs � ' + hauteur + ' pixels.');
	// on redimentionne l'ensemble
	if (Contenu) {Contenu.style.height = hauteur + 24 + "px"; }
	if (ColLeft) {ColLeft.style.height = hauteur +"px"; }
	if (ColRigh) {ColRigh.style.height = hauteur +"px"; }

}
/***************************************/
