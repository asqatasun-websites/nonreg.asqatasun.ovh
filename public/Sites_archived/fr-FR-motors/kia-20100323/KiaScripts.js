﻿
$(document).ready(function() {


  $("a[href='#']").click(function () { return false;});

//    $(".dfDateDropDown").each(function (){
//        $(this).find(".dfSelect").eq(1).find("select").addClass("dfDateDropDownMonth");
//    });

    /* CUFON replacement */
    /* topnav */
    $(".btnCorner").prepend("<div class='btnCornerPx'>&nbsp;</div>")
                   .prepend("<div class='btnCornerPx'>&nbsp;</div>")
                   .prepend("<div class='btnCornerPx'>&nbsp;</div>")
                   .prepend("<div class='btnCornerPx'>&nbsp;</div>");
    $(".btnCorner .btnCornerPx:eq(0)").css({'left':'0px','top':'0px'});
    $(".btnCorner .btnCornerPx:eq(1)").css({'left':'0px','bottom':'0px'});
    $(".btnCorner .btnCornerPx:eq(2)").css({'right':'0px','top':'0px'});
    $(".btnCorner .btnCornerPx:eq(3)").css({'right':'0px','bottom':'0px'});
  
  $('.topNavigation a.cufon,.topNavigation .topNavigationSubMenu a').css("text-transform","uppercase");
  $(".topNavigation a[href='#']").addClass("noLink");
  
  Cufon.replace('.topNavigation a.cufon');
  Cufon.replace('.topNavigation .topNavigationSubMenu a');
  
  Cufon.replace('.DE_formTitle');
  Cufon.replace('.DE_calltoaction');
  Cufon.replace('#pnlOccasionDealer h5');
  Cufon.replace('#globalnav h2');
  
  if(typeof($("div.scrollable").scrollable)==  "function")
    occasionScroller = $("div.scrollable").scrollable({ vertical:true, size: 3 , api : true  });
  
  $("#occasionImages .prevImage").click(function(){
    thisURL = $("#occasionImage").attr("src");
    newURL = $("#occasionThumbs a[href='"+thisURL+"']").prev("a").attr("href");
    if(newURL!=undefined)
    {
        showPicture(newURL,null,null);
        occasionScroller.prev();
        $("#occasionThumbs a[href='"+thisURL+"']").prev("a").trigger("click");
        $("#occasionThumbs a[href='"+thisURL+"']").removeClass("active");
        $("#occasionThumbs a[href='"+thisURL+"']").prev("a").addClass("active");
    }
  });
  
  $("#occasionImages .nextImage").click(function(){
    thisURL = $("#occasionImage").attr("src");
    newURL = $("#occasionThumbs a[href='"+thisURL+"']").next("a").attr("href");
    if(newURL!=undefined)
    {
        showPicture(newURL,null,null);
        occasionScroller.next();
        $("#occasionThumbs a[href='"+thisURL+"']").next("a").trigger("click");
        $("#occasionThumbs a[href='"+thisURL+"']").removeClass("active");
        $("#occasionThumbs a[href='"+thisURL+"']").next("a").addClass("active");
    }
  });
  
  
  //Cufon.replace('.popupPanelFormTop');
  
  if($.browser.msie)
    $(".topNavigation li").addClass("isIE")
  
  $(".topNavigation a.cufon, .topNavigation .topNavigationSubMenu a").hover(
        function ()
        {   $(this).addClass("hover");        
            Cufon.replace($(this));
        },
        function ()
        {   $(this).removeClass("hover");
            Cufon.replace($(this));
        });
        
  /* globalnav */
  $('#globalnav a.cufon').css("text-transform","uppercase");
  Cufon.replace('#globalnav a.cufon');
  
  $("#globalnav a.cufon.topNavA").hover(
        function ()
        {   $(this).addClass("hover");
            Cufon.replace($(this));
        },
        function ()
        {   $(this).removeClass("hover");
            Cufon.replace($(this));
        });
        
  $("#globalnav a.cufon.topNavArrow").hover(
        function ()
        {   $(this).addClass("hover");
            Cufon.replace($(this));
        },
        function ()
        {   $(this).removeClass("hover");
            Cufon.replace($(this));
        });
  /* tabs */      
  
  $('#tabs a.cufon').css("text-transform","uppercase");
  Cufon.replace('#tabs a.cufon');
  $("#tabs a.cufon").hover(
        function ()
        {   $(this).addClass("hover");
            Cufon.replace($(this));
        },
        function ()
        {   $(this).removeClass("hover");
            Cufon.replace($(this));
        });

/* // --- */
   Cufon.replace("#header h1");
   
   Cufon.replace(".dfSubmit");
   
   Cufon.replace(".toolboxTop");
   Cufon.replace("#copyright a", { hover: true });
   Cufon.replace("#footerNavigation a", { hover: true });

   Cufon.replace("#contentMenu a", {hover:true});
   
   Cufon.replace("#footerTeaserBoxContainer #teaserBox h3");
   Cufon.replace("#footerTeaserBoxContainer #teaserBox label");
   
   Cufon.replace("#footerTeaserBoxContainer #teaserBox .btn_widgetsubmit");
   Cufon.replace("#footerTeaserBoxContainer #teaserBox .btnVersionCompare");
   Cufon.replace("#footerTeaserBoxContainer #teaserBox .fromPricePanel div");
   
   Cufon.replace("#footerTeaserBoxContainer #teaserBox #widgetOffer a span");
   
   Cufon.replace("#contentPrintStep1 .close");
   Cufon.replace("#contentRegister1 .close");
   
   Cufon.replace(".popupPanelForm .closeButton");
   
   
   Cufon.replace(".price");
   Cufon.replace("#occasionResults .readmore", {hover:true});
   
   Cufon.replace(".previous a", {hover:true});
   Cufon.replace(".next a", {hover:true});
   
   $("#footerButtonbar ul li:first").addClass("firstItem");
   Cufon.replace("#footerButtonbar ul li .footerbuttonLabel");
   
   Cufon.replace("a.dfSubmit", {hover:true});

  $("a.dfSubmit").hover(
        function ()
        {   $(this).addClass("dfSubmitHover");
            Cufon.replace($(this));
        },
        function ()
        {   $(this).removeClass("dfSubmitHover");
            Cufon.replace($(this));
        });

   
    carconfiguratorReady = function()
    {
        
       $(".topNavigation > li").hover(   
             
            function() {               
                if($(this).hasClass("busy"))  return;              
                $("#header").css("z-index","99997");
                $(this).stopTime("hideNav");
                if(!$.browser.msie)
                    $(this).children(".topNavigationSubMenu").find(".topNavigationSubMenuInner").fadeIn();
                $(this).children(".topNavigationSubMenu").slideDown();
            },
            function() { 
               $("#header").css("z-index","0");   
               $(this).oneTime(500, "hideNav", function() {                                 
                    $(this).addClass("busy");
                    $(this).children(".topNavigationSubMenu").slideUp("normal",function () { $(".busy").removeClass("busy"); });
                    if(!$.browser.msie)
                        $(this).children(".topNavigationSubMenu").find(".topNavigationSubMenuInner").fadeOut();
               });
        });
        
    }
    

   if($("#experienceImage, #header").height()!=584 && $("#experienceImage").hasClass("swfInternal584")==false)
   {  
        carconfiguratorReady();   
   }    
    
    $(".modelNavigation .topNavigationSubMenuInner li").hover(
        function() {    
            $(".modelSnippet").hide();
            posParent = $(this).parents("ul").find("li:first").offset();
            posEl = $(this).offset();
            calculateTop = posParent.top-posEl.top-5;
            
            $(this).find(".modelSnippet").css("top",calculateTop+"px");
            
            $(this).stopTime("hideModelSnippet");
            $(this).find(".modelSnippet").fadeIn();
            
        }, 
        function() {
           $(this).oneTime(1000, "hideModelSnippet", function() {
                $(this).find(".modelSnippet").fadeOut();
           });
        });
    $(".modelSnippet").hide();
    Cufon.replace(".modelSnippet h3");
    
    if($("#tabs").length<1 && $(".contentContainerHome").length<1 )
    {   
        $(".topNavigation").after('<div id="tabs"><div id="tabsInner"><div class="tabCornerLeft">&nbsp;</div>&nbsp;<div class="tabCornerRight">&nbsp;</div></div></div>');
    }
    
    $('#popupwrapper').click(function() { popUp(); })

    $("#footerContainer #footerNavigation #languageSelect ul").slideUp();
    $("#footerNavigation #languageSelect").hover(
        function() {
            if($(this).hasClass("busy"))  return;
            $(this).addClass("hover");
            $("#footerContainer #footerNavigation #languageSelect ul").slideDown();
        },
        function() {
            $(this).addClass("busy");
            $(this).removeClass("hover");
            $("#footerContainer #footerNavigation #languageSelect ul").slideUp("normal",function () { $(".busy").removeClass("busy"); });
        });
        $(".currentLanguage").mouseenter(function() { $(".busy").removeClass("busy"); });

    $(".newsPic:empty").hide();
    
/* switch model*/
    //Cufon.replace("#switchDealerOverlay .modelSnippetName", {hover:true});
    if(typeof($("div.scrollable").scrollable)=="function")
        $("div.scrollable").scrollable(); 
             //closeOnClick: false, clickable: false
  //  if(typeof($("div.scrollable").overlay)=="function")    
   //     $("div.scrollable").overlay({ closeOnClick: false});
       
    switchDealerOverlayHeight = 127;
    var bookMarkExpand = false;
    var switchModelExpand = false;
    
    $(".footerbutton1").bind("click",
        function(){
        
            if($("#switchDealerOverlay").height() == 0)
            {
                resetFooterButtons();
                bookMarkExpand = false;
                switchModelExpand = true;
                $(".footerbutton2").removeClass("hoverFooterbutton");
                Cufon.replace("#footerButtonbar ul li .footerbuttonLabel");    
                
                $("#switchDealerOverlay").animate({"height":switchDealerOverlayHeight},function () {
                   // $(".footerbutton1").addClass("hoverFooterbutton1");
                   // Cufon.replace("#footerButtonbar ul li .footerbuttonLabel");                
                })
            }
            if($("#switchDealerOverlay").height() == switchDealerOverlayHeight)
            {
                switchModelExpand = false;
                $("#switchDealerOverlay").animate({"height":0}, function (){})
            }            
        });
    $(".footerbutton1").bind("mouseenter",
        function(){
            $(this).addClass("hoverFooterbutton1");
            Cufon.replace("#footerButtonbar ul li .footerbuttonLabel");
        });

    $(".footerbutton1").bind("mouseleave",
        function(){
           if($("#switchDealerOverlay").height() != switchDealerOverlayHeight)
           {
                if(!switchModelExpand){   
                    $(".footerbutton1").removeClass("hoverFooterbutton1");
                    Cufon.replace("#footerButtonbar ul li .footerbuttonLabel");
                }
           }
        }
    );
    $(".footerbutton2").bind("click",
        function(){
        
            $(".footerbutton1").removeClass("hoverFooterbutton1");
            Cufon.replace("#footerButtonbar ul li .footerbuttonLabel");
        
            if(!bookMarkExpand){
                switchModelExpand = false;
                resetFooterButtons();
                $("#bookmarkOverlay").fadeIn(); 
                bookMarkExpand= true;
            }else{
                $("#bookmarkOverlay").fadeOut();
                bookMarkExpand = false;
            }            
        }
    );
    
    $(".footerbutton2").bind("mouseenter",
        function(){        
            $(this).addClass("hoverFooterbutton");
            Cufon.replace("#footerButtonbar ul li .footerbuttonLabel");
          
        });

    $(".footerbutton2").bind("mouseleave",
        function(){   
           if(!bookMarkExpand){     
                $(this).removeClass("hoverFooterbutton");
                Cufon.replace("#footerButtonbar ul li .footerbuttonLabel");
           }
        }
    );
    
    
    $(".footerbutton3").hover(
        function(){
            $(this).addClass("hoverFooterbutton");
            Cufon.replace("#footerButtonbar ul li .footerbuttonLabel");
        }, 
        function(){
            $(this).removeClass("hoverFooterbutton");
            Cufon.replace("#footerButtonbar ul li .footerbuttonLabel");
        }
    );
    



/* modelchapterNavigation*/
    handleText = $(".handle .handleLabel").text();
    newHandleText = "";
    for (i=0;i<handleText.length;i++)
    {
        newHandleText = newHandleText+handleText.substr(i,1)+"<BR />";
    }
    //$(".handle .handleLabel").html(newHandleText);
    $('#modelChaptersNavigation').tabSlideOut({ tabHandle: '.handle' });    
    $('#modelChaptersNavigation').css( { "visibility":"visible"});
    //Cufon.replace(".handle .handleLabel");
    Cufon.replace("#modelChaptersNavigation h3");
    Cufon.replace("#modelChaptersNavigation ul li a");
    
    
    
    $('#modelChaptersNavigation .handle').hover( 
        function(){
            $(this).addClass("handleHover");
            handleDoRollOver(true);
            // Cufon.replace($(this).find(".handleLabel"));    
        },
        function(){
            $(this).removeClass("handleHover");
            handleDoRollOver(false);
            // Cufon.replace($(this).find(".handleLabel"));    
        });

        $('#modelChaptersNavigation ul li a.default').hover( 
            function(){
                $(this).addClass("hover");
                Cufon.replace($(this));    
            },
            function(){
                $(this).removeClass("hover");
                Cufon.replace($(this));    
        });
        
        
        
        if($('#modelChaptersNavigation .handle').outerHeight()>$('#modelChaptersNavigation').outerHeight())
        {   
            if ($.browser.msie && $.browser.version<7) { 
                $('#modelChaptersNavigation>ul').css({ "height": $('#modelChaptersNavigation .handle').outerHeight()});
            }
            else
            {
                $('#modelChaptersNavigation>ul').css({ "min-height": $('#modelChaptersNavigation .handle').outerHeight()});
            }
        }
        
        if ($.browser.msie && $.browser.version<7) {  //effect in IE6 is not that nice so fallback-workaround
            $('#modelChaptersNavigation>ul>li>a').click( function() {                 
                if($(this).parent("li").find("ul:visible").length>0)
                {   $(this).parent("li").find("ul").hide();
                }
                else
                {   $('#modelChaptersNavigation ul li ul').hide();
                    $(this).parent("li").find("ul").toggle();
                }
            });
        }
        else
        {   
            $('#modelChaptersNavigation>ul>li>a').click( function() {      
                $('#modelChaptersNavigation ul li').removeClass("open");
                $('#modelChaptersNavigation ul li ul').slideUp();
                if($(this).parent("li").find("ul:visible").length<1)
                {   
                    $(this).parent("li").toggleClass("open");
                    $(this).parent("li").find("ul").slideToggle()
                }
                Cufon.replace($('#modelChaptersNavigation ul li a'));
            });
        }
        
        Cufon.replace(".mcPanel .mcPanelContent h2", { fontFamily: 'NeoSansPro-Regular' }); 
        
        Cufon.replace(".offerImageCorner .offerCornerText1", { fontFamily: 'NeoSansPro-Regular' });
        Cufon.replace(".offerImageCorner .offerCornerText2", { fontFamily: 'NeoSansPro-Regular' });
        Cufon.replace(".offerPrice h2");
        Cufon.replace(".offerPrice h3");
        Cufon.replace(".btn_offer",{ hover: {color: '0xb3000f'}});
        
        Cufon.replace(".pnlVersion h2", { fontFamily: 'NeoSansPro-Regular' });
        Cufon.replace(".pnlVersion .versionPriceLabel"); 
        Cufon.replace(".pnlVersion .versionPrice"); 
        
        Cufon.replace(".pnlVersion .btnVersionCompare",{ hover: {color: '0xb3000f'}});
        Cufon.replace(".pnlVersion .btnVersionConfigure",{ hover: {color: '0xb3000f'}});
        Cufon.replace("#modelchaptersubmenu a",{ hover: {color: '0xffffff'}});
               
        
        Cufon.replace(".popupImageOverview ul li a", {hover:true});
        Cufon.replace(".popupSteps ul li", {hover:true});
        
        $(".popupimageoverview ul li .modelchoise").bind("click",function () {
                        
            $("#chosenmodelid").val($(this).attr("href").replace("#",""));
            $("#popupbrochurestep1").hide();
            $("#popupbrochurestep2").show();
            hs.getexpander("popupformbrochure").reflow();
            $(".popupsteps ul li").eq(0).removeclass("active");
            $(".popupsteps ul li").eq(1).addclass("active");
            cufon.replace(".popupsteps ul li");
            
        });

});

function handleDoRollOver(doRollOver){

 

//    if(doRollOver) $('.handle #handleImg').src = "";
 //   else $('.handle #handleImg').src = "";
}

function openMenuLayer(){
    $('.handle').click();
}

function resetFooterButtons()
{

    /* button 1 */
    $("#switchDealerOverlay").animate({"height":0}, function (){})
    
    /* button 2 */ 
    $("#bookmarkOverlay").fadeOut();
}

function reloadJQuery()
{

    /* modelchapterNavigation*/
    handleText = $(".handle .handleLabel").text();
    newHandleText = "";
    for (i=0;i<handleText.length;i++)
    {
        newHandleText = newHandleText+handleText.substr(i,1)+"<BR />";
    }
    //$(".handle .handleLabel").html(newHandleText);
    $('#modelChaptersNavigation').tabSlideOut({ tabHandle: '.handle' });    
    $('#modelChaptersNavigation').css( { "visibility":"visible"});
    //Cufon.replace(".handle .handleLabel");
    Cufon.replace("#modelChaptersNavigation h3");
    Cufon.replace("#modelChaptersNavigation ul li a");
    
    
    
    $('#modelChaptersNavigation .handle').hover( 
        function(){
            $(this).addClass("handleHover");
             handleDoRollOver(true);
            //Cufon.replace($(this).find(".handleLabel"));    
        },
        function(){
            $(this).removeClass("handleHover");
             handleDoRollOver(false);
            //Cufon.replace($(this).find(".handleLabel"));    
    });

    $('#modelChaptersNavigation ul li a').hover( 
        function(){
            $(this).addClass("hover");
            Cufon.replace($(this));    
        },
        function(){
            $(this).removeClass("hover");
            Cufon.replace($(this));    
    });
        
        
        
    if($('#modelChaptersNavigation .handle').outerHeight()>$('#modelChaptersNavigation').outerHeight())
    {   
        if ($.browser.msie && $.browser.version<7) { 
            $('#modelChaptersNavigation>ul').css({ "height": $('#modelChaptersNavigation .handle').outerHeight()});
        }
        else
        {
            $('#modelChaptersNavigation>ul').css({ "min-height": $('#modelChaptersNavigation .handle').outerHeight()});
        }
    }
    
    if ($.browser.msie && $.browser.version<7) {  //effect in IE6 is not that nice so fallback-workaround
        $('#modelChaptersNavigation>ul>li>a').click( function() {   
            if($(this).parent("li").find("ul:visible").length>0)
            {   $(this).parent("li").find("ul").hide();
            }
            else
            {   $('#modelChaptersNavigation ul li ul').hide();
                $(this).parent("li").find("ul").toggle();
            }
        });
    }
    else
    {   
        $('#modelChaptersNavigation>ul>li>a').click( function() {
            $('#modelChaptersNavigation ul li').removeClass("open");
            $('#modelChaptersNavigation ul li ul').slideUp();
            if($(this).parent("li").find("ul:visible").length<1)
            {   
                $(this).parent("li").toggleClass("open");
                $(this).parent("li").find("ul").slideToggle()
            }
            Cufon.replace($('#modelChaptersNavigation ul li a'));
        });
    }
    
}

function refreshCufonPopupSteps()
{
    Cufon.replace(".popupSteps ul li", {hover:true});
}


function fontAdjust(doThis)
{           
	if(document.getElementById('col2'))  contentPart = document.getElementById('col2');
        if(document.getElementById('col12')) contentPart = document.getElementById('col12');        
        if(contentPart)
        {           
	        actualSize = parseFloat(contentPart.style.fontSize);
        	if(!actualSize)     { actualSize  = 1.2; }
                        if(doThis=='plus' && actualSize<1.7)        { newSize = actualSize + 0.2;}
                        if(doThis=='min' && actualSize>0.9)         { newSize = actualSize - 0.2;}
                contentPart.style.fontSize = newSize + 'em' ; 
         }
}

function hideSelects(action) {  
	//documentation for this script at http://www.shawnolson.net/a/1198/
	//possible values for action are 'hidden' and 'visible'
	if (action!='visible'){action='hidden';}
	if ($.browser.msie && $.browser.version<7) { 
	for (var S = 0; S < document.forms.length; S++){
	for (var R = 0; R < document.forms[S].length; R++) {
	if (document.forms[S].elements[R].options) {
	document.forms[S].elements[R].style.visibility = action;
	}
	}
	}
	}
}
function activateTab(tabId)
{	
	for(i=1;i<=4;i++)
	{  
	    if(document.getElementById('tab'+i)!=null)
	    {
	        j = document.getElementById(('tab'+i));
		    j.className=j.className.replace("active", "");	
		}
    }
	document.getElementById(('tab'+tabId)).className+=" active";

	for(i=1;i<=4;i++)
	{ 	
	    if(document.getElementById('tabContent'+i)!=null)
	    {
	        j = document.getElementById(('tabContent'+i)); 
		    j.className=j.className.replace("active", "");	
		}
    }
	document.getElementById(('tabContent'+tabId)).className+="active";
}

function findPos(obj)
{
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		curleft = obj.offsetLeft
		curtop = obj.offsetTop
		while (obj = obj.offsetParent) {
			curleft += obj.offsetLeft
			curtop += obj.offsetTop
		}
	}
	return curtop;
}

function swfPopUp(state) {
	if(state=='open')   display = 'block' ; 
	else	            display = 'none';
	
	if(document.getElementById('popupwrapperNoFlash')!=null)   document.getElementById('popupwrapperNoFlash').style.display = display ;
	if(document.getElementById('popupNoFlash')!=null)          document.getElementById('popupNoFlash').style.display = display ;
}

function popUp(url,posY) {
	if(url) { 
		document.getElementById('carPicture').src = url;
		display = 'block' ;
		document.getElementById('popup').style.top = posY-100+'px';
		document.getElementById('popup').style.left = (($(document).width()-924)/2)+200 +'px';
		
        $('#popupwrapper').css("height",$("#container").height()+'px');
	}
	else	display = 'none';
	
	if(document.getElementById('popupwrapper')!=null)   document.getElementById('popupwrapper').style.display = display ;
	if(document.getElementById('popup')!=null)          document.getElementById('popup').style.display = display ;
    
}



function showPicture(url,imgNm,thumb) {
	
	if(imgNm != 'plain') {
	    
	    $("#occasionImage").attr("src",url);
	    
	return;
		var showMedium = "occasionImage";

		if(document.all) {
		    document.images[showMedium].filters.blendTrans.apply();
		}
		
		
		document.images[showMedium].src = url;
		if(document.all) {
		    document.images[showMedium].filters.blendTrans.play();
		}

		a = document.getElementById("occasionThumbs");
		for (i=0; i<a.childNodes.length; i++) {
			node = a.childNodes[i];
			if (node.nodeName=="A") {	node.style.borderColor = '#ffffff';	}
		}
	
		//thumb.style.borderColor = '#B81116';
	 	
	}
}


function BPEscape(url) {
	
	return escape(url);
}

function chooseModel(modelId, hfModelId, ddlModelId)
{
	document.getElementById('allmodels').style.display = 'none';
	document.getElementById('col1').style.display = 'block';
	document.getElementById('col2').style.visibility = 'visible';
	document.getElementById('content').style.backgroundImage = "url('../img/bg_hoofdnavigatie.gif')";
	hideSelects('visible');
	
	//model opslaan in HiddenField
	document.getElementById(hfModelId).value = modelId;
	//model selecteren in dropdown
	if (ddlModelId != '')
	{
		el = document.getElementById(ddlModelId);
		for (i=0; i< el.options.length; i++)
		{
			if (el.options[i].value == modelId)
			{
				el.options[i].selected = true;
			}
		}
	}
	// site layout opnieuw opbouwen

}

function changeModelTypeImage(imageUrl, imageId)
{
	document.getElementById(imageId).setAttribute("src", imageUrl);
}

function imageCheckDownload(ImageId, placeholder)
{
    loadHtmlSnippet('/layouts/press/cpm/DownloadHandler.aspx?imageId=' + ImageId +'&a=' + escape(new Date()), null, '');    
}

var dtCh= "-";
var minYear=1900;
var maxYear=2100;

function stripCharsInBag(s, bag)
{
    var i;
    var returnString = "";
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) 
{
    for (var i = 1; i <= n; i++) {
	    this[i] = 31
	    if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
	    if (i==2) {this[i] = 29}
   } 
   return this
}

function isValidDate(strval)
{
    var daysInMonth = DaysArray(12)
    var pos1=strval.indexOf(dtCh)
    var pos2=strval.indexOf(dtCh,pos1+1)
    var strMonth=strval.substring(0,pos1)
    var strDay=strval.substring(pos1+1,pos2)
    var strYear=strval.substring(pos2+1)
    strYr=strYear
    if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
    if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
    for (var i = 1; i <= 3; i++) {
	    if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
    }
    month=parseInt(strMonth)
    day=parseInt(strDay)
    year=parseInt(strYr)
    if (pos1==-1 || pos2==-1){
	    return false
    }
    if (strMonth.length<1 || month<1 || month>12){
	    return false
    }
    if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
	    return false
    }
    if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
	    return false
    }
    if (strval.indexOf(dtCh,pos2+1)!=-1 || IsNumeric(stripCharsInBag(strval, dtCh))==false){
	    return false
    }
    return true
}

function isValidTime(strval)
{    
    var pos1 = strval.indexOf(':');

    //Checking hours
    var horval =  trimString(strval.substring(0,pos1));

    if(horval > 23)
    {
        return false;
    }
    else if(horval < 0)
    {
        return false;
    }
    
    
    //checking minutes
    var minval =  trimString(strval.substring(pos1+1,pos1 + 3));

    if(minval > 59)
    {
        return false;
    }   
    else if(minval < 0)
    {
        return false;
    }
    
    return true;  
 }
 
function trimString(str) 
{ 
     var str1 = ''; 
     var i = 0; 
     while ( i != str.length) 
     { 
         if(str.charAt(i) != ' ') str1 = str1 + str.charAt(i); i++; 
     }
     var retval = IsNumeric(str1); 
     if(retval == false) 
         return -100; 
     else 
         return str1; 
}

function trimAllSpace(str) 
{ 
    var str1 = ''; 
    var i = 0; 
    while(i != str.length) 
    { 
        if(str.charAt(i) != ' ') 
            str1 = str1 + str.charAt(i); i ++; 
    } 
    return str1; 
}

function IsNumeric(strString) 
{ 
    var blnResult = false; 
    
    if (strString.length == 0) 
        return false; 
       
    //alert('regex: ' + strString.search("^\\d*$"));
    if(strString.search("^\\d*$") != -1)
    {
        blnResult = true;
    }
    return blnResult; 
}


function setPopupLayerID(srcLayer){
    if(document.getElementById('ccPopupLayerID')!=null)    
        document.getElementById('ccPopupLayerID').src = srcLayer;
    else
        document.getElementById('PopupLayerID').src = srcLayer;
    showPopupLayerID(true);
}

function showCCPopupLayerID(showLayer){
    showPopupLayerID(showLayer);
}

function showPopupLayerID(showLayer){                   
    if(showLayer){
        document.getElementById('header').style.zIndex= '-1';         
        if(document.getElementById('ccPopupLayerID')!=null)
            document.getElementById('ccPopupLayerID').style.display = 'block';    
        else
            document.getElementById('PopupLayerID').style.display = 'block';    
        document.getElementById('mcPopupWrapper').style.display = 'block';             
    }else{   
        document.getElementById('header').style.zIndex= '0'; 
        if(document.getElementById('ccPopupLayerID')!=null)
        {
            document.getElementById('ccPopupLayerID').src = '';   
            document.getElementById('ccPopupLayerID').style.display = 'block';  
        }
        else
        {
            document.getElementById('PopupLayerID').src = '';     
            document.getElementById('PopupLayerID').style.display = 'none';    
        }
        document.getElementById('mcPopupWrapper').style.display = 'none';  
        
    }        
}   






 