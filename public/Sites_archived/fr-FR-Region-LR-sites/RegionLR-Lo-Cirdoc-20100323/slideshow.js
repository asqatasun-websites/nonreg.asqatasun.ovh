$(function() {
    //  Fix lightbox opacity so that it validates
	$('#overlay').css('opacity', '0.6');
    
    // add background image to  slideshow div and remove it from each li
	$('.slideshow > div').css('background', 'url(/img/carousel_.png) no-repeat center bottom');
    $('.slideshow ul li').css('background-image', 'none');
    
    // create next and prev buttons for jCarousel Lite
    var prevButton = $('<span class="prev">prev<span></span></span>');
    var nextButton = $('<span class="next">next<span></span></span>'); 
    $('.slideshow').append(prevButton);
    $('.slideshow').append(nextButton);    
    
    // create jCarousel
	$(".slideshow > div").jCarouselLite({
      btnNext: ".slideshow .next",
      btnPrev: ".slideshow .prev",
      scroll: 1,
      visible: 1,
      circular: true
    });    
	
    // apply lightbox to necessary links
	//$('.slideshow a[rel*=lightbox]').lightbox();
});	