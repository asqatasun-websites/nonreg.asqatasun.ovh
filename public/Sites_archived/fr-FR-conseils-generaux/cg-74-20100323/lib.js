<!--


// debut positionnement
var NS4 = (document.layers) ? 1 : 0;
var IE4 = (document.all) ? 1 : 0;
var NS6 = (!document.all && document.getElementById) ? 1 : 0;
var MAC= navigator.userAgent.indexOf("Mac")>-1 

// tab ou l'on stocke les popup qui ont deja ete positionne dans popup
var tabPosLayerRel = Array();

// tab pour les donnees des layers
var tabDonneesMenu = Array();
tabDonneesMenu[1] = Array();
tabDonneesMenu[1]['start_x'] = -50;
tabDonneesMenu[1]['start_x_abs'] = 0;
tabDonneesMenu[1]['start_y_abs'] = 0;
tabDonneesMenu[1]['decalage_case_y'] = 0;
tabDonneesMenu[1]['decalage_case_y_racine'] = 0;
tabDonneesMenu[1]['decalage_layer_prof_1'] = 0;
// si no_layer est � 1 , toutes les fonctions font un return 0
if ((MAC) ){

	var no_layer = 0;
	
}
else{
	var no_layer = 0;
}


if (!global_chemin_url)	var global_chemin_url = "http://www.cg74.fr/";
if (!global_chemin_url_images)	var global_chemin_url_images = "http://www.cg74.fr/";

var img_retourne_desactive = false;

var anim=0;
var anim2=0;
var anim3=0;
var anim4=0;

var anim_effet=0;

var nb_layer=5


// traitement des retournements des layers
var tab_retournement = Array();
tab_retournement[0] = Array();
var sens = 1;
tab_retournement[0]['decalage'] = 0;
tab_retournement[0]['sens'] = sens;
var decalage_layer=0;
var taille_fleche_droite = 0;
var taille_fleche_gauche = 0;

var tab_posx_menu = Array();

var taille_fenetre_min = 0;

tab_posx_menu[1] = 0;
tab_posx_menu[2] = 120;
tab_posx_menu[3] = 0;

var tab_posx_abs_menu = Array();
tab_posx_abs_menu[1] = 120;
tab_posx_abs_menu[2] = -5;
tab_posx_abs_menu[3] = 120;

//fin positionnement

//tableau contenant les id des layers 
var tab_layers_visibles = Array();
var tab_item_racine_select = Array();

var tab_id_item_racine_item = Array();


function traitePosPixel(ch)
{
	var tab = ch.match(/[\-0-9]+/g);

	return tab[0];
}





function chgtImgTitre(compteur,url){
	var img = "imgTitre_"+compteur;
	obj_img = document.getElementById(img);
	if (obj_img.src.indexOf('titre_select.gif')>=0) obj_img.src=url+'titre_non_select.gif';
	else obj_img.src=url+'titre_select.gif';
}

function chgtImgFils(compteur,url){
	var img = "imgFils_"+compteur;
	obj_img = document.getElementById(img);
	if (obj_img.src.indexOf('fils_select.gif')>=0) obj_img.src=url+'fils_non_select.gif';
	else obj_img.src=url+'fils_select.gif';
}
function lien(lien,target){
	if (no_layer) return 0;
	if (!target) {
		window.name='idem';
		target='idem'
	}
	if (lien) {
		action = open(lien,target);	
	}
}




function l_d(posx,posy,profondeur,largeur_layer,idItem,numMenu){

	var nom = "im"+idItem;
	var typemenu = "menu"+numMenu;
	
	if (no_layer) return 0;

	if ((IE4)||(NS6)||(NS4)){
		
		decalage_layer = tab_retournement[profondeur-1]['decalage'];
		sens = tab_retournement[profondeur-1]['sens'];
		tab_retournement[profondeur] = Array();
		tab_retournement[profondeur]['decalage'] = decalage_layer;
		
		
		if (((IE4||NS6)&&!MAC)){
			if (!tab_posx_menu) tab_posx_menu = Array();
			if (!tab_posx_menu[numMenu])
			{
				tab_posx_menu[numMenu] = 0;
			}
			if (!tabDonneesMenu) tabDonneesMenu = Array();
			if (!tabDonneesMenu[numMenu])
			{
				tabDonneesMenu[numMenu] = Array();
			}
			if(!tabDonneesMenu[numMenu]['start_x']) tabDonneesMenu[numMenu]['start_x'] = 0;
			if ( (posx>=taille_fenetre_min) && document.body.clientWidth - (posx+tab_posx_menu[numMenu]+tabDonneesMenu[numMenu]['start_x']) < largeur_layer)
			{
				if (sens==1)
				{
					if ((profondeur!=1))
					{
						decalage_tmp = 2*largeur_layer+taille_fleche_gauche;
						posxTmp = posx - (decalage_tmp)+tab_retournement[profondeur]['decalage'];
					}
					else
					{
						decalage_tmp = largeur_layer;
						posxTmp = posx - (decalage_tmp)+tab_retournement[profondeur]['decalage'];
					}
					
					//tabDonneesMenu[numMenu]['start_x']
					if( (posxTmp+3*largeur_layer+tab_posx_menu[numMenu])>=document.body.clientWidth
					  && posxTmp>(tab_posx_abs_menu[numMenu]) )
					{
						// on change de sens
						sens = -1;
						
						posx = posxTmp;
						decalage_tmp = 2*largeur_layer+taille_fleche_gauche+taille_fleche_droite;
						decalage_layer += -(decalage_tmp);
						if ((profondeur!=1))
						{
							tab_retournement[profondeur]['decalage'] = decalage_layer;
						}
						else
						{
							tab_retournement[profondeur]['decalage'] = -largeur_layer+2;
						}
					}
					else
					{
						posx = posx + tab_retournement[profondeur]['decalage'];
					}
					
				}
				else
				{
					var decalage_tmp = 2*largeur_layer+taille_fleche_gauche;
					decalage_layer += -(decalage_tmp);
					posxTmp = posx + decalage_layer;
					
					if( posxTmp>(tab_posx_abs_menu[numMenu]))
					{
						posx = posxTmp;
						tab_retournement[profondeur]['decalage'] = decalage_layer-taille_fleche_droite;
					}
					else
					{
						// on change de sens
						sens = 1;
						
						var decalage_tmp = 0;
						posxTmp = posx - (decalage_tmp)+tab_retournement[profondeur]['decalage'];
						
						tab_retournement[profondeur]['decalage'] -= decalage_tmp;
						posx = posxTmp;
					}
				}
			}
		}
		else if (NS4)
		{
			if (!tabDonneesMenu) tabDonneesMenu = Array();
			if (!tabDonneesMenu[numMenu])
			{
				tabDonneesMenu[numMenu] = Array();
			}
			if (!tabDonneesMenu[numMenu]['start_x_abs']) tabDonneesMenu[numMenu]['start_x_abs'] = 0;
			if (!tabDonneesMenu[numMenu]['start_y_abs']) tabDonneesMenu[numMenu]['start_y_abs'] = 0;
			posx += tabDonneesMenu[numMenu]['start_x_abs'];
			posy += tabDonneesMenu[numMenu]['start_y_abs'];
		}
		
		
		tab_retournement[profondeur]['sens'] = sens;

		if (NS4) {document.write('<layer name="' + nom + '" VISIBILITY="hide" Z-INDEX="' + profondeur + '"  pagey="' + posy + '" pagex="' + posx + '" bgcolor="#FFFFFF">');}
		if ((IE4)||(NS6)) {document.write('<span ID="' + nom + '" STYLE="POSITION: absolute; Z-INDEX: ' + profondeur + '; VISIBILITY: hidden; TOP: ' + posy + 'px; LEFT: ' + posx + 'px;">');}

		if ((typemenu=="menu2")&&(profondeur=="1"))
		{
			if (sens==1)
			{
				document.write('<table border="0" cellspacing="0" cellpadding="0" width="'+largeur_layer+'"><tr><td colspan=3 id="petit"><table border=0 cellpadding=0 cellspacing=0 width="100%"><tr><td id="petit" bgcolor="">&nbsp;&nbsp;</td><td id="text10" bgcolor="#FFFFFF" width="99%">&nbsp;</td></tr></table></tr>');
			}
			// retournement du layer de profondeur 1 pour le menu 2
			else
			{
				document.write('<table border="0" cellspacing="0" cellpadding="0" width="'+(largeur_layer+2)+'"><tr><td colspan=\"3\" id="petit"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width="100%"><tr><td id="text10" bgcolor="#FFFFFF" width="99%">&nbsp;</td><td id="petit" bgcolor="">&nbsp;&nbsp;</td></tr></table></tr>');
			}
			
		}
		else
		{
			if (typemenu=="menu1"){document.write('<table border="0" cellspacing="0" cellpadding="0" width="175">');}
			else {document.write('<table border="0" cellspacing="0" cellpadding="0" width="175">');}
		}
	}
}	


function donnePosLayerRel(idLayer,layerContenantIdItemPointe,listeFilsLayerPrecedant,numMenu,profondeur,numItem,listeFilsLayerSuivant)
{
	if (NS4) return;
	
	// menu vertical : on ne fait rien
	if (listeFilsLayerPrecedant==-1) return;
		
	// pour les items racines
	if (layerContenantIdItemPointe==-1 || layerContenantIdItemPointe=='' || layerContenantIdItemPointe==0)
	{
		//if(numMenu!=3 || tabDonneesMenu[numMenu]['start_y_ok'])
		//{
			if (!tabDonneesMenu) tabDonneesMenu = Array();
			if (!tabDonneesMenu[numMenu])
			{
				tabDonneesMenu[numMenu] = Array();
			}
			if (!tabDonneesMenu[numMenu]['start_y_ok']) tabDonneesMenu[numMenu]['start_y_ok'] = 0;
			if (tabDonneesMenu[numMenu]['start_y_ok'])
			{
				var pos = tabDonneesMenu[numMenu]['start_y_ok'];
			}
			else
			{
				var pos = 0;
			}
		/*}
		else
		{
			if (!tabItemRacineMenu) return;
			
			var pos = 0;
			pos = eval(""+pos+"+"+tabItemRacineMenu[numMenu]['decalage']);
			
			var listeItemRacine = tabItemRacineMenu[numMenu]['listeItemRacine'];
			var tabListeItemRacine = Array();
			var nbListeItemRacine = 0;
			if (listeItemRacine.length>0)
			{
				listeItemRacine+="_";
				tabListeItemRacine = listeItemRacine.split('_');
				nbListeItemRacine = tabListeItemRacine.length-1;
				if (NS4)
				{
					// NE MARCHE PAS
				}
				else if (IE4)
				{
					for(var i=0; i<nbListeItemRacine; i++)
					{
						var objTmp = document.all["posLayerRel_"+tabListeItemRacine[i]];
						if (objTmp)
						{
							pos+= objTmp.offsetTop;
						}
					}
				}
				else if (NS6)
				{
					for(var i=0; i<nbListeItemRacine; i++)
					{
						var objTmp = document.getElementById("posLayerRel_"+tabListeItemRacine[i]);
						if (objTmp)
						{
							pos+= objTmp.offsetTop;
						}
					}
				}
			}
			tabDonneesMenu[numMenu]['start_y_ok'] = pos;
		}*/
	}
	else
	{
		var libLayerContenantIdItemPointe = "im"+layerContenantIdItemPointe;
		
		if (IE4)
		{
			var pos = traitePosPixel(document.all[libLayerContenantIdItemPointe].style.top);
		}
		else if (NS6)
		{
			var pos = traitePosPixel(document.getElementById(libLayerContenantIdItemPointe).style.top);
		}
		
		if (!tabDonneesMenu) tabDonneesMenu = Array();
		if (!tabDonneesMenu[numMenu])
		{
			tabDonneesMenu[numMenu] = Array();
		}
		if (!tabDonneesMenu[numMenu]['decalage_layer_prof_1']) tabDonneesMenu[numMenu]['decalage_layer_prof_1'] = 0;
		
		if (profondeur==1 && numItem==1) pos = eval(""+pos+"+"+tabDonneesMenu[numMenu]['decalage_layer_prof_1']);
	}
	

	var libLayerCourant = "im"+idLayer;
	var libPosLayerRelItem = "posLayerRel_"+idLayer;
	
	
	var tabListeFilsLayerPrecedant = Array();
	var nbFilsLayerPrecedant = 0;

	if (listeFilsLayerPrecedant && listeFilsLayerPrecedant.length>0)
	{
		listeFilsLayerPrecedant+="_";
		tabListeFilsLayerPrecedant = listeFilsLayerPrecedant.split('_');
		nbFilsLayerPrecedant = tabListeFilsLayerPrecedant.length-1;
	}

	var tabListeFilsLayerSuivant = Array();
	var nbFilsLayerSuivant = 0;
	
	if (listeFilsLayerSuivant)
	{
		if(listeFilsLayerSuivant.length>0)
		{
			listeFilsLayerSuivant+="_";
			tabListeFilsLayerSuivant = listeFilsLayerSuivant.split('_');
			nbFilsLayerSuivant = tabListeFilsLayerSuivant.length;
		}
	}

	var pos2 = 0;
	// car il y a x px entre chaque cellule
	if(profondeur==0)
	{
		if (!tabDonneesMenu) tabDonneesMenu = Array();
		if (!tabDonneesMenu[numMenu])
		{
			tabDonneesMenu[numMenu] = Array();
		}
		if (!tabDonneesMenu[numMenu]['decalage_case_y_racine']) tabDonneesMenu[numMenu]['decalage_case_y_racine'] = 0;
		pos2 = nbFilsLayerPrecedant*tabDonneesMenu[numMenu]['decalage_case_y_racine'];
	}
	else
	{
		if (!tabDonneesMenu) tabDonneesMenu = Array();
		if (!tabDonneesMenu[numMenu])
		{
			tabDonneesMenu[numMenu] = Array();
		}
		if (!tabDonneesMenu[numMenu]['decalage_case_y']) tabDonneesMenu[numMenu]['decalage_case_y'] = 0;
		pos2 = nbFilsLayerPrecedant*tabDonneesMenu[numMenu]['decalage_case_y'];
	}
	
	if (!tabDonneesMenu) tabDonneesMenu = Array();
	if (!tabDonneesMenu[numMenu])
	{
		tabDonneesMenu[numMenu] = Array();
	}
	if (!tabDonneesMenu[numMenu]['decalage_case_y']) tabDonneesMenu[numMenu]['decalage_case_y'] = 0;

	if (listeFilsLayerSuivant>0) pos2 -= nbFilsLayerSuivant*tabDonneesMenu[numMenu]['decalage_case_y'];
	if (NS4)
	{
		// NE MARCHE PAS
	}
	else if (IE4)
	{
		for(var i=0; i<nbFilsLayerPrecedant; i++)
		{
			var objTmp = document.all["posLayerRel_"+tabListeFilsLayerPrecedant[i]];
			if (objTmp)
			{
				pos2+= objTmp.offsetTop;
			}
		}
		if (nbFilsLayerSuivant>0)
		{
			var objTmp = document.all["posLayerRel_"+tabListeFilsLayerSuivant[0]];
			if (objTmp)
			{
				pos2-= objTmp.offsetTop;
			}
		}
		
		document.all[libLayerCourant].style.top = pos;
		document.all[libLayerCourant].style.top = eval(""+traitePosPixel(document.all[libLayerCourant].style.top)+"+"+pos2);
		
		tabPosLayerRel[idLayer] = 1;
	}
	else if (NS6)
	{
		for(var i=0; i<nbFilsLayerPrecedant; i++)
		{
			var objTmp = document.getElementById("posLayerRel_"+tabListeFilsLayerPrecedant[i]);
			if(objTmp)
			{
				pos2+= objTmp.offsetTop;
			}
		}
		
		for(var i=0; i<nbFilsLayerSuivant; i++)
		{
			var objTmp = document.getElementById("posLayerRel_"+tabListeFilsLayerSuivant[i]);
			if (objTmp)
			{
				pos2-= objTmp.offsetTop;
			}
		}
		
		document.getElementById(libLayerCourant).style.top = pos;
		
		document.getElementById(libLayerCourant).style.top = eval(""+traitePosPixel(document.getElementById(libLayerCourant).style.top)+"+"+pos2);
	}
}




// effacer cheminPereinferieur - ne sert plus
function l_l(titre,ch_style,ch_href,ch_target,tempo,cheminPereinferieur,cheminPere,idItem,hauteur_td,typemenu,numItem,nb_item,profondeur,listeFilsLayerPrecedant,numMenu){
	
	if (no_layer) return 0;
	layerContenantIdItemPointe = '';
	tabCheminPereinferieur = cheminPere.split('_');
	cheminPereinferieur = "";
	var l = tabCheminPereinferieur.length;
	
	if (l>=0)
	{
		var itemRacine = tabCheminPereinferieur[0];
	}
	else var itemRacine = "";
	
	if(l>0)
	{
		l--;
		layerContenantIdItemPointe = tabCheminPereinferieur[l];
		for(var i =0; i<l; i++)
		{
			tab_id_item_racine_item[tabCheminPereinferieur[i]] = itemRacine;
			
			cheminPereinferieur += tabCheminPereinferieur[i]+"_";
		}
		cheminPereinferieur = cheminPereinferieur.substr(0, cheminPereinferieur.length-1);
	}

	typemenu = "menu"+typemenu;
	
	var balise_lien="";
	var balise_lien_fermante="";
	var balise_lien_ns4="";
	var balise_lien_ns4_fermante="";
	var cellule_fleche_exterieur='';
	var cellule_avec_fleche='';
	var fin_de_table_et_cellule='';
	var ouverture_cellule_avec_lien = '';
	
	if (tempo == 1){
		 ch_onmouseover = "desactive_tempo();desactiver(\'" + cheminPere + "\');";
		 //ch_onmouseout = "tempo(\'" + cheminPereinferieur + "\');";
		 ch_onmouseout = "tempo2(\'-1\');";
	}
	else if (tempo == 2){ 
		ch_onmouseover = "popUp("+ idItem + ",\'" + cheminPere + "\', "+layerContenantIdItemPointe+", \'"+listeFilsLayerPrecedant+"\', "+numMenu+","+profondeur+", '-1', "+numItem+");";
		//$onmouseout = "desactive_tempo();";// onmouseout=tempo() ";
		//ch_onmouseout = "tempo(\'" + cheminPereinferieur + "\');tempo2(\'" + cheminPere + "\');";
		ch_onmouseout = "tempo2(\'-1\');";
	}
	
	
	// cellule_fleche_exterieur
	if ((typemenu=="menu2")&&(profondeur=="1")) {cellule_fleche_exterieur = "<td id='petit' bgcolor=''>&nbsp;&nbsp;</td>";}
	else 
	{
		cellule_fleche_exterieur = "<td width='15' align='right' valign='top'";
		
			//((numItem=="1")&&((typemenu=="menu1")||(typemenu=="menu2")||((typemenu=="menu3")&&(profondeur!='1')))&&(cheminPere!="68_99")&&(cheminPere!="68_84")&&(cheminPere!="15_388")&&(cheminPere!="15_389")&&(cheminPere!="15_390"))
			//||((numItem==nb_item)&&(((typemenu=="menu3")&&(profondeur=='1'))||(cheminPere=="68_99")||(cheminPere=="68_84")||(cheminPere=="15_388")||(cheminPere=="15_389")||(cheminPere=="15_390")))
			
		
		/*if 	(
			((numItem=="1")&&((typemenu=="menu1")||(typemenu=="menu2")||((typemenu=="menu3")&&(profondeur!='1')))&&(cheminPere!="15_389")&&(cheminPere!="15_388")&&(cheminPere!="15_390"))
			||((numItem==nb_item)&&(((typemenu=="menu3")&&(profondeur=='1'))||(cheminPere=="15_389")||(cheminPere=="15_388")||(cheminPere=="15_390")))
			) 
			*/
		/*if (
			(numItem=="1"&&(typemenu=="menu1"||typemenu=="menu2"||(typemenu=="menu3"&&profondeur!=1)))
			||
			(numItem==nb_item && typemenu=="menu3" && profondeur==1)
		   )
			{
			if (sens==1 || img_retourne_desactive) {cellule_fleche_exterieur += " bgcolor='#DEE4EC'><img src='" + global_chemin_url_images + "images/bord_hd.gif' width='8' height='20'";}
			else {cellule_fleche_exterieur += " bgcolor='#DEE4EC'><img src='" + global_chemin_url_images + "images/bord_hd_d.gif' width='8' height='20'";}
			}
		if (sens==1 || img_retourne_desactive) var ch_tmp = "";
		else ch_tmp = "<IMG src='" + global_chemin_url_images + "images/vide.gif' width='25' height='1'>";
	
		cellule_fleche_exterieur += ">"+ch_tmp+"</td>";*/
	}
	
	
	// BALISE_LIEN
	var class_over="lienmenuover";
	var class_out="lienmenuout";
	var class_over2="lienmenuover2";
	var class_out2="lienmenuout2";
	if ((ch_href=="#")||(!ch_href)||(ch_href=="index.php"))
		{
		//alert(cheminPere+":"+idItem);
		ch_href="";
		class_over="lienmenuoverdeca";
		class_out="lienmenuoutdeca";
		class_over2="lienmenuover2deca";
		class_out2="lienmenuout2deca";
		}
	
	balise_lien_ns4= "<TD id='petit'><A id=\"" + ch_style + "\" ";

	if (ch_href)
		{
		if (ch_href.indexOf("http://")<0 && ch_href.indexOf("/")!=0 && ch_href.indexOf("https://")<0 && ch_href.indexOf("mailto:")<0){
			ch_href = global_chemin_url + ch_href;
		}
	
		if (ch_href != "")
			{
			balise_lien_ns4 += "href=\"" + ch_href + "\"";
			
			if (ch_target != "") balise_lien_ns4+= "target=\"" + ch_target + "\"";
			}
		}
			
	balise_lien_ns4 += " onmouseover=\"" + ch_onmouseover + "\" onmouseout=\"" + ch_onmouseout + "\" ";
	balise_lien_ns4_fermante += "</A>";
	
	//cellule_avec_fleche
    /*if (tempo=="2"){
		if (sens==1 || img_retourne_desactive){
			cellule_avec_fleche +="<td width='4' valign='top'><img src='" + global_chemin_url_images + "images/bord_hd.gif' width='8' height='20'></td>";
		}
		else{
			cellule_avec_fleche +="<td width='8' valign='top' id='petit'><img src='" + global_chemin_url_images + "images/bord_hd_d.gif' width='8' height='20'>&nbsp;&nbsp;&nbsp;</td>";
		}
    }
	else{
		cellule_avec_fleche +="<td width='8'><img src='" + global_chemin_url_images + "images/vide.gif' width='8' height='20'></td>";
	}*/
	
	if (numMenu==2 && profondeur==1) width_tmp=178;
	else width_tmp='100%';


	if (MAC){
		balise_lien = "<td class=\"" + class_out + "\" onMouseOver=\""+ ch_onmouseover + "\" "+
		  "onMouseOut=\"" + ch_onmouseout + "\" "+
		  "onclick=\'lien_td(\""+ch_href+"\",\""+ch_target+"\");\'>"+
		  "<table cellpadding='0' cellspacing='0' width='"+width_tmp+"' border='0'><tr>"+
		  "<td id='petit' width='3'>&nbsp;</td>";
		  
		  ouverture_cellule_avec_lien += "<td height=\"" + hauteur_td + "\"  "+
			  "class=\"" + class_out2 + "\" "+
			  ">";
	}
	else{
		
		balise_lien = "<td class=\"" + class_out + "\" onMouseOver=\"this.className=\'" + class_over + "\';" + ch_onmouseover + "\" "+
		  "onMouseOut=\"this.className=\'" + class_out + "\';" + ch_onmouseout + "\" "+
		  "onclick=\'lien_td(\""+ch_href+"\",\""+ch_target+"\");\'>"+
		  "<table cellpadding='0' cellspacing='0' width='"+width_tmp+"' border='0'><tr>"+
		  "<td id='petit' width='3'>&nbsp;</td>";
		  
		ouverture_cellule_avec_lien += "<td height=\"" + hauteur_td + "\"  "+
			  "class=\"" + class_out2 + "\" onMouseOver=\"this.className=\'" + class_over2 + "\';\" "+
			  "onMouseOut=\"this.className=\'" + class_out2 + "\'; \">";
	  
	}
      	
      	
    if (NS4) {balise_lien+=balise_lien_ns4;balise_lien_fermante+=balise_lien_ns4_fermante}
	balise_lien_fermante += "</td>";
	
	fin_de_table_et_cellule = "</tr></table>";
      	
	if (NS4){
	  	//fin_de_table_et_cellule+= "<layer name='posLayerRel_"+idItem+"'></layer>";
	}
	else{
	 	if (!NS4) fin_de_table_et_cellule+= "<div id='posLayerRel_"+idItem+"' style='LEFT: 0px; POSITION: relative; TOP: 0px; '></div>";
	}
	fin_de_table_et_cellule+= "</td>";

	if (sens==1 || img_retourne_desactive) {
		var res = balise_lien + ouverture_cellule_avec_lien + titre + balise_lien_fermante + cellule_avec_fleche + fin_de_table_et_cellule;
	}

	else {
		var res = balise_lien + cellule_avec_fleche + ouverture_cellule_avec_lien + titre + balise_lien_fermante + fin_de_table_et_cellule;
	}
	
	
	
	document.write('<tr>');
	document.write(res);
	//alert(res);
	//document.write('</tr><tr>');
	document.write('</tr>');
	
	
}

function l_f(){

	if (no_layer) return 0;
	
	if ((IE4)||(NS6)||(NS4)){
		document.write('</table>');
		if (NS4) {document.write('</layer>');}
		if ((IE4)||(NS6)) {document.write('</span>');}
	}
}






// debut menu

function popUp(idLayer,desactive,layerContenantIdItemPointe,listeFilsLayerPrecedant,numMenu,profondeur,listeFilsLayerSuivant,numItem) 
	{
	
	if (no_layer) return 0;
	if (!(numItem>0)) numItem = -1;
	if (tabPosLayerRel[idLayer]!=1) donnePosLayerRel(idLayer,layerContenantIdItemPointe,listeFilsLayerPrecedant,numMenu,profondeur,numItem,listeFilsLayerSuivant);
	
	var menuName = "im"+idLayer;
	//popUp2(menuName,desactive);
	
	if (document.images) 
        	{
        	clearTimeout(anim2);
        	desactive_tempo();
        	anim2=setTimeout ("popUp2('" + idLayer + "','" + desactive + "')", 80);
		}
	
	}

function popUp2(idLayer,desactive) 
{
	var menuName = "im"+idLayer;
	if (no_layer) return 0;
	if (menuName)
	{
		if (desactive == -1) desactiver2(-1);
		else desactiver2(desactive); 
		
		//desactive_tempo();	
	
		if (NS4) document.layers[menuName].visibility = "show" 
		if (IE4) document.all[menuName].style.visibility = "visible";
		if (NS6) document.getElementById(menuName).style.visibility="visible";	
		
		tab_layers_visibles[tab_layers_visibles.length]=idLayer;
		effet_mouse_over(idLayer, 1);		
	}
}



function popDown(idLayer)
	{
	
	var menuName = "im"+idLayer;
	
	if (no_layer) return 0;
	//alert("popDown " + menuName);
	if (document.images) 
	{
		//clearTimeout(anim);
		if (NS6) document.getElementById(menuName).style.visibility="hidden";	
		if (NS4) document.layers[menuName].visibility = "hide" 
		if (IE4 && document.all[menuName]) document.all[menuName].style.visibility = "hidden";
		fleche_blanche_gauche(idLayer,0);
		fleche_blanche_droite(idLayer,0);
	}
}

function desactiver(ids_layers)
	{
	
	if (no_layer) return 0;
	
	
	if (document.images) 
        	{
        	clearTimeout(anim3);
        	anim3=setTimeout ("desactiver2('" + ids_layers + "')", 80);
	        }
	}
function desactiver2(ids_layers)
	{
	if (no_layer) return 0;
	var reg=new RegExp("[_]+", "g");
	var chaine=new String(ids_layers);
	var tab_ids=chaine.split(reg);
	//var tab_ids=Array();

	if (tab_ids.length == 0){ 
		tab_ids = new Array(chaine);
	}
	
	var new_tab_visible = new Array();
	
	
	for (i=0;i<tab_layers_visibles.length;i++)
		{
			//alert(i + " " + tab_layers_visibles[i]);
			
			var reste_visible = 0;		
			
			for (h=0;h<tab_ids.length;h++){	
				var id = tab_ids[h];
				//alert("desactive sauf" + id);
				if (tab_layers_visibles[i] == id){ 
					reste_visible = 1;
				}
			}
			
			if (reste_visible == 0){ 
				popDown(tab_layers_visibles[i]);
	
			}
			else{
				new_tab_visible[new_tab_visible.length] = tab_layers_visibles[i];
			}
			
		}
		
	// on vide le tableau		
	//tab_layers_visibles.splice(0,tab_layers_visibles.length);
	tab_layers_visibles.length=0;
	
	//on r� ins�re les �l�ments rest�s ouverts
	//tab_layers_visibles.push("im"+id);
	for (i=0;i<new_tab_visible.length;i++){
		
		tab_layers_visibles[tab_layers_visibles.length] = new_tab_visible[i];
	}
	
	if (tab_layers_visibles.length==0) effet_mouse_out(-1);
}

function tempo(id) 
	{
	
	
	if (no_layer) return 0;
	
	if (document.images) 
        	{
        	clearTimeout(anim);
        	anim=setTimeout ("desactiver('" + id + "')", 500);
	        }
	}

function tempo2(ids_layers)
{
	if (no_layer) return 0;
	
	
	if (document.images) 
        	{
        	clearTimeout(anim4);
        	anim4 = setTimeout ("desactiver('"+ids_layers+"')", 300);
	        }
}

function desactive_tempo() 
	{
	
	if (no_layer) return 0;
	
		
	if (document.images) 
        	{
        	clearTimeout(anim);
        	clearTimeout(anim2);
        	clearTimeout(anim3);
        	clearTimeout(anim4);
        	}
	}

function lien_td(lien,target)
	{
	
	if (no_layer) return 0;
	
	
	if (!target) 
		{
		window.name='idem';
		target='idem'
		}
	if (lien) {
		action_td = open(lien,target);
		
	}

}



function class_td(nom_objet,classout){
	
	if (NS4){
	}
	else{
		if (NS4) objet = document.layers[nom_objet];
		if (IE4) objet = document.all[nom_objet];
		if (NS6) objet = document.getElementById(nom_objet);

		if (objet)
		{
			objet.className = classout;
		}
	}
}

function effet_mouse_over(idLayer, elt_seul)
{
	var menuName = "im"+idLayer;
	
	if (!elt_seul) effet_mouse_out(idLayer);
	
	class_td("td" + menuName,'lienout');
	class_td("soustd" + menuName,'lienout');
	class_td("a" + menuName,'lienout');
	
	tab_item_racine_select[tab_item_racine_select.length] = menuName;
	fleche_blanche_gauche(idLayer,1);
	fleche_blanche_droite(idLayer,1);
}



function effet_mouse_out_tempo()
{
	clearTimeout(anim_effet);
	anim_effet = setTimeout("effet_mouse_out('-1')", 1000);
}


function effet_mouse_out(idLayer)
{
	var menuName = "im"+idLayer;
	
	clearTimeout(anim_effet);
	
	var l = tab_item_racine_select.length;
	for (var i=0; i<l; i++)
	{
		var idItemRacine = tab_item_racine_select[i];
		
		if (idItemRacine!=idLayer)
		{
			class_td("td" + idItemRacine,'lienout');
			class_td("soustd" + idItemRacine,'lienout');
			class_td("a" + idItemRacine,'lienout');
		}
	}
	
	//alert(l+"  "+tab_item_racine_select.length);
	tab_item_racine_select.length = 0;
		
}

var tab_id_item_racine=Array();

function fleche_blanche_gauche(idLayer,param){
	var img = "fleche_"+idLayer;
	var test = 0;
	var l = tab_id_item_racine.length;

	obj_img = document.getElementById(img);
	if (!obj_img) {
		return "";
	}
	//test que l'item ne soit pas d�j� dans le tableau
	for (var i=0; i<l; i++)	{
		if (idLayer==tab_id_item_racine[i]){
			test=1;
		}
	}
	if (test==0) {
		tab_id_item_racine[l]=idLayer;
		l=l+1;
	}
	
	var src1 = global_chemin_url_images+"images/menu_1/fleche1.png";
	var src2 = global_chemin_url_images+"images/menu_1/fleche2.png";
	
	//suppression de toutes les images coch�es
	for (var i=0; i<l; i++)	{
		var img1 = "fleche_"+tab_id_item_racine[i];
		obj_img1 = document.getElementById(img1);
		obj_img1.src=src1;
	}
	
	
	if (param==1) obj_img.src=src2;
	else obj_img.src=src1;
}


function fleche_blanche_droite(idLayer,param){
	var img = "select_"+idLayer;
	var test = 0;
	var l = tab_id_item_racine.length;
	obj_img = document.getElementById(img);
	
	
	//test que l'item ne soit pas d�j� dans le tableau
	for (var i=0; i<l; i++)	{
		if (idLayer==tab_id_item_racine[i]){
			test=1;
		}
	}
	if (test==0) {
		tab_id_item_racine[l]=idLayer;
		l=l+1;
	}
		
	var src1 = global_chemin_url_images+"images/vide.gif";
	var src2 = global_chemin_url_images+"images/menu_1/select.gif";
	
	//suppression de toutes les images coch�es
	for (var i=0; i<l; i++)	{
		var img1 = "select_"+tab_id_item_racine[i];
		obj_img1 = document.getElementById(img1);
		obj_img1.src=src1;
	}
	
	if (param==1) obj_img.src=src2;
	else obj_img.src=src1;
}


//fin menu


function flash_fermer(){
	var mon_div = document.getElementById('div_carte_voeux');
	mon_div.style.display='none';
}

function flash_redirection(url){
	if (url) window.open(url);
}

function genere_menu_1()
{

// GENERE_MENU_1
l_d(268,13,1,175,1,1);
l_l("L&#039;institution","menuNAV","pages/fr/menu1/le-conseil-general/l-institution/histoire-4.html","",1,"","1",6,21,1,1,11,1,"",1);
l_l("Les �lus","menuNAV","pages/fr/menu1/le-conseil-general/les-elus/president-et-vice-presidents-5.html","",1,"","1",124,21,1,2,11,1,"6",1);
l_l("Les commissions","menuNAV","pages/fr/menu1/le-conseil-general/les-commissions/la-commission-permanente-22.html","",1,"","1",8,21,1,3,11,1,"124",1);
l_l("Les services","menuNAV","pages/fr/menu1/le-conseil-general/les-services/services-generaux-111.html","",1,"","1",125,21,1,4,11,1,"8",1);
l_l("Le budget","menuNAV","pages/fr/menu1/le-conseil-general/le-budget/le-vote--du-budget-54.html","",1,"","1",126,21,1,5,11,1,"125",1);
l_l("Les actes de l&#039;Assembl�e","menuNAV","pages/fr/menu1/le-conseil-general/les-actes-de-l-assemblee/les-d-eacute-lib-eacute-rations-de-la-commission-permanente-350.html","",1,"","1",127,21,1,6,11,1,"126",1);
l_l("La coop�ration internationale","menuNAV","pages/fr/menu1/le-conseil-general/la-cooperation-internationale-57.html","",1,"","1",128,21,1,7,11,1,"127",1);
l_l("L&#039;Assembl�e des Pays de Savoie","menuNAV","pages/fr/menu1/le-conseil-general/l-assemblee-des-pays-de-savoie/le-fonctionnement-58.html","",1,"","1",129,21,1,8,11,1,"128",1);
l_l("Espace presse","menuNAV","pages/fr/menu1/le-conseil-general/espace-presse/communiques-de-presse-101.html","",1,"","1",130,21,1,9,11,1,"129",1);
l_l("Les publications","menuNAV","pages/fr/menu1/le-conseil-general/les-publications/haute-savoie--le-magazine-du-conseil-general-103.html","",1,"","1",131,21,1,10,11,1,"130",1);
l_l("La coop�ration d�centralis�e","menuNAV","pages/fr/menu1/le-conseil-general/la-cooperation-decentralisee/qu-est-ce-que-la-cooperation-decentralisee-417.html","",1,"","1",342,21,1,11,11,1,"131",1);
l_f();
l_d(268,13,1,175,2,1);
l_l("Donn�es statistiques d�partementales","menuNAV","pages/fr/menu1/la-haute-savoie/donnees-statistiques-departementales-156.html","",1,"","2",14,21,1,1,7,1,"",1);
l_l("Les communes de Haute-Savoie","menuNAV","http://www.cg74.fr/index.php?id=itemmenu_communes_15__1_1","",1,"","2",15,21,1,2,7,1,"14",1);
l_l("Histoire","menuNAV","pages/fr/menu1/la-haute-savoie/histoire-62.html","",1,"","2",228,21,1,3,7,1,"15",1);
l_l("G�ographie","menuNAV","pages/fr/menu1/la-haute-savoie/geographie-63.html","",1,"","2",133,21,1,4,7,1,"228",1);
l_l("Economie","menuNAV","pages/fr/menu1/la-haute-savoie/economie-64.html","",1,"","2",134,21,1,5,7,1,"133",1);
l_l("Tourisme","menuNAV","pages/fr/menu1/la-haute-savoie/tourisme-65.html","",1,"","2",135,21,1,6,7,1,"134",1);
l_l("Culture et patrimoine","menuNAV","pages/fr/menu1/la-haute-savoie/culture-et-patrimoine-66.html","",1,"","2",136,21,1,7,7,1,"135",1);
l_f();
l_d(268,13,1,175,3,1);
l_l("Les missions","menuNAV","pages/fr/menu1/les-actions/les-missions-132.html","",1,"","3",241,21,1,1,8,1,"",1);
l_l("Social, sant�","menuNAV","pages/fr/menu1/les-actions/social--sante/le-depistage-du-cancer-du-sein-74.html","",1,"","3",242,21,1,2,8,1,"241",1);
l_l("Routes, transports","menuNAV","pages/fr/menu1/les-actions/routes--transports/le-contournement-de-poisy-76.html","",1,"","3",245,21,1,3,8,1,"242",1);
l_l("Education, Universit�","menuNAV","pages/fr/menu1/les-actions/education--universite/la-politique-de-l-education-258.html","",1,"","3",250,21,1,4,8,1,"245",1);
l_l("Am�nagement du territoire","menuNAV","pages/fr/menu1/les-actions/amenagement-du-territoire/le-schema-d-amenagement-departemental-78.html","",1,"","3",251,21,1,5,8,1,"250",1);
l_l("Environnement","menuNAV","pages/fr/menu1/les-actions/environnement/la-politique-de-l-environnement-249.html","",1,"","3",291,21,1,6,8,1,"251",1);
l_l("Culture et patrimoine","menuNAV","pages/fr/menu1/les-actions/culture-et-patrimoine/l-operation-grand-site-de-sixt-fer-a-cheval-81.html","",1,"","3",258,21,1,7,8,1,"291",1);
l_l("S�curit� routi�re","menuNAV","pages/fr/menu1/les-actions/kit-de-conduite-421.html","",1,"","3",312,21,1,8,8,1,"258",1);
l_f();

// FIN_GENERE_MENU_1

}

if (NS4) genere_menu_1();


function genere_menu_2()
{

// GENERE_MENU_2
l_d(0,98,1,175,150,2);
l_l("Coordonn�es et organigramme","menuNAV","pages/fr/menu2/espace-emploi/coordonnees-245.html","",1,"","150",282,21,2,1,4,1,"",2);
l_l("Offres d&#039;emploi","menuNAV","http://www.cg74.fr/index.php?id=itemmenu_offreemploi_277_1_1","",1,"","150",277,21,2,2,4,1,"282",2);
l_l("Job Alerte","menuNAV","http://www.cg74.fr/index.php?id=itemmenu_extranet_276__1_1&amp;op_ext=accueil","",1,"","150",276,21,2,3,4,1,"277",2);
l_l("Faire une demande de stage","menuNAV","pages/fr/menu2/espace-emploi/formulaire-de-candidature-a-un-stage-215.html","",1,"","150",284,21,2,4,4,1,"276",2);
l_f();
l_d(0,98,1,175,149,2);
l_l("Social, sant�","menuNAV","pages/fr/menu2/liens-utiles/social--sante-12.html","",1,"","149",212,21,2,1,9,1,"",2);
l_l("Routes, transports","menuNAV","pages/fr/menu2/liens-utiles/routes--transports-13.html","",1,"","149",213,21,2,2,9,1,"212",2);
l_l("Education, Universit�","menuNAV","pages/fr/menu2/liens-utiles/education--universite-14.html","",1,"","149",214,21,2,3,9,1,"213",2);
l_l("Economie, nouvelles technologies","menuNAV","pages/fr/menu2/liens-utiles/economie--nouvelles-technologies-15.html","",1,"","149",215,21,2,4,9,1,"214",2);
l_l("Am�nagement du territoire","menuNAV","pages/fr/menu2/liens-utiles/amenagement-du-territoire-16.html","",1,"","149",216,21,2,5,9,1,"215",2);
l_l("Culture et patrimoine","menuNAV","pages/fr/menu2/liens-utiles/culture-et-patrimoine-17.html","",1,"","149",217,21,2,6,9,1,"216",2);
l_l("Tourisme et sports","menuNAV","pages/fr/menu2/liens-utiles/tourisme-et-sports-18.html","",1,"","149",218,21,2,7,9,1,"217",2);
l_l("Justice","menuNAV","pages/fr/menu2/liens-utiles/justice/justice-340.html","",2,"","149",322,21,2,8,9,1,"218",2);
l_l("Coop�ration internationale","menuNAV","pages/fr/menu2/liens-utiles/cooperation-internationale-364.html","",1,"","149",338,21,2,9,9,1,"322",2);
l_f();
l_d(-13,102,2,175,322,2);
l_l("Justice","menuNAV","pages/fr/menu2/liens-utiles/justice/justice-340.html","",1,"149","149_322",323,21,2,1,1,2,"",2);
l_f();
l_d(0,98,1,175,329,2);
l_l("Communiqu�s de presse","menuNAV","pages/fr/menu2/espace-presse/communiques-de-presse-101.html","",1,"","329",339,21,2,1,3,1,"",2);
l_l("Agenda du Conseil","menuNAV","pages/fr/menu2/espace-presse/agenda-du-conseil-102.html","",1,"","329",340,21,2,2,3,1,"339",2);
l_l("Charte graphique","menuNAV","pages/fr/menu2/espace-presse/charte-graphique-136.html","",1,"","329",341,21,2,3,3,1,"340",2);
l_f();

// FIN_GENERE_MENU_2

}

if (NS4) genere_menu_2();


function genere_menu_3()
{

// GENERE_MENU_3
l_d(0,23,1,175,267,3);
l_f();

// FIN_GENERE_MENU_3

}

if (NS4) genere_menu_3();


function genere_menu_4()
{

// GENERE_MENU_4

// FIN_GENERE_MENU_4

}

if (NS4) genere_menu_4();




//  --> 