var accordeon = function(){
   $(".accordeon dd").hide();
   $(".accordeon dt a.lien-plus-moins").click(function () {			
			var theParent = $(this).parent();
			// Si le contenu etait deja ouvert, on le referme :      
      	if ($(theParent).hasClass("ddOuvert")) {
				$(theParent).removeClass("ddOuvert");
		   	$(theParent).next("dd").slideUp();				
			}
			else{// Si le contenu etait cache, on l'affiche :
				$(theParent).addClass("ddOuvert")
				$(theParent).next("dd").slideDown("normal");
			}
			return false;
      });
}

/* onload */
$(document).ready(function(){
   accordeon();
	$(".agenda .accordeon dd").eq(0).show();
	$(".agenda .accordeon dt").eq(0).addClass("ddOuvert");
	$(".agenda .accordeon dd").eq(1).show();
	$(".agenda .accordeon dt").eq(1).addClass("ddOuvert");
	$(".agenda .accordeon dd").eq(2).show();
	$(".agenda .accordeon dt").eq(2).addClass("ddOuvert");
	$(".interviews .accordeon dd").eq(0).show();
	$(".interviews .accordeon dt").eq(0).addClass("ddOuvert");
	$(".interviews .accordeon dd").eq(1).show();
	$(".interviews .accordeon dt").eq(1).addClass("ddOuvert");
	$(".interviews .accordeon dd").eq(2).show();
	$(".interviews .accordeon dt").eq(2).addClass("ddOuvert");
});
/* END onload */