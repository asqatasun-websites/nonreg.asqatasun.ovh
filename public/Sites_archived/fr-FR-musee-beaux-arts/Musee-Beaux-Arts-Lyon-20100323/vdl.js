/*******************************************************************
 $Id: vdl.js,v 1.19 2006/11/13 13:23:16 pmaris Exp $
********************************************************************/

function MM_swapImage() { //v3.0
	var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
	var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
	document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function setFocus() {
   field = document.getElementById('field_focus')
   if (field) {
	  field.focus();
   }
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function autoOpen(link) {
	h = 400; w = 700; t = 50; l = 50;
	x=window.open(link.href, 'popup', 'height='+h+', width='+w+', top='+t+', left='+l+', resizable=yes, status=yes');
	x.focus();
}	

// permet de ne d�clencher l action que sur la touche Enter 
function ouvreKeypress(KeyPressEvenement, url) {
	if(KeyPressEvenement.keyCode==13) {
		ouvre(url);
		return false;
	}
	return true;
}

function ouvre(pageok,l,h) {
	if (!l) {
		l = 792;
	}
	if (!h) {
		h = 520;
	}
	if (!pageok) {
		pageok = this.href;
	}
	x=window.open(pageok,"zoom",'width='+parseInt(l)+',height='+parseInt(h)+',top=5,left=5,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=0,top=0');
	x.focus();
}

function ouvre_flash(pageok,l,h) {
	if (!pageok) {
		pageok = this.href;
	}
	if (!l) {
		l = 780;
	}
	if (!h) {
		h = 520;
	}
	/*50 = hauteur du bandeau des popups*/
	x=window.open(pageok+"&width="+l+"&height="+(parseInt(h)-50),"zoom",'width='+l+',height='+(h+7)+',top=5,left=5,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=yes,left=0,top=0');
	x.focus();
}

function ouvre_photo(pageok,titre,l,h) {
	x=window.open(pageok,"zoom",'width='+l+',height='+h+',top=5,left=5,toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,left=0,top=0');
	x.focus();
}


function ouvre_blank(pageok) {
	if (!pageok) {
		pageok = this.href;
	}
	x=window.open(pageok,"_blank",'width=800, height=600, top=0, left=0,toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes');
	x.focus();
}

// permet de ne d�clencher l action que sur la touche Enter 
function ouvre2Keypress(KeyPressEvenement, url) {
	if(KeyPressEvenement.keyCode==13) {
		ouvre2(url);
		return false;
	}
	return true;
}
function ouvre2(pageok, id) {
	if (!pageok) {
		pageok = this.href;
	}
	x=window.open(pageok, id,'width=583,height=450,toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,left=20,top=20');
	x.focus();
}

// permet de ne d�clencher l action que sur la touche Enter 
function ouvre_imageKeypress(KeyPressEvenement, image, titre, l, h) {
	if(KeyPressEvenement.keyCode==13) {
		ouvre(image,titre,l,h);
		return false;
	}
	return true;
}
function ouvre_image(img,titre,l,h) {

	// Ajustement de la marge pour IE 
	var margeHeight = 38;
	var margeWidth = 10;
	if (/Mozilla\/5\.0/.test(navigator.userAgent)) {
		margeHeight = 57;
		margeWidth = 9;
	}	
	var titre=titre;
	w=open("",'image','width='+l+',height='+h+',toolbar=no,scrollbars=auto,resizable=yes,directories=no,copyhistory=no,left=20,top=20');
	w.document.write("<HTML><HEAD><TITLE>"+titre+"</TITLE></HEAD>");
	w.document.write("<script language=javascript>function checksize() { if (document.images[0].complete) { window.resizeTo(document.images[0].width+"+margeWidth+",document.images[0].height+"+margeHeight+"); window.focus();} else { setTimeout('check()',250) } }</"+"SCRIPT>");
	w.document.write("<BODY onload='checksize()' leftMargin=0 topMargin=0 marginwidth=0 marginheight=0><IMG src='"+img+"' border=0>");
	w.document.write("");
	w.document.write("</BODY></HTML>");
	w.document.close();	
}

// permet de ne d�clencher l action que sur la touche Enter 
function cartoKeypress(KeyPressEvenement, url) {
	if(KeyPressEvenement.keyCode==13) {
		carto(url);
		return false;
	}
	return true;
}
function carto(url) {
	x=window.open(url,"carto","toolbar=no,location=no,status=no,directories=no,menubar=yes,scrollbars=no,resizable=no,copyhistory=no,width=550,height=500,left=50,top=50");
	x.focus();
}


function deroule(x, h, l) {
	if (typeof(x)=='string') {
		x = MM_findObj(x);
	}
	x.style.zIndex=4;
	x.style.height=h;
	x.style.visibility='visible';
	if (l) {
		x.style.width=l;   
	}
}



function enroule(x,h, l) {
	if (typeof(x)=='string') {
		x = MM_findObj(x);
	}
	x.style.zIndex=1;
	x.style.height=h;
	if (l) {
		x.style.width=l;   
	}
}

function roArrdt(a) {
	MM_findObj('vdlcarte').src='/static/vdl/images/a'+a+'.gif'
}

function reArrdt() {
	MM_findObj('vdlcarte').src='/static/vdl/images/carte.gif'
}

//fonction de controle en fonction des types de champs

var lastFocus = null;

function controle(formulaire,champ,format,mini,maxi) {	
var  ok = 0;
var  RE="";
// N : num�rique
if ( format == "N" ) { RE = /^\d+$/;}
// CP : code postal fran�ais (5 chiffres)
if ( format == "CPT" ) { RE = /^\d{5}$/;}
//TEL  : telephone et telecopie
if ( format == "TEL" ) { RE = /^\d{10}$/;}
// EMAIL : email
if ( format == "EMAIL" ) { RE = /^[A-Za-z0-9\.\-_]+[@][A-Za-z0-9\-\.]+[\.][A-Za-z][A-Za-z][A-Za-z]?$/;}
var controle = eval('document.' + formulaire + '.' + champ);
message = '';
if (controle.value.length > 0) {
if (!RE.test(controle.value))
{
message = message+'Votre saisie est incorrecte.\n';
ok = 1; }}
if ( mini != 0 ) {
if (controle.value.length < mini ) {
message = message+'Vous devez saisir au moins ' + mini + ' caracteres.\n';
ok = 1;}
if ( maxi != 0 )
{
if (controle.value.length > maxi )
{
message = message+'Vous ne devez pas saisir plus de ' + maxi + ' caracteres.\n';
ok = 1;}}
if ( ok == 1 ) {
alert(message);
lastFocus = controle;
}}}


function getFocus() {
	if (lastFocus) {
		lastFocus.focus();
		lastFocus.select();
		lastFocus = null;
	}
}

// permet de ne d�clencher l action que sur la touche Enter 
function AjoutFavorisKeypress(KeyPressEvenement, theURL, theTITLE) {
	if(KeyPressEvenement.keyCode==13) {
	  window.external.AddFavorite(theURL, theTITLE);
	}
}
function AjoutFavoris(theURL, theTITLE) {
  window.external.AddFavorite(theURL, theTITLE);
}


//Fonction spécifique Plan Interactif Ville
function ouvrir_mappy(liste_trouvee) {
  map_way = liste_trouvee.options[liste_trouvee.selectedIndex].value;
  iframe_rafraichir('plan_mappy', map_way);
}

function iframe_rafraichir(iframe_id, lien, rafraichir) {
  if (rafraichir != '') {
	parent.document.getElementById(iframe_id).src = lien;
  }
}

htmlareapopup = false;

function roll_over(x) {
  x.src=x.src.replace('off','over')
}

function roll_off(x) {
  x.src=x.src.replace('over','off')
}

// Pour menu
var to;
var currentMenu;
currentMenu="";
function montre(id) {
	clearTimeout(to);
	if (id=='') {
		tempo=500;
	}
	else {
		tempo=100;
	}
	to = setTimeout("immediateMontre(\""+id+"\")",tempo);
}

function findPosX(obj)
{
	var curleft = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	}
	else if (obj.x)
		curleft += obj.x;
	return curleft;
}

function immediateMontre(id) {
	if (id!=currentMenu) {
		var d = document.getElementById(id);
		if (currentMenu!="") {
			document.getElementById(currentMenu).style.display='none';
		}
		if (d) {
			d.style.display='block';
			divmenu = document.getElementById('menu');
			x = findPosX(divmenu)+divmenu.offsetWidth - (d.offsetLeft+d.offsetWidth);
			if (x<=0) {
				var m = document.getElementById(id+'_title');
				left = findPosX(m) + m.offsetWidth - d.offsetWidth +1;
				d.style.left=left+'px';
			}
		}
		currentMenu=id;
	}
}

// permet de ne d�clencher l action que sur la touche Enter 
function locationKeypress(KeyPressEvenement, urlToOpen) {
	if(KeyPressEvenement.keyCode==13) {
		location.href = urlToOpen;
		return false;
	}
	return true;
}
function closeKeypress(KeyPressEvenement) {
	if(KeyPressEvenement.keyCode==13) {
		window.close();
	}
}
function printKeypress(KeyPressEvenement) {
	if(KeyPressEvenement.keyCode==13) {
		window.print();
	}
}

/* fonction a ajouter sur action onclick 
   pour executer script python qui retourne
   valeur si go = 1
*/
function confirmAction(theLink, theMsg)
{
  var is_confirmed = confirm(theMsg);
  if (is_confirmed) {
	theLink.href += '&go=1';
  }
  return is_confirmed;
}

function ouvreCommentaires(pageok) {
	if (!pageok) {
		pageok = this.href;
	}
	x=window.open(pageok,"zoom",'width=455,height=520,top=5,left=5,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=0,top=0');
	x.focus();
}

function ouvreNewsletter(pageok) {
	if (!pageok) {
		pageok = this.href;
	}
	x=window.open(pageok,"zoom",'width=585,height=600,top=5,left=5,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=0,top=0');
	x.focus();
}

/* Affichage al�atoire des images */

function pickRandom(range) {
  var now = new Date();
  x = (now.getTime()/1000) % range;
  valeur = Math.round(x);
  return valeur;
}


function changeImage(base,rootNav,nb,default_image,default_pattern,default_base){
	var page = $('page');
	var fond1 = $('fond1');
  if (rootNav == "1") {	
	rnd = pickRandom(nb);
	if (rnd == 0) {
		rnd = rnd + 1;
	}
	if (rnd < 10) {
		rnd = '0'+rnd;
	}
	newImage  = "url("+base+rnd+".jpg)";
	newImage2 = "url("+base+rnd+"-pat.gif)";
	page.style.backgroundImage = newImage;
	fond1.style.backgroundImage = newImage2;
  }
	else if (rootNav == "2") {
		// Pages expo ...
		newImage  = "url("+default_base+default_image+")";
		newImage2 = "url("+default_base+default_pattern+")";
		page.style.backgroundImage = newImage;
		fond1.style.backgroundImage = newImage2;
	}
  else {
	return 1;
  }
}

function chargeSpecificImage(base,rootNav,image1,image2){
	  if (rootNav == "0") {
		var page = $('page');
		var fond1 = $('fond1');
		var menu = $('menu-mba');
		var fondMenu = $('fond-menu');
		if (image1) {
			newImage  = "url("+base+image1+")";
			page.style.backgroundImage = newImage;
			menu.style.backgroundImage = newImage;
		}
		if (image2) {
		newImage2 = "url("+base+image2+")";
		fond1.style.backgroundImage = newImage2;
		fondMenu.style.backgroundImage = newImage2;
	  }
	  
	}
	else {
		  return 1;
		}
}

function getWindowHeight() {
	var windowHeight=0;
	if (typeof(window.innerHeight)=='number') {
		windowHeight=window.innerHeight;
	}
	else {
		if (document.documentElement&&
		document.documentElement.clientHeight) {
			windowHeight=
			document.documentElement.clientHeight;
			}
		else {
			if (document.body&&document.body.clientHeight) {
				windowHeight=document.body.clientHeight;
			}
		}
	}
	return windowHeight;
}


function getCookieVal(offset){
  var endstr=document.cookie.indexOf (";", offset);
  if (endstr==-1) endstr=document.cookie.length;
  return unescape(document.cookie.substring(offset, endstr));
}

function LireCookie(nom){
  var arg=nom+"=";
  var alen=arg.length;
  var clen=document.cookie.length;
  var i=0;
  while (i<clen)
    {
      var j=i+alen;
      if (document.cookie.substring(i, j)==arg) return getCookieVal(j);
      i=document.cookie.indexOf(" ",i)+1;
      if (i==0) break;     
    }
  return null;
}



function EcrireCookie(nom, valeur){
	var argv=EcrireCookie.arguments;
	var argc=EcrireCookie.arguments.length;
	var expires=(argc > 2) ? argv[2] : null;
	var path=(argc > 3) ? argv[3] : null;
	var domain=(argc > 4) ? argv[4] : null;
	var secure=(argc > 5) ? argv[5] : false;
	document.cookie=nom+"="+escape(valeur)+
	((expires==null) ? "" : ("; expires="+expires.toGMTString()))+
	((path==null) ? "" : ("; path="+path))+
	((domain==null) ? "" : ("; domain="+domain))+
	((secure==true) ? "; secure" : "");
}
function DesableComment() {
  if ($('commentaireClose') && $('commentaire')) {
	Element.hide('commentaireClose');
	Element.hide('commentaire');
  }
  
  EcrireCookie('displayComment', 'non');
  var currentURL = unescape(window.location.pathname);
  window.location.href = currentURL;
}

function EnableComment() {
  if ($('commentaireClose') && $('commentaire')) {
	Element.show('commentaireClose');
  }
  
  EcrireCookie('displayComment', 'oui');
  var currentURL = unescape(window.location.pathname);
  window.location.href = currentURL;
}


function ajustOeuvre() {
	
	if($('ffNavOeuvre') && !(/MSIE/.test(navigator.userAgent))) {
		var pied = $('pied-mba');
		var posNavOeuvre = Position.cumulativeOffset($('ffNavOeuvre'));
		var navOeuvreTop = posNavOeuvre[1];
		var piedTop = 0;
		if (pied) {
			var posPied = Position.cumulativeOffset(pied);
  		piedTop = posPied[1];
  	}		
  	var navOeuvreHeight = piedTop-navOeuvreTop;
  	$('ffNavOeuvre').style.height = navOeuvreHeight+'px';
	}
	else {
		return 1;
	}
}


function chgCB(base){
  CHKB = document.getElementById('CPSFormulaire').getElementsByTagName('input');
  for(i=0; i < CHKB.length; i++){
    if(CHKB[i].type == "checkbox"){
      CHKB[i].className='form_checkbox_cache';
      IMG = document.createElement('img');
      CHKB[i].parentNode.insertBefore(IMG, CHKB[i]);
      IMG.src = base+"activites/checkbox_off.gif";
      if(CHKB[i].checked  == true) IMG.src = base+"activites/checkbox_on.gif";
      if(CHKB[i].disabled == true) IMG.src = base+"activites/cbperso2.gif";
      else{ // Les comportements de changement d'�tat ne sont appliqu�s que si la checkbox est cliquable !
        
        IMG.onchange = CHKB[i].onclick;
        
        IMG.ref_id = CHKB[i].id;
        
        
        //Pour mozilla
        IMG.onclick = function(){
          ref = document.getElementById(this.ref_id)
          ref.checked = !ref.checked;
          //ref.checked = !ref.checked;
          IMG = this.parentNode.getElementsByTagName('img')[0];
          if(ref.checked == true) IMG.src = base+'activites/checkbox_on.gif'
          else IMG.src = base+'activites/checkbox_off.gif'
        }
        CHKB[i].onchange = function(){
          this.checked = !this.checked;
          IMG = this.parentNode.getElementsByTagName('img')[0];
          if(this.checked == true) IMG.src = base+'activites/checkbox_on.gif'
          else IMG.src = base+'activites/checkbox_off.gif'
        }
        
        if(!window.sidebar){
          CHKB[i].parentNode.onclick = function(){
            CHK = this.getElementsByTagName('input')[0];
            CHK.checked = (CHK.checked == true) ? false : true;
            CHK.onchange();
          }
        }
      }
      CHKB[i].style.visibility = 'hidden';
      CHKB[i].style.position   = 'absolute';
    }
  }
}

function chgRB(base){

  CHKB = document.getElementById('CPSFormulaire').getElementsByTagName('input');
  for(i=0; i < CHKB.length; i++){
    if(CHKB[i].type == "radio"){
      IMG = document.createElement('img');
      CHKB[i].parentNode.insertBefore(IMG, CHKB[i]);
      IMG.src = base+'activites/checkbox_off.gif';
      IMG.ref_id = CHKB[i].id;
      IMG.id = 'img'+CHKB[i].id;
      IMG.relation = CHKB[i].name;
 
      if(CHKB[i].checked == true) IMG.src = base+'activites/checkbox_on.gif';
      if(CHKB[i].disabled == true) IMG.src = "cbperso2.gif";
      else{ // Les comportements de changement d'�tat ne sont appliqu�s que si la checkbox est cliquable !
        IMG.onclick = function(){
          ref = document.getElementById(this.ref_id) 
          IMG = document.getElementById('CPSFormulaire').getElementsByTagName('img');

          for(i=0; i < IMG.length; i++){
            if(IMG[i].relation != ref.name) continue;
            if(IMG[i].src.indexOf('cbperso2.gif') != -1) continue;
            if(IMG[i].id == 'img'+ref.id) 
              {
              	
                IMG[i].src = base+'activites/checkbox_on.gif';
                document.getElementById(this.ref_id).checked=true;
              }
            else 
              {              	              	
                IMG[i].src = base+'activites/checkbox_off.gif'
                document.getElementsByTagName(IMG[i].ref_id).checked=false;
              }
              
          }
 
        }
        
        //CHK[i].onchange=IMG.onclick
        if(!window.sidebar){
          CHKB[i].parentNode.onclick = function(){
            CHK = this.getElementsByTagName('input')[0];
            //CHK.checked = (CHK.checked == true) ? false : true;
            //CHK.onchange()
          }
        }
      }
      CHKB[i].style.visibility = "hidden";
      CHKB[i].style.position   = "absolute";
    }
  }
}

function selectReplacement(obj) {
  var autoSubmit = (obj.className.indexOf('autosubmit')>=0);
  submitButton = $('submitButton');
  if (submitButton && autoSubmit) {
	Element.hide(submitButton);
  }
  var select = document.createElement('div');
  select.className = 'select';
  var title = document.createElement('div');
  title.className = 'title';
  title.appendChild(document.createTextNode('Titre'));
  title.onclick = function() {
    Element.show(content);
  }
  select.appendChild(title);
  var content = document.createElement('div');
  content.className = 'content';
  content.onmouseout = function() {
    Element.hide(content);
  }
  select.appendChild(content);
  var ul = document.createElement('ul');
  content.appendChild(ul);
  ul.onmouseover = function() {
    Element.show(content);
  }
  drawMenu(obj, ul, title, content, autoSubmit);
  obj.parentNode.insertBefore(select,obj);
  Element.hide(obj);
}
function drawMenu(obj, ul, title, content, autoSubmit) {
  var opts = obj.options;
  var selectedValue = opts[obj.selectedIndex].value;
  ul.innerHTML = '';
  Element.hide(content);
  for (var i=0; i<opts.length; i++) {
    var li = document.createElement('li');
    li.id = 'l'+i;
    li.onclick = function(){
        selIndex = parseInt(this.id.substring(1));
        this.oldClassName = 'selected';
        obj.selectedIndex = selIndex;
        drawMenu(obj, ul, title, content, autoSubmit);
        if (autoSubmit) {
            obj.form.submit();
        }
        Element.hide(content);
    }
    li.onmouseover = function() {
        Element.show(content);
        this.oldClassName = this.className;
        this.className += ' selectover';
    }
    li.onmouseout = function() {
        this.className = this.oldClassName;
    }
    li.appendChild(document.createTextNode(opts[i].text));
    if (selectedValue == opts[i].value) {
        title.innerHTML = opts[i].text;
        li.className = 'selected';
    }
    ul.appendChild(li);
  }
}

function setForm() {
  var s = document.getElementsByClassName('nice');
  for (var i=0; i<s.length; i++) {
    selectReplacement(s[i]);
  }
  ajustOeuvre();
}
oldOnLoad=window.onload; 

function generic()
{
      (document.all && !window.print) ? null : setForm();
};

//setup onload function
if(typeof window.addEventListener != 'undefined')
{
	//.. gecko, safari, konqueror and standard
	window.addEventListener('load', generic, false);
}
else if(typeof document.addEventListener != 'undefined')
{
	//.. opera 7
	document.addEventListener('load', generic, false);
}
else if(typeof window.attachEvent != 'undefined')
{
	//.. win/ie
	window.attachEvent('onload', generic);
}

//** remove this condition to degrade older browsers
else
{
	//.. mac/ie5 and anything else that gets this far
	
	//if there's an existing onload function
	if(typeof window.onload == 'function')
	{
		//store it
		var existing = onload;
		
		//add new onload handler
		window.onload = function()
		{
			//call existing onload function
			existing();
			
			//call generic onload function
			generic();
		};
	}
	else
	{
		//setup onload function
		window.onload = generic;
	}
}       

function commentaireInit() {
	// surcharg�e par commentaire.js si besoin/contexte
	return 1;
}          