/*
 * runOnLoad.js: portable registration for onload event handlers.
 * 
 * This module defines a single runOnLoad() function for portably registering
 * functions that can be safely invoked only when the document is fully loaded
 * and the DOM is available.
 *
 * Functions registered with runOnLoad() will not be passed any arguments when
 * invoked. They will not be invoked as a method of any meaningful object, and
 * the this keyword should not be used.  Functions registered with runOnLoad()
 * will be invoked in the order in which they were registered.  There is no
 * way to deregister a function once it has been passed to runOnLoad().
 *
 * In old browsers that do not support addEventListener() or attachEvent(),
 * this function relies on the DOM Level 0 window.onload property and will not
 * work correctly when used in documents that set the onload attribute
 * of their <body> or <frameset> tags.
 */
function runOnLoad(f) {
    if (runOnLoad.loaded) f();    // If already loaded, just invoke f() now.
    else runOnLoad.funcs.push(f); // Otherwise, store it for later
}

runOnLoad.funcs = []; // The array of functions to call when the document loads
runOnLoad.loaded = false; // The functions have not been run yet.

// Run all registered functions in the order in which they were registered.
// It is safe to call runOnLoad.run() more than once: invocations after the
// first do nothing. It is safe for an initialization function to call
// runOnLoad() to register another function.
runOnLoad.run = function() {
    if (runOnLoad.loaded) return;  // If we've already run, do nothing

    for(var i = 0; i < runOnLoad.funcs.length; i++) {
        try { runOnLoad.funcs[i](); }
        catch(e) { /* An exception in one function shouldn't stop the rest */ }
    }
    
    runOnLoad.loaded = true; // Remember that we've already run once.
    delete runOnLoad.funcs;  // But don't remember the functions themselves.
    delete runOnLoad.run;    // And forget about this function too!
};

// Register runOnLoad.run() as the onload event handler for the window
if (window.addEventListener)
    window.addEventListener("load", runOnLoad.run, false);
else if (window.attachEvent) window.attachEvent("onload", runOnLoad.run);
else window.onload = runOnLoad.run;

function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

runOnLoad(function(){
    try {
        if(readCookie('ROULARTA_CODE_40') == null) {
            createCookie('ROULARTA_CODE_40',readCookie('userProfile_R_NEWS').substr(0,readCookie('userProfile_R_NEWS').lastIndexOf(':')),365);
        }
    }catch(err){}
    
	$('#kanaalzPrevious').click(function() {
		var currentpage = $(document.getElementById('currentpage')).html();
		if (currentpage != undefined) {
			var previouspage = parseInt(currentpage) - 1;
			var nextpage = parseInt(currentpage) + 1;
			
			var currentDiv = 'page' + currentpage;
			var previousDiv = 'page' + previouspage;
			var nextDiv = 'page' + nextpage;
			if(currentpage != 1) {
				$(document.getElementById(currentDiv)).css("display", "none");
				$(document.getElementById(previousDiv)).css("display", "block");
				
				$(document.getElementById('currentpage')).html(previouspage);
			}
		}
		return false;
	});
	
	$('#kanaalzNext').click(function() {
		var currentpage = $(document.getElementById('currentpage')).html();
		if (currentpage != undefined) {
			var previouspage = parseInt(currentpage) - 1;
			var nextpage = parseInt(currentpage) + 1;
			
			var currentDiv = 'page' + currentpage;
			var previousDiv = 'page' + previouspage;
			var nextDiv = 'page' + nextpage;
			
			var lastpage = $(document.getElementById('lastpage')).html();
			
			if(currentpage != lastpage){
				
				$(document.getElementById(currentDiv)).css("display", "none");
				$(document.getElementById(nextDiv)).css("display", "block");
				
				$(document.getElementById('currentpage')).html(nextpage);
			}
		}
		return false;

	});


	if($("#adv").height() < 20){	   
	   $("#adv").slideUp("slow");
	}
	//if($("div.beweb").height() < 20){
	//   $("div.beweb").slideUp("slow");
	//}	
	$("div.beweb").each(function () {
    	boxheight = $(this).next().height();
		if($(this).height() < 20){
	   		$(this).slideUp("slow");
		}
	});	
	if($("div.adbutton").height() < 20){
	   $("div.adbutton").slideUp("slow");
	}	
	var hSmall = $("div.small").height();
	var hSmallLast = $("div.smallLast").height();
	
	if($("div.small").height() < $("div.smallLast").height()){
		$("div.small").height(hSmallLast+"px");
	}
	else {
		$("div.smallLast").height(hSmall+"px");
	}
    
    var slideArrow = $("#go .slideButton img");
	
	$("#go .slideButton").click(function () {
      if($("#go .slideButton img").hasClass("closed")){
			$(slideArrow).removeClass("closed");
			$(slideArrow).attr("src","/images/slidebutton_arrow_down.gif");
	  }
	  else {
		 	$(slideArrow).addClass("closed");
			$(slideArrow).attr("src","/images/slidebutton_arrow_up.gif");
	  }
	  $("#go #slidePanel").slideToggle("slow");
    });
	
	var goTabs = new Yetii({
		id: 'goTabs'
		//active: 2
	});
});

function displayPrint() { 
	win = window.open("/print/index.jsp", 'popup'); 
	win.opener = self;
}

function gotoAnchorTopPage() {
	document.location.hash="topPage";
}	

function addRssFeed(name, url) {
    var rssFeed = document.createElement("link");
    rssFeed.setAttribute("rel", "alternate");
    rssFeed.setAttribute("title", name);
    rssFeed.setAttribute("href", url);
    rssFeed.setAttribute("type", "application/rss+xml");
    document.getElementsByTagName("head")[0].appendChild(rssFeed);
}

var today = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
var year = today.getFullYear();
var month = (today.getMonth() + 1) + "";
var day = today.getDate() + "";
if(month.length < 2) {
	month = "0" + month;
}
if(day.length < 2) {
	day = "0" + day;
}

/*function popUpVideo(URL) {
	day = new Date();
    id = day.getTime();
    eval( "page" + id + " = window.open(URL, \'" + id + "\', \'resizable=yes,scrollbars=yes,width=600,height=470,left = 50,top = 50\');");
}*/

function popUpVideo(url) {
    window.open(url, "_blank", "resizable = yes, scrollbars = yes, width = 854, height = 540, left = 50, top = 50");
}
/** Zet de titel met al dan niet voorgaand update.
*@title  titel van het artikel
*@datum  datum van de laatste wijziging
*@link    link naar het artikel
*/
function zetTeasertitel(titel, datum, link){
	
	/*
	datum is van de vorm dd-mm-yyyy hh:mm
	*/

	var jaar = datum.substring(6,10);
	var maand = datum.substring(3,5)-1;
	var dag = datum.substring(0,2);
	var uren = datum.substring(11,13);
	var minuten = datum.substring(14);
	var seconden = 0;

	if(uren == 24)
		uren = 0;


    	var updateDate = new Date(jaar,maand,dag,uren,minuten,seconden);
	var currentDate = new Date();	

	
	
	if ((currentDate - updateDate) < 86400000){
			document.write('<span class="update">update</span><a href="' + link + '">' + titel + '</a>');
	}
	else {
			document.write('<a href="' + link + '">' + titel + '</a>');
	}
    	
}

var ForumMessageNotFound='';


/*fontsizer logic*/
$(function() {
    $("#fontSizer a").click(function(){
        if (this.id == 'normalFont'){
            $('#articleContainer p').addClass("normalFont").removeClass("largerFont").removeClass("largestFont");
        }else if (this.id == 'largerFont'){
            $('#articleContainer p').removeClass("normalFont").addClass("largerFont").removeClass("largestFont");
        }else if (this.id == 'largestFont'){
            $('#articleContainer p').removeClass("normalFont").removeClass("largerFont").addClass("largestFont");
        }
    });
});