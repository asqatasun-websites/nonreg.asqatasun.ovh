function nextPartenaire (){
	HideAllPartenaires();

	var divPartenaire;
	var idNextPartenaire;
	var intCurrentPartenaire = 0;
	var intMaxPartenaire = 0;
	var objCurrentPartenaire;
	
	intMaxPartenaire = document.getElementById('max_partenaire').value;
	objCurrentPartenaire = document.getElementById('current_partenaire')
	intCurrentPartenaire = objCurrentPartenaire.value;
	
	intCurrentPartenaire++;
	if (intCurrentPartenaire < intMaxPartenaire){
		objCurrentPartenaire.value = intCurrentPartenaire;
		idNextPartenaire = 'partenaire' + intCurrentPartenaire;
		divPartenaire = document.getElementById(idNextPartenaire);
	}
	else{
		objCurrentPartenaire.value = 0;
		idNextPartenaire = 'partenaire' + '0';
		divPartenaire = document.getElementById(idNextPartenaire);
	}
	
	divPartenaire.style.display = 'block';
}

function prevPartenaire (){
	HideAllPartenaires();

	var divPartenaire;
	var idPrevPartenaire;
	var intCurrentPartenaire = 0;
	var intMaxPartenaire = 0;
	var objCurrentPartenaire;
	
	intMaxPartenaire = document.getElementById('max_partenaire').value;
	objCurrentPartenaire = document.getElementById('current_partenaire')
	intCurrentPartenaire = objCurrentPartenaire.value;
	
	intCurrentPartenaire--;
	if (intCurrentPartenaire >= 0){
		objCurrentPartenaire.value = intCurrentPartenaire;
		idPrevPartenaire = 'partenaire' + intCurrentPartenaire;
		divPartenaire = document.getElementById(idPrevPartenaire);
	}
	else{
		intCurrentPartenaire = intMaxPartenaire - 1;
		objCurrentPartenaire.value = intCurrentPartenaire;
		idPrevPartenaire = 'partenaire' + intCurrentPartenaire;
		divPartenaire = document.getElementById(idPrevPartenaire);
	}
	
	divPartenaire.style.display = 'block';
}

function HideAllPartenaires(){
	var tabAllPartenaires;
	tabAllPartenaires = document.getElementsByTagName('div');
	maxElement = tabAllPartenaires.length;
	for(i=0;i<maxElement;i++){
		if (tabAllPartenaires[i].className == 'LePartenaire'){
			tabAllPartenaires[i].style.display = 'none';
		}
	}
}