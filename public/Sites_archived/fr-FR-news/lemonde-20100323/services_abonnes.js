
if (undefined===window.MIA)
{
   var MIA = {};
}

MIA.Class_ServicesAbonnes = MiaClass.inherits(
{
   construct : function()
   {
      // Array d info de l utilisateur (prealablement lus dans le cookie info_user_web)
      this._infoInUserCookie = null;
      
      this._majAjaxEnclenchee = false;
      
      // liste des callback (qui sont appell�es apres la maj AJAX des cookies)'
      this._callbackSuccesFonctionsMaj = new Array();
   },
   
   /*
    * Test existence du cookie perso (abonne ou simple authentifie)
    * @acces : public
    */
   testerExistenceCookiePerso : function()
   {
      var val_cook_temp = GetCookie('info_user_web');
      var val_cook_id   = GetCookie('tdb_user_id');
      var dtstamp = new Date();
      var tstamp = dtstamp.getTime();

      if (((val_cook_temp != null) && (val_cook_temp != "")) && ((val_cook_id != null) && (val_cook_id != ""))) 
      {
         try
         {  
            $evaluation_cookie = eval("(" + val_cook_temp + ")");
         }
         catch (e)
         {
            //si on a un ancien cookie le test repond false, on considere alors que le cookie n existe pas
            return false;
         }
         if (typeof $evaluation_cookie['info_usr'] != undefined)
            return true;
      } 
      return false;
   },
   
   /*
    * Getter pour le contenu du cookie info_user_web
    */
   getInfoInUserCookie : function ()
   {
      if (this._infoInUserCookie == null)
      {
         this.chargerInfoDeUserCookie();
      }
      return this._infoInUserCookie;
   },
   
   /*
    * Recuperation du cookie d info perso
    * @acces : public
    */ 
   chargerInfoDeUserCookie : function()
   {
      var val_cook_temp = GetCookie('info_user_web');
      this._infoInUserCookie = eval("(" + val_cook_temp + ")");
   },
   
   /*
    * Callback en cas de reussite de la mise a jour des cookie par PHP
    * @acces : private
    */
   _callbackSuccesMajServices : function (html)
   {
      var ct = 0;
      this._majAjaxEnclenchee = false;  
      //mis � jour des differentes interface HTML
      for (ct = 0; ct < this._callbackSuccesFonctionsMaj.length; ct++)
      {
         this._callbackSuccesFonctionsMaj[ct]();
      
      }
      //this.mettreAjourOreillePersoGold();
      //this.activerBlocAbonnePremium();
   },
   
   /*
    * Ajout d une fonction pour les mises � jour de l interface apres maj du cookie
    * [ cf : _callbackSuccesMajServices() ]
    * @acces : private
    */
   ajouterFonctionMaj : function(func)
   {
      this._callbackSuccesFonctionsMaj.push(func);
   },
   
   /*
    * Callback en cas d echec de la mise a jour des cookie par PHP
    * @acces : private
    */
   _callbackEchecMajServices : function (XMLHttpRequest, textStatus, errorThrown)
   {
      //{ alert( "maj service Abonne Error: " + errorThrown + ' ' + textStatus  + '.' ); }
   },
   
   /*
    * Appel Ajah d une page php mettant a jour les cookies abonnes
    * @acces : public
    */
   mettreAJourServices : function()
   {
      if (! this.testerExistenceCookiePerso()) 
      {
         var user_id = GetCookie('tdb_user_id');
         var closure = this;
         if ((user_id != null) && (user_id != "")) 
         {
            // appel ajax
            //on informe qu une maj est enclanchee
            this._majAjaxEnclenchee = true;            
            var url_maj_service = "http://www.lemonde.fr/services/maj_services.html";
 
            $.ajax({
                     url: url_maj_service,
                     cache: false,
                     success: 
                        function(html) {closure._callbackSuccesMajServices(html);},
                     error:
                        function(XMLHttpRequest, textStatus, errorThrown)
                           {closure._callbackEchecMajServices(XMLHttpRequest, textStatus, errorThrown);} });
                           
         }
      }
   },
   testerMiseAJourAjaxEnclenchee : function(str)
   {
      return this._majAjaxEnclenchee;
   },
   testerExistenceInfo : function (str)
   {
      // on force la lecture.
      this.getInfoInUserCookie();
      
      
      return(  typeof this._infoInUserCookie != 'undefined' && 
               (this._infoInUserCookie != null) && 
               typeof this._infoInUserCookie[str] != 'undefined');
   
   }
  
});

// Appel de la mise � jour des services
MIA.ServicesAbonnes = new MIA.Class_ServicesAbonnes();
MIA.ServicesAbonnes.mettreAJourServices();
