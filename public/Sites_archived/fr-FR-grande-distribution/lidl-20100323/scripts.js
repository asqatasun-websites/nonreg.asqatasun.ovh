﻿
/*<rde-dm:attribute mode="write" attribute="request:rdeResponseMimetype" op="set" value="text/javascript"/>*/
// JavaScript Document
// Ein/Ausblenden von DIV Containern
function showHide(id) {
    obj = document.getElementsByTagName("div");
    if (obj[id].style.display == 'block'){
    obj[id].style.display = 'none';
    }
    else {
    obj[id].style.display = 'block';
    }
}
/* Tabanvigation Detailseiten */
function showTab1h() {
    document.getElementById("contenttabh").style.display = 'block';
    document.getElementById("contenttab2h").style.display = 'none';
    document.getElementById("menu12").style.background = 'url(/static_content/lidl_fr/images/contenttabs.png) no-repeat scroll right top';
    document.getElementById("menu11").style.background = 'url(/static_content/lidl_fr/images/contenttabs.png) no-repeat scroll left top';
    document.getElementById("menu22").style.background = 'url(/static_content/lidl_fr/images/contenttabs.png) no-repeat scroll right bottom';
    document.getElementById("menu21").style.background = 'url(/static_content/lidl_fr/images/contenttabs.png) no-repeat scroll left bottom';
}
function showTab2h() {
    document.getElementById("contenttabh").style.display = 'none';
    document.getElementById("contenttab2h").style.display = 'block';
    document.getElementById("menu22").style.background = 'url(/static_content/lidl_fr/images/contenttabs.png) no-repeat scroll right top';
    document.getElementById("menu21").style.background = 'url(/static_content/lidl_fr/images/contenttabs.png) no-repeat scroll left top';
    document.getElementById("menu12").style.background = 'url(/static_content/lidl_fr/images/contenttabs.png) no-repeat scroll right bottom';
    document.getElementById("menu11").style.background = 'url(/static_content/lidl_fr/images/contenttabs.png) no-repeat scroll left bottom';
}
function showTab1v() {
    document.getElementById("contenttabv").style.display = 'block';
    document.getElementById("contenttab2v").style.display = 'none';
    document.getElementById("menu12").style.background = 'url(/static_content/lidl_fr/images/contenttabs.png) no-repeat scroll right top';
    document.getElementById("menu11").style.background = 'url(/static_content/lidl_fr/images/contenttabs.png) no-repeat scroll left top';
    document.getElementById("menu22").style.background = 'url(/static_content/lidl_fr/images/contenttabs.png) no-repeat scroll right bottom';
    document.getElementById("menu21").style.background = 'url(/static_content/lidl_fr/images/contenttabs.png) no-repeat scroll left bottom';
}
function showTab2v() {
    document.getElementById("contenttabv").style.display = 'none';
    document.getElementById("contenttab2v").style.display = 'block';
    document.getElementById("menu22").style.background = 'url(/static_content/lidl_fr/images/contenttabs.png) no-repeat scroll right top';
    document.getElementById("menu21").style.background = 'url(/static_content/lidl_fr/images/contenttabs.png) no-repeat scroll left top';
    document.getElementById("menu12").style.background = 'url(/static_content/lidl_fr/images/contenttabs.png) no-repeat scroll right bottom';
    document.getElementById("menu11").style.background = 'url(/static_content/lidl_fr/images/contenttabs.png) no-repeat scroll left bottom';
}
/* Variantenbilder anzeigen  */
function loadImage(UrlFrom){
    document.getElementById('loadpic').setAttribute("src",UrlFrom);
}
function PopUp(w,h,ziel) {
  h = h - 20; var x=0, y=0, parameter="";
  if (w < screen.availWidth || h < screen.availHeight) {
    x = (screen.availWidth - w - 12) / 2;
    y = (screen.availHeight - h - 40) / 2; //104
    if (window.opera) y = 60; // Opera positioniert unter den Symbolleisten 0
    if (x<0 || y<0) { x=0; y=0; }
    else parameter = "width=" + w + ",height=" + h + ",";
  }
  parameter += "left=" + x + ",top=" + y;
  parameter += ",menubar=no,location=no,toolbar=no,status=no";
  parameter += ",resizable=yes,scrollbars=yes";
  var Fenster = window.open(ziel,"PopUp",parameter);
  if (Fenster) {
      Fenster.focus();      
  }
  return !Fenster;
}
function onoff (id) {
    
       document.getElementById(id).style.visibility = "hidden";              
}
/*neue Druckfunktion */
 var printContent = function(id){ 
/*alert('printContent'); */
var win = window.open('','','width=612,scrollbars=yes');
win.document.open();
var obj = document.getElementById(id);
if(!obj)
var obj = document.getElementById('printer1');
var header = document.getElementsByTagName('head')[0];
do{
try{
var script = header.getElementsByTagName('script');
var len = script.length;
header.removeChild(script[0]);
}
catch(e){len=0;}
}
while(len > 1);
do{
try{
var script = obj.getElementsByTagName('script');
var len= script.length;
obj.removeChild(script[0]);
}catch(e){len = 0;}
}
while(len > 1);
var content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict/EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
content+='<html>';
content+='<head><!-- PageID 3961 - published by RedDot 7.5 - 7.5.2.16 - 26723 -->';
content+=header.innerHTML;
content+='<style type="text/css">.box1x1{ border: 1px solid #738eb5;}#box_pos{margin-top:0px;margin-bottom:24px;}#print_pos{display:none;}#ueberschriftbalken{background-color:#fff;}.headline{color:#738eb5;} </style>';
content+='</head>';
content+='<body onload="self.print();"><div style="margin-left:8px;">';
content+= obj.innerHTML.replace(/class=\"shop\"/g,'class="shopprint"').replace(/class=shop/g,'class="shopprint"').replace(/datum2/g,'datum2 datum2_print').replace(/nachlass/g,'nachlass nachlass_print').replace(/=nachlass/g,'="nachlass').replace(/=datum2/g,'="datum2').replace(/print>/g,'print">').replace(/nachlass_print\">/g,'nachlass_print"><div></div>').replace(/box1x1/g, 'box1x1 box1x1_print').replace(/=box1x1 box1x1_print/g, '="box1x1 box1x1_print"');
content+='</div></body>';
content+='</html>';
win.document.write(content);
win.document.close();
}
function GetPrintContent()
                  {
                        var PrintDiv =  document.getElementById('printcontent');
                        var ContentDiv =  window.opener.document.getElementById('popup');
                        PrintDiv.innerHTML = ContentDiv.innerHTML;
                  }
function PrintThisPage() 
{ 
   var sOption="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
         sOption+="scrollbars=yes,width=750,height=600,left=100,top=25"; 
   
   var winprint=window.open("print.html","Print",sOption); 
 
    winprint.focus(); 
}
var BrowserDetect = {
    init: function () {
        this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
        this.version = this.searchVersion(navigator.userAgent)
            || this.searchVersion(navigator.appVersion)
            || "an unknown version";
        
        
    },
    searchString: function (data) {
        for (var i=0;i<data.length;i++)    {
            var dataString = data[i].string;
            var dataProp = data[i].prop;
            this.versionSearchString = data[i].versionSearch || data[i].identity;
            if (dataString) {
                if (dataString.indexOf(data[i].subString) != -1)                
                    return data[i].identity;
            }
            else if (dataProp)
                return data[i].identity;
        }
    
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index == -1) return;
        return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
    },
    dataBrowser: [
        {
            string: navigator.userAgent,
            subString: "Chrome",
            identity: "Chrome"
        },
        {     string: navigator.userAgent,
            subString: "OmniWeb",
            versionSearch: "OmniWeb/",
            identity: "OmniWeb"
        },
        {
            string: navigator.vendor,
            subString: "Apple",
            identity: "Safari",
            versionSearch: "Version"
        },
        {
            prop: window.opera,
            identity: "Opera"
        },
        {
            string: navigator.vendor,
            subString: "iCab",
            identity: "iCab"
        },
        {
            string: navigator.vendor,
            subString: "KDE",
            identity: "Konqueror"
        },
        {
            string: navigator.userAgent,
            subString: "Firefox",
            identity: "Firefox"
        },
        {
            string: navigator.vendor,
            subString: "Camino",
            identity: "Camino"
        },
        {        // for newer Netscapes (6+)
            string: navigator.userAgent,
            subString: "Netscape",
            identity: "Netscape"
        },
        {
            string: navigator.userAgent,
            subString: "MSIE",
            identity: "Explorer",
            versionSearch: "MSIE"
        },
        {
            string: navigator.userAgent,
            subString: "Gecko",
            identity: "Mozilla",
            versionSearch: "rv"
        },
        {         // for older Netscapes (4-)
            string: navigator.userAgent,
            subString: "Mozilla",
            identity: "Netscape",
            versionSearch: "Mozilla"
        }
    ]
  
};
BrowserDetect.init();
function fenster_anpassen() { 
    if ( document.getElementById)
     { 
        
                    
                    var bildhei =  document.getElementById("bild").offsetHeight + 100;
                var heitext =  document.getElementById("text").offsetHeight + 100;
                
                     var bild =  document.getElementById("bild").offsetWidth + 16;
              var brei2 = 360;
                var brei3 = bild + brei2;
                
                var breite_logo = 64;
                var breite_balken =  document.getElementById("ueberschriftbalken_sms").offsetWidth + 32;
                var breite_header = breite_balken +breite_logo;
                
                
                if(breite_header > brei3)
                {
                    brei4 = breite_header;
                }
                else
                {
                    brei4 = brei3;
                }
                
                  if(heitext > bildhei)
                  {
                       hei3 = heitext;
                  }
                  else
                  {
                      hei3 = bildhei;
                  }
                  
          }
            var Versioni = BrowserDetect.version;
            var Browseri = BrowserDetect.browser;
                var opera = Browseri.search(/Opera/);
                var mozilla = Browseri.search(/Firefox/);
                    var safari = Browseri.search(/Safari/);
                var explorer = Browseri.search(/Explorer/);
                if(opera != -1 )
                {
                    hei3 = hei3 +64;
                }
                  if( mozilla != -1 )
                  {
                  
                      hei3 = hei3 + 102;
                  }
                  if(safari != -1) {
                      hei3 = hei3 + 57;
                  }
     if( explorer != -1)
     {
         if(Versioni == 6){
     hei3 = hei3 + 72;
    }
    if(Versioni == 7){    
        hei3 = hei3 + 56;
    }
    }
      
        window.resizeTo(brei4,hei3);
      
      window.focus();
    }
function fenster_estate() {
    if (document.getElementById) {
        var hoehe = 723;
        var breite =  608;
        var Versioni = BrowserDetect.version;
        var Browseri = BrowserDetect.browser;
        var opera = Browseri.search(/Opera/);
        var mozilla = Browseri.search(/Firefox/);
        var safari = Browseri.search(/Safari/);
        var explorer = Browseri.search(/Explorer/);
                    //alert(Versioni);
                    
        if(explorer != -1) {
            if(Versioni == 6){
              hoehe = hoehe - 46;
              breite = breite - 8;
             }
             if(Versioni == 7){
                hoehe = hoehe + 25;
                breite = breite + 4;
             }
         }
            
         if(opera != -1 )
         {
            hoehe = hoehe - 38;
            breite = breite - 15;
         }
         if( mozilla != -1)
         {
             breite = breite ;
             hoehe = hoehe;
          }
          if(safari != -1 )
          {
             hoehe = hoehe - 45;
             breite = breite - 20;
          }
        window.resizeTo(breite, hoehe);
        window.focus();
    }
} 
    
function fenster_filial_anpassen() {
    
            var hoehe1 =  document.getElementById("popup_filial").offsetHeight + 100;
            
          var hoehe2 = document.getElementById("filialen").offsetHeight + 60;
          var hoehe = hoehe1 + hoehe2;     
                 var breite =  624;
            
            var Versioni = BrowserDetect.version;
            var Browseri = BrowserDetect.browser;
                var opera = Browseri.search(/Opera/);
                var mozilla = Browseri.search(/Firefox/);
                var safari = Browseri.search(/Safari/);
                var explorer = Browseri.search(/Explorer/);
                    //alert(Versioni);
                    
                if(explorer != -1) {
                        if(Versioni == 6){
                            hoehe = hoehe + 16;
                            breite = breite - 8;
                        }
                    if(Versioni == 7){
                            hoehe = hoehe - 4;
                            breite = breite - 8;
                        }
                }
            
                if(opera != -1 )
                {
                    hoehe = hoehe + 8;
                    breite = breite - 12;
                }
                  if( mozilla != -1)
                  {
                      breite = breite - 12;
                      hoehe = hoehe + 44;
                  }
             if(safari != -1 )
                {
                    hoehe = hoehe + 1 ;
                    breite = breite - 16;
                }
      
        window.resizeTo(breite,hoehe);
             window.focus();
     
    
    
}
function fenster_sms_anpassen() {
    if (document.getElementById) {
        var hoehe = document.getElementById("endOfPAge").offsetTop + 40;
        var breite =  624;
        var Versioni = BrowserDetect.version;
        var Browseri = BrowserDetect.browser;
        var opera = Browseri.search(/Opera/);
        var mozilla = Browseri.search(/Firefox/);
        var safari = Browseri.search(/Safari/);
        var explorer = Browseri.search(/Explorer/);
                    //alert(Versioni);
                    
        if(explorer != -1) {
            if(Versioni == 6){
              hoehe = hoehe + 11;
              breite = breite - 12;
             }
             if(Versioni == 7){
                hoehe = hoehe + 33;
                breite = breite - 12;
             }
         }
            
         if(opera != -1 )
         {
            hoehe = hoehe + 2;
            breite = breite - 15;
         }
         if( mozilla != -1)
         {
             breite = breite - 16;
             hoehe = hoehe + 41;
          }
          if(safari != -1 )
          {
             hoehe = hoehe - 14;
             breite = breite - 16;
          }
        window.resizeTo(breite, hoehe);
        window.focus();
    }
}function validate_form(error)
{
    var strng = '';
    var stripped = '';
    if (document.frmMailSolution.email) 
    {
       strng = document.frmMailSolution.email.value;
       stripped = strng.replace(/[\(\)\.\-\/\ ]/g, '');  
    }
    if (document.frmMailSolution.EMMAIL) 
    {
       strng = document.frmMailSolution.EMMAIL.value;
       stripped = strng.replace(/[\(\)\.\-\/\ ]/g, '');  
    }    //alert(stripped);
    if (isNaN(parseInt(stripped))) 
    {
        //error = "Ihre Handynummer scheint nicht korrekt zu sein.";
        document.getElementById('error').innerHTML = '<span style="padding-top:10px">' + error + '</span>';
        return false; 
    } else 
    {
        return true;
    }
    /* if (!(stripped.length > 10)) 
    {
        error = "Ihre Handynummer scheint nicht korrekt zu sein.";
        alert(error);
        return error;
    } */
}

// Überprüfung für die RegionMap 
function regionSearch(type)
{
     if (type==2)
    {
            if(typeof(proof) == "undefined" || proof != 1)
            {
               var url = document.forms.region_selection_form.url.value+'&ERR=noservice';
            }
            else
            {
                var url = document.forms.region_selection_form.url.value;
            }            
            document.forms.region_selection_form.action = url;
            document.forms.region_selection_form.submit();
    }   
}
// Überprüfung für die Filialsuche
function zipcodeSearch(type)
{
    if(typeof(proof) == "undefined" || proof != 1)
    {
        type=4;
    }
    if (type==1)
    {
            document.getElementById('zipcode_fehler').style.visibility = 'hidden';
            document.getElementById('zipcode_fehler').style.display = 'none';
            document.forms.plzsuche.submit();
    }
    if (type==2)
    {
            document.getElementById('zipcode_fehler').style.visibility = 'hidden';
            document.getElementById('zipcode_fehler').style.display = 'none';
            var url = document.forms.plzsuche.url.value;           
            document.forms.plzsuche.action = url;
            document.forms.plzsuche.submit();
    }
    if (type==3)
    {
            document.getElementById('zipcode_fehler').style.visibility = 'hidden';
            document.getElementById('zipcode_fehler').style.display = 'none';
            var url = document.forms.plzsuche.url.value;
            var sp = '';
            if (document.forms.plzsuche.zipcodesearch) sp = document.forms.plzsuche.zipcodesearch.value;
            var so = '';
            if (document.forms.plzsuche.loc) so = document.forms.plzsuche.loc.value;
            var sto = '';
            if (document.forms.plzsuche.district) sto = document.forms.plzsuche.district.value;
            var attribute = 'status=0,toolbar=1,menubar=1,scrollbars=1,resizable=0,width=622,height=520,top=10,left=10,screenX=10,screenY=10';
            var windowNew = window.open(url + '?SO=' + so + '&STO=' + sto + '&SP=' + sp + '&SEARCH=TRUE', 'Storefinder', attribute);
            windowNew.focus();
     }
     if (type==4)
     {
            var url = document.forms.plzsuche.url.value+'&ERR=noservice';
            document.forms.plzsuche.action = url;
            document.forms.plzsuche.submit();
    }

   
}
// Filialsuche über Teaser
function teaserZipcodeSearch()
{
    if(typeof(proof) == "undefined" || proof != 1)
    {
        
        if (document.forms.plzsuche)
        {
            var url = document.forms.plzsuche.url.value+'&ERR=noservice';
            document.forms.plzsuche.action = url;
            document.forms.plzsuche.submit();
        }
        else 
        { 
                document.getElementById('zipcode_teaser_nostore').style.visibility = 'hidden';
                document.getElementById('zipcode_teaser_nostore').style.display = 'none';
                document.getElementById('zipcode_fehler_teaser').style.visibility = 'visible';
                document.getElementById('zipcode_fehler_teaser').style.display = 'block';
        }
    }
    else
    {
        document.getElementById('zipcode_fehler_teaser').style.visibility = 'hidden';
        document.getElementById('zipcode_fehler_teaser').style.display = 'none';
        var urlBack = document.forms.changestore.urlBack.value;
        var url = document.forms.changestore.url.value;
        var variablesback = document.forms.changestore.variablesback.value;  
        var attribute = 'status=0,toolbar=1,menubar=1,scrollbars=1,resizable=0,width=622,height=520,top=10,left=10,screenX=10,screenY=10';
        if(document.forms.changestore.lng)
        {
            var lng = document.forms.changestore.lng.value;
            var windowNew = window.open(url + '?lng=' + lng + '&variablesback=' + variablesback + '&url=' + urlBack, 'Storefinder', attribute);
        }else if (document.forms.changestore.sc)
        {
            var lng = document.forms.changestore.sc.value;
            var windowNew = window.open(url + '?sc=' + lng + '&variablesback=' + variablesback + '&url=' + urlBack, 'Storefinder', attribute);
        }else 
        {
            var windowNew = window.open(url + '?variablesback=' + variablesback + '&url=' + urlBack, 'Storefinder', attribute);           
        }
        windowNew.focus();
    }
}
// Update der Selectbox Divisions
function updateDivisions()
{
    document.stellensuche.div.options.length = 1;
    var value = document.stellensuche.field.value;
    var divisions = getDivisions(value);
    for(var i=0; i < divisions.length; i++)
    {
        var option = document.createElement("OPTION");
        option.text = divisions[i]["name"];
        option.value = divisions[i]["id"];
        document.stellensuche.div.options.add(option);
    }
}
// Selektierung der ausgewählen Optionen der Selectboxes für Jobs
function initiateJobSelections(field, div, term)
{
    for(var i=0;i<document.stellensuche.field.options.length;i++)
    {
        var option = document.stellensuche.field.options[i]
        var valueTemp = option.value.split("||");
        value = valueTemp[0];
        if(value == field)
        {
            option.selected = "selected";
        }
    }
    updateDivisions();
    for(var i=0;i<document.stellensuche.div.options.length;i++)
    {
        var option = document.stellensuche.div.options[i]
        var value = option.value;
        if(value == div)
        {
            option.selected = "selected";
        }
    }
    for(var i=0;i<document.stellensuche.term.options.length;i++)
    {
        var option = document.stellensuche.term.options[i]
        var value = option.value;
        if(value == term)
        {
            option.selected = "selected";
        }
    }
}
// Shopping List Funktionen
// type 0 = add, type 1 = remove
function editBasket(type, pageId)
{
    var form = document.createElement("form");
    document.body.appendChild(form);
    form.method = "get";
    form.name = "shoppingList"; 
    form.action = "";
    
    var input = document.createElement("input");
    input.type = "hidden";
    if (type == 0)
    {
        input.name = "addToBasket";
    }
    else if (type == 1)
    {
        input.name = "removeFromBasket";
    }
    else
    {
        input.name = "leer";
    }
    input.value = pageId;
    form.appendChild(input);
    form.submit();
}
function editBasketPopup(type, pageId, id)
{
    document.getElementById(id).onclick = '';
    var form = document.createElement("form");
    document.body.appendChild(form);
    form.method = "get";
    form.name = "shoppingList"; 
    form.action = "";
    
    var input = document.createElement("input");
    input.type = "hidden";
    if (type == 0)
    {
        input.name = "addToBasket";
    }
    else if (type == 1)
    {
        input.name = "removeFromBasket";
    }
    else
    {
        input.name = "leer";
    }
    input.value = pageId;
    form.appendChild(input);
    form.submit();
}
function openArticle(url)
{
    opener.location.href=url;
    self.close();
}
//Nachbarländer
function GoLink(ZIP){
    document.location.href=('http://213.144.6.110/lidl/searchModul.do?zipcodesearch='+ZIP+'&variablesintro=&resetFilialDaten=1&variablesback=id;country;zipcode;city;district;street;ar;nf&url=#RDE-REQUEST:rdeServletServer/#/#RDE-REQUEST:rdePrefix/#/xchg#RDE-REQUEST:rdeUrlPathSessionID/#/#RDE-REQUEST:rdeProjectID/#/hs.xsl/index.htm?change=1&ar=');
}
// Region Map Functions
function save_region(id, name)
{
    document.region_selection_form.id.value = id;
    document.region_selection_form.name.value = name;
    document.region_selection_form.submit();
}
// Shop-Function
function openShop(url, id)
{
    document.getElementById(id).onclick = '';
    window.location.href = url;
}
function fenster_ekz_anpassen() { 
                    
                    var hoehe =  document.getElementById("box_pos").offsetHeight + document.getElementById("hint").offsetHeight + 100;
                           
                     var breite =  612;
            
            var Versioni = BrowserDetect.version;
            var Browseri = BrowserDetect.browser;
                var opera = Browseri.search(/Opera/);
                var mozilla = Browseri.search(/Firefox/);
                var safari = Browseri.search(/Safari/);
                var explorer = Browseri.search(/Explorer/);
                    //alert(Versioni);
                    
                if(explorer != -1) {
                        if(Versioni == 6){
                            breite = breite + 1;
                        }
                    if(Versioni == 7){
                            hoehe;
                            breite = breite + 4;
                        }
                }
            
                if(opera != -1 )
                {
                    hoehe = hoehe + 12;
                    breite = breite - 19
                }
                  if( mozilla != -1)
                  {
                      breite = breite - 4;
                      hoehe = hoehe + 49;
                  }
             if(safari != -1 )
                {
                    hoehe = hoehe - 2;
                    breite = breite - 23;
                }
      
        window.resizeTo(breite,hoehe);
             window.focus();
     
    }
 
function fenster_sms_erinnerung() {
    if (document.getElementById) {
        var hoehe = document.getElementById("endOfPAge").offsetTop + 40;
        var breite =  624;
        var Versioni = BrowserDetect.version;
        var Browseri = BrowserDetect.browser;
        var opera = Browseri.search(/Opera/);
        var mozilla = Browseri.search(/Firefox/);
        var safari = Browseri.search(/Safari/);
        var explorer = Browseri.search(/Explorer/);
                    //alert(Versioni);
                    
        if(explorer != -1) {
            if(Versioni == 6){
              hoehe = hoehe - 9;
              breite = breite - 12;
             }
             if(Versioni == 7){
                hoehe = hoehe - 9;
                breite = breite - 12;
             }
         }
            
         if(opera != -1 )
         {
            hoehe = hoehe + 2;
            breite = breite - 31;
         }
         if( mozilla != -1)
         {
             breite = breite - 16;
             hoehe = hoehe + 41;
          }
          if(safari != -1 )
          {
             hoehe = hoehe + 2;
             breite = breite - 36;
          }
        window.resizeTo(breite, hoehe);
        window.focus();
    }
}
function getElemPosX(elementId) {
    return document.getElementById(elementId).offsetLeft;
}
function getElemPosY(elementId) {
    return document.getElementById(elementId).offsetTop;
}
 
function etracker_escape(str)
{
    str = str.replace(/Á/g,'&#193;');
    str = str.replace(/É/g,'&#201;');
    str = str.replace(/Í/g,'&#205;');
    str = str.replace(/Ó/g,'&#211;');
    str = str.replace(/Ú/g,'&#218;');
    str = str.replace(/Ý/g,'&#221;');
    str = str.replace(/á/g,'&#225;');
    str = str.replace(/é/g,'&#233;');
    str = str.replace(/í/g,'&#237;');
    str = str.replace(/ó/g,'&#243;');
    str = str.replace(/ú/g,'&#250;');
    str = str.replace(/ý/g,'&#253;');
    str = str.replace(/Ą/g,'&#260;');
    str = str.replace(/ą/g,'&#261;');
    str = str.replace(/Ď/g,'&#270;');
    str = str.replace(/ď/g,'&#271;');
    str = str.replace(/Ę/g,'&#280;');
    str = str.replace(/ę/g,'&#281;');
    str = str.replace(/Ě/g,'&#282;');
    str = str.replace(/ě/g,'&#283;');
    str = str.replace(/Ć/g,'&#262;');
    str = str.replace(/ć/g,'&#263;');
    str = str.replace(/Č/g,'&#268;');
    str = str.replace(/č/g,'&#269;');
    str = str.replace(/Đ/g,'&#272;');
    str = str.replace(/đ/g,'&#273;');
    str = str.replace(/Ł/g,'&#321;');
    str = str.replace(/ł/g,'&#322;');
    str = str.replace(/Ń/g,'&#323;');
    str = str.replace(/ń/g,'&#324;');
    str = str.replace(/Ň/g,'&#327;');
    str = str.replace(/ň/g,'&#328;');
    str = str.replace(/Ř/g,'&#344;');
    str = str.replace(/ř/g,'&#345;');
    str = str.replace(/Ś/g,'&#346;');
    str = str.replace(/Š/g,'&#352;');
    str = str.replace(/š/g,'&#353;');
    str = str.replace(/Ť/g,'&#356;');
    str = str.replace(/ť/g,'&#357;');
    str = str.replace(/Ů/g,'&#366;');
    str = str.replace(/ů/g,'&#367;');
    str = str.replace(/Ź/g,'&#377;');
    str = str.replace(/ź/g,'&#378;');
    str = str.replace(/Ż/g,'&#379;');
    str = str.replace(/ż/g,'&#380;');
    str = str.replace(/Ž/g,'&#381;');
    str = str.replace(/ž/g,'&#382;');
    str = str.replace(/;/g,'&#894;');
    str = str.replace(/΄/g,'&#900;');
    str = str.replace(/΅/g,'&#901;');
    str = str.replace(/Ά/g,'&#902;');
    str = str.replace(/·/g,'&#903;');
    str = str.replace(/Έ/g,'&#904;');
    str = str.replace(/Ή/g,'&#905;');
    str = str.replace(/Ί/g,'&#906;');
    str = str.replace(/Ό/g,'&#908;');
    str = str.replace(/Ύ/g,'&#910;');
    str = str.replace(/Ώ/g,'&#911;');
    str = str.replace(/ΐ/g,'&#912;');
    str = str.replace(/Α/g,'&#913;');
    str = str.replace(/Β/g,'&#914;');
    str = str.replace(/Γ/g,'&#915;');
    str = str.replace(/Δ/g,'&#916;');
    str = str.replace(/Ε/g,'&#917;');
    str = str.replace(/Ζ/g,'&#918;');
    str = str.replace(/Η/g,'&#919;');
    str = str.replace(/Θ/g,'&#920;');
    str = str.replace(/Ι/g,'&#921;');
    str = str.replace(/Κ/g,'&#922;');
    str = str.replace(/Λ/g,'&#923;');
    str = str.replace(/Μ/g,'&#924;');
    str = str.replace(/Ν/g,'&#925;');
    str = str.replace(/Ξ/g,'&#926;');
    str = str.replace(/Ο/g,'&#927;');
    str = str.replace(/Π/g,'&#928;');
    str = str.replace(/Ρ/g,'&#929;');
    str = str.replace(/Σ/g,'&#931;');
    str = str.replace(/Τ/g,'&#932;');
    str = str.replace(/Υ/g,'&#933;');
    str = str.replace(/Φ/g,'&#934;');
    str = str.replace(/Χ/g,'&#935;');
    str = str.replace(/Ψ/g,'&#936;');
    str = str.replace(/Ω/g,'&#937;');
    str = str.replace(/Ϊ/g,'&#938;');
    str = str.replace(/Ϋ/g,'&#939;');
    str = str.replace(/ά/g,'&#940;');
    str = str.replace(/έ/g,'&#941;');
    str = str.replace(/ή/g,'&#942;');
    str = str.replace(/ί/g,'&#943;');
    str = str.replace(/ΰ/g,'&#944;');
    str = str.replace(/α/g,'&#945;');
    str = str.replace(/β/g,'&#946;');
    str = str.replace(/γ/g,'&#947;');
    str = str.replace(/δ/g,'&#948;');
    str = str.replace(/ε/g,'&#949;');
    str = str.replace(/ζ/g,'&#950;');
    str = str.replace(/η/g,'&#951;');
    str = str.replace(/θ/g,'&#952;');
    str = str.replace(/ι/g,'&#953;');
    str = str.replace(/κ/g,'&#954;');
    str = str.replace(/λ/g,'&#955;');
    str = str.replace(/μ/g,'&#956;');
    str = str.replace(/ν/g,'&#957;');
    str = str.replace(/ξ/g,'&#958;');
    str = str.replace(/ο/g,'&#959;');
    str = str.replace(/π/g,'&#960;');
    str = str.replace(/ρ/g,'&#961;');
    str = str.replace(/ς/g,'&#962;');
    str = str.replace(/σ/g,'&#963;');
    str = str.replace(/τ/g,'&#964;');
    str = str.replace(/υ/g,'&#965;');
    str = str.replace(/φ/g,'&#966;');
    str = str.replace(/χ/g,'&#967;');
    str = str.replace(/ψ/g,'&#968;');
    str = str.replace(/ω/g,'&#969;');
    str = str.replace(/ϊ/g,'&#970;');
    str = str.replace(/ϋ/g,'&#971;');
    str = str.replace(/ό/g,'&#972;');
    str = str.replace(/ύ/g,'&#973;');
    str = str.replace(/ώ/g,'&#974;');
    str = str.replace(/Ç/g,'&#199;');
    str = str.replace(/ç/g,'&#231;');     
    str = str.replace(/Ğ/g,'&#286;');        
    str = str.replace(/ğ/g,'&#287;');       
    str = str.replace(/İ/g,'&#304;');        
    str = str.replace(/ı/g,'&#305;');          
    str = str.replace(/Ö/g,'&#214;');     
    str = str.replace(/ö/g,'&#246;');     
    str = str.replace(/Ş/g,'&#350;');     
    str = str.replace(/ş/g,'&#351;');     
    str = str.replace(/Ü/g,'&#220;');   
    str = str.replace(/ü/g,'&#252;');
    return str;
}
 
function etracker_length(etracker_path)
{
    var anzahl_zeichen = etracker_path.length;
    if (anzahl_zeichen > 255)
    {
        etracker_path = etracker_path.substr(anzahl_zeichen-255);
    }
    return etracker_path;
}

/**
*
*  UTF-8 data decode
*  http://www.webtoolkit.info/
*
**/
         function decode(utftext) {
                var string = "";
                var i = 0;
                var c = c1 = c2 = 0;
                while ( i < utftext.length ) {
                        c = utftext.charCodeAt(i);
                        if (c < 128) {
                                string += String.fromCharCode(c);
                                i++;
                        }
                        else if((c > 191) && (c < 224)) {
                                c2 = utftext.charCodeAt(i+1);
                                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                                i += 2;
                        }
                        else {
                                c2 = utftext.charCodeAt(i+1);
                                c3 = utftext.charCodeAt(i+2);
                                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                                i += 3;
                        }
                }
                return string;
        }


function html_decode(input)
    {
        var output = input;
        output = output.replace(/&nbsp;/g,' ');
        output = output.replace(/&iexcl;/g,'¡');
        output = output.replace(/&cent;/g,'¢');
        output = output.replace(/&pound;/g,'£');
        output = output.replace(/&curren;/g,'¤');
        output = output.replace(/&yen;/g,'¥');
        output = output.replace(/&brvbar;/g,'¦');
        output = output.replace(/&sect;/g,'§');
        output = output.replace(/&uml;/g,'¨');
        output = output.replace(/&copy;/g,'©');
        output = output.replace(/&ordf;/g,'ª');
        output = output.replace(/&laquo;/g,'«');
        output = output.replace(/&not;/g,'¬');
        output = output.replace(/&shy;/g,'­');
        output = output.replace(/&reg;/g,'®');
        output = output.replace(/&macr;/g,'¯');
        output = output.replace(/&deg;/g,'°');
        output = output.replace(/&plusmn;/g,'±');
        output = output.replace(/&sup2;/g,'²');
        output = output.replace(/&sup3;/g,'³');
        output = output.replace(/&acute;/g,'´');
        output = output.replace(/&micro;/g,'µ');
        output = output.replace(/&para;/g,'¶');
        output = output.replace(/&middot;/g,'·');
        output = output.replace(/&cedil;/g,'¸');
        output = output.replace(/&sup1;/g,'¹');
        output = output.replace(/&ordm;/g,'º');
        output = output.replace(/&raquo;/g,'»');
        output = output.replace(/&frac14;/g,'¼');
        output = output.replace(/&frac12;/g,'½');
        output = output.replace(/&frac34;/g,'¾');
        output = output.replace(/&iquest;/g,'¿');
        output = output.replace(/&Agrave;/g,'À');
        output = output.replace(/&Aacute;/g,'Á');
        output = output.replace(/&Acirc;/g,'Â');
        output = output.replace(/&Atilde;/g,'Ã');
        output = output.replace(/&Auml;/g,'Ä');
        output = output.replace(/&Aring;/g,'Å');
        output = output.replace(/&AElig;/g,'Æ');
        output = output.replace(/&Ccedil;/g,'Ç');
        output = output.replace(/&Egrave;/g,'È');
        output = output.replace(/&Eacute;/g,'É');
        output = output.replace(/&Ecirc;/g,'Ê');
        output = output.replace(/&Euml;/g,'Ë');
        output = output.replace(/&Igrave;/g,'Ì');
        output = output.replace(/&Iacute;/g,'Í');
        output = output.replace(/&Icirc;/g,'Î');
        output = output.replace(/&Iuml;/g,'Ï');
        output = output.replace(/&ETH;/g,'Ð');
        output = output.replace(/&Ntilde;/g,'Ñ');
        output = output.replace(/&Ograve;/g,'Ò');
        output = output.replace(/&Oacute;/g,'Ó');
        output = output.replace(/&Ocirc;/g,'Ô');
        output = output.replace(/&Otilde;/g,'Õ');
        output = output.replace(/&Ouml;/g,'Ö');
        output = output.replace(/&times;/g,'×');
        output = output.replace(/&Oslash;/g,'Ø');
        output = output.replace(/&Ugrave;/g,'Ù');
        output = output.replace(/&Uacute;/g,'Ú');
        output = output.replace(/&Ucirc;/g,'Û');
        output = output.replace(/&Uuml;/g,'Ü');
        output = output.replace(/&Yacute;/g,'Ý');
        output = output.replace(/&THORN;/g,'Þ');
        output = output.replace(/&szlig;/g,'ß');
        output = output.replace(/&agrave;/g,'à');
        output = output.replace(/&aacute;/g,'á');
        output = output.replace(/&acirc;/g,'â');
        output = output.replace(/&atilde;/g,'ã');
        output = output.replace(/&auml;/g,'ä');
        output = output.replace(/&aring;/g,'å');
        output = output.replace(/&aelig;/g,'æ');
        output = output.replace(/&ccedil;/g,'ç');
        output = output.replace(/&egrave;/g,'è');
        output = output.replace(/&eacute;/g,'é');
        output = output.replace(/&ecirc;/g,'ê');
        output = output.replace(/&euml;/g,'ë');
        output = output.replace(/&igrave;/g,'ì');
        output = output.replace(/&iacute;/g,'í');
        output = output.replace(/&icirc;/g,'î');
        output = output.replace(/&iuml;/g,'ï');
        output = output.replace(/&eth;/g,'ð');
        output = output.replace(/&ntilde;/g,'ñ');
        output = output.replace(/&ograve;/g,'ò');
        output = output.replace(/&oacute;/g,'ó');
        output = output.replace(/&ocirc;/g,'ô');
        output = output.replace(/&otilde;/g,'õ');
        output = output.replace(/&ouml;/g,'ö');
        output = output.replace(/&divide;/g,'÷');
        output = output.replace(/&oslash;/g,'ø');
        output = output.replace(/&ugrave;/g,'ù');
        output = output.replace(/&uacute;/g,'ú');
        output = output.replace(/&ucirc;/g,'û');
        output = output.replace(/&uuml;/g,'ü');
        output = output.replace(/&yacute;/g,'ý');
        output = output.replace(/&thorn;/g,'þ');
        output = output.replace(/&yuml;/g,'ÿ');
        output = output.replace(/&quot;/g,'"');
        output = output.replace(/&lt;/g,'<');
        output = output.replace(/&gt;/g,'>');
        output = output.replace(/&amp;/g,'&');

        output = output.replace(/&#280;/g,'E');
        output = output.replace(/&#281;/g,'e');
        output = output.replace(/&#282;/g,'E');
        output = output.replace(/&#283;/g,'e');
        output = output.replace(/&#366;/g,'U');
        output = output.replace(/&#367;/g,'u');
        output = output.replace(/&#252;/g,'Y');
        output = output.replace(/&#253;/g,'y');
        output = output.replace(/&#268;/g,'C');
        output = output.replace(/&#269;/g,'c');
        output = output.replace(/&#270;/g,'D');
        output = output.replace(/&#271;/g,'d');
        output = output.replace(/&#327;/g,'N');
        output = output.replace(/&#328;/g,'n');
        output = output.replace(/&#344;/g,'R');
        output = output.replace(/&#345;/g,'r');
        output = output.replace(/&#352;/g,'S');
        output = output.replace(/&#353;/g,'s');
        output = output.replace(/&#356;/g,'T');
        output = output.replace(/&#357;/g,'t');
        output = output.replace(/&#381;/g,'Z');
        output = output.replace(/&#382;/g,'z');
        return output;
    }
 
function makeValidSMSstring(str)
{
    str = str.replace(/Á/g,'&#193;');
    str = str.replace(/É/g,'&#201;');
    str = str.replace(/Í/g,'&#205;');
    str = str.replace(/Ó/g,'&#211;');
    str = str.replace(/Ú/g,'&#218;');
    str = str.replace(/Ý/g,'&#221;');
    str = str.replace(/á/g,'&#225;');
    str = str.replace(/é/g,'&#233;');
    str = str.replace(/í/g,'&#237;');
    str = str.replace(/ó/g,'&#243;');
    str = str.replace(/ú/g,'&#250;');
    str = str.replace(/ý/g,'&#253;');
    str = str.replace(/Ą/g,'&#260;');
    str = str.replace(/ą/g,'&#261;');
    str = str.replace(/Ď/g,'&#270;');
    str = str.replace(/ď/g,'&#271;');
    str = str.replace(/Ę/g,'&#280;');
    str = str.replace(/ę/g,'&#281;');
    str = str.replace(/Ě/g,'&#282;');
    str = str.replace(/ě/g,'&#283;');
    str = str.replace(/Ć/g,'&#262;');
    str = str.replace(/ć/g,'&#263;');
    str = str.replace(/Č/g,'&#268;');
    str = str.replace(/č/g,'&#269;');
    str = str.replace(/Đ/g,'&#272;');
    str = str.replace(/đ/g,'&#273;');
    str = str.replace(/Ł/g,'&#321;');
    str = str.replace(/ł/g,'&#322;');
    str = str.replace(/Ń/g,'&#323;');
    str = str.replace(/ń/g,'&#324;');
    str = str.replace(/Ň/g,'&#327;');
    str = str.replace(/ň/g,'&#328;');
    str = str.replace(/Ř/g,'&#344;');
    str = str.replace(/ř/g,'&#345;');
    str = str.replace(/Ś/g,'&#346;');
    str = str.replace(/Š/g,'&#352;');
    str = str.replace(/š/g,'&#353;');
    str = str.replace(/Ť/g,'&#356;');
    str = str.replace(/ť/g,'&#357;');
    str = str.replace(/Ů/g,'&#366;');
    str = str.replace(/ů/g,'&#367;');
    str = str.replace(/Ź/g,'&#377;');
    str = str.replace(/ź/g,'&#378;');
    str = str.replace(/Ż/g,'&#379;');
    str = str.replace(/ż/g,'&#380;');
    str = str.replace(/Ž/g,'&#381;');
    str = str.replace(/ž/g,'&#382;');
    str = str.replace(/;/g,'&#894;');
    str = str.replace(/΄/g,'&#900;');
    str = str.replace(/΅/g,'&#901;');
    str = str.replace(/Ά/g,'&#902;');
    str = str.replace(/·/g,'&#903;');
    str = str.replace(/Έ/g,'&#904;');
    str = str.replace(/Ή/g,'&#905;');
    str = str.replace(/Ί/g,'&#906;');
    str = str.replace(/Ό/g,'&#908;');
    str = str.replace(/Ύ/g,'&#910;');
    str = str.replace(/Ώ/g,'&#911;');
    str = str.replace(/ΐ/g,'&#912;');
    str = str.replace(/Α/g,'&#913;');
    str = str.replace(/Β/g,'&#914;');
    str = str.replace(/Γ/g,'&#915;');
    str = str.replace(/Δ/g,'&#916;');
    str = str.replace(/Ε/g,'&#917;');
    str = str.replace(/Ζ/g,'&#918;');
    str = str.replace(/Η/g,'&#919;');
    str = str.replace(/Θ/g,'&#920;');
    str = str.replace(/Ι/g,'&#921;');
    str = str.replace(/Κ/g,'&#922;');
    str = str.replace(/Λ/g,'&#923;');
    str = str.replace(/Μ/g,'&#924;');
    str = str.replace(/Ν/g,'&#925;');
    str = str.replace(/Ξ/g,'&#926;');
    str = str.replace(/Ο/g,'&#927;');
    str = str.replace(/Π/g,'&#928;');
    str = str.replace(/Ρ/g,'&#929;');
    str = str.replace(/Σ/g,'&#931;');
    str = str.replace(/Τ/g,'&#932;');
    str = str.replace(/Υ/g,'&#933;');
    str = str.replace(/Φ/g,'&#934;');
    str = str.replace(/Χ/g,'&#935;');
    str = str.replace(/Ψ/g,'&#936;');
    str = str.replace(/Ω/g,'&#937;');
    str = str.replace(/Ϊ/g,'&#938;');
    str = str.replace(/Ϋ/g,'&#939;');
    str = str.replace(/ά/g,'&#940;');
    str = str.replace(/έ/g,'&#941;');
    str = str.replace(/ή/g,'&#942;');
    str = str.replace(/ί/g,'&#943;');
    str = str.replace(/ΰ/g,'&#944;');
    str = str.replace(/α/g,'&#945;');
    str = str.replace(/β/g,'&#946;');
    str = str.replace(/γ/g,'&#947;');
    str = str.replace(/δ/g,'&#948;');
    str = str.replace(/ε/g,'&#949;');
    str = str.replace(/ζ/g,'&#950;');
    str = str.replace(/η/g,'&#951;');
    str = str.replace(/θ/g,'&#952;');
    str = str.replace(/ι/g,'&#953;');
    str = str.replace(/κ/g,'&#954;');
    str = str.replace(/λ/g,'&#955;');
    str = str.replace(/μ/g,'&#956;');
    str = str.replace(/ν/g,'&#957;');
    str = str.replace(/ξ/g,'&#958;');
    str = str.replace(/ο/g,'&#959;');
    str = str.replace(/π/g,'&#960;');
    str = str.replace(/ρ/g,'&#961;');
    str = str.replace(/ς/g,'&#962;');
    str = str.replace(/σ/g,'&#963;');
    str = str.replace(/τ/g,'&#964;');
    str = str.replace(/υ/g,'&#965;');
    str = str.replace(/φ/g,'&#966;');
    str = str.replace(/χ/g,'&#967;');
    str = str.replace(/ψ/g,'&#968;');
    str = str.replace(/ω/g,'&#969;');
    str = str.replace(/ϊ/g,'&#970;');
    str = str.replace(/ϋ/g,'&#971;');
    str = str.replace(/ό/g,'&#972;');
    str = str.replace(/ύ/g,'&#973;');
    str = str.replace(/ώ/g,'&#974;');
    str = str.replace(/Ç/g,'&#199;');
    str = str.replace(/ç/g,'&#231;');     
    str = str.replace(/Ğ/g,'&#286;');        
    str = str.replace(/ğ/g,'&#287;');       
    str = str.replace(/İ/g,'&#304;');        
    str = str.replace(/ı/g,'&#305;');          
    str = str.replace(/Ö/g,'&#214;');     
    str = str.replace(/ö/g,'&#246;');     
    str = str.replace(/Ş/g,'&#350;');     
    str = str.replace(/ş/g,'&#351;');     
    str = str.replace(/Ü/g,'&#220;');   
    str = str.replace(/ü/g,'&#252;');
    return str;
}