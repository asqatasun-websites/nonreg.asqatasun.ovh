window.onload=montre;
window.onload=afficheOnglets;
// Variables globales
var img=0;
var fetchDone = new Array(false, false, false, false, false);
var activeNode = new Array(true, true, true, true, true);

// Fonction permettant de rajouter les éléments récupérés aux éléments statiques.
function constructElement(/** int */ number)
{
	if (document.getElementById('onglet' + number + '-actif-content') && document.getElementById('onglet' + number + '-actif'))
	{
		var element = document.getElementById('onglet' + number + '-actif');
		var fetchElement = document.getElementById('onglet' + number + '-actif-content').childNodes;
		//On insère après le premier noeud, puisque le premier noeud correspond au titre 
		element.insertBefore(fetchElement[1], element.childNodes[1]);
		fetchDone[number - 1] = true;
	}
}


// Fonction d'affichage du menu top
function montre(id) {
var d = document.getElementById(id);
	for (var i = 1; i<=10; i++) {
		if (document.getElementById('smenu'+i)) {document.getElementById('smenu'+i).style.display='none';}
	}
if (d) {d.style.display='block';}
}

// Fonction permettant l'affichage des onglets
function afficheOnglets(nb) {
	var fetchedContent = nb == 1 || nb == 4 || nb == 5;
	if (fetchedContent && !fetchDone[nb - 1])
	{
		constructElement(nb);
	}
	// Commence par fermer tous les onglets
	for (var j = 1; j<=10; j++) {
		if (document.getElementById('onglet'+j+'-actif')) {
			document.getElementById('onglet'+j+'-actif').style.display='none';
			document.getElementById('onglet'+j+'-inactif').style.display='block';
		}	
	}
	// Puis ouvre l'onglet cliqu� si celui-ci existe
	if ((nb)&&(document.getElementById('onglet'+nb+'-actif'))) {
		document.getElementById('onglet'+nb+'-inactif').style.display='none';
		document.getElementById('onglet'+nb+'-actif').style.display='block';
	}
//	document.getElementById('onglet1-actif').style.position ='absolute';
//	document.getElementById('onglet1-actif').style.top ='310px';
//	document.getElementById('onglet1-actif').style.display='block';	
	
}

// Fonction permettant l'affichage des onglets
function afficheMenuRapide(nb) {
	// Commence par fermer tous les onglets
	for (var j = 1; j<=10; j++) {
		if (document.getElementById('menu'+j+'-actif')) {
			document.getElementById('menu'+j+'-actif').style.display='none';
			document.getElementById('menu'+j+'-inactif').style.display='block';
		}	
	}
	// Puis ouvre l'onglet cliqu� si celui-ci existe
	if ((nb)&&(document.getElementById('menu'+nb+'-actif'))) {
		document.getElementById('menu'+nb+'-actif').style.display='block';
		document.getElementById('menu'+nb+'-inactif').style.display='none';
	}
}

//Fonction d'affichage du diaporama en page d'accueil
function diaporama() {
document.getElementById('imageDiapo').src = context.contextPath + '/skins/Univ-Evry/templates/index/resources/img/diaporama/'+images[img]['src'];
document.getElementById('titreDiapo').innerHTML = images[img]['titre'];
document.getElementById('descripDiapo').innerHTML = images[img]['description'];
img +=1;
if (img >= images.length) img = 0;
setTimeout("diaporama()",5000);	
}

function lancementJavascript() {
	afficheMenuRapide();
	afficheOnglets(1); 
	montre();
}
function lancementJavascriptIndex() {
	afficheMenuRapide();
	afficheOnglets(1); 
	montre();
	diaporama();
}

//
function giveActifStatus(/** int */ number)
{
	var result = 0;
	var j = 0;
	while (!activeNode[j] && j < activeNode.length)
	{
		j++;
	}
	result = j + 1;
	return result;
}

//Fonction qui permet de masquer un onglet si son contenu est vide dans la page des enseignants chercheurs.
function hideTab(/** int */ askedNumber)
{
	var ulElement = document.getElementById("ligne-onglets");
	var child = ulElement.firstChild;
	for (var i = 1; i < askedNumber; i++)
	{
		child = child.nextSibling;
	}
	child.style.display = "none";
	activeNode[askedNumber - 1] = false;
	var activeStatus = giveActifStatus(askedNumber);
	child = ulElement.firstChild;
	for (var k = 1; k < activeStatus; k++)
	{
		child = child.nextSibling;
	}
	child.className = "actif";
}

