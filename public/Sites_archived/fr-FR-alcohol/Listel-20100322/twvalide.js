<!--
function twValide(nForm) {
  // ********************************************
  //  FONCTION UNIVERSELLE POUR FORCER LA SAISIE
  // DES TOUS LES TYPES DE CHAMPS D'UN FORMULAIRE
  // Plus plusieurs validations de champs.
  // ********************************************
  // twValide 2.2 (octobre 2001)
  // Cr�ation : Django (Oznog) Blais
  // http://www.trucsweb.com
  //
  // LIBRE DE DROITS � LA CONDITION DE LAISSER
  // CETTE REMARQUE.
  //
  // INSTRUCTION
  // Dans le fichier "valide.htm" ou sur le site �
  // http://www.trucsweb.com/Javascript/
  // (cliquer > Fonction de validation twValide)
  // ********************************************
  // MAJ - Octobre : Ajout des fonctions
  //                 - twValide_courriel()
  //                 - twValide_date()
  //     - Septembre : Ajustement de la fonction
  //                 - twValide_char()
  //                   Validation minimum de caract�res
  //     - Septembre : Ajustement d'un bogue.
  //     - �t� 2001 : Version 2
  // ********************************************
  // COMPATIBILIT� test�e sur ces navigateurs
  // - Internet Explorer 5.0, 5.5, 6.0 (Win98/2000)
  // - Internet Explorer 3.0, 4.0 (Mac OS7.6+)
  // - Internet Explorer 4.5, 5.0, 6.0 (Mac G4)
  // - Mozilla 0.9.3 (Win98/2000)
  // - Netscape 4.7, 6.0 (Win98/2000)
  // - Communicator 4.0 (Mac OS6+)
  // - Navigator gold 3.0 (Mac OS6+)
  // - Netscape 4.5, 6.0 (Mac G4)
  // - Opera 5.0 (Win98/2000)
  //
  // COMPATIBILIT� suppos�e
  // - Internet Explorer 4+ (Win)
  // - Netscape 3+ (Win)
  // - Opera 4+ (Win)
  // - AOL 4+ (Win)
  // ********************************************
  
  // ********************************************
  // Ne rien modifier ci-dessous
  // ********************************************
  var RdCkOk = false;
  var RdMess = "";
  var CbMess = "";
  var CbCkOk = false;
  var msg = ""
  var eng = "_____________________________________________________________\n\n";
      eng += "The form was not submitted because of the following error.\n";
      eng += "Please correct these error(s) and resubmit.\n";
      eng += "_____________________________________________________________\n\n";
      eng += "- The following required field(s) are empty :"
  var fra = "_____________________________________________________________\n\n";
      fra += "Le formulaire n\'a pas �t� envoy�, vous devez remplir tous les champs\n";
      fra += "obligatoire(s), veuillez corriger et envoyer de nouveau le formulaire. Merci!\n";
      fra += "_____________________________________________________________\n\n";
      fra += "- Vous devez entrer le(s) champ(s) suivant :"
  for (var i=0; i<nForm.elements.length; i++) {
    if (varValid.indexOf(nForm.elements[i].name) == -1 && nForm.elements[i].type != 'submit' && nForm.elements[i].type != 'reset' && nForm.elements[i].type != 'button' && nForm.elements[i].type != 'hidden') {
      if (nForm.elements[i].type == 'select-one') {
        if (nForm.elements[i].selectedIndex == 0) {
          msg += "\n         " + nForm.elements[i].name;
        }
      } else
      if (nForm.elements[i].type == 'select-multiple') {
        if (nForm.elements[i].selectedIndex < 0) {
          msg += "\n         " + nForm.elements[i].name;
        }
      } else
      if (nForm.elements[i].type == 'text') {
        if (nForm.elements[i].value == '') {
          msg += "\n         " + nForm.elements[i].name;
        }
      } else
      if (nForm.elements[i].type == 'textarea') {
        if (nForm.elements[i].value == '') {
          msg += "\n         " + nForm.elements[i].name;
        }
      } else
      if (nForm.elements[i].type == 'password') {
        if (nForm.elements[i].value == '') {
          msg += "\n         " + nForm.elements[i].name;
        }
      } else
      if (nForm.elements[i].type == 'radio') {
        if (nForm.elements[i].checked) {
            RdCkOk = true;
            RdMess = "";
        }
        if (RdCkOk == false) {
          RdMess = "\n         " + nForm.elements[i].name;
        }
      } else
      if (nForm.elements[i].type == 'checkbox'&&CbCkOk == false) {
        if (nForm.elements[i].checked) {
          CbCkOk = true;
          CbMess = "";
        }
        if (CbCkOk == false) {
            CbMess = "\n         " + nForm.elements[i].name;
        }
      } 	
    }
  }
  if (msg.length != 0||RdMess.length != 0||CbMess.length != 0) {
    msg += RdMess + CbMess; 
    if (nlang=="fr") msg = fra + msg + "\n"; else msg = eng + msg + "\n";
    alert(msg);
    return false;
  }
}
// ********************************************
// FIN DE LA FONCTION twValide()
// ********************************************

// ********************************************
// FONCTION twCompare_password()
// ********************************************
function twCompare_password(nPass1,nPass2) {
  // Ajouter ce code dans la saisie du deuxi�me mot de passe
  // onChange="twCompare_password(this.form.pass1,this);"
  // Noter que le premier mot de passe doit avoir le nom "pass1"
  if (nPass1.value != nPass2.value) {
    nPass1.value = "";
    nPass2.value = "";
    nPass1.focus();
    if (nlang=="fr") 
      alert ("\nVous n'avez pas entr� le m�me mot de passe deux foi.\nVeuillez entrer � nouveau le mot de passe..")
    else 
      alert ("\nYou did not enter the same new password twice.\nPlease re-enter your password.")
  } else return true;
}

// ********************************************
// FONCTION twValide_char()
// ********************************************
function twValide_char(nChamp,nAccepte,nbr) {
  // Ajouter ce code dans la saisie du champs
  //  onBlur="twValide_char(this,'()-0123456789')"
  // Les caract�res accept�s peuvent �tre chang�s
  var ok = "oui";
  var min = "oui";
  var nChar;
  for (var i=0; i<nChamp.value.length; i++) {
    nChar = "" + nChamp.value.substring(i, i+1);
    if (nAccepte.indexOf(nChar) == "-1") ok = "non";
  }
  if (ok == "non") {
    if (nlang=="fr") 
      alert ("\nEntr� invalide!\nSeul ces caract�res sont valide:\n "+nAccepte)
    else 
      alert("\nInvalid entry!\nOnly this characters are accepted:\n "+nAccepte);
    nChamp.focus();
    nChamp.select();
  }
  if (nChamp.value.length < nbr&&nChamp.value.length > 0) min = "non";
  if (min == "non") {
    if (nlang=="fr")
      alert("\nSaisie invalide!\nAssurez-vous d'entrer au moins " + nbr + " caract�res.");
    else 
      alert("\nInvalid entry!\nYou must at least " + nbr + " characters.");
    nChamp.focus();
    nChamp.select();
  }
}
// ********************************************
// FONCTION twValide_char2()
// ********************************************
function twValide_char2(nChamp,nAccepte) {
  // Ajouter ce code dans la saisie du champs
  //  onBlur="twValide_char(this,'()-0123456789')"
  // Les caract�res accept�s peuvent �tre chang�s
  var ok = "oui";
  var min = "oui";
  var nChar;
  for (var i=0; i<nChamp.value.length; i++) {
    nChar = "" + nChamp.value.substring(i, i+1);
    if (nAccepte.indexOf(nChar) == "-1") ok = "non";
  }
  if (ok == "non") {
    if (nlang=="fr") 
      alert ("\nEntr� invalide!\nSeul ces caract�res sont valide:\n "+nAccepte)
    else 
      alert("\nInvalid entry!\nOnly this characters are accepted:\n "+nAccepte);
    nChamp.focus();
    nChamp.select();
  }
  
}

// ********************************************
// FONCTION twValide_courriel()
// ********************************************
function twValide_courriel(nChamp) {
  // Ajouter ce code dans la saisie du champs
  //  onBlur="twValide_courriel(this)"
  if (nChamp.value.length > 0) {
    if ((nChamp.value.lastIndexOf(".") < nChamp.value.indexOf("@")+3 
      || nChamp.value.lastIndexOf(".") > nChamp.value.length-3 
      || nChamp.value.indexOf("@") < 1 ) 
      || nChamp.value.indexOf("@") != nChamp.value.lastIndexOf("@")) {
      if (nlang=="fr")
        alert("\nSaisie invalide!\nAssurez-vous d'entrer un format valide de courriel \"nn@nn.nn\".");
      else
        alert("\nInvalid entry!\nYou must enter a valid email address \"nn@nn.nn\".");
      nChamp.focus();
      nChamp.select();
    }
  }
}

// ********************************************
// FONCTION twValide_date()
// ********************************************
function twValide_date(nDate,nA1,nA2,nFormat) {
  // Ajouter ce code dans la saisie du champs date
  //  onBlur="twValide_date(this,1950,2020,'jj/mm/aaaa')"
  // La date valide doit �tre entre 1950 et 2020.
  //    vous pouvez changer l'interval.
  // Format valide: (ne pas utiliser d'autre format...
  // - jj/mm/aaaa
  // - mm/jj/aaaa
  // - aaaa/mm/jj
  // Noter que vous devriez indiquer � l'usager le format valide...
  var ok = "oui";
  var nChar;
  var aDate;
  var nJ;
  var nM;
  var nA;
  var jMax = 31;
  nAccepte = "0123456789/";
  if (nDate.value.length > 0) {
    if (nDate.value.length < 8 || nDate.value.length > 10) ok = "non";
    for (var i=0; i<nDate.value.length; i++) {
      nChar = "" + nDate.value.substring(i, i+1);
      if (nAccepte.indexOf(nChar) == "-1") ok = "non";
    }
    if (ok == "oui") {
      aDate = nDate.value.split("/");
      if (nFormat == "jj/mm/aaaa") { nJ = aDate[0]; nM = aDate[1]; nA = aDate[2];
      } else if (nFormat == "mm/jj/aaaa") { nJ = aDate[1]; nM = aDate[0]; nA = aDate[2];
        } else if (nFormat == "aaaa/mm/jj") { nJ = aDate[2]; nM = aDate[1]; nA = aDate[0]; }
      if ((nJ < 1 || nJ > jMax) || (nM < 1 || nM > 12) || (nA < nA1 || nA > nA2)) ok = "non";
      else {
        if (nM == 2) { 
          if (AnneeBissex(nA) == true) { jMax = 29 } else { jMax = 28 }
        }
        if ((nM == 1 || nM == 3 || nM == 5 || nM == 7 || nM == 8 || nM == 10 || nM == 12)) jMax = 31
        if ((nM == 4 || nM == 6 || nM == 9 || nM == 11)) jMax = 30
        if (nJ < 1 || nJ > jMax) ok = "non";
      }
    }
    if (ok == "non") {
      nDate.focus();
      nDate.select();
      if (nlang=="fr") 
        alert ("\nDate invalide!. Veuillez entrer une date valide: "+nFormat+"\nentre "+nA1+" et "+nA2)
      else 
        alert ("\nInvalid date. Please re-enter valid date: "+nFormat+"\nbetween "+nA1+" et "+nA2)
    }
  }
}

// ********************************************
// FONCTION twCompteur_Texte()
// ********************************************
function twCompteur_Texte(nTexte, nCompteur, nLimite) {
  // Ajouter ce code dans la saisie du champ "textarea"
  // onKeyDown="twCompteur_Texte(this,this.form.compte,2000);"
  // onKeyUp="twCompteur_Texte(this,this.form.compte,2000);"
  // 2000 repr�sente le nombre maximum de caract�res accept�
  // Vous devez aussi ajouter une bo�te de saisie qui
  // indique le nombre de caract�res restant.
  // <input readonly="readonly" type="text" name="compte" value="2000">
  if (nTexte.value.length > nLimite)
    nTexte.value = nTexte.value.substring(0, nLimite);
  else 
    nCompteur.value = nLimite - nTexte.value.length;
}
// -->