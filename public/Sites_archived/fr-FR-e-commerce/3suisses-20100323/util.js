function adjustIFrameSize (iframeWindow) {
  if (iframeWindow.document.height) {
    var iframeElement = parent.document.getElementById(iframeWindow.name);
    iframeElement.style.height = iframeWindow.document.height + 'px';
    iframeElement.style.width = iframeWindow.document.width + 'px';
  }
  else if (document.all) {
    var iframeElement = parent.document.all[iframeWindow.name];
    if (iframeWindow.document.compatMode &&
        iframeWindow.document.compatMode != 'BackCompat') 
    {
      iframeElement.style.height = 
        iframeWindow.document.documentElement.scrollHeight + 5 + 'px';
      iframeElement.style.width = 
        iframeWindow.document.documentElement.scrollWidth + 5 + 'px';
    }
    else {
      iframeElement.style.height = iframeWindow.document.body.scrollHeight + 5 + 'px';
      iframeElement.style.width = iframeWindow.document.body.scrollWidth + 5 + 'px';
    }
  }
}


function appliqueStyleErreur(nomChamp){
if( (document.getElementById(nomChamp).className != null) && (document.getElementById(nomChamp).className.length > 0) ){
document.getElementById(nomChamp).className = 'entourageErreur_'+document.getElementById(nomChamp).className ;
return true ;
}
if( (document.getElementById(nomChamp).style.border != null) && (document.getElementById(nomChamp).style.border.length > 0) ){
document.getElementById(nomChamp).style.border = '1px solid #CC1541';
return true ;
}
if( (nomChamp.match('radioBoutonSI3SI') != -1) || (nomChamp.match('comboBoxSI3SI') != -1) ){
if(document.getElementById(nomChamp).style.border != null){
document.getElementById(nomChamp).style.border = '1px solid #CC1541';
return true ;
}
}
return false ;
}
function openPopupPage(urlPopup,urlPage,mWidth,mHeigth) {
window.open(urlPopup+"?urlPage="+urlPage,"","width="+mWidth+",height="+mHeigth+",left=" +
((screen.width - mWidth)/2) + ",scrollbars=yes, top=" + ((screen.height - mHeigth)/2) );
}
function openPopupNousContacter() {
window.open("portlets/nousContacter/NousContacterController.jpf?premiere=oui","","scrollbars=yes, width=500,height=470,left=" +
((screen.width - 500)/2) + ",top=" + ((screen.height - 470)/2) );
}
function openPopupPageWithoutScrollbars(urlPopup,param,mWidth,mHeigth) {
    window.open(urlPopup+"?"+param,"","width="+mWidth+",height="+mHeigth+",left=" + 
            ((screen.width - mWidth)/2) + ",scrollbars=no, top=" + ((screen.height - mHeigth)/2) );
}
function getEvent(event)
{
if (!event)
{
var event = window.event;
}
return event;
}
var console = new Console();
function Console()
{
this.canvas = null;
this.println = consolePrintln;
this.show = consoleShow;
}
function consolePrintln(object)
{
if (!this.canvas)
{
this.canvas = window.open("about:blank", "Console", "toolbar = no, width = 640, height = 480, directories = no, status = no, scrollbars = yes, resize = no, menubar = no");
}
this.canvas.document.write(object);
this.canvas.document.write("<br/>");
}
function consoleShow(object)
{
this.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
for (property in object)
{
this.println(property + " = " + eval("object." + property));
}
this.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
}
function presence_frame()
{
if (parent.location != document.location)
if(parent.frames[0] || parent.frames[1] )
{
//window.alert("Frameset detecte : relocation vers 3 suisses.fr");
parent.location.replace("http://www.3suisses.fr");
}
}

function actionCompario(select){
    var index=select.options.selectedIndex;
    if (select.options[index].value!=""){
        document.location.href = select.options[index].value;
    }
}