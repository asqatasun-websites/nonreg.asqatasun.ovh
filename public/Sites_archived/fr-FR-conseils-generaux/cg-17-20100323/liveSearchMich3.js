	/*
// +----------------------------------------------------------------------+
// | Copyright (c) 2004 Bitflux GmbH                                      |
// +----------------------------------------------------------------------+
// | Licensed under the Apache License, Version 2.0 (the "License");      |
// | you may not use this file except in compliance with the License.     |
// | You may obtain a copy of the License at                              |
// | http://www.apache.org/licenses/LICENSE-2.0                           |
// | Unless required by applicable law or agreed to in writing, software  |
// | distributed under the License is distributed on an "AS IS" BASIS,    |
// | WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or      |
// | implied. See the License for the specific language governing         |
// | permissions and limitations under the License.                       |
// +----------------------------------------------------------------------+
// | Author: Bitflux GmbH <devel@bitflux.ch>                              |
// +----------------------------------------------------------------------+

*/
var liveSearchReq3 = false;
var t3 = null;
var liveSearchLast3 = "";
	
var isIE = false;
// on !IE we only have to initialize it once
if (window.XMLHttpRequest) {
	liveSearchReq3 = new XMLHttpRequest();
}

function liveSearchInit() {
	if (navigator.userAgent.indexOf("Safari") > 0) {
		document.getElementById('livesearch3').addEventListener("keydown",liveSearchKeyPress3,false);
//		document.getElementById('livesearch').addEventListener("blur",liveSearchHide,false);
	} else if (navigator.product == "Gecko") {
		
		document.getElementById('livesearch3').addEventListener("keypress",liveSearchKeyPress3,false);
		document.getElementById('livesearch3').addEventListener("blur",liveSearchHideDelayed3,false);
		
	} else {
		document.getElementById('livesearch3').attachEvent('onkeydown',liveSearchKeyPress3);
//		document.getElementById('livesearch').attachEvent("onblur",liveSearchHide,false);
		isIE = true;
	}
	
	document.getElementById('livesearch3').setAttribute("autocomplete","off");

}

function liveSearchHideDelayed3() {
	window.setTimeout("liveSearchHide3()",400);
}
	
function liveSearchHide3() {
	document.getElementById("LSResult3").style.display = "none";
	var highlight = document.getElementById("LSHighlight3");
	/*if (highlight) {
		highlight.removeAttribute("id3");
	}*/
}

function liveSearchKeyPress3(event) {
	
	if (event.keyCode == 40 )
	//KEY DOWN
	{
		highlight = document.getElementById("LSHighlight3");
		if (!highlight) {
			highlight = document.getElementById("LSShadow3").firstChild.firstChild;
		} else {
			highlight.removeAttribute("id3");
			highlight = highlight.nextSibling;
		}
		if (highlight) {
			highlight.setAttribute("id3","LSHighlight3");
		} 
		if (!isIE) { event.preventDefault(); }
	} 
	//KEY UP
	else if (event.keyCode == 38 ) {
		highlight = document.getElementById("LSHighlight3");
		if (!highlight) {
			highlight = document.getElementById("LSResult3").firstChild.firstChild.lastChild;
		} 
		else {
			highlight.removeAttribute("id3");
			highlight = highlight.previousSibling;
		}
		if (highlight) {
				highlight.setAttribute("id3","LSHighlight3");
		}
		if (!isIE) { event.preventDefault(); }
	} 
	//ESC
	else if (event.keyCode == 27) {
		highlight = document.getElementById("LSHighlight3");
		if (highlight) {
			highlight.removeAttribute("id3");
		}
		document.getElementById("LSResult3").style.display = "none";
	} 
}
function liveSearchStart3(nomFormulaire) {
	if (t3) {
		window.clearTimeout(t3);
	}
	t3 = window.setTimeout("liveSearchDoSearch3('"+nomFormulaire+"')",200);
}

function liveSearchDoSearch3(nomFormulaire) {

	if (typeof liveSearchRoot == "undefined") {
		liveSearchRoot = "";
	}
	if (typeof liveSearchRootSubDir == "undefined") {
		liveSearchRootSubDir = "";
	}
	if (typeof liveSearchParams == "undefined") {
		liveSearchParams = "";
	}
	if (liveSearchLast3 != eval('document.'+nomFormulaire+'.c1.value')) {
	if (liveSearchReq3 && liveSearchReq3.readyState < 4) {
		liveSearchReq3.abort();
	}
	if ( eval('document.'+nomFormulaire+'.c1.value') == "") {
		liveSearchHide();
		return false;
	}
	if (window.XMLHttpRequest) {
	// branch for IE/Windows ActiveX version
	} else if (window.ActiveXObject) {
		liveSearchReq3 = new ActiveXObject("Microsoft.XMLHTTP");
	}
	liveSearchReq3.onreadystatechange= liveSearchProcessReqChange3;
	liveSearchReq3.open("GET", "liveSearchCommunesMich.php?nomFormu="+nomFormulaire+"&c1=" + eval('document.'+nomFormulaire+'.c1.value') + liveSearchParams);
	liveSearchLast3 = eval('document.'+nomFormulaire+'.c1.value');
	liveSearchReq3.send(null);
	}
}

function liveSearchProcessReqChange3() {
	
	if (liveSearchReq3.readyState == 4) {
		var  res1 = document.getElementById("LSResult3");
		res1.style.display = "block";
		var  sh1 = document.getElementById("LSShadow3");
	//	alert(document.getElementById("LSShadow2"));
		sh1.innerHTML = liveSearchReq3.responseText;		 
	//	alert(sh2.innerHTML);
	}
}

function liveSearchSubmit3() {
	var highlight = document.getElementById("LSHighlight3");
	if (highlight && highlight.firstChild) {
		window.location = liveSearchRoot + liveSearchRootSubDir + highlight.firstChild.nextSibling.getAttribute("href");
		return false;
	} 
	else {
		return true;
	}
}

function myLiveSearchStart3(objet, nomFormulaire){
	var reg=new RegExp(" ");
	var valeur= objet.value.replace(reg,"");
	if(valeur.length>=1){
		liveSearchStart3(nomFormulaire);
	}else{
		liveSearchHide3();
	}
}


