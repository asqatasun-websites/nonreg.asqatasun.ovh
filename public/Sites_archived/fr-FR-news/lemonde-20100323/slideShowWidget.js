// Classe de manipulation des slide show en "fade"
// @version       $Revision: 42173 $
// @date          $Date: 2009-09-14 16:02:29 +0200 (Mon, 14 Sep 2009) $
//
slideShowWidget = function(slide_show_html_elem)
{
   this.nbElem = 0;
   this.curNumElem = 0;
   this.listElem = new Array();
   this.slideShowHtmlElem = slide_show_html_elem;
   this.loopFlag = 0;
   this.dispoFlag = 1;
   /**
    * L ajout d element HTML 
    */                         
   this.addHtmlElem = function(html_elem) 
   {
      html_elem.style.position = "absolute";
      html_elem.style.left = "0px";
      html_elem.style.right = "0px";
      html_elem.style.display = "none";
      
      this.slideShowHtmlElem.appendChild(html_elem);
      this.listElem[this.nbElem] = html_elem;
      this.nbElem++;           
   }
   
   this.activateLoop = function()
   {
      this.loopFlag = 1;
   }
   
   
   this.fadePrev = function ()
   {
      if (this.dispoFlag == 1)
      {
         this.curNumElem--;
         if ((this.curNumElem >= 0) && (this.curNumElem < this.nbElem))
         {
            this._fadeDown(this.curNumElem + 1, 100); 
            this._fadeUp(this.curNumElem, 0);
         }
         else
         {
            if (this.loopFlag == 1)
            {
               this._fadeDown(0, 100); 
               this._fadeUp(this.nbElem - 1, 0);
               this.curNumElem = this.nbElem - 1;                         
            }
            else
            {
               this.curNumElem++;
            }
         }
      }
   }
   
   this.fadeNext = function (num)
   {
      if (this.dispoFlag == 1)
      {
         this.curNumElem++;
         if ((this.curNumElem > 0) && (this.curNumElem < this.nbElem))
         {
            this._fadeDown(this.curNumElem - 1, 100); 
            this._fadeUp(this.curNumElem, 0);
         }
         else
         {
            if (this.loopFlag == 1)
            {
               this._fadeDown(this.nbElem - 1, 100); 
               this._fadeUp(0, 0);
               this.curNumElem = 0;                         
            }
            else
            {
               this.curNumElem--;
            }
            
         }
      }
   }
   
   this.showFirstElem = function()
   {
      this._fadeUp(0, 0);
   }
   
   this.showRandomFirstElem = function()
   {
      var randNum = Math.floor(Math.random() * (this.nbElem));
      this._fadeUp(randNum, 0);
      this.curNumElem = randNum;
      return(randNum);
   }
   
   this.showFirstElemWithNum = function(randNum)
   {
      this._fadeUp(randNum, 0);
      this.curNumElem = randNum;
   }
   
   
   /*
    * Method privee
    */             
   this._fadeUp = function(num, val)
   {
      var html_elem = this.listElem[num];
      if (typeof html_elem != 'undefined')
      {
         this.dispoFlag = 0;
         if (val == 0)
         {  
            html_elem.style.display = "block";
            //this.slideShowHtmlElem.style.height = html_elem.clientHeight;
         }
         var agent = navigator.userAgent.toLowerCase();
         if ((navigator.appName.indexOf("Netscape") != -1) && (parseInt(navigator.appVersion) >= 5))
         {
            html_elem.style.MozOpacity = val / 100;
         }
         else if ((!(agent.indexOf("opera") != -1)) && (navigator.appName.indexOf("Microsoft") != -1) && (parseInt(navigator.appVersion) >= 4))
         {
            html_elem.style.filter= "alpha(opacity=" + val + ")"; 
            //html_elem.filters.alpha.opacity = val;
         }
         else
         {
            html_elem.style.opacity = val / 100;
         }
         if (val < 100)
         {
            var closure_this = this;
            var closure_num = num;
            var closure_val = val;
            
            setTimeout( function () 
                        { 
                           closure_this._fadeUp(closure_num , (closure_val + 20)); 
                        }, 
                        100 );
         }
         else
         {
            this.dispoFlag = 1;
         }
      }
   }
   
   /*
    * Method privee
    */  
   this._fadeDown = function(num, val)
   {
      this.dispoFlag = 0;
      var html_elem = this.listElem[num];
      
      if (typeof html_elem != 'undefined')
      {
         var agent = navigator.userAgent.toLowerCase();
         if ((navigator.appName.indexOf("Netscape") != -1) && (parseInt(navigator.appVersion) >= 5))
         {
            html_elem.style.MozOpacity = val / 100;
         }
         else if ((!(agent.indexOf("opera") != -1)) && navigator.appName.indexOf("Microsoft")!=-1&&parseInt(navigator.appVersion)>=4)
         {
            html_elem.style.filter= "alpha(opacity=" + val + ")";
            //html_elem.filters.alpha.opacity = val;
         }
         else
         {
            html_elem.style.opacity = val / 100;
         }
         if (val > 0)
         {
            var closure_this = this;
            var closure_num = num;
            var closure_val = val;
            
            setTimeout( function () 
                        {
                           closure_this._fadeDown(closure_num, (closure_val - 20));
                        }, 
                        100 );
         }
         else
         {
            this.dispoFlag = 1;
            html_elem.style.display = "none";
         }
      }
   }
}


//Recommanded Version
//vrai slideShow
simpleSlideShowWidget = function ()
{
   this._nbElem = 0;
   this._curNumElem = 0;
   this._listDomElement = new Array();
   this._sliderDiv = null;
   this._movableDiv = null;
   this._movableDivLeft = 0;
   this._increment = 1;
   this._movableDivWidth = 0;
   this._prevImpossibleCallback = null;
   this._prevPossibleCallback = null;
   this._nextImpossibleCallback = null;
   this._nextPossibleCallback = null;
   
   this.setSliderDiv = function (dom_elem)
   {
      this._sliderDiv = dom_elem;
      
      this._sliderDiv.style.position = "relative";
      this._sliderDiv.style.overflow = "hidden";
      
      this._movableDiv = document.createElement('div');

      this._movableDiv.style.position="absolute";
      this._movableDiv.style.top="0px";
      this._movableDiv.style.left="0px";
      
      this._movableDiv.style.height = this._sliderDiv.clientHeight + 'px';
      
      this._sliderDiv.appendChild(this._movableDiv);
   }
   
   this.setNbElem = function (nb_elem)
   {
      this.nbElem = nb_elem;
   }
   
   this.addDomElement = function(dom_elem)
   {
      this._listDomElement[this._nbElem] = dom_elem;
      this._nbElem++;
      this._movableDiv.appendChild(dom_elem);
      
      this._movableDivWidth += dom_elem.clientWidth;
      this._movableDiv.style.width = this._movableDivWidth + 'px';
   }
      
   this.setIncrement = function(increment)
   {
      this._increment = increment;
   }
   
   this.setCallbackPrevImpossible = function(func_elem)
   {
      this._prevImpossibleCallback = func_elem;
   }
   
   this.setCallbackPrevPossible = function(func_elem)
   {
      this._prevPossibleCallback = func_elem;
   }
   
   this.setCallbackNextImpossible = function(func_elem)
   {
      this._nextImpossibleCallback = func_elem;
   }
   
   this.setCallbackNextPossible = function(func_elem)
   {
      this._nextPossibleCallback = func_elem;
   }
   
   this.currentPosition = function()
   {
      return this._curNumElem;
   }
   
   this.move = function(num_element)
   {
      if ( num_element > this._curNumElem )
         this.moveNext(num_element - this._curNumElem);
      else if ( num_element < this._curNumElem)
         this.movePrev(this._curNumElem - num_element);
   }
   
   this.movePrev = function()
   {
      if ( typeof nb_increment == 'undefined' )
         nb_increment = 1;
         
      if (this._curNumElem > 0)
      {
         var deplacement = 0;
         var tmp_cur_num_elem = this._curNumElem;
         var tmp_increment = this._increment;
         
         if (this._curNumElem % this._increment)
         {
            tmp_increment = this._curNumElem % this._increment;
         }
        
         for ( var ct = (tmp_cur_num_elem - 1); 
               ct >= (tmp_cur_num_elem - tmp_increment); ct--)
         {
            deplacement += this._listDomElement[ct].clientWidth;
            this._curNumElem--;
         }
         this._movableDivLeft += deplacement;
         
         $(this._movableDiv).animate({left: this._movableDivLeft}, 500);
      }
      this.checkMoveState();
   }
   
   this.moveNext = function(nb_increment)
   {
      if ( typeof nb_increment == 'undefined' )
         nb_increment = 1;
         
      if (this._curNumElem + (this._increment * nb_increment) < this._nbElem)
      {
         var deplacement = 0;
         var tmp_cur_num_elem = this._curNumElem;
         for (var ct = tmp_cur_num_elem + this._increment; (ct <  tmp_cur_num_elem + (this._increment * (nb_increment + 1))) && (ct < this._nbElem); ct++)
         {
            deplacement += this._listDomElement[ct].clientWidth;
            this._curNumElem++;
         }
         this._movableDivLeft -= deplacement;
         
         $(this._movableDiv).animate({left: this._movableDivLeft}, 500);
      }
      
      this.checkMoveState();
   }
   
   this.checkMoveState = function()
   {
      //check "next" move
      if (this._curNumElem + this._increment < this._nbElem)
      {
         if (this._nextImpossibleCallback)
            this._nextPossibleCallback();
      }
      else
      {
         if (this._nextImpossibleCallback)
            this._nextImpossibleCallback();
      }
      
      //check "prev" move
      if (this._curNumElem > 0)
      {
         if (this._prevImpossibleCallback)
            this._prevPossibleCallback();
      }
      else
      {
         if (this._prevImpossibleCallback)
            this._prevImpossibleCallback();
      }
   }
}