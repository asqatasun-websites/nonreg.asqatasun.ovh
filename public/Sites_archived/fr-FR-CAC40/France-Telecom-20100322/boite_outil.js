//Alexis 080214 : Fondu du texte de r�ponse du glossaire lorsque le message d'erreur s'affiche
var fade_glossaire;

var transition_finie = true;

//compte des boites d'�dition ouvertes pour que lorsqu'elle sont toute �t� ferm�es, les typographies
//SIFR soient r�activ�es
var boites_edit_ouvertes = 0;
 
/************* Les classes des boites outils de navigation horizontale sur la home page et de la barre de droite dans les pages internes ************/
var docCenter = { 
	name : 'DocCenter',
	lien : config_docs.link_xml,
	nameDiv : 'conteneur_global_doccenter', 
	bool_first :0,
	config : config_docs,
	cookie_first_time:0
}

var myMag = {
	name :'MyMag',
	lien : config_mymag.link_xml,
	nameDiv : 'conteneur_global_myMag',
	bool_first :0,
	config : config_mymag,
	cookie_first_time:0
}

var glossary = {
	name :'Glossary',
	lien : config_glossary.link_xml,
	nameDiv : 'conteneur_global_glossary',
	config : config_glossary
}
	
var bookmarks = {
	name :'Bookmarks',
	nameDiv : 'conteneur_global_bookmarks_short',
	config : config_bookmarks
}




var tab_link = config_calendar.link_xml.split(',')
var calendrier = {
	name : 'Calendar',
	lien : tab_link[0],
	nameDiv : 'conteneur_global_calendar',
	laTab : [],
	laTabAdd : function(val){ this.laTab.push(val)},
	laTabEmpty : function(){ this.laTab = []},
	laTabBool : 'vide_le',
	bool_first :0,
	config : config_calendar,
	id_event_long :'', // sers pour l ouil calendrier long
	first_event : 0,
	cookie_first_time : 0 
}
	
var xml_press = config_press.link_xml.split(',');
	
var press_releases = {
	name :'Press releases',
	lien : xml_press[1],
	nameDiv : 'conteneur_global_Cp',
	bool_first :0,
	config : config_press,
	cookie_first_time : 0 
}

var products = {
	name :'Product and Services',
	nameDiv : 'conteneur_global_products',
	config : config_products
}


var highlight = {
	name :'Hightlight',
	nameDiv :'outil_highlight',
	config : config_hightlights
}

var alaune = {
	name :'A la Une',
	nameDiv :'outil_alaune',
	config : config_alaune
}
var bubble = {
	name :'Bubble Planet',
	nameDiv :'outil_bubble',
	config : config_bubble
}

	
var news = {
	name :'News',
	nameDiv :'outil_news',
	config : config_news
}

var jobs = {
	name :'Job Board',
	lien : config_jobs.link_xml,
	nameDiv :'outil_job',
	config : config_jobs
}

var share = {
	name :'Share Price',
	nameDiv : 'outil_share',
	config : config_share
}

var media  = {
	name :'media center',
	nameDiv : 'outil_media',
	config : config_media
}
	
var flash = {
	name :'flash card',
	lien : config_flash.link_xml,
	nameDiv :'outil_flash',
	config :config_flash
}

var poll = {
	name: 'sondages',
	lien: config_poll.link_xml,
	id_sondage: config_poll.libelle_defaut,
	nameDiv: 'outil_poll',
	nameDivInterne: 'outil_poll_int',
	nameDivMoodal: 'outil_poll_moodal',
	config: config_poll
}

var sites = {
	name: 'sites',
	nameDiv: 'outil_sites',
	nameDivInterne: 'outil_sites_int',
	nameDivMoodal: 'outil_sites_moodal',
	config : ''/*config_sites*/
}

//alert("load boite outil");
var BoiteOutil = new Class({
//Classe g�rant le comportement d'une boite outil qui peut �tre un calendrier,
//une base de documents, etc.
    

	// initialisation de la boite
	initialize: function(id_boite, url_boite, config_boite){
		this.id_boite = id_boite;
		if (config_boite) {
			this.libelle = config_boite.libelle;
	        this.id_outil = config_boite.id_outil;
	        this.suf_boite = this.id_outil.substr(this.id_outil.indexOf("_")+1).toLowerCase();;
	        this.name_link = config_boite.name_link;
	        this.the_link = config_boite.the_link;
	        this.edit = config_boite.edit;
	        this.libelle_defaut = config_boite.libelle_defaut;
	        
	        this.link_xml = config_boite.link_xml;
	        if (this.link_xml.indexOf(",")) {
				var tab_link = this.link_xml.split(',')
				this.link_xml = tab_link[0];
			}
			this.swf_large = config_boite.swf_large;
		}

        this.tag_xml = '';
       
        this.chaine_choix = "";
        this.cookie_first_time = 0;
        this.nom_cookie="";
        this.url_boite = url_boite;
        
        this.form_edit=0;
        
        var boite = this;
        
        var div_boite = $(this.id_boite).getFirst().getNext();

		var _self = this;
		//console.log(boite.url_boite);
		var ajax = new Ajax(encodeURI(boite.url_boite),{
			method : 'get',
			encoding: 'utf-8',
			onRequest : function(){
				setBoxLoader(_self.id_boite);
			},
			
			onSuccess : function(retour){
				div_boite.innerHTML = retour;
				
	            //Une fois que la structure html est charg�e, on peut lancer l'initialisation de son contenu
	            //boite.affiche_contenu();
				
			 	(function(){
					boite.affiche_contenu();
				}.delay(100));
				
				
				
				
				boite.correct_png_background_div(id_boite);
				unsetBoxLoader(_self.id_boite);
			},
			onComplete : function(){}
			
		}).request();
	},
	/**
	 * Correction du fond bleu sur les images de fond uniquement sur cette boite.
	 * Elle s'effectue au chargement ajax de la boite.
	 */
	correct_png_background_div : function(id) {
		if(document.all) {
			var images_list=$(id).getElements('div');
			//alert(images_list.join(','));
			for(var i=0;i<images_list.length;i++) {
				var image_name=images_list[i].currentStyle.backgroundImage.replace(/url[s]*()+/,'');
				
				//alert(images_list[i].currentStyle.backgroundImage);
				
				image_name=image_name.replace(/(")+/g,'');
				image_name=image_name.substr(1,image_name.length-1);
				image_name=image_name.substr(0,image_name.length-1);
				if(image_name.substring(image_name.length-3,image_name.length)=='png'){
					images_list[i].runtimeStyle.backgroundImage="url( '"+base+"/images/blank.gif' )";
					images_list[i].runtimeStyle.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader( src='"+image_name+"', sizingMethod='scale' )";
				}
			}
		}
		return true;
	},
	
	/**
	 * Par d�faut, les boites n'ont pas de contenu, c'est par d�rivation de la classe
	 * que l'on pourra sp�cifier les traitements d'affichage du contenu
	 */
	affiche_contenu : function() {
		this.initBoite();
	},
	
	initBoite : function ()
	{
		/**
		 * Initialisation des attributs html communs des boites : titre de la boite, lien "plus d'infos" et lien vers la boite d'�dition
		 * Si le lien existe, on cr�e �galement la boite d'�dition selon le format pr�cis�.
		 */
		if(this.libelle && this.libelle!= "")
		{
			var id_libelle = 'libelle_'+this.id_outil;
			$(id_libelle).setHTML(this.libelle);
		}
		
		if(this.name_link && this.name_link != "")
		{
			var id_more = 'more_'+this.id_outil;
			
			if($(id_more) != false)
			{
				if($(id_more) != null) {
					if(this.the_link == "#") {
						$(id_more).setProperty('href',this.the_link+this.id_outil);
					} else {
						$(id_more).setProperty('href',this.the_link);
					}
					
					$(id_more).setHTML(this.name_link);
				}
			}
		}
		
		if(this.edit && this.edit != "" && this.id_outil != "outil_sites")
		{
			var id_edit = 'edit_'+this.id_outil;
			var link_id_edit = $(id_edit).firstChild.attributes.getNamedItem('id').nodeValue;
			$(id_edit).empty();
			var elem_a = new Element('a',{'href':'javascript:void(0);','id':link_id_edit});
			elem_a.setHTML(this.edit);
			$(id_edit).appendChild(elem_a);
		}
	},
	
	getThemesFromCookie : function(style) {
		this.chaine_choix = "all";
		//alert(Cookie.get(tools['sites_element']));
		//Si un filtre de th�mes a �t� d�finit dans la page html, il a la priorit� 
		if (typeof(this.filtre_page) != 'undefined' && this.filtre_page !='') {
			this.chaine_choix = this.filtre_page;
		//Sinon, un filtre a peut �tre �t� d�finit dans le profil du type d'utilisateur courant
		} else if(typeof(this.filtre_profil) != 'undefined' && this.filtre_profil !='' ) {
			this.cookie_first_time = 1;
			this.chaine_choix = this.filtre_profil;
		//Sinon, on r�cup�re les th�mes coch�s lors de la derni�re visite
		} else if(Cookie.get(getEndString('/', this.nom_cookie)) ) {
			var cookie_filtre = String(getValueCookie( getEndString('/',this.nom_cookie),4));
			
			if(cookie_filtre != 'undefined' && cookie_filtre != "")
			{
				/**
				 * Selon le type de formulaire de th�mes, le format des donn�es
				 * attendu n'est pas le m�me
			     */
				if (style=="checkbox") {
					this.cookie_first_time = 1;
					var	tab_chaine_cookie = getValueCookie( getEndString('/',this.nom_cookie),4);
					
					//alert(tab_chaine_cookie.join(','));
					
					if (tab_chaine_cookie != null && tab_chaine_cookie !='' ) {
						//alert("tab_chaine_cookie.length = "+tab_chaine_cookie.length);
						//alert("cases_themes.length+1 = "+(cases_themes.length+1));
						this.chaine_choix=tab_chaine_cookie.join(",");
					}
				} else if (style=="select"){
					this.cookie_first_time = 1;
					this.chaine_choix = getValueCookie(getEndString('/',this.nom_cookie),4)[0];
					//alert(this.chaine_choix);
				}
				
			} else if(cookie_filtre == 'undefined' && typeof(this.filtre_profil) != 'undefined' && this.filtre_profil =='' && this.cookie_first_time ==1) {
				this.chaine_choix="all";
			} else {
				//Sinon, il n'y a ni for�age, ni profil, ni cookies, on r�cup�re les th�mes pr�cis�s dans la configuration
				//de la boite
				if(this.libelle_defaut !="" && this.cookie_first_time ==0){	
					this.cookie_first_time = 1;
				} else {
					this.chaine_choix="all";
				}
			}
		}
	},
	
	/**
	 * - Initialisation de la boite de filtrage des �v�nements du calendrier
	 * et des th�mes choisis (cases coch�es et option s�lectionn�e)
	 * - Initialisation des �l�ments xml � afficher appartenant aux th�mes choisis
	 */
	init_data_xml : function(style) {
		var tag_choix = "";
		var boite = this;
		
		//Cr�ation de l'animation faisant appara�tre la boite d'�dition
		var div_doc = new Fx.Styles("conteneur_global_"+this.suf_boite, {duration:300, wait:false});
		$('link_edit_'+this.suf_boite).addEvent('click', function(){
			boites_edit_ouvertes++;
			
			div_doc.start({'opacity':0.8});
			$('edit_boite_'+boite.suf_boite).setStyle('display', 'block' );
		});
		
		/**
		 * Requ�te XML vers la liste de th�mes de tri des donn�es
		 */
		
		var lien_xml = this.link_xml;		

		if (this.link_xml.indexOf(",")) {
			var tab_link = this.link_xml.split(',')
			lien_xml = tab_link[0];
		}
		var liste_themes = $("liste_themes_"+this.suf_boite);
		
		var index=0;
		while(index < liste_themes.childNodes.length  )
		{
			liste_themes.removeChild(liste_themes.childNodes[index]);
		}
		
		//var form_choix = $('fom_edit_'+this.suf_boite);
		
		/**
		 * Selon le type de boite edit (cases � cocher ou s�lection)
		 */
		if (style=='select') {	
			var form_choix = new Element('select', {'id' : 'form_edit_'+this.suf_boite, 'class':'form_edit_short'});
			var elem_option = new Element('option',{'id':'tout_'+this.suf_boite,'value':'all'});
			elem_option.setHTML(libelle_all_press);
			form_choix.appendChild(elem_option);
		} else {
			var form_choix = new Element('div', {'id' : 'form_edit_'+this.suf_boite});
			var case_choix_theme = new Element('div', {'class':'case_choix_theme'});
			var case_all = new Element('input', {'id':'tout_'+this.suf_boite, 'type':'checkbox','class':'check_calendar_all', 'name':'tout'});
			var label_all = new Element('label', {'for':'tout'});
			label_all.setHTML(libelle_all_calendar);
			case_choix_theme.appendChild(case_all);
			case_choix_theme.appendChild(label_all);
			form_choix.appendChild(case_choix_theme);
		}
		liste_themes.appendChild(form_choix);
		boite.getXMLThemesAndItems(form_choix, lien_xml);
		
		$('valid_go_'+this.suf_boite).addEvent('click',function(){boites_edit_ouvertes--; boite.bool_first=1,boite.update_data_xml(false);});
	},
	
	/** 
	 * Comportement des cases � cocher de la boite d'�dition lorsque l'on en coche qu'une alors que 'tout' est coch�
	 */
	selectOne : function() {
		var case_tout = $('tout_'+this.suf_boite);
		
		$$('.choix_theme_'+this.suf_boite).each(function(item,index){
			item.addEvent('click',function(){
				if(case_tout.checked=true)
				{	
					case_tout.checked=false;
				}
			});
		});
	},
	selectall : function(cas,choix) {
		  //test si on a plusieur ligne
		  if(choix.length>0){
		    if (cas.checked){
		      for (var i=0; i<choix.length;i++){
		        choix[i].checked=true;
		      }
		    }else{
		      for (var i=0; i<choix.length;i++){
		        choix[i].checked=false;
		      }
		    }
		  }
		  else{
		    if (cas.checked){
		      choix.checked=true;
		    }
		    else{
		      choix.checked=false;
		    }
		  }
	},
	
	// fonction pour modifier le nombre de parametre et ajout? les parametres dans le cookie
	// utiliser lors d'un edit d'un outil 
	//tab_test = ['theme1'];
	//tab_test1 = ['theme1','theme2','theme3'];
	//setParamCookie('b_press.html',tab_test)
	setParamCookie : function(myCookie,tab_param)
	{
		if(Cookie.get(myCookie))
		{
			trace_cookies = "get cookie "+myCookie+" : "+Cookie.get(myCookie)+"\n";
			
			var tab_cookie = getTab_cookie(Cookie.get(myCookie));
			var nbparam = tab_param.length;
			tab_cookie[3] = nbparam;
			var tab_copy =$A([]);
			tab_cookie.each(function(item,index){
				//r�cup�ration des quatre premiers items uniquement (num�ro, x, y, nombre de param�tres)
				//pour leur ajouter les nouveaux param�tres � la suite
				if (index <= 3) {
					tab_copy.include(item);
				}
			 });
			tab_cookie = tab_copy;
			var chaine='';
			
			
			trace_cookies+="tab_cookie new = "+tab_param.join(', ')+"\n";
			
			tab_param.each(function(item,index){
				 tab_cookie.include(item);
			});
			
			
			
			tab_cookie.each(function(item2,index2){
				 if(item2 !='')
				 {
					chaine += item2+'@';
				 }
			});
			Cookie.remove(myCookie);
			
			trace_cookies += "set cookie "+myCookie+" : "+chaine+"\n";
			Cookie.set(myCookie,chaine, {duration:365,path:"/"});
			//alert(trace_cookies);
			
		} else {
			alert("error : le cookie n'existe pas")
		}
	},
	/**
	 * R�cup�ration classique des �l�ments XML � charger dans une boite
	 * Le principe s'appuie sur une structure de th�mes ou autres balises qui contiennent chacun
	 * un groupe d'�l�ments (articles, �v�nements,etc.)
	 */
	getXMLThemesAndItems : function(form_choix, lien_xml) {
		
		var boite = this;

		var cases = 0;
		
		var tab_choix = this.chaine_choix.split(',');
		
		
		var ajax = new Ajax(lien_xml,{
			method : 'get',
			onSuccess : function(themes, xml_themes){
				for (var i=0; i<xml_themes.getElementsByTagName('topic').length ;i++){
					var theme = xml_themes.getElementsByTagName('topic')[i];

					var id_theme = theme.getAttribute('id');
					var nom_theme = theme.getAttribute('name');
					if (boite.edit_style=='select') {
						var option = document.createElement("option");
						option.setAttribute("id",id_theme);
						option.setAttribute("value",nom_theme);
						option.setAttribute("class","choix_theme_"+boite.suf_boite);
						option.innerHTML= nom_theme;
						form_choix.appendChild(option);
					} else if(boite.edit_style=='checkbox'){
						var case_choix_theme = new Element('div', {'class':'case_choix_theme'});
						var case_choix = new Element('input', {'id':"theme"+id_theme, 'value':nom_theme, 'type':'checkbox','class':"choix_theme_"+boite.suf_boite, 'name':'choix'});
						var label_choix = new Element('label', {'id':'label_theme'+id_theme, 'for':"theme"+id_theme});
						label_choix.setHTML(nom_theme);
						case_choix_theme.appendChild(case_choix);
						case_choix_theme.appendChild(label_choix);
						form_choix.appendChild(case_choix_theme);

						//Lorsque l'on clique une case alors que la case tout est active, elle est d�coch�e
						case_choix.addEvent('click',function(){
							if(case_all.checked=true)
							{
								case_all.checked=false;
							}
						});
					}

					/**
					 * Si le th�me fait partie des th�mes coch� pr�c�demment, il est coch� � sa cr�ation
					 * et tous les �v�nements qu'il contient sont ins�r�s dans le calendrier
					 */
					if (tab_choix.contains(nom_theme)) {
						if (boite.edit_style=='select') {
							option.selected=true;
						} else if(boite.edit_style=='checkbox'){
							case_choix.checked=true;
						}
						boite.affiche_results_xml(theme);
					} else if (boite.chaine_choix == 'all' && boite.edit_style=='select') {
						$('tout_'+boite.suf_boite).selected = 'true';
						boite.affiche_results_xml(theme);
					}

					if(boite.edit_style=='checkbox'){
						var case_all = $('tout_'+boite.suf_boite);
						/**
						 * Lorsqu'on clique sur une des cases et que la case tout est coch�e, elle est d�coch�e automatiquement
						 */
						case_choix.addEvent('click',function(){
							if(case_all.checked=true)
							{
								case_all.checked=false;
							}
						});
					}

					cases++;
				}
				
				
				/**
				 * Gestion du cas o� tous les th�mes sont choisis en prenant en compte le type de liste de choix
				 */	
				if(boite.edit_style=='checkbox'){
					if (tab_choix.length >= cases) {$('tout_'+boite.suf_boite).checked="true"}
					//Au clic sur la case tout du formulaire de choix du th�me
					//
					boite.form_edit = document.getElementsByName("myformu_"+boite.suf_boite)[0];
					$('tout_'+boite.suf_boite).addEvent('click',function(){boite.selectall(boite.form_edit.tout,boite.form_edit.choix)});
				}
				if (boite.chaine_choix == 'all' && boite.edit_style=='select') {
					$('tout_'+boite.suf_boite).selected = 'true';
				}
			}
		}).request();
	
	},
	
	//071107 Alexis : Fonction de r?cup?ration de la hauteur de div correspondant au nombre de caract?res ? y ins?rer LA LIMITE EST DE DEUX LIGNES
	getHauteurDivForChars : function(nb_chars) {
		if (nb_chars <= vNbCharsByLine) {
			return 15;
		} else if (nb_chars > vNbCharsByLine) {
			return 30;
		}
	},

	getShorterString : function(chaine) {
		var new_length = vNbCharsByLine*2;
		if (chaine.length > new_length) {
			//alert ("chaine ? d?couper = "+chaine);
			//Si le d?coupage risque de se faire sur un mot, on se place avant lui et on ajoute des points de suspension
			while(chaine.charAt(new_length) != " ") {
				new_length--;
			}
			chaine = chaine.substr(0, new_length);
			chaine+="...";
		}
		return chaine;
	}
	
});

var BoiteCalendrier = BoiteOutil.extend({
	
	initialize : function (id_boite, url_boite, config_boite,event_display, edit_style){
		
		this.parent(id_boite, url_boite, config_boite);
		
		//Pr�cise si les �v�nements d'un jour lorsqu'il est cliqu� s'affichent
		//Dans la boite elle m�me (self) ou dans la page calendrier interne (blank)
		//Le 'calendrier long' des pages internes du site affiche par exemple les �v�nements
		//en mode self et son lien 'en savoir plus' change de page
		this.event_display = event_display;
		
		//Mise en forme du formulaire de choix du (des) th�me(s) de filtrage (select box, checkboxes,etc.)
		this.edit_style = edit_style;
		
		this.tag_xml = 'theme';
		
		this.bool_first = 0;
		
		//Insertion du calendrier dans la boite
		//this.calendrier = new Calendrier();
		//this.calendrier.displayCalendar('w0');
		
		var boite = this;
		
		this.first_event = 0;
		
		//Nom sous lequel sont stock�s coordonn�es et valeurs choisies dans la boite de choix du th�me
		this.nom_cookie = tools['calendar_element'];
		this.parent.nom_cookie = this.nom_cookie;
		
		this.filtre_page="";
		this.filtre_profil="";
		
		//Initialisation des diff�rentes variables contenant �ventuellement des filtres, le nom change en fonction
		//du type de boite
		if(typeof(outil_calendar_new_filtre)!= 'undefined') {
			this.filtre_page = outil_calendar_new_filtre;
		}
		if(typeof(home_filtre_outil_calendar)!= 'undefined') {
			this.filtre_profil = home_filtre_outil_calendar;
		}
		

	},
	
	affiche_contenu : function() {
	 	var boite = this;
	 	
	 	
	 	boite.initBoite();
	    boite.calendrier = new Calendrier();
		boite.calendrier.displayCalendar('w0');
		if(boite.first_event == 0)
		{
			boite.first_event++;
			$('cal_m_01').addEvent('click',function(e){
				new Event(e).stop();
				setBoxLoader(boite.id_boite);
				(function(){
					boite.incrementTheMonth();
				}).delay(500);
			});
			$('cal_m_00').addEvent('click',function(e){
				new Event(e).stop();
				setBoxLoader(boite.id_boite);
				(function(){
					boite.decrementTheMonth();
				}).delay(500);
			});
		}
		/**
		 * R�cup�ration du tableau des th�mes s�lectionn�s et stock�s dans les cookies ou dans les filtres existants
		 */
		boite.getThemesFromCookie(boite.edit_style);
		/**
		 * Chargement de la boite des th�mes de tri et des �l�ments xml � afficher
		 */
		boite.init_data_xml(boite.edit_style);
	},
	
	getXMLThemesAndItems : function(form_choix, lien_xml) {
		
		var boite = this;

		var cases = 0;
		boite.registeredXML = false;
		
		var tab_choix = this.chaine_choix.split(',');
		
		var ajax = new Ajax(lien_xml,{
			method : 'get',
			onSuccess : function(themes, xml_themes){
				//on enregistre le flux
				boite.registeredXML = xml_themes;
				for (var i=0; i<xml_themes.getElementsByTagName('theme').length ;i++){
					var theme = xml_themes.getElementsByTagName('theme')[i];
					var id_theme = theme.getAttribute("id");
					var nom_theme = theme.getAttribute("name");

					if (boite.edit_style=='select') {
						var option = document.createElement("option");
						option.setAttribute("id",id_theme);
						option.setAttribute("value",nom_theme);
						option.setAttribute("class","choix_theme_"+boite.suf_boite);
						option.innerHTML= nom_theme;
						form_choix.appendChild(option);
					} else if(boite.edit_style=='checkbox'){
						var case_choix_theme = new Element('div', {'class':'case_choix_theme'});
						var case_choix = new Element('input', {'id':"theme"+id_theme, 'value':nom_theme, 'type':'checkbox','class':"choix_theme_"+boite.suf_boite, 'name':'choix'});
						var label_choix = new Element('label', {'id':'label_theme'+id_theme, 'for':"theme"+id_theme});
						label_choix.setHTML(nom_theme);
						case_choix_theme.appendChild(case_choix);
						case_choix_theme.appendChild(label_choix);
						form_choix.appendChild(case_choix_theme);

						//Lorsque l'on clique une case alors que la case tout est active, elle est d�coch�e
						case_choix.addEvent('click',function(){
							if(case_all.checked=true)
							{
								case_all.checked=false;
							}
						});
					}
					//alert("theme "+nom_theme+" contenu dans "+tab_choix.join(',')+" ??");

					/**
					 * Si le th�me fait partie des th�mes coch� pr�c�demment, il est coch� � sa cr�ation
					 * et tous les �v�nements qu'il contient sont ins�r�s dans le calendrier
					 */
					if (tab_choix.contains(nom_theme)) {
						if (boite.edit_style=='select') {
							option.selected=true;
						} else if(boite.edit_style=='checkbox'){
							case_choix.checked=true;
						}
						boite.affiche_results_xml(theme);
					} else if (boite.chaine_choix == 'all') {
						if (boite.edit_style=='select') {
							$('tout_'+boite.suf_boite).selected = 'true';
						}else if(boite.edit_style=='checkbox'){
							$('tout_'+boite.suf_boite).checked = 'true';
							case_choix.checked=true;
						}
						//enregistrement de l'id du site lorsque les pr�c�dents choix de site indiquent 'tous'
						boite.affiche_results_xml(theme);

					}
					
					
					
					if(boite.edit_style=='checkbox'){
						var case_all = $('tout_'+boite.suf_boite);
						/**
						 * Lorsqu'on clique sur une des cases et que la case tout est coch�e, elle est d�coch�e automatiquement
						 */
						case_choix.addEvent('click',function(){
							if(case_all.checked=true)
							{
								case_all.checked=false;
							}
						});
					}
					cases++;
				}
				if(boite.edit_style=='checkbox'){
					if (tab_choix.length >= cases) {$('tout_'+boite.suf_boite).checked="true"}
					//Au clic sur la case tout du formulaire de choix du th�me
					//
					boite.form_edit = document.getElementsByName("myformu_"+boite.suf_boite)[0];
					$('tout_'+boite.suf_boite).addEvent('click',function(){boite.selectall(boite.form_edit.tout,boite.form_edit.choix)});
				}
				boite.loadTips();
			}
			

		}).request();
	},
	
	affiche_results_xml : function(xml_results) {
		var boite = this;
		var events = xml_results.getElementsByTagName('event');
		for (var i=0; i<events.length ;i++){
			var xml_event = events[i];
			var xml_event_title = xml_event.getElementsByTagName('title')[0].firstChild.nodeValue;
			var xml_event_date = xml_event.getElementsByTagName('date')[0].firstChild.nodeValue;
			var laDate = new Date();
			laDate.setTime(xml_event_date+'000');
			
			var jour =  boite.clean_day(getDateString(laDate,3));
			
			var annee = laDate.getFullYear();
			var mois = (laDate.getMonth()+1);
			
			var item = $(jour+"_"+(laDate.getMonth()+1)+"_"+laDate.getFullYear());

			if(item) // meme mois et ann??e
			{
				
				item.className='active';
				item.innerHTML="";
				var baliseA = document.createElement("a");
				
				var chaine = getDateString(laDate,0).split('&nbsp;');
				var nb = Number(obtenirMois_int(chaine[1]));
				
				var lachaine =  chaine[0]+'&nbsp;'+Obenir_mois_fr(nb)+'&nbsp;'+chaine[2];
				//alert("lachaine = "+lachaine);
				baliseA.setAttribute("href",config_calendar.the_link+"?d="+laDate.getTime());
				baliseA.setAttribute("title",lachaine+"::"+ xml_event_title);
				
				// ff
				//baliseA.setAttribute("class","Tips3");
				//ie ...
				//baliseA.setAttribute("className","Tips3");
				baliseA.className = 'Tips3';
		
				
				
				baliseA.innerHTML=jour;
				item.appendChild(baliseA);
				
				item.date = laDate.getTime();
				
				//alert("baliseA.innerHTML"+baliseA.innerHTML);
				//new Tips(baliseA , {className: 'bulle_calendar','fixed':true,'maxTitleChars':50});

				item.addEvent('click',function(){window.location.href = config_calendar.the_link+"?d="+evt.date});
				//$$('.bulle_calendar-tip').remove();
				//new Tips($$('.Tips3'), {className: 'bulle_calendar','fixed':true,'maxTitleChars':50});
				//boite.loadTips;
			}
		}
		
		
		
	},
	
	loadTips : function() {
		this.tips = new Tips($$('.Tips3'), {className: 'bulle_calendar','fixed':true,'maxTitleChars':50});
	},
	
	update_data_xml : function(init) {
		var boite = this;
		
		boite.tips_loaded = false;
		
		var lien_xml = this.link_xml;		

		if (this.link_xml.indexOf(",")) {
			var tab_link = this.link_xml.split(',')
			lien_xml = tab_link[0];
		}
		
		if ($$('.active').length > 0) {
			this.calendrier.fillCalendar();
		}
		
		var themes_selected = [];
		
		if (init) {
			themes_selected = this.chaine_choix.split(',');
		} else {
			themes_selected = []; //cr??ation tableau pr stock?? les check coch??
			if (boite.edit_style=='checkbox') {
				
				$$('.choix_theme_'+this.suf_boite).each(function(item,index){	
					var bool = item.checked ? themes_selected.push(item.value) : false;
				});
			} else if (boite.edit_style=='select') {
				themes_selected.include($('form_edit_'+this.suf_boite).value);
			}
			//alert("lesEvents dans calendar : "+lesEvents);
			if(Cookie.get(getEndString('/', this.nom_cookie)))
			{
				this.setParamCookie(getEndString('/', this.nom_cookie),themes_selected);
			}
		}
	
		var tag_xml = this.tag_xml;
		boite.themes = [];
		var calendrier = this.calendrier;
		tab_events = [];
		if (!boite.registeredXML){
			var ajax = new Ajax(lien_xml,{
				method : 'get',
				onSuccess : function(themes, xml_themes){
					boite.registeredXML = xml_themes;
					var themes = xml_themes.getElementsByTagName('theme');
					for (var i=0; i<themes.length ;i++){
						var theme = themes[i];
						if(themes_selected.contains(theme.getAttribute("name"))) // Si les cases coch??s existent dans le xml
						{
							boite.affiche_results_xml(theme);
						}
					}
				}


			}).request();
		}else{
			var themes = boite.registeredXML.getElementsByTagName('theme');
			for (var i=0; i<themes.length ;i++){
				var theme = themes[i];
				if(themes_selected.contains(theme.getAttribute("name"))){
					boite.affiche_results_xml(theme);
				}
			}
						
			// au retour dun nouveau chargement XML
			// on vide dabord les bulles
			$$(".bulle_calendar-tip").each(function(elm){
				elm.remove();
			});
			// on les recr�es
			this.tips = new Tips('.Tips3', {className: 'bulle_calendar','fixed':true,'maxTitleChars':50});
			unsetBoxLoader(boite.id_boite);
		}
		
		
		// Pinouf // 16/01/2008 //
		//modif sur laffichage de certaines date du tool Tips
		//var Tips4 = new Tips($$('.Tips3'), {className: 'bulle_calendar','fixed':true,'maxTitleChars':50});
		
		if(this.bool_first != 0)
		{
			var div_doc = new Fx.Styles('conteneur_global_'+this.suf_boite, {duration:750, wait:false});
			if (navigator.appName =="Netscape") div_doc.addEvent('onComplete', function() {if (boites_edit_ouvertes==0) initSIFR();});
			div_doc.start({'opacity':1});
			$('edit_boite_'+this.suf_boite).setStyle( 'display', 'none' );
			this.bool_first = 1;
		}
		
		//alert("time_elapsed = "+time_elapsed);
	},	
	incrementTheMonth : function() {
		this.calendrier.incrementMonth();
		this.update_data_xml(false);
	},
	decrementTheMonth : function() {
		this.calendrier.decrementMonth();
		this.update_data_xml(false);
	},
	//Fonction transformant un jour au format 0x en x afin d'�viter des erreurs de calcul (sous firefox notamment)
	clean_day : function(day)
	{
		var tab =day.split('');
		if(tab[0] == 0)
		{
			return tab[1];
		}
		else 
		{return day;}
	}
	
});
/**
 * Boite des communiqu�s de presse du site : Elle r�cup�re dans un fichier XML des articles
 * Qu'elle affiche en partie dans cette boite. Le reste des articles est affich� au complet dans les pages internes.
 */
var BoitePresse = BoiteOutil.extend({
	
	initialize : function (id_boite, url_boite, config_boite, edit_style){
		
		this.parent(id_boite, url_boite, config_boite);
		
		//Mise en forme du formulaire de choix du (des) th�me(s) de filtrage (select box, checkboxes,etc.)
		this.edit_style = edit_style;
		
		this.tag_xml = 'theme';
		
		this.bool_first = 0;
		
		//Insertion du calendrier dans la boite
		//this.calendrier = new Calendrier();
		//this.calendrier.displayCalendar('w0');
		
		var boite = this;
		
		this.first_event = 0;
		
		//Nom sous lequel sont stock�s coordonn�es et valeurs choisies dans la boite de choix du th�me
		this.nom_cookie = tools['press_releases_element'];
		this.parent.nom_cookie = this.nom_cookie;
		
		this.filtre_page="";
		this.filtre_profil="";
		
		//Initialisation des diff�rentes variables contenant �ventuellement des filtres, le nom change en fonction
		//du type de boite
		if(typeof(outil_press_new_filtre)!= 'undefined') {
			this.filtre_page = outil_press_new_filtre;
		}
		if(typeof(home_filtre_outil_presse)!= 'undefined') {
			this.filtre_profil = home_filtre_outil_presse;
		}
		
	},
	
	affiche_contenu : function() {
	 	var boite = this;
	 	boite.initBoite();
		/**
		 * R�cup�ration du tableau des th�mes s�lectionn�s et stock�s dans les cookies ou dans les filtres existants
		 */
		boite.getThemesFromCookie(boite.edit_style);
		/**
		 * Chargement de la boite des th�mes de tri et des �l�ments xml � afficher
		 */
		boite.init_data_xml(boite.edit_style);
	},
	
	getXMLThemesAndItems : function(form_choix, lien_xml) {
		//console.log(form_choix, lien_xml, this);
		var boite = this;
		//boite.registeredXML = false;
		var cases = 0;
		
		var tab_choix = this.chaine_choix.split(',');
		
		boite.registeredXML = false;
		
		var tab_themes = $A([]);
		
		var tab_articles = $A([]);
		
		var case_all = $('tout_'+boite.suf_boite);
		//console.log(boite.registeredXML)
		/*if (boite.registeredXML){
			boite.affiche_results_xml(registeredXML);
			return;
		}*/
		var ajax = new Ajax(lien_xml,{
			method : 'get',
			onSuccess : function(themes, xml_themes){
				boite.registeredXML = xml_themes;
				themes = xml_themes.getElementsByTagName('theme')
				for (var i=0; i<themes.length ;i++){
					var theme = themes[i];
					var nom_theme = theme.firstChild.nodeValue;
					tab_themes.include(nom_theme);
				}
				/**
				 * Cr�ation de la liste de choix dans la boite d'�dition
				 */
				tab_themes.each(function(nom_theme, ind) {
					if (boite.edit_style=='select') {
						var option = document.createElement("option");
						option.setAttribute("value",nom_theme);
						option.setAttribute("class","choix_theme_"+boite.suf_boite);
						option.innerHTML= nom_theme;
						form_choix.appendChild(option);
					} else if(boite.edit_style=='checkbox') {
						var case_choix_theme = new Element('div', {'class':'case_choix_theme'});
						var case_choix = new Element('input', {'id':"theme"+nom_theme, 'value':nom_theme, 'type':'checkbox','class':"choix_theme_"+boite.suf_boite, 'name':'choix'});
						var label_choix = new Element('label', {'id':'label_theme'+nom_theme, 'for':"theme"+nom_theme});
						label_choix.setHTML(nom_theme);
						case_choix_theme.appendChild(case_choix);
						case_choix_theme.appendChild(label_choix);
						form_choix.appendChild(case_choix_theme);

						//Lorsque l'on clique une case alors que la case tout est active, elle est d�coch�e
						case_choix.addEvent('click',function(){
							if(case_all.checked=true)
							{
								case_all.checked=false;
							}
						});
					}
					/**
					 * Si le th�me fait partie des th�mes coch� pr�c�demment, il est coch� � sa cr�ation
					 * et tous les �v�nements qu'il contient sont ins�r�s dans le calendrier
					 */
					if (tab_choix.contains(nom_theme)) {
						if (boite.edit_style=='select') {
							option.selected=true;
						} else if(boite.edit_style=='checkbox') {
							case_choix.checked=true;
						}
					}
					cases++;
				});
				boite.affiche_results_xml(xml_themes);
			}

			
		}).request();
	},
	
	affiche_results_xml : function(xml_results) {
		
		//alert("affiche results press choix : ["+this.chaine_choix+"]");
		
		var tab_choix = this.chaine_choix.split(',');
		//console.log(this.chaine_choix, this)
		
		var boite = this;
		
		/**
		* 071107 Alexis : Adaptation du nombre d'articles ? l'espace en hauteur laiss? dans la boite (height dans le style css)
		*		Le premier article sera d?pos? dans tous les cas. Pour les suivants, on divise la hauteur de la boite par la hauteur d'un article.
		*		Et on place nb_art-1 articles dans l'espace restant.
		*		ATTENTION : 
		*		1.L'algorithme se r?f?re aux donn?es des ?l?ments de la feuille de style main_styles.css,
		*		aux classes div.lien_cp et div.contenu_text_droit_Cp. 
		*		2.Les donn?es css de hauteur doivent ?tre en PIXELS (px)
		*		3.Un changement de hauteur sur div.lien_cp implique un changement de hauteur des classes div.boite_contenu_gauche_middle2 et .s_contenu_sroll2
		*/

		/*hauteur d?finie par le style du premier article (tous ont la m?me hauteur, comme ca pas de jaloux)*/
		var h_titre_cp;
		var hauteur_totale_affichee = 0;
		
		//Hauteur de la boite contenant tous les articles
		var hauteur_boite_cp = String($('contenu_text_droit_Cp').getStyle('height'));
		hauteur_boite_cp = Number(hauteur_boite_cp.substring(0,hauteur_boite_cp.indexOf("px")));
		
		var nb_art_affichables = 2;
		compteur_color =0;
	
		var compteur = 0;
	
		var content_outil_cp = $('press_table');
	
		var index=0;
		while(index < content_outil_cp.childNodes.length  )
		{
			content_outil_cp.removeChild(content_outil_cp.childNodes[index]);
		}
		
		function writeArticle(item_xml){
			var link_ = item_xml.getElementsByTagName('link')[0].firstChild.nodeValue;
		 	var mydate = new Date();
		 	mydate.setTime(item_xml.getElementsByTagName('date')[0].firstChild.nodeValue +"000");
		 	
			var elem_br = new Element('br');
			var elem_img = new Element('img',{'src':base+'/images/fleche_orange_bas.gif','alt':''});
					
		 	var elem_span = new Element('span',{'class':'couleur_orange'}).setHTML(getDateString_fr(mydate,0));
		 	
		 	//071107 Alexis : Ajout d'une div de hauteur fixe pour garder le m?me espace pour tous
			var chaine_cp = item_xml.getElementsByTagName('title')[0].firstChild.nodeValue;
		 	//Calcul de la hauteur de la div a afficher
			var h_div = boite.getHauteurDivForChars(chaine_cp.length);
			
			if (chaine_cp.length > vNbCharsByLine) {
				chaine_cp = boite.getShorterString(chaine_cp);
			}
			//alert("chaine_cp = "+chaine_cp);
		 	if(compteur_color%2 == 0)
			{
				//var elem_tr = new Element('tr');
				var elem_div = new Element('div',{'class':'content_case_press'});
			}
			else
			{
	 			//var elem_tr = new Element('tr',{'class':'case_gray'});
				var elem_div = new Element('div',{'class':'content_case_press case_gray'});
			}
			compteur_color++;
		 	var div_lien_art = new Element('div',  {'class':'titre_cp_box', 'styles':{'height':h_div}});
		 	var elem_link = new Element('a',{'href':link_,'class':'hover_orange lien_fleche'}).setHTML(chaine_cp);
		 	div_lien_art.appendChild(elem_link);
		 	
		 	elem_div.appendChild(elem_span);
			elem_div.appendChild(elem_br);
			//elem_div.appendChild(elem_img);
			elem_div.appendChild(elem_link);
			
			content_outil_cp.appendChild(elem_div);
	 		
			/*recuperation de la hauteur d?finie par le style de l'article pr?c?dent, et de sa marge basse*/
			var h_titre_cp = div_lien_art.getStyle('height');
			var margin_bas_titre_cp = div_lien_art.getStyle('margin-bottom');
			margin_bas_titre_cp = Number(margin_bas_titre_cp.substring(0,margin_bas_titre_cp.indexOf("px")));
			h_titre_cp = Number(h_titre_cp.substring(0,h_titre_cp.indexOf("px")))+margin_bas_titre_cp;
	 		nb_art_affichables = (hauteur_boite_cp-hauteur_totale_affichee)/h_titre_cp;
	 		
	 		nb_art_affichables = hauteur_boite_cp/h_titre_cp;
		}
		
		for (var indexXml=0, iCounter=0; indexXml<xml_results.getElementsByTagName('article').length && iCounter<nb_art_affichables; indexXml++){
			var article = xml_results.getElementsByTagName('article')[indexXml];
			var item_xml = article;
			var themes = article.getElementsByTagName('theme');
			
			//si on est sur all
			if (this.chaine_choix.match(/\ball\b/)){
				writeArticle(item_xml);
				iCounter++;
			}else{
				for (var j=0; j < themes.length; j++){
					var re = new RegExp(this.chaine_choix,'g');
					if(!!themes[j].firstChild.nodeValue.match(re)){
						writeArticle(item_xml);
						iCounter++;
					}
				}
			}
		}
	},
	
	update_data_xml : function(init) {
		var boite = this;
		
		var lien_xml = this.link_xml;		

		if (this.link_xml.indexOf(",")!=-1) {
			var tab_link = this.link_xml.split(',');
			lien_xml = tab_link[0];
		}
		
		var themes_selected = [];
		
		if (init) {
			themes_selected = this.chaine_choix.split(',');
		} else {
			themes_selected = []; //cr??ation tableau pr stock?? les check coch??
			
			if (boite.edit_style=='checkbox') {
				$$('.choix_theme_'+this.suf_boite).each(function(item,index){
					var bool = item.checked ? themes_selected.include(item.value) : false;
				});
			} else if (boite.edit_style=='select') {
				themes_selected.include($('form_edit_'+this.suf_boite).value);
			}
			//alert("lesEvents dans calendar : "+lesEvents);
			if(Cookie.get(getEndString('/', this.nom_cookie)))
			{
				this.setParamCookie(getEndString('/', this.nom_cookie),themes_selected);
			}
			
			this.chaine_choix = themes_selected.join(',');
		}
		
		var tag_xml = this.tag_xml;
		
		var calendrier = this.calendrier;
		tab_events = [];
		if (!boite.registeredXML){
			var ajax = new Ajax(lien_xml,{
				method : 'get',
				onSuccess : function(themes, xml_themes){
					boite.affiche_results_xml(xml_themes);
				}
			}).request();
		}else{
			boite.affiche_results_xml(boite.registeredXML);
		}
		
		
	
		if(this.bool_first != 0)
		{
			var div_doc = new Fx.Styles('conteneur_global_'+this.suf_boite, {duration:750, wait:false});
			if (navigator.appName =="Netscape") div_doc.addEvent('onComplete', function() {if (boites_edit_ouvertes==0) initSIFR();});
			div_doc.start({'opacity':1});
			$('edit_boite_'+this.suf_boite).setStyle( 'display', 'none' );
			this.bool_first = 1;
		}
		
		//alert("time_elapsed = "+time_elapsed);
	}
});

/**
 * Boite des documents (pdf, ppt, etc.) du site : Elle r�cup�re dans un fichier XML des th�mes de documents et les �l�ments correspondants
 * L'int�gralit� des documents est pr�sente dans les pages internes.
 */
var BoiteDocuments = BoiteOutil.extend({
	
	initialize : function (id_boite, url_boite, config_boite, edit_style){
		
		this.parent(id_boite, url_boite, config_boite);
		
		//Mise en forme du formulaire de choix du (des) th�me(s) de filtrage (select box, checkboxes,etc.)
		this.edit_style = edit_style;
		this.tag_xml = 'topic';
		
		this.bool_first = 0;
		
		//Insertion du calendrier dans la boite
		//this.calendrier = new Calendrier();
		//this.calendrier.displayCalendar('w0');
		
		var boite = this;
		boite.registeredXML = false;
		this.first_event = 0;
		
		//Nom sous lequel sont stock�s coordonn�es et valeurs choisies dans la boite de choix du th�me
		this.nom_cookie = tools[this.id_boite];
		this.parent.nom_cookie = this.nom_cookie;
		
		this.filtre_page="";
		this.filtre_profil="";
		
		//Initialisation des diff�rentes variables contenant �ventuellement des filtres, le nom change en fonction
		//du type de boite
		if(typeof(outil_doccenter_new_filtre)!= 'undefined') {
			this.filtre_page = outil_doccenter_new_filtre;
		}
		if(typeof(home_filtre_outil_doccenter)!= 'undefined') {
			this.filtre_profil = home_filtre_outil_doccenter;
		}
		
	},
	affichage : function(arrayDoc){
		var boite = this;
		if (!boite.div_docCenter) boite.div_docCenter = $('doccenter_contenu');
		if(!boite.elm_ul) boite.elem_ul = new Element('ul', {'class':'liste_docs'});
		boite.div_docCenter.innerHTML = '';
		for (var i=0; i < arrayDoc.length && i < 6 ; i++){
			var file = arrayDoc[i].getElementsByTagName('file')[0].firstChild.nodeValue;
			var sType = Right(arrayDoc[i].getElementsByTagName('file')[0].getAttribute('type'),3);
			var texte = arrayDoc[i].getElementsByTagName('title')[0].firstChild.nodeValue;			
			boite.elem_ul.innerHTML += '<li class="puce_document_' + sType + '"><a href="' + file + '">' + texte + '</a></li>';
			
		}
		boite.div_docCenter.appendChild(boite.elem_ul);
	},
	
	affiche_contenu : function() {
	 	var boite = this;
	 	boite.initBoite();
		/**
		 * R�cup�ration du tableau des th�mes s�lectionn�s et stock�s dans les cookies ou dans les filtres existants
		 */
		boite.getThemesFromCookie(boite.edit_style);
		/**
		 * Chargement de la boite des th�mes de tri et des �l�ments xml � afficher
		 */
		//console.log(boite.edit_style)
		this.init_data_xml(boite.edit_style);
	},

	affiche_results_xml : function(xml_results) {
		var arrayDoc = [];
		if (this.chaine_choix.match(/\ball\b/)){
			var documents = xml_results.getElementsByTagName('doc');
			for (var i=0; i<documents.length ;i++){
				arrayDoc.push(documents[i]);
			}
			//je tries par date
			arrayDoc.sort(sortFunction('date'));
			this.affichage(arrayDoc);
			
		}else{
			var topics = xml_results.getElementsByTagName('topic');
			for (var i=0; i < topics.length; i++) {
				if (topics[i].getAttribute('name') == this.chaine_choix){
					var documents = topics[i].getElementsByTagName('doc');
					for (var j=0; j<documents.length ;j++){
						arrayDoc.push(documents[j]);
					}
				}
			};
			this.affichage(arrayDoc);
			/*
			var documents = [this.chaine_choix].getElementsByTagName('doc');
			for (var i=0; i<documents.length ;i++){
				arrayDoc.push(documents[i]);
			}
			*/
		}
	},
	getXMLThemesAndItems : function(form_choix, lien_xml) {
		
		var boite = this;
		
		var cases = 0;
		
		var tab_choix = this.chaine_choix.split(',');
		var ajax = new Ajax(lien_xml,{
			method : 'get',
			onSuccess : function(themes, xml_themes){
				boite.registeredXML = xml_themes;
				var topics = xml_themes.getElementsByTagName('topic');
				for (var i=0; i<topics.length ;i++){
					var theme = topics[i];

					var id_theme = theme.getAttribute('id');
					var nom_theme = theme.getAttribute('name');
					if (boite.edit_style=='select') {
						var option = document.createElement("option");
						option.setAttribute("id",id_theme);
						option.setAttribute("value",nom_theme);
						option.setAttribute("class","choix_theme_"+boite.suf_boite);
						option.innerHTML= nom_theme;
						form_choix.appendChild(option);
						if (nom_theme == boite.chaine_choix) option.selected = 'true'
					}

				}
				boite.affiche_results_xml(xml_themes);

				/**
				 * Gestion du cas ou tous les thèmes sont choisis en prenant en compte le type de liste de choix
				 */
				if (boite.chaine_choix == 'all' && boite.edit_style=='select') {
					$('tout_'+boite.suf_boite).selected = 'true';
				}
			}
		}).request();
	
	},
	
	update_data_xml : function(init) {
		var boite = this;
		
		var lien_xml = this.link_xml;		

		if (this.link_xml.indexOf(",")!=-1) {
			var tab_link = this.link_xml.split(',');
			lien_xml = tab_link[0];
		}
		
		var themes_selected = [];
		
		if (init) {
			themes_selected = this.chaine_choix.split(',');
		} else {
			themes_selected = []; //cr??ation tableau pr stock?? les check coch??
			
			if (boite.edit_style=='checkbox') {
				$$('.choix_theme_'+this.suf_boite).each(function(item,index){
					var bool = item.checked ? themes_selected.include(item.value) : false;
				});
			} else if (boite.edit_style=='select') {
				themes_selected.include($('form_edit_'+this.suf_boite).value);
			}
			//alert("lesEvents dans calendar : "+lesEvents);
			if(Cookie.get(getEndString('/', this.nom_cookie)))
			{
				this.setParamCookie(getEndString('/', this.nom_cookie),themes_selected);
			}
			
			this.chaine_choix = themes_selected.join(',');
		}
		
		var tag_xml = this.tag_xml;
		
		var calendrier = this.calendrier;
		
		tab_events = [];
		if (!boite.registeredXML){
			var ajax = new Ajax(lien_xml,{
				method : 'get',
				onSuccess : function(themes, xml_themes){
					boite.affiche_results_xml(xml_themes);
				}
			}).request();
		}else{
			boite.affiche_results_xml(boite.registeredXML);
		}
		
		if(this.bool_first != 0)
		{
			var div_doc = new Fx.Styles('conteneur_global_'+this.suf_boite, {duration:750, wait:false});
			if (navigator.appName =="Netscape") div_doc.addEvent('onComplete', function() {if (boites_edit_ouvertes==0) initSIFR();});
			div_doc.start({'opacity':1});
			$('edit_boite_'+this.suf_boite).setStyle( 'display', 'none' );
			this.bool_first = 1;
		}
		
		//alert("time_elapsed = "+time_elapsed);
	}
});

var BoiteStocks = BoiteOutil.extend({
	initialize : function (id_boite, url_boite, config_boite, edit_style){
		this.parent(id_boite, url_boite, config_boite);
	},
	affiche_contenu : function() {
		this.initBoite();
		$('iframe_share').setAttribute('src', sCheminPageStock);
	}
});

var BoiteProduits = BoiteOutil.extend({
	initialize : function (id_boite, url_boite, config_boite, edit_style){
		this.parent(id_boite, url_boite, config_boite);
	},
	affiche_contenu : function() {
		this.initBoite();
		this.init_data_xml();
	},
	
	initBoite : function() {
		var index=0;
		
		//Pinouf // 16/01/2008 // rajout select box dynamic des libell?s
		var tab_product_libelle = [select_pays_product,select_services_product,select_site_product];
		$('products_contenu').getElements('option').each(function(item,index){
		  	item.setHTML(tab_product_libelle[index]);
		});
		// fin rajout
		var opac_button = new Fx.Styles('valide_go_products', {duration:750, wait:false});
		opac_button.start({'opacity':0.3});
	
	},
	
	init_data_xml : function() {
		var boite = this;
		this.hasAjaxReturn = false;
		this.getXMLThemesAndItems("country");
		$('select_country').addEvent('change', function() {
			if(this.value != 0) {
				boite.getXMLThemesAndItems("service", "country", this.value);
				boite.getXMLThemesAndItems("website", "country", this.value);
			} else {
				$('select_service').disabled=true;
				$('select_service').selectedIndex = 0;
				var elem_option_vide = new Element('option',{'value':0});
				elem_option_vide.setHTML(select_services_product);
				$('select_service').appendChild(elem_option_vide);
				$('select_website').disabled=true;
				$('select_website').selectedIndex = 0;
				var elem_option_vide_link = new Element('option',{'value':0});
				elem_option_vide_link.setHTML("produits");
				$('select_website').appendChild(elem_option_vide_link);
			}
		});
		$('select_service').addEvent('change', function() {
			if (this.value != 0) {
				boite.getXMLThemesAndItems("website", "service", this.value);
			} else {
				$('valide_go_products').disabled=true;
				boite.getXMLThemesAndItems("service", "country", $('select_country').value);
				boite.getXMLThemesAndItems("website", "country", $('select_country').value);
			}
		});
		$('select_website').addEvent('change', function() {
			if(this.value != 0) {
				var opac_button = new Fx.Styles('valide_go_products', {duration:750, wait:false});
				opac_button.start({'opacity':1});
				$('valide_go_products').disabled=false;
			} else {
				$('valide_go_products').disabled=true;
				boite.getXMLThemesAndItems("country");
			}
		});
		$('valide_go_products').addEvent('click',function(){boite.ouvrir_lien_produit();});
	},
	
	getXMLThemesAndItems : function(select_name, filtre, valeur) {
		
		var tab_options = $A([]);
		
		if (filtre != undefined && filtre == 'country') {
			var opac_button = new Fx.Styles('valide_go_products', {duration:750, wait:false});
			opac_button.start({'opacity':0.3});
			$('select_'+select_name).empty();
		}
		$('select_'+select_name).disabled = false;
		
		
		$('select_'+select_name).empty();
		var elem_option_vide = new Element('option',{'value':0});
		
		if(select_name != undefined) {
			switch(select_name) {
				case "country":
					elem_option_vide.setHTML(select_pays_product);
				break;
				case "service":
					elem_option_vide.setHTML(select_services_product);
				break;
				case "website":
					elem_option_vide.setHTML(select_site_product);
				break;
			}
		}
		
		$('select_'+select_name).appendChild(elem_option_vide);
		
		if(filtre != undefined && filtre == 'service') {
			var opac_button = new Fx.Styles('valide_go_products', {duration:750, wait:false});
			opac_button.start({'opacity':1});
			$('valide_go_products').disabled=false;
			
		}
		var _self =this;
		_self.xml;
		if (!_self.hasAjaxReturn){
			var ajax = new Ajax(this.link_xml,{
				method : 'get',
				onRequest : function(){
					setBoxLoader(_self.id_boite);
				},
				onSuccess : function(elements, xml_elements){
					_self.hasAjaxReturn = true;
					_self.xml = xml_elements;
					onResponse(_self.xml);


				},onComplete : function(){
					unsetBoxLoader(_self.id_boite);
				}


			}).request();
		}else{
			onResponse(_self.xml);
		}
		
		
		function onResponse(xml_elements){
			for (var i=0; i<xml_elements.getElementsByTagName('product').length ;i++){
				var product = xml_elements.getElementsByTagName('product')[i];
				if (!tab_options.contains(product.getElementsByTagName(select_name)[0].firstChild.nodeValue)) {

					if (filtre != undefined) {
						if (product.getElementsByTagName(filtre)[0].firstChild.nodeValue == valeur) {
							tab_options.include(product.getElementsByTagName(select_name)[0].firstChild.nodeValue);
						}
					} else {
						tab_options.include(product.getElementsByTagName(select_name)[0].firstChild.nodeValue);
					}
				}
			}
			
			tab_options.each(function(option, i) {
				var elem_option = new Element('option',{'value':option});
				elem_option.setHTML(option);
				$('select_'+select_name).appendChild(elem_option);
			});
		}
	
	},
	ouvrir_lien_produit : function() {
		var select_country = $('select_country').value;
		var select_website = $('select_website').value;
		var select_service = $('select_service').value;
		
		var lien_a_ouvrir = "";
		
		//Tableau stockant les nombres d'�l�ments correspondant aux valeurs s�lectionn�es pour chaque �l�ment du xml
		//Cela sert � rep�rer le produit qui correspond le plus aux crit�res et d'afficher apr�s son lien
		var tab_nb_elements_egaux = [];
		
		
		var ajax = new Ajax(this.link_xml,{
			method : 'get',
			onSuccess : function(elemnts, xml_elements){
				for (var i=0; i<xml_elements.getElementsByTagName('product').length ;i++){
					var product = xml_elements.getElementsByTagName('product')[i];
					
					tab_nb_elements_egaux[product.getAttribute('link')] = 0;

					if (product.getElementsByTagName('country')[0].firstChild.nodeValue == select_country) {
						tab_nb_elements_egaux[product.getAttribute('link')] += 1;
					}
					if (product.getElementsByTagName('service')[0].firstChild.nodeValue == select_service) {
						tab_nb_elements_egaux[product.getAttribute('link')] += 1;
					}
					if (product.getElementsByTagName('website')[0].firstChild.nodeValue == select_website) {
						tab_nb_elements_egaux[product.getAttribute('link')] += 1;
					}
					
					
					
					
				}
				var plus_grd_nb_corresp = 0;

				for (var lien in tab_nb_elements_egaux) {
					//Si le nombre de correspondances de crit�res est plus grand pour ce lien,
					//Il devient le lien � ouvrir, et ce en boucle jusqu'� trouver le plus grand nombre
					if (typeof tab_nb_elements_egaux[lien] != 'function' && tab_nb_elements_egaux[lien] > plus_grd_nb_corresp) {				
						plus_grd_nb_corresp = tab_nb_elements_egaux[lien];
						lien_a_ouvrir = lien;
					}
				}

				if (lien_a_ouvrir != "") window.open(lien_a_ouvrir);
			
			}
			
			
		}).request();

	}
});

var BoiteGlossaire = BoiteOutil.extend({
	initialize : function (id_boite, url_boite, config_boite, edit_style){
		this.parent(id_boite, url_boite, config_boite);
		this.autoCompleterLoad = false;
		this.tokens = [];
	},
	affiche_contenu : function() {
		this.init_data_xml();
		
	},
	
	init_data_xml : function() {
		
		
		var boite = this;
		var ajax = new Ajax(this.link_xml,{
			method : 'get',
			onSuccess : function(words, xml_words){
				
				for (var i=0; i<xml_words.getElementsByTagName('word').length ;i++){
					var word = xml_words.getElementsByTagName('word')[i];
					var petit_tableau = [];
					var xml_word =word.getAttribute('name');
					petit_tableau.include(xml_word);
					petit_tableau.include("");
					boite.tokens.include(petit_tableau);
				}
				var blanc =['',''];
				boite.tokens.include(blanc);

			}
			
			
		}).request();

		$('valide_go_glossary').addEvent('click',function(){boite.update_data_xml();});
		
		$('text_go').addEvent('click', function() { this.focus(); if (!boite.autoCompleterLoad) {boite.autoImplemente();}})
		
		var addContenu = $('contenu_text_glossary');
		
		//affichage
		//var xml_words = responseXML.getElementsByTagName("word");
		//xml_words = $A(xml_words)
		
		//Vidage du contenu de la d�finition du mot
		var index = 0;
		while(index < addContenu.childNodes.length)
		{
			addContenu.removeChild(addContenu.childNodes[index]);
		}
		
		fade_glossaire = new Fx.Styles(addContenu, {
			duration: 300,
			wait: false
		});
		
		var elem_br = document.createElement('br');
		
		//080214 : Initialisation du message du glossaire
		var msg_init = new Element('span');
		msg_init.innerHTML = sMsgGlossaire[sLangSite];
		addContenu.appendChild(elem_br);
		addContenu.appendChild(msg_init);
		
	},
	
	update_data_xml : function() {
		
		/*		var xml_words = responseXML.getElementsByTagName("word");*/
		var laRecherche = $('text_go').value;
		var addContenu = $('contenu_text_glossary');
		var i = 0 ;
	
		var bool = 0;
		var leBr = document.createElement("br");
	
		var index = 0;
		while (index < addContenu.childNodes.length) {
			addContenu.removeChild(addContenu.childNodes[index]);
		}
		
		var ajax = new Ajax(this.link_xml,{
			method : 'get',
			onSuccess : function(words, xml_words){
				for (var i=0; i<xml_words.getElementsByTagName('word').length ;i++){
					var word = xml_words.getElementsByTagName('word')[i];
					
					var mot = word.getAttribute('name');

					if (laRecherche.toLowerCase() === mot.toLowerCase()) {
						var resultRecherche = word.firstChild.nodeValue;
						leSpan = document.createElement("span");
						leSpan.setAttribute("className", "couleur_orange");
						leSpan.setAttribute("class", "couleur_orange");
						leSpan2 = new Element('span', {
							'class': 'glossary_text'
						});
						leSpan.innerHTML = mot;
						leSpan2.innerHTML = resultRecherche;
						addContenu.appendChild(leSpan);
						addContenu.appendChild(leBr);
						addContenu.appendChild(leSpan2);
						bool = 1;
					}
				}
				//Pinouf 16/01/2008 // 
				if (laRecherche != "" && bool == 0) {

					$('valide_go_glossary').disabled = true;

					leSpan2 = new Element('span', {
						'class': 'glossary_text'
					});
					leSpan2.innerHTML = glossary_empty_message;
					addContenu.appendChild(leBr);
					addContenu.appendChild(leSpan2);

					//Alexis 080214 : fonction d'effacement du message d'erreur en fondu apr�s quelques secondes et Retour au message d'accueil
					var myTimer = (function(){
						fade_glossaire.addEvent("onComplete", function(){

							if (!transition_finie) {
								transition_finie = true;
								var index = 0;
								while (index < addContenu.childNodes.length) {
									addContenu.removeChild(addContenu.childNodes[index]);
								}

								var msg_init = new Element('span');
								msg_init.innerHTML = sMsgGlossaire[sLangSite];
								addContenu.appendChild(leBr);
								addContenu.appendChild(msg_init);

								this.start({
									'opacity': 1
								});
							} else {
								$('valide_go_glossary').disabled = false;
							}
						});
						transition_finie = false;
						fade_glossaire.start({
							'opacity': 0
						});
					}).delay(2000);

				} else if (laRecherche == "") {
					//Alexis 080214 : Initialisation du message du glossaire lorsque le champ est vid�
					var index = 0;
					while (index < addContenu.childNodes.length) {
						addContenu.removeChild(addContenu.childNodes[index]);
					}
					var msg_init = new Element('span');
					msg_init.innerHTML = sMsgGlossaire[sLangSite];
					addContenu.appendChild(leBr);
					addContenu.appendChild(msg_init);
				}
			
			}
			
			
		}).request();
	
	},
	
	/**
	 * Cr�ation de l'autoimpementeur : Il s'agit de l'aide � la recherche du mot par
	 * reconnaissance de la saisie en cours. Cette cr�ation n'est faite qu'une fois lors du premier clic sur le champ de recherche
	 * car c'est uniquement � ce moment l� que son chargement est utile.
	 */
	autoImplemente : function()
	{
		this.autoCompleterLoad = true;
		//var el =$ES('input','#text_go')
		var el =$('form_autoImple').getElements('input[id=text_go]');
		
		var tokens = this.tokens;
		var completer1 = new Autocompleter.Local(el[0], tokens, {
			'delay': 100,
			'zIndex' : parseInt($(this.id_boite).style.zIndex)+1,
			'filterTokens': function() {
				var regex = new RegExp('^' + this.queryValue.escapeRegExp(), 'i');
				return this.tokens.filter(function(token){
					return (regex.test(token[0]) || regex.test(token[1]));
				});
			},
			'injectChoice': function(choice) {
				var el = new Element('li')
					.setHTML(this.markQueryValue(choice[0]))
					.adopt(new Element('span', {'class': 'example-info'}).setHTML(this.markQueryValue(choice[1])));
				el.inputValue = choice[0];
				this.addChoiceEvents(el).injectInside(this.choices);
			}
		});
		
	},
	load_tab_tokens_impl : function()
	{
		var tab_autoImplemente = [];
		var ajax = new Ajax(this.link_xml,{
			method : 'get',
			onSuccess : function(toks, tokens){
				for (var i=0; i<tokens.getElementsByTagName('word').length ;i++){
					var word = tokens.getElementsByTagName('word')[i];
					
					var petit_tableau = [];
					var xml_word =word.getAttribute('name');
					//var xml_word_decouper = decoupe_word(xml_word);
					petit_tableau.include(xml_word);
					petit_tableau.include("");
					tab_autoImplemente.include(petit_tableau);
				}
			
			}
			
			
		}).request();

		var blanc =['',''];
		tab_autoImplemente.include(blanc)
		return tab_autoImplemente;
	}
});

var BoiteBookmarks = BoiteOutil.extend({
	
	initialize : function (id_boite, url_boite, config_boite, edit_style){
		this.parent(id_boite, url_boite, config_boite);
	},
	
	affiche_contenu : function() {
		
		var contenu_bookmarks = $('contenu_bookmarks');
	
		contenu_bookmarks.empty();
	
		if(Cookie.get('myBookmarks'))
		{
			var mycookie = Cookie.get('myBookmarks');
			var tab_bookmarks = mycookie.split('*');
			var tab_bookmark = $A([])
			tab_bookmarks.each(function(item,index){
				if(index >0)
				{
					tab_bookmark.include(item);
				}
			});
	
			var index=0;
			while(index < contenu_bookmarks.childNodes.length  )
			{
				contenu_bookmarks.removeChild(contenu_bookmarks.childNodes[index]);
			}
			
			tab_bookmark.each(function(item,index){
			   var tab_cookie_data = item.split('@');
			   var url = tab_cookie_data[0];
			   var title = tab_cookie_data[1];
			   var elem_img_fleche = new Element('img',{src:base+'/images/fleche_orange_bas.gif','alt':''});							   
			   var elem_a = new Element('a',{'class':'couleur_orange_link lien_fleche','href':url});
			   var elem_div= new Element('div',{'class':'pt15'});
			   elem_a.setHTML(title);
			   contenu_bookmarks.appendChild(elem_div);
			   contenu_bookmarks.appendChild(elem_a);
			});
			
			} else {
				var elem_p = new Element('p');
				var elem_br = new Element('br');
				elem_p.setHTML(bookmarks_message_default[sLangSite]);
				contenu_bookmarks.appendChild(elem_br);
				contenu_bookmarks.appendChild(elem_p);
			}	
	}
	
});

var BoiteSondage = BoiteOutil.extend({
	
	initialize : function (id_boite, url_boite, config_boite, edit_style){
		this.parent(id_boite, url_boite, config_boite);
		this.id_sondage = config_boite.libelle_defaut;
	},
	
	affiche_contenu : function() {
        this.initBoite();
		var boite = this;
		//alert('here')
		//Ajout des cellules contenant les diff�rents messages de notification
		var div_sondage = $('sondage');
		var div_wait = new Element('div', {'id':'poller_waitMessage'+this.id_sondage});
		var tab_results = new Element('div', {'id':'poller_results'+this.id_sondage, 'class':'tab_poller_results'});
		
		div_sondage.appendChild(div_wait);
		div_sondage.appendChild(tab_results);
		
		if(useCookiesToRememberCastedVotes){
			var cookieValue = Poller_Get_Cookie('dhtmlgoodies_poller_'+this.id_sondage);

			if(cookieValue && cookieValue.length>0) {
				displayResultsWithoutVoting(this.id_sondage);
			} else {
				(function(){boite.init_data_xml();}.delay(100))
			}
		} else {
			(function(){boite.init_data_xml();}.delay(100))
		}
	},
	
	init_data_xml : function() {
		var id_sondage = this.id_sondage;
		var xml_sondage = null;
		var ajax = new Ajax(this.link_xml+"view?view="+id_sondage,{
			method : 'get',
			onSuccess : function(sondages, xml_sondages){
				if (!xml_sondages) return;
				
				for (var i=0; i<xml_sondages.getElementsByTagName('sondage').length ;i++){
					var sondage = xml_sondages.getElementsByTagName('sondage')[i];
					if (sondage.getAttribute('id') == id_sondage) {
						var question = sondage.getAttribute('question');
						var div_question = $('poller_question');
						div_question.setAttribute('id','poller_question'+id_sondage);
						var tabQuestion = new Element("table", {'style':'width:100%'});
						var trQuestion = new Element("tr");
						var trReponse = new Element("tr");
						var tdQuestion = new Element("td", {'colspan':'2'});
						var tdReponse = new Element("td");
						var tdButton = new Element("td",  {'class':'pollerButton'});
						
						//Insertion de la question d�finie dans le xml du sondage
						var p_question = new Element("p", {'class':'pollerTitle'});
	                    p_question.innerHTML = question;
						tdQuestion.appendChild(p_question);
						trQuestion.appendChild(tdQuestion);

	                    //Insertion du bouton "go"
	                    var lien_send = new Element('a', {'id':'vote','href':'#'});
				        var img_send = new Element('img', {'src':'/images/btn_ok.jpg', 'alt':'send my vote'});
				        lien_send.appendChild(img_send);
						tdButton.appendChild(lien_send);

			            //Contruction du tableau
			            trReponse.appendChild(tdReponse);
			            trReponse.appendChild(tdButton);

			            tabQuestion.appendChild(trQuestion);
			            tabQuestion.appendChild(trReponse);
						
						
						for (var i=0; i<sondage.getElementsByTagName('reponse').length ;i++){
							var reponse = sondage.getElementsByTagName('reponse')[i];
							//R�ponses au sondage
							var p_reponse = new Element('p');
							var id_reponse = reponse.getAttribute('id');
							var texte_reponse = reponse.firstChild.nodeValue;
							p_reponse.innerHTML = "<input type=\"radio\" id=\"pollerOption"+id_reponse+"\" style=\"vertical-align:middle\" value=\""+id_reponse+"\" name=\"vote["+id_sondage+"]\" ><label for=\"pollerOption"+id_reponse+"\" style=\"vertical-align:middle\" id=\"optionLabel"+id_reponse+"\" >"+texte_reponse+"</label>";
							p_reponse.className="pollerOption";

							tdReponse.appendChild(p_reponse);
						}

	                    //Ajout du tableau dans le div conteneur
	                    div_question.innerHTML = "<table style='width:100%'>" + tabQuestion.innerHTML + "</table>";
	
	
	                    //Ajout de l'action sur le lien de vote
						
				        $('vote').addEvent('click', function() {
				            castMyVote(id_sondage,document.getElementsByName('form_sondage')[0]);
				        });
				
					}
				}
		
			}
			
			
		}).request();

	}
});

function loadBoxContent(id_item) {	
	//alert(id_item);
	var box = id_item;
	
	if (box.indexOf("element")==-1) {
		box += "_element";
	}
	if($(box)){
		$(box).loader.setOpacity(1);
		switch(box) {
				case 'press_releases_element':
					var boite_presse = new BoitePresse(box, tab_pages_outils[box],press_releases.config,  "select");
				break;
				case 'televideo_element':
					var boite_mediacenter = new BoiteOutil(box, tab_pages_outils[box],media.config,  "select");
				break;
				case 'calendar_element':
					var boite_calendar = new BoiteCalendrier(box, tab_pages_outils[box],calendrier.config,  "self", "checkbox");
				break;
				case 'production_element':
					var boite_products = new BoiteOutil(box, tab_pages_outils[box],products.config,  "select");
				break;
				case 'stocks_element':
					var boite_stocks = new BoiteStocks(box, tab_pages_outils[box],share.config,"select");
				break;
				case 'jobs_element':
					var boite_jobs = new BoiteOutil(box, tab_pages_outils[box],jobs.config, "select");
				break;
				case 'docs_center_element':
					var boite_docs = new BoiteDocuments(box, tab_pages_outils[box], docCenter.config, "select");
				break;
				case 'mymag_element':
					var boite_mymag = new BoiteOutil(box, tab_pages_outils[box], myMag.config, "select");
				break;
				case 'glossary_element':
					var boite_glossaire = new BoiteGlossaire(box, tab_pages_outils[box],glossary.config, "select");
				break;
				case 'bookmarks_element':
					var boite_bookmarks = new BoiteBookmarks(box, tab_pages_outils[box], bookmarks.config, "select");
				break;
				case 'cards_element':
					var boite_cards = new BoiteOutil(box, tab_pages_outils[box], flash.config, "select");
				break;
				case 'curiculum-vitae_element':
					var boite_news = new BoiteOutil(box, tab_pages_outils[box], news.config, "select");
				break;
				case 'poll_element':
					var boite_poll = new BoiteSondage(box, tab_pages_outils[box], poll.config, "select");
				break;
				case 'highlight_element':
					var boite_highlight = new BoiteOutil(box, tab_pages_outils[box], highlight.config, "select");
				break;
				case 'alaune_element':
					var boite_alaune = new BoiteOutil(box, tab_pages_outils[box], alaune.config, "select");
				break;
				case 'bubble_element':
					var boite_bubble = new BoiteOutil(box, tab_pages_outils[box], bubble.config, "select");
				break;
				case 'sites_element':
					var boite_sites = new BoiteOutil(box, tab_pages_outils[box], sites.config, "select");
				break;
		}
	}
}
function unsetBoxLoader(id){
	var loaderId = 'loader_'+id;
	var loader = $(loaderId);
	if (loader){
		loader.setOpacity(1);
		loader.style.display = 'none';
	}
}
function setBoxLoader(id){
	var loaderId = 'loader_'+id;
	var loader = $(loaderId);
	if (loader){
		loader.setOpacity(1);
		loader.style.display = 'block';
	}
}
var sortFunction = function (name){
	return function(m,n){
		var a,b;
		if (typeof m === 'object' && typeof n === 'object' && m && n){
			//console.log(a, o.getElementsByTagName('date'))
			a = m.getElementsByTagName(name)[0].firstChild.nodeValue;
			b = n.getElementsByTagName(name)[0].firstChild.nodeValue;
			a = parseInt(a);
			b = parseInt(b);
			if (a == b){
				return 0
			}else{
				return a > b ? -1 : 1;
			}
		}
	}
}
