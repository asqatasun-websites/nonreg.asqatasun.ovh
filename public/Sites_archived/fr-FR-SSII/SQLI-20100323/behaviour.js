$(function(){
	
	$("#print").css("display", "block");
	
	// formulaires
	$("input.autoreset").focus(function(){
			if($(this).val()==$(this).attr("title")){
				$(this).val("");
			}
	});
	$("input.autoreset").blur(function(){
		if($(this).val()==""){
				$(this).val($(this).attr("title"));
		}
	});
	
	$(".formulaire-type .saisie").focus(function(){
			$(this).addClass("focus");
	});
	$(".formulaire-type .saisie").blur(function(){
			$(this).removeClass("focus");
	});
	
	
	$(".diaporama-menu .img-menu").css("display", "none");
	/*var imgDefault = "#img-"+$("#menu-niv-4 a.encour").attr("id");*/
	var imgDefault = "#img-roll-menu-1";
	$(imgDefault).css("display", "block");
	
	
	/*liens services*/
	$("#menu-niv-4 a").hover(function(){
		$(".diaporama-menu .img-menu").css("display", "none");
		var imgID = "#img-"+$(this).attr("id");
		$("#menu-niv-4 a").removeClass("encour");
		$(this).addClass("encour");
		$(imgID).css("display", "block");
		
		},function(){
		//$("#img-menu").attr({ src: $("#menu-niv-4 a.encour").attr("title") });
		//$(this).removeClass("encour");
	});
	
	$(".sector-diaporama .image-menu").css("display", "none");
	$(".sector-diaporama #default").css("display", "block");
	
	/*liens secteur*/
	$("#left-colonne .menu a").hover(function(){
		$(".image-menu").css("display", "none");
		var imgID = "#img-"+$(this).attr("id");
		$(imgID).css("display", "block");
		},function(){
		$(".sector-diaporama .image-menu").css("display", "none");
		$(".sector-diaporama #default").css("display", "block");
	});
	
	
	$("#tri-references ul li.child").children("ul").hide();
	
	/*menu deroulant (tri references)*/
	$("#tri-references .liste>li.child").mouseover(function(){
		$(".sousnavcut").remove();
		menuHide();
		$(this).children("ul").show();
		$(this).children("ul").addClass("sousnav");
		o = $(this).children("ul").offset();
		$(this).children("ul").clone().prependTo("body");
		$("body").children("ul").addClass("sousnavcut");
		$(".sousnavcut").css("top", o.top);
		$(".sousnavcut").css("left", o.left);
		$(".sousnavcut").css("position", "absolute");
		$(this).children("ul").hide();
		
		$(".sousnavcut a").click(function(){
			var selecteur = $(this).attr("href").substr(($(this).attr("href").lastIndexOf("#")+1), 99);
			if(
			   		($("#services_onglets").is("ul") == true)
				&&	($("#"+selecteur).is("a") == true)
			  )
				{
				$(".entete").hide();
				$("#entete_"+selecteur).show();
				$("#services_onglets").children("li").removeClass("on");
				$("#"+selecteur).parent().addClass("on");
				$("#services_contenu").children("div").hide();
				$("#contenu_"+selecteur).show();
				$(".sousnavcut").remove();
			}
		});
		
	});
	function menuHide() {
		$("html").mouseover(function(o) {
			o = o.target;
			while(o.parentNode) {
				if( (o.className == "liste") || (o.className == "sousnav sousnavcut") ) {
					break;
				}
				if(o.nodeName == "HTML") {
					$(".sousnavcut").remove();
					$("html").unbind();
				}
				o = o.parentNode;
			}
		});
	}
		
			
	// slider news
	if ($(".news1").length >0){
		$(".news1").accessNews({
				newsHeadline: "Infos",
				newsSpeed: "normal",
				slideBy:1
		});
	}
	
	
});