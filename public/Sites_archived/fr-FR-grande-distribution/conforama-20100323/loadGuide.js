$(document).ready(function () {
	var isGuideOpen=true;
	$(".listAllGuide").hide();
	$("a.defaultLoadGuide").addClass("active");
	$("a.defaultLoadGuide").parents(".bandeauRayon").siblings(".listAllGuide").slideDown("slow", function() {$(this).height('');});
	
	$("a.defaultLoadGuide").parents(".bandeauRayon").siblings(".listAllGuide").addClass("isDown");
	$("a.loadGuide").click(function(){
		var robert=$(this);
		if($(robert).attr("class").indexOf("active")!=-1){
			$(robert).removeClass("active");
			$(robert).parents(".bandeauRayon").siblings(".listAllGuide").removeClass("isDown");
			$(robert).parents(".bandeauRayon").siblings(".listAllGuide").slideUp("fast", function() {$(this).height('');});
			$(".isDown").removeClass("isDown");
		}else{
			if($(".isDown").is("div")){
				$("a.active").removeClass("active");
				$(".isDown").slideUp("fast", function(){
					$(".isDown").removeClass("isDown");
					$(robert).addClass("active");
					$(".active").parents(".bandeauRayon").siblings(".listAllGuide").slideDown("slow", function() {$(this).height('');});
					$(".active").parents(".bandeauRayon").siblings(".listAllGuide").addClass("isDown");
				});
			}else{
				$(robert).addClass("active");
				$(robert).parents(".bandeauRayon").siblings(".listAllGuide").addClass("isDown");
				$(".isDown").slideDown("slow", function() {$(this).height('');});
			}
		}
   	return false;
   });
});