try
{
    var rg = {};   
    rg.al1 = [];
    rg.al2 = [];
    rg.al3 = [];
    rg.list = [];
    rg.adgetList = [];
    
    //assign adgets to slot1
    rg.al1.push({pgAdget:'NBCRM2'});
    
    //assign adgets to slot2
    rg.al2.push({pgAdget:'NBCRM3'});

    //assign adgets to slot3
    rg.al3.push({pgAdget:'NBCRM1'});
    rg.al3.push({pgAdget:'NBCRM4'});
    
    //shuffles the adget lists
    rg.al1.sort(function() {return 0.5 - Math.random()});
    rg.al2.sort(function() {return 0.5 - Math.random()});
    rg.al3.sort(function() {return 0.5 - Math.random()});
    
    //add to the resource guide list
    rg.list.push({pgBadge:'NBCRC1'});
    rg.list.push({pgBadge:'NBCRC2'});
    rg.list.push({pgBadge:'NBCRC3'});
    rg.list.push({pgBadge:'NBCRC4'});
    rg.list.push({pgBadge:'NBCRC5'});
    rg.list.push({pgBadge:'NBCRC6'});
    rg.list.push({pgBadge:'NBCRC7'});
    
    //shuffles the resource guide list
    rg.list.sort(function() {return 0.5 - Math.random()});
    rg.list.pop();

    //add to the adget list
    rg.adgetList.push({pgAdget:rg.al1[0].pgAdget});
    rg.adgetList.push({pgAdget:rg.al2[0].pgAdget});
    rg.adgetList.push({pgAdget:rg.al3[0].pgAdget});
}
catch(e)
{
    //do nothing
}