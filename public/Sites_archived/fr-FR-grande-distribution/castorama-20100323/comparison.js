

var comparisonContainerUrl;
var comparisonMngUrl;
var arrayOfProducts = new Array();
	

	$(document).ready(function(){	
			
			prepareDraggables();			
					
			comparisonContainerUrl = contextPath + "/comparison/comparisonContainer.jsp";
			comparisonMngUrl = contextPath + "/comparison/comparisonMng.jsp";
			
			// expand comparison block if there are items inside
			if($('#campExpanded a.campRemove').length){
				document.getElementById('campExpanded').style.display = "block";
				document.getElementById('campCollapsed').style.display = "none";	
			}

            // remove "droppable" classes from items inside comparison block   
			var divs = $("div#campExpanded div.rendered");					
			jQuery.each(divs, function(i, val) {
				var holder = $(val);
				//holder.removeClass("ui-droppable");
				//holder.removeClass("droppable");
				//holder.droppable('option', 'accept', '');
		    });
		    
		    // add onclick event function to every item in comparison block
		    var images = $("div#campExpanded a.campRemove");					
			jQuery.each(images, function(i, val) {
		    	$(val).click(function(ev){removeImage(ev);} );
		    });	
			
			// add item to the comparison block
			$(".droppable").droppable({
				greedy: true,
				accept: '.prdMarker',
				scope: 'addToComp', 				
				drop: function(event, ui) {	
					var thisDiv = $(this);
					if(thisDiv.contents().length == 0){
						addImage($(this), ui.draggable);
					} 										
				}
			});
			
			// expand comparison block if item is above it 
			$(".droppable2").droppable({
				greedy: false,
				accept: '.prdMarker',
				scope: 'addToComp', 
				tolerance: 'pointer', 
				over: function(event, ui) {					
					var campExpaned = document.getElementById('campExpanded');
					var campCollapsed = document.getElementById('campCollapsed');

					if (campExpaned.style.display != "block") {						
						campExpaned.style.display = "block";
						campCollapsed.style.display = "none";						
					} 					
				},				
				drop: function(event, ui) {					
				}
			});
			
			$("body").droppable({
				greedy: false,
				accept: '.cmpMarker',	
				scope: 'removeFromComp',			
				drop: function(event, ui) {
					removeFromComparison($(this), ui.draggable);
				}
			});
			
			$(".cmpMarker").draggable({ 
				opacity: 0.7, 
				helper: 'clone',
				zIndex: 10000, 
				appendTo: 'body',
				revert: 'invalid', 
				scope: 'removeFromComp'
			});
			
			// opens comparison page
			$('#btnCompareProducts').click(function (){			
				var comparedProducsAmount = document.getElementById('comparedProducsAmount').value;
				if(comparedProducsAmount > 1){
					showPopup('comparateurPopup');
					$("#comparateurPopup").load(comparisonContainerUrl, function(){
						setNewCompPosition('comparateurPopup');
					});
				} else if(comparedProducsAmount == 1) {
					alert("Vous devez sélectionner au moins deux produits.");
				} else {
					alert("Aucun produit dans le comparateur.");
				}				
		    });
		    
		    // clears comparison block
		    $('#btnClearComparator').click(function (){			
				var images = $("div#campExpanded a.campRemove");					
				jQuery.each(images, function(i, val) {
			    	var productId = val.id.substring(5);
					var placeholder = $(val).parent('div');
					var productIndex = placeholder.attr("id");
					doRemoveFromComparison(placeholder, productId, productIndex);
			    });			
		    });
		    
		});
		
		function prepareDraggables() {
			$(".prdMarker").draggable({ 
				opacity: 0.7, 
				helper: 'clone',
				zIndex: 10000, 
				appendTo: 'body', 
				revert: 'invalid', 
				scope: 'addToComp',
				cursorAt: { cursor: 'move', top: 46, left: 64 },
				refreshPositions: true,
				start: function (event, ui){
					$(".prdMarker .prodHighlight").hide();
				    $(".prdMarker .priceSection").hide();
				    $(".prdMarker .imgDesc").hide();
				    $(".prdMarker .addToCartSection").hide();
				}			
			});	
		}
		
		function addImage(holder, item) {	
			var skuId = item.attr("id");
			var productId = item.attr("productId");
			var altName = item.attr("alt");
			// item is dragged from carousel, it is not an image, but <div> with image inside
			if(! skuId){
				var skuId = item.find(".draggableImage").attr("id");
				var productId = item.find(".draggableImage").attr("productId");
				var altName = item.find(".draggableImage").attr("alt");
			}
			
			if (checkIndexOf(skuId) == -1) {				
				arrayOfProducts[arrayOfProducts.length] = skuId;
			} else {
				return;
			}					
					
			holder.removeClass("ui-droppable");
			holder.removeClass("droppable");
			holder.droppable('option', 'accept', '');
			
			var imageSrc = item[0].src;
			if(! imageSrc){
				var imageSrc = item.find(".draggableImage").attr("src");
			}
						
			var newimage = $("<img src='" +  imageSrc + "' width='100' height='100'/>");
			newimage.attr("title", altName);
			newimage.appendTo(holder);
			
			var removeLink = document.createElement("a");
			$(removeLink).attr("class", "campRemove").attr("title", "Remove").attr("href", "javascript:void(0)").attr("id", "comp_" + skuId).text("Remove").click(function (ev) {removeImage(ev)});
			holder.append(removeLink);
			var productIndex = holder.attr("id");
			
			$.ajax ({
				url: comparisonMngUrl,
				data: "action=add&skuId=" + skuId + "&productId=" + productId + "&productIndex=" + productIndex,				
				cache: false
			});
			item.draggable({ opacity: 0.7, helper: 'clone',zIndex: 10000, appendTo: 'body',revert: 'invalid'});
			increaseProductsAmount();
		}
		
		function removeImage(ev) {	
			var target = $(ev.target);
			var placeholder = target.parent('div');
			var skuId = target.attr("id").substring(5);
			var productIndex = placeholder.attr("id");	
			doRemoveFromComparison(placeholder, skuId, productIndex);						
		}	
		
		function removeFromComparison(holder, placeholder) {
			var productIndex = placeholder.attr("id");
			var picLink = placeholder.contents().filter("a");
			if(picLink.length != 0){
				var skuId = picLink.attr("id").substring(5);
				doRemoveFromComparison(placeholder, skuId, productIndex);
			}
		}
		
		function doRemoveFromComparison(placeholder, skuId, productIndex) {
			placeholder.empty();
			placeholder.droppable({
				accept: '.prdMarker',				
				drop: function(event, ui) {
					addImage($(this), ui.draggable);
				}
			});
			placeholder.droppable('option', 'accept', '.prdMarker');	
			placeholder.addClass("ui-droppable");
			placeholder.addClass("droppable");
			placeholder.removeClass("rendered");
			$.ajax ({
				url: comparisonMngUrl,
				data: "action=remove&productIndex=" + productIndex,				
				cache: false
			});
			
			if (checkIndexOf(skuId) != -1) {
				arrayOfProducts.splice(checkIndexOf(skuId), 1);
			}
			
			decreaseProductsAmount();
		}
		
		function increaseProductsAmount() {
		    var comparedProducsAmount = document.getElementById('comparedProducsAmount');
		    var value = comparedProducsAmount.value;
		    comparedProducsAmount.value = 1 + parseInt(value);
		}
		function decreaseProductsAmount() {
		    var comparedProducsAmount = document.getElementById('comparedProducsAmount');
		    var value = comparedProducsAmount.value;
		    comparedProducsAmount.value = value - 1;
		}
		
		function checkIndexOf(searchString){
			var found = -1;
	
			for (var i = 0; i < arrayOfProducts.length; i++) {
			  if (arrayOfProducts[i] == searchString) {
			    found = i;
			    break;
			  }
			}
			return found;
		
		}
		
		function addProductLink(params) {

			var comparedProducsAmount = document.getElementById('comparedProducsAmount');
		    var value = comparedProducsAmount.value;
			if(value == 4){
				alert("Vous ne pouvez comparer que 4 produits maximum.");
				return;
			}
	
			var productId = params.productId;
			var imageSrc = params.skuImage;
			var skuId = params.skuId;
			var altName = params.altName;
			
			var campExpaned = document.getElementById('campExpanded');
			var campCollapsed = document.getElementById('campCollapsed');

			if (campExpaned.style.display != "block") {						
				campExpaned.style.display = "block";
				campCollapsed.style.display = "none";						
			} 					
			
			var holders = $(".campProduct");
			var holder;
			jQuery.each(holders, function(i, val) {
				holder = $(val);
				if(holder[0].childNodes.length == 0){
					return false;
				}
		    });
			
			
			if (checkIndexOf(skuId) == -1) {				
				arrayOfProducts[arrayOfProducts.length] = skuId;
			} else {
				return;
			}					
					
			holder.removeClass("ui-droppable");
			holder.removeClass("droppable");
			holder.droppable('option', 'accept', '');
			
			var newimage = $("<img src='" +  imageSrc + "' width='100' height='100'/>");
			newimage.attr("title", altName);
			newimage.appendTo(holder);
			
			var removeLink = document.createElement("a");
			$(removeLink).attr("class", "campRemove").attr("title", "Remove").attr("href", "javascript:void(0)").attr("id", "comp_" + skuId).text("Remove").click(function (ev) {removeImage(ev)});
			holder.append(removeLink);
			var productIndex = holder.attr("id");
			
			$.ajax ({
				url: comparisonMngUrl,
				data: "action=add&skuId=" + skuId + "&productId=" + productId + "&productIndex=" + productIndex,				
				cache: false
			});
			
			increaseProductsAmount();
		}