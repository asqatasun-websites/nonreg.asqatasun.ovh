/****************************************************************************************
'
' JAVASCRIPT.JS
'
' Name:         URLHistory.js
' Description:  Creates URL History hyperlinks using URL History substring stored in user cookie and 
'               updates cookie upon page load
'           
*****************************************************************************************
'
' FUNCTIONS
'
' Name:         UHGetCookieVal
' Description:  Gets URL History substring from from cookie using index number of URL History
' Accepts:      offset - index number in string for the URL History cookie
' Returns:      URL History Cookie substring
'
' ...
'
' Name:         UHGetCookie
' Description:  Searches for index number of URL History String within the Cookie
' Accepts:      name - name of substring to search for
' Returns:      If substring is found returns substring else returns an empty string
'
' ...
'
' Name:         GetUHList
' Description:  Uses UHGetCookie method to retieve URL History substring then splits the substring
'               into each link properties, creating an array of Links for the URL History Control
' Accepts:      N/A
' Returns:      URL HistoryArray - Array of links to be used for the creation of hyperlinks
'
' ...
'
' Name:         UHCreateLinks
' Description:  Using array created from URL History substring, creates html hyperlinks with
'               URL History cookie string. Gets URL and Display name of each link and generates
'               hyperlinks in html format.
' Accepts:      N/A
' Returns:      N/A
'
' ...
'
' 
' Name:         UHMatchPage
' Description:  Matches/Inserts current page into URL History cookie string, creates URL History string
'               to be passed to update function of cookie
' Accepts:      N/A
' Returns:      N/A
'
' ...
'
' Name:         UHUpdateCookie
' Description:  Retrieves Cookie string passed and updates cookie w/ string recieved
' Accepts:      cookieString - Updated URL History cookie string to be stored in cookie
' Returns:      N/A
'
*****************************************************************************************
'
' REVISION HISTORY
'
' Date Created: 01/05/2005
' Author:       NND
' 
' Revision Date      Author         Description
' -------------      ------         -----------
' 10/30/06			Alvin S. Moreno	RMT 56 Fix - Added tracking of Referrer and External Referrer Urls
' 05/12/09          Eduard H. Anglo  RMT 4173 - Increase tracking History
'
'
****************************************************************************************/
<!--
//Set cookie substring variable for creation of URL History links string
var _UHcookieSubstring;
//Set Current Page URL and Title variable seperated by hyphen  	
var _UHcurrentPage = document.title + "~" + unescape(document.location.href.toLowerCase()) 
//Set URL History key value to be used in accessing and saving URL History data
var _UHCookieKey = "URLHistory";
var _UHLinks = new Array();
var _UHLinkDefinitions = new Array(); 
var _UHArray = new Array();
//Set URL History Links max Length variable
var _UHMaxLength = 15;
//Set checker variable to determine if condition is false
var	_checker = -1;

//Set cookie string for External Referrer URL
var _ERUrlString;
//Set External Referrer URL key value
var _ERCookiekey = "External_ReferrerURL";
//Set External Referrer Page URL 
var _ExternalReferrerUrl = document.referrer;
//Set Referrer Page URL 
var _ReferrerUrl = document.referrer;
//Set Referrer URL key value
var _RCookiekey = "ReferrerURL";

//Set Cookie Expiration - 24 hours
var _expireDate = new Date();
_expireDate.setHours(_expireDate.getHours()+24);
	
_UHLinkDefinitions = GetUHList();
UHCreateLinkArray()
UHMatchPage();

if (top.location != self.location)
{
	top.location = self.location
}

//Check if there is a referrer page
if (_ExternalReferrerUrl.length > 0)
    {
    //Function to set External Referrer URL cookie
    SetExternalReferrerUrlCookie();    
    }
    
//Function to set Referrer URL cookie
SetReferrerUrlCookie();

//get cookie URL History substring in cookie and removes ASP.NET session id from cookie string
function UHGetCookieVal(offset) 
{
	var endstr = unescape(document.cookie.indexOf (";", offset));
	if (endstr == _checker)
	{
		endstr = document.cookie.length;
	}
	return unescape(document.cookie.substring(offset, endstr));
}

//Search cookie for "URL History" string to get URL History cookie string
function UHGetCookie(name) 
{
	//Set argument to search for in cookie to parameter passed in calling method
	var _argument = name;
	var _argumentLength = _argument.length;
	var _cookieLength = document.cookie.length;
	var _cookieIndex = 0;
	
	// Loop through URL History String for 'URLHistory' substring
	while (_cookieIndex < _cookieLength) 
	{
		var _endingIndex = _cookieIndex + _argumentLength;
		
		// If URL History argument is found in cookie string get URL History substring in cookie and break
		if (document.cookie.substring(_cookieIndex, _endingIndex) == _argument)
		{
			return UHGetCookieVal(_endingIndex);
			break;
		}
		
		_cookieIndex++;
	}
	
	// Return empty cookie string if URL History substring is not found
	return "";
}

function GetUHList()
{
	var extRef = unescape(_ExternalReferrerUrl);
		
	// Get URL History substring in cookie
	_UHcookieSubstring = unescape(UHGetCookie(_UHCookieKey + '='));
		
	if (extRef != "")
	{
		if (extRef.indexOf(window.location.host) < 0)
		{  
			//Check if the URL contains querystrings
			if (extRef.indexOf("?") > 0)
			{
				//Remove the querystrings
				extRef = extRef.substring(0,extRef.indexOf("?"));
			}   
			_UHcookieSubstring = extRef + " " + "|" + _UHcookieSubstring  
		}
	}
	extRef = "";
	
	// Split URL History substring into Link Definitions
	_UHArray = _UHcookieSubstring.split("|");

	// return Links Definition Array to calling object
	return _UHArray;
}

function UHMatchPage()
{ 
	var _cookie="";
	var _match;
	var _end = 0;
	
	// match current Page with URL History string
	_match = _UHcookieSubstring.indexOf(_UHcurrentPage + "|"); 
	
	preUrl = UHUrlChecker(_UHcookieSubstring)
	curUrl = UHUrlChecker(_UHcurrentPage)
	
	urlMode = _UHcurrentPage.match("nrmode")
	
	if (urlMode == "nrmode" || preUrl == curUrl){return}
	
	// if number of links is not equal to five, insert current page to links
	// else remove last link and insert current page
	if(_UHLinks.length!=_UHMaxLength)
	{	
		//Check the lenght of the cookie,
		//This is an alternative approach to check the size of the cookie
		if (document.cookie.length > 4095) 
			{	
				//Get index of last link seperator (pipeline)
				_end = _UHcookieSubstring.lastIndexOf("|");
				//Remove last Pipeline from cookieSubstring variable
				_UHcookieSubstring = _UHcookieSubstring.substring(0,_end);
				//Get new last index of seperator (Pipeline)
				_end = _UHcookieSubstring.lastIndexOf("|");
				//Remove last link string from cookieString using seperator index
				_UHcookieSubstring = _UHcookieSubstring.substring(0,_end+1);
				
				_cookie = _UHcurrentPage + "|" + _UHcookieSubstring;
				
			}	
		_cookie = _UHcurrentPage + "|" + _UHcookieSubstring;
	}
	else
	{	
		//Get index of last link seperator (pipeline)
		_end = _UHcookieSubstring.lastIndexOf("|");
		//Remove last Pipeline from cookieSubstring variable
		_UHcookieSubstring = _UHcookieSubstring.substring(0,_end);
		//Get new last index of seperator (Pipeline)
		_end = _UHcookieSubstring.lastIndexOf("|");
		//Remove last link string from cookieString using seperator index
		_UHcookieSubstring = _UHcookieSubstring.substring(0,_end+1);
		
		_cookie = _UHcurrentPage + "|" + _UHcookieSubstring;
		
	}
	
	//Update cookie w/ URL History string
	UHUpdateCookie(_cookie);

	//Clean Variables
	_cookie = "";
	_match=0;
	_end=0;
}
	
//Update Cookie w/ cookieString
function UHUpdateCookie(value)
{
	var _updatedCookie = _UHCookieKey  + "=" + escape(value) + "; expires=" + _expireDate.toGMTString() + "; Path=/";
	document.cookie = _updatedCookie;
	
	//Clean Variables
	_updatedCookie = "";
}



function UHCreateLinkArray()
	{	
	
	//document.write(document.cookie())
	// For each link definition in array, create html hyperlink	
	for(var _counter=0;_counter<_UHLinkDefinitions.length-1;++_counter)
		{									
		var _link = _UHLinkDefinitions[_counter].split("~");		
		
		_UHLinks[_counter] = _link;
		} 
	
	// return Links array to calling object	
	return _UHLinks; 
	
	
	}	

function UHUrlChecker(Url)
	{
		var cookieArray = new Array();
		var pageUrl;
		var pageTitle
				
		cookieArray = Url.split("|");
		
		if (cookieArray != null)
			{
				pageUrl = cookieArray[0];
			}
		else
			{
				pageUrl = Url
			}
		
		pageUrl = pageUrl.replace("http://","");
		pageUrl = pageUrl.replace("https://","");
		pageTitle = pageUrl.substring(0,pageUrl.indexOf("~"));
		pageUrl = pageTitle + pageUrl.substring(pageUrl.indexOf("/"));
		
		return(pageUrl)
	}

//Function to set Cookie for External_ReferrerURL
function SetExternalReferrerUrlCookie()
{
	//Get the value of the referrer page
    _ERUrlString = unescape(_ExternalReferrerUrl);
    
    //Check if the URL contains querystrings
    if (_ERUrlString.indexOf("?") > 0)
        {
        //Remove the querystrings
        _ERUrlString = _ERUrlString.substring(0,_ERUrlString.indexOf("?"));
        }
    
    //Check if the URL is from an external site
    if (_ERUrlString.indexOf(window.location.host) < 0)
        {
			//Set the cookie with the external referrer url
            var _newCookie = _ERCookiekey + "=" + unescape(_ExternalReferrerUrl) + "; expires=" + _expireDate.toGMTString() + "; Path=/";
            document.cookie = _newCookie;
            _newCookie = "";
            
        }
    
}

//Function to set Cookie for ReferrerURL
function SetReferrerUrlCookie()
{
	var curUrlMode = _UHcurrentPage.toLowerCase().indexOf("nrmode");
	var refUrlMode = _ReferrerUrl.toLowerCase().indexOf("nrmode");
	var referrer = "";

	//Get the previous URL
	var prevUrl = _ReferrerUrl;
	//Get the URL path of the current request
	var curUrl = document.location.pathname;

	//Get the offset for the hostname in the previous URL
	var hostName = document.location.hostname;
	var offset = prevUrl.indexOf(hostName) + hostName.length;

	//If offset has negative value, set it to 0
	if (offset < 0){offset = 0;}

	//Get the query string in previous URL
	var qStringIndex = prevUrl.indexOf("?");
	
	//If there's no query string in previous URL, set the index to the end of string
	if (qStringIndex < 0){qStringIndex = prevUrl.length;}

	//Get the URL path of the previous URL without hostname and query strings
	prevUrl = prevUrl.substring(offset, qStringIndex);

	//If previous URL and current URL is the same, do not update the cookie
	if (prevUrl != curUrl){

		//If URL is postback, do not update the cookie
		if((curUrlMode<0)&&(refUrlMode<0)){
		
			//If referrer page is empty, use the last value in URL history
			if (_ReferrerUrl.length>0){
				referrer = _ReferrerUrl;
			}
			else if (_UHLinks.length>0){
				referrer = _UHLinks[0][1];
			}

			//Set the cookie with the external referrer url
			var _newCookie2 = _RCookiekey + "=" + unescape(referrer) + "; expires=" + _expireDate.toGMTString() + "; Path=/";
			document.cookie = _newCookie2;
			_newCookie2 = "";
		}
	}
}

//Clean Variables         
_UHLinks = null;
_UHcurrentPage = "";
_UHCookieKey="";
_ERCookieKey="";
_RCookiekey="";
_ExternalReferrerUrl="";
_ReferrerUrl="";