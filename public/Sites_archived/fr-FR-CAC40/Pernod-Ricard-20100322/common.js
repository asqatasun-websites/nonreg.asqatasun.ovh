function getElement(elementId)
{
	return document.getElementById(elementId);
}

function openPopup(pageUrl, popupName, popupWidth, popupHeight, showScrollbar) 
{
	var iWidth = popupWidth;
	var iHeight = popupHeight;
	var leftVal = (screen.width-iWidth)/2;
	var topVal = (screen.height-iHeight)/2;
	
	if (showScrollbar == true) 
	{
		newWindow = window.open(pageUrl, popupName, 'width='+iWidth+',height='+iHeight+',left='+leftVal+',top='+topVal+',scrollbars=yes');
	}
	else
	{
		newWindow = window.open(pageUrl, popupName, 'width='+iWidth+',height='+iHeight+',left='+leftVal+',top='+topVal+',scrollbars=no');
	}
	
	if (parseInt(navigator.appVersion) >= 4) 
	{ 
		newWindow.window.focus(); 
	}

	return false;
}

function checkPremiumSize()
{
	var width;
	var height;

	if (document.layers) 
	{
		width = window.innerWidth;
		height = window.innerHeight;
	} 
	else 
	{
		width = document.body.clientWidth;
		height = document.body.clientHeight;
	} 
	
	if ((width != 759) || (height != 600))
	{
		setTimeout("resizeOuterTo(759,600)", 200);
	}
}

function resizeOuterTo(w,h) 
{
	if (parseInt(navigator.appVersion)>3) 
	{
		if (navigator.appName=="Netscape") 
		{
			top.outerWidth=w;
			top.outerHeight=h+40;
		} 
		else
		{
			top.resizeTo((w+12),(h+30));
		}
	}
}
