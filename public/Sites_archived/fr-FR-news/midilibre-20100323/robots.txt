User-agent: * 
Disallow: /ajax/ 
Disallow: /includes/
Disallow: /js/
Disallow: /pnw/
Disallow: /ssi/
Disallow: /styles/
Disallow: /tags/
Disallow: /css/
Disallow: /forumv3/
Disallow: /video/
Disallow: /meteo/
Disallow: /dark/
Disallow: /admin/
Disallow: /ecriture_old/
Disallow: /energaia/
Disallow: /blogs/archives
Disallow: /blogs/fonctions
Disallow: /blogs/includes
Disallow: /blogs/js
Disallow: /blogs/ssi
Disallow: /blogs2/
Disallow: /carnet/admin
Disallow: /carnet/js
Disallow: /classements/
Disallow: /concours/admin/
Disallow: /concours/images/
Disallow: /concours/ssi/
Disallow: /concours/vote/
Disallow: /concours2008/admin/
Disallow: /concours2008/images/
Disallow: /concours2008/ssi/
Disallow: /concours2008/vote/
Disallow: /mobile-rss/
Disallow: /newsletters/
Disallow: /recherche/
Disallow: /TagCloud/
Disallow: /ts_files/
Disallow: /userfiles/
Disallow: /rss/
Disallow: /sitemap.xml


