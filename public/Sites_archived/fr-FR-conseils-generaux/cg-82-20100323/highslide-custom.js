hs.graphicsDir = '/js/highslide/graphics/';
hs.outlineType = 'rounded-white';
hs.wrapperClassName = 'draggable-header no-footer';
hs.allowSizeReduction = false;
hs.dimmingOpacity = 0.5;

hs.lang = {
	cssDirection: 'ltr',
	loadingText : 'Chargement...',
	loadingTitle : 'Cliquer pour annuler',
	focusTitle : 'Cliquez pour mettre au 1er plan',
	fullExpandTitle : 'Agrandir � la taille actuelle (f)',
	creditsText : '',
	creditsTitle : '',
	previousText : 'Pr�c�dent',
	nextText : 'Suivant', 
	moveText : 'D�placer',
	closeText : 'Fermer', 
	closeTitle : 'Fermer (echap)', 
	resizeTitle : 'Redim.',
	playText : 'Jouer',
	playTitle : 'Jouer le diaporama (espace)',
	pauseText : 'Pause',
	pauseTitle : 'Pause (espace)',
	previousTitle : 'Pr�c�dent (fl�che gauche)',
	nextTitle : 'Suivant (fl�che droite)',
	moveTitle : 'D�placer',
	fullExpandText : 'Plein �cran',
	number: 'Image %1 sur %2',
	restoreTitle : 'Click to close image, click and drag to move. Use arrow keys for next and previous.'
};
	