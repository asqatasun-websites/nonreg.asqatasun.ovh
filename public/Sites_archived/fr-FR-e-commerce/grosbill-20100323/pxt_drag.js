var slots_id =new Array();
var deltaX=-1;
var deltaY=-1;
var div_nom_minipanier='minipanier_drop';
var ispxtdrag=0;
var pxt_nbslots=4;
var pxt_sel_swap_slot,pxt_sel_swap_prod;
//var pxt_dd_swap='replace';
var pxt_dd_swap='autofill';
var tmpid_save=0;

Number.prototype.NaN0=function(){return isNaN(this)?0:this;}

YAHOO.DDProd = function(id, sGroup, config) {
    this.initProd(id, sGroup, config);
};

// YAHOO.DDProd.prototype = new YAHOO.util.DDProxy();
YAHOO.extend(YAHOO.DDProd, YAHOO.util.DDProxy);
YAHOO.DDProd.TYPE = "DDProd";

YAHOO.DDProd.prototype.initProd = function(id, sGroup, config) {
    if (!id) { return; }

    this.init(id, sGroup, config);
    this.initFrame();

    this.logger = this.logger || YAHOO;
    var el = this.getDragEl()
    YAHOO.util.Dom.setStyle(el, "borderColor", "transparent");
    YAHOO.util.Dom.setStyle(el, "opacity", 0.76);

    // specify that this is not currently a drop target
    this.isTarget = false;

    this.originalStyles = [];

    this.type = YAHOO.DDProd.TYPE;
    this.slot = null;

    this.startPos = YAHOO.util.Dom.getXY( this.getEl() );
    this.logger.log(id + " startpos: " + this.startPos);
};

YAHOO.DDProd.prototype.startDrag = function(x, y) {
    this.logger.log(this.id + " startDrag");
    var Dom = YAHOO.util.Dom;
	var dragEl = this.getDragEl();
    var clickEl = this.getEl();

	deltaX=this.deltaX;
	deltaY=this.deltaY;

	pic=new Image;
	pic.src=GetUrlPng50(clickEl);

    dragEl.innerHTML = clickEl.innerHTML;
    dragEl.className = clickEl.className;
    Dom.setStyle(dragEl, "color",  Dom.getStyle(clickEl, "color"));
    Dom.setStyle(dragEl, "backgroundColor", Dom.getStyle(clickEl, "backgroundColor"));
    Dom.setStyle(clickEl, "opacity", 0.1);

    var targets = YAHOO.util.DDM.getRelated(this, true);
    this.logger.log(targets.length + " targets");
    ispxtdrag=1;
    if(pxtmode.indexOf('visuels')!=-1){
		hide_div_produit(1);
	}
};

YAHOO.DDProd.prototype.getTargetDomRef = function(oDD) {
    if (oDD.Prod) {
        return oDD.Prod.getEl();
    } else {
        return oDD.getEl();
    }
};
YAHOO.DDProd.prototype.b4EndDrag = function(e) {
    YAHOO.util.Dom.setStyle(this.getDragEl(), "visibility", "hidden");
}
YAHOO.DDProd.prototype.endDrag = function(e) {
    // reset the linked element styles
    YAHOO.util.Dom.setStyle(this.getEl(), "opacity", 1);
    this.resetTargets();
    ispxtdrag=0;
};

YAHOO.DDProd.prototype.resetTargets = function() {

    // reset the target styles
    var targets = YAHOO.util.DDM.getRelated(this, true);
    for (var i=0; i<targets.length; i++) {
        var targetEl = this.getTargetDomRef(targets[i]);
        var oldStyle = this.originalStyles[targetEl.id];
        if (oldStyle) {
            targetEl.className = oldStyle;
        }
    }
};

YAHOO.DDProd.prototype.onDragDrop = function(e, id) {
    // get the drag and drop object that was targeted

    var oDD, isswap;

    if ("string" == typeof id) {
        oDD = YAHOO.util.DDM.getDDById(id);
    }
	else {
        oDD = YAHOO.util.DDM.getBestMatch(id);
    }
    var el = this.getEl();

    if (id==div_nom_minipanier){
		var sid=PXT_dd_getGBprodid(el);
    	//eval("buyItem_"+sid+"()");
    	var nom_tmp, prix_tmp, poids_tmp;
	eval("poids_tmp = document.itemsform_"+sid+".poids_"+sid+".value");
	eval("prix_tmp = document.itemsform_"+sid+".prix_"+sid+".value");
	eval("nom_tmp = document.itemsform_"+sid+".nom_"+sid+".value");
	rbuyItem(nom_tmp, prix_tmp, poids_tmp, sid, 1);
	//YAHOO.util.Dom.setStyle(el,'display','none');
    }
    else if(document.getElementById(id).id.substring(0,4)=='ddt_'){
 	// alert(id);
	document.getElementById(id).className = "pxt_bg_produit_ma_selection";
	var pxt_cslot=parseInt(oDD.getEl().id.substring(4,oDD.getEl().id.length));
	//si il y'a deja un prod
	if(slots_id[pxt_cslot]>0){
		if(pxt_dd_swap.indexOf('replace')>-1 && PXT_sel_getemptyslot()>-1){
			if(pxtmode.indexOf('visuels')!=-1){
				var old_block = document.getElementById('dd_'+slots_id[pxt_cslot]);
				if(old_block){
					old_block.style.display='inline';
				}
			}
			else if(pxtmode.indexOf('listing')!=-1){
				var old_block = document.getElementById('prod_'+slots_id[pxt_cslot]);
				if(old_block){
					old_block.style.display='';
				}
			}
			oDD.getEl().childNodes[0].src='http://images.grosbill.com/imagesproduitnew/images50jpg/'+PXT_dd_getGBpict50(el);
			slots_id[pxt_cslot]=parseInt(PXT_dd_getGBprodid(el));
		}
		else if(PXT_sel_getemptyslot()<0){
			PXT_sel_swapshow(pxt_cslot);
			pxt_sel_swap_prod=PXT_dd_getGBprodid(el);
			isswap=1;
		}
		else{
			pxt_cslot=PXT_sel_getemptyslot();
			document.getElementById('ddt_'+pxt_cslot).childNodes[0].src='http://images.grosbill.com/imagesproduitnew/images50jpg/'+PXT_dd_getGBpict50(el);
			slots_id[pxt_cslot]=parseInt(PXT_dd_getGBprodid(el));
		}
	}
	else{
		oDD.getEl().childNodes[0].src='http://images.grosbill.com/imagesproduitnew/images50jpg/'+PXT_dd_getGBpict50(el);
		slots_id[pxt_cslot]=parseInt(PXT_dd_getGBprodid(el));
	}
	//on cache le produit
	if(isswap!=1){
		if(pxtmode.indexOf('visuels')!=-1){
			YAHOO.util.Dom.setStyle(el,'display','none');
		}
		else if(pxtmode.indexOf('listing')!=-1){
			YAHOO.util.Dom.setStyle(document.getElementById('prod_'+PXT_dd_getGBprodid(el)),'display','none');
		}
	}
	//ont update le slot

	PXT_sel_update();
	this.resetTargets();
	}
	ispxtdrag=0;
};
/*
YAHOO.DDProd.prototype.swap = function(el1, el2) {
    var Dom = YAHOO.util.Dom;
    var pos1 = Dom.getXY(el1);
    var pos2 = Dom.getXY(el2);
    Dom.setXY(el1, pos2);
    Dom.setXY(el2, pos1);
};*/

function GetUrlPng50(el)
{
	return "http://images.grosbill.com/imagesproduitnew/images50jpg/"+PXT_dd_getGBpict50(el);
}

function setMiniPanierBorder(border_color)
{
	if (document.getElementById(div_nom_minipanier))
	{
		document.getElementById(div_nom_minipanier).style.borderWidth='2px';
		document.getElementById(div_nom_minipanier).style.borderColor=border_color;
		document.getElementById(div_nom_minipanier).style.borderStyle='solid';
	}
}

YAHOO.DDProd.prototype.onDragOver = function(e, id) {
	var clickEl = this.getEl();
	if (id!=div_nom_minipanier)
		document.getElementById(id).className = "pxt_bg_produit_ma_selection_on";
	else
		setMiniPanierBorder('#9C9C9C');

	var dragEl = this.getDragEl();

	//dragEl.innerHTML='<img src=\'http://images.grosbill.com/imagesproduitnew/images50jpg/'+PXT_dd_getGBpict50(clickEl)+'\'>';
	dragEl.innerHTML="<img src='"+GetUrlPng50(clickEl)+"'>";
        this.setDelta(25,25);
};

function GetCloser(v,close_to)
{
	if (v==close_to)
		return v;

	if (v>close_to)	{
		v=v-2;
	}
	if (v<close_to) {
		v=v+2;
	}
	return v;
}

YAHOO.DDProd.prototype.onDragOut = function(e, id) {
	if (id!=div_nom_minipanier)
		document.getElementById(id).className = "pxt_bg_produit_ma_selection";
	else
		setMiniPanierBorder('#FFFFFF');

	var clickEl = this.getEl();
	var dragEl = this.getDragEl();
	dragEl.innerHTML = clickEl.innerHTML;
	// this.setDelta(GetCloser(this.deltaX,deltaX),GetCloser(this.deltaY,deltaY));
	this.setDelta(deltaX,deltaY);
};

YAHOO.DDProd.prototype.onDrag = function(e, id) {
//var dragEl = this.getDragEl();
};



YAHOO.DDApp = function() {
    var slots = [];
    var Prods = [];
    var Event = YAHOO.util.Event;
    var DDM = YAHOO.util.DDM;
    return {
        init: function() {
            // slots
			var tmpslot='';
			var islot=0;

//PXT_sel_fixvar();
//PXT_typeid=217;

			setMiniPanierBorder('#FFFFFF');
			for(islot=0; islot<pxt_nbslots; islot++){
				tmpslot+='<div class=\'pxt_bg_produit_ma_selection\' id=\'ddt_'+islot+'\'><img src=\'/bk.gif\' id=\'ddti_'+islot+'\'/></div><div class=\'pb05\'></div>';
				slots[islot] = new YAHOO.util.DDTarget("ddt_"+islot, "ddt");
				slots_id[islot]=0;
			}
			slots[islot] = new YAHOO.util.DDTarget(div_nom_minipanier,"ddt");
			islot++;
			try
			{
				document.getElementById('slotplace').innerHTML=tmpslot;
				var iprod=0;
				if(pxtmode.indexOf('visuels')!=-1){
					var tmpddprod=document.getElementById("prodcell").getElementsByTagName('div');
				}
				else if(pxtmode.indexOf('listing')!=-1){
					var tmpddprod=document.getElementById("prodcell").getElementsByTagName('tr');
				}
				if(pxtmode.indexOf('visuels')!=-1 || pxtmode.indexOf('listing')!=-1){
					for (i=0; i<tmpddprod.length; i++){
						if(pxtmode.indexOf('visuels')!=-1){
							if(tmpddprod[i].id.indexOf('dd_')!=-1){
								Prods[iprod] = new YAHOO.DDProd('dd_'+tmpddprod[i].id.substring(3,tmpddprod[i].id.length)+'', "ddt");
								iprod++;
							}
						}
						else if(pxtmode.indexOf('listing')!=-1){
							if(tmpddprod[i].id.indexOf('prod_')!=-1){
								Prods[iprod] = new YAHOO.DDProd('dd_'+tmpddprod[i].id.substring(5,tmpddprod[i].id.length)+'', "ddt");
								iprod++;
							}
						}
					}
				}
				DDM.mode = 0;
				if(PXT_typeid>0){
					PXT_sel_restore();
				}
			}
			catch(e)
			{
			}
		}
    };
} ();


function PXT_dd_getGBpict50(id){
	var realid=id.id.substring(3,id.id.length);
	var tmppath=PXT_dd_getGBprodid(id)+'.jpg';
	return(tmppath);
}
function PXT_dd_getGBprodid(id){
	var realid=id.id.substring(3,id.id.length);
	return(realid);
}

function PXT_sel_update(){
   var i;
   //format du cookie :
   //#idtype#idprod;idprod;|#idtype#idprod;idprod
   var PXT_sel_typepersist = 10; //nombre de type a garder en cookie
   var PXT_sel_cookie = getCookie('PXT_sel_cookie');//cookie en cours
   var PXT_sel_Newcookie = '';//nouveau cookie
   //recup des data
   if(PXT_sel_cookie!=null){
      var PXT_sel_type = PXT_sel_cookie.split('|');
      //Reecriture du cookie
      var PXT_sel_nbtype=PXT_sel_type.length-1;
      if(PXT_sel_cookie.indexOf('#'+PXT_typeid+'#')==-1){PXT_sel_nbtype++;}
      if(PXT_sel_nbtype>0){
         if(PXT_sel_nbtype>PXT_sel_typepersist){
            var PXT_sel_offset=PXT_sel_nbtype-PXT_sel_typepersist;
         }
         else{var PXT_sel_offset=0;}
         for (i=0; i<PXT_sel_type.length; i++){
            if(PXT_sel_offset<=i){
               if(PXT_sel_type[i].indexOf('#'+PXT_typeid+'#')==-1 && PXT_sel_type[i].length>0){
                  PXT_sel_Newcookie+=PXT_sel_type[i]+'|';
               }
            }
         }
      }
   }
    PXT_sel_Newcookie+='#'+PXT_typeid+'#';
   var islot=0;
   for(islot=0; islot<pxt_nbslots; islot++){
   	if(parseInt(slots_id[islot])>0){
		PXT_sel_Newcookie+=parseInt(slots_id[islot])+';';
	}
   }
  PXT_sel_Newcookie+='|';
   setCookie('PXT_sel_cookie',PXT_sel_Newcookie,'Sat, 28-Nov-20 20:00:01 GMT','/','.grosbill.com','');
   PXT_sel_updatecompare();
   //alert(unescape('#'+PXT_typeid+'#'+parseInt(slots_id[0])+';'+parseInt(slots_id[1])+';'+parseInt(slots_id[2])+';'+parseInt(slots_id[3])+'|'));
}
function PXT_sel_restore(){
   var i;
   var restorestate=0;
   var PXT_sel_cookie = getCookie('PXT_sel_cookie');//cookie en cours
   //recup des data
   if(PXT_sel_cookie!=null){
      var PXT_sel_type = PXT_sel_cookie.split('|');
      PXT_typeid = document.typeselection.PXT_typeid.value;
	if(document.cookie.indexOf("karel_debug=")!=-1)
		alert(""+PXT_sel_type+" - "+document.typeselection.PXT_typeid.value);
	//Reecriture du cookie
      for (i=0; i<PXT_sel_type.length; i++){
         if(PXT_sel_type[i].indexOf('#'+PXT_typeid+'#')!=-1){
            slots_id=PXT_sel_type[i].substring(PXT_sel_type[i].lastIndexOf('#')+1,PXT_sel_type[i].length).split(';');
            restorestate=1;
         }
      }
      if(restorestate==1){
         for (i=0; i<slots_id.length; i++){i
            //alert(slots_id[i]);
            if(slots_id[i]>0){
               //ont cache le produit si il est encore sur la page
               if(pxtmode.indexOf('visuels')!=-1){
                var tmpdiv=document.getElementById('dd_'+slots_id[i]);
               }
               else if(pxtmode.indexOf('listing')!=-1){
               	var tmpdiv=document.getElementById('prod_'+slots_id[i]);
               }
               if(tmpdiv){
                  tmpdiv.style.display='none';
               }
               //ont met a jours le slot visible
               document.getElementById('ddt_'+i).childNodes[0].src='http://images.grosbill.com/imagesproduitnew/images50jpg/'+slots_id[i]+'.jpg';
            }
         }
      }

   }
   PXT_sel_updatecompare();
}
function setCookie(name, value, expires, path, domain, secure)
{

   document.cookie= name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}
function getCookie(name)
{
  var dc = document.cookie;
  var prefix = name + "=";
  var begin = dc.indexOf("; " + prefix);
  if (begin == -1)
  {
      begin = dc.indexOf(prefix);
      if (begin != 0) return null;
  }
  else
  {
      begin += 2;
  }
  var end = document.cookie.indexOf(";", begin);
  if (end == -1)
  {
      end = dc.length;
  }
  return unescape(dc.substring(begin + prefix.length, end));
}

function deleteCookie(name, path, domain)
{
  if (getCookie(name))
  {
        setCookie(name, '', 'Thu, 01-Jan-70 00:00:01 GMT', path, domain);
  }
}

function PXT_sel_remove(idtodel){
	var i,ib;
	//format du cookie :
	//#idtype#idprod;idprod;|#idtype#idprod;idprod
	var PXT_sel_typepersist = 10; //nombre de type a garder en cookie
	var PXT_sel_cookie = getCookie('PXT_sel_cookie');//cookie en cours
	var PXT_sel_Newcookie = '';//nouveau cookie
	//recup des data
	if(PXT_sel_cookie!=null){
		var PXT_sel_type = PXT_sel_cookie.split('|');
		//Reecriture du cookie
		var PXT_sel_nbtype=PXT_sel_type.length-1;
		if(PXT_sel_cookie.indexOf('#'+PXT_typeid+'#')==-1){PXT_sel_nbtype++;}
		if(PXT_sel_nbtype>0){
			if(PXT_sel_nbtype>PXT_sel_typepersist){
				var PXT_sel_offset=PXT_sel_nbtype-PXT_sel_typepersist;
			}
			else{var PXT_sel_offset=0;}
			for (i=0; i<PXT_sel_type.length; i++){
				if(PXT_sel_offset<=i){
					if(PXT_sel_type[i].indexOf('#'+PXT_typeid+'#')==-1 && PXT_sel_type[i].length){
						PXT_sel_Newcookie+=PXT_sel_type[i]+'|';
					}
					else{
						var tmpprods = PXT_sel_type[i].substring(PXT_sel_type[i].lastIndexOf('#'),PXT_sel_type[i].length).split(';');
						PXT_sel_Newcookie+='#'+PXT_typeid+'#';
						for (ib=0; ib<PXT_sel_type.length; ib++){
							tmpprods[i]=parseInt(tmpprods[i]);
							if(tmpprods[i]!=parseInt(idtodel) && tmpprods[i]>0){
								PXT_sel_Newcookie+=tmpprods[i]+';';
							} else {
								for (imgpos=0; imgpos<4; imgpos++) {
									if (document.getElementById('ddt_'+imgpos).childNodes[0].src.indexOf(idtodel+'.jpg') != -1) {
										document.getElementById('ddt_'+imgpos).childNodes[0].src='/bk.gif';
										slots_id[imgpos]=0;
									}
								}
							}
						}
						PXT_sel_Newcookie+='|';
					}
				}
			}
		}
	}
	setCookie('PXT_sel_cookie',PXT_sel_Newcookie,'Sat, 28-Nov-20 20:00:01 GMT','/','grosbill.com','');
	PXT_sel_updatecompare();
//	PXT_sel_restore();
//	PXT_sel_update();
}
function PXT_sel_add(tmpid){
	if(PXT_sel_getemptyslot()<0){
		PXT_sel_swapshow(-1);
		pxt_sel_swap_prod=tmpid;
		var isswap=1;
	}
	else{
		pxt_cslot=PXT_sel_getemptyslot();
		document.getElementById('ddt_'+pxt_cslot).childNodes[0].src='http://images.grosbill.com/imagesproduitnew/images50jpg/'+tmpid+'.jpg';
		slots_id[pxt_cslot]=parseInt(tmpid);
	}
	if(isswap!=1){
		if(pxtmode.indexOf('visuels')!=-1){
			YAHOO.util.Dom.setStyle(document.getElementById('dd_'+tmpid),'display','none');
		}
		else if(pxtmode.indexOf('listing')!=-1){
			YAHOO.util.Dom.setStyle(document.getElementById('prod_'+PXT_dd_getGBprodid(el)),'display','none');
		}
	}
	//ont update le slot
	PXT_sel_update();
}

function PXT_sel_getemptyslot(){
	var emptyslot=-1;
	var islot=0;
    for(islot=0; islot<pxt_nbslots; islot++){
		if(slots_id[islot]<1){
			return(islot);
		}
	}
	return emptyslot;
}
function PXT_sel_restoreid()
{
   var i;
   var PXT_sel_cookie = getCookie('PXT_sel_cookie');//cookie en cours
   //recup des data
   if(PXT_sel_cookie!=null)
   {
      var PXT_sel_type = PXT_sel_cookie.split('|');
      //Reecriture du cookie
      for (i=0; i<PXT_sel_type.length; i++)
	  {
         if(PXT_sel_type[i].indexOf('#'+PXT_typeid+'#')!=-1)
		 {
            return PXT_sel_type[i].substring(PXT_sel_type[i].lastIndexOf('#')+1,PXT_sel_type[i].length).split(';');
         }
      }
	  return 666;
   }
   else
   {//JP080829
	return 0;
   }
}
function PXT_sel_updatecompare(){
	var i;
	var tmplink="/comparaisonv2.cgi?univers=";
	var iprod=0;
	var ids = PXT_sel_restoreid();
	// FBI 	tmplink='/comparaisonv2.cgi?idtype='+PXT_typeid+'&univers='+PXT_univers;
	if(ids instanceof Array == true){
		for (i=0; i<ids.length; i++){
			ids[i]=parseInt(ids[i]);
			if(ids[i]>0){
				iprod++;
				tmplink+='&produit'+iprod+'='+ids[i];
			}
		}
		if(iprod>0){
			document.getElementById('place_comparer').innerHTML="<table><tr onclick=\"window.location='"+tmplink+"'\" class=\"pxt_hand\"><td><img src=\"/iv2/pxt_btn_orange_left.png\" alt=\"\" /></td><td class=\"pxt_btn_orange\" style=\"width:46px;padding:0\"><a href=\""+tmplink+"\" class=\"pxt_btn_orange_txt\">Comparer</a></td><td><img src=\"/iv2/pxt_btn_orange_right.png\" alt=\"\" /></td></tr></table>";
		}
	}
}

function PXT_sel_fixvar(){
	PXT_typeid=document.getElementById(PXT_typeid);
	PXT_univers=document.getElementById(PXT_univers);
}
function PXT_sel_swaphide(){
	var div = document.getElementById('pxtdivswap');
	div.style.display='none';
	div.style.visibility='hidden';
	ispxtdrag=0;
}
function PXT_sel_swapshow(swapslotid){
	var tmpdiv, corner,i;
	ispxtdrag=1;
	var body = document.body;
	var div = document.getElementById('pxtdivswap');
	if (!div) {
		div    = document.createElement("div");
		div.id = 'pxtdivswap';
		var s  = div.style;
		s.position   = "absolute";
		s.visibility = "hidden";
		s.display = "none";
		s.zIndex = 999;
		body.insertBefore(div, body.firstChild);
	}
	if(swapslotid>=0){
		corner='<td class="pxt_layer_t_left_co_on">';
	}
	else{
		corner='<td class="pxt_layer_t_left_co_off">';
	}
	tmpdiv='<table> <tr> '+corner+'<img src="/bk.gif" width="45" height="48" alt="" /></td> <td class="w01 pxt_layer_t_bg"></td> <td class="pxt_layer_t_right_co_off"><img src="/bk.gif" width="50" height="48" alt="" /></td> </tr> <tr> <td class="pxt_layer_cl_bg"></td> <td class="pb10 WH"><div class="pxt_layer_over"><table class="normalpetit"> <tr valign="top"> <td class="nomproduit2 pdg10">Remplacer le produit</td> </tr> <tr valign="top"> <td class="pdg10"><p>Vous avez atteind le maximum de produit que peut contenir la s&eacute;lection.</p> <p>Cliquez sur le produit que vous souhaitez remplacer puis validez.</p> <p class="pb10"></p><table> <tr>';
	for (i=0; i<pxt_nbslots; i++){
		if(slots_id[i]>0){
			tmpdiv+='<td class="pdg05 pxt_hand"><div class="pxt_bg_produit_ma_selection" id="ddswapt_'+i+'" align="center" onclick="PXT_sel_swapsel('+i+')"><img src=\'http://images.grosbill.com/imagesproduitnew/images50jpg/'+slots_id[i]+'.jpg\' id=\'ddswapti_'+i+'\'/></div></td> ';
		}
	}
	var lance_sel_swaplink="";
	if (tmpid_save!=0) {
		lance_sel_swaplink="PXT_sel_swaplink("+tmpid_save+");";
	}
	tmpdiv+='</tr> </table> <p class="pb10"></p> <table align="right"> <tr> <td class="pd10"><table> <tr onclick="PXT_sel_swap();" class="pxt_hand"> <td><img src="/iv2/pxt_btn_orange_left.png" alt="" /></td> <td class="pxt_btn_orange"><p class="pxt_btn_orange_txt" onclick=\'PXT_sel_swap();'+lance_sel_swaplink+'\'>Valider</p></td> <td><img src="/iv2/pxt_btn_orange_right.png" alt="" /></td> </tr> </table></td> <td class="pg10"><table> <tr onclick="PXT_sel_swaphide();" class="pxt_hand"> <td><img src="/iv2/pxt_btn_orange_left.png" alt="" /></td> <td class="pxt_btn_orange"><p class="pxt_btn_orange_txt" onclick=\'PXT_sel_swaphide();\'>Annuler</p></td> <td><img src="/iv2/pxt_btn_orange_right.png" alt="" /></td> </tr> </table></td> </tr> </table></td> </tr> </table></div></td> <td class="pxt_layer_cr_bg"></td> </tr> <tr> <td class="pxt_layer_b_left_co_off"><img src="/bk.gif" width="45" height="48" alt="" /></td> <td class="pxt_layer_b_bg"></td> <td class="pxt_layer_b_right_co_off"><img src="/bk.gif" width="50" height="48" alt="" /></td> </tr> </table> ';
	div.innerHTML=tmpdiv;
	div.style.display='';
	div.style.visibility='visible';
	if(swapslotid>=0){
		corner='<td class="pxt_layer_t_left_co_on">';
		YAHOO.util.Dom.setX('pxtdivswap',YAHOO.util.Dom.getX('ddt_'+swapslotid)+50);
		YAHOO.util.Dom.setY('pxtdivswap',YAHOO.util.Dom.getY('ddt_'+swapslotid));
		//ddt_
	}
	else{

		corner='<td class="pxt_layer_t_left_co_off">';
		YAHOO.util.Dom.setX('pxtdivswap',100);
		YAHOO.util.Dom.setY('pxtdivswap',200);

		/*YAHOO.util.Dom.setX('pxtdivswap',parseInt((YAHOO.util.Dom.getViewportWidth()/2)-(document.getElementById('ddt_'+swapslotid).width/2)));
		corner='<td class="pxt_layer_t_left_co_off">';
		YAHOO.util.Dom.setX('pxtdivswap',parseInt((YAHOO.util.Dom.getViewportWidth()/2)-(document.getElementById('ddt_'+swapslotid).width/2)));
		YAHOO.util.Dom.setY('pxtdivswap',parseInt((YAHOO.util.Dom.getViewportHeight()/2)-(document.getElementById('ddt_'+swapslotid).width/2)));*/
	}
	PXT_sel_swapsel(0);
}

function PXT_sel_swap(){
		if(pxtmode.indexOf('visuels')!=-1){
			var old_block = document.getElementById('dd_'+slots_id[pxt_sel_swap_slot]);
			if(old_block){
				old_block.style.display='inline';
			}
			YAHOO.util.Dom.setStyle(document.getElementById('dd_'+pxt_sel_swap_prod),'display','none');
		}
		else if(pxtmode.indexOf('listing')!=-1){
			var old_block = document.getElementById('prod_'+slots_id[pxt_sel_swap_slot]);
			if(old_block){
				old_block.style.display='';
			}
			YAHOO.util.Dom.setStyle(document.getElementById('prod_'+pxt_sel_swap_prod),'display','none');
		}
		document.getElementById('ddt_'+pxt_sel_swap_slot).childNodes[0].src='http://images.grosbill.com/imagesproduitnew/images50jpg/'+pxt_sel_swap_prod+'.jpg';
		slots_id[pxt_sel_swap_slot]=parseInt(pxt_sel_swap_prod);
		PXT_sel_update();
		PXT_sel_swaphide();

}
function PXT_sel_swapsel(tmpslotid){
	var i;
	pxt_sel_swap_slot=tmpslotid;
	for(i=0; i<pxt_nbslots; i++){
		document.getElementById('ddswapt_'+i).className = "pxt_bg_produit_ma_selection";
	}
	document.getElementById('ddswapt_'+tmpslotid).className = "pxt_bg_produit_ma_selection_on";
}
function PXT_sel_swaplink(tmpid){
	tmpid_save=tmpid;
	var ids = PXT_sel_restoreid();
	var isexist=0;
	var i=0;
	for (i=0; i<ids.length; i++){
		if(parseInt(ids[i])==parseInt(tmpid)){
			isexist=1;
		}
	}
	if(isexist==1){
		var tmpdiv='<table> <tr onclick="PXT_sel_remove('+tmpid+');PXT_sel_swaplink('+tmpid+');" class="pxt_hand"> <td><img src="/iv2/pxt_btn_noir_left.png" alt="" /></td> <td class="w01 pxt_btn_noir"><div onclick="PXT_sel_remove('+tmpid+');PXT_sel_swaplink('+tmpid+');" class="pxt_btn_noir_txt"><img src="/iv2/pxt_btn_noir_arrow.png" alt="" /></div></td> <td align="center" class="pxt_btn_noir"><div onclick="PXT_sel_remove('+tmpid+');PXT_sel_swaplink('+tmpid+');" class="pxt_btn_noir_txt">Retirer</a></td> <td><img src="/iv2/pxt_btn_noir_right.png" alt="" /></td> </tr></table>';
	}else{
		var tmpdiv='<table> <tr onclick="PXT_sel_add('+tmpid+');PXT_sel_swaplink('+tmpid+');" class="pxt_hand"> <td><img src="/iv2/pxt_btn_noir_left.png" alt="" /></td> <td class="w01 pxt_btn_noir"><div onclick="PXT_sel_add('+tmpid+');PXT_sel_swaplink('+tmpid+');" class="pxt_btn_noir_txt"><img src="/iv2/pxt_btn_noir_arrow.png" alt="" /></div></td> <td align="center" class="pxt_btn_noir"><div onclick="PXT_sel_add('+tmpid+');PXT_sel_swaplink('+tmpid+');" class="pxt_btn_noir_txt">S&eacute;lectionnez pour comparer</a></td> <td><img src="/iv2/pxt_btn_noir_right.png" alt="" /></td> </tr></table>';
	}
	document.getElementById("pxt_myswaplink").innerHTML=tmpdiv;
}

// ***********************************************************************************
// * Ajout d'une fonction pour le lien "selectionnez et comparer" - MK - 7/01/2009   *
// ***********************************************************************************
function PXT_sel_swaplink2(tmpid)
{
	tmpid_save=tmpid;
	var ids = PXT_sel_restoreid();
	var isexist=0;
	var i=0;
	for (i=0; i<ids.length; i++)
	{
		if(parseInt(ids[i])==parseInt(tmpid))
		{
			isexist=1;
		}
	}
	if(isexist==1)
	{
		var tmpdiv='<a href=\"javascript:PXT_sel_remove('+tmpid+');PXT_sel_swaplink2('+tmpid+');\" title=\"Retirer\">Retirer</a>';	
	}
	else
	{
		var tmpdiv='<a href=\"javascript:PXT_sel_add('+tmpid+');PXT_sel_swaplink2('+tmpid+');\" title=\"S&eacute;lectionnez pour comparer\">S&eacute;lectionnez pour comparer</a>';
	}
	document.getElementById("selectionner").innerHTML=tmpdiv;
}

	YAHOO.util.Event.addListener(window, "load", YAHOO.DDApp.init);

//second drag

// JavaScript Document
YAHOO.DDProdRev = function(id, sGroup, config) {
    this.initProd(id, sGroup, config);
};

// YAHOO.DDProdRev.prototype = new YAHOO.util.DDProxy();
YAHOO.extend(YAHOO.DDProdRev, YAHOO.util.DDProxy);
YAHOO.DDProdRev.TYPE = "DDProdRev";

YAHOO.DDProdRev.prototype.initProd = function(id, sGroup, config) {
    if (!id) { return; }

    this.init(id, sGroup, config);
    this.initFrame();

    this.logger = this.logger || YAHOO;
    var el = this.getDragEl()
    YAHOO.util.Dom.setStyle(el, "borderColor", "transparent");
    YAHOO.util.Dom.setStyle(el, "opacity", 0.76);

    // specify that this is not currently a drop target
    this.isTarget = false;

    this.originalStyles = [];

    this.type = YAHOO.DDProdRev.TYPE;
    this.slot = null;

    this.startPos = YAHOO.util.Dom.getXY( this.getEl() );
    this.logger.log(id + " startpos: " + this.startPos);
};

YAHOO.DDProdRev.prototype.startDrag = function(x, y) {
    this.logger.log(this.id + " startDrag");
    var Dom = YAHOO.util.Dom;
	var dragEl = this.getDragEl();
    var clickEl = this.getEl();

    dragEl.innerHTML = clickEl.innerHTML;
    dragEl.className = clickEl.className;
    Dom.setStyle(dragEl, "color",  Dom.getStyle(clickEl, "color"));
    Dom.setStyle(dragEl, "backgroundColor", Dom.getStyle(clickEl, "backgroundColor"));
	Dom.setStyle(dragEl, "width",  '54px');

    Dom.setStyle(clickEl, "opacity", 0.1);

    var targets = YAHOO.util.DDM.getRelated(this, true);
    this.logger.log(targets.length + " targets");
    ispxtdrag=1;
    if(pxtmode.indexOf('visuels')!=-1){
		hide_div_produit(1);
	}
};

YAHOO.DDProdRev.prototype.getTargetDomRef = function(oDD) {
    if (oDD.Prod) {
        return oDD.Prod.getEl();
    } else {
        return oDD.getEl();
    }
};
YAHOO.DDProdRev.prototype.b4EndDrag = function(e) {
    YAHOO.util.Dom.setStyle(this.getDragEl(), "visibility", "hidden");
}
YAHOO.DDProdRev.prototype.endDrag = function(e) {
    // reset the linked element styles
    YAHOO.util.Dom.setStyle(this.getEl(), "opacity", 1);
    this.resetTargets();
    ispxtdrag=0;
};

YAHOO.DDProdRev.prototype.resetTargets = function() {

    // reset the target styles
    //var targets = YAHOO.util.DDM.getRelated(this, true);
    //for (var i=0; i<targets.length; i++) {
    //    var targetEl = this.getTargetDomRef(targets[i]);
    //    var oldStyle = this.originalStyles[targetEl.id];
    //    if (oldStyle) {
    //        targetEl.className = oldStyle;
    //    }
    //}
};


YAHOO.DDProdRev.prototype.onDragDrop = function(e, id) {
    // get the drag and drop object that was targeted
    var oDD;
    //document.getElementById(id).className = "pxt_bg_produit_ma_selection";
    if ("string" == typeof id) {
        oDD = YAHOO.util.DDM.getDDById(id);
    }
	else {
        oDD = YAHOO.util.DDM.getBestMatch(id);
    }
    var el = this.getEl();
	var pxt_cslot=parseInt(el.id.substring(4,el.id.length));
	if(pxtmode.indexOf('visuels')!=-1){
		var old_block = document.getElementById('dd_'+slots_id[pxt_cslot]);
		if(old_block){
			old_block.style.display='inline';
		}
		slots_id[pxt_cslot]=0;
	}
	else if(pxtmode.indexOf('listing')!=-1){
		var old_block = document.getElementById('prod_'+slots_id[pxt_cslot]);
		if(old_block){
			old_block.style.display='';
		}
		slots_id[pxt_cslot]=0;
	}
	el.childNodes[0].src='/bk.gif';
	//ont update le slot
	PXT_sel_update();
	this.resetTargets();
	ispxtdrag=0;
};


YAHOO.DDProdRev.prototype.onDragOver = function(e, id) {
	//var clickEl = this.getEl();
	//document.getElementById(id).className = "pxt_bg_produit_ma_selection_on";
	//var dragEl = this.getDragEl();
	//alert('toto');
	//dragEl.innerHTML='<img src=\'http://images.grosbill.com/imagesproduitnew/images50jpg/'+PXT_dd_getGBpict50(clickEl)+'\'>';

};
YAHOO.DDProdRev.prototype.onDragOut = function(e, id) {
//document.getElementById(id).className = "pxt_bg_produit_ma_selection";
//var clickEl = this.getEl();
//var dragEl = this.getDragEl();
//dragEl.innerHTML = clickEl.innerHTML;
};

YAHOO.DDProdRev.prototype.onDrag = function(e, id) {
//var dragEl = this.getDragEl();
};



YAHOO.DDApp2 = function() {
    var slots2 = [];
    var Prods2 = [];
    var Event = YAHOO.util.Event;
    var DDM = YAHOO.util.DDM;
    return {
        init: function() {
            // slots
			var islot=0;
			for(islot=0; islot<pxt_nbslots; islot++){
				Prods2[islot]=new YAHOO.DDProdRev('ddt_'+islot+'', "ddt2");
         	}
			slots2[0] = new YAHOO.util.DDTarget("prodcell", "ddt2");
			DDM.mode = 0;
		}
    };
} ();


YAHOO.util.Event.addListener(window, "load", YAHOO.DDApp2.init);
/*
function mymousepos(e)
{
	x = (navigator.appName.substring(0,3) == "Net") ? e.pageX : event.x+document.body.scrollLeft;
	y = (navigator.appName.substring(0,3) == "Net") ? e.pageY : event.y+document.body.scrollTop;
	window.status = "Souris X:"+x+" / Y:"+y;
}

if(navigator.appName.substring(0,3) == "Net")
document.captureEvents(Event.MOUSEMOVE);
document.onmousemove = mymousepos;
*/
