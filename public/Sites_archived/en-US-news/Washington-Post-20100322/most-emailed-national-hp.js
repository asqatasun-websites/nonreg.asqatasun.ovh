function getLeaf( url )
{
  var leaves = new Array() ;
  leaves = url.split("/") ;
  var leaf = leaves[leaves.length-1] ;
  return leaf ;
}

function getId( url )
{
  var leaf = getLeaf( url ) ;
  return leaf.split(".")[0] ;
}

function isArticle( url )
{
  var leaf = getLeaf( url ) ;
  if ( leaf.match(/^AR\d+.*/) )
    return true ;
  else
    return false ;
}

function fixSource( source,isArticle )
{
  if (! isArticle )
  {
    var sourcesAsString = ",ap,aponline,cq,e-media,edition,liveonline,post,reuters,technews.com,washtechway,wpni,wpniblog," ;
    if ( sourcesAsString.indexOf( source.toLowerCase() ) != -1 )
	  source = "Link" ;
  }
  else
    source = "Article" ;

  return source ;
}

function getMostEmailedAsHtml(count)
{
  var output = '' ;
  //var img = '<img src="http://media.washingtonpost.com/wp-srv/images/icon_mail.gif" width="24" height="11" border="0" alt="Email Link">' ;
  //var number_image_path = "http://www.washingtonpost.com/wp-srv/mostemailed/" ;

  if ( typeof count == "undefined" ||
       isNaN(count) ||
	   count > most_emailed.length ||
	   count <= 0
	 )
    count = most_emailed.length ;

  var image_max = 10 ;
  //var image_path = "http://media.washingtonpost.com/wp-srv/mostemailed/" ;
	
 

  for ( var i=0; i<count; i++ )
  {
    var text = most_emailed[i].text ;
    var url = unescape(most_emailed[i].url) ;
	
	var nav = "nav=most_emailed" ;
	var path = window.location.pathname ;

	
	if ( url.indexOf("?") != -1 )
	  url += "&"+nav ;
	else
	  url += "?"+nav ;
	
    var source = most_emailed[i].source ;

	var isAnArticle = isArticle( url ) ;
	source = fixSource(source,isAnArticle) ;
	

	
	// LIST ITEMS HERE
	
	output+= '<li style="color:#666;font-weight:bold;padding:0;"><a href="'+url+'" class="gallery_text" style="text-decoration:none;">'+text+'</a></li>';

//	var link_url = '' ;
	if (! isAnArticle )
	{
      link_url = "http://www.washingtonpost.com/ac2/wp-dyn/emailalink" ;
	  link_url += '?link='+escape(url) ;
	  link_url += '&title='+escape(text) ;
	  link_url += '&type='+source ;
	  link_url += '&site=admin&sent=no&site=Search' ;
	}
	else
	  link_url = 'http://www.washingtonpost.com/ac2/wp-dyn/emailafriend?contentId='+getId( url )+'&sent=no' ;
    
	//output += '<A HREF="'+link_url+'">' ;
    //output += img ;
    //output += '</A>' ;
	//output += '&nbsp;' ;
	//output += '<A HREF="'+link_url+'">' ;
	//output += link_text ;
   // output += '</A>' ;

  }

 
  output += '<div style="clear:both;"></div>';
  return output ;  
}
//function getMostEmailedAsHtmlForEmailAFriend(count) {

	//if ( typeof count == "undefined" ||
		//isNaN(count) ||
		//count > most_emailed.length ||
		//count <= 0
	//) {
    	//count = 5 ;
	//}

	//var output = '' ;
	//output += '<div id="most-emailed">' ;
	//output += '<img src="http://media.washingtonpost.com/wp-srv/emailafriend/emailLogo_18x11.gif" border="0" width="18" height="11"/>&nbsp;<b>Most e-mailed</b><br/>' ;
