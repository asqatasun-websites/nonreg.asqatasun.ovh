var listArrivees;
var listDepart;
listArrivees = document.createElement("table");
listDepart   = document.createElement("table");	
var langue = 'fr';
	
	function DEP()
	{
		if (langue == 'fr')
		{
			document.getElementById("imgArr").src ="../../projet-ressources/images/arriveedujour_bleu.gif";
			document.getElementById("imgDep").src ="../../projet-ressources/images/departdujour_blanc.gif";
		}
		else
		{
			document.getElementById("imgArr").src ="../../projet-ressources/images/arriveedujour_bleu_gb.gif";
			document.getElementById("imgDep").src ="../../projet-ressources/images/departdujour_blanc_gb.gif";
		}
		
		if(std_isInternetExplorer)
		{
			if (langue == 'fr')
			{
				document.getElementById("cellEnteteDestProv").innerText = "Destination"; 
				document.getElementById("cellEnteteSallBanq").innerText = "Banque"; 
				if(document.getElementById("idEnr"))
				{
					document.getElementById("idEnr").innerText = "Enregistrement";
				}
			}
			else
			{
				document.getElementById("cellEnteteDestProv").innerText = "To"; 
				document.getElementById("cellEnteteSallBanq").innerText = "Bank"; 			
				if(document.getElementById("idEnr"))
				{
					document.getElementById("idEnr").innerText = "Check-in";
				}
			}
		}		
		else
		{
			if (langue == 'fr')
			{
				document.getElementById("cellEnteteDestProv").textContent = "Destination"; 
				document.getElementById("cellEnteteSallBanq").textContent = "Banque"; 
				if(document.getElementById("idEnr"))
				{
					document.getElementById("idEnr").textContent = "Enregistrement";
				}
			}
			else
			{
				document.getElementById("cellEnteteDestProv").textContent = "To"; 
				document.getElementById("cellEnteteSallBanq").textContent = "Check-in"; 			
				if(document.getElementById("idEnr"))
				{
					document.getElementById("idEnr").textContent = "Check-in";
				}
			}
		
		}
		
		document.getElementById("imgDep").selected ="yes";
		document.getElementById("imgArr").selected ="no";
		try
		{
				document.getElementById("lienTousLesVols").href="javascript:naviguerVers('./vols.aspx?DEP')"
		}
		catch(e){}
			
	}
	
	function ARR()
	{
		if (langue == 'fr')
		{
			document.getElementById("imgArr").src ="../../projet-ressources/images/bt_arrivee2.gif";
			document.getElementById("imgDep").src ="../../projet-ressources/images/departdujour.gif";
		}
		else
		{
			document.getElementById("imgArr").src ="../../projet-ressources/images/bt_arrivee2_gb.gif";
			document.getElementById("imgDep").src ="../../projet-ressources/images/departdujour_gb.gif";
		}
		
		if(std_isInternetExplorer)
		{
		
			if (langue == 'fr')
			{
				document.getElementById("cellEnteteDestProv").innerText = "Provenance"; 
				document.getElementById("cellEnteteSallBanq").innerText = "Salle"; 
			}
			else
			{
				document.getElementById("cellEnteteDestProv").innerText = "From"; 
				document.getElementById("cellEnteteSallBanq").innerText = "Room"; 			
			}
			if(document.getElementById("idEnr"))
			{
				document.getElementById("idEnr").innerText = "";
			}
		}
		else
		{
			if (langue == 'fr')
			{
				document.getElementById("cellEnteteDestProv").textContent = "Provenance"; 
				document.getElementById("cellEnteteSallBanq").textContent = "Salle"; 
			}
			else
			{
				document.getElementById("cellEnteteDestProv").textContent = "From"; 
				document.getElementById("cellEnteteSallBanq").textContent = "Room"; 			
			}
			if(document.getElementById("idEnr"))
			{
				document.getElementById("idEnr").textContent = "";
			}		
		}	
		document.getElementById("imgDep").selected ="no";
		document.getElementById("imgArr").selected ="yes";
		try
		{
				document.getElementById("lienTousLesVols").href="javascript:naviguerVers('./vols.aspx?ARR')"
		}
		catch(e){}
	}
	
	function afficherArr()
	{
		afficher("edLigneVolArr");
		ARR();
	}
	
	function afficherArrEn()
	{
		langue = "en";
		afficherArr();
	}
	
	function afficherDep()
	{
		afficher("edLigneVolDep");
		DEP();
	}
	
	function afficherDepEn()
	{
		langue = "en";
		afficherDep();
	}
	
	function afficherDefautArr()
	{
		afficher("edLigneVolArr");
		ARR();
		document.getElementById("lienTousLesVols").href="javascript:naviguerVers('./vols.aspx?ARR')"
	}
	
	function afficherDefautArrEn()
	{
		langue = "en";
		afficherDefautArr();
	}
	
	function afficherDefautDep()
	{	
		afficher("edLigneVolDep");
		DEP();
		document.getElementById("lienTousLesVols").href="javascript:naviguerVers('./vols.aspx?DEP')"
	}
	
	function afficherDefautDepEn()
	{
		langue = "en";
		afficherDefautDep();
	}

	function afficherDefaut()
	{
		afficherDefautDep();
	}
	
	function afficher(nomTypeLigne)
	{	
	
		insererDate();
		var nbActive;
		nbActive = 0;
		for(i = 0; i< document.getElementById("idTabAff").childNodes.length;i++ )
		{
			
			if(std_isInternetExplorer)
			{
				if(document.getElementById("idTabAff").childNodes[i].name == nomTypeLigne)
				{
					document.getElementById("idTabAff").childNodes[i].style.display="inline-block";
				}
				if(document.getElementById("idTabAff").childNodes[i].name != nomTypeLigne)
				{
					document.getElementById("idTabAff").childNodes[i].style.display="none";
				}
			}
			else
			{
				try
				{
					if(document.getElementById("idTabAff").childNodes[i].getAttribute("name") == nomTypeLigne)
					{	
						document.getElementById("idTabAff").childNodes[i].setAttribute("style","display:inline-block;");
					}
					if(document.getElementById("idTabAff").childNodes[i].getAttribute("name")!= nomTypeLigne)
					{
						document.getElementById("idTabAff").childNodes[i].setAttribute("style","display:none;");
					}
				}
				catch (ex)
				{
				}
			}
		}
	}

function rafraichir(page)
{	
	document.getElementById("panneauAff").innerHTML = "";
	document.getElementById("panneauAff").innerText = "Patientez, rafraichissement en cours ...";
	executerPF(page);
} 

function rafraichirAuto(page,temps)
{	
	refresh = function(){rafraichir(page);} 
	window.setInterval(refresh,temps)
} 

function HandlerPF(resultat)
{
	/**************************************************************
	* Le handler prend en param�tre le r�sultat de la requ�te    *
	* Cette m�thode sera lanc�e lorsque la requ�te sera termin�e *
	**************************************************************/
	
	document.getElementById("panneauAff").innerHTML = resultat;
	if (document.getElementById("imgDep").selected=="yes")
	{
		afficherDep();
	}
	else
	{
		afficherArr();
	}
	insererDate();
}

function insererDate()
{
	var texte;
	if (document.getElementById("idCelluleDatePanAff"))
	{
		if (langue=="fr")
		{
			texte = " Le " + aujourdhui();
		}
		else
		{
			texte = " " + aujourdhuiEn();
		}
		if(std_isInternetExplorer)
		{
			document.getElementById("idCelluleDatePanAff").innerText = texte;
		}
		else
		{
			document.getElementById("idCelluleDatePanAff").textContent = texte;
		}
	}
}

function executerPF(page)
{	
	/*****************************************************************************************************
	* Cette m�thode permet l'envoi de la requ�te vers un service sp�cifique                             *
	* ex : http://localhost/workspace/sigma-ajax/getVillesParPays;jsessionid=AZERTYU?idPays= + ....     *
	* l'url �tant construite depuis javascript                                                          *
	*****************************************************************************************************/
	
	var url = page;
	var handler = HandlerPF;

	//1�re m�thode
	var monService = new SigmaService();
	monService.url = url;
	monService.callback = handler;
	monService.lancer();
	
	//2�me m�thode
	/*executerService(url, null, handler);*/
}
