var novUtils = {
	
	addEvent : function(oElem, sEvType, fn) {
		return oElem.addEventListener?oElem.addEventListener(sEvType, fn, false):oElem.attachEvent?oElem.attachEvent('on' + sEvType, fn):oElem['on' + sEvType] = fn;
	},
	
	removeEvent : function(oElem, sEvType, fn) {
		return oElem.removeEventListener?oElem.removeEventListener(sEvType, fn, false):oElem.attachEvent?oElem.detachEvent('on' + sEvType, fn):oElem['on' + sEvType] = fn;
	},
	
	getLeft : function(l) {
		if (l.offsetParent) return (l.offsetLeft + novUtils.getLeft(l.offsetParent));
  		else return (l.offsetLeft);	
	},
	
	getTop : function(l) {
		if (l.offsetParent) return (l.offsetTop + novUtils.getTop(l.offsetParent));
	  	else return (l.offsetTop);
	},
	$ : function() {
		var elements = new Array();
		for (var i = 0; i < arguments.length; i++)
		{
			var element = arguments[i];
			if (typeof element == 'string')
			element = document.getElementById(element);

			if (arguments.length == 1) return element;
			elements.push(element);
		}
		return elements;
	},
	
	getElementsByClassName : function(needle, tag, idlimit) {
		if (!tag || !document.getElementsByTagName(tag))
		tag = '*';
		if (!idlimit || !document.getElementById(idlimit))
			var my_array = document.getElementsByTagName(tag);
		else
			var my_array = document.getElementById(idlimit).getElementsByTagName(tag);
		var retvalue = new Array();
		var ii, jj;
		for (ii = 0, jj = 0; ii < my_array.length; ii++)
		{
			var c = " " + my_array[ii].className + " ";
			if (c.indexOf(" " + needle + " ") != -1)
				retvalue[jj++] = my_array[ii];
		}
		return retvalue;	
	},
	
	$$CN : function(needle, tag, idlimit) { return novUtils.getElementsByClassName(needle, tag, idlimit); },
	
	getAncestorByTagName : function(obj,tag,depth)
	{
		if (!depth||depth<1) depth=1;
		obj=obj.parentNode;
		if ((obj.tagName!=tag)&&(obj.tagName!="BODY") )
			return novUtils.getAncestorByTagName(obj,tag,depth);
		else
		{
			if ((obj.tagName==tag)&&(depth!=1))
				return novUtils.getAncestorByTagName(obj,tag,(depth-1));
		  else if ((obj.tagName==tag)&&(depth==1))
				return obj;
			else
				return false;
		}
	},
	$$A : function(obj,tag,depth) { return novUtils.getAncestorByTagName(obj,tag,depth); },
	
	/*
	* change dynamiquement les propri�t�s d'une class d�finie dans la css styleSheetname (pb sous Op�ra)
	* celle ci doit avoir �t� incluse dans le doc html � l'aide de balise <link rel="stylesheet" href="styleSheetname" type="text/css">
	* changes = new Array('classname###propri�t�###valeu', ...);
	* ex: Array('.classbg1###background###yellow', '.classbg2###background###yellow');
	*/
	modifyCssClassDefinition : function(styleSheetname, changes) {
			var tabCss = document.styleSheets;
			var couleurCss;
			var nbCss=tabCss.length;
			for(var i=0;i<nbCss;i++) {
					if(tabCss[i].href.match(styleSheetname)) {
						couleurCss = tabCss[i];			
						break;
					}
			}
			var rules = couleurCss.rules||couleurCss.cssRules;
			var nbChanges=changes.length;
			for(var i=0;i<nbChanges;i++) {
					var res=changes[i].split('###');
					var nbRules=rules.length;
					for(var r=0;r<nbRules;r++) {
						if(rules[r].selectorText==res[0]) 
							rules[r].style[res[1]] = res[2];
					}		
			}
	},
	
	faitFondre : function(id_div,left,top,opacityDeb,opacityFin,intervalMilliSec,intervalDec,tempsAffichagePlein,persistant) {
		var wait;
		var valOpacite=parseInt(opacityDeb);
		var leDiv=novUtils.$(id_div);
		leDiv.style.top=parseInt(top)+"px";
		leDiv.style.left=parseInt(left)+"px";
		if (parseInt(intervalDec)<0)
		{
			leDiv.style.display="block";
			setTimeout(function () { wait=setInterval(function () { degrade() } , parseInt(intervalMilliSec)) }, parseInt(tempsAffichagePlein)*1000);
		}else
		{
			leDiv.style.filter="alpha(opacity="+parseInt(opacityDeb)+")";
			leDiv.style.opacity=parseInt(opacityDeb)/100;
			leDiv.style.display="block";
			wait=setInterval(function () { degrade() } , parseInt(intervalMilliSec));
		}
		function degrade()
		{
			leDiv.style.filter="alpha(opacity="+parseInt(valOpacite)+")";
			leDiv.style.opacity=(parseInt(valOpacite)/100);
			if (parseInt(intervalDec)<0)
			{
				if (parseInt(valOpacite)<=parseInt(opacityFin)) {
					clearInterval(wait);
					leDiv.style.filter="alpha(opacity="+parseInt(opacityFin)+")";
					leDiv.style.opacity=parseInt(opacityFin)/100;
					if (parseInt(persistant)==0)
						leDiv.style.display="none";
				}
				else
					valOpacite=parseInt(valOpacite)+(parseInt(intervalDec));
			}else
			{
				if (parseInt(valOpacite)>=parseInt(opacityFin)) {
					clearInterval(wait);
					leDiv.style.filter="alpha(opacity="+parseInt(opacityFin)+")";
					leDiv.style.opacity=parseInt(opacityFin)/100;
					if (parseInt(persistant)==0)
						setTimeout(function () { leDiv.style.display="none";},(parseInt(tempsAffichagePlein)*1000));
				}else
					valOpacite=parseInt(valOpacite)+(parseInt(intervalDec));
					
			}
		}
	},
	
	novAlert : function(mess,classDiv,temps,left,top,arrondi,idVouluForBgColor)	{
		var tempsPlein=temps;
		var alertDiv=document.createElement("div");
		var idUnique=Math.round(Math.random(0)*500)+1;
		alertDiv.style.display='none';
		alertDiv.setAttribute('id',"novaLert_"+idUnique);
		alertDiv.innerHTML=mess;
		alertDiv.className=classDiv;
		if ( (idVouluForBgColor) && (idVouluForBgColor.length>0))
			novUtils.$(idVouluForBgColor).appendChild(alertDiv);
		else
			document.body.appendChild(alertDiv);
		if (arrondi==1)
			Nifty("div."+classDiv,"transparent");
		// Si left==droite alors le top est la largeur et top vaut 0
		if (left=="droite")
		{
			left=((window.innerWidth||document.body.clientWidth)-top)+"px";
			alertDiv.style.width=(top-5)+"px";
			top="0px";
		}
		// Si left==id alors le top = id:+left:+top
		if (left=="id")
		{
			var champs=top.split(':');
			left=(parseInt(novUtils.getLeft(novUtils.$(champs[0])))+parseInt(champs[1]))+"px";
			top=(parseInt(novUtils.getTop(novUtils.$(champs[0])))+parseInt(champs[2]))+"px";
		}
		
		novUtils.faitFondre("novaLert_"+idUnique,left,top,0,100,5,2,tempsPlein,1);
		novUtils.faitFondre("novaLert_"+idUnique,left,top,100,0,5,-2,tempsPlein,0);
	},
	
	preload : function (listenomimage, chemin) {
	 if (listenomimage.length > 0) {
	  var taille=listenomimage.length;
	  for(i=0; i<taille; i++) {
	   document.image_chargee = new Image();
	   document.image_chargee.src = chemin + listenomimage[i];
	  }
	 }
	},
	
	/* Recuperer en Javascript les parametres en GET */
	$_GET : function(key) {
		var paramsArray = new Array();
		if (location.href.split('?').length > 1) 
			paramsArray = location.href.split('?')[1].split('&');
		var paramsArrayCount = paramsArray.length;
		var resultArray = new Array();
		if (paramsArrayCount != 0) for (var i = 0; i<paramsArrayCount; i++) {
			var keyAndValue = paramsArray[i].split('=');
			if ((keyAndValue[0]==key) && (keyAndValue[0]!="")) { return keyAndValue[1]; }
			var hash  = {
				key    : keyAndValue[0],
				value  : keyAndValue[1]
			}
			resultArray.push(hash);
		}
		return resultArray;
	},
	
	/* Fonction qui permet de stopper la propagation des evenements */
	cancelBubble : function(netEvent) {
	    if (document.all) window.event.cancelBubble = true;
	    else netEvent.cancelBubble = true;
	},
	
	getEvtTarget : function(e) {
		var targ;
		e= e||window.event;
		if (e.target) targ = e.target;
		else if (e.srcElement) targ = e.srcElement;
		if (targ.nodeType == 3) /* defeat Safari bug */ targ = targ.parentNode;
		return targ;
	},
	
	/* Cookie */
	setCookie : function(nom, valeur) {
	        var argv=arguments;
	        var argc=arguments.length;
	        var expires=(argc > 2) ? argv[2] : null;
	        var path=(argc > 3) ? argv[3] : null;
	        var domain=(argc > 4) ? argv[4] : null;
	        var secure=(argc > 5) ? argv[5] : false;
	        document.cookie=nom+"="+escape(valeur)+
	                ((expires==null) ? "" : ("; expires="+expires.toGMTString()))+
	                ((path==null) ? "" : ("; path="+path))+
	                ((domain==null) ? "" : ("; domain="+domain))+
	                ((secure==true) ? "; secure" : "");
	},
	
	getCookieVal : function(offset) {
	        var endstr=document.cookie.indexOf (";", offset);
	        if (endstr==-1) endstr=document.cookie.length;
	        return unescape(document.cookie.substring(offset, endstr));
	},

	getCookie : function(nom) {
	        var arg=nom+"=";
	        var alen=arg.length;
	        var clen=document.cookie.length;
	        var i=0;
	        while (i<clen)
	        {
	                var j=i+alen;
	                if (document.cookie.substring(i, j)==arg) return novUtils.getCookieVal(j);
	                i=document.cookie.indexOf(" ",i)+1;
	                if (i==0) break;
	        }
	        return null;
	},
	
	// Detects if the browser supports Ajax 
    browserSupportsAjax : function()
    {
        if (typeof XMLHttpRequest == "undefined" && typeof ActiveXObject == "undefined" && window.createRequest == "undefined")
            return false;
        return true
    },
    // Detects if the browser can use ActiveX if necessary
    ActiveXEnabledOrUnnecessary : function()
    {
        if (typeof ActiveXObject != "undefined")
        {
            var xhr = null;
            try{ xhr=new ActiveXObject("Msxml2.XMLHTTP");
            }catch (e){
                try{ xhr=new ActiveXObject("Microsoft.XMLHTTP");
                }catch (e2){
                    try{ xhr=new ActiveXObject("Msxml2.XMLHTTP.4.0");
                    }catch (e3){xhr=null;}
                }
            }
            if (xhr == null) return false
        }
        return true;
    }
};

var novFormUtils = {
	isRadioChecked :  function(radioObj) {
		for (var i = 0; i < radioObj.length; i++)
			if (radioObj[i].checked) return true ;
		return false ;
	},
	
	isCheckBoxChecked : function(cBoxObj) {
		return cBoxObj.checked;
	},
	
	isEmail : function(eml)	{
		a = eml.search(/^[-a-z0-9_]+([-._]+[-a-z0-9_]+)*@[a-z0-9]([.-]?[a-z0-9])*\.[a-z]{2,4}$/i);
		if(a!=-1)
			return true
		else
			return false
	},
	
	isInteger : function(value) {
		var exp = new RegExp("^[0-9]*$");
		 
		if(exp.test(value))
			return true
		else
			return false
	},

	isDouble : function(value) {
		var exp = new RegExp("^[0-9\.]*$");
		if(exp.test(value))
			return true
		else
			return false
	},
	
	isInputEmpty : function(inputObj) {
		return (inputObj.value.length == 0) ;
	},

	isDateValid : function(dateObj) {
		str = dateObj.value ;
		format = "jj-mm-aaaa" ;
		posj=format.search(/jj/i)
		posm=format.search(/mm/i)
		posa=format.search(/aaaa/i)
		poss=format.search(/\W/)
		poss2=format.substring(poss+1, format.length).search(/\W/)+poss+1
		s1=format.substring(poss, poss+1)
		s2=format.substring(poss2, poss2+1)
		s_jj=str.substring(posj, posj+2)
		s_mm=str.substring(posm, posm+2)
		s_aaaa=str.substring(posa, posa+4)
		jj=parseInt(s_jj*1)
		mm=parseInt(s_mm*1)
		aaaa=parseInt(s_aaaa*1)
		sep1=str.substring(poss, poss+1)
		sep2=str.substring(poss2, poss2+1)
		if(
			(
				isNaN(jj) || isNaN(mm) || isNaN(aaaa)
				|| jj<1 || jj>31 || mm<1 || mm>12 || aaaa<1900 || aaaa>3000
				|| s1!=sep1 || s2!=sep2
				|| s_jj.indexOf(" ")!="-1" || s_mm.indexOf(" ")!="-1" || s_aaaa.indexOf(" ")!="-1"
			)
		)
			return false ;
		return true ;
	}
};

var NovTableSort  = {
	iColSel : null,
	col : null,
	tableauHTML : null,
	/* Tri ascendant sur cha�nes de caract�res */
	TrierColAsc :  function(x1,x2) { return (x1[NovTableSort.iColSel] < x2[NovTableSort.iColSel])? -1 : 1; },   
	// Tri descendant sur cha�ne de caract�res
	TrierColDesc : 	function(x1,x2) { return (x1[NovTableSort.iColSel] > x2[NovTableSort.iColSel])? -1 : 1; },
	// Tri ascendant sur nombre
	TrierColNbAsc :  function(x1,x2) { return parseInt(x1[NovTableSort.iColSel])-parseInt(x2[NovTableSort.iColSel]); },  
	 // Tri descendant sur nombre
	TrierColNbDesc : function(x1,x2) { return parseInt(x2[NovTableSort.iColSel])-parseInt(x1[NovTableSort.iColSel]); },
	goSort :  function(type, idCol, order)
    {
	    this.iColSel = idCol;
		if (type=="int")	
		{
			if (order=="asc")
				this.col.sort(this.TrierColNbAsc);
			else
				this.col.sort(this.TrierColNbDesc);
		}
		if (type=="string")	
		{
			if (order=="asc")
				this.col.sort(this.TrierColAsc);
			else
				this.col.sort(this.TrierColDesc);
		}
	},
	init : function(tabHTML) {
		this.tableauHTML=tabHTML;
		var lesTRs=this.tableauHTML.getElementsByTagName('TR');
		var TlesTRs=lesTRs.length;
		this.col=new Array();
		/* On commence a 1 pour enlever le TH */
		for(var i=1;i<TlesTRs;i++)
		{
			/* On boucle sur les lignes */
			var lesTDs=lesTRs[i].getElementsByTagName('TD');
			var TlesTDs=lesTDs.length;
			this.col[i-1]=new Array();
			for(var j=0;j<TlesTDs;j++)
				this.col[i-1][j]=lesTDs[j].innerHTML;
		}
	},
	remake : function() {
		var lesTRs=this.tableauHTML.getElementsByTagName('TR');
		var TlesTRs=lesTRs.length;
		/* On commence a 1 pour enlever le TH */
		for(var i=1;i<TlesTRs;i++)
		{
			/* On boucle sur les lignes */
			var lesTDs=lesTRs[i].getElementsByTagName('TD');
			var TlesTDs=lesTDs.length;
			for(var j=0;j<TlesTDs;j++)
			lesTDs[j].innerHTML=this.col[i-1][j];
		}
	}
};

/* Sliders */
var Sliders = function (elt,prop) {
		this.element=elt;
		this.openTimer=false;
		this.CloseTimer=false;
		this.temps=500;
		this.interval=13;
		this.property=prop;
		this.opening=false;
		this.closing=false;
		this.etat=0;
		if (this.property=="height")
		 	this.taille=this.element.offsetHeight;
		else
		 	this.taille=this.element.offsetWidth;
		 // 500 ms d'affichage avec un timer de 10 ms
		this.incr=Math.ceil((this.taille/this.temps)*this.interval);
		this.element.style['overflow']='hidden';
	}
	Sliders.prototype = {
		slide : function(a) {
				
				this.element.style['visibility']='visible';	
				var z =this;
				var timer=setInterval(function(){ 
					z.update(a);
				},this.interval);
				if (a>0) this.openTimer=timer;
				else	this.closeTimer=timer;
		},
		slideOpen : function() {
			if (this.etat==0)
			{
				this.opening=true;
				this.element.style[this.property]='1px';	
				this.slide(1);
			}
		},
		slideClose :function() {
			if (this.etat==1)
			{
				this.closing=true;
				this.element.style[this.property]=this.taille+'px';
				this.slide(-1);
			}
		},
 		update : function(direction) {
 			if (direction>0)
 			{
				if (parseInt(this.element.style[this.property])>=this.taille)
				{
					clearInterval(this.openTimer);
					this.opening=false;
					this.etat=1;
				}
				else
				{
					if ((parseInt(this.element.style[this.property])+this.incr)>this.taille)
						this.element.style[this.property]=parseInt(this.taille)+"px";
					else 
						this.element.style[this.property]=(parseInt(this.element.style[this.property])+this.incr)+"px";
				}
 			}else
 			{
 				if (parseInt(this.element.style[this.property])<=1)
 				{
 					clearInterval(this.closeTimer);
 					this.closing=false;
 					this.element.style['visibility']="hidden";
 					this.etat=0;
 				}
				else
				{
					if ((parseInt(this.element.style[this.property])-this.incr)<0)
					{
						this.element.style[this.property]="1px";
						this.element.style['visibility']="hidden";
					}
					else 
						this.element.style[this.property]=(parseInt(this.element.style[this.property])-this.incr+1)+"px";
				}
 			}
 			
		}
	};


/****** DEPRECIATED ***/
/* Assure la compatibilite ascendante */
function getLeft(l) { return novUtils.getLeft(l); }
function getTop(l)  { return novUtils.getTop(l); }
function $() { var elements = new Array();for (var i = 0; i < arguments.length; i++){var element = arguments[i];if (typeof element == 'string')element = document.getElementById(element);if (arguments.length == 1) return element;elements.push(element);}return elements;}
function $$CN(needle, tag, idlimit) { return novUtils.getElementsByClassName(needle, tag, idlimit); }
function getElementsByClassName(needle, tag, idlimit) { return novUtils.getElementsByClassName(needle, tag, idlimit); }
function $$A(obj,tag,depth) { return novUtils.getAncestorByTagName(obj,tag,depth); }
function getAncestorByTagName(obj,tag,depth) { return novUtils.getAncestorByTagName(obj,tag,depth); }
function isRadioChecked(radioObj) { return novFormUtils.isRadioChecked(radioObj); }
function isCheckBoxChecked(cBoxObj) { return novFormUtils.isCheckBoxChecked(cBoxObj); }
function isEmail(eml) { return novFormUtils.isEmail(eml); }
function isInteger(value) { return novFormUtils.isInteger(value); }
function isDouble(value) { return  novFormUtils.isDouble(value); }
function isInputEmpty(inputObj) { return novFormUtils.isInputEmpty(inputObj);}
function isDateValid(dateObj) { return novFormUtils.isDateValid(dateObj); }
function modifyCssClassDefinition(styleSheetname, changes) { return novUtils.modifyCssClassDefinition(styleSheetname, changes);}
function faitFondre(id_div,left,top,opacityDeb,opacityFin,intervalMilliSec,intervalDec,tempsAffichagePlein,persistant) { return novUtils.faitFondre(id_div,left,top,opacityDeb,opacityFin,intervalMilliSec,intervalDec,tempsAffichagePlein,persistant); }
function novAlert(mess,classDiv,temps,left,top,arrondi,idVouluForBgColor) { return novUtils.novAlert(mess,classDiv,temps,left,top,arrondi,idVouluForBgColor);}
function preload(listenomimage, chemin) { return novUtils.preload(listenomimage, chemin);}
function $_GET(key) { return novUtils.$_GET(key);}
function cancelBubble(netEvent) { return novUtil.cancelBubble(netEvent); }
function setCookie(nom, valeur) { return novUtils.setCookie(nom, valeur); }
function getCookieVal(offset) { return novUtils.getCookieVal(offset); }
function getCookie(nom) { return novUtils.getCookie(nom); }
/* ************ */