//document.write('<style type="text/css">#dernieres_minutes .fragment {display:none}#dernieres_minutes #fragment-1 {display:block}</style>');

$(document).ready(function(){

		// boutons sociaux
		$("a.social").click( function () {
			$(this).parents("li").parents("ul").next(".on_partage").slideToggle();
			return false;
		});
	
		// Menu d�roulant Superfish
			$("#navigation > ul").superfish({
				hoverClass:"on",
				pathClass:'current',
				autoArrows:false
			});
			
			// style selon deroule
			if ($(".sf-breadcrumb").length!=0) {
			 $('#left_plus_middle').css('padding-top', '31px');
      }

			// Masque le menu proprement en cas de d�sactivation du js
			$("#navigation ul li").mouseover(function(){
				$(this).children('ul').addClass('survol');
			});
			$("#navigation ul li a").focus(function(){
				$(this).parent().children('ul').addClass('survol');
			});

		// Onglets
		$('ul.onglets').tabs({selected:0, fx:{opacity:"toggle"}});
		$('ul.onglets_2').tabs({selected:0, fx:{opacity:"toggle"}});

		//carousel
		$("#carousel").jCarouselLite({
        btnNext: ".next",
        btnPrev: ".prev"
    	});
		
		// fonction qui permet au clic des trois thumbs de remplacer l'image principale et de recuperer la legende
		$(".thumbs a").click(function(){
		  var largePath = $(this).attr("href");
		  var largeAlt = $(this).attr("title");
		  $("#largeImg").attr({ src: largePath, alt: largeAlt });
		  $("em.credit").html(" " + largeAlt + ""); return false;
		});

		// Lighbox
		$('a[@rel*=lightbox]').lightBox();

		// Menu d�roulant sur les boutons "galeries", "vid�os", ...
		$("ul.multimedia li").hover(function(){
		   $(this).addClass("sfHover");
		 },function(){
		   $(this).removeClass("sfHover");
		 });

		 // correction de la superposition
		  $("ul.multimedia:odd").addClass("pair");
		  $("ul.multimedia:even").addClass("impair");

			// rendre invisible les listes impaires
			$(".pair").hover(function(){
			   $(".impair").not('#article .impair').addClass("invisible");
			 },function(){
			   $(".impair").not('#article .impair').removeClass("invisible");
			 });
			 // rendre invisible les listes paires
			$(".impair").hover(function(){
			   $(".pair").not('#article .pair').addClass("invisible");
			 },function(){
			   $(".pair").not('#article .pair').removeClass("invisible");
		  });

    	// augmenter / diminuer la taille du texte
    	$("a.textsize_increase").click(function(){
    		var CurrentFontSize = $("#article").css("font-size");
    		FontSizeDefault = parseInt(CurrentFontSize);
    		var NewFontSize = FontSizeDefault+1;
    		$("#article").css('font-size', NewFontSize)
    	});
    	
    	// changer les href du bloc de breves
    	$("#dernieres_minutes .onglets li:eq(0) a").click(function(){
        $("a#toutes_breves").attr("href", "/Actualite/Depeches/");
    	});
    	$("#dernieres_minutes .onglets li:eq(1) a").click(function(){
        $("a#toutes_breves").attr("href", "/Sports/Breves/");
    	});
    	$("#dernieres_minutes .onglets li:eq(2) a").click(function(){
        $("a#toutes_breves").attr("href", "/France-Monde/Breves/");
    	});

		// les targets de la mort qui tue que c'est tellement sympa que m�me en 2002 les gens en avaient marre de cet horible attribut qui continue de fasciner les clients...
		$("a.ext").attr("target", "_blank");
	
					
});
