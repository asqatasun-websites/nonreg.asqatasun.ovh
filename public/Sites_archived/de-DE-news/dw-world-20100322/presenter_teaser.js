
$(document).ready(function(){	
	if($("div.Presenter").attr("name") == "true"){ // teaser is in TV mode		
		if($("div.Presenter .presenterDetailBlock").length > 0){ // detail text is available		
			 // setup event handlers
			$("div.Presenter .teaserImage a, div.Presenter .presenterDetailBlock a").click(function(event){

				var thisTeaser = $(this).closest(" .teaserWrapper"); // get parent container	    
					
				var detailBlock = thisTeaser.children().eq(1);
				var detailHeadlineLink = detailBlock.children().eq(0);

				var icon = detailHeadlineLink.children().eq(0);
				var detailHeadlineMoreText = detailHeadlineLink.attr("name");
			    var detailHeadlineLessText = detailHeadlineLink.children().eq(1).attr("name");	    	
			    var detailText = detailBlock.children().eq(1);

			    if(detailText.is(":visible")){

			    	detailHeadlineLink.children().eq(1).text(detailHeadlineMoreText);
			        icon.attr("src", "/pics/listplus.gif");

			        detailText.fadeOut('slow');
			    	//detailText.hide('slow');
			        //detailText.hide();	        
				    }else{			    
				    detailHeadlineLink.children().eq(1).text(detailHeadlineLessText);
				    icon.attr("src", "/pics/listminus.gif");

				    detailText.fadeIn('slow');
				    //detailText.show('slow');
				    //detailText.show();
			    }	   
				event.preventDefault();	
			});	
		}else{
			// disable clicks		
			$("div.Presenter .teaserImage a, div.Presenter .teaserImage a:hover").css("cursor", "default");	
			$("div.Presenter .teaserImage a, div.Presenter .teaserImage a:hover").css("text-decoration", "none");			
			$("div.Presenter .teaserImage a").click(function(event){
				event.preventDefault();
			});
		}		
	} // isTV == true
});