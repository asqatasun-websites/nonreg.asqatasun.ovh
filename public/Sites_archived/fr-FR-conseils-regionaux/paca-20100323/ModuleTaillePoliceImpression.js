// ***********************************************************************************************
// Fonction pour changer la taille de la police du site web
// ***********************************************************************************************
// @uteur Jean-Philippe RIVET

// ---------------------------------------------------------
// Initialisation des balises d'agrandissement et de r�duction de la taille
function iniAgrandirReduirePolice(){
	
	document.taillePolice = 100;
	
	/* Pointage sur les �l�ments */
	var pointeurAgrandirPolice = document.getElementById("augmenterTaillePolice");
	var pointeurReduirePolice = document.getElementById("diminuerTaillePolice");
	var pointeurImprimer = document.getElementById("imprimerLaPage");

	/* Attribution des fonctions */
	pointeurAgrandirPolice.setAttribute("href", "javascript: funcAgrandirPolice()");

	pointeurReduirePolice.setAttribute("href", "javascript: funcReduirePolice()");

	pointeurImprimer.setAttribute("href", "javascript: window.print()");
}

// ---------------------------------------------------------
// Agrandissement de la taille
function funcAgrandirPolice(){
	document.taillePolice = document.taillePolice + 10;
	if(document.taillePolice > 140){
		document.taillePolice = 140;
	}
	var pointeurBody = document.getElementsByTagName("body")[0];
	pointeurBody.style.fontSize = document.taillePolice + "%";
}

// ---------------------------------------------------------
// R�duction de la taille
function funcReduirePolice(){
	document.taillePolice = document.taillePolice - 10;
	if(document.taillePolice < 70){
		document.taillePolice = 70;
	}
	var pointeurBody = document.getElementsByTagName("body")[0];
	pointeurBody.style.fontSize = document.taillePolice + "%";
}
