jQuery.ajaxSetup ({
    cache: true
});


/**
 * Main Menu
 */

var MainMenu = {};

MainMenu.closeTimer = null;

MainMenu.cancelTimer = function() {
	
	if(MainMenu.closeTimer){

		window.clearTimeout(MainMenu.closeTimer);
		closeTimer = null;
	}
	
};

/**
 * mouse over handler
 */
MainMenu.mouseoverHandler = function(e) {
	
	MainMenu.cancelTimer(); 
	MainMenu.showSubMenu(this);
	
};

/**
 * mouse out handler
 */
MainMenu.mouseoutHandler = function(e) {
	
	e.stopPropagation();
	closeTimer = window.setTimeout(function() {MainMenu.hideSubMenu(e.currentTarget)}, 50);
	
};

/**
 * Show submenu
 */
MainMenu.showSubMenu = function(menuElement) {
	
	
	if(jQuery(menuElement).children('ul').is(':hidden'))
		jQuery(menuElement).children('ul').fadeIn('fast');
	
};

/**
 * Hide submenu
 */
MainMenu.hideSubMenu = function(menuElement) {
	
	if(jQuery(menuElement).children('ul').is(':visible'))
		jQuery(menuElement).children('ul').fadeOut('fast');
	
	
};

/**
 * Middle Column height adjustment
 */

function adjustCenterColumnHeight() {
	
	var maxSideColumnsHeight = (jQuery('#left-column').outerHeight() > jQuery('#right-column').outerHeight())? jQuery('#left-column').outerHeight() : jQuery('#right-column').outerHeight();
	
	
	if(jQuery('#center-column').outerHeight() < maxSideColumnsHeight) {
		
		jQuery('#center-column').height(maxSideColumnsHeight);
		
	}
	
	return maxSideColumnsHeight;
	
};

function adjustContentHeight() {
	
	value = adjustCenterColumnHeight();
	
	value = value - jQuery('#center-column > div.footer').outerHeight();
	value -= parseInt(jQuery('#content').css('margin-bottom').replace('px',''));
	value -= 10;
	
	//Main content div
	jQuery('#content').height(value);
	
	
	/** content under header **/
	
	if(jQuery('#content > div.content').size() > 0)
		value -= parseInt(jQuery('#content > div.content').css('padding-top').replace('px',''));
	
	if(jQuery('#content h1').size() > 0)
		value -= jQuery('#content h1').outerHeight();
	
	if(jQuery('#content div.bottom').size() > 0) {
	
		// show bottom that has been hidden in init function
		jQuery('#content > div.bottom').show();
		
		value -= jQuery('#content > div.bottom').outerHeight();
		
	}

	if(jQuery('#content > div.content').size() > 0)
		jQuery('#content > div.content').height(value);
	
	
};


/** INIT COLLAPSABLES **/
function initializeCollapsables() {
	
	var collapsables = jQuery('.collapsable');
	
	collapsables.each(
			
			function(index) {
				
				var container = jQuery(this);
				
				var title = container.children('h2').html();
				container.children('h2').remove();
				
				container.wrapInner('<div class="c-content"></div>');
				
				var content = container.children('.c-content');
				content.css('overflow', 'auto');
				
				var handle_html = '<div class="handle"></div>';
				var handle = content.before(handle_html).prev('.handle');
				handle.append(title);
				
				var toggle = function() {
						
					content.toggle();
					
				}
				
				if(container.hasClass('collapsed'))
					toggle();
				
				handle.click(
						
					function(e) {
						
						toggle();
						//jQuery.scrollTo(e.target, {duration: 1000});
						
					}
				);
				
				
			}
	
	);
	
	
}

/**
 * 
 * setup the scrolling of the right column of rdv artists list along with the main window
 * 
 * @return
 */
function initializeRDVScroll() {
	
	viewPortHeight = jQuery(window).height();
	 
	// element To Be offseted
	var el = jQuery("#rdv-list");
	var elOffsetTop = el.offset().top;
	var elPositionTop = el.position().top;
	var elHeight = el.outerHeight();
	

	//do not apply if screen is not high enough
	if(elHeight > viewPortHeight) return;
	
	windowScrollTimeout = null;
	detectEndOfWindowScroll = function() { 
		
		clearTimeout(windowScrollTimeout);
		windowScrollTimeout = setTimeout(moveColumn, 100);
		
	}
	
	moveColumn = function(e) {
		
		// window scroll ratio
		var scrollTop = jQuery(document).scrollTop();
		
		if(scrollTop > (elOffsetTop - 10)) {
			
			el.animate( {top : elPositionTop +  (scrollTop - elOffsetTop) + 20}, 300, 'easeOutQuart');
			
		}
		else {
			el.animate( {top : elPositionTop}, 200, 'easeOutQuart');
		}
		
	}
	
	jQuery(window).bind('scroll', detectEndOfWindowScroll);
	
}


/**
 * setup of the process that allows to scroll the content in the middle of the page hen the main window content div
 * the div had an overflow of 'auto' so we could scroll it independently but to allow scrolling to th ebottom we had to make the main window scroll 
 * propotionnaly
 * 
 * due to the lack of support of mousedow and mouseup events on the main window scrollbar this was too difficult or impossible to do (for me at least :)
 * 
 */
function initializeDoubleScroll() {
	
		
	
		documentHeight = jQuery(document).height();
		viewPortHeight = jQuery(window).height();
	
		contentOuterContainer = jQuery('#content > div.content');
		
		// wrap content html in div
		contentOuterContainer.wrapInner('<div id="contentInnerContainer" style="float: left; width: 100%"></div>');

		
		/**
		 * faking mouseup since it's nowhere to be found for scrollbars (except inff)
		 */
		contentOuterContainerScrollTimeout = null;
		detectEndOfContentContainerScroll = function() { 
			
			clearTimeout(contentOuterContainerScrollTimeout);
			contentOuterContainerScrollTimeout = setTimeout(simulateContentOuterContainerMouseup, 1000);
			
		}
		
		// fake mouseup callback
		simulateContentOuterContainerMouseup = function(e) {
			
			syncWindowToContentScroll();
			contentOuterContainer.unbind('scroll', detectEndOfContentContainerScroll);
				
		}
		
		/** SCROLL FUNCTIONS **/
		
		scrollContent = function(e) {
			
			contentHeight = contentOuterContainer.height();
			contentInnerContainerHeight = contentOuterContainer.children('#contentInnerContainer').height();
			
			// window scroll ratio
			var scrollTop = jQuery(document).scrollTop();
			var documentScrollratio = scrollTop / (documentHeight - viewPortHeight);
			
			var contentMaxScroll = contentInnerContainerHeight - contentHeight;
			
			/* DEBUG
			console.log('---------------------------------');
			console.log('documentHeight : ' + documentHeight);
			console.log('viewPortHeight : ' + viewPortHeight);
			console.log('contentInnerContainerHeight : ' + contentInnerContainerHeight);
			console.log('contentHeight : ' + contentHeight);
			console.log('contentMaxScroll : ' + contentMaxScroll);
			*/
			
			if(contentMaxScroll > 0) {
				
				contentOuterContainer.scrollTo(contentMaxScroll * documentScrollratio, {axis: 'y'});
				
			}
			
		}
		
		scrollWindow = function(e) {
			
			// window scroll ratio
			var scrollTop = jQuery(this).scrollTop();
			var contentScrollratio = scrollTop / (contentInnerContainerHeight - contentHeight);
			
			var windowMaxScroll = documentHeight - viewPortHeight;
			
			if(windowMaxScroll > 0)
				jQuery(window).scrollTo(windowMaxScroll * contentScrollratio, {axis: 'y'});
			
		}
		
		/** SYNC FUNCTIONS **/
		// enable/disable window/content sync when click on content div (scrollbar)
		syncContentToWindowScroll = function() {
			
			//jQuery(this).scroll(detectEndOfContentContainerScroll);			
			
			contentOuterContainer.bind('scroll', scrollWindow);
			jQuery(window).unbind('scroll', scrollContent);
			
		}
		
		syncWindowToContentScroll = function() {
			
			jQuery(window).bind('scroll', scrollContent);
			contentOuterContainer.unbind('scroll', scrollWindow);
			
		}
		
		
		// initial bind - scroll content when window is scrolled
		syncWindowToContentScroll();
		//syncContentToWindowScroll();
		
		/**
		 * attempt to initiate window scrolling when scrolling content
		 * because since there is no mouse up event for scrollbars (except in ff)
		 * we have to fake mouse up with the timeout trick
		 * but it does not cut it because when stopping scrolling the sync with window scroll is removed
		 * so we needed a way to re-bind it when resuming scroll...
		 */
		
		// do not apply after this
		return;
		
		contentOuterContainer.scroll(
			
				function(e) {
					
					var scrollEvents = jQuery.data(this, 'events').scroll;
					var scrollWindowIsBound = false;
					
					for(var i in scrollEvents) {
						
						if(scrollEvents[i] == scrollWindow)
							scrollWindowIsBound = true;
						
						
					}
					
					if(!scrollWindowIsBound) {
						
						syncContentToWindowScroll();
						contentOuterContainer.bind('scroll', detectEndOfContentContainerScroll);
						
					}
						
					
				}
		
		);
		
		
		
	
}

/** INIT **/

function init() {
	
	/** PRELOAD CSS IMAGES **/
	jQuery.preloadCssImages();
	
	// hide bottom to avoid visual artifact
	jQuery('#content > div.bottom').hide();
	
	setTimeout(adjustContentHeight, 200);
	setTimeout(initializeDoubleScroll, 400);
	
	initializeRDVScroll();
	
	initializeCollapsables();
	
	
	
	
};

function ajaxInit() {
	
	jQuery.ajaxSetup ({
		cache: true
	});

	// hide bottom to avoid visual artifact
	jQuery('#content > div.bottom').hide();
	
	setTimeout(adjustContentHeight, 200);
	setTimeout(initializeDoubleScroll, 400);
	
	tb_init('#content a.thickbox');

	//jQuery('#content').scrollTo(0, 400, {easing: 'easeOutQuad'});

	initializeCollapsables();
	
}

/** JQUERY READY **/

jQuery(document).ready(init);

/** ON LOAD **/

jQuery(window).load(function(){if(jQuery('#mask').size() > 0) jQuery('#mask').hide();});

