/*
'-------------------------------------------------------------------------------------------------
' Nom du fichier : formCheckV2.js
' Projet : Perso
' Sur une id�e Originale de : Jerome CHAGNOUX
' Adaptation & modification : Harmen CHRISTOPHE
' Date de modification : 03/02/2003
' Version : 0.0
'-------------------------------------------------------------------------------------------------
' Role : Permet une simplification du contr�le des formulaires....
'    Compatible avec les formulaires situ�s dans des layers
'-------------------------------------------------------------------------------------------------

OBJET : FormCheck
Constructeur FormCheck()
    Collection:
        fields            -->    Contient l'ensemble des objets Field pour une instance donn�e (Array)
    Propriete:
        formName        -->    Libelle de l'attribut "name" du fomuliare � controler (<form name="form"...>) (Chaine)
    Methode:
        addField        -->    Ajoute un hamp de contr�le � l'objet FormCheck
        validateForm        -->    R�alise le contr�le de l'ensemble des objets Field de la collection Fields
        validateTextField    -->    R�alise le contr�le d'un objet field pour les champs de formulaire de type texte (texte, textarea, file..)
        validateSelect        -->    R�alise le contr�le d'un objet field pour les champs de formulaire de type select (simple, multiple..)
        forceChecking        -->    Prend le contr�le de l'�vennement "Onsubmit" et de la methode "Submit" pour le fomulaire (quelque soit les actions la validation aura lieu)


OBJET : FrmChkField
Constructeur FrmChkField()
    Collection:
        conditions        -->    Conteint l'ensemble des conditions � appliquer a un champ de fomulaire (Array)
    Propriete:
        fieldName        -->    Libelle de l'attibut "name" du champ de formulaire (inpu type="texte" name="FiledName"...>) (Chaine)
        alertName        -->    Nom utilis� dans les alertes en cas d'erreur sur les donn�es (chaine)
    Methode:
        addCondition        -->    Ahjoute une condition � la collection "conditions"
*/

/*CONSTRUCTEUR: FORMCHECK
Role:
    INITIALISE L'ENSEMBLES DES METHODES ET PROPRIETES
Parametre:
    - formName: Nom du formulaire � contr�ler
Retour:
    -
Action:
    - initialise l'ensemble des methodes et proprietes
*/
function FormCheck(formName) {
    this.formName = formName;
    this.fields = new Array();
    this.addField = formCheck_addField;
    this.validateForm = formCheck_validateForm;
    this.validateTextField =  formCheck_validateTextField;
    this.validateSelect =  formCheck_validateSelect;
    this.forceChecking = formCheck_forceChecking;
}
/*METHODE: FORMCHECK
Role:
    AJOUTE UN CHAMP DE CONTROLE A L'OBJET FORMCHECK
Parametre:
    - fieldName: String - Nom du champ du formulaire � contr�ler
    - alertName: String - Nom utilis� dans l'alert pour d�finir le champ
Retour:
    -
Action:
    - r�cup�re les arguments pass�s � la m�thode
    - cr�e un nouvel objet Field qui est affect� � la collection fields
    - passe chacun des arguments (autre que les deux premiers) en parametre de la m�thode addCondition du nouvel object cr�e
*/
function formCheck_addField(fieldName,alertName) {
    var i, args;
    args= formCheck_addField.arguments;
    this.fields[this.fields.length] = new FrmChkField(fieldName,alertName);
    for(i=2;i<args.length;i++) {this.fields[this.fields.length-1].addCondition(args[i]);}
}
/*METHODE: FORMCHECK
Role:
    REALISE LE CONTROLE DE L'ENSEMBLE DES OBJECTS FIELD
Parametre:
    -
Retour:
    - result: Booleen indiquant s'il y a eu une erreur durant le contr�le des donn�es
Action:
    - Pour chaque objet Field de la collection Fields
    - si l'objet poss�de des conditions
    - tente de valider chacune d'entre elles en �x�cutant la m�thode validateTextField ou validateSelect selon le type de champ
*/
function formCheck_validateForm() {
    var result, fieldType, j;
    result=true;
    // Pour chaque champs � contr�ler
    for(j=0; j<this.fields.length; j++) {
        // Si des conditions existent
        if (this.fields[j].conditions.length!=0) {
            // On r�cup�re le type de champ
            fieldType=eval("findForm(this.formName).elements['" + this.fields[j].fieldName + "'].type");
            // Pour les types text
            if ((fieldType.substring(0,4)=="text") || (fieldType=="file") || (fieldType=="password")) {
                result=this.validateTextField(j);
            // Pour les types select
            } else if (fieldType.substring(0,6)=="select") {
                result=this.validateSelect(j);
            }
        }
        if (!result) {break;}
    }
    return result;
}
/*METHODE: FORMCHECK
Role:
    REALISE LE CONTROLE D'UN OBJET FIELD POUR LES CHAMPS DE FORMULAIRE DE TYPE TEXTE '
Parametre:
    - fieldId: Id de l'objet Field dans la collection Fields
Retour:
    - result: Booleen indiquant s'il y a eu une erreur durant le contr�le des donn�es
Action:
    - Pour chacune des conditions de l'objet Field tente de r�cup�rer le nom de la condition et l'�ventuel parametre supl�mentaire
    - selon la valeur de la condition �x�cute la m�thode de validation et initiazlise l'eventuel message d'erreur
    - s'il y a une erreur affiche le message d'erreur et selection le champ du formulaire fautif
*/
function formCheck_validateTextField(fieldId) {
    var result, myString, arg, i, j, k, valueToTest, errorMsg;
    result = true;
    // On r�cup�re la valeur a tester
    valueToTest = eval("findForm(this.formName).elements['" + this.fields[fieldId].fieldName +"'].value");
    // Pour chacune des conditions
    for (i=0; i<this.fields[fieldId].conditions.length; i++) {
        // On r�cup�re la condition
        myString=this.fields[fieldId].conditions[i];
        // on r�cup�re les index s�parant la condition de l'eventuelle parametre
        j = myString.indexOf(":",0);
        k = myString.lastIndexOf(":",myString.length);
        // On r�cup�re l'eventuel parametre de la condition
        arg = myString.substring(k+1,myString.length);
        // On r�cup�re le nom de la condition
        myString = myString.substring(0,j);
        // Si le nom de la condition est vide cela signifi que la condition est dans la variable parametre
        if (myString=="") {myString=arg;}
        // Selon la condition
        switch(myString) {
        	case "isLike":
        	   var valueToTest2 = eval("findForm(this.formName).elements['" + arg +"'].value");
                result=isLike(valueToTest,valueToTest2);
                errorMsg = "La v�rification de mot de passe a �chou�.";
                break;
            case "notNull":
                result=notNull(valueToTest);
                errorMsg = "Une valeur est requise pour le champ \"" + this.fields[fieldId].alertName + "\".";
                break;
            case "isInt":
                if (notNull(valueToTest)) {
                    result=isInt(valueToTest);
                    errorMsg = "Une valeur entiere est attendue pour le champ \"" + this.fields[fieldId].alertName + "\".";
                }
                break;
            case "isIn" :
                if (notNull(valueToTest)) {
                    result=isIn(valueToTest,arg);
                    var regPipe = /\|/g;
                    errorMsg = "Les valeurs autoris�es pour le champ \"" + this.fields[fieldId].alertName + "\" sont \"" + arg.replace(regPipe,"\" , \"") + "\".";
                    regPipe = null;
                }
                break;
            case "isFloat":
                if (notNull(valueToTest)) {
                    result=isFloat(valueToTest,arg);
                    errorMsg = "Une valeur numerique est attendue pour le champ \"" + this.fields[fieldId].alertName + "\".";
                }
                break;
            case "isEmail":
                if (notNull(valueToTest)) {
                    result=isEmail(valueToTest);
                    errorMsg = "La valeur du champ \"" + this.fields[fieldId].alertName + "\" n\'est pas une adresse mail valide.";
                }
                break;
            case "isWord":
                if (notNull(valueToTest)) {
                    result=isWord(valueToTest);
                    errorMsg = "Le message d'erreur";
                }
                break;
            case "isDate":
                if (notNull(valueToTest)) {
                    result=isDate(valueToTest);
                    errorMsg = "Le message d'erreur";
                }
                break;
            case "isImage":
                if (notNull(valueToTest)) {
                    result=isImage(valueToTest);
                    errorMsg = "Le message d'erreur";
                }
                break;
            case "isFlash":
                if (notNull(valueToTest)) {
                    result=isFlash(valueToTest);
                    errorMsg = "Le message d'erreur";
                }
                break;
            case "isVideo":
                if (notNull(valueToTest)) {
                    result=isVideo(valueToTest);
                    errorMsg = "Le message d'erreur";
                }
                break;
            case "isZipFile":
                if (notNull(valueToTest)) {
                    result=isZipFile(valueToTest);
                    errorMsg = "Le message d'erreur";
                }
                break;
            case "isPdfFile":
                if (notNull(valueToTest)) {
                    result=isPdfFile(valueToTest);
                    errorMsg = "Une fichier pdf (.pdf) est attendue pour le champ \"" + this.fields[fieldId].alertName + "\".";
                }
                break;
            case "isCssFile":
                if (notNull(valueToTest)) {
                    result=isCssFile(valueToTest);
                    errorMsg = "Une feuille de style (.css) est attendue pour le champ \"" + this.fields[fieldId].alertName + "\".";
                }
                break;
            case "minLength":
                if (notNull(valueToTest)) {
                    result=minLength(valueToTest,arg);
                    errorMsg = "Il doit y avoir au moins " + arg + " caracteres pour le champ \"" + this.fields[fieldId].alertName + "\".";
                }
                break;
            case "maxLength":
                if (notNull(valueToTest)) {
                    result=maxLength(valueToTest,arg);
                    errorMsg = "Il ne peut y avoir plus de " + arg + " caracteres pour le champ \"" + this.fields[fieldId].alertName + "\".";
                }
                break;
            case "isInternationalPhone":
                if (notNull(valueToTest)) {
                    result=isInternationalPhone(valueToTest);
                    errorMsg = "La valeur du champ \"" + this.fields[fieldId].alertName + "\" n\'est pas un num�ro de t�l�phone international.";
                }
                break;

            default:
                result=false;
                break;
        }
        if (!result) {
            Confirmation2(errorMsg);
            eval("findForm(this.formName).elements['" + this.fields[fieldId].fieldName + "'].focus()");
            eval("findForm(this.formName).elements['" + this.fields[fieldId].fieldName + "'].select()");
            break;
        }
    }
    return result;
}
/*METHODE: FORMCHECK
Role:
    REALISE LE CONTROLE D'UN OBJET FIELD POUR LES CHAMPS DE FORMULAIRE DE TYPE SELECT '
Parametre:
    - fieldId: Id de l'objet Field dans la collection Fields
Retour:
    - result: Booleen indiquant s'il y a eu une erreur durant le contr�le des donn�es
Action:
    - Pour chacune des conditions de l'objet Field tente de r�cup�rer le nom de la condition et l'�ventuel parametre supl�mentaire
    - selon la valeur de la condition �x�cute la m�thode de validation et initiazlise l'eventuel message d'erreur
    - s'il y a une erreur affiche le message d'erreur et selection le champ du formulaire fautif
*/

/* DEBUT ANCIENNE VERSION */
function formCheck_validateSelect(fieldId) {
    var result, l, j,errorMsg;
    result = true;
    l = this.fields[fieldId].conditions.length;
    // Pour chaque conditions
    for(j=0;j<l;j++) {
        switch(this.fields[fieldId].conditions[j]) {
            case "maxSelect":
            case "minSelect":
            case "isNotIn":
            case "isIn":
            case "selected":
                errorMsg = "Une valeur est requise pour le champ \"" + this.fields[fieldId].alertName + "\".";
                result=eval("findForm(this.formName).elements['" + this.fields[fieldId].fieldName+"'].options.selectedIndex!=-1");
                break;
			case "notNull":
            	errorMsg = "Une valeur est requise pour le champ \"" + this.fields[fieldId].alertName + "\".";
                result=notNull(eval("findForm(this.formName).elements['" + this.fields[fieldId].fieldName+"'].value"));
				break;
            default:
                result=false;
                break;
        }
        if (!result) {
            Confirmation2(errorMsg);
            eval("findForm(this.formName).elements['" + this.fields[fieldId].fieldName + "'].focus()");
            break;
        }
    }
    return result;
}
/* FIN ANCIENNE VERSION */

/* DEBUT NOUVELLE VERSION

function formCheck_validateSelect2(fieldId) {
	var result, l, j;
	result = true;
	l = this.fields[fieldId].conditions.length;
	for(j=0;j<l;j++) {
		switch(this.elt[ind].condition[j]) {
			case "notNull":
				result=eval("document."+this.formName+"." + this.fields[fieldId].fieldName+".options.selectedIndex!=-1");
				break;
			default:
				result=false;
				break;
		}
		if (!result) {break;}
	}
	return result;
}

FIN NOUVELLE VERSION*/

/*METHODE: FORMCHECK
Role:
    PREND LE CRONTROLE DE L'EVENEMNENT ONSUBMIT ET DE LA METHODE SUBMIT POUR LE FORMULAIRE
Parametre:
    - fieldId: Id de l'objet Field dans la collection Fields
Retour:
    - result: Booleen indiquant s'il y a eu une erreur durant le contr�le des donn�es
Action:
    - Pour chacune des conditions de l'objet Field tente de r�cup�rer le nom de la condition et l'�ventuel parametre supl�mentaire
    - selon la valeur de la condition �x�cute la m�thode de validation et initiazlise l'eventuel message d'erreur
    - s'il y a une erreur affiche le message d'erreur et selection le champ du formulaire fautif
*/
function formCheck_forceChecking() {
    // On r�cup�re l'objet Formulaire
    objForm = findForm(this.formName);
    // On rajoute au formulaire l'objet formCheck
    objForm.FormCheck = this;
    // On r�cup�re le corp de l�vennement onsubmit;
    var strOldEvent = new String(objForm.onsubmit);
    var intStart = strOldEvent.indexOf("{") + 1;
    var intLen = (strOldEvent.lastIndexOf("}")- intStart);
    var strOldEventBody = strOldEvent.substr(intStart, intLen);
    // On cr�e le d�but du corp du nouvel �vennement
    sNewEventStart = "var valide = this.FormCheck.validateForm();";
    sNewEventStart = sNewEventStart + "if (valide) {"
    // On cr�e la fin du corp du nouvel �vennement
    sNewEventEnd = "}return valide;";
    // On cr�e le corp complet du nouvel �venenement
    var strNewEventBody = sNewEventStart + strOldEventBody + sNewEventEnd;
    // On red�finie l'�v�nnement onsubmit
    objForm.onsubmit = new Function(strNewEventBody);
    // On rajoute au formulaire une m�thode "oldSubmit" qui correspond � la m�thode submit du m�me formulaire
    objForm.oldSubmit = objForm.submit;
    new_formSubmit = new Function("var valide = this.FormCheck.validateForm();if (valide) {this.oldSubmit();}")
    // On redefini la m�thode submit du formulaire
    objForm.submit = new_formSubmit;
}
/*CONSTRUCTEUR: FIELD
Role:
    INITIALISE L'ENSEMBLES DES METHODES ET PROPRIETES
Parametre:
    - fieldName: Nom du champ de formulaire
    - alertName: Nom utilis� dans les alertes en cas d'erreur sur les donn�es
Retour:
    -
Action:
    - initialise l'ensemble des methodes et proprietes
*/
function FrmChkField(fieldName,alertName) {
    this.fieldName = fieldName;
    this.alertName = alertName;
    this.conditions = new Array();
    this.addCondition= FrmChkField_addCondition;
}
/*METHODE: FIELD
Role:
    AJOUTE UNE CONDITION A UN OBJET FIELD
Parametre:
    - conditionName: Libelle de la condition
Retour:
    -
Action:
    - A joute � la collection conditions la condition pass�e en parametre
*/
function FrmChkField_addCondition(conditionName) {
    this.conditions[this.conditions.length]=conditionName;
}
//===================================================================
/*METHODE: GLOBALE
Role:
    TROUVE UN OBJET FORMULAIRE DONT LE NOM EST PASSE EN PARAMETRE
Parametre:
    - formName : Nom du formulaire (name="formulaireName")
    - d : Objet html, utilis�e dans un mode r�cursif, repr�sentant l'objet document des layers avec le client NC4
Retour:
    -
Action:
    - Tente de trouver l'objet formulaire dont le nom est pass� en parametre
    - Si l'objet n'est pas trouv� et que le client est NC4, tente de le trouver dans chacun des layers contenu dans l'objet "d" courant.
*/
function findForm(formName,d) {
    var p,i,x;
    if(!d) d=document;
    for (i=0;!x&&i<d.forms.length;i++) {
        if (d.forms[i].name == formName) {
            x=d.forms[i];
        }
    }
    for(i=0;!x&&d.layers&&i<d.layers.length;i++)  {
        x=this.findForm(formName,d.layers[i].document);
    }
    return x;
}
//--- isFlash: function, verify if the given string correspond to a flash file
function isFlash(myString) {
    var j, ext;
    j=myString.lastIndexOf(".", myString.length);
    ext=myString.substring(j+1,myString.length);
    ext=ext.toLowerCase();
    return (ext=="swf");
}

//--- isImage: function, verify if the given string correspond to an image
function isImage(myString) {
    var j, ext;
    j=myString.lastIndexOf(".", myString.length);
    ext=myString.substring(j+1,myString.length);
    ext=ext.toLowerCase();
    return ((ext=="gif")||(ext=="jpg"));
}

//--- isImage: function, verify if the given string correspond to a video
function isVideo(myString) {
    var j, ext;
    j=myString.lastIndexOf(".", myString.length);
    ext=myString.substring(j+1,myString.length);
    ext=ext.toLowerCase();
    return ((ext=="avi")||(ext=="mov")||(ext=="mpg"));
}

//--- isZipFile: function, verify if the given string correspond to an ZIP file
function isZipFile(myString) {
    var j, ext;
    j=myString.lastIndexOf(".", myString.length);
    ext=myString.substring(j+1,myString.length);
    ext=ext.toLowerCase();
    return (ext=="zip");
}

//--- isPdfFile: function, verify if the given string correspond to a PDF file
function isPdfFile(myString) {
    var j, ext;
    j=myString.lastIndexOf(".", myString.length);
    ext=myString.substring(j+1,myString.length);
    ext=ext.toLowerCase();
    return (ext=="pdf");
}

//--- isCssFile: function, verify if the given string correspond to a CSS file
function isCssFile(myString) {
    var j, ext;
    j=myString.lastIndexOf(".", myString.length);
    ext=myString.substring(j+1,myString.length);
    ext=ext.toLowerCase();
    return (ext=="css");
}

//--- isFloat: function, verify if the given string may be a float ---
function isFloat(myString, arg) {
    var reSpec, reFloat, l, j, k, reFloatF;
    reSpec = new RegExp('^[0-9]\\.[0-9]$');
    if (!reSpec.test(arg)) {
        reFloat = new RegExp('^[0-9]+(\\.[0-9]+)?$');
        return reFloat.test(myString);
    } else {
        l=arg.split('.');
        j=l[0];
        k=l[1];
        reFloatF = new RegExp('^[0-9]{1,'+j+'}(\\.[0-9]{1,'+k+'})?$');
        return reFloatF.test(myString);
    }
}
//--- isInt: function, verify if the given string may be an integer ---
function isInt(myString) {
    var reInt;
    reInt = new RegExp('^[0-9]+$');
    return reInt.test(myString);
}
//--- isEmail: function, verify if the given string may be an email address ---
function isEmail(myString) {
    var strChar, regMail, regMailNo;
    regMail = new RegExp("^[a-z0-9_\.-]+@[a-z0-9_\.-]+\.[a-z0-9_\.-]+$","i");
    regMailNo = /([_\-\.]@)|(@[_\-\.])|([_\-\.][_\-\.])+|(^[_\-\.])|([_\-\.]$)/;
    if ((!regMail.test(myString)) || (regMailNo.test(myString))) {
        return false;
    } else {
        return true;
    }
}
//--- isWord: function, verify if the given string is composed of only word characters ---
function isWord(myString) {
    var reWord;
    reWord = new RegExp('^[a-zA-Z0-9-_\.]+$');
    return reWord.test(myString);
}
//--- isDate: function, verify if the given string may be a date ---
function isDate(myString) {
    reDate = new RegExp('^((0?[1-9])|(1[0-9])|(2[0-9])|(3[0-1]))\/((0?[1-9])|(1[0-2]))\/[0-9]{4}$');
    return reDate.test(myString);
}
//--- minLength: function, verify if the given string length is bigger than the given number ---
function minLength(myString,nb) {
    return (myString.length>=nb);
}
//--- maxLength: function, verify if the given string length is minor to the given number ---
function maxLength(myString,nb) {
    return (myString.length<=nb);
}
//--- notNull: function, verify if the given string is not null ---
function notNull(myString) {
    return !((myString==null)||(trim(myString)==""));
}


function isIn(myString,list) {
    var arrIn, i;
    arrIn = list.split("|");

    for (i=0; i < arrIn.length; i++) {
        if (myString==arrIn[i]) {return true;}
    }
    return false;
}

function isLike(myString,MaVal) {
	return (MaVal == myString) ? true : false;
}

//--- isInternationalPhone: function, verify if the given string may be a valid international phone number ---
function isInternationalPhone(myString) {
    var reInternationalPhone;
    reInternationalPhone = new RegExp('^[+][0-9]+$');
    return reInternationalPhone.test(myString);
}
//===================================================================







/*======================================================================================*
 * SUPRIME L'ENSEMBLE DES CARACTERES BLANC (\f\n\r\t\v) EN DEBUT ET FIN DE CHAINE    *
 *======================================================================================*/
    function trim(str) {
        str = trimLeft(str);
        str = trimRight(str);
        return str;
    }
    function trimLeft(str) {
        var regTrimleft = /^\s+/g;
        str = str.replace(regTrimleft,"");
        return str;
    }
    function trimRight(str) {
        var regTrimright = /\s+$/g;
        str = str.replace(regTrimright,"");
        return str;
    }
/*======================================================================================*
 * VALIDE DES DONNEES DE TYPE DATE                            *
 *======================================================================================*/

function checkDate(jour, mois, annee) {
    //var srtDate = jour + "/" + mois + "/" + annee;
    var valide=true;
    var msg ="";
    var Calendar = new Array();
    Calendar[0] = 31;
    Calendar[1] = 31;
    Calendar[3] = 31;
    Calendar[4] = 30;
    Calendar[5] = 31;
    Calendar[6] = 30;
    Calendar[7] = 31;
    Calendar[8] = 31;
    Calendar[9] = 30;
    Calendar[10] = 31;
    Calendar[11] = 30;
    Calendar[12] = 31;
    // Validation du type entier
    var regInt = new RegExp("^[0123456789]+$");
    if (!regInt.test(jour)||!regInt.test(mois)||!regInt.test(annee)) {
        msg = "Vous devez saisir des entiers";
        valide = false;
    }
    // Validation de l'annee
    if ((valide==true)&&((annee < 1900) || (annee == "") || (annee > 2100))) {
        msg = "Vous devez saisir une valeur \"annee\" entre 1900 et 2100";
        valide = false;

    }
    // Validation du mois
    if ((valide == true) && ((mois-0 < 1 || 12 < mois-0 || (mois == "")))) {
        msg = "Vous devez saisir une valeur \"mois\" entre 1 et 12";
        valide = false;
    }
    // Prise ne compte des ann�es bi
    if ((valide == true) && ((!(annee % 4) && (annee % 100)) || !(annee % 400))) {
        Calendar[2] = 29;
    } else {
        Calendar[2] = 28;
    }
    // Validation du jour
    if ((valide==true)&&((jour-0 < 1 || (jour == "") || Calendar[Number(mois)] < jour-0))) {
        msg = "Vous devez saisir une valeur \"jour\" entre 1 et " + Calendar[Number(mois)];
        valide = false;
    }
    return msg;
}