$(document).ready(function () {

	var agt=navigator.userAgent.toLowerCase();
	var is_safari = ((agt.indexOf('safari')!=-1) &&(agt.indexOf('mac')!=-1))?true:false;
	if (is_safari != true){
		accessMenu._Init();//initialisation du menu Access
   		loadPopinNav();
	}
	
	// rajout validation moteur de recherche
	if($("#searchForm").is("form")){
		
		$("#searchForm").keyup(function(e) {
			if(e.keyCode == 13) {

			}else{
				$("input#inputSearchFormHidden").val($("#inputSearchForm").val().toLowerCase());
			}
		});

		$("#searchForm .validOK").click(function(){

			var searchFormInput = $("#inputSearchForm");
			var search = searchFormInput.val().toLowerCase();
			var query = encodeURI(search);
			var returnStatus = true;
			
			/*if($("isAlreadyInSearch").val()=="false"){
				$("isAlreadyInSearch").val("true");
			}*/

			searchFormInput.trigger("click");
			// le mot clef 'Recherche' (ou sa traduction locale) sera effac� par le trigger
			if (searchFormInput.val() == '') {
				returnStatus = false;
				
				// focus ou trigger("focus") de jQuery ne positionne pas le curseur dans le input...
				var searchFormInput2 = document.getElementById("inputSearchForm");
				searchFormInput2.focus();
			}
			
			return(returnStatus);
		});
		$("#inputSearchForm").click( function() {
			if($("#inputSearchForm").val()=='Recherche') {
				$("#inputSearchForm").val('');	
			}	
		});
		
		
	}
	
	
	if($("#newsForm").is("form")){
		
		var submitNewsForm = (function(){
		
			var emailValue = $("#inputNewsForm").attr("value");

			var exp = new RegExp("^[\\w\\-]+(\\.[\\w\\-]+)*@[\\w\\-]+(\\.[\\w\\-]+)*\\.[\\w\\-]{2,6}$", "i");
			var match = exp.test(emailValue);
			if(!match) { 
				alert($("#alertMsg").html());
				return false;
			}
			masqueShow();
			initPopin();//dans scripts/popin.js
			showPopin();//dans scripts/popin.js
			
			var maCss="/webapp/wcs/stores/servlet/Conforama/css/general/neutre/color.css";
			$("#cssColorTemp").attr("href", maCss);

			var myForm = $("#newsForm");
	
			var result = "";
			$("#newsForm input[@type=text]").each(function() {
				result += $(this).attr("name")+"="+$(this).val()+"&";
			})
			$("#newsForm input[@type=hidden]").each(function() {
				result += $(this).attr("name")+"="+$(this).val()+"&";
			})
			
			$.ajax({
				type:"POST",
				url: myForm.attr("action"),
				data: result,
				success:function(msg) {
					//affichage le contenu de la page r�sultat
					var content = msg.responseText;
					
					document.getElementById("layerPopin").innerHTML = content;
					
					initClosePopin();
					initCheckForm();
				}
			});
		});
	
		$("#newsForm").submit(function(){
			submitNewsForm();
			return(false);
		});
		$("#newsForm .validOK").click(function(){
			submitNewsForm();
			return(false);
		});
	}
	
	if($("#formNewsLetterFooter").is("form")){
		$("#formNewsLetterFooter .validOK").click(function(){

			var emailValue = $("#inputNewsLetterFooter").attr("value");

			var exp = new RegExp("^[\\w\\-]+(\\.[\\w\\-]+)*@[\\w\\-]+(\\.[\\w\\-]+)*\\.[\\w\\-]{2,6}$", "i");
			var match = exp.test(emailValue);
			if(!match) { 
				alert("Veuillez saisir une adresse email valide");
				return false;
			}
			masqueShow();
			initPopin();//dans scripts/popin.js
			showPopin();//dans scripts/popin.js
			
			var maCss="/webapp/wcs/stores/servlet/Conforama/css/general/neutre/color.css";
			$("#cssColorTemp").attr("href", maCss);
			
			window.location="#header";
			
			var myForm = $("#formNewsLetterFooter");
	
			var result = "";
			$("#formNewsLetterFooter input[@type=text]").each(function() {
				result += $(this).attr("name")+"="+$(this).val()+"&";
			})
			$("#formNewsLetterFooter input[@type=hidden]").each(function() {
				result += $(this).attr("name")+"="+$(this).val()+"&";
			})

			
			$.ajax({
				type:"POST",
				url: myForm.attr("action"),
				data: result,
				success:function(msg) {
					//affichage le contenu de la page r�sultat
					var content = msg.responseText;
					
					document.getElementById("layerPopin").innerHTML = content;
					
					initClosePopin();
					initCheckForm();
				}
			});
			
			return(false);			
		});
	}
	
	$("#inputNewsLetterHead").click(function(){
		if(this.value=='Votre e-mail' || this.value=='Votre e-mail '){
			this.value='';
		}
	});
	/** Click sur le panier	*/
	$(".noClick").click(function(){
		goShoppingCart();
		return false;
	});
	$(".noClick").dblclick(function(){
		goShoppingCart();
		return false;
	});
	/** fin : Click sur le panier	*/
});

// element roll a droite du header. &eacute;l&eacute;ment transverse a toutes les pages du site.
	
var accessMenu={
	_Init: function(){
		$(" #headerCenterTopRight div").addClass("withBg");//initialisation de l'affichage du picto de gauche
		$("#headerCenterTopRight li a").each(function(i){//on récupère les éléments liens
			if(i==0){//si c'est le premier lien de la liste
				$(this).addClass("actif");// on le passent en actif
			}
			$(this).mouseover(function(){//action au rollover de chaque liens
				$("#headerCenterTopRight li a.actif").removeClass("actif");// on retire la classe actif de tout les liens
				$("#headerCenterTopRight div").css({backgroundPosition: "0px -"+(i*68)+"px"});// on redéfinit la position du background du div
				$(this).addClass("actif");// on le passent en actif
			})
		});
	}
};

//Fonction pour ouvrir les popin &agrave; partir des boutons de la nav
var loadPopinNav = (function (){
	$("a.loadPopinNav").click(function(){
		masqueShowNav();
		initPopinNav();//dans scripts/popin.js
		showPopin();//dans scripts/popin.js
		
		var maCss="/webapp/wcs/stores/servlet/Conforama/css/general/"+getUrlVar($(this).attr("href"),"univers")+"/color.css";
		var maLangCss="/webapp/wcs/stores/servlet/Conforama/css/fr_FR/"+getUrlVar($(this).attr("href"),"univers")+"/color.css";
		$("#cssColorTemp").attr("href", maCss);
		$("#cssColorLangTemp").attr("href", maLangCss);
		window.location="#header";
		myHref = this.href.replace("_embeded", "");									
		if(myHref.indexOf("/")!=-1){
		    var indexSlash = myHref.indexOf("/");
			var basicUrl = myHref.substr(myHref, myHref.lastIndexOf("/") + 1);
			var urlVar=myHref.substr(myHref.lastIndexOf("/")+1);
			var tabUrlVar=urlVar.split("_");
			var originalUrl = "NavigationMenuView?";
			// TODO: rustine pour depanner => a modifier + tard
			if (myHref.indexOf("webapp") == -1) {
				originalUrl = "webapp/wcs/stores/servlet/"+originalUrl;
			}
			if(tabUrlVar.length == 7){	
				originalUrl = originalUrl + "univers=" +  tabUrlVar[6];			
				originalUrl = originalUrl + "&storeId=" +  tabUrlVar[2];
				originalUrl = originalUrl + "&catalogId=" +  tabUrlVar[3];
				originalUrl = originalUrl + "&langId=" +  tabUrlVar[4];
				originalUrl = originalUrl + "&universeId=" +  tabUrlVar[5];				
			}			
			myHref = basicUrl + originalUrl;
		}
		$("#layerPopin").load(myHref, function(){loadNavInit();});
		$(this).unbind();
		return(false);
	});
});

var loadNavInit = (function (){
	initClosePopinNav();
	loadPopinNav();
});

function masqueShowNav(){
		var pageH = $("#contentAllWebsite").height();
		$("#masque").height(pageH+"px");
		$("#masque").show();	
}

var initPopinNav = (function () {
	// cacher les div popin
	$("div.popin").hide();
	$("div.popin").removeClass("navTopPopin");
	$("div.popin").addClass("navTopPopin");
	$("#masque").click(function(){
		hidePopinNav();
	});
	initClosePopinNav();	
});

var hidePopinNav = (function () {
	$("#cssColorTemp").attr("href", "/webapp/wcs/stores/servlet/Conforama/css/general/common/color_temp.css");
	$("#cssColorLangTemp").attr("href", "/webapp/wcs/stores/servlet/Conforama/css/general/common/color_temp.css");
	$("div.popin").empty();
	$("#masque").hide();
	$("div.popin").removeClass("navTopPopin");
	$("div.popin").hide();
	if($.browser.msie){
		$("select").css("visibility","visible");
	}
	if((navigator.userAgent.toLowerCase().indexOf("mac")!=-1)?true:false){
		$("embed").css("visibility","visible");
	}
	//afficher les div contenant du flash
	if($(".embed").is("div")){$(".embed").css("visibility","visible");}
	return false;
});

var initClosePopinNav = (function () {
	initPopinH();
	$("a.closePopin").click(function(){
		hidePopinNav();
		return false;
	});
});

function checkEmail(e, emailValue){
	var key;     
    if(window.event){
         key = window.event.keyCode; //IE
    }
    else{          
         key = e.which; //firefox   
	}
    if (key == 13){       
		var exp = new RegExp("^[\\w\\-]+(\\.[\\w\\-]+)*@[\\w\\-]+(\\.[\\w\\-]+)*\\.[\\w\\-]{2,6}$", "i");
		var match = exp.test(emailValue);
		if(!match) { 
			alert("Veuillez saisir une adresse email valide");
			return false;
		}
		masqueShow();
		initPopin();//dans scripts/popin.js
		showPopin();//dans scripts/popin.js
		
		var maCss="/webapp/wcs/stores/servlet/Conforama/css/general/neutre/color.css";
		$("#cssColorTemp").attr("href", maCss);
		
		window.location="#header";
		
		var myForm = $("#formNewsLetterFooter");

		var result = "";
		$("#formNewsLetterFooter input[@type=text]").each(function() {
			result += $(this).attr("name")+"="+$(this).val()+"&";
		})
		$("#formNewsLetterFooter input[@type=hidden]").each(function() {
			result += $(this).attr("name")+"="+$(this).val()+"&";
		})

		
		$.ajax({
			type:"POST",
			url: myForm.attr("action"),
			data: result,
			success:function(msg) {
				//affichage le contenu de la page r�sultat
				var content = msg.responseText;
				
				document.getElementById("layerPopin").innerHTML = content;
				
				initClosePopin();
				initCheckForm();
			}
		});
		
		return(false);	
    }
}

function checkValue(value, obj) {
	if (value == obj.value) {
		obj.value = '';
	}
}

	/** Click sur le panier	*/
var clicked = false;
var pageLink = "#";
function goShoppingCart(){
	 if(!clicked){
	 	clicked = true;
	  	goLink();	  
	 } else{
	 	return false;
	 }
 }	
 
 function goLink(){
 		//attendre 3s
 		setTimeout("activeLink();",3000);
		pageLink = $(".noClick").attr("href");
		$(".noClick").removeAttr("href");
		document.location.href = pageLink;
 }
 
 function activeLink(){
 	clicked = false;
 	$(".noClick").attr("href",pageLink);
 }	
	/** fin : Click sur le panier	*/