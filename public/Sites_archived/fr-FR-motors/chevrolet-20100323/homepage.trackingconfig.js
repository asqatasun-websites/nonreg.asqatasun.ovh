/*** CONFIG: *****/

var ohomepageTrackingConfig = function (t) {
	return {

		elementClicked: function (data) {
			t.omniture.addEvent(data, 'prop22');
			t.omniture.track();
		}
	};
};	

jQuery(document).ready(function () {
	var pageTracking = new WebAppTracker(ohomepageTrackingConfig);
	jQuery(".sdtPath0_home .buttonLeft").live("click", function () {
		var handlerData = "Home-Teasers-Arrows-Left";
		pageTracking.setTrackingEvent('elementClicked', handlerData);
		return false;
		});
	jQuery(".sdtPath0_home .buttonRight").live("click", function () {
		var handlerData = "Home-Teasers-Arrows-Right";
		pageTracking.setTrackingEvent('elementClicked', handlerData);
		return false;
		});
	jQuery(".sdtPath0_home #displayTeaserSlider > li > div").live("click", function () {
			var teaserType = "",
			teaserPosition = "",
			absoluteTeaserPosition = "",
			pageNumber = "";
				if(this.className == "teaser1"){
				  teaserType = "Landscape";
				  }
				else{
				  teaserType="Panel";
				  }
				pageNumber = jQuery(this).parents("div").parents("div").parents("div").children("div").next().children(".pageActive").prevAll().length + 1;
				teaserPosition = jQuery(this).parents("li").prevAll().length - 2;
				absoluteTeaserPosition = teaserPosition + ((pageNumber - 1) * 3);
				teaserLink = jQuery(this).find("a").attr("href").replace(/http:\/\//,'');
				var handlerData = "Home-Teasers-Pos" + absoluteTeaserPosition + "-" + teaserType + "-" + teaserLink;
				pageTracking.setTrackingEvent('elementClicked', handlerData);
				return true;
		});
});