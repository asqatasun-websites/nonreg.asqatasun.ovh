/*
//================================================================
//  newcssmenu.js
//================================================================
//        $Id: newcssmenu.js,v 1.33 2008/10/31 15:47:15 libert-li Exp $
//================================================================
//   $RCSfile: newcssmenu.js,v $
//  $Revision: 1.33 $
//      $Date: 2008/10/31 15:47:15 $
//    $Author: libert-li $
//================================================================
*/
/*
Marquage CVS
$Header: /data/cvs/repository_v1.0/applis/src/fo/ebxapp/ebx/framework/skins/ebxDefault/js/newcssmenu.js,v 1.33 2008/10/31 15:47:15 libert-li Exp $
*/

    var rootMenuId = "mymenu";
    var maxWidth = 760;
    var popup;
        
    var scriptLoaded = false;
    var initialized = false;
    var currentMenu = null;
    var mytimer = null;
    var timerOn = false;
    var delay = 150;
    
    var ie = (navigator.userAgent.indexOf("MSIE") != -1);
    var n6 = (navigator.userAgent.indexOf("Netscape6") != -1);
    
    if (!document.getElementById)
        document.getElementById = function() { return null; }

    window.onload = function() {
       loadMenu();
       var root = document.getElementById(rootMenuId);
       positionMenus(root, root);
    }     
    
    window.onunload = function() {
        ViderCookie();
    }

    window.onresize = function() {
      var root = document.getElementById(rootMenuId);
      var starter = root.getElementsByTagName("A").item(0);
      var menu  = root.getElementsByTagName("UL").item(0);
      starter.onmouseover(); 
      menu.style.visibility = "hidden";
      stopTime();
    }
    
    function loadMenu() {
        if (initialized) return;
        if (!scriptLoaded) return;
        var root = document.getElementById(rootMenuId);
        getMenus(root, root);
        if (!ie) { positionMenus(root, root); }
        initialized = true;
    }

    function initialiseMenu(menu, starter, root) {

             
        if (menu == null || starter == null) return;

        currentMenu = menu;

        starter.onmouseover = function() {
            if (currentMenu) {                
                currentMenu.style.visibility = "hidden";                
                currentMenu = null;
                this.showMenu();
            }
        }

        starter.showMenu = function() {
            
            currentMenu = menu;
            menu.style.display = "block";
            menu.style.visibility = "visible";
            var root = document.getElementById(rootMenuId);
            positionMenus(root, root);
        }

        starter.onfocus = function() {
            starter.onmouseover();
        }

        menu.onmouseover = function() {
            if (currentMenu) {
                currentMenu = null;             
                this.showMenu();
            }
        }   

        menu.onfocus = function() {
//          currentMenu.style.visibility = "hidden";
        }

        menu.showMenu = function() {
            
            menu.style.display = "block";
            menu.style.visibility = "visible";
            currentMenu = menu;
            stopTime();
        }

        menu.hideMenu = function()  {
            if (!timerOn) {
                mytimer = setInterval("killMenu('" + this.id + "', '" + root.id + "');", delay);
                timerOn = true;
            }
        }

        menu.onmouseout = function(event) {
            this.hideMenu();
        }

        starter.onmouseout = function() {
            menu.hideMenu();
        }
    }

   

    function killMenu(menu, root) {
        var menu = document.getElementById(menu);
        var root = document.getElementById(root);
        menu.style.visibility = "hidden";
        stopTime();
    }
    
    function stopTime() {
        if (mytimer) {
             clearInterval(mytimer);
             mytimer = null;
             timerOn = false;
        }
    } 
    
    function getMenus(elementItem, root) {    
        var menuStarter;
        var menuItem;
        for (var x = 0; x < elementItem.childNodes.length; x++) {
            if (elementItem.childNodes[x].nodeName == "TD") {
                if (elementItem.childNodes[x].getElementsByTagName("UL").length > 0) {
                    menuStarter = elementItem.childNodes[x].getElementsByTagName("A").item(0);
                    menuItem = elementItem.childNodes[x].getElementsByTagName("UL").item(0);
                    initialiseMenu(menuItem, menuStarter, root);
                }
            }
        }
    }

    function positionMenus(elementItem, root) {
        var menuStarter;
        var menuItem;
        for (var x = 0; x < elementItem.childNodes.length; x++) {
            if (elementItem.childNodes[x].nodeName == "TD") {
                if (elementItem.childNodes[x].getElementsByTagName("UL").length > 0) {
                    menuStarter = elementItem.childNodes[x].getElementsByTagName("A").item(0);
                    menuItem = elementItem.childNodes[x].getElementsByTagName("UL").item(0);
                    positionMenu(menuItem, menuStarter, root);
                }
            }
        }
    }
    
    
    function positionMenu(menu, starter, root) {
        menu.style.left = "";
        
        var diff = maxWidth - (starter.parentNode.offsetLeft + menu.offsetWidth);
        if (diff < 0) {
        var menuOffsetLeft = menu.offsetLeft;
        menu.style.display = "none";
        menu.style.left = (menuOffsetLeft + diff) + "px";
        menu.style.display = "block";
        }
    }
    
// Initialise un cookie 
function SetCookie(name, value) {
    var argv = SetCookie.arguments;
    var argc = SetCookie.arguments.length;
    var expires = (argc > 2) ? argv[2] : null;
    var path = (argc > 3) ? argv[3] : null;
    var domain = (argc > 4) ? argv[4] : null;
    var secure = (argc > 5) ? argv[5] : false;
    document.cookie=name+"="+escape(value)+
                    ((expires==null) ? "" : ("; expires="+expires.toGMTString()))+
                    ((path==null) ? "" : ("; path="+path))+
                    ((domain==null) ? "" : ("; domain="+domain))+
                    ((secure==true) ? "; secure" : "");
} 

// Retourne la valeur d'un �lement enregistr� dans le cookie 
function getCookieVal(offset){
    var endstr=document.cookie.indexOf(";",offset);
    if (endstr==-1)
        endstr=document.cookie.length;
    return unescape(document.cookie.substring(offset, endstr));
}

// Teste l'existence d'un cookie (retourne null si non) 
function GetCookie(name){
    var arg=name+"=";
    var alen=arg.length;
    var clen=document.cookie.length;
    var i=0;
    while (i<clen) {
        var j=i+alen;
        if (document.cookie.substring(i,j)==arg)
            return getCookieVal(j);
        i=document.cookie.indexOf(" ",i)+1;
        if (i==0)
            break;    
    }
    return null;
}

// Efface le cookie en lui fixant une date expir�e 
function ViderCookie(){
    var pathname=location.pathname;
    var myDomain=pathname.substring(0,pathname.lastIndexOf('/'))+'/';
    var date_exp=new Date();
    date_exp.setTime(date_exp.getTime()-(1000));
    SetCookie("pop1fois","",date_exp,myDomain);
}

// Ouvre une popup et initialise un cookie 
function ouvrePopup(p){
    popup = window.open(p,'',"top=0","left=0","width=1","height=1","menubar=no","scrollbars=yes","resizable=no","status=no");
    var pathname=location.pathname;
    var myDomain=pathname.substring(0,pathname.lastIndexOf('/'))+'/'; 
    var date_exp=new Date();
    date_exp.setTime(date_exp.getTime()+(0.1*3600*1000));
    SetCookie("pop1fois","ok",date_exp,myDomain);
    popup.focus();
}

// Lancement de la popup SIG 
function popupSIG(p) {
            
  if (GetCookie("pop1fois")==null){
     ouvrePopup(p);   
  }   
  else {
         popup.close();          
         ViderCookie();         
         ouvrePopup(p);        
  } 
}

//function for the page print
function popupPrint(p) {
    var printPop = p+"/printer";    
    window.open(printPop,'','width=450,height=450,top=0,left=0,menubar=yes,scrollbars=yes,resizable=yes,status=yes');
}

//function for the page print WEBSAM
function popupPrintWebSam(p) {
    var printPop = p+"/printerWEBSAM";  
    window.open(printPop,'','width=450,height=450,top=0,left=0,menubar=yes,scrollbars=yes,resizable=yes,status=yes');
}

//function for the page calculette PE websam 2
function popupCalculettePE(p) {
    var printPop = p+"/calculPE";  
    window.open(printPop,'CalculettePE','width=650,height=500,top=0,left=0,menubar=no,scrollbars=no,resizable=yes,status=yes');
}

//function for the page calculette TR websam 2
function popupCalculetteTR(p) {
    var printPop = p+"/calculTR";  
    window.open(printPop,'CalculetteTR','width=650,height=500,top=0,left=0,menubar=no,scrollbars=no,resizable=yes,status=yes');
}

//function for the help download
function popupDownload(contexPath, pictoPath) {
    var popupPath = contexPath+"/downloadFiles"+"?pictoPath="+pictoPath;
    var screenHeight = window.screen.availHeight;
    var screenWidth = window.screen.availWidth;
    var popupHeight = 400;
    var popupWidth = 450;
    var leftCorner = parseInt(screenWidth/2) - parseInt(popupWidth/2);
    var topCorner = parseInt(screenHeight/2) - parseInt(popupHeight/2);    
    window.open(popupPath, '', 'width=' + popupWidth + ', height=' + popupHeight + ', left=' + leftCorner + ', top=' + topCorner + ', menubar=no,scrollbars=no,resizable=no,status=no');    
}

//supprime les tags dans le navpath pour affichage title dans popupimage
function ReformuleNavPath(str){
	var newtext="";
	var j=0;
	var bIsTag=false
	for(var i=0;i<str.length;i++){
		var tempChar=str.substr(i,1);

		if(tempChar=="<"){
			bIsTag=true;
		}
		if(!bIsTag){
			newtext+=tempChar;
		}
		if(tempChar==">"){
			bIsTag=false;
		}

	}
	return newtext;
}
//ouvre une popup image
function popupImage(ciblef5, boutonFermer,width,height,legend,copyright){  
    var titrepopup=ciblef5;
    var ebxNavpathInnerHTML="";
    if(document.getElementById("ebxNavpath")){
      ebxNavpathInnerHTML= document.getElementById("ebxNavpath").innerHTML;
       titrepopup=ReformuleNavPath(ebxNavpathInnerHTML);
    }
    var mypop=window.open(titrepopup,null,"height="+height+",width="+width+",menubar=no,scrollbars=no,resizable=no,status=no");
    mypop.document.write('<html><head><title>'+titrepopup+'</title>');
    for(var i=0;i<document.styleSheets.length;i++){
                mypop.document.write("<link href=\""+document.styleSheets[i].href+"\" rel=\"stylesheet\" type=\"text/css\" />");
    }
     mypop.document.write('</head><body>');
     mypop.document.write('<div class="container"><table width="100%" ><tr><td width="80%" align=left>');
     mypop.document.write('<img src="http://www.bordeaux.fr/ebx/framework/skins/ebxDefault/images/logo_impression.gif">');
     mypop.document.write('</td><td width="20%" align=right><div class="container"><span class="Link2"><ul><li>');            
     mypop.document.write('<a  href="javascript:onclick=self.close();" title="fermer la fen&ecirc;tre">Fermer la fen&ecirc;tre</a>');
     mypop.document.write('</li></ul></span></div></td></tr><tr><td id="navpath"></td><td></td></tr></table></div>');
     mypop.document.write('<br><center><img src="http://www.bordeaux.fr/ebx/framework/skins/ebxDefault/js/'+ciblef5+'"></center>');
     mypop.document.write('<span class="Commun1"><center style="margin-top:10px">'+legend + '<br>'+ copyright+'</center></span>');
     mypop.document.write('</body></html>');
}   

function popupAide(cible){        
    var width = 800;
    var height = 450;
    var top = (screen.availheight - height - 30) / 2;
    var left = (screen.availwidth - width) / 2; 
    var mypop=window.open(cible,'','top=' + top + ',left=' + left + ',width=' + width + ',height=' + height + ',status=no,menubar=no,toolbar=no,navigationbar=no,scrollbars=yes,resizable=yes');         
}      
        
scriptLoaded = true;

