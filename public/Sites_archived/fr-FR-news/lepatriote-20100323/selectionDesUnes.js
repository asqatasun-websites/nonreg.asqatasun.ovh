var selectionDesUnes = {

	// create imageElements in Dom
	putHTMLInDom:function(tabImages){
		$("#imagesContains").slideUp("slow");
		for (var i = 0; i < tabImages.length; i++) {
			if(tabImages[i] != ''){
				
				// determine fullImage Src
				var partOfPath = tabImages[i].split('/');
				var imageName = partOfPath.pop();
				var fullImageSrc = partOfPath.join('/') + '/fullImage/' + imageName;
				
				$("#imagesContains").append('<a href="' + fullImageSrc + '" class="lightbox" rel="<a href=\'/index.php/plus/abonnement\'>Commander cette �dition</a>"><img src="' + tabImages[i] + '" /></a>');	
			}
		}
		$('.lightbox').lightBox({overlayOpacity: 0.8});
		$("#imagesContains").slideDown("fast");
		$("#imagesContains img").animate({width: "70%", height: "70%", marginLeft: "13%"}, "fast");
		$("#imagesContains img").hover(function() {
	    $(this).animate({width: "100%", height: "100%", marginLeft: "0"}, "slow");
		  }, function() {
		  $(this).animate({width: "70%", height: "70%", marginLeft: "13%"}, "fast");
		});

	},

	// ajax : return fileName and call putHTMLInDom()
	displayOne:function(month,year){
		
				$.ajax({
				   type: "POST",
					url: "/index.php?page=shop/lePatrioteSelectionDesUnes",
				   data : "month=" + month + "&year=" + year,
				   dataType: "json",
				   /*success: function(data){
					 alert( "Data Saved: " +  data.city );
					 
				   }*/
				   complete: function(request, settings){
						var images = request.responseText;					
						var tabImages = images.split('!!');	
						selectionDesUnes.putHTMLInDom(tabImages);
					   }
				 });
				 
		
	},


	// init
	init:function(){
		selectionDesUnes.initYear();
		
		// par d�faut, affichage des unes du mois courant 
		var dob=new Date();
		var numMonth = dob.getMonth() + 1;
		month = selectionDesUnes.getMonthByNum(numMonth);// mettre numMonth - 1 pour afficher le mois pr�c�dent au chargement
		var year=dob.getFullYear();
		selectionDesUnes.displayOne(month, year);
		
		// prepare evenment onclick for previous and next year
		selectionDesUnes.initChangeYear();
		
		//met en valeur le mois actuel par d�faut
		selectionDesUnes.formatCurrentMonth($("#" + month));
		
		// click on a month
		$(".month").click(function(){
			//enleve les anciennes images
			$("#imagesContains").empty();
			selectionDesUnes.changeMonth($(this));	
			
			// d�formatage de tous les mois
			selectionDesUnes.unFormatMonth();

		
			//met en valeur le mois selectionn�
			selectionDesUnes.formatCurrentMonth($(this));
		});
	},
	
	unFormatMonth:function(){
		$(".month").each(function(m){
			 $(this).css({ background: "url(/design/images/calendrier_mois.gif)" });	
		 });
	},
	
	formatCurrentMonth:function(monthElement){
		// formate le mois voulu
		monthElement.css({ background: "url(/design/images/calendrier_mois_over.gif)" });	
	},
		
	getMonthByNum:function(numMonth){
		var month = {"1": "janvier", "2": "fevrier", "3": "mars", "4": "avril", "5": "mai", "6": "juin", "7": "juillet", "8": "aout", "9": "septembre", "10": "octobre", "11": "novembre", "12": "decembre"};
		return month[numMonth];
	},
	
	changeMonth:function(monthElement){
		var month = monthElement.attr("id");
		var year = $('#year').html();
		selectionDesUnes.displayOne(month, year);
	},
	
	//prepare evenment click on previous and next button year
	initChangeYear:function(){
		$("#previousYearButton").click(function(){
			selectionDesUnes.viewPreviousYear();
		});
		$("#nextYearButton").click(function(){
			selectionDesUnes.viewNextYear();
		});
	},
	
	yearAllowed:new Array('2005','2006','2007', '2008', '2009'),
	
	viewPreviousYear:function(){
		var year = $('#year').html();// ann�e actuellement vue
		year = parseInt(year) - 1;
		var previousYear = year - 1;
		var nextYear = year + 1;
		
		// d�formatage de tous les mois
		selectionDesUnes.unFormatMonth();
			
		if(selectionDesUnes.in_array(year, selectionDesUnes.yearAllowed)){
			$('#nextYear').empty();
			$('#nextYear').append(nextYear);	
			$('#year').empty();
			$('#year').append(year);
			$('#previousYear').empty();	
			if(selectionDesUnes.in_array(previousYear, selectionDesUnes.yearAllowed)){
				$('#previousYear').append(previousYear);
			}
		}
	},
	
	viewNextYear:function(){
		var year = $('#year').html();// ann�e actuellement vue
		year = parseInt(year) + 1;
		var previousYear = year - 1;
		var nextYear = year + 1;
		
		// d�formatage de tous les mois
		selectionDesUnes.unFormatMonth();
		
		if(selectionDesUnes.in_array(year, selectionDesUnes.yearAllowed)){
			$('#previousYear').empty();
			$('#previousYear').append(previousYear);	
			$('#year').empty();
			$('#year').append(year);
			$('#nextYear').empty();
			if(selectionDesUnes.in_array(nextYear, selectionDesUnes.yearAllowed)){
				$('#nextYear').append(nextYear);
			}		
		}	
	},
	
	in_array:function(needle, haystack, argStrict) {
		var found = false, key, strict = !!argStrict;
	 
		for (key in haystack) {
			if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
				found = true;
				break;
			}
		}
	 
		return found;
	},
	
	
	
	initYear:function(){
		var dob=new Date();
		var year=dob.getFullYear();// ann�e actuelle
		var previousYear = year -1;
		var nextYear = year +1;
		
		if(selectionDesUnes.in_array(previousYear, selectionDesUnes.yearAllowed)){
			$('#previousYear').append(previousYear);		
		}		
		$('#year').append(year);		
		if(selectionDesUnes.in_array(nextYear, selectionDesUnes.yearAllowed)){
			$('#nextYear').append(nextYear);
		}
	}
}

//addEvent(window, "load", init);
$(document).ready(function(){ selectionDesUnes.init()});