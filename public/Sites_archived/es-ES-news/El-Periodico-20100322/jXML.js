// Funciones JavaScript para AJAX
// En principio creadas para el 11ideal de sport
// Creaci�: 02/11/2006 
// David Bolet

//var url="http://zetasport2007.gzimasd.gz.es"
url="http://www.sport.es"

// ******************************************************
// * Funci� que crea un HTTPRequest independentment 
// * del navegador (en teoria...)
// ******************************************************
function creaRequest() 
{
         try {
				//IE5.5 i 6
                 req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch ( e) {
                 try { 
						//IE 5.0
                         req= new ActiveXObject ("Microsoft.XMLHTTP");
                 } catch (E) {
                          req= false; 
                } 
        }
        //Firefox
        if (! req && typeof XMLHttpRequest!= 'undefined') {
                 req = new XMLHttpRequest();
        } 
        return req
}

// ******************************************************
// * Funci� que recull les dades XML de les estadistiques 
// * i les dibuixa
// ******************************************************
function mostraEstadistica(idTemporada,idCompeticion,idJornada,tipo,mantener,idDIVResultado)
{
	var xsltProcessor,ffox,tipoDato;
	if (document.implementation && document.implementation.createDocument) 
	{ 
		// Firefox 
		xsltProcessor = new XSLTProcessor(); 
		ffox=true;
	}
	else
		ffox=false;
	// Load XSL 
	var myXMLHTTPRequest = creaRequest(); 
	//if (typeof(myXMLHTTPRequest)!='undefined') alert("myXMLHTTPRequest creat");
	try 
	{
		if (tipo=="res")
		{
			myXMLHTTPRequest.open("GET", url+"/comunes/11ideal/ResultOnceNou.xsl", false); 
			tipoDato="RESULTADO";
		}
		else						
		{
			myXMLHTTPRequest.open("GET", url+"/comunes/11ideal/EstadisticasNouE.xsl", false); 
			tipoDato="ESTADISTICAS";
		}
		myXMLHTTPRequest.send(null); 
	}
	catch ( e) 
	{
		alert("Excepcion: "+e);
		return null;
	}
	// Get XML 	
	xslStylesheet = myXMLHTTPRequest.responseXML; 	
	if (ffox)
	{
		try 
		{
			xsltProcessor.importStylesheet(xslStylesheet); 
		}
		catch ( e) 
		{
			alert("Excepcion: "+e);
			return null;
		}
		// Pass all filter threshold values 
		//xsltProcessor.setParameter(null, "http://www.elperiodico.com/comunes/11ideal/", js_threshold_num_pixel_diff) ; 
	}
	// Load XML 
	myXMLHTTPRequest = creaRequest(); 
	myXMLHTTPRequest.open("GET", url+"/comunes/11ideal/Datos.aspx?idTemporada="+idTemporada+"&idCompeticion="+idCompeticion+"&idJornada="+idJornada+"&tipoDato="+tipoDato, false); 
	myXMLHTTPRequest.send(null); 
	var xmlSource = myXMLHTTPRequest.responseXML;     
	// Transform 
	if (ffox)
	{
		var resultDocument = xsltProcessor.transformToFragment(xmlSource, document); 		
		while (document.getElementById(idDIVResultado).hasChildNodes()) {
		    document.getElementById(idDIVResultado).removeChild(document.getElementById(idDIVResultado).firstChild)
		}
		document.getElementById(idDIVResultado).appendChild(resultDocument);		
		if (document.getElementById("textoVotado")&& mantener!="mantener") 
			document.getElementById("textoVotado").removeChild(document.getElementById("textoVotado").firstChild);
		if (document.getElementById("textoVotado1")&& mantener!="mantener") 
			document.getElementById("textoVotado1").removeChild(document.getElementById("textoVotado").firstChild);
	}
	else
	{
	    var xsldoc = new ActiveXObject("Microsoft.XMLDOM");		
		xsldoc.load(xslStylesheet);
		var outputXHTML = xmlSource.transformNode(xsldoc);
		document.getElementById(idDIVResultado).innerHTML�=�outputXHTML;
		if (false && tipo=="res")
		    alert("Resultado votacion:\n"+outputXHTML);		    
		if (document.getElementById("textoVotado") && mantener!="mantener") 
			document.getElementById("textoVotado").innerHTML�="";
		if (document.getElementById("textoVotado1") && mantener!="mantener") 
			document.getElementById("textoVotado1").innerHTML�="";


	}
}


// ******************************************************
// * Funci� que recull les dades XML de les estadistiques 
// * i les dibuixa
// ******************************************************
function mostraEstadistica1(idTemporada,idCompeticion,idJornada,tipo,mantener)
{
	var xsltProcessor,ffox,tipoDato;
	if (document.implementation && document.implementation.createDocument) 
	{ 
		// Firefox 
		xsltProcessor = new XSLTProcessor(); 
		ffox=true;
	}
	else
		ffox=false;
	// Load XSL 
	var myXMLHTTPRequest = creaRequest(); 
	//if (typeof(myXMLHTTPRequest)!='undefined') alert("myXMLHTTPRequest creat");
	try 
	{
		if (tipo=="res")
		{
			myXMLHTTPRequest.open("GET", url+"/comunes/11ideal/Resultonce.xsl", false); 
			tipoDato="RESULTADO";
		}
		else						
		{
			myXMLHTTPRequest.open("GET", url+"/comunes/11ideal/EstadisticasNE.xsl", false); 
			tipoDato="ESTADISTICAS";
		}
		myXMLHTTPRequest.send(null); 
	}
	catch ( e) 
	{
		alert("Excepcion: "+e);
		return null;
	}
	// Get XML 	
	xslStylesheet = myXMLHTTPRequest.responseXML; 	
	if (ffox)
	{
		try 
		{
			xsltProcessor.importStylesheet(xslStylesheet); 
		}
		catch ( e) 
		{
			alert("Excepcion: "+e);
			return null;
		}
		// Pass all filter threshold values 
		//xsltProcessor.setParameter(null, "http://www.elperiodico.com/comunes/11ideal/", js_threshold_num_pixel_diff) ; 
	}
	// Load XML 
	myXMLHTTPRequest = creaRequest(); 
	myXMLHTTPRequest.open("GET", url+"/comunes/11ideal/Datos.aspx?idTemporada="+idTemporada+"&idCompeticion="+idCompeticion+"&idJornada="+idJornada+"&tipoDato="+tipoDato, false); 
	myXMLHTTPRequest.send(null); 
	var xmlSource = myXMLHTTPRequest.responseXML;     
	// Transform 
	if (ffox)
	{
		var resultDocument = xsltProcessor.transformToFragment(xmlSource, document); 		
		if (document.getElementById("resultadoBarsa").hasChildNodes())
		    document.getElementById("resultadoBarsa").removeChild(document.getElementById("resultadoBarsa").firstChild)
		    document.getElementById("resultadoBarsa").appendChild(resultDocument);		
		if (document.getElementById("textoVotado")&& mantener!="mantener") 
			document.getElementById("textoVotado").removeChild(document.getElementById("textoVotado").firstChild);
	}
	else
	{
	    var xsldoc = new ActiveXObject("Microsoft.XMLDOM");		
		xsldoc.load(xslStylesheet);
		var outputXHTML = xmlSource.transformNode(xsldoc);
		document.getElementById("resultadoBarsa").innerHTML�=�outputXHTML;
		if (document.getElementById("textoVotado") && mantener!="mantener") 
			document.getElementById("textoVotado").innerHTML�="";
	}
}



// ******************************************************
// * Funci� que recull les dades XML d'un equip i 
// * les dibuixa
// ******************************************************
function procesaXMLEquipo(idEquipo,idDeporte,idDIVResultado)
{
	var xsltProcessor,ffox;
	if (document.implementation && document.implementation.createDocument) 
	{ 
		// Firefox 
		xsltProcessor = new XSLTProcessor(); 
		ffox=true;
	}
	else
		ffox=false;
	// Load XSL 
	var myXMLHTTPRequest = creaRequest(); 
	//if (typeof(myXMLHTTPRequest)!='undefined') alert("myXMLHTTPRequest creat");
	try 
	{
		myXMLHTTPRequest.open("GET", url+"/comunes/11ideal/Equipo.xsl", false); 
		myXMLHTTPRequest.send(null); 
	}
	catch ( e) 
	{
		alert("Excepcion: "+e);
		return null;
	}
	// Get XML 	
	xslStylesheet = myXMLHTTPRequest.responseXML; 	
	if (ffox)
	{
		try 
		{
			xsltProcessor.importStylesheet(xslStylesheet); 
		}
		catch ( e) 
		{
			alert("Excepcion: "+e);
			return null;
		}
		// Pass all filter threshold values 
		//xsltProcessor.setParameter(null, "http://www.elperiodico.com/comunes/11ideal/", js_threshold_num_pixel_diff) ; 
	}
	// Load XML 
	myXMLHTTPRequest = creaRequest(); 
	myXMLHTTPRequest.open("GET", url+"/comunes/11ideal/Datos.aspx?idEquipo="+idEquipo+"&tipoDato=EQUIPO", false); 
	myXMLHTTPRequest.send(null); 
	var xmlSource = myXMLHTTPRequest.responseXML; 

	// Transform 
	if (ffox)
	{
		var resultDocument = xsltProcessor.transformToFragment(xmlSource, document); 		
		document.getElementById(idDIVResultado).removeChild(document.getElementById(idDIVResultado).firstChild)
		document.getElementById(idDIVResultado).appendChild(resultDocument); 
	}
	else
	{
	    var xsldoc = new ActiveXObject("Microsoft.XMLDOM");		
		xsldoc.load(xslStylesheet);
		var outputXHTML = xmlSource.transformNode(xsldoc);
		//alert(outputXHTML);
		document.getElementById(idDIVResultado).innerHTML�=�outputXHTML;
	} 
}


// ******************************************************
// * Funci� que recull les dades XML d'un equip i 
// * les dibuixa
// ******************************************************
function ProcesaCaye(fecha,idDIVResultado)
{
	var xsltProcessor,ffox,fecha2;
    var day=fecha.substring(0,fecha.indexOf("/"));
    var pos2=fecha.indexOf("/",fecha.indexOf("/")+1);
    var month=fecha.substring(fecha.indexOf("/")+1,pos2);
    var anyo=fecha.substring(pos2+1);	
	if (day<10) 
	    fecha2="0"+(1*day);
    else
    	fecha2=""+(1*day);
	if (month<10)
	    fecha2=fecha2+"/0"+(1*month);
	else
	    fecha2=fecha2+"/"+(1*month);
	fecha2=fecha2+"/"+anyo;

	    
	if (document.implementation && document.implementation.createDocument) 
	{ 
		// Firefox 
		xsltProcessor = new XSLTProcessor(); 
		ffox=true;
	}
	else
		ffox=false;
	// Load XSL 
	var myXMLHTTPRequest = creaRequest(); 
	//if (typeof(myXMLHTTPRequest)!='undefined') alert("myXMLHTTPRequest creat");
	try 
	{
	    if (idDIVResultado=="resultado2")
	    	myXMLHTTPRequest.open("GET", url+"/dissenys/xsl/caye2.xsl", false); 
		else
    		myXMLHTTPRequest.open("GET", url+"/dissenys/xsl/caye.xsl", false); 
		myXMLHTTPRequest.send(null); 
	}
	catch ( e) 
	{
		alert("Excepcion: "+e);
		return null;
	}
	// Get XML 	
	xslStylesheet = myXMLHTTPRequest.responseXML; 	
	if (ffox)
	{
		try 
		{
			xsltProcessor.importStylesheet(xslStylesheet); 
		}
		catch ( e) 
		{
			alert("Excepcion: "+e);
			return null;
		}
		// Pass all filter threshold values 
		//xsltProcessor.setParameter(null, "http://www.elperiodico.com/comunes/11ideal/", js_threshold_num_pixel_diff) ; 
	}
	// Load XML 
	myXMLHTTPRequest = creaRequest(); 
	myXMLHTTPRequest.open("GET", url+"/comunes/11ideal/GetXmlFotos.aspx?idSeccio=800&fecha="+fecha2, false); 
	myXMLHTTPRequest.send(null); 
	var xmlSource = myXMLHTTPRequest.responseXML; 

	// Transform 
	if (ffox)
	{
		var resultDocument = xsltProcessor.transformToFragment(xmlSource, document); 	
		while (document.getElementById(idDIVResultado).childNodes.length>0)
		    document.getElementById(idDIVResultado).removeChild(document.getElementById(idDIVResultado).firstChild)
		document.getElementById(idDIVResultado).appendChild(resultDocument); 
	}
	else
	{
	    var xsldoc = new ActiveXObject("Microsoft.XMLDOM");		
		xsldoc.load(xslStylesheet);
		var outputXHTML = xmlSource.transformNode(xsldoc);
		//alert(outputXHTML);
		document.getElementById(idDIVResultado).innerHTML�=�outputXHTML;
	} 
}



