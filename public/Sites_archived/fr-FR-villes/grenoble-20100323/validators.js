function isNumeric(str)
{
	for (var i=0;i<str.length;i++)
		if ("0123456789".indexOf(str.substr(i,1))<0)
			return false;

	return true;
}

function checkMultiple(args)
{
	for (var i=1;i<checkMultiple.arguments.length;i++)
    if (!checkMultiple.arguments[0](checkMultiple.arguments[i]))
      return false;

	return true;
}

function checkRequired(formField)
{
  if (formField.value == "")
  {
    alert("Les champs marqu�s d'un (*) sont obligatoires !");
    formField.focus();
    return false;
  }

	return true;
}

function checkRequiredRadioGroup(formField)
{
  for (i=0; i<formField.length; i++)
  {
    if (formField[i].checked)
      return true;
  }

  alert("Les champs marqu�s d'un (*) sont obligatoires !");
  formField[0].focus();
	return false;
}

function checkNumber(formField, required)
{
	if (!required && formField.value == "")
		return true;

  var str = formField.value;

	// Note: doesn't use regular expressions to avoid early Mac browser bugs
	for (var i=0;i<str.length;i++)
		if ("0123456789".indexOf(str.substr(i,1))<0)
		{
		  alert("Veuillez saisir un nombre entier !");
  		formField.focus();
			return false;
		}

	return true;
}

function checkRequiredDate(formField)
{
  return checkDate(formField, true);
}

function checkDate(formField, required)
{
	if (!required && formField.value == "")
		return true;

	if (required && !checkRequired(formField))
		return false;

	var result = true;

 	if (result)
 	{
 		var elems = formField.value.split("/");

 		result = (elems.length == 3); // should be three components

 		if (result)
 		{
			var day = parseInt(elems[0],10);
 			var month = parseInt(elems[1],10);
 			var year = parseInt(elems[2],10);
			result = isNumeric(elems[0]) && (month > 0) && (month < 13) &&
					 isNumeric(elems[1]) && (day > 0) && (day < 32) &&
					 isNumeric(elems[2]) && ((elems[2].length == 2) || (elems[2].length == 4));
 		}

  	if (!result)
 		{
 			alert("Veuillez saisir une date au format JJ/MM/AAAA");
			formField.focus();
		}
	}

	return result;
}

function isFirstDateBeforeSecondDate(firstDateFormField, secondDateFormField)
{
  var elems1 = firstDateFormField.value.split("/");
  var elems2 = secondDateFormField.value.split("/");

  var yyyymmdd1 = elems1[2] + elems1[1] + elems1[0];
  var yyyymmdd2 = elems2[2] + elems2[1] + elems2[0];

  if (yyyymmdd1 > yyyymmdd2)
  {
    alert("La date de fin doit �tre post�rieure � la date de d�but !");
    secondDateFormField.focus();
  	return false;
  }

	return true;
}

function checkEmailAndRequirement(formField, required, fieldName)
{
	if (!required && formField.value == "")
		return true;

	if (required && !checkRequired(formField))
		return false;
		
	return checkEmailWithFieldName(formField, fieldName);
}

function checkEmailWithFieldName(formField, fieldName) {

  var emailStr = formField.value;

  /* The following variable tells the rest of the function whether or not
  to verify that the address ends in a two-letter country or well-known
  TLD.  1 means check it, 0 means don't. */

  var checkTLD=1;

  /* The following is the list of known TLDs that an e-mail address must end with. */

  var knownDomsPat=/^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum)$/;

  /* The following pattern is used to check if the entered e-mail address
  fits the user@domain format.  It also is used to separate the username
  from the domain. */

  var emailPat=/^(.+)@(.+)$/;

  /* The following string represents the pattern for matching all special
  characters.  We don't want to allow special characters in the address.
  These characters include ( ) < > @ , ; : \ " . [ ] */

  var specialChars="\\(\\)><@,;:\\\\\\\"\\.\\[\\]";

  /* The following string represents the range of characters allowed in a
  username or domainname.  It really states which chars aren't allowed.*/

  var validChars="\[^\\s" + specialChars + "\]";

  /* The following pattern applies if the "user" is a quoted string (in
  which case, there are no rules about which characters are allowed
  and which aren't; anything goes).  E.g. "jiminy cricket"@disney.com
  is a legal e-mail address. */

  var quotedUser="(\"[^\"]*\")";

  /* The following pattern applies for domains that are IP addresses,
  rather than symbolic names.  E.g. joe@[123.124.233.4] is a legal
  e-mail address. NOTE: The square brackets are required. */

  var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;

  /* The following string represents an atom (basically a series of non-special characters.) */

  var atom=validChars + '+';

  /* The following string represents one word in the typical username.
  For example, in john.doe@somewhere.com, john and doe are words.
  Basically, a word is either an atom or quoted string. */

  var word="(" + atom + "|" + quotedUser + ")";

  // The following pattern describes the structure of the user

  var userPat=new RegExp("^" + word + "(\\." + word + ")*$");

  /* The following pattern describes the structure of a normal symbolic
  domain, as opposed to ipDomainPat, shown above. */

  var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$");

  /* Finally, let's start trying to figure out if the supplied address is valid. */

  /* Begin with the coarse pattern to simply break up user@domain into
  different pieces that are easy to analyze. */

  var matchArray=emailStr.match(emailPat);

  if (matchArray==null) {

  /* Too many/few @'s or something; basically, this address doesn't
  even fit the general mould of a valid e-mail address. */

  alert(fieldName + " invalide !");
  formField.focus();
  return false;
  }
  var user=matchArray[1];
  var domain=matchArray[2];

  // Start by checking that only basic ASCII characters are in the strings (0-127).

  for (i=0; i<user.length; i++) {
  if (user.charCodeAt(i)>127) {
  alert(fieldName + " invalide !");
  formField.focus();
  return false;
     }
  }
  for (i=0; i<domain.length; i++) {
  if (domain.charCodeAt(i)>127) {
  alert(fieldName + " invalide !");
  formField.focus();
  return false;
     }
  }

  // See if "user" is valid

  if (user.match(userPat)==null) {

  // user is not valid

  alert(fieldName + " invalide !");
  formField.focus();
  return false;
  }

  /* if the e-mail address is at an IP address (as opposed to a symbolic
  host name) make sure the IP address is valid. */

  var IPArray=domain.match(ipDomainPat);
  if (IPArray!=null) {

  // this is an IP address

  for (var i=1;i<=4;i++) {
  if (IPArray[i]>255) {
  alert(fieldName + " invalide !");
  formField.focus();
  return false;
     }
  }
  return true;
  }

  // Domain is symbolic name.  Check if it's valid.

  var atomPat=new RegExp("^" + atom + "$");
  var domArr=domain.split(".");
  var len=domArr.length;
  for (i=0;i<len;i++) {
  if (domArr[i].search(atomPat)==-1) {
  alert(fieldName + " invalide !");
  formField.focus();
  return false;
     }
  }

  /* domain name seems valid, but now make sure that it ends in a
  known top-level domain (like com, edu, gov) or a two-letter word,
  representing country (uk, nl), and that there's a hostname preceding
  the domain or country. */

  if (checkTLD && domArr[domArr.length-1].length!=2 &&
  domArr[domArr.length-1].search(knownDomsPat)==-1) {
  alert(fieldName + " invalide !");
  formField.focus();
  return false;
  }

  // Make sure there's a host name preceding the domain.

  if (len<2) {
  alert(fieldName + " invalide !");
  formField.focus();
  return false;
  }

  // If we've gotten this far, everything's valid!
  return true;
}

// V1.1.3: Sandeep V. Tamhankar (stamhankar@hotmail.com)
function checkEmail(formField) {

  var emailStr = formField.value;

  /* The following variable tells the rest of the function whether or not
  to verify that the address ends in a two-letter country or well-known
  TLD.  1 means check it, 0 means don't. */

  var checkTLD=1;

  /* The following is the list of known TLDs that an e-mail address must end with. */

  var knownDomsPat=/^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum)$/;

  /* The following pattern is used to check if the entered e-mail address
  fits the user@domain format.  It also is used to separate the username
  from the domain. */

  var emailPat=/^(.+)@(.+)$/;

  /* The following string represents the pattern for matching all special
  characters.  We don't want to allow special characters in the address.
  These characters include ( ) < > @ , ; : \ " . [ ] */

  var specialChars="\\(\\)><@,;:\\\\\\\"\\.\\[\\]";

  /* The following string represents the range of characters allowed in a
  username or domainname.  It really states which chars aren't allowed.*/

  var validChars="\[^\\s" + specialChars + "\]";

  /* The following pattern applies if the "user" is a quoted string (in
  which case, there are no rules about which characters are allowed
  and which aren't; anything goes).  E.g. "jiminy cricket"@disney.com
  is a legal e-mail address. */

  var quotedUser="(\"[^\"]*\")";

  /* The following pattern applies for domains that are IP addresses,
  rather than symbolic names.  E.g. joe@[123.124.233.4] is a legal
  e-mail address. NOTE: The square brackets are required. */

  var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;

  /* The following string represents an atom (basically a series of non-special characters.) */

  var atom=validChars + '+';

  /* The following string represents one word in the typical username.
  For example, in john.doe@somewhere.com, john and doe are words.
  Basically, a word is either an atom or quoted string. */

  var word="(" + atom + "|" + quotedUser + ")";

  // The following pattern describes the structure of the user

  var userPat=new RegExp("^" + word + "(\\." + word + ")*$");

  /* The following pattern describes the structure of a normal symbolic
  domain, as opposed to ipDomainPat, shown above. */

  var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$");

  /* Finally, let's start trying to figure out if the supplied address is valid. */

  /* Begin with the coarse pattern to simply break up user@domain into
  different pieces that are easy to analyze. */

  var matchArray=emailStr.match(emailPat);

  if (matchArray==null) {

  /* Too many/few @'s or something; basically, this address doesn't
  even fit the general mould of a valid e-mail address. */

  alert("Adresse email invalide !");
  formField.focus();
  return false;
  }
  var user=matchArray[1];
  var domain=matchArray[2];

  // Start by checking that only basic ASCII characters are in the strings (0-127).

  for (i=0; i<user.length; i++) {
  if (user.charCodeAt(i)>127) {
  alert("Adresse email invalide !");
  formField.focus();
  return false;
     }
  }
  for (i=0; i<domain.length; i++) {
  if (domain.charCodeAt(i)>127) {
  alert("Adresse email invalide !");
  formField.focus();
  return false;
     }
  }

  // See if "user" is valid

  if (user.match(userPat)==null) {

  // user is not valid

  alert("Adresse email invalide !");
  formField.focus();
  return false;
  }

  /* if the e-mail address is at an IP address (as opposed to a symbolic
  host name) make sure the IP address is valid. */

  var IPArray=domain.match(ipDomainPat);
  if (IPArray!=null) {

  // this is an IP address

  for (var i=1;i<=4;i++) {
  if (IPArray[i]>255) {
  alert("Adresse email invalide !");
  formField.focus();
  return false;
     }
  }
  return true;
  }

  // Domain is symbolic name.  Check if it's valid.

  var atomPat=new RegExp("^" + atom + "$");
  var domArr=domain.split(".");
  var len=domArr.length;
  for (i=0;i<len;i++) {
  if (domArr[i].search(atomPat)==-1) {
  alert("Adresse email invalide !");
  formField.focus();
  return false;
     }
  }

  /* domain name seems valid, but now make sure that it ends in a
  known top-level domain (like com, edu, gov) or a two-letter word,
  representing country (uk, nl), and that there's a hostname preceding
  the domain or country. */

  if (checkTLD && domArr[domArr.length-1].length!=2 &&
  domArr[domArr.length-1].search(knownDomsPat)==-1) {
  alert("Adresse email invalide !");
  formField.focus();
  return false;
  }

  // Make sure there's a host name preceding the domain.

  if (len<2) {
  alert("Adresse email invalide !");
  formField.focus();
  return false;
  }

  // If we've gotten this far, everything's valid!
  return true;
}
