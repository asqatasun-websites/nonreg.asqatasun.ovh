//The following line is critical for menu operation, and MUST APPEAR ONLY ONCE. If you have more than one menu_array.js file rem out this line in subsequent files
menunum=0;
menus=new Array();
_d=document;
function addmenu(){
	menunum++;
	menus[menunum]=menu;
}
function dumpmenus(){
	mt="<scr"+"ipt language=\"javascript\" type=\"text/javascript\">";
	for(a=1;a<menus.length;a++){
		mt+=" menu"+a+"=menus["+a+"];";
	}
	mt+="<\/scr"+"ipt>";
	_d.write(mt);
}
//Please leave the above line intact. The above also needs to be enabled if it not already enabled unless this file is part of a multi pack.



////////////////////////////////////
// Editable properties START here //
////////////////////////////////////

// Special effect string for IE5.5 or above please visit http://www.milonic.co.uk/menu/filters_sample.php for more filters
effect = "Fade(duration=0.2);Alpha(style=0,opacity=95);Shadow(color='#777777', Direction=135, Strength=5)"


timegap=500			// The time delay for menus to remain visible
followspeed=5		// Follow Scrolling speed
followrate=40		// Follow Scrolling Rate
suboffset_top=4;	// Sub menu offset Top position 
suboffset_left=6;	// Sub menu offset Left position
closeOnClick = true

style1=[			// style1 is an array of properties. You can have as many property arrays as you need. This means that menus can have their own style.
"FFFFFF",			// Mouse Off Font Color
"09205f",			// Mouse Off Background Color
"FFFFFF",			// Mouse On Font Color
"09205f",			// Mouse On Background Color
"09205f",			// Menu Border Color 
10,					// Font Size in pixels
"normal",			// Font Style (italic or normal)
"bold",				// Font Weight (bold or normal)
"Arial",			// Font Name
0,					// Menu Item Padding
"arrow.gif",		// Sub Menu Image (Leave this blank if not needed)
,					// 3D Border & Separator bar
"66ffff",			// 3D High Color
"000099",			// 3D Low Color
"",					// Current Page Item Font Color (leave this blank to disable)
"",					// Current Page Item Background Color (leave this blank to disable)
"",					// Top Bar image (Leave this blank to disable)
"ffffff",			// Menu Header Font Color (Leave blank if headers are not needed)
"000099",			// Menu Header Background Color (Leave blank if headers are not needed)
"09205f",			// Menu Item Separator Color
]
style2=[			// style1 is an array of properties. You can have as many property arrays as you need. This means that menus can have their own style.
"FFFFFF",			// Mouse Off Font Color
"09205f",			// Mouse Off Background Color
"09205f",			// Mouse On Font Color
"FFFFFF",			// Mouse On Background Color
"ffffff",			// Menu Border Color 
9,					// Font Size in pixels
"normal",			// Font Style (italic or normal)
"bold",				// Font Weight (bold or normal)
"verdana",			// Font Name
4,					// Menu Item Padding
"arrow.gif",		// Sub Menu Image (Leave this blank if not needed)
,					// 3D Border & Separator bar
"66ffff",			// 3D High Color
"000099",			// 3D Low Color
"",					// Current Page Item Font Color (leave this blank to disable)
"",					// Current Page Item Background Color (leave this blank to disable)
"",					// Top Bar image (Leave this blank to disable)
"ffffff",			// Menu Header Font Color (Leave blank if headers are not needed)
"ffffff",			// Menu Header Background Color (Leave blank if headers are not needed)
"ffffff",			// Menu Item Separator Color
]