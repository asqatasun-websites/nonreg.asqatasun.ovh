var yes = 1;
var no = 0;
var menubar = no; // The File, Edit, View Menus
var scrollbars = no; // Horizontal and vertical scrollbars
var locationbar = no; // The location box with the site URL
var directories = no; // the "What's New", "What Cool" links
var resizable = yes; // Can the window be resized?
var statusbar = no; // Status bar (with "Document: Done")
var toolbar = no; // Back, Forward, Home, Stop toolbar

if (navigator.appName == "Microsoft Internet Explorer"){ // better be ie6 at least
windowprops = "width=" + (screen.width-10) + ",height=" + (screen.height-30) + ",top=0,left=0";
}
else { // i.e. if Firefox
windowprops = "width=" + (screen.width-5) + ",height=" + (screen.height-30) + ",top=0,left=0";
}
windowprops += (menubar ? ",menubars" : "") +
(scrollbars ? ",scrollbars" : "") +
(locationbar ? ",location" : "") +
(directories ? ",directories" : "") +
(resizable ? ",resizable" : "") +
(statusbar ? ",status" : "") +
(toolbar ? ",toolbar" : "");