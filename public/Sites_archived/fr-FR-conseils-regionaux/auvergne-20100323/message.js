var CraMessage = {
	id_page:0,
	/**
	 * complete callback
	 */
	completePost : function(request){
		var result = eval('(' + request.responseText + ')');
	
		if (result.isValid)
		{
			CraMessage.toggle();
			CraMessage.update(0, 0);
			Modalbox.show('', 'message_confirme.html', {width:380, height:250});
		}
		else
		{
			$('message_error').innerHTML = result.message;
			$('message_error').style.display = "block";
		}
	},
	post:function(form)
	{
		$('message_error').style.display = 'none';

		var ajax = new Ajax.Request(PmsUrl.getBaseUrl() + 'message/post', 
		{
			method: 'post', 
			parameters: Form.serialize(form),
			onComplete: CraMessage.completePost
		});
	},
	toggle :function()
	{
		var a = $('message_a');
		if ($('add_comment').style.display == 'none') {
			new Effect.BlindDown('add_comment');
			a.style.backgroundImage = 'url(public/gfx/deco/close.gif)';
		}
		else {
			new Effect.BlindUp('add_comment');		
			a.style.backgroundImage = 'url(public/gfx/titres/fr/boutons/je_reagis.gif)';
		}
		setFooter();
	},
	update : function(id, page)
	{
		if (id)
			CraMessage.id_page = id;
		else
			id = CraMessage.id_page;
		
		/*var ajax = new Ajax.Updater(
					'comments',
					'show_comments.html', 
					{
						method: 'get',
						parameters: 'page=' + page+'&id=' + id
					}
		);*/
		new Ajax.Request(
			PmsUrl.getBaseUrl()+'show_comments.html',
			{
				method: 'get',
				parameters: 'page=' + page+'&id=' + id,
				onComplete:this.onUpdateComplete
			}
		);
	},
	onUpdateComplete:function(response){
		$('comments').innerHTML=response.responseText;
	}
};