/******************************************************************************
* odfClientRuntime.js
*******************************************************************************
Runtime client pour les controles
*******************************************************************************
*                                                                             *
* Copyright 2000-2002								                          *
*                                                                             *
******************************************************************************/
function formatCurrency(currency, simple)
{
	var langPrefix = lang.substr(0, 2);
	if(simple != true && langPrefix == "en") simple = true;
	var sign ="";
	if(currency < 0) {
		currency = -currency;
		sign = "-";
	}
	var integer = Math.floor(currency);
	var full = "" + integer;
	// NB : A cause des arrondis, on utilise round au lieu de floor et on ne va pas factoriser l'expression mathematique en ecrivant : 100 * (money - integer)
	// Cas � probl�me : 583.05 | 152.89
	var decimal = "" + Math.round((100 * currency) - (100 * integer));
	if(decimal.length == 1) decimal = "0"+decimal;
	var res = null;
	var pos = full.length;
	while(pos > 3) {
		block = full.substr(pos - 3, 3);
		if(res == null) {
			res = block;
		} else {
			if(simple) {
				res = block+","+res;
			} else {
				res = block+"\240"+res;
			}
		}
		pos -= 3;
	}
	if(pos > 0) {
		block = full.substr(0, pos);
		if(res == null) {
			res = block;
		} else {
			if(simple) {
				res = block+","+res;
			} else {
				res = block+"\240"+res;
			}
		}
	}
	if(simple) {
		res += "."+decimal;
	} else {
		res += ","+decimal;
	}
	return sign+res;
}

function formatTimestamp(timestamp, format)
{
	switch(format) {
	case "hours":
		return objDateUtils.getStringDate(timestamp, "G:i:s");
	case "time":
		return objDateUtils.getStringDate(timestamp, "d/m/Y G:i:s");
	case "date":
	default:
		return objDateUtils.getStringDate(timestamp, "d/m/Y");
	}
}

function showModalWindow(url, name, width, height, arguments)
{
	var leftPos = (screen.availWidth - width) / 2;
	var topPos = (screen.availHeight - height) / 2;
	if(false && document.all) {
		width = parseInt(width, 10);
		height = parseInt(height, 10);
		width += 8;
		height += 29;
		var options = 'resizable=yes;scroll=no;edge=sunken;status=no;dialogWidth=' + width + 'px;dialogHeight=' + height + 'px;dialogLeft=' + leftPos + 'px;dialogTop=' + topPos + "px;";
		//consoleDump("window.showModalDialog(\"" + url + "\", arguments, \"" + options +"\")");
		window.showModalDialog(url, arguments, options);
	} else {
		var options = 'width=' + width + ',height=' + height + ',left=' + leftPos + ',top=' + topPos;
		var win = window.open(fixUrl(url), name, options);
		if (win==null) {
			alert(objThesaurus.translate("odfCheckPopupBlocker"));
		} else {
			win.focus();
		}
	}
}

function opennewwindow(linkElement, options, target)
{
	var href = linkElement.href;
	if(href == null || href == "") return false;
	var win = window.open(href, target, options);
	return false;
}

function tabClick(a)
{
	var srcLi = a.parentNode;
	var ul = srcLi.parentNode;
	for(var i=0;i < ul.childNodes.length;i++) {
		var li = ul.childNodes.item(i);
		if(li.nodeName != "LI") continue;
		var id = li.id;
		var tabBody = document.getElementById("tab" + id);
		if(li.tagName != "LI" || tabBody == null) {
			continue;
		}
		if(li == srcLi) {
			tabBody.className = tabBody.className.replace(/^tabBody$|tabBody\s/,"tabBodySelected ")
			li.className = li.className.replace(/^tabButton$|tabButton\s/,"tabButtonSelected ")
		} else {
			tabBody.className = tabBody.className.replace(/^tabBodySelected$|tabBodySelected\s/,"tabBody ")
			li.className = li.className.replace(/^tabButtonSelected$|tabButtonSelected\s/,"tabButton ")
		}
	}
	return false;
}

var isTabRegistered = false;

function showTargetElement()
{
	var hash = document.location.hash;
	if(hash.substr(0,1) != "#") return;
	hash = hash.substr(1);
	var elements = document.getElementsByName(hash);
	if(elements.length == 0) return;
	var element = elements[0];
	var tabIds = new Array();
	while(element != null && element.nodeType == 1) {
		var id = element.id;
		if(id != "" && id.substr(0,3) == "tab") {
			id = id.substr(3);
			var li = document.getElementById(id);
			if(li != null) tabClick(li.firstChild);
		}
		element = element.parentNode;
	}
}

function registerTab(id)
{
	if(!isTabRegistered) {
		addLoadAction(showTargetElement);
		isTabRegistered = true;
	}
}


function getTableSelectionOids(event)
{
	if (document.all != null) {
		var form =  window.event.srcElement;
	} else {
		if(event == null) return null;
		var form =  event.currentTarget;
	}
	while(form && form.nodeType == 1 && form.tagName != "FORM") {
		form = form.parentNode;
	}
	if(form == null || form.nodeType != 1) return null;
	var allCheckbox = form.topAllOids;
	if(allCheckbox != null && allCheckbox.checked) {
		return "all";
	}
	var checkboxes = form.oids;
	if(checkboxes == null) checkboxes = new Array();
	if(checkboxes.length == null) {
		checkboxes = new Array(checkboxes);
	}
	var oids = "";
	for(var i=0;i<checkboxes.length;i++) {
		var checkbox = checkboxes[i];
		if(!checkbox.checked) continue;
		if(oids != "") oids += " ";
		oids += checkbox.value;
	}
	return oids;
}

function odfSelectionTableLimitChange(select, id, anchor)
{
	var cmd = "limit:" + select.value;
	repostPage(id, cmd, anchor, null);
}

var allItlEnumerator = {
}

function ItlEnumerator(id, anchor)
{
	this._id = id;
	this._anchor = anchor;
	this._oids = "";
	allItlEnumerator[id] = this;
}

ItlEnumerator.prototype.setOids = function(oids)
{
	this._oids = oids;
}


ItlEnumerator.prototype.getOids = function(oids)
{
	return this._oids;
}

ItlEnumerator.prototype.incrementRank = function(rank)
{
	var oids = this._oids.split(" ");
	var oid = oids[rank+1];
	oids[rank+1] = oids[rank];
	oids[rank] = oid;
	var str = "";
	for(var i=0;i<oids.length;i++) {
		if(i) str += " ";
		str += oids[i];
	}
	repostPage(this._id, "modifyRank", this._anchor, str);
}


ItlEnumerator.prototype.decrementRank = function(rank)
{
	var oids = this._oids.split(" ");
	var oid = oids[rank];
	oids[rank] = oids[rank-1];
	oids[rank-1] = oid;
	var str = "";
	for(var i=0;i<oids.length;i++) {
		if(i) str += " ";
		str += oids[i];
	}
	repostPage(this._id, "modifyRank", this._anchor, str);
}

ItlEnumerator.prototype.reOrder = function(oids, anchor)
{
	repostPage(this._id, "modifyRank", anchor, oids);
}