// JavaScrfilInfopt

if (jQuery.browser.safari) {
	jQuery.fn.oldhide = jQuery.fn.hide;
	jQuery.fn.oldshow = jQuery.fn.show;
	jQuery.fn.hide = function() {
		this.removeClass('transition');
		this.removeClass('show');
		return this.addClass('hide');
		// return this.css('display','none');
	};
	jQuery.fn.show = function() {
		this.removeClass('transition');
		this.removeClass('hide');
		return this.addClass('show');
	};
	jQuery.fn.fadeIn = function(speed) {
		this.removeClass('hide');
		this.addClass('transition').addClass('show');
		return this;
	};
	jQuery.fn.fadeOut = function(speed) {
		this.removeClass('show');
		return this.addClass('transition').addClass('hide');
	};

}
is_selected = true;
var bgColor = '#1E549E';
var radiosNav = false;
var intervalMEA1 = null;
var fx = function() {
	jquerygetelement(this).css('display', 'block').animate( {
		vartoto : 1
	}, 4000, function() {
		jquerygetelement(this).css('display', 'none');
		news_idx++;
		if (news_idx > news.length - 1)
			news_idx = 0;
		jquerygetelement(news[news_idx]).queue(fx);
		jquerygetelement(news[news_idx]).dequeue();
	});
};
var news;
var news_idx;

jquerygetelement(document)
		.ready( function() {
			// Menu navRadios
				jquerygetelement('#navTop li.first>a').toggle( function() {
					// Toggle show
						if (!radiosNav) {
							jquerygetelement('#navRadios').css("display",
									"block");
							radiosNav = true;
						}
					}, function() {
						// Toggle show
						if (radiosNav) {
							jquerygetelement('#navRadios').css("display",
									"none");
							radiosNav = false;
						}
					})
				news = jquerygetelement('.news_item');
				news_idx = 0;
				jquerygetelement(news[0]).queue(fx).dequeue();
				/***************************************************************
				 * le menu de tete
				 **************************************************************/

				// mise en forme sous rubrique pour ie6
				if (typeof document.body.style.maxHeight === "undefined") {// si ie6
					jquerygetelement('ul.ssr li a').css( {
						display : 'inline-block',
						width : '100%'
					});
				}
				// on masque les sous-rubriques
				jquerygetelement('ul.ssr').hide();
				// on les reposition pour ne pas le faire � chaque fois
				jquerygetelement('ul.ssr').css( {
					left : '0',
					top : '38px',
					'padding-top' : '2px',
					'float' : 'left'
				});

				// hover sur la rubrique
				jquerygetelement('#navTete>li').hover( function() {
					// on affiche la sous-rubrique
						jquerygetelement('ul.ssr', this).show();
					}, function() {
						// on masque la sous-rubrique
						jquerygetelement('ul.ssr', this).hide();
					});

				/***************************************************************
				 * Menu Onglet (MEA Type 1 en haut � gauche)
				 **************************************************************/
				// On masque les annonces sauf la premiere
				jquerygetelement('#mea1 .annonceMea1').hide();
				jquerygetelement('#mea1 .annonceMea1:eq(0)').show();

				// D�clenchement de l'animation sans effet de fondu
				changeMEA1('', false);

				// Fonction au survol d'un onglet
				jquerygetelement('#mea1 li').hover( function() {

					// Id de l'onglet
						var idOnglet = jquerygetelement(this).attr("id");

						// On arr�te le d�filement
						if (intervalMEA1 != null)
							window.clearTimeout(intervalMEA1);

						showMEA1(idOnglet, false);
					}, function() {
						if (intervalMEA1 != null)
							window.clearTimeout(intervalMEA1);

						// Id de l'onglet
						var idOnglet = jquerygetelement(this).attr("id");

						// On reprend le d�filement
						intervalMEA1 = window.setTimeout("changeMEA1('"
								+ idOnglet + "', false)", 4000);
					});
				jquerygetelement('#mea1 .contenu').click( function() {

					//var link = jquerygetelement('a.lienMEA1:visible');
					//document.location = link[0].href;
				});

				/***************************************************************
				 * le menu dock
				 **************************************************************/

				// on masque les sous-rubriques
				jquerygetelement('ul.ssrDock').hide();

				// hover sur la rubrique
				jquerygetelement('#zoneDock>li').hover( function() {
					// on affiche la sous-rubrique
						jquerygetelement('ul.ssrDock', this).fadeIn("slow");
					}, function() {
						// on masque la sous-rubrique
						jquerygetelement('ul.ssrDock', this).fadeOut("slow");
					});

				// hover sur la rubrique
				jquerygetelement('#orchestres>li').hover( function() {
					// on affiche la sous-rubrique
						jquerygetelement('ul.ssrDock', this).fadeIn("slow");
					}, function() {
						// on masque la sous-rubrique
						jquerygetelement('ul.ssrDock', this).fadeOut("slow");
					});

				jquerygetelement('#mea2 li').each(
						function() {
							this.bgImage = jquerygetelement(".typeP", this)
									.css("background-image");
							this.bgColor = jquerygetelement(".typeP", this)
									.css("background-color");
							jquerygetelement(".typeP", this).css(
									"background-color", 'transparent');
						});
				jquerygetelement('#mea2 li').each(
						function() {
							jquerygetelement(this).bind(
									"mouseenter",
									function() {
										jquerygetelement('.typeP', this).css(
												"background-color",
												this.bgColor);
										jquerygetelement('.typeP', this).css(
												"background-image", "none");
									});
							jquerygetelement(this).bind(
									"mouseleave",
									function() {
										jquerygetelement('.typeP', this).css(
												"background-color",
												"transparent");
										jquerygetelement('.typeP', this).css(
												"background-image",
												this.bgImage);
									});
						});

				/***************************************************************
				 * selections
				 **************************************************************/

				// calcul de la taille des block onglets
				if (jQuery.browser.version.substr(0, 1) == '6') {
					var width = Math
							.floor(640 / jquerygetelement('#selections>ul>li').length);
					var reste = 640 % jquerygetelement('#selections>ul>li').length
				} else {
					var width = Math
							.floor((640 - 8 * jquerygetelement('#selections>ul>li').length)
									/ jquerygetelement('#selections>ul>li').length);
					var reste = (640 - 8 * jquerygetelement('#selections>ul>li').length)
							% jquerygetelement('#selections>ul>li').length
				}

				jquerygetelement('#selections>ul>li')
						.css("width", width + 'px');
				jquerygetelement('#selections>ul>li.first').css("width",
						width + reste + 'px');

				jquerygetelement('#selections div>ul li img')
						.hover( function() {
							//var zonelien = jquerygetelement('#selections div.ongletRadioContent>a');
								// zonelien[0].innerHTML=this.title;
								// zonelien[0].href=this.alt;

								// r�cupration du parent
								var divParent = this.parentNode.parentNode.parentNode.parentNode;
								var zonelien = jquerygetelement('#' + divParent.id + '>a');

								// attention test de longueur
								var tmp = this.parentNode.title.replace(/''/,"'").replace(/''/,"'");
								if (tmp.length > 70) {
									tmp = tmp.substring(0, 70);
									tmp = tmp
											.substring(0, tmp.lastIndexOf(' ')) + '...';
								}

								zonelien[0].innerHTML = tmp;
								zonelien[0].href = this.alt;

								/* alert(divParent.id); */

								// var zoneLien =
								// this.parentNode.parentNode.parentNode.parentNode;//jquerygetelement('#selections
								// div.ongletRadioContent');
								/*
								 * zoneLien.getElementsByTagName('a')[0].innerHTML=this.title;//zoneLien[0].getElementsByTagName('a')[0].innerHTML=this.title;
								 * //zoneLien.getElementsByTagName('a')[0].href=this.alt;//zoneLien[0].getElementsByTagName('a')[0].href=this.alt;
								 */
							}, function() {
							});
				jquerygetelement('#selectionsEditionsRadioFrance div>ul li img')
						.hover(
								function() {
									/*var zoneLien = this.parentNode.parentNode.parentNode.parentNode;//jquerygetelement('#selectionsEditionsRadioFrance div.ongletRadioContent');
									zoneLien.getElementsByTagName('a')[0].innerHTML=this.title;
									zoneLien.getElementsByTagName('a')[0].href=this.alt;*/
									var zonelien = jquerygetelement('#selectionsEditionsRadioFrance div>a');

									// attention test longueur
									var tmp = this.parentNode.title.replace(/''/,"'").replace(/''/,"'");
									if (tmp.length > 70) {
										tmp = tmp.substring(0, 70);
										tmp = tmp.substring(0, tmp
												.lastIndexOf(' ')) + '...';
									}

									zonelien[0].innerHTML = tmp;
									zonelien[0].href = this.alt;
								}, function() {
								});

				// conditionnel des fleches du scroll
				if (jquerygetelement('#lyr1 li').length <= 2) {
					jquerygetelement('#scrollLinks1 img')
							.css("display", 'none');
				}
				if (jquerygetelement('#lyr2 li').length <= 5) {
					jquerygetelement('#scrollLinks2 img')
							.css("display", 'none');
				}
				if (jquerygetelement('#lyr3 li').length <= 5) {
					jquerygetelement('#scrollLinks3 img')
							.css("display", 'none');
				}
				if (jquerygetelement('#lyr4 li').length <= 5) {
					jquerygetelement('#scrollLinks4 img')
							.css("display", 'none');
				}
				if (jquerygetelement('#lyr5 li').length <= 4) {
					jquerygetelement('#scrollLinks5 img')
							.css("display", 'none');
				}
				if (jquerygetelement('#lyr6 li').length <= 4) {
					jquerygetelement('#scrollLinks6 img')
							.css("display", 'none');
				}
				if (jquerygetelement('#lyr7 li').length <= 4) {
					jquerygetelement('#scrollLinks7 img')
							.css("display", 'none');
				}
				if (jquerygetelement('#lyr8 li').length <= 3) {
					jquerygetelement('#scrollLinks8 img')
							.css("display", 'none');
				}
				if (jquerygetelement('#lyr9 li').length <= 3) {
					jquerygetelement('#scrollLinks9 img')
							.css("display", 'none');
				}
				if (jquerygetelement('#lyr10 li').length <= 2) {
					jquerygetelement('#scrollLinks10 img').css("display",
							'none');
				}
				if (jquerygetelement('#lyr11 li').length <= 2) {
					jquerygetelement('#scrollLinks11 img').css("display",
							'none');
				}
				if (jquerygetelement('#lyr12 li').length <= 2) {
					jquerygetelement('#scrollLinks12 img').css("display",
							'none');
				}
				if (jquerygetelement('#lyr13 li').length <= 2) {
					jquerygetelement('#scrollLinks13 img').css("display",
							'none');
				}
				actus=jquerygetelement('#agenda ul li.first');
				actus_items=jquerygetelement('#wn1 ul li',actus);
				
				if ((actus_items.length==1) && (actus_items[0].childNodes.length<3)) {
					actus.css('display','none');
				}
			});

/**
 * Affiche la MEA de Type 1 suivante
 * 
 * @param string
 *            Id onglet ou vide pour afficher le premier onglet par d�faut
 * @param boolean
 *            Activer/Desactiver le fondu pour passer � l'animation suivante
 */
function changeMEA1(idOnglet, withEffect) {

	// Nombre de MEA total
	var nbMEA = jquerygetelement('#mea1 .menuOnglet li').length;

	if (nbMEA > 0) {

		// Par d�faut, affichage du premier onglet
		if (idOnglet == '') {
			idOnglet = jquerygetelement('#mea1 .menuOnglet li').get(0).id;
			// Par de fondu pour le premier onglet affich� au d�marrage
			withEffect = false;
		}

		//*** Calcul de la prochaine MEA � afficher ***
		// Num�ro de la MEA en cours
		var currentIndex = jquerygetelement('#mea1 li').index(
				jquerygetelement('#mea1 #' + idOnglet));

		if (currentIndex >= (nbMEA - 1))
			var nextIndex = 0;
		else
			var nextIndex = currentIndex + 1;

		// R�cup�ration de l'onglet suivant ayant cet index
		var nextIdOnglet = jquerygetelement('#mea1 li').get(nextIndex).id;

		// Affichage de la MEA
		showMEA1(idOnglet, withEffect);

		if (intervalMEA1 != null)
			window.clearTimeout(intervalMEA1);
		intervalMEA1 = window.setTimeout("changeMEA1('" + nextIdOnglet
				+ "', true)", 4000);
	}
}

/**
 *	Affiche une MEA de Type 1
 *
 *	@param string Id de l'onglet
 *	@param boolean Activer/Desactiver le fondu pour passer � l'animation suivante
 */
function showMEA1(idOnglet, withEffect) {

	// Suppression de la classe actif dans les liens
	jquerygetelement('#mea1 li').removeClass('actif');

	// Ajout de la classe actif sur le lien en cours
	jquerygetelement('#mea1 #' + idOnglet).addClass('actif');

	// On masque les annonces

	// On masque les images
	jquerygetelement('#mea1 .annonceMea1').hide();
	jquerygetelement('#mea1 .lienMEA1').hide();
	jquerygetelement('#mea1 .imageMEA1').hide();

	// On affiche l'annonce et l'image correspondant � l'onglet
	if (withEffect) {
		//	jquerygetelement('#mea1 .' + idOnglet).css('display','block');
		jquerygetelement('#mea1 .annonceMea1.' + idOnglet).fadeIn("slow");
		jquerygetelement('#mea1 .lienMEA1.' + idOnglet).fadeIn("slow");
		jquerygetelement('#mea1 .imageMEA1.' + idOnglet).fadeIn("slow");

	} else {
		jquerygetelement('#mea1 .' + idOnglet).show();

	}
}

function showPopup(id) {
	jquerygetelement(id).fadeOut();
}

function hideOnglets() {
	// cache tous les div � l'interieur du div "selections"
	jquerygetelement('#selections div').hide();
}
function showOnglet(id) {
	jquerygetelement('#' + id).show();
}
