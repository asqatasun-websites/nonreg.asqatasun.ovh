var posPointRougeVideo = 1;
var NB_IMAGE_PAGE_VIDEO = 3;
$(document).ready(function(){
	
	function stpsl(str) {return (str+'').replace(/\\(.?)/g, function (s, n1) {switch (n1) { case '\\': return '\\'; case '0': return '\0'; case '': return ''; default: return n1;}});}
	
	function bougePosVideo()
	{
		$('#video_nave div[id^=page]').removeClass('video_pt_rouge').addClass('video_pt_gris');
		$('#video_nave div:eq('+(posPointRougeVideo+1)+')').addClass('video_pt_rouge');
		$('#videos_refresh').animate({'marginLeft':'-'+(posPointRougeVideo-1)*(parseInt($('#bloc_3_videos').css('width'))+15)+'px'});
	}
	function chargeVignette(nb)
	{
		var start = $('#videos_refresh div').length;
		$.each(lstVideos,function(i,s){
			if (i%NB_IMAGE_PAGE_VIDEO == 0 && i > start+nb && i!=0) {
				return false;
			}
			
		var brow_vervid = "N";
		if( $.browser.safari)
		var brow_vervid = "Safari_";
		if( $.browser.opera)
		var brow_vervid = "Opera_";
		if( $.browser.msie)
		var brow_vervid = "IE_";
		if( $.browser.mozilla)
		var brow_vervid = "mozilla_";
			 
	 	
	if (( i >= start )&& (brow_ver != "N" )){
				$('#videos_refresh').append('<div id="video_'+i+'" class="cap_video_1"><a href="http://video.lequipe.fr/video/'+s.ID+'.html" target="_blank" title="'+s.TITRE+'"><img src="'+stpsl(s.VIGNETTE)+'" width="80" height="60"/>'+s.TITRE+'</a></div>');
			}
		});
	}
	$('.video_pt_gris').live('click',function(){
		posPointRougeVideo = parseInt($(this).attr('id').replace(/page_/,''));
		if ( posPointRougeVideo > $('#videos_refresh div').length/NB_IMAGE_PAGE_VIDEO ) {
			chargeVignette(posPointRougeVideo*NB_IMAGE_PAGE_VIDEO-$('#videos_refresh div').length);
		}
		bougePosVideo();
	});
	$('#video_retour').click(function(){
		if (posPointRougeVideo > 1 ){
			posPointRougeVideo--;
			bougePosVideo();
		}
	});
	$('#video_suite').click(function(){
		chargeVignette(NB_IMAGE_PAGE_VIDEO);
		if (posPointRougeVideo < 5 ){
			posPointRougeVideo++;
			bougePosVideo();
		}
	});
});