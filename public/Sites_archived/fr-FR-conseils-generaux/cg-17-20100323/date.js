// JavaScript Document
function getCalendarDate()
{
   var months = new Array(13);
   months[0]  = "janvier";
   months[1]  = "f�vrier";
   months[2]  = "mars";
   months[3]  = "avril";
   months[4]  = "mai";
   months[5]  = "juin";
   months[6]  = "juillet";
   months[7]  = "ao�t";
   months[8]  = "septembre";
   months[9]  = "octobre";
   months[10] = "novembre";
   months[11] = "d�cembre";
   var days = new Array(7);
   days[0] = "Dimanche";
   days[1] = "Lundi";
   days[2] = "Mardi";
   days[3] = "Mercredi";
   days[4] = "Jeudi";
   days[5] = "Vendredi";
   days[6] = "Samedi";
   var now = new Date();
   var weekday = now.getDay();
   var weekdayname = days[weekday];
   var monthnumber = now.getMonth();
   var monthname   = months[monthnumber];
   var monthday    = now.getDate();
   var year        = now.getYear();
   if(year < 2000) { year = year + 1900; }
   var dateString = weekdayname +
   					' ' +
   					monthday + 
   					' ' +
					monthname + 
                    ' ' + 
                    year;
   return dateString;
} // function getCalendarDate()

function getClockTime()
{
   var now    = new Date();
   var hour   = now.getHours();
   var minute = now.getMinutes();
   if (hour   < 10) { hour   = "0" + hour;   }
   if (minute < 10) { minute = "0" + minute; }
   var timeString = hour + 
                    ':' + 
                    minute;
   return timeString;
} // function getClockTime()

