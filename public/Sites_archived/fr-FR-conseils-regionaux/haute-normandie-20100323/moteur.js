function viderSelect(idobj,defaultOption) 
{
	  // on vide les options
	  var objselect=document.getElementById(idobj);
	  
	  for ( i= objselect.options.length-1; i>=0; i-- ) 
	  {
		objselect.options[i] = null;
	  }
	  var opt = new Option(defaultOption,""); 
	  objselect.options[objselect.length] = opt;
}
	
function getfilieres(filiere_id)
{
//		if( filiere_id >0) 
//			var vfiliere = filiere_id;
//		else
			var vfiliere= $("#category").val();
		  viderSelect('filiere','');
				
			$.ajax({type: "POST",
	             url: "/recherche/getfilieres/" + vfiliere,
	             dataType: 'xml',
	             error: function()
	             {
//	             	alert('Le chargement a Ã©chouÃ© (1) !');
									$('#filiere_loading').hide();
	           	},
	           success: function(xml)
	           {
	           	
	           		var scadre = document.getElementById('filiere');
	           		var opt = new Option('Sélectionner une filière','');
	               		scadre.options[0] = opt;
	             	           		
	             	var cadres=xml.getElementsByTagName('filiere');	                            
	             	for(i=0;i<cadres.length;i++) 
	             	{
	               		var nom = cadres[i].getElementsByTagName('nom').item(0).childNodes[0].nodeValue;
	               		var idss = cadres[i].getElementsByTagName('id').item(0).childNodes[0].nodeValue;
	               		var opt = new Option(nom,idss);
	               		scadre.options[scadre.length] = opt;
//	               		if( idss == {/literal}'{$selected_cadre}'{literal} )
//		                 	scadre.selectedIndex= scadre.length-1;
	             	}
	             	$('#filiere_loading').hide();
	             	
	           }
	         });
//	      }	
}
	
function getcadre(filiere_id)
{
//		if( filiere_id >0) 
//			var vfiliere = filiere_id;
//		else
			var category = $("#category").val();
			viderSelect('cadre','');
			
			var filiere = $("#filiere").val();
				
			$.ajax({type: "POST",
	             url: "/recherche/getcadre/"+ category +"/"+ filiere,
	             dataType: 'xml',
	             error: function()
	             {
//	             	alert('Le chargement a Ã©chouÃ© (1) !');
									$('#cadre_loading').hide();
	           	},
	           success: function(xml)
	           {
	           	
	           		var scadre = document.getElementById('cadre');
	           		var opt = new Option('Sélectionner un cadre','');
	               		scadre.options[0] = opt;
	             	           		
	             	var cadres=xml.getElementsByTagName('cadre');	                            
	             	for(i=0;i<cadres.length;i++) 
	             	{
	               		var nom = cadres[i].getElementsByTagName('nom').item(0).childNodes[0].nodeValue;
	               		var idss = cadres[i].getElementsByTagName('id').item(0).childNodes[0].nodeValue;
	               		var opt = new Option(nom,idss);
	               		scadre.options[scadre.length] = opt;
//	               		if( idss == {/literal}'{$selected_cadre}'{literal} )
//		                 	scadre.selectedIndex= scadre.length-1;
	             	}
	             	$('#cadre_loading').hide();
	           }
	         });
//	      }	
}
	
function getgrade(filiere_id)
{
//		if( filiere_id >0) 
//			var vfiliere = filiere_id;
//		else
			var category= $("#category").val();
				viderSelect('grade','');
				
			var filiere= $("#filiere").val();
			var cadre= $("#cadre").val();
			
			$.ajax({type: "POST",
	             url: "/recherche/getgrade/"+ category +"/"+ filiere +"/"+ cadre,
	             dataType: 'xml',
	             error: function()
	             {
//	             	alert('Le chargement a Ã©chouÃ© (1) !');
								$('#grade_loading').hide();
	           	},
	           success: function(xml)
	           {
	           	
	           		var scadre = document.getElementById('grade');
	           		var opt = new Option('Sélectionner un grade','');
	               		scadre.options[0] = opt;
	             	           		
	             	var cadres=xml.getElementsByTagName('grade');	                            
	             	for(i=0;i<cadres.length;i++) 
	             	{
	               		var nom = cadres[i].getElementsByTagName('nom').item(0).childNodes[0].nodeValue;
	               		var idss = cadres[i].getElementsByTagName('id').item(0).childNodes[0].nodeValue;
	               		var opt = new Option(nom,idss);
	               		scadre.options[scadre.length] = opt;
//	               		if( idss == {/literal}'{$selected_cadre}'{literal} )
//		                 	scadre.selectedIndex= scadre.length-1;
	             	}
	             	$('#grade_loading').hide();
	           }
	         });
//	      }	
}

	$(document).ready(function() 
	{
		$('#grade_loading').hide();
		$('#cadre_loading').hide();
		$('#filiere_loading').hide();
		
		$("#category").change(function () 
		{ 
			$('#grade_loading').show();
			$('#cadre_loading').show();
			$('#filiere_loading').show();
			getfilieres('');		
			getcadre('');		
			getgrade('');		
		});
		
		$("#filiere").change(function () 
		{ 
			$('#grade_loading').show();
			$('#cadre_loading').show();
			getcadre('');		
			getgrade('');		
		});
	
		$("#cadre").change(function () 
		{ 
			$('#grade_loading').show();
			getgrade('');		
		});
	});		