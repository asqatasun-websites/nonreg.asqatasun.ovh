function rollover_img(nom,src) {	
	document.images[nom].src=src;
}

function switchview(n) {
	if (document.getElementById(n).style.display == 'none') 
	{
		document.getElementById(n).style.display = 'block';
	} 
	else
	{
		document.getElementById(n).style.display = 'none';
	}
}

function ouvrirgp(numero) { 
	window.open('site_guide/pop-guide.asp?id='+numero, 'fichegp'+numero, 'toolbar=0, location=0, directories=0, status=0, scrollbars=1, resizable=1, menuBar=0, width=500, height=350'); 
} 


function ouvriragenda(numero) { 
	window.open('site_agenda/pop-agenda.asp?id='+numero, 'agenda'+numero, 'toolbar=0, location=0, directories=0, status=0, scrollbars=1, resizable=1, menuBar=0, width=500, height=350'); 
}

function ouvrirbreve(numero) {
	window.open('site_actus/breve.asp?id='+numero, 'Breves', 'toolbar=0, location=0, directories=0, status=0, scrollbars=1, resizable=1, menuBar=0, width=420, height=450, left=20, top=20'); 
}

function ouvrirvideo(laquelle,largeur,hauteur) {
	window.open('site_video/popup_video.asp?video='+laquelle, 'Video', 'toolbar=0, location=0, directories=0, status=0, scrollbars=1, resizable=1, menuBar=0, width='+largeur+', height='+hauteur+', left=20, top=20'); 
}

function ouvrirpopup(site,largeur,hauteur) {
	window.open(site, 'pop', 'toolbar=0, location=0, directories=0, status=0, scrollbars=1, resizable=1, menuBar=0, width='+largeur+', height='+hauteur+', left=0, top=0');
}

function getHauteurMax() {
	var hauteurmax = 0;
	var hauteurGauche = document.getElementById('mgauche').offsetHeight;
	var hauteurCentre = document.getElementById('centre').offsetHeight;
	var hauteurDroite = document.getElementById('mdroite').offsetHeight;
	hauteurmax = Math.max(hauteurGauche, hauteurCentre);
	hauteurmax = Math.max(hauteurmax, hauteurDroite);
	return hauteurmax;
}

function positionnerBas() {
	document.getElementById('bas').style.top = getHauteurMax()+90+'px';
}

function tailleFontes(grosseur,adresse) {
	switch (grosseur) {
		case 'normal':
			var feuillestyle = 'taillenormale.css';
			var taille = '0.8em';
			break;
		case 'grande':
			var feuillestyle = 'taillegrande.css';
			var taille = '1em';
			break;
	}
	
	if (document.all) {
		// pour une fois que IE me simplifie la vie ! :)
		document.createStyleSheet(feuillestyle);
		// ecrire le cookie
		Set_Cookie('feuillestyle', feuillestyle);
		// puis refoutre le bas � sa place :
		// positionnerBas();		
	}
	else {
		window.location.href=('changertaille.asp?taille='+grosseur);
		return false;
	}

}

function Set_Cookie(name,value,expires,path,domain,secure) {
    document.cookie = name + "=" +escape(value) +
        ( (expires) ? ";expires=" + expires.toGMTString() : "") +
        ( (path) ? ";path=" + path : "") + 
        ( (domain) ? ";domain=" + domain : "") +
        ( (secure) ? ";secure" : "");
}