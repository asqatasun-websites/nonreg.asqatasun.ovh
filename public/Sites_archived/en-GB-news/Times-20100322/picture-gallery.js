
// -----------------------
// FUNCTION: pictureGalleryPopup
// DESCRIPTION: A function that opens a pop up window.
// ARGUMENTS: Publication URL, article ID and section Name
// RETURN: True
// -----------------------
function pictureGalleryPopupPic(pubUrl) {
	var newWin = window.open(pubUrl,'mywindow','menubar=0,resizable=false,width=1000,height=711,left=0,top=0');
}
