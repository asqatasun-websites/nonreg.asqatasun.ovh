if (jQuery==undefined) {
	jQuery = {};
}
if (jQuery.cookie==undefined) {
	jQuery.cookie = function(name, value, options) {
	    if (typeof value != 'undefined') { // name and value given, set cookie
	        options = options || {};
	        if (value === null) {
	            value = '';
	            options.expires = -1;
	        }
	        var expires = '';
	        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
	            var date;
	            if (typeof options.expires == 'number') {
	                date = new Date();
	                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
	            } else {
	                date = options.expires;
	            }
	            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
	        }
	        var path = options.path ? '; path=' + (options.path) : '';
	        var domain = options.domain ? '; domain=' + (options.domain) : '';
	        var secure = options.secure ? '; secure' : '';
	        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
	    } else { // only name given, get cookie
	        var cookieValue = null;
	        if (document.cookie && document.cookie != '') {
	            var cookies = document.cookie.split(';');
	            for (var i = 0; i < cookies.length; i++) {
	                var cookie = jQuery.trim(cookies[i]);
	                // Does this cookie string begin with the name we want?
	                if (cookie.substring(0, name.length + 1) == (name + '=')) {
	                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	                    break;
	                }
	            }
	        }
	        return cookieValue;
	    }
	};
}

jQuery(document).ready(function(){
	// Listener for Breaking News Alerts Sign-up
	BNAEmailAlerts.listener();
});

// Breaking News Alerts - FOR FN. Updated: 11/12/2009
// Dependencies: jQuery, jQuery.cookie plugin
var BNAEmailAlerts = {
	id: 'newsAlertsEmailSignup', 
	reqURL: '/portal/newsalertsubscribe', 
	cookie: { set:true,name:'breakingNewsAlertSubmit',expires:3 },
	formObj: {},
	SLID: { 
		NewsBrief:'6700EAF45F07D38E6C4BE5C9AA7C4F7F',
		BreakingNews:'6700EAF45F07D38EABE36F2E8EE267CE' 
	},
	formTxt: {
		email:'Invalid email address.',
		chkBox:'Choose at least one news alert.',
		submitted:'You have already submitted.',
		error:'Request Error. Please try again.',
		defaultEmail:'Enter Email Address:'
	},
	validate: function() {
		this.checkForm();
	},
	retry: function() {
		var form = this.formObj['obj'];
		if (this.cookie['set'] && jQuery.cookie(this.cookie['name'])) {
			jQuery("fieldset input[name='email']",form).attr("value",this.formTxt['defaultEmail']);
			jQuery("input[type='checkbox']",form).attr("checked","checked");
		}
		this.setCookie(0);
		this.setForm();
	},
	setForm: function() {
		var form; 
		var thisObj = this;
		var fObj = this.formObj;
		jQuery("#"+thisObj.id).each(function(){ 
			thisObj.formObj['obj'] = this;
			form = this;
		});
		// reset form items
		jQuery(".t-y,.e-r",form).css({ display:"none",paddingTop:"0" });
		jQuery("fieldset",form).fadeIn(200);
		jQuery("div.submitting",form).css("display","none");
		jQuery("input",form).attr("disabled","");
		this.formObj['emailOK']=false;
		this.formObj['chkBoxOK']=false;
		if (this.cookie['set'] && jQuery.cookie(this.cookie['name']) && !fObj['submitted']) { // Cookie check
			jQuery(".e-r",form).css({ display:"block",paddingTop:"35px" });
			jQuery(".e-r h2",form).html(this.formTxt['submitted']);
			jQuery("fieldset",form).css("display","none");
			jQuery("input[type='checkbox']",form).attr("disabled","disabled");
		}
		if (fObj['submitted']) { // Submit check
			jQuery(".e-r",form).css("display","none");
			jQuery("fieldset",form).css("display","none");
			jQuery(".t-y",form).fadeIn(200).css("paddingTop","35px");
			jQuery("input[type='checkbox']",form).attr("disabled","disabled");
		}
	},
	checkForm: function() {
		var thisObj = this;
		jQuery("#"+thisObj.id).each(function(){
			thisObj.formObj['obj'] = this;
			thisObj.setForm();
			thisObj.formObj['submitted']=false; // Reset
			// Check email
			var email = jQuery("fieldset input[name='email']",this).attr("value");
			var emailOK = thisObj.checkEmail(email);
			thisObj.formObj['emailOK'] = emailOK;
			if (emailOK) { 
				thisObj.formObj['email']=email;
				thisObj.checkChkBox();
			} else { thisObj.errorForm('email'); }
			if (thisObj.formObj['emailOK']&&thisObj.formObj['chkBoxOK']) { thisObj.submitForm(); }
		});
	},
	checkEmail: function(email) { // Email check
		var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
		return (pattern.test(email))?true:false;
	},
	checkChkBox: function() { // Check checkboxes
		var thisObj = this;
		var form = thisObj.formObj['obj'];
		var chkboxOk=false;
		var ckArray = [];
		jQuery("input[type='checkbox']",form).each(function(){
			if (jQuery(this).attr("checked")) {
				var name = jQuery(this).attr("name");
				ckArray.push(name); chkBoxOk = true;
			}
		});
		if (ckArray.length<1) { thisObj.errorForm('chkBox'); } 
		else {
			var SLIDStr = "";
			jQuery(ckArray).each(function(i){
				SLIDStr += ((i>0)?",":"")+thisObj.SLID[ckArray[i]];
			})
			thisObj.formObj['SLID'] = SLIDStr;
			thisObj.formObj['chkBoxOK'] = true;
		}
	},
	listener: function() { // This is called on page load, checking the cookie if enabled
		var thisObj = this;
		jQuery("#"+thisObj.id).each(function(){
			jQuery("input",this).keypress(function(e){ if (e.which==13) { thisObj.validate(); } }); // Enter - submit
			jQuery("fieldset input[name='add']",this).unbind("click").click(function(){ thisObj.validate(); });
			jQuery("fieldset input[name='submit']",this).unbind("click").click(function(){ thisObj.validate(); });
			jQuery("p[class='try-again'] a",this).unbind("click").click(function(){ thisObj.retry(); });
			jQuery(".e-r a",this).click(function(){ thisObj.retry(); });
			jQuery("fieldset input[name='email']",this)
				.blur(function(){ var val=jQuery(this).attr("value"); jQuery(this).attr("value",((val=='')?'Enter Email Address:':val)) })
				.focus(function(){ var val=jQuery(this).attr("value"); jQuery(this).attr("value",((val=='Enter Email Address:')?'':val)) });
		});
		if (this.cookie['set']) {
			this.setForm();
		}
	},
	clearDefault: function(el) {
		if (el.value == "Enter Email Address:") { el.value = ""; }
	},
	setDefault: function(el) {
		if (el.value == "") { el.value = "Enter Email Address:"; }
	},
	errorForm: function(err) {
		var thisObj = this;
		var form = thisObj.formObj['obj'];
		this.cErr = (this.cErr==undefined)?err:this.cErr;
		jQuery("input",form).attr("disabled","disabled"); // Disable inputs. Let user retry
		jQuery(".e-r",form).each(function(){
			if (thisObj.cErr==err) { jQuery(this).css("display","block"); } else { jQuery(this).fadeIn(100); thisObj.cErr=err; }
			jQuery("h2:first",this).html(thisObj.formTxt[err]);
		});
	},
	submitForm: function() {
		var thisObj = this;
		var info = thisObj.formObj;
		var form = info['obj'];
		var str = 'slids='+info['SLID']+'&email='+info['email']; // Form query string
		this.qURL=str;
		jQuery("div.submitting",form).fadeIn(100);
		thisObj.qaSubmit(thisObj.reqURL+'?'+str);
		jQuery.ajax({
		  	url: thisObj.reqURL,
			data: str,
		  	success: function(r){
				jQuery("div.submitting",form).css("display","none");
				var obj = thisObj.xmlObj(r);
				var isErr = false;
				
				for (i in obj) {
					if (obj[i].indexOf('ok')<0) { isErr = true; break; }
				}
				if (!isErr) {
					thisObj.formObj['submitted']=true;
					thisObj.setCookie(1);
					thisObj.setForm();
				} else { thisObj.errorForm('error'); }
		  	},
			error: function() { jQuery("div.submitting",form).css("display","none"); thisObj.errorForm('error'); }
		});
	},
	setCookie: function(s) {
		if (this.cookie['set']) {
			var cName = this.cookie['name'];
			if (s==1) { jQuery.cookie(cName,true,{ expires:this.cookie['expires'] }); } else { jQuery.cookie(cName,null); }
		}
	},
	xmlObj: function(xml) {
		var obj = {};
		jQuery(xml).find('list').each(function(){ obj[jQuery(this).attr("slid")]=jQuery.trim(jQuery(this).text()); });
		return obj;
	},
	qaSubmit: function(str) {
		var q = (window.location.search).substr(1);
		var qVars = q.split("&");
		var isQA = false;
		for (x=0;x<qVars.length;x++) {
			var s = qVars[x].split("=");
			if (s[0]=='test' && s[1]=='y') { isQA = true;}
		}
		if (isQA) { alert(str); }
	}
};

