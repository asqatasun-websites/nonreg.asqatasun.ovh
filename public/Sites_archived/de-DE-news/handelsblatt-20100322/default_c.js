

if(!path2JsFolder) {
	var path2JsFolder = '' 	
}


var jsList = new Array(); 
if (window.location.host != "localhost")
{
	if (window.location.hostname.toLowerCase().indexOf('handelsblatt.com') != -1)
	{
		jsList[0] = window.location.protocol + '//' + window.location.hostname + '/' + 'js/jquery_carousel_c.js';
		jsList[1] = window.location.protocol + '//' + window.location.hostname + '/' + 'js/easing_extension.js';
		jsList[2] = window.location.protocol + '//' + window.location.hostname + '/' + 'js/jquery.dimensions.js';
		jsList[3] = window.location.protocol + '//' + window.location.hostname + '/' + 'js/ui.mouse.js';
		jsList[4] = window.location.protocol + '//' + window.location.hostname + '/' + 'js/ui.draggable.js';
		jsList[5] = window.location.protocol + '//' + window.location.hostname + '/' + 'js/ui.draggable.ext.js';
		jsList[5] = window.location.protocol + '//' + window.location.hostname + '/' + 'js/jquery.accordion.js';
	}
	else
	{
		jsList[0] = 'http://www.handelsblatt.com/js/jquery_carousel_c.js';
		jsList[1] = 'http://www.handelsblatt.com/js/easing_extension.js';
		jsList[2] = 'http://www.handelsblatt.com/js/jquery.dimensions.js';
		jsList[3] = 'http://www.handelsblatt.com/js/ui.mouse.js';
		jsList[4] = 'http://www.handelsblatt.com/js/ui.draggable.js';
		jsList[5] = 'http://www.handelsblatt.com/js/ui.draggable.ext.js';
		jsList[5] = 'http://www.handelsblatt.com/js/jquery.accordion.js';
	}
}
else
{
	jsList[0] = window.location.protocol + '//' + window.location.hostname + '/news3/' + 'js/jquery_carousel_c.js';
	jsList[1] = window.location.protocol + '//' + window.location.hostname + '/news3/' + 'js/easing_extension.js';
	jsList[2] = window.location.protocol + '//' + window.location.hostname + '/news3/' + 'js/jquery.dimensions.js';
	jsList[3] = window.location.protocol + '//' + window.location.hostname + '/news3/' + 'js/ui.mouse.js';
	jsList[4] = window.location.protocol + '//' + window.location.hostname + '/news3/' + 'js/ui.draggable.js';
	jsList[5] = window.location.protocol + '//' + window.location.hostname + '/news3/' + 'js/ui.draggable.ext.js';
	jsList[5] = window.location.protocol + '//' + window.location.hostname + '/news3/' + 'js/jquery.accordion.js';
}

var cssList = new Array(); 
cssList[0] = new Array('css/opera.css', 'text/x-opera-css;charset=utf-8', window.opera);

function setScriptTag(src) {	
	var scriptTag = document.createElement("script");
	obj = document.getElementsByTagName("head")[0].appendChild(scriptTag);
	obj.setAttribute("type", "text/javascript");
	obj.setAttribute("src", path2JsFolder + src);	
}

for(var i = 0; i < jsList.length; i++) {
	//setScriptTag(jsList[i]);
	document.write('\<script type="text/javascript" src="' + path2JsFolder + jsList[i] + '"\>\<\/script\>');
}		

for(var i = 0; i < cssList.length; i++) {
	if(cssList[i][2]) {
		document.write('\<link rel="stylesheet" type="' + cssList[i][1]  + '" href="' + path2JsFolder + cssList[i][0] + '" />');
	}
	//document.write('\<link rel="stylesheet" type="text/x-opera-css;charset=utf-8" href="' + path2JsFolder + cssList[i] + '" />');
}	


$(document).ready(function(){
	try {
		document.execCommand("BackgroundImageCache", false, true);
	} catch(err) {}
	
	if(navigator.appVersion.toLowerCase().indexOf('mac')!=-1 || navigator.appVersion.toLowerCase().indexOf('khtml')!=-1) {
		$('div.topicNav').find('ul.topic').find('li').css('padding-right', '4px');
	}
	
	if(navigator.userAgent.toLowerCase().indexOf('khtml')!=-1) {
		if($('div.cnSearchResults')) {
			$('div.cnSearchResults ul.cards li a').css('padding', '2px 8px 0');
			$('div.cnSearchResults ul.cards li.active a').css('padding-left', '5px');
		}
	}
	selectLayer.action();	
	searchItemsList.init();	
	
});

$(window).load(function(){
    //$(".cnLeftCol > div:last").css('padding-bottom', '0');
	coordinateCols();
});

var selectLayer = {
	action: function () {
		$('.selectLayer01').find('a.select').bind("click", function(event) {
			$('.selectLayer01').find('ul').bind("mouseleave", function(event) { 												   
				if(event.target.tagName == 'UL') {
					$(event.target).parent().css('visibility', 'hidden');
				}	
			});
			obj = $(this).parent().find('.list');
			if(obj.css('visibility') == 'visible') {
				obj.css('visibility', 'hidden');
			}
			else {
				$('.selectLayer01').find('.list').css('visibility', 'hidden');
				obj.css('visibility', 'visible');
			}
			return false;
		});	
	}
};


/**********************************************************
Ist ein trasparenter Layer, der sich �ber die Seite legt.
**********************************************************/
var transparentLayer = {

	pageWidth: false,
	pageheight: false,
	windowWidth: false,
	windowHeight: false,
	init: function () {
		scrollWidth = document.getElementsByTagName('body')[0].offsetWidth;
		scrollHeight = document.getElementsByTagName('body')[0].offsetHeight;
		documentWidth = document.getElementsByTagName('body')[0].scrollLeft;
		documentHeight = document.getElementsByTagName('body')[0].scrollTop;
		if(window.innerWidth) {
			this.windowWidth =  window.innerWidth;
			this.windowHeight = window.innerHeight;
		}
		else {
			this.windowWidth =  document.body.clientWidth;
			this.windowHeight = document.body.clientHeight;
		}

		if(document.documentElement.scrollHeight) {
			this.pageHeight = document.documentElement.scrollHeight;
		}
		else if(document.body.scrollHeight) {
			this.pageHeight = document.body.scrollHeight;
		}

		this.pageHeight = $(document).height();
		//this.pageWidth = '100%'; //document.getElementsByTagName('body')[0].offsetWidth - sL;
		//this.pageHeight = document.getElementsByTagName('body')[0].offsetHeight - sT;
	},



	open: function () {
		if(document.getElementById('transparentLayer')) {
			this.init();
			document.getElementById('transparentLayer').style.width = '100%'; //this.pageWidth + 'px';
			document.getElementById('transparentLayer').style.height = this.pageHeight + 'px';
			document.getElementById('transparentLayer').style.zIndex = '19999';
			document.getElementById('transparentLayer').style.visibility = 'visible';
		}
	},

	close: function () {
		if(document.getElementById('transparentLayer')) {
			document.getElementById('transparentLayer').style.visibility = 'hidden';
		}
	},

	debug: function () {
		alert(this.pageWidth + " x " + this.pageHeight);
	}
};


//----------------------------------------------------------------
// Drag n Drop Popup Layer
//----------------------------------------------------------------

var popupLayer = {

	dragAndDrop: function(obj, urlString) {
		//$(obj).position()
		dragPopup.close();
		dragPopup.create(urlString, '1');
		return false;
	},


	resizeImage: function(obj, urlString) {
		dragPopup.close();
		$('body').append('<div id="transparentLayer">&nbsp;</div>');
		transparentLayer.open();
		dragPopup.create(urlString, '0');
		return false;
	}

};


/*
	This Function is private.
*/
var dragPopup = {

	create: function(urlString, dragndrop) {

		var dummyContent = '<div id="popupLayer">'+
		'<div id="PopupMain">'+
			'<div id="PopupHeader">'+
				'<a href="#" onclick="dragPopup.close();return false;" title="Schlie&szlig;en">'+
					'<img id="PopupCloseIcon" src="images/icon/ico_grey_closeDragArea.gif" border="0" width="9" height="9" alt="Schlie&szlig;en" />'+
				'</a>';
		if(dragndrop == 1)
			dummyContent+= '<img id="PopupMoveIcon" src="images/icon/ico_grey_moveDragArea.gif" width="9" height="9" alt="" />';

		dummyContent+= '</div>'+
			'<div id="PopupContent">'+
			'</div>'+
		'</div>'+
		'<div id="PopupTopRight"></div>'+
		'<div id="PopupRight"></div>'+
		'<div id="PopupFooter">'+
			'<div id="PopupBottomLeft"></div>'+
			'<div id="PopupBottom"></div>'+
			'<div id="PopupBottomRight"></div>'+
		'</div>'+
	'</div>	';

	$('body').append(dummyContent);




	if(urlString) {
		$.ajax({
		  url: urlString,
		  cache: false,
		  success: function(html){
				$('div#PopupContent').append(html);
				dragPopup.fixSize(dragndrop); //Hoehen und Breiten anpassen
				dragPopup.open(); //Popup oeffnen
		  }
		});
	}


	//Soll Schattenbox DragnDrop f�hig sein?
	if(dragndrop == 1)
		this.init(); //Drag n Drop initialisieren

	return false;
	},


	fixSize: function(dragndrop) {
		var contentBreite = $('body').find('div#PopupContent').width();

		//Breite fuer die Bildunterschrift generieren - nur bei Image Vergr��erung
		if(dragndrop == 0)
			$('div.cnImageZoomResize').width(contentBreite-28); //28 = paddingright+padding-left;

		var contentHoehe = $('body').find('div#PopupContent').height();
		var headerHoehe = $('body').find('div#PopupHeader').height();

		$('body').find('div#PopupContent').height(contentHoehe-1);
		$('body').find('div#PopupContent').width(contentBreite-2);

		$('body').find('div#PopupMain').height(contentHoehe+15); //15=Hoehe des Headers inkl. Border
		$('body').find('div#PopupMain').width(contentBreite);

		$('body').find('div#popupLayer').width(contentBreite+5);  //5= Breite des Schattens rechts

		$('body').find('div#PopupRight').height(contentHoehe);

		$('body').find('div#PopupFooter').width(contentBreite+5); //5= Breite des Schattens rechts
		$('body').find('div#PopupBottom').width(contentBreite-5);  //10 = Breite Schatten rechts + Schatten links

		return false;
	},

	init: function() {

		//Cursor auf Move oder Auto setzen
		$('#PopupHeader').css({cursor:'move'});


		$("#popupLayer").mousemove(function(event) {
			$('#popupLayer').draggable();

			if(event.target.id == 'PopupHeader') {
				$('#popupLayer').draggable('enable');
			}
			else if(event.target.id == 'PopupContent') {
				$('#popupLayer').draggable('disable');
			}
			else if(event.target.id == 'PopupMoveIcon') {
				$('#popupLayer').draggable('enable');
			}
			else if(event.target.id == 'PopupCloseIcon') {
				$('#popupLayer').draggable('disable');
			}
			else if(event.target.id == 'PopupContent') {
				$('#popupLayer').draggable('disable');
			}
			else if(event.target.id == 'PopupBottomLeft') {
				$('#popupLayer').draggable('disable');
			}
			else if(event.target.id == 'PopupBottom') {
				$('#popupLayer').draggable('disable');
			}
			else if(event.target.id == 'PopupBottomRight') {
				$('#popupLayer').draggable('disable');
			}
			else if(event.target.id == 'PopupRight') {
				$('#popupLayer').draggable('disable');
			}
			else if(event.target.id == 'PopupFooter') {
				$('#popupLayer').draggable('disable');
			}
			else if(event.target.id == 'PopupTopRight') {
				$('#popupLayer').draggable('disable');
			}
			else if(event.target.id == '') {
				$('#popupLayer').draggable('disable');
			}
		});
		return false;
	},

	open:function(urlString, dragndrop) {
		if($('#transparentLayer')) {
		   	$('#transparentLayer').click(function () {
			  dragPopup.close();
			});
			y = ($(document).scrollTop() - Math.ceil($('#PopupMain').height()/2)  + ($(window).height()/2));
			x = ($(document).width()/2) -  Math.ceil($('#PopupMain').width() / 2);
			$('div#popupLayer').css({top: y + 'px',left: x +'px'});
		}
		else {
			$('div#popupLayer').css({top:'50px',left:'50px'});
		}

		return false;
	},

	close:function() {
		if($('#transparentLayer')) {
			$('#transparentLayer').remove();
		}
		$('div#popupLayer').css({visibility:'hidden'})
		$('div#popupLayer').remove();
		return false;
	}
}


//---------------------------------------------------------------
// Bildergalerie
//---------------------------------------------------------------
var imageGallery = {


	init:function(id,size) {
		this.id = id;
		
		
		//Arrays initialisieren
		if(typeof(this.pages) == 'undefined')
			this.pages = new Array();
		if(typeof(this.activePage) == 'undefined')
			this.activePage = new Array();
		if(typeof(this.liste) == 'undefined')
			this.liste = new Array();
		if(typeof(this.text) == 'undefined')
			this.text = new Array();
		if(typeof(this.size) == 'undefined')
			this.size = new Array();
    
    this.link = "return imageGallery.countDown(this);";
    
		this.size[this.id] = 1; //0 = small -- 1 = large

		this.activePage[this.id] = 1; //

		$("div#" + id).find("ul.listImageGallery").find('a.image').mouseover(function () {
			  return imageGallery.showText(this);
		});


		//den Text des 1ten Bildes holen und anzeigen
		this.text[this.id] = $("div#" + id).find("ul.listImageGallery").children("li:eq('0')").find("p").html();
		$("div#" + this.id).find("div.text").find("p").html(this.text[this.id]);

		this.headline = $("div#" + id).find("ul.listImageGallery").children("li:eq('0')").find("h3").html();
		$("div#" + this.id).find("h3.headline").html(this.headline);
		//this.headline[] = $("div#" + id).find("ul.listImageGallery").children("li:eq('0')").find("h3").html();
		//this.gallery.find("h3.headline").html(this.headline);


		//den wei�en Arrow auf das erste Bild setzen
		$("div#" + id).find("ul.listImageGallery").children("li:eq('0')").find("img.overlay").hide();

		//Anzahl der Bilder
		this.listLength = $("div#" + id).find("ul.listImageGallery").children("li").length;

		//3 Bilder - das mittlere Bild - Abstand setzen
		$("div#" + id).find("ul.listImageGallery").children("li:eq('1')").addClass('middle');
		this.pages[this.id] = Math.ceil((parseInt(this.listLength)/parseInt(3)));
		

		//insert
		$('div.btBack').find('a').css('cursor','default');
		$('div.btBack').find('a').addClass('passiv');
		                                    
		if( this.activePage[this.id] == this.pages[this.id]){
			$('div.btForward').find('a').css('cursor','default');	
		}

		this.setPage(1,this.id);
		
		
		//$('div#' + id).find('div.btBack').find('a').bind("click", function(){
		//		return imageGallery.countDown(this);
		//	});
		
		$('div#' + id).find('div.btForward').find('a').bind("click", function(){
				return imageGallery.countUp(this);
			});
		
		return false;
	},

	//Init Gallery for related content column
	initSmall:function(id) {
		this.init(id);
		return false;
	},

	//Init Gallery for normal content column
	initLarge:function(id) {
		this.init(id);
		return false;
	},

	//Seite runterzaehlen
	countDown:function(object) {
		//Gallery finden
		this.gallery = $(object).parent().parent().parent();
		//Liste finden
		var id = this.gallery.attr('id');
		this.liste[id] = this.gallery.find('ul');

		//Aktive Seite setzen
		if(this.activePage[id] > 1)
			this.activePage[id]--;
		this.setPage(this.activePage[id],id);


		//Das 1ste Bild mit Arrow und Text versehen
		var index = ((this.activePage[id] * 3) - 3);

		this.liste[id].children('li').find("img.overlay").show();
		this.liste[id].children('li:eq("' + index+ '")').find("img.overlay").hide();

		//den Text holen und anzeigen
		this.text = this.liste[id].children('li:eq("' + index+ '")').find("p").html();
		this.gallery.find("div.text").find("p").html(this.text);

		this.headline = this.liste[id].children('li:eq("' + index+ '")').find("h3").html();
		this.gallery.find("h3.headline").html(this.headline);

		//Listitems ein/ausblenden
		this.showHideItems(id);
    
    $('div#' + id).find('div.btForward').find('a').css('cursor','pointer');
    $('div#' + id).find('div.btForward').find('a').removeClass('passiv');
    $('div#' + id).find('div.btForward').find('a').unbind('click').bind("click", function(){
				return imageGallery.countUp(this);
			});                  


    
    if( this.activePage[id] == 1){                                   
			$('div#' + id).find('div.btBack').find('a').addClass('passiv');
			$('div#' + id).find('div.btBack').find('a').css('cursor','default');
			$('div#' + id).find('div.btBack').find('a').unbind('click');
		}
    
		return false;

	},

	//Seite hochzaehlen
	countUp:function(object) {
		//Gallery finden
		this.gallery = $(object).parent().parent().parent();
		//Liste finden
		var id = this.gallery.attr('id');
		this.liste[id] = this.gallery.find('ul.listImageGallery');

		//Aktive Seite setzen
		if(this.activePage[id] < this.pages[id])
			this.activePage[id]++;
		this.setPage(this.activePage[id],id);

		//Das 1ste Bild mit Arrow und Text versehen
		var index = ((this.activePage[id] * 3) - 3);

		this.liste[id].children('li').find("img.overlay").show();
		this.liste[id].children('li:eq("' + index+ '")').find("img.overlay").hide();

		//den Text holen und anzeigen
		this.text = this.liste[id].children('li:eq("' + index+ '")').find("p").html();
		this.gallery.find("div.text").find("p").html(this.text);

		this.headline = this.liste[id].children('li:eq("' + index+ '")').find("h3").html();
		this.gallery.find("h3.headline").html(this.headline);

		//Listitems ein/ausblenden
		this.showHideItems(id);
    
    
		$('div#' + id).find('div.btBack').find('a').css('cursor','pointer');
		$('div#' + id).find('div.btBack').find('a').removeClass('passiv');
		$('div#' + id).find('div.btBack').find('a').unbind('click').bind("click", function(){
				return imageGallery.countDown(this);
			});
		      
    if( this.activePage[id] == this.pages[id]){
    	$('div#' + id).find('div.btForward').find('a').addClass('passiv');
			$('div#' + id).find('div.btForward').find('a').css('cursor','default');
			$('div#' + id).find('div.btForward').find('a').unbind('click');
		}
		return false;
	},

	//Ein/Ausblenden der Listitems
	showHideItems:function(id) {
		//alle Elemente ausblenden
		this.liste[id].children('li').hide();

		//die richtigen 3 Bilder einblenden
		this.liste[id].children('li:eq(' + ((this.activePage[id] * 3) - 3) + ')').show();
		this.liste[id].children('li:eq(' + ((this.activePage[id] * 3) - 2) + ')').addClass('middle');
		this.liste[id].children('li:eq(' + ((this.activePage[id] * 3) - 2) + ')').show();
		this.liste[id].children('li:eq(' + ((this.activePage[id] * 3) - 1) + ')').show();


		return false;
	},

	showText:function(object) {
		this.gallery = $(object).parent().parent().parent().parent().parent();
		var id = this.gallery.attr('id');

		//Alle wei�en Arrows verstecken
		this.liste[id] = this.gallery.find('ul');
		this.liste[id].find('img.overlay').show();

		//Den passenden wei�en Arrow anzeigen
		$(object).parent().find('img.overlay').hide();

		this.gallery.find("div.text").find("p").html(this.text = $(object).parent().find("p").html());
		this.gallery.find("h3.headline").html(this.headline = $(object).parent().find("h3").html());

		return false;
	},


	//Funktion um die Seite zusetzen
	//wird von countDown und countUp aufgerufen
	setPage:function(active, id) {
		$("div#" + id).find("div.imageGalleryCounter").find("span").text(active + "/" + this.pages[id]);

		return false;
	}
}


//---------------------------------------------------------------
// Horizontaler Slider f�r Bilder etc.
//---------------------------------------------------------------

var slider = {
	//Funktion um die Breite der Liste zu setzen
	//wird nicht direkt aufgerufen sondern von der Funktion initSmall / initLarge
	setLength: function (id) {
		this.id = id;
		this.ul = document.getElementById(id).getElementsByTagName('UL')[0];
		this.listLI = this.ul.getElementsByTagName('LI');
		width = 0;
		for(var i = 0; i < this.listLI.length; i++) {
			width += parseInt(this.listLI[i].offsetWidth);
		}
		this.ul.style.width = width + 'px';
	},
	
	setHeight: function (id) {
		this.listH2 = this.ul.getElementsByTagName('H2');
		if (this.listH2.length) {
			height = this.listH2[0].offsetHeight - 3;
		}
		for(var i=1; i< this.listH2.length; i++) {
			if (this.listH2[i].offsetHeight > height) {
				height = this.listH2[i].offsetHeight;
			}
		}
		for(var i=0; i< this.listH2.length; i++) {
			$(this.listH2[i]).height(height);
		}
		height -= 16;
		height += 'px';	

		topBt = parseInt($(this.ul).find('h2:first').css('height')) + parseInt($(this.ul).find('h2:first').css('padding-top'));
		topCounter = parseInt($(this.ul).find('h2:first').css('height')) - 12;
		
		$('#' + id).find('div.btBack').css('top', topBt);
		$('#' + id).find('div.btForward').css('top', topBt);
		$('#' + id).find('div.pageCounterBtBack').css('top', topCounter);
		$('#' + id).find('div.pageCounterBtForward').css('top', topCounter);
		
		$(this.ul).height($(this.ul).outerHeight());
	},

	//Funktion um den Sldier zu initialisieren
	//wird nicht direkt aufgerufen sondern von der Funktion initSmall / initLarge
	init: function(interval,id) {
		
		$('.cnSliderLarge').find('li:last').css('border-right', '0px');
		
		this.scrollInterval = new Array();
		var itemsCount = new Array();

		this.scrollInterval[id] = interval;
		itemsCount[id] = this.listLI.length;

		ul = document.getElementById(id).getElementsByTagName('UL')[0];

		//Seitennummerierung initialisieren
		if(typeof(this.activePage) == 'undefined')
			this.pages = new Array();


		this.pages[id] = Math.ceil((parseInt(this.listLI.length)/parseInt(this.scrollInterval[id])));
		$("div#" + id).find("div.btBack").find("a").css("cursor","default");

		if(this.pages[id]==1) {
			$("div#" + id).find("div.btForward").find("a").css("cursor","default");
		}		
		var itemsToAdd = new Array();
		itemsToAdd[id] = (this.scrollInterval[id]*this.pages[id]) - itemsCount[id];
		itemsCount[id] +=itemsToAdd[id];


		if(this.pages[id] > 1) {
			jQuery.easing.def = "easeInOutCirc";
			jQuery('#' + ul.id).jcarousel({
				scroll: this.scrollInterval[id],
				size: itemsCount[id],
				animation:1200
			   });
		}

		if(typeof(this.activePage) == 'undefined')
			this.activePage = new Array();

		this.activePage[id] = 1; // init the PageCounter
		
		if(this.pages[id] < 2) {
			//var tempIndex = document.getElementById(id).getElementsByTagName('a').length - 1;
			//this.fwdButton = document.getElementById(id).getElementsByTagName('a')[tempIndex];
			//this.fwdButtonImg = this.fwdButton.getElementsByTagName('img')[0];
			//this.fwdButtonImg.src = this.fwdButtonImg.src.replace(/act.gif/, "pas.gif");
			//this.fwdButton.style.cursor = '';
		}
		
		this.setPage(1, id);

		$('#' +  id).find("div.btBack").find('a').click( function() { slider.countDown(id); } );
		$('#' +  id).find("div.btForward").find('a').click( function() { slider.countUp(id); } );

	},

	//Funktion um den kleinen Slider mit einer Scrolllaenge von 3 zu initialisieren
	//muss 1mal im HTML aufgerufen werden
    initSmall: function (id) {
        this.setLength(id);
		this.setHeight(id);
        this.init(3,id);
    },

	//Funktion um den grossen Slider mit einer Scrolllaenge von 5 zu initialisieren
	//muss 1mal im HTML aufgerufen werden
    initLarge: function (id) {
        this.setLength(id);
		this.setHeight(id);
        this.init(5,id);
    },

	//Funktion um die Seitenzahlen hochzuzaehlen
	countUp:function(id) {
				
		this.backButton = $('#' + id).find('.btBack').find('a');
		this.fwdButton = $('#' + id).find('.btForward').find('a');
		

		if(this.activePage[id] < this.pages[id]) {
			
			this.activePage[id]++;
			this.setPage(this.activePage[id], id);
		}
		
		if(this.activePage[id] == this.pages[id]) {
			$(this.fwdButton).attr('class', '');
			$(this.fwdButton).css('cursor', 'default');
		} else {
			$(this.fwdButton).attr('class', 'active');
			$(this.fwdButton).css('cursor', '');
		}

		if(this.activePage[id] > 1) {
			$(this.backButton).attr('class', 'active');
			$(this.backButton).css('cursor', '');
		} else {
			$(this.backButton).attr('class', '');
			$(this.backButton).css('cursor', 'default');
		}
		this.backButton.onclick=function(){};
		this.fwdButton.onclick=function(){};
		
		
		
		return false;					
	},

	//Funktion um die Seitenzahlen runterzuzaehlen
	countDown:function(id) {

		if(this.activePage[id] != 1) {
			this.backButton = $('#' + id).find('.btBack').find('a');
			this.fwdButton = $('#' + id).find('.btForward').find('a');

			if(this.activePage[id] > 1 ) {
				this.activePage[id]--;
				this.setPage(this.activePage[id], id);
			}
			
			if(this.activePage[id] < 2 ) {
				$(this.backButton).attr('class', '');
				$(this.backButton).css('cursor', 'default');
			} else {
				$(this.backButton).attr('class', 'active');
				$(this.backButton).css('cursor', '');
			}
			if(this.activePage[id] < this.pages[id]) {
				$(this.fwdButton).attr('class', 'active');
				$(this.fwdButton).css('cursor', '');
			}
 			else {
				$(this.fwdButton).attr('class', '');
				$(this.fwdButton).css('cursor', 'default');
			}
			this.backButton.onclick=function(){};
			this.fwdButton.onclick=function(){};
		}
		return false;		
	},

	//Funktion um die Seite zusetzen
	//wird von countDown und countUp aufgerufen
	setPage:function(active, id) {
		$("div#" + id).find("div.pageCounterBtBack").find("span").text(active + "/" + this.pages[id]);
		$("div#" + id).find("div.pageCounterBtForward").find("span").text(active + "/" + this.pages[id]);
	}
}




//-----------------------------------------------------------------------
// �ndert per AJAX den Inhalt von rt-Boxen.
//-----------------------------------------------------------------------
var rtContentBox = {

	changeCardContent:function(obj, urlString) {
		return rtContentBoxes.changeContent(obj, urlString);
	}

}



//-----------------------------------------------------------------------
//Vertikaler Textslider f�r News etc.
//-----------------------------------------------------------------------
var px = 10;//mehr=schneller
var timer = new Array();

//var scrollStartPos = 0;
//var scrollEndPos = 0;


var rtContentBoxes = {

	scrollMsDiv:function(obj, direction) {
		//var id = this.getCurrentCard(obj);
		clearTimeout(timer);
		this.d = document.getElementById('scrollMsArea');
		y=this.d.scrollTop;

		if(direction==1)y-=px;
		if(direction==2)y+=px;
		if(y <= this.d.scrollHeight - this.d.offsetHeight + px && y >= 0 - px){
			this.d.scrollTop=y;
			timer=setTimeout(function() {rtContentBoxes.scrollMsDiv(obj, direction)},50);
		}
		if(y<0||y>this.d.scrollHeight-this.d.offsetHeight){
			clearTimeout(timer);
		}
		return false;
	},


	scrollDiv:function(obj,direction) {
		//var id = this.getCurrentCard(obj);
		clearTimeout(timer);
		this.d = $(obj).parent().parent().parent().parent().find("div")[0].getElementsByTagName('div')[1];
		y=this.d.scrollTop;

		if(direction==1)y-=px;
		if(direction==2)y+=px;
		if(y <= this.d.scrollHeight - this.d.offsetHeight + px && y >= 0 - px){
			this.d.scrollTop=y;
			timer=setTimeout(function() {rtContentBoxes.scrollDiv(obj,direction)},50);
		}
		if(y<0||y>this.d.scrollHeight-this.d.offsetHeight){
			clearTimeout(timer);
		}
		return false;
	},

	clearTimeout:function(obj) {
		//var id = this.getCurrentCard(obj);
		clearTimeout(timer);
		return false;
	},

	changeContent:function(obj, urlString) {
		ulObj = obj.parentNode.parentNode;
		for(i=0;i<ulObj.getElementsByTagName('li').length;i++) {
			if(ulObj.getElementsByTagName('li')[i] != obj.parentNode) {
				ulObj.getElementsByTagName('li')[i].className = "";
			} else {
				obj.parentNode.className = "active";
			}
		}

		if(urlString) {
		    vspace = $(obj).parent().parent().parent().find("div")[0].offsetHeight / 2 - 20;
		    $(obj).parent().parent().parent().find("div")[0].innerHTML = "<center><img vspace=" + vspace + " src=\"images/icon/loading.gif\" align=\"center\" width=\"40\" height=\"40\"></center>";
			$.ajax({
			  url: urlString,
			  cache: false,
			  success: function(html){
				$(obj).parent().parent().parent().find("div")[0].innerHTML = html;
				execJS($(obj).parent().parent().parent().find("div")[0]);
			  }
			});
		} else {

			myCards = $(obj).parent().parent().parent().find("div.card");
			for(i=0; i<myCards.length; i++) {
				if($(myCards[i]).hasClass(obj.className)) {
					myCards[i].style.display = 'block';
				} else {
					myCards[i].style.display = 'none';
				}
			}
		}

		return false;
	}
}

//------------------------------------------------------------------------------
// Bookmark hinzuf�gen.
//------------------------------------------------------------------------------
var favorit = {
	add: function() {
	//alert(location.href + '\n' + document.title );
		/*
		if ((typeof window.sidebar == "object") && (typeof window.sidebar.addPanel == "function")) {
			if ( window.location.protocol == "file:" ) {
				alert('Eine lokale Datei k�nnen Sie nicht zu den Favoriten hinzuf�gen');
			} else {
				window.sidebar.addPanel( location.href, document.title, "");
			}
		}
		else */

		if( window.external && window.external.AddFavorite ) {
			window.external.AddFavorite(location.href, document.title);
		} else {
			alert('Funktion in diesem Browser nicht m�glich!');
		}
	}
}

/*
function csssidebar_netscape () {
  if ((typeof window.sidebar == "object") && (typeof window.sidebar.addPanel == "function")) {
    if ( window.location.protocol == "file:" ) {
      var Check = confirm(NSHinw);
      if (Check == true) {
        window.sidebar.addPanel ("SELFHTML: CSS",SBCSSo,"");
      }
    } else {
      window.sidebar.addPanel ("SELFHTML: CSS",SBCSS,"");
    }
  } else {
    alert ("Diese Funktion steht nur ab Netscape 6 und in Mozilla zur Verf�gung.");
  }
}
*/


//------------------------------------------------------------------------------
// Tools
//------------------------------------------------------------------------------
var tools = {
	openArticleImage: function(path, width, height) {

	}
}


//------------------------------------------------------------------------------
// Rating
//------------------------------------------------------------------------------
var rating = {

	setRatingArticle: function(obj,hbid,hbwert) {
	
	    cookiename = 'hbbewert'+hbid;
	    cookiewert = getCookie(cookiename);
	    
	    //a = document.cookie;
	    //cookiename = a.substring(0,a.search('='));
        //cookiewert = a.substring(a.search('=')+1,a.search(';'));
        
        rfurl = "";
        if (window.location.host == "localhost")
        {rfurl = "/news3/SingleClip.aspx?_t=ratingresult&docid=" + hbid}
        else
        {rfurl = "/SingleClip.aspx?_t=ratingresult&docid=" + hbid}
        
        //alert(cookiewert+'XXX'+hbid);
        
        if(cookiewert == hbid)
        {
             rfurlinsert = rfurl + "&newrating=0";
             alerttext = "Dieser Artikel wurde bereits von Ihnen bewertet.";
             //alert('Cookie gefunden');
             alert(alerttext);
        }
        else
        {
            var expiredate = new Date();
            expiredate = new Date(expiredate.getTime() +1000*60*60*24*365);
            document.cookie = cookiename+'='+hbid+'; expires='+expiredate.toGMTString()+'; path=/;'; 
            rfurlinsert = rfurl + "&newrating=" + hbwert;
            alerttext = "Vielen Dank f�r Ihre Bewertung.";
            //alert('Cookie geklebt');
            //alert(alerttext);
        }
        
	    rfu(rfurlinsert,"ratingdisplay");
	    // rfu(rfurl,"ratinginput");
	    	    
     	// window.scrollTo(0, 0);
		
		return false;
	},

	over: function (obj) {
		pos = this.getPosition(obj);
		ul = obj.parentNode.parentNode;
		for(i = 0; i < ul.getElementsByTagName('li').length; i++) {
			if((i+1)<=pos) {
				path = ul.getElementsByTagName('li')[i].getElementsByTagName('a')[0].getElementsByTagName('img')[0].src;
				ul.getElementsByTagName('li')[i].getElementsByTagName('a')[0].getElementsByTagName('img')[0].src = path.replace(/blue_12x12_0.gif/g, "orange_12x12_1.gif");
			}
		}
	},

	out: function (obj) {
		pos = this.getPosition(obj);
		ul = obj.parentNode.parentNode;
		for(i = 0; i < ul.getElementsByTagName('li').length; i++) {
			path = ul.getElementsByTagName('li')[i].getElementsByTagName('a')[0].getElementsByTagName('img')[0].src;
			ul.getElementsByTagName('li')[i].getElementsByTagName('a')[0].getElementsByTagName('img')[0].src = path.replace(/orange_12x12_1.gif/g, "blue_12x12_0.gif");
		}
	},

	getPosition: function (obj) {
		ul = obj.parentNode.parentNode;
		for(i = 0; i < ul.getElementsByTagName('li').length; i++) {
			if(ul.getElementsByTagName('li')[i].getElementsByTagName('a')[0] == obj) {
				return i+1;
			}
		}
	}

}


//------------------------------------------------------------------------------
// Rating png
//------------------------------------------------------------------------------
var ratingaudio = {

	setRatingAudio: function(obj,hbid,hbwert) {
	
	    cookiename = 'hbbewert'+hbid;
	    cookiewert = getCookie(cookiename);
	    
	    //a = document.cookie;
	    //cookiename = a.substring(0,a.search('='));
        //cookiewert = a.substring(a.search('=')+1,a.search(';'));
        
        rfurl = "";
        if (window.location.host == "localhost")
        {rfurl = "/news3/SingleClip.aspx?_t=ratingresultaudio&docid=" + hbid}
        else
        {rfurl = "/SingleClip.aspx?_t=ratingresultaudio&docid=" + hbid}
        
        //alert(cookiewert+'XXX'+hbid);
        
        if(cookiewert == hbid)
        {
             rfurlinsert = rfurl + "&newrating=0";
             alerttext = "Dieser Beitrag wurde bereits von Ihnen bewertet.";
             //alert('Cookie gefunden');
             alert(alerttext);
        }
        else
        {
            var expiredate = new Date();
            expiredate = new Date(expiredate.getTime() +1000*60*60*24*365);
            document.cookie = cookiename+'='+hbid+'; expires='+expiredate.toGMTString()+'; path=/;'; 
            rfurlinsert = rfurl + "&newrating=" + hbwert;
            alerttext = "Vielen Dank f�r Ihre Bewertung.";
            //alert('Cookie geklebt');
            //alert(alerttext);
        }
        
	    rfu(rfurlinsert,"ratingdisplay");
	    // rfu(rfurl,"ratinginput");
	    	    
     	// window.scrollTo(0, 0);
		
		return false;
	},

	over: function (obj) {
		pos = this.getPosition(obj);
		ul = obj.parentNode.parentNode;
		for(i = 0; i < ul.getElementsByTagName('li').length; i++) {
			if((i+1)<=pos) {
				path = ul.getElementsByTagName('li')[i].getElementsByTagName('a')[0].getElementsByTagName('img')[0].src;
				ul.getElementsByTagName('li')[i].getElementsByTagName('a')[0].getElementsByTagName('img')[0].src = path.replace(/blue_12x12_0.png/g, "orange_12x12_1.png");
			}
		}
	},

	out: function (obj) {
		pos = this.getPosition(obj);
		ul = obj.parentNode.parentNode;
		for(i = 0; i < ul.getElementsByTagName('li').length; i++) {
			path = ul.getElementsByTagName('li')[i].getElementsByTagName('a')[0].getElementsByTagName('img')[0].src;
			ul.getElementsByTagName('li')[i].getElementsByTagName('a')[0].getElementsByTagName('img')[0].src = path.replace(/orange_12x12_1.png/g, "blue_12x12_0.png");
		}
	},

	getPosition: function (obj) {
		ul = obj.parentNode.parentNode;
		for(i = 0; i < ul.getElementsByTagName('li').length; i++) {
			if(ul.getElementsByTagName('li')[i].getElementsByTagName('a')[0] == obj) {
				return i+1;
			}
		}
	}

}



var searchItemsList = {

	// changed to 5 by c.pohl (see Issue 1682)
	defaultItemsLength: 5, 
	linkTextArrow: "\<span class=\"arrow\"\>\<\/span\>",
	linkTextClose: 'close',
	linkTextOpen: 'open',

	init: function() {
		ul= $('div.searchItems').find('ul');

		for(i = 0; i < ul.length; i++) {
			li= $(ul[i]).find('li');
			/*
			for(j = 0; j < li.length; j++) {
				with($($(li[j]).children()[0])) {
					addClass('plus');
				}
			}
			*/
			for(j = this.defaultItemsLength; j < li.length; j++) {
				li[j].style.display = 'none';
			}
		}
	},

	actionOnGroup: function(obj) {
		ul = obj.parentNode.parentNode;
		li= $(ul).find('li');
		for(i = this.defaultItemsLength; i < li.length; i++) {
			if(li[i].style.display == 'none' || li[i].style.display == '') {
				li[i].style.display = 'block';
			}
			else {
				li[i].style.display = 'none';
			}
		}
		currentText = $(obj).text();
		if(this.linkTextOpen != currentText) {
			$(obj).html(this.linkTextOpen);
		}
		else {
			$(obj).html(this.linkTextClose);
		}
		coordinateCols();
		return false;
	}
}


var searchSubItemsList = {

	defaultItemsLength: 4,
	linkTextArrow: '',
	linkTextClose: 'close',
	linkTextOpen: 'open',

	init: function() {

		ul = $('div.searchItems > div.item > ul');

		for(i = 0; i < ul.length; i++) {
			ul[i].className = 'firstGen';
			li = $(ul[i]).children();

			for(j = 0; j < li.length; j++) {
				li[j].className = 'firstGen';
				
				with($($(li[j]).children()[0])) {
					if(parent().find('li').length) {
						addClass('minus');
					click(function () {


						with($(this)) {

							this.subLis = parent().find('li');
							
							for(i=0; i<this.subLis.length; i++ ) {
								
								if(this.subLis[i].style.display == 'none') {
									this.subLis[i].style.display = '';
								} else {
									this.subLis[i].style.display = 'none';
								}
							}
							
							if(hasClass('plus') && this.subLis.length ) {
								removeClass('plus');
								addClass('minus');
							} else {
								removeClass('minus');
								addClass('plus');
							}
							
						}
						coordinateCols();
						return false;
					});
					} else {
						addClass('plus');
					}
					
				}
				
				if(j >= this.defaultItemsLength) {
					li[j].style.display = 'none';
				} else {
					li[j].style.display = '';
				}
				
			}
			
		}

		$('ul.firstGen > li > ul > li').css("display", "block");
		coordinateCols();
	},

	actionOnGroup: function(obj) {
		ul = obj.parentNode.parentNode;
		li = $(ul).find('li.firstGen');

		for(i = this.defaultItemsLength; i < li.length; i++) {
			if(li[i].style.display == 'none') {
				li[i].style.display = '';
			}
			else {
				li[i].style.display = 'none';
			}
		}
		currentText = $(obj).text();
		if(this.linkTextOpen != currentText) {
			$(obj).html(this.linkTextOpen + this.linkTextArrow);
		}
		else {
			$(obj).html(this.linkTextClose + this.linkTextArrow);
		}
		coordinateCols();
		return false;
	}
}



function clearDefaultValue(obj) {
	with(obj) {
		if(value == defaultValue) value = '';
		style.color = '#666666';
	}
}

function restoreDefaultValue(obj) {
	with(obj) { 
		if(value == '') {
			value = defaultValue;
			style.color = '';
		}
	}
}

function preventDefaultValueSubmit (obj) {
	with(obj) {
		for(i=0; i<elements.length; i++) {
			with(elements[i]) {value = value.replace(/[\t\r\n]/g,''); if((value == defaultValue)&&(type.toLowerCase() == 'text')) value = "" };
		}
	}
}

function coordinateCols () {

  // funktion deaktiviert. w. jablonowski
  return false;

	for(j=0; j< $('div.rtRightCol').length; j++) {
		$('div.rtRightColTeaserBox').eq(j).css({ height:"auto" });
		var grBxHt = $('div.rtRightCol').eq(j).parent().outerHeight();
		for(i=0; i< $('div.rtRightCol').eq(j).children().length-1; i++) {
			grBxHt -= $('div.rtRightCol').eq(j).children()[i].offsetHeight;
			grBxHt -= 7;
		}
		grBxHt -= 7;
		if ($('div.rtRightColTeaserBox').eq(j).height() < grBxHt ) {
			$('div.rtRightColTeaserBox').eq(j).height(grBxHt);
		}
	}
}

// ================ NEW FUNCTIONS HB2009 BEGIN ================= //

$(document).ready(function() {
	$("li.hasSubCategory").hover(
		function() {
			$(this).addClass('over');
		 },  
		 function() {  
			$(this).removeClass('over');
		 }  
		);

   $("p.hideBox").hide();
   
    $('.disturberBox a.overlay').each(function() {
        var hiddenText = $(this).parent().find('p.hideBox');
        $(this).bind('mouseenter', function() {
            hiddenText.slideDown('fast');
        });
        $(this).bind('mouseleave', function() {
            hiddenText.slideUp('fast');
		});
		
	});
	
	$('.ctWp1TabularData tbody tr, table.analyser tr, table.Wp1MOMarkOffTopBottom tr, div.cnWp1TabularData tbody tr, div.gnExchangeTable tbody tr').each(function() {
        $(this).bind('mouseenter', function() {
            $(this).addClass('tableHover');
        });
        $(this).bind('mouseleave', function() {
            $(this).removeClass('tableHover');
		});
	});
});

(function($) {
	$.fn.uk0Slider = function(options){

		// default configuration properties
		var defaults = {
			speed:					1000,
			autoPlay:				false,
			pause:					2000
		};

		var options = $.extend(defaults, options);
		var itemsPerPage = null;
		var buttons = false; // indicates if next & prev buttons are existing
		var activeItem = null;
		var itemsToSlide = null;
		var isHovered = false;
		var scroll = false;

		this.each(function() {
			var obj = $(this);
			obj.css("overflow","hidden");

			var s = $("li", obj).length;
			var w = $("li", obj).width() + parseInt($("li", obj).css("margin-right"));
			var h = $("li", obj).height(); 

			$("ul", obj).css('width',s * w);

			itemsPerPage = Math.floor( parseInt($(obj).width()) / w );

			// calculate <li> total width to determine if navigation-buttons are needed.
			if (s * w > parseInt($(obj).width())) {
				buttons = true;
				itemsPerPage--;
				// add navigation-buttons to ThubnailsWrapper
				var objThubnailsWrapper = $(".uk0SliderThubnailsWrapper", obj);

				var htmlNavPrev = '<span class="uk0SliderNavigationPrev"><a href=\"javascript:void(0);\"></a></span>';
				var htmlNavNext = '<span class="uk0SliderNavigationNext"><a href=\"javascript:void(0);\"></a></span>';

				$(objThubnailsWrapper).before(htmlNavPrev);
				$(objThubnailsWrapper).after(htmlNavNext);

				var objNavNextButton = $('.uk0SliderNavigationNext', obj);
				var objNavPrevButton = $('.uk0SliderNavigationPrev', obj);

				// assign triggers to buttons
				$(objNavNextButton).click(function(){
					animate("next", true, obj);
				});
				$(objNavPrevButton).click(function(){
					animate("prev", true, obj);
				});

				// recalculate uk0SliderThubnailsWrapper.width to prevent overlapping of content
				$(".uk0SliderThubnailsWrapper", obj).css('width',parseInt($(obj).css("width")) - parseInt($(objNavNextButton).css("width")) - parseInt($(objNavPrevButton).css("width")));
			};

			// generate preview on hover for each <li> within <ul>
			$(obj).find('.uk0SliderThubnails > li').each(function(i) {
				var pr = $('.uk0SliderItemPreview', this).html();
				if (i == 0) { // show first preview as default
					$(".uk0SliderPreview", obj).html(pr);
				};
				$(this).hover( function() {
					activateItem(this,false);
				})
			});

			// add .active class to first <li> since it will be "selected" on init
			$(".uk0SliderThubnails > li:first", obj).addClass('active');
			activeItem = 0;

			function activateItem(item, setActiveItem) {
				$(".uk0SliderThubnails > li.active.", obj).removeClass('active'); // remove class from previously selected <li>'s (especially init select 1st since there is no onMouseOut)
				$(item).addClass('active');

				var pr = $('.uk0SliderItemPreview', item).html();
				$(".uk0SliderPreview", obj).html(pr);

				if (setActiveItem || options.autoPlay) { // only set internal activeItem to new index if set got scrolled or autoPlay happend. do not set it on hover to prevent slide-confusion
					activeItem = $(".uk0SliderThubnails > li", obj).index(item);
				};
			}

			function deActiveItem(item) {
				return;
			}

			function animate(dir, clicked, obj){
				if (clicked) {
					itemsToSlide = itemsPerPage;
					options.autoPlay = false;
				} else {
					itemsToSlide = 1;
				};

				switch(dir){
					case "next":

					if (( (activeItem + 1) % itemsPerPage) == 0) { 
						scroll = true;
					} else {
						scroll = false;
					}

					if (clicked) { // scroll to next set or to max item on click

						if (activeItem + itemsPerPage >= (s - itemsPerPage) ) {
							diff = s - (activeItem + itemsPerPage);
							if (diff <= 0) {
								scroll = true;
								activeItem = 0;
							} else {
								activeItem = activeItem + diff;
							}
						} else {
							activeItem = activeItem + itemsPerPage;
						}

						p =  - (activeItem * w);

					} else { // scroll to next set if end of set reached on autoPlay
						if (activeItem +1 == s) { // rewind
							activeItem = 0;
							p = 0;
							scroll = true;
						} else {
							activeItem++; // always slide to next item on autoPlay
							p =  - (activeItem * w);
							// now lets see where we have to slide to
							var itemsLeft = s-activeItem;
							if (itemsLeft < itemsPerPage) { // next item is on a page that has not enough items for a whole set
								diff = (s - (activeItem)) - itemsPerPage;
								p =  - ((activeItem + diff) * w);
							}

						}
					}

					break;
					case "prev":
						options.autoPlay = false;
						if (activeItem - itemsPerPage <= 0) {
							activeItem = 0;
						} else {
							activeItem = activeItem - itemsPerPage +1;
						}
						p =  - (activeItem * w);
					break;
					default: break;
				}

				if (buttons && scroll || buttons && clicked) { // only autoPlay if there are buttons
					$("ul", obj).animate( { marginLeft: p }, options.speed);
					tagIt('ivw','slider','rotate');
				};

				activateItem($(".uk0SliderThubnails > li:eq("+activeItem+")", obj), true);
				if(clicked) clearTimeout(timeout);

				if(options.autoPlay && dir == "next" && !clicked && !isHovered){;
					timeout = setTimeout(function(){
						animate("next", false, obj);
					}, options.speed + options.pause);
				};

			};


			// init
			var timeout;

			$(obj).hover(
				function (e) {
					isHovered = true;
					clearTimeout(timeout);
				},
				function (e) { // restart autoPlay on mouseLeave
					if (options.autoPlay) {
						isHovered = false;
						timeout = setTimeout(function(){
														animate("next", false, obj);
													}, options.speed + options.pause);
					};
				} 
			);

			if(options.autoPlay){;
				timeout = setTimeout(function(){
					animate("next", false, obj);
				},options.pause);
			};

		});
	};
})(jQuery);

