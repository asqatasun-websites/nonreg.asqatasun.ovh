﻿function show(id) {
    var d = document.getElementById('smenu'+id);
    if(!d) return;
    
    var dNavidSearchSelect = document.getElementById('navidsearch');
    var dMoteurAttributsDiv = document.getElementById('MoteurAttributs');
    var dOrderByPrixPAPDiv = document.getElementById('trilist');
    var dOrderBySizeShoesDiv = document.getElementById('TAILLEPAP');
	 
	if(navigator.appName == 'Microsoft Internet Explorer'){
        if(!(document.getElementById('menu'+id)).contains(window.event.fromElement)){
            if (d) {
                d.style.display='block';
                
                var sNavigator = navigator.appVersion;
	            var iPositionVersion = sNavigator.indexOf('MSIE ');
	            var iIEVersion = sNavigator.substr(iPositionVersion+5,1);
	            
	            if(iIEVersion < 7 && dNavidSearchSelect){
	                // cache le select dans le panier
	                var aSelectBasket = document.getElementsByTagName("select");
                    for(var i=0;i<aSelectBasket.length;i++){
                        aSelectBasket[i].style.visibility='hidden';
                        
                    }
                    dNavidSearchSelect.style.visibility='hidden';
                    if(dMoteurAttributsDiv){
                        dMoteurAttributsDiv.style.visibility='hidden';
                    }
                    if(dOrderByPrixPAPDiv){
                        dOrderByPrixPAPDiv.style.visibility='hidden';
                    }
                    if(dOrderBySizeShoesDiv){
                        dOrderBySizeShoesDiv.style.visibility='hidden';
                    }
                }
            }
        }
	}
	else{	
        if (d) {
            d.style.display='block';
        }
    }
	var o = document.getElementById('a'+id);
    if (o) {
        if (o.className != 'a'+id+'sel')
            o.className = 'a'+id+'on';
    }
}

function hide(id) {
    var d = document.getElementById('smenu'+id);
    if(!d) return;
    
	var dNavidSearchSelect = document.getElementById('navidsearch');
    var dMoteurAttributsDiv = document.getElementById('MoteurAttributs');
    var dOrderByPrixPAPDiv = document.getElementById('trilist');
    var dOrderBySizeShoesDiv = document.getElementById('TAILLEPAP');
	if(navigator.appName == 'Microsoft Internet Explorer'){
	    if(!(document.getElementById('menu'+id)).contains(window.event.toElement)){
	        if (d) {
	            d.style.display='none';
	             
	            var sNavigator = navigator.appVersion;
	            var iPositionVersion = sNavigator.indexOf('MSIE ');
	            var iIEVersion = sNavigator.substr(iPositionVersion+5,1);
	            
	            if(iIEVersion < 7 && dNavidSearchSelect){
	                var aSelectBasket = document.getElementsByTagName("select");
                    for(var i=0;i<aSelectBasket.length;i++){
                        aSelectBasket[i].style.visibility='visible';
                    }
                    dNavidSearchSelect.style.visibility='visible';
                    if(dMoteurAttributsDiv){
                        dMoteurAttributsDiv.style.visibility='visible';
                    }
                    if(dOrderByPrixPAPDiv){
                        dOrderByPrixPAPDiv.style.visibility='visible';
                    }
                    if(dOrderBySizeShoesDiv){
                        dOrderBySizeShoesDiv.style.visibility='visible';
                    }
                }
            } 
        }
	}
	else{
        if (d) {
            d.style.display='none';
        }
	}
	var o = document.getElementById('a'+id);
	if (o) {
        if (o.className != 'a'+id+'sel')
            o.className = 'a'+id+'off';
    }
}


/*function show(idx) {
    var nMenus = 15;
    if(!document.getElementById) return;
    var d = document.getElementById('smenu'+idx);
    //if(navigator.appName != 'Microsoft Internet Explorer'){
        for (var i = 1; i<=nMenus; i++) {
	        if (document.getElementById('smenu'+i)) {
	            document.getElementById('smenu'+i).style.display='none';
            }
	        if (document.getElementById('a'+i) && document.getElementById('a'+i).className != 'a'+i+'sel') {
	            document.getElementById('a'+i).className='a'+i+'off';
            }
	    }
        if (d) {
            var o = document.getElementById('a'+idx);
            d.style.display='block';
            if (o) {
                if ( o.className != 'a'+idx+'sel')
                    o.className = 'a'+idx+'on';
            }
        }
        
        // hide elements for IE < 7 
        var dMoteurAttributsDiv = document.getElementById('MoteurAttributs');
        var dOrderByPrixPAPDiv = document.getElementById('trilist');
        var dOrderBySizeShoesDiv = document.getElementById('TAILLEPAP');
        var sNavigator = navigator.appVersion;
        var iPositionVersion = sNavigator.indexOf('MSIE ');
        var iIEVersion = sNavigator.substr(iPositionVersion+5,1);
        if(iIEVersion < 7) {
            if(idx) {
                var aSelectBasket = document.getElementsByTagName("select");
                for(var i=0;i<aSelectBasket.length;i++){
                    aSelectBasket[i].style.visibility='hidden';
                }
                dNavidSearchSelect.style.display='none';
                if(dMoteurAttributsDiv){
                    dMoteurAttributsDiv.style.visibility='hidden';
                }
                if(dOrderByPrixPAPDiv){
                    dOrderByPrixPAPDiv.style.visibility='hidden';
                }
                if(dOrderBySizeShoesDiv){
                    dOrderBySizeShoesDiv.style.visibility='hidden';
                }            
            } else {
                var aSelectBasket = document.getElementsByTagName("select");
                for(var i=0;i<aSelectBasket.length;i++){
                    aSelectBasket[i].style.visibility='visible';
                }
                dNavidSearchSelect.style.display='inline';
                if(dMoteurAttributsDiv){
                    dMoteurAttributsDiv.style.visibility='visible';
                }
                if(dOrderByPrixPAPDiv){
                    dOrderByPrixPAPDiv.style.visibility='visible';
                }
                if(dOrderBySizeShoesDiv){
                    dOrderBySizeShoesDiv.style.visibility='visible';
                }
            }
        }
    //}
}*/

// Change element class with id and classname
function fnchangecss(sId, sCSS)
{
    if(!document.getElementById) return;

    var oElem = document.getElementById(sId);
    if(!oElem) return;
    
    oElem.className = sCSS;
}


// validate newsletter subscriptions
function checknewslet(form) {
    var testform = false;
    if (form.email.value == '' || form.email.value.indexOf('@') == -1 || form.email.value.indexOf('.') == -1)
    {
        form.email.focus();
        alert('Entrez un email valide sous la forme nom_utilisateur@nom_domaine');
    } else
        testform = true;
    if (testform){ 
        window.open('','po','scrollbars=yes,resizable=no,toolbar=no,status=no,menubar=no,width=580,height=570');
        return true;
    } else 
        return false;
}

// validate that the customer
function adultConfirm(url) {
    if (confirm("Cette page est réservée aux personnes majeures.\nSi vous n'êtes pas majeur, cliquez sur Annuler.\nContinuer ?")) {
        document.location.href = RootPath + '/' + url;
    }
}
				
// add to favorites
