var EventListener = {
	listeners:[],
	bCapture:false,
	bSafari:/safari/i.test(navigator.userAgent),
	
	addEvents:function(els, type, func, scope) {
		var events = [];
		for(var event,i=0; i<els.length; i++) {
			event = this.addEvent(els[i], type, func, scope);
			events.push(event);
		}
		return events;
	},

	addEvent:function(el, type, func, scope) {
		if(!el) return;
		var handler = this.delegate(func, scope || el);
		try {
			el.addEventListener(type, handler, this.bCapture);
		} catch (e) {
			el.attachEvent('on' + type, handler);
		}

		var event = { element:el, type:type, handler:handler, capture:this.bCapture };
		this.listeners.push(event);
		return event;
	},

	removeEvents:function(events) {
		for (var i=0; i<events.length; i++) {
			this.removeEvent(events[i]);
		}
	},

	removeEvent:function(event) {
		try {
			event.element.removeEventListener(event.type, event.handler, event.capture);
		} catch (e) {
			event.element.detachEvent('on' + event.type, event.handler);
		}

		for(var i=0; i<this.listeners.length; i++) {
			if(this.listeners[i] == event) {
				this.listeners.splice(i, 1);
				break;
			}
		}
	},

	dispatchEvents:function(elements, type) {
		for (var i=0; i<elements.length; i++) {
			this.dispatchEvent(elements[i], type);
		}
	},
	
	dispatchEvent:function(el, type) {
		var events, node = el, event = {target:el, type:type};
		while(node && !event.cancelBubble) {
			events = this.getEvents(node, type);
			for(var i=0; i<events.length; i++) {
				events[i].handler(event);
			}
			node = node.parentNode;
		}
	},

	getTarget:function(e, name) {
		var target = e.target || e.srcElement;
		var reg = name? new RegExp('^'+name+'$', 'i') : null;
		while(target && (target.nodeType != 1 || (reg && !reg.test(target.nodeName)))) {
			target = target.parentNode;
		}
		return target;
	},

	getEvents:function(el, type) {
		var result = [];
		for(var item,i=0; (item = this.listeners[i++]);) {
			if((!el || el == item.element) && (!type || type == item.type)) {
				result.push(item);
			}
		}
		return result;
	},

	cancelEvent:function(e) {
		this.preventDefault(e);
		this.stopPropagation(e);
		return false;
	},

	preventDefault:function(e) {
		try {
			e.preventDefault();
		} catch (exception) {
			e.returnValue = false;
		}

		if(this.bSafari) {
			var target = this.getTarget(e, 'a');
		 	if(target) { target.onclick = function() { return false; }; };
		}
	},

	stopPropagation:function(e) {
		try {
			e.stopPropagation();
		} catch (exception) {
			e.cancelBubble = true;
		}
	},
	
	setCapture:function(toggle) {
		this.bCapture = toggle;
	},

	delegate:function(func, scope) {
		return function() {
			func.apply(scope, arguments);
		}
	}
}

function toggle_visibility(id) {
	var e = document.getElementById(id);
	if(e.style.display == 'block')
		e.style.display = 'none';
	else
		e.style.display = 'block';
}

function hide_other(id) {
	var o = document.getElementById(id);
	o.style.display = 'none';
}

// Set navigation navMenu

var sfHover = function() {
	//set hovers 
	if (document.getElementById("navMenu")) {
		//if ( (/msie 6/i.test(navigator.userAgent)) || (/msie 7/i.test(navigator.userAgent)) ) {
			// iframe coverage for IE6 and below
			var frm = document.createElement("iframe");
			frm.className = "cover-frm";
			frm.frameBorder = 0;
			document.getElementById("wrap").appendChild(frm);
			
			var sfEls = document.getElementById("navMenu").getElementsByTagName("li");
			for (var i=0; i<sfEls.length; i++) {
				sfEls[i].onmouseover = function() {
					ClassName.remove(this, "closed");
					ClassName.add(this, "open");
				}
				sfEls[i].onmouseout = function() {
					ClassName.remove(this, "open");
					ClassName.add(this, "closed");
				}
			}			
		//}
		/*var links = document.getElementById("navMenu").getElementsByTagName("a");
		for (var j=0; j<links.length; j++) {
			if (links[j].className.indexOf('topItem') != -1) {				
				links[j].onmouseout = function() { this.style.background = 'url(static/img/btn-left.gif) top left no-repeat'; this.style.color = '#fff'; }
			}			
		}
		var spans = document.getElementById("navMenu").getElementsByTagName("span");
		for (var h=0; h<spans.length; h++) {			
			if (spans[h].className.indexOf('topItem') != -1) {
				spans[h].onmouseout = function() { this.style.background = 'url(static/img/btn-right.gif) top right no-repeat'; }				
			}			
		}*/
	}
}
//if (window.attachEvent) window.attachEvent("onload", sfHover);

EventListener.addEvent(window, "load", function() {
	sfHover();
} );


/*******
** vp - ClassName module - (c) Indivirtual 2008 - 1.0 **	
*******/

var ClassName = {
	add:function (node, name) {
		if (!this.contains(node, name)) node.className += ' ' + name;
	},

	remove:function (node, name) {
		if (node.className)
			node.className = node.className.replace(new RegExp('(^|\\s)'+name+'(\\s|$)','g'), '');
			node.className = node.className.replace(' ' + name, '');
	},

	contains:function (node, name) {
		return new RegExp('(^|\\s)'+name+'(\\s|$)').test(node.className);
	},

	swap:function (node, old, name) {
		node.className = this.contains(node, old)?
			node.className.replace(new RegExp('(^|\\s)'+old+'(\\s|$)','g'), '$1'+name+'$2') : 
			node.className.replace(new RegExp('(^|\\s)'+name+'(\\s|$)','g'), '$1'+old+'$2');
	},

	toggle:function(node, name) {
		if (!this.contains(node, name)) {
			this.add(node, name);
		}
		else {
			this.remove(node, name);
		}
	}
}