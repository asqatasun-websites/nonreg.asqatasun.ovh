var scripts = {
	init: function () {
		// TEST FLASH
		if($('#test_flash').text()) {
			var langue = "fr";
			var flashfile = "http://media.mth.net/comedie_prod/images/swf/Ban_kaleos_FR-autre.swf"; 
			var fo = new FlashObject(flashfile, "test_flash", "204", "187", "8", "#FFFFFF");
			fo.addParam("wmode","transparent");
			fo.write("test_flash");
		}

		// AJOUT du 3-12-08 Scrollbar sur les fiches programme
		$('.programme #gauche .scroll-pane').css('overflow', 'none');
				$('.programme #gauche .scroll-pane').jScrollPane({scrollbarOnLeft:true, showArrows:true});


		$('#contenu_fiche_spectacle p').jScrollPane({scrollbarOnLeft:true, showArrows:false});
		$('#contenu_fiche_spectacle .jScrollPaneContainer p').css('padding-left','20px');
$('#contenu_fiche_spectacle .jScrollPaneContainer p').css('width','300px');
$('#contenu_fiche_spectacle .jScrollPaneContainer').css('clear','both');




		// AJOUT 23-09-08 du scroll sur les pages cr�ations com�die
		$('#conteneur_reglement .scroll-pane').jScrollPane({scrollbarOnLeft:true, showArrows:false});
			$('#conteneur_reglement .scroll-pane').css('padding-right', '0px');
$('#conteneur_reglement').css('padding-left', '10px').css('width', '530px');
$('.creation .jScrollPaneContainer').css('width', '530px').css('height', '370px');
$('.creation .scroll-pane').css('padding-left', '25px').css('width', '505px');
$('.creation .jScrollPaneTrack').css('height', '368px');

// AJOUT 10-12-2009 scroll pour les gagnants
if ($('#contenu_gagnants').css('height') > '359px'){
$('#contenu_gagnants').jScrollPane({scrollbarOnLeft:true, showArrows:false});
$('#gagnants .jScrollPaneTrack').css('left', '5px');
}

//$('').css('', '');

		// Ajout du menu deroulant sous la video de la colonne de gauche sur les pages creations comedie
		scripts.videoSanders();
		scripts.videoEmission();
		scripts.videoMerci();
		scripts.videoManu();
		scripts.videoSaints();
		scripts.videoMax();
		scripts.videoFontenay();
		scripts.videoBravo();
		scripts.videoNousAppartient();
		scripts.videoTimsit();
		scripts.videoPublivores();
		scripts.videoQcmm();
		scripts.videoArthur();
		scripts.videoSoireeArthur();
		scripts.videoDrolesDeJobs();

		// AJOUT 23-09-08 du scroll sur les pages cr�ations com�die
		$('#scroll_creations').jScrollPane({scrollbarOnLeft:true, showArrows:true});

$('.sanders .jScrollPaneContainer').css('width','500px');



		// Ajout du 22-07-2008 - GESTION DU SELECT SUR LA PAGE RECHERCHE
		if($('#select_page_recherche').html()) {
			$('#select_page_recherche').combobox({
				comboboxContainerClass: "conteneurListe_recherche", 
				comboboxValueContainerClass: "conteneurValeurListe_recherche", 
				comboboxValueContentClass: "contenuValeurListe_recherche", 
				comboboxDropDownClass: "conteneurDeroule_recherche", 
				comboboxDropDownButtonClass: "boutonListe_recherche", 
				comboboxDropDownItemClass: "elementListe_recherche", 
				comboboxDropDownItemHoverClass: "elementOverListe_recherche", 
				comboboxDropDownGroupItemHeaderClass: "elementListe_recherche", 
				comboboxDropDownGroupItemContainerClass: "elementListe_recherche", 
				animationType: "slide", 
				width: "160px" }
			);
		}
		$('.conteneurDeroule_recherche').css('width', '125px');

		if($('#comboSujet').html()) {
			$('#comboSujet').combobox({
				comboboxContainerClass: "conteneurListe_recherche", 
				comboboxValueContainerClass: "conteneurValeurListe_recherche", 
				comboboxValueContentClass: "contenuValeurListe_recherche", 
				comboboxDropDownClass: "conteneurDeroule_recherche", 
				comboboxDropDownButtonClass: "boutonListe_recherche", 
				comboboxDropDownItemClass: "elementListe_recherche", 
				comboboxDropDownItemHoverClass: "elementOverListe_recherche", 
				comboboxDropDownGroupItemHeaderClass: "elementListe_recherche", 
				comboboxDropDownGroupItemContainerClass: "elementListe_recherche", 
				animationType: "slide", 
				width: "252px" }
			);
			$('#contenu_la_chaine_4').css('display','none');
		}

		$('#contactUs .conteneurDeroule_recherche li.elementListe_recherche').click(function(){
			if(($(this).attr('title')=='Programmes/Cha�ne')||($(this).attr('title')=='Autres')){
				$('[name=receive]').each(function(){
					$(this).removeAttr("disabled");
				});
				$('#fournisseurName').removeAttr("disabled");
			}else{
				$('[name=receive]').each(function(){
					$(this).attr("disabled","disabled");
				});
				$('#fournisseurName').attr("disabled","disabled");
			}
		});

		
		// Ajout du 22-07-2008 - Ascenseur sur la page R�sultat Rechercher
		$('#resultat_recherche').jScrollPane({scrollbarOnLeft:true, showArrows:true});

		$('#logo_rss').click(function (){return false;});

		//Navigation entre les gagnants sur la page COMMUNAUTE
		var nb_gagnants = $('#contenu_gagnants li').length;
		if(nb_gagnants > 2) {
			$('.js_alternatif').show();
			$('#contenu_gagnants li').slice(2, (nb_gagnants)).hide();
			$('.js_alternatif a').click(function () {
				// Modifie le style du lien selectionne
				$(this).parent().parent().find('li').removeClass('select');
				$(this).parent().addClass('select');
				if($(this).html() == "1") {
					$('#contenu_gagnants li').slice(0, 2).show();
					$('#contenu_gagnants li').slice(2, $('#contenu_gagnants li').length).hide();
				} else {
					$('#contenu_gagnants li').slice(0, 2).hide();
					$('#contenu_gagnants li').slice(2, $('#contenu_gagnants li').length).show();
				}
				return false;
			});
		}

		var url = String(window.location);
		if($('#conteneur_flash').text()) {
			var langue = "fr";
			var flashfile = "http://media.mth.net/comedie_prod/images/swf/main.swf"; 
			var xmlfile = "datas/xml/home.xml"; 
			var fo = new FlashObject(flashfile, "conteneur_flash", "581", "482", "9", "#ffffff");
			fo.addVariable("configxml","xml/datas.xml");
			fo.addVariable("appswf","http://media.mth.net/comedie_prod/images/swf/main.swf");
			fo.addParam("wmode","transparent");
			fo.addParam("allowScriptAccess","always");
			fo.write("conteneur_flash");
		}



		if('.conteneurLien a'){
			$('.conteneurLien a').addClass('lien_seul popup');
		}


		// Gestion des POPUP
		$('.popup').click(function () { if($(this).attr('href')) {
			window.open($(this).attr('href'),'pop','width=1024,height=800,toolbar=1,menubar=1,location=1,status=1,scrollbars=1,resizable=1,directories=1');
			return false;
		}});

		// Gestion POPUP PROFIL
		$('.popupprofil').click(function () { if($(this).attr('href')) {
			window.open($(this).attr('href'),'pop','width=640,height=530,toolbar=0,menubar=0,location=0,status=0,scrollbars=1,resizable=1,directories=0');
			return false;
		}});
		
		// Gestion de l'affichage/masque de la zone en ce moment sur comedie !
		$('#en_ce_moment').hover(
			function () {
				$('#popin_en_ce_moment').show('fast');
			}, function () {
				// Rien en sortant
			}
		);
		$('#popin_en_ce_moment').hover(
			function () {
				$('#popin_en_ce_moment').show();
			}, function () {
				$('#popin_en_ce_moment').fadeOut('fast');
			}
		);
		// Formulaire rechercher dans l'entete
		$('#champ_rech').focus(function () {
			if($(this).val() == "Rechercher") $(this).val("");
		});
		$('#champ_rech').blur(function () {
			if($(this).val() == "") $(this).val("Rechercher");
		});
		// Formulaire sur la page videos
		$('.video form#recherche input#titre').focus(function () {
			if($(this).val() == "titre") $(this).val("");
		});
		$('.video form#recherche input#titre').blur(function () {
			if($(this).val() == "") $(this).val("titre");
		});
		// Formulaire recevez la newsletter sur la page chaine
		$('#champ_email').focus(
			function () {
				if($(this).val() == "Saisir votre e-mail") $(this).val("");
			}
		);
		$('#champ_email').blur(
			function () {
				if($(this).val() == "") $(this).val("Saisir votre e-mail");
			}
		);
		// Gestion du sous menu dans l'entete
		$('#menu > li').hover(
			function () {
				$('.conteneur_sous_menu').not($(this).find('.conteneur_sous_menu')).hide();
				$(this).find('.conteneur_sous_menu').show();
				$('#menu li:eq(1)').find('* > a >img').hide();
			}, function () {
				//$('.conteneur_sous_menu').hide();
			}
		);
		$('.conteneur_sous_menu').hover(
			function () {
				// Rien
			}, function () {
				$('#menu li:eq(1)').find('* > a >img').show();
				$('.conteneur_sous_menu').fadeOut(200);
			}
		);
		var affiche = 'false';
		var affiche_episodes = 'false';
		// Gesion de la nav au coeur de la page pr�sentation, s'abonner... sur la page chaine
		$('#sous_nav:not(.no_js):not(.js_alternatif) a').click( function () {
			// changment de l'�tat du lien
			if($('#sous_nav a img').attr('src')) {
				$('#sous_nav a img').each( function () {
					$(this).attr('src', $(this).attr('src').replace('_select.jpg', '.jpg'));
				});
				$(this).find('img').attr('src', $(this).find('img').attr('src').replace('.jpg', '_select.jpg'));
			} else {
				$('#sous_nav li').removeClass('select');
				$(this).parent().addClass('select');
			}
			// changement de la dispo des calques visibles avec contenu...
			// On modifie contenu_serie_X en contenu_serie
			var classObj = $(this).attr('class');
			aClass = classObj.split("_");
			aClass.pop();
			if($('#contenu_serie_3')){
				if ($('#contenu_serie_3').css('display')=='block'){
					$('#contenu_lecture_en_cours').html('');
				}
			}

			$('.'+aClass.join("_")+':visible').fadeOut('fast');
			$('#'+$(this).attr('class')).fadeIn();

			// Ascenseur perso sur la page S�rie onglet episotheque
			if ($(this).attr('href').indexOf('episotheque') > -1) {
				if(affiche_episodes == 'false'){
					$('#liste_episodes').jScrollPane({scrollbarOnLeft:true, showArrows:true});
					$('#desc_episode').jScrollPane({scrollbarOnLeft:true, showArrows:true});
					$('#contenu_serie_5 .jScrollPaneContainer:first').addClass('first');
					affiche_episodes = 'true';
				}
			}




			if ($('#contenu_artiste_2').css('display')=='block'){
				if(affiche == 'false'){
					$('#conteneur_scroll_2').jScrollPane({scrollbarOnLeft:true, showArrows:true});
					affiche = 'true';
				}
			}
			scripts.appli_combo_serie();
			return false;
		});

		$('#englob #titres_episodes').jScrollPane({scrollbarOnLeft:true, showArrows:false});
		$('#englob #titres_bonus').jScrollPane({scrollbarOnLeft:true, showArrows:false});
		$('#contenu_epis_friends #episodes_friends').jScrollPane({scrollbarOnLeft:true, showArrows:false});
if ($('#contenu_serie_1.contenu_serie').css('display')=='block'){
		$('#contenu_serie_1.contenu_serie p.scroll-pane').jScrollPane({scrollbarOnLeft:true, showArrows:true});
}




		if($('#conteneur_scroll_1').html()){
			$('#conteneur_scroll_1').jScrollPane({scrollbarOnLeft:true, showArrows:true});
		}

		$('.clicVideoSerie').click(function(){
			$('#formvideoserie').submit();
			return false;
		});

		$('#mentions_legales').click(function(){
			window.open($('#mentions_legales').attr('href'),'Mentions','width=620,height=600');
			return false;
		});


		$('#clickClose').click(function(){
			window.close();
			return false;
		});


		scripts.afficheScrollDiffSerie();

		$('.serie #annexe dl').jScrollPane({scrollbarOnLeft:true, showArrows:false});

		// Gestion de la navigation avec les fleches jaunes pour le coeur de la page LA CHAINE
		// nav vers la gauche
		$('#centre .mini_nav a:eq(0)').click( function () {
			if($('#sous_nav a img').attr('src')) {
				// calcul le nombre d'�l�ment dans la liste
				var nb_elmt = $('#sous_nav a img').length;
				// r�cupere le num�ro de position de l'�l�ment selectionn�
				var liste_elmt = $('#sous_nav a img');
				var i = 0;
				var trouve = false;
				while ((!trouve) && (i < liste_elmt.length)) {
					if($(liste_elmt[i]).attr('src').indexOf('_select.jpg') > -1) {
						trouve = true;
					} else {
						i++;
					}
				}
			} else {
				// calcul le nombre d'�l�ment dans la liste
				var nb_elmt = $('#sous_nav a').length;
				// r�cupere le num�ro de position de l'�l�ment selectionn�
				var liste_elmt = $('#sous_nav a');
				var i = 0;
				var trouve = false;
				while ((!trouve) && (i < liste_elmt.length)) {
					if($(liste_elmt[i]).parent().attr('class')) {
						if($(liste_elmt[i]).parent().attr('class').indexOf('select') > -1) {
							trouve = true;
						} else {
							i++;
						}
					} else {i++;}
				}
			}
			// on s�l�ctionne le bon
			if(i == 0) {
				i = nb_elmt-1;
			} else {
				i--;
			}
			$('#sous_nav a:eq('+i+')').click();
			return false;
		});
		// nav vers la droite
		$('#centre .mini_nav a:eq(1)').click( function () {
			if($('#sous_nav a img').attr('src')) {
				// calcul le nombre d'�l�ment dans la liste
				var nb_elmt = $('#sous_nav a img').length;
				// r�cupere le num�ro de position de l'�l�ment selectionn�
				var liste_elmt = $('#sous_nav a img');
				var i = 0;
				var trouve = false;
				while ((!trouve) && (i < liste_elmt.length)) {
					if($(liste_elmt[i]).attr('src').indexOf('_select.jpg') > -1) {
						trouve = true;
					} else {
						i++;
					}
				}
			} else {
				// calcul le nombre d'�l�ment dans la liste
				var nb_elmt = $('#sous_nav a').length;
				// r�cupere le num�ro de position de l'�l�ment selectionn�
				var liste_elmt = $('#sous_nav a');
				var i = 0;
				var trouve = false;
				while ((!trouve) && (i < liste_elmt.length)) {
					if($(liste_elmt[i]).parent().attr('class')) {
						if($(liste_elmt[i]).parent().attr('class').indexOf('select') > -1) {
							trouve = true;
						} else {
							i++;
						}
					} else {i++;}
				}
			}
			// on s�l�ctionne le bon
			if(i == (nb_elmt-1)) {
				i = 0;
			} else {
				i++;
			}
			$('#sous_nav a:eq('+i+')').click();
			return false;
		});
		// Gestion de la nav de dernieres videos sur la home
		var largeur_dv = $('#liste_dernieres_videos li').length * 145;
		$('#liste_dernieres_videos').css('width', largeur_dv+'px');
		// Ligne suivante tr�s importante
		$.scrollTo.defaults.axis = 'x'; 
		$('#dernieres_videos .mini_nav a:eq(0)').click( function () {
			if($('#liste_dernieres_videos').offset().left - $('#dernieres_videos').offset().left >= 0) {
				$('#conteneur_dernieres_videos_defil').stop().scrollTo( {top:'+=0px', left:'+='+$('#liste_dernieres_videos').width()}, 1000, {onAfter: function () {$('#conteneur_dernieres_videos_defil').stop();}} );
			} else {
				$('#conteneur_dernieres_videos_defil').stop().scrollTo( {top:'+=0px', left:'-=290px'}, 300, {onAfter: function () {$('#conteneur_dernieres_videos_defil').stop();}} );
			}
		});
		$('#dernieres_videos .mini_nav a:eq(1)').click( function () {
			if($('#liste_dernieres_videos').offset().left - $('#dernieres_videos').offset().left <= -($('#liste_dernieres_videos').width() - $('#dernieres_videos').width())) {
				$('#conteneur_dernieres_videos_defil').stop().scrollTo( {top:'+=0px', left:'-='+$('#liste_dernieres_videos').width()}, 1000, {onAfter: function () {$('#conteneur_dernieres_videos_defil').stop();}} );
			} else {
				$('#conteneur_dernieres_videos_defil').stop().scrollTo( {top:'+=0px', left:'+=290px'}, 300, {onAfter: function () {$('#conteneur_dernieres_videos_defil').stop();}} );
			}
		});
		var url = String(window.location);
		// DEBUG pour la largeur des tableau dans les tableau sur la page  GUIDE TV
		if(url.indexOf("guide_tv") > -1){
		scripts.widthProg();
		}

		//Gestion de la nav galerie photo sur la page ARTISTE DU MOIS
		// on masque les vignettes superflues
		$('#galerie_photo ul:not(.mini_nav) li').slice(8, $('#galerie_photo ul:not(.mini_nav) li a').length).hide();
		scripts.init_click_slide();
		
		// Gestion de la liste archives des artistes
		$('#liste_artiste').hide();
		$('#champ_select_artiste').click(function () {
			$('#liste_artiste').fadeIn('fast');
			$('#liste_artiste').css('position', 'absolute');
			$('#liste_artiste').css('top', '41px');
			//$('#liste_artiste').css('left', '0px');
			$('#liste_artiste').css('margin', '0px');
			$('#liste_artiste').hover(
				function () {
					// rien
				}, function () {
					$(this).fadeOut('fast');
				}
			);
			return false;
		});
		
		// Gestion de l'effet highslide
		$('.effet_highslide').lightBox();
				
		// Liste deroulante perso pour les videos
		if($('#comboCategorie').html()) {
			$('#comboCategorie').combobox({
				comboboxContainerClass: "conteneurListe", 
				comboboxValueContainerClass: "conteneurValeurListe", 
				comboboxValueContentClass: "contenuValeurListe", 
				comboboxDropDownClass: "conteneurDeroule", 
				comboboxDropDownButtonClass: "boutonListe", 
				comboboxDropDownItemClass: "elementListe", 
				comboboxDropDownItemHoverClass: "elementOverListe", 
				comboboxDropDownGroupItemHeaderClass: "elementListe", 
				comboboxDropDownGroupItemContainerClass: "elementListe", 
				animationType: "slide", 
				width: "255px" }
			);
		}


		$('.contenu_blague #formulaire_blague #BLAGUE').keypress(function(){
			if(this.value.length>=250){
				this.value=this.value.substr(0,250);
			}
		});




		$('.contenu_blague #formulaire_blague a').click(function(){
			attention = false;
			message = '';
			if($('#NAME').val()==''){
				message += "Veuillez indiquer votre nom\n";
				attention = true;
			}
			if($('#VORNAME').val()==''){
				message += "Veuillez indiquer votre pr�nom\n";
				attention = true;
			}
			if($('#DEPT').val()==''){
				message += "Veuillez indiquer votre d�partement\n";
				attention = true;
			}
			if($('#BLAGUE').val()==''){
				message += "Veuillez indiquer votre blague\n";
				attention = true;
			}
			var reg = /^[a-z0-9._-]+@[a-z0-9.-]{2,}[.][a-z]{2,3}$/
			if(!reg.exec($('#EMAIL').val())) {
				message += "Veuillez indiquer votre adresse �lectronique\n";
				attention = true;
			}
			if (attention){alert(message);}else{$('#formulaire_blague').submit();}
			return false;
		});		

		// Accordeon blagues
		if($("#contenuGauche").html()) $("#contenuGauche").accordion({header: 'h4', event: 'mouseover'});
		
		// Gestion du "lire la suite" sur la page ARTISTE
		$('.lien_lire_la_suite_artiste').click(function () {
			var block = $(this).parent().attr('class').split(' ');
			block = block[1];
			$('.'+block).toggle();
			return false;
		});
		
		// Flash pied
		if($("#logo_rss").html()){
			var langue = "fr";
			var flashfile = "swf/rss.swf";
			var fo = new FlashObject(flashfile, "swf", "35", "35", "8", "#000000");
			fo.addParam("wmode","opaque");
			fo.write("logo_rss");
		}

		// Gestion du changement de video sur la page VIDEOS
		$('#videos li a').click(function () {

			// Gestion de l'affichage du message d'interdiction moins de 18 sur la page VIDEOS
			if($(this).hasClass('moins_18')) {
				$('#message_moins_18').show();
				var url_temp = $(this).attr('rel');
				$('.selection_video_temp').removeClass('selection_video_temp');
				$(this).addClass('selection_video_temp');
				$('.lien_moins_18').click(function () {
					$('#message_moins_18').hide();
					return false;
				});
				$('.lien_plus_18').click(function () {
					$('#message_moins_18').hide();
					changeVideo($('.selection_video_temp').attr('rel'),$('.selection_video_temp').attr('ref'));
					return false;
				});
				return false;
			} else {
				changeVideo($(this).attr('rel'),$(this).attr('ref'));
			}
			return false;
		});
		
		//gestion des clics sur les videos des pages series
		$('#contenu_serie_3 #player ol li a').click( function(){
			$('#contenu_serie_3 #player ol li').removeClass('select');
			$(this).parent().addClass('select');
			changeVideo($(this).attr('rel'),$(this).attr('ref'));
			return false;
		});

		var url = String(window.location);
		if(url.indexOf("#abonner")>0){
			$('.contenu_la_chaine_2').click();
		}


		// Suppression des evenements dans les block no_js
		$('.no_js a').unbind("click");
		$('.no_js').unbind("click");



		// Gestion du select sur la page SERIES
		$("#contenu_serie_5").show().css('left', '-10000px');
		scripts.conteneurBoxSerie();
		$("#contenu_serie_5").hide().css('left', '15px');

		$('.video .conteneurDeroule').css('width', '218px');
		$('.serie .conteneurDeroule').css('width', '123px');
		$('.serie .conteneurDerouleSerie').css('width', '123px');
		// debug pour ie...

		// Verif champ email dans contact
		$(".la_chaine #formContact").submit(
			function(){

				// Nom
				if($("#nom").val().length < 1) {
					alert("Merci de saisir votre nom");
					return false;
				}

				// Mail
				var patternMail = new RegExp ( "^\\w[\\w+\.\-]*@[\\w\-]+\.\\w[\\w+\.\-]*\\w$", "gi" ) ;
				if($("#email").val().search( patternMail ) == -1) {
					alert("Merci de saisir une adresse email correcte");
					return false;
				}

				// Sujet
				if($("#comboSujet").val() == 'default' ) {
					alert("Merci de saisir votre sujet");
					return false;
				}

				//Receive
				checked="false";
				if(!$('#receive1').attr('disabled')){
					$('[name=receive]').each(function(){
						if($(this).attr("checked")){
							checked="true";
						}
					});
					if(checked=='false'){
						alert("Merci de nous dire comment vous recevez Comedie!");
						return false;
					}
				}

				//Fournisseur
				if(!$('#fournisseurName').attr('disabled')){
					if($("#fournisseurName").val().length < 1 ) {
						alert("Merci de saisir votre fournisseur/operateur");
						return false;
					}
				}

				// Message
				if($("#message").val().length < 1 ) {
					alert("Merci de saisir votre message");
					return false;
				}

				return true;
			}
		);

		var params = window.location.search;
		if(params == '?sent=1'){
			$(".contenu_la_chaine_4:eq(0)").click();
		}
	},
		init_click_slide: function () {
		$('#galerie_photo .mini_nav a:eq(0)').click(function () {
			var val_tot = $('#galerie_photo ul:not(.mini_nav) li a').length;
			var val_debut = $('#galerie_photo .infos').html().split(' ');
			var val_fin = val_debut[3];
			val_debut = val_debut[1];
			if(val_debut > 1) {
				val_debut = Number(val_debut) - 8;
				val_fin = val_debut + 7;
				if(val_fin > val_tot) {
					val_fin = val_tot;
				}
				$('#galerie_photo ul:not(.mini_nav) li').hide();
				$('#galerie_photo ul:not(.mini_nav) li').slice((val_debut-1), (val_fin)).show();
				$('#galerie_photo .infos').html('Photos '+(val_debut)+' � '+val_fin+' sur '+val_tot)
			}
			return false;
		});
		$('#galerie_photo .mini_nav a:eq(1)').click(function () {
			var val_tot = $('#galerie_photo ul:not(.mini_nav) li a').length;
			var val_debut = $('#galerie_photo .infos').html().split(' ');
			var val_fin = val_debut[3];
			val_debut = val_debut[1];
			if(val_fin < val_tot) {
				val_debut = Number(val_debut) + 8;
				val_fin = Number(val_debut) + 7;
				if(val_fin > val_tot) {
					val_fin = val_tot;
				}
				$('#galerie_photo ul:not(.mini_nav) li').hide();
				$('#galerie_photo ul:not(.mini_nav) li').slice((val_debut-1), (val_fin)).show();
				$('#galerie_photo .infos').html('Photos '+val_debut+' � '+val_fin+' sur '+val_tot)
			}
			return false;
		});

	},
	mini_png_killer:function(obj) {
		if ( $.browser.msie ) {
			var temp = $.browser.version;
			temp = temp.split('.');
			temp = temp[0];
			if(temp <= 6) {
				var oImage = $(obj);
				if(!oImage.length)  return;
				for(var i=0;i<oImage.length;i++) {
					with(oImage[i].style) {
						background = 'none';
						filter = 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' + oImage[i].src + '", sizingMethod="image")';
					}
					oImage[i].src = 'img/px.gif';
				}
			}
		}
	},
	widthProg:function(){
		$('.boite_contenu_defil').each( function () {
			var largeur_tab_debug = ($(this).find('tr td').length)/2 * 160;
			$(this).css('width', largeur_tab_debug+'px');
		});
	},
	afficheScrollDiffSerie:function(){
		$('.serie #annexe dl dt a').click(function(){


			$('.serie #annexe dl').css('padding','10px 20px 0');
			$('.serie #annexe dl').css('height','140px');
			$('.serie #annexe dl').css('width','auto');
			if($('.serie #annexe div.jScrollPaneTrack').lenght>0){

				$('.serie #annexe div.jScrollPaneTrack').each(function(){
					$(this).remove();
				});
			}
			var old_list = $('.serie #annexe div.jScrollPaneContainer dl').html();
			var old_list = '<dl style="padding:10px 20px 0;height:140px;width:auto;">'+old_list+'</dl>'

			$('.serie #annexe div.jScrollPaneContainer').remove();
			$('.serie #annexe .conteneurDl').html(old_list);
			var a_afficher = $(this).attr('rel');
			$('.serie #annexe dl dd').each( function () {
				if($(this).attr('rel')==a_afficher){
					if($(this).css('display')=='block'){
						$(this).css('display','none');
					}else{
						if($(this).css('display')=='none'){
							$(this).css('display','block');
						}
					}
				}
			});


			$('.serie #annexe dl').jScrollPane({scrollbarOnLeft:true, showArrows:false});

			$('.serie #annexe dl').css('padding','10px 20px 0');

			scripts.afficheScrollDiffSerie();
			return false;
		});
	},
	conteneurBoxSerie:function(){
		if($("#comboSeries").html()) {
			if(!($('.conteneurValeurListe').html())) {
				$("#comboSeries").combobox({  
					comboboxContainerClass: "conteneurListe", 
					comboboxValueContainerClass: "conteneurValeurListe", 
					comboboxValueContentClass: "contenuValeurListe", 
					comboboxDropDownClass: "conteneurDerouleSerie", 
					comboboxDropDownButtonClass: "boutonListe", 
					comboboxDropDownItemClass: "elementListeSerie", 
					comboboxDropDownItemHoverClass: "elementOverListe", 
					comboboxDropDownGroupItemHeaderClass: "elementListe", 
					comboboxDropDownGroupItemContainerClass: "elementListe", 
					animationType: "slide", 
					width: "160px" }
				);
			}
		}
		$('.elementListeSerie').click(function () { 
			var lien_courant = $(this);
			var conteneur = $(this).parent();
			var i = 0;
			var nbli = $(this).parent().find('li').length;
			var find = 0;
			var goodli = 0;
			while(i < nbli && find==0){
				if($(this).parent().find('li:eq('+i+')').html() == lien_courant.html()){
					find=1;
					goodli = i;
				}
				i++;
			}
			goodli = goodli+1;
			goodli = goodli+(parseInt($('#first_seasons').attr('value')))-1;

			afficheSaison(goodli,$('#id_of_serie').attr('value'));
			// Ascenseur perso sur la page S�rie onglet episotheque
			$('#liste_episodes').jScrollPane({scrollbarOnLeft:true, showArrows:true});

		});
		$('#contenu_episodes ul li a').click(function(){
//test
			$('#contenu_episodes ul li').removeClass('select');
			$(this).parent().addClass('select');
			afficheEpisode($(this).attr('rel'));
			var old_desc = $('#contenu_serie_5 p#desc_episode').html();
			old_desc = '<p id="desc_episode" class="scrollPerso left" >'+old_desc+'</p>';
			$('#contenu_serie_5 div.jScrollPaneContainer.first').remove();
			$('#contenu_serie_5 h4:first').after(old_desc);
			$('#desc_episode').jScrollPane({scrollbarOnLeft:true, showArrows:true});
			$('#contenu_serie_5 .jScrollPaneContainer:first').addClass('first');

			return false;
		});
		$('#frmEpisodes, #frmEpisodes *').css('z-index', '999999');
		$('.jScrollPaneTrack').css('z-index', '2');
	},
	videoSanders:function(){
		if($("#liste_video_creations").html()) {
			$("#liste_video_creations").combobox({  
				comboboxContainerClass: "conteneurListe", 
				comboboxValueContainerClass: "conteneurValeurListe", 
				comboboxValueContentClass: "contenuValeurListe", 
				comboboxDropDownClass: "conteneurDerouleSerie", 
				comboboxDropDownButtonClass: "boutonListe", 
				comboboxDropDownItemClass: "elementListeSanders", 
				comboboxDropDownItemHoverClass: "elementOverListe", 
				comboboxDropDownGroupItemHeaderClass: "elementListe", 
				comboboxDropDownGroupItemContainerClass: "elementListe", 
				animationType: "slide", 
				width: "254px" }
			);
			$('#form_liste_videos_creations .conteneurDerouleSerie').css('width', '215px');
		$('.elementListeSanders').click(function(){
			var temp = $(this).html();

			if(temp=="Teaser"){
				displayVotes('creations/bcls/webboxes/videoBox?film=2','video');
			}else{
				if(temp=="Bande Annonce"){
					displayVotes('creations/bcls/webboxes/videoBox?film=1','video');
				}
			}
		});

		}
	},
videoSaints:function(){
		if($("#liste_video_creations_saints").html()) {
			$("#liste_video_creations_saints").combobox({  
				comboboxContainerClass: "conteneurListe", 
				comboboxValueContainerClass: "conteneurValeurListe", 
				comboboxValueContentClass: "contenuValeurListe", 
				comboboxDropDownClass: "conteneurDerouleSerie", 
				comboboxDropDownButtonClass: "boutonListe", 
				comboboxDropDownItemClass: "elementListeSaints", 
				comboboxDropDownItemHoverClass: "elementOverListe", 
				comboboxDropDownGroupItemHeaderClass: "elementListe", 
				comboboxDropDownGroupItemContainerClass: "elementListe", 
				animationType: "slide", 
				width: "254px" }
			);
			$('#form_liste_videos_creations .conteneurDerouleSerie').css('width', '215px');
		$('.elementListeSaints').click(function(){
			var temp = $(this).html();

			if(temp=="Extrait 1"){
				displayVotes('creations/nous-ne-sommes-pas-des-saints/web/videoBox?film=1','contenu_lecture_en_cours');
			}else{
				if(temp=="Extrait 2"){
					displayVotes('creations/nous-ne-sommes-pas-des-saints/web/videoBox?film=2','contenu_lecture_en_cours');
				}else{
				if(temp=="Extrait 3"){
					displayVotes('creations/nous-ne-sommes-pas-des-saints/web/videoBox?film=3','contenu_lecture_en_cours');
				}
				}
			}
		});

		}
	},videoMax:function(){
		if($("#liste_video_creations_max").html()) {
			$("#liste_video_creations_max").combobox({  
				comboboxContainerClass: "conteneurListe", 
				comboboxValueContainerClass: "conteneurValeurListe", 
				comboboxValueContentClass: "contenuValeurListe", 
				comboboxDropDownClass: "conteneurDerouleSerie", 
				comboboxDropDownButtonClass: "boutonListe", 
				comboboxDropDownItemClass: "elementListeMax", 
				comboboxDropDownItemHoverClass: "elementOverListe", 
				comboboxDropDownGroupItemHeaderClass: "elementListe", 
				comboboxDropDownGroupItemContainerClass: "elementListe", 
				animationType: "slide", 
				width: "254px" }
			);
			$('#form_liste_videos_creations .conteneurDerouleSerie').css('width', '215px');
		$('.elementListeMax').click(function(){
			var temp = $(this).html();

			if(temp=="Extrait 1"){
				displayVotes('creations/max_les_veut_toutes/web/videoBox?film=1','video');
			}else{
				if(temp=="Extrait 2"){
					displayVotes('creations/max_les_veut_toutes/web/videoBox?film=2','video');
				}else{
				if(temp=="Extrait 3"){
					displayVotes('creations/max_les_veut_toutes/web/videoBox?film=3','video');
				}else{
					if(temp=="Extrait 4"){
						displayVotes('creations/max_les_veut_toutes/web/videoBox?film=4','video');
					}
				}
				}
			}
		});

		}
	},videoFontenay:function(){
		if($("#liste_video_creations_fontenay").html()) {
			$("#liste_video_creations_fontenay").combobox({  
				comboboxContainerClass: "conteneurListe", 
				comboboxValueContainerClass: "conteneurValeurListe", 
				comboboxValueContentClass: "contenuValeurListe", 
				comboboxDropDownClass: "conteneurDerouleSerie", 
				comboboxDropDownButtonClass: "boutonListe", 
				comboboxDropDownItemClass: "elementListeFontenay", 
				comboboxDropDownItemHoverClass: "elementOverListe", 
				comboboxDropDownGroupItemHeaderClass: "elementListe", 
				comboboxDropDownGroupItemContainerClass: "elementListe", 
				animationType: "slide", 
				width: "254px" }
			);
			$('#form_liste_videos_creations .conteneurDerouleSerie').css('width', '215px');
		$('.elementListeFontenay').click(function(){
			var temp = $(this).html();

			if(temp=="Sommaire"){
				displayVotes('creations/grosse_emission_de_fontenay/webboxes/videoBox?film=1','contenu_lecture_en_cours');
			}else{
				if(temp=="Extrait 1"){
					displayVotes('creations/grosse_emission_de_fontenay/webboxes/videoBox?film=2','contenu_lecture_en_cours');
				}else{
					if(temp=="Extrait 2"){
						displayVotes('creations/grosse_emission_de_fontenay/webboxes/videoBox?film=3','contenu_lecture_en_cours');
					}else{
						if(temp=="Extrait 3"){
							displayVotes('creations/grosse_emission_de_fontenay/webboxes/videoBox?film=4','contenu_lecture_en_cours');
						}
					}
				}
			}
		});

		}
	},



	videoEmission:function(){
		if($("#liste_video_creations_emission").html()) {
			$("#liste_video_creations_emission").combobox({  
				comboboxContainerClass: "conteneurListe", 
				comboboxValueContainerClass: "conteneurValeurListe", 
				comboboxValueContentClass: "contenuValeurListe", 
				comboboxDropDownClass: "conteneurDerouleSerie", 
				comboboxDropDownButtonClass: "boutonListe", 
				comboboxDropDownItemClass: "elementListeEmission", 
				comboboxDropDownItemHoverClass: "elementOverListe", 
				comboboxDropDownGroupItemHeaderClass: "elementListe", 
				comboboxDropDownGroupItemContainerClass: "elementListe", 
				animationType: "slide", 
				width: "254px" }
			);
			$('#form_liste_videos_creations .conteneurDerouleSerie').css('width', '215px');
		$('.elementListeEmission').click(function(){
			var temp = $(this).html();
			if(temp=="Sommaire"){
				displayVotes('creations/grosse_emission/webboxes/videoBox?film=1','video');
			}else{
				if(temp=="Extrait 1"){
					displayVotes('creations/grosse_emission/webboxes/videoBox?film=2','video');
				}else{
					if(temp=="Extrait 2"){
						displayVotes('creations/grosse_emission/webboxes/videoBox?film=3','video');
					}else{
						if(temp=="Extrait 3"){
							displayVotes('creations/grosse_emission/webboxes/videoBox?film=4','video');
						}else{
							if(temp=="Marianne James"){
								displayVotes('creations/grosse_emission/webboxes/videoBox?film=5','video');
							}
						}
					}
				}
			}
		});

		}
	},
	videoMerci:function(){
		if($("#liste_video_creations_merci").html()) {
			$("#liste_video_creations_merci").combobox({  
				comboboxContainerClass: "conteneurListe", 
				comboboxValueContainerClass: "conteneurValeurListe", 
				comboboxValueContentClass: "contenuValeurListe", 
				comboboxDropDownClass: "conteneurDerouleSerie", 
				comboboxDropDownButtonClass: "boutonListe", 
				comboboxDropDownItemClass: "elementListeMerci", 
				comboboxDropDownItemHoverClass: "elementOverListe", 
				comboboxDropDownGroupItemHeaderClass: "elementListe", 
				comboboxDropDownGroupItemContainerClass: "elementListe", 
				animationType: "slide", 
				width: "254px" }
			);
			$('#form_liste_videos_creations .conteneurDerouleSerie').css('width', '215px');
		$('.elementListeMerci').click(function(){
			var temp = $(this).html();
			if(temp=="Extrait N�2"){
				displayVotes('creations/merci/webboxes/videoBox?film=2','video');
			}else{
				if(temp=="Extrait N�1"){
					displayVotes('creations/merci/webboxes/videoBox?film=1','video');
				}
			}
		});

		}
	},
	videoManu:function(){
		if($("#liste_video_creations_manu_bruno").html()) {
			$("#liste_video_creations_manu_bruno").combobox({  
				comboboxContainerClass: "conteneurListe", 
				comboboxValueContainerClass: "conteneurValeurListe", 
				comboboxValueContentClass: "contenuValeurListe", 
				comboboxDropDownClass: "conteneurDerouleSerie", 
				comboboxDropDownButtonClass: "boutonListe", 
				comboboxDropDownItemClass: "elementListeManu", 
				comboboxDropDownItemHoverClass: "elementOverListe", 
				comboboxDropDownGroupItemHeaderClass: "elementListe", 
				comboboxDropDownGroupItemContainerClass: "elementListe", 
				animationType: "slide", 
				width: "254px" }
			);
			$('#form_liste_videos_creations .conteneurDerouleSerie').css('width', '215px');
		$('.elementListeManu').click(function(){
			var temp = $(this).html();
			if(temp=="Fausses Pubs"){
				displayVotes('creations/manu_et_bruno_sont_show/webboxes/videoBox?film=4','video');
			}else{
				if(temp=="Imitation perso"){
					displayVotes('creations/manu_et_bruno_sont_show/webboxes/videoBox?film=3','video');
				}else{
					if(temp=="Micro Trottoir"){
						displayVotes('creations/manu_et_bruno_sont_show/webboxes/videoBox?film=2','video');
					}else{
						if(temp=="Acceuil"){
							displayVotes('creations/manu_et_bruno_sont_show/webboxes/videoBox?film=1','video');
						}
					}
				}
			}
		});

		}
	},
             videoBravo:function(){
		if($("#liste_video_creations_bravo").html()) {
			$("#liste_video_creations_bravo").combobox({  
				comboboxContainerClass: "conteneurListe", 
				comboboxValueContainerClass: "conteneurValeurListe", 
				comboboxValueContentClass: "contenuValeurListe", 
				comboboxDropDownClass: "conteneurDerouleSerie", 
				comboboxDropDownButtonClass: "boutonListe", 
				comboboxDropDownItemClass: "elementListeBravo", 
				comboboxDropDownItemHoverClass: "elementOverListe", 
				comboboxDropDownGroupItemHeaderClass: "elementListe", 
				comboboxDropDownGroupItemContainerClass: "elementListe", 
				animationType: "slide", 
				width: "254px" }
			);
			$('#form_liste_videos_creations .conteneurDerouleSerie').css('width', '215px');
		$('.elementListeBravo').click(function(){
			var temp = $(this).html();

			if(temp=="Bande-annonce"){
				displayVotes('creations/Eh_bien_bravo/webboxes/videoBox?film=1','contenu_lecture_en_cours');
			}else{
				if(temp=="Betisier"){
					displayVotes('creations/Eh_bien_bravo/webboxes/videoBox?film=2','contenu_lecture_en_cours');
				}else{
					if(temp=="Extrait 2"){
						displayVotes('creations/Eh_bien_bravo/webboxes/videoBox?film=3','contenu_lecture_en_cours');
					}else{
						if(temp=="Extrait 3"){
							displayVotes('creations/Eh_bien_bravo/webboxes/videoBox?film=4','contenu_lecture_en_cours');
						}
					}
				}
			}
		});

		}
	},
	videoNousAppartient:function(){
		if($("#liste_video_creations_nous_appartient").html()) {
			$("#liste_video_creations_nous_appartient").combobox({  
				comboboxContainerClass: "conteneurListe", 
				comboboxValueContainerClass: "conteneurValeurListe", 
				comboboxValueContentClass: "contenuValeurListe", 
				comboboxDropDownClass: "conteneurDerouleSerie", 
				comboboxDropDownButtonClass: "boutonListe", 
				comboboxDropDownItemClass: "elementListeNousAppartient", 
				comboboxDropDownItemHoverClass: "elementOverListe", 
				comboboxDropDownGroupItemHeaderClass: "elementListe", 
				comboboxDropDownGroupItemContainerClass: "elementListe", 
				animationType: "slide", 
				width: "254px" }
			);
			$('#form_liste_videos_creations .conteneurDerouleSerie').css('width', '215px');
		$('.elementListeNousAppartient').click(function(){
			var temp = $(this).html();

			if(temp=="Bande-annonce 2")
				displayVotes('creations/la_nuit_nous_appartient/webboxes/videoBox?film=2','contenu_lecture_en_cours');
			else if(temp=="Bande-annonce 3")
				displayVotes('creations/la_nuit_nous_appartient/webboxes/videoBox?film=3','contenu_lecture_en_cours');
			else if(temp=="Bande-annonce 1")
				displayVotes('creations/la_nuit_nous_appartient/webboxes/videoBox?film=1','contenu_lecture_en_cours');
		});

		}
	},
         videoTimsit:function(){
		if($("#liste_video_creations_timsit").html()) {
			$("#liste_video_creations_timsit").combobox({  
				comboboxContainerClass: "conteneurListe", 
				comboboxValueContainerClass: "conteneurValeurListe", 
				comboboxValueContentClass: "contenuValeurListe", 
				comboboxDropDownClass: "conteneurDerouleSerie", 
				comboboxDropDownButtonClass: "boutonListe", 
				comboboxDropDownItemClass: "elementListeTimsit", 
				comboboxDropDownItemHoverClass: "elementOverListe", 
				comboboxDropDownGroupItemHeaderClass: "elementListe", 
				comboboxDropDownGroupItemContainerClass: "elementListe", 
				animationType: "slide", 
				width: "254px" }
			);
			$('#form_liste_videos_creations .conteneurDerouleSerie').css('width', '215px');
		$('.elementListeTimsit').click(function(){
			var temp = $(this).html();

			if(temp=="Bande-annonce 2")
				displayVotes('creations/grosse_emission_timsit/webboxes/videoBox?film=2','contenu_lecture_en_cours');
			else if(temp=="Bande-annonce 3")
				displayVotes('creations/grosse_emission_timsit/webboxes/videoBox?film=3','contenu_lecture_en_cours');
			else if(temp=="Bande-annonce 1")
				displayVotes('creations/grosse_emission_timsit/webboxes/videoBox?film=1','contenu_lecture_en_cours');
		});

		}
	},
               videoPublivores:function(){
		if($("#liste_video_creations_Publivores").html()) {
			$("#liste_video_creations_Publivores").combobox({  
				comboboxContainerClass: "conteneurListe", 
				comboboxValueContainerClass: "conteneurValeurListe", 
				comboboxValueContentClass: "contenuValeurListe", 
				comboboxDropDownClass: "conteneurDerouleSerie", 
				comboboxDropDownButtonClass: "boutonListe", 
				comboboxDropDownItemClass: "elementListePublivores", 
				comboboxDropDownItemHoverClass: "elementOverListe", 
				comboboxDropDownGroupItemHeaderClass: "elementListe", 
				comboboxDropDownGroupItemContainerClass: "elementListe", 
				animationType: "slide", 
				width: "254px" }
			);
			$('#form_liste_videos_creations .conteneurDerouleSerie').css('width', '215px');
		$('.elementListePublivores').click(function(){
			var temp = $(this).html();

			if(temp=="Bande-annonce 2")
				displayVotes('creations/publivores/webboxes/videoBox?film=2','contenu_lecture_en_cours');
			else if(temp=="Bande-annonce 1")
				displayVotes('creations/publivores/webboxes/videoBox?film=1','contenu_lecture_en_cours');
		});

		}
	},

 videoQcmm:function(){
		if($("#liste_video_creations_qcmmm").html()) {
			$("#liste_video_creations_qcmmm").combobox({  
				comboboxContainerClass: "conteneurListe", 
				comboboxValueContainerClass: "conteneurValeurListe", 
				comboboxValueContentClass: "contenuValeurListe", 
				comboboxDropDownClass: "conteneurDerouleSerie", 
				comboboxDropDownButtonClass: "boutonListe", 
				comboboxDropDownItemClass: "elementListeQcmm", 
				comboboxDropDownItemHoverClass: "elementOverListe", 
				comboboxDropDownGroupItemHeaderClass: "elementListe", 
				comboboxDropDownGroupItemContainerClass: "elementListe", 
				animationType: "slide", 
				width: "254px" }
			);
			$('#form_liste_videos_creations .conteneurDerouleSerie').css('width', '215px');
		$('.elementListeQcmm').click(function(){
			var temp = $(this).html();

			if(temp=="Teaser"){
				displayVotes('creations/le_qcmmm/webboxes/videoBox?film=1','contenu_lecture_en_cours');
			}else{
				if(temp=="Bande Annonce 1"){
					displayVotes('creations/le_qcmmm/webboxes/videoBox?film=2','contenu_lecture_en_cours');
				}else{
					if(temp=="Bande Annonce 2"){
						displayVotes('creations/le_qcmmm/webboxes/videoBox?film=3','contenu_lecture_en_cours');
					}
				}
				
			}
		});

		}
	},
	     videoArthur:function(){
		if($("#liste_video_creations_arthur").html()) {
			$("#liste_video_creations_arthur").combobox({  
				comboboxContainerClass: "conteneurListe", 
				comboboxValueContainerClass: "conteneurValeurListe", 
				comboboxValueContentClass: "contenuValeurListe", 
				comboboxDropDownClass: "conteneurDerouleSerie", 
				comboboxDropDownButtonClass: "boutonListe", 
				comboboxDropDownItemClass: "elementListeArthur", 
				comboboxDropDownItemHoverClass: "elementOverListe", 
				comboboxDropDownGroupItemHeaderClass: "elementListe", 
				comboboxDropDownGroupItemContainerClass: "elementListe", 
				animationType: "slide", 
				width: "254px" }
			);
			$('#form_liste_videos_creations .conteneurDerouleSerie').css('width', '215px');
		$('.elementListeArthur').click(function(){
			var temp = $(this).html();

			if(temp=="Bande-annonce")
				displayVotes('creations/grosse_emission_arthur/webboxes/videoBox?film=1','contenu_lecture_en_cours');
			else
				displayVotes('creations/grosse_emission_arthur/webboxes/videoBox?film=1','contenu_lecture_en_cours');
		});

		}
	},

            videoSoireeArthur:function(){
		if($("#liste_video_creations_soiree_arthur").html()) {
			$("#liste_video_creations_soiree_arthur").combobox({  
				comboboxContainerClass: "conteneurListe", 
				comboboxValueContainerClass: "conteneurValeurListe", 
				comboboxValueContentClass: "contenuValeurListe", 
				comboboxDropDownClass: "conteneurDerouleSerie", 
				comboboxDropDownButtonClass: "boutonListe", 
				comboboxDropDownItemClass: "elementListeSoireeArthur", 
				comboboxDropDownItemHoverClass: "elementOverListe", 
				comboboxDropDownGroupItemHeaderClass: "elementListe", 
				comboboxDropDownGroupItemContainerClass: "elementListe", 
				animationType: "slide", 
				width: "254px" }
			);
			$('#form_liste_videos_creations .conteneurDerouleSerie').css('width', '215px');
		$('.elementListeSoireeArthur').click(function(){
			var temp = $(this).html();

			if(temp=="Bande-annonce")
				displayVotes('creations/soiree_arthur/webboxes/videoBox?film=1','contenu_lecture_en_cours');
			else if(temp=="Bande-annonce 1")
				displayVotes('creations/soiree_arthur/webboxes/videoBox?film=1','contenu_lecture_en_cours');
		});

		}
	},
videoDrolesDeJobs:function(){
		if($("#liste_video_creations_droles_de_jobs").html()) {
			$("#liste_video_creations_droles_de_jobs").combobox({  
				comboboxContainerClass: "conteneurListe", 
				comboboxValueContainerClass: "conteneurValeurListe", 
				comboboxValueContentClass: "contenuValeurListe", 
				comboboxDropDownClass: "conteneurDerouleSerie", 
				comboboxDropDownButtonClass: "boutonListe", 
				comboboxDropDownItemClass: "elementListeDrolesDeJobs", 
				comboboxDropDownItemHoverClass: "elementOverListe", 
				comboboxDropDownGroupItemHeaderClass: "elementListe", 
				comboboxDropDownGroupItemContainerClass: "elementListe", 
				animationType: "slide", 
				width: "254px" }
			);
			$('#form_liste_videos_creations .conteneurDerouleSerie').css('width', '215px');
		$('.elementListeDrolesDeJobs').click(function(){
			var temp = $(this).html();

			if(temp=="Bande-annonce")
				displayVotes('creations/droles_de_jobs/webboxes/videoBox?film=1','contenu_lecture_en_cours');
			else if(temp=="Bande-annonce 1")
				displayVotes('creations/droles_de_jobs/webboxes/videoBox?film=1','contenu_lecture_en_cours');
		});
		}
	},
	appli_combo_serie: function () {
		if($("#comboSeries").html()) {
			if(!($('.conteneurValeurListe').html())) {
				$("#comboSeries").combobox({  
					comboboxContainerClass: "conteneurListe", 
					comboboxValueContainerClass: "conteneurValeurListe", 
					comboboxValueContentClass: "contenuValeurListe", 
					comboboxDropDownClass: "conteneurDerouleSerie", 
					comboboxDropDownButtonClass: "boutonListe", 
					comboboxDropDownItemClass: "elementListeSerie", 
					comboboxDropDownItemHoverClass: "elementOverListe", 
					comboboxDropDownGroupItemHeaderClass: "elementListe", 
					comboboxDropDownGroupItemContainerClass: "elementListe", 
					animationType: "slide", 
					width: "160px" }
				);
			}
		}
		$('.elementListeSerie').click(function () {
			var lien_courant = $(this);
			var conteneur = $(this).parent();
			var i = 0;
			var nbli = $(this).parent().find('li').length;
			var find = 0;
			var goodli = 0;
			while(i < nbli && find==0){
				if($(this).parent().find('li:eq('+i+')').html() == lien_courant.html()){
					find=1;
					goodli = i;
				}
				i++;
			}
			goodli = goodli+1;
			goodli = goodli+(parseInt($('#first_seasons').attr('value')))-1;
			afficheSaison(goodli,$('#id_of_serie').attr('value'));

			// Ascenseur perso sur la page S�rie onglet episotheque
			//$('#liste_episodes').jScrollPane({scrollbarOnLeft:true, showArrows:true});
		});
		$('#frmEpisodes, #frmEpisodes *').css('z-index', '999999');
		$('.jScrollPaneTrack').css('z-index', '2');
	}

}

function ouvrirRss(){
	var largeur = 500;
	var hauteur = 400;
	var top = (screen.height - hauteur) /2;
	var left = (screen.width - largeur) /2;
	var w = window.open($("#logo_rss").attr("href"), "rss", "top="+top+",left="+left+",width="+largeur+",height="+hauteur+",scrollbars=1,resizable=1");
	w.focus();
	//return false;

}
$(document).ready(scripts.init);