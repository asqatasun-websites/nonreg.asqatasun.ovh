function schedule(start, end, headline){
	this.start = start;
	this.end = end;
	this.headline = headline;
}

var nextShow = ""; var laterShow = "";
var radioArray = new Array();

// Sunday
radioArray[0] = new Array();
radioArray[0][0] = new schedule(0,59,"FOX Report Sunday");
radioArray[0][1] = new schedule(100,159,"FOX News Talk");
radioArray[0][2] = new schedule(200,229,"FOX News Watch");
radioArray[0][3] = new schedule(230,259,"Beltway Boys");
radioArray[0][4] = new schedule(300,359,"America's News Headquarters ");
radioArray[0][5] = new schedule(400,459,"FOX Report Sunday");
radioArray[0][6] = new schedule(500,529,"Bulls &amp; Bears");
radioArray[0][7] = new schedule(530,559,"Cavuto on Business");
radioArray[0][8] = new schedule(600,629,"Forbes on FOX");
radioArray[0][9] = new schedule(630,659,"Cashin' In");
radioArray[0][10] = new schedule(700,959,"Best of Brian &amp; The Judge");
radioArray[0][11] = new schedule(1000,1029,"FOX News Watch");
radioArray[0][12] = new schedule(1030,1059,"Editorial");
radioArray[0][13] = new schedule(1100,1129,"Bulls &amp; Bears");
radioArray[0][14] = new schedule(1130,1159,"Cavuto on Business");
radioArray[0][15] = new schedule(1200,1229,"Forbes on FOX");
radioArray[0][16] = new schedule(1230,1259,"Cashin' In");
radioArray[0][17] = new schedule(1300,1359,"FOX News Sunday");
radioArray[0][18] = new schedule(1400,1659,"Best of John Gibson");
radioArray[0][19] = new schedule(1700,1759,"FOX News Sunday");
radioArray[0][20] = new schedule(1800,2059,"Best of FOX Across America");
radioArray[0][21] = new schedule(2100,2159,"FOX News Sunday");
radioArray[0][22] = new schedule(2200,2359,"The Best of The Alan Colmes Show");


// Monday
radioArray[1] = new Array();
radioArray[1][0] = new schedule(0,59,"The Best of The Alan Colmes Show");
radioArray[1][1] = new schedule(100,129,"Bulls &amp; Bears");
radioArray[1][2] = new schedule(130,159,"Cavuto on Business");
radioArray[1][3] = new schedule(200,229,"Forbes on FOX");
radioArray[1][4] = new schedule(230,259,"Cashin' In");
radioArray[1][5] = new schedule(300,359,"FOX NEWS TALK");
radioArray[1][6] = new schedule(400,459,"FOX Report Sunday");
radioArray[1][7] = new schedule(500,559,"FOX News Sunday");
radioArray[1][8] = new schedule(600,659,"FOX NEWS TALK");
radioArray[1][9] = new schedule(700,729,"Bulls &amp; Bears");
radioArray[1][10] = new schedule(730,759,"Cavuto on Business");
radioArray[1][11] = new schedule(800,829,"Forbes on FOX");
radioArray[1][12] = new schedule(830,859,"Cashin' In");
radioArray[1][13] = new schedule(900,1159,"Brian &amp; The Judge<br/>Call in! 1-866-408-7669");
radioArray[1][14] = new schedule(1200,1359,"The John Gibson Show<br/>Call in! 1-888-788-9910");
radioArray[1][15] = new schedule(1500,1759,"Tom Sullivan<br/>Call in! 1-866-859-2788");
radioArray[1][16] = new schedule(1800,1959,"FOX Across America<br>Call in! 1-866-868-6861");
radioArray[1][17] = new schedule(2000,2059,"Glenn Beck");
radioArray[1][18] = new schedule(2100,2159,"The O'Reilly Factor");
radioArray[1][19] = new schedule(2200,2359,"The Alan Colmes Show<br/>Call in! 1-877-FOR-ALAN");

// Tuesday, Wednesday, Thursday, Friday
radioArray[2] = new Array();
radioArray[2][0] = new schedule(0,59,"The Alan Colmes Show<br/>Call in! 1-877-FOR-ALAN");
radioArray[2][1] = new schedule(100,359,"The Alan Colmes Show Encore");
radioArray[2][2] = new schedule(400,459,"Your World with Neil Cavuto");
radioArray[2][3] = new schedule(500,559,"Special Report");
radioArray[2][4] = new schedule(600,659,"OReilly Factor");
radioArray[2][5] = new schedule(700,759,"Hannity");
radioArray[2][6] = new schedule(800,859,"On the Record");
radioArray[2][7] = new schedule(900,1159,"Brian &amp; The Judge<br>Call in! 1-866-408-7669");
radioArray[2][8] = new schedule(1200,1459,"The John Gibson Show<br>Call in! 1-888-788-9910");
radioArray[2][9] = new schedule(1500,1759,"Tom Sullivan<br/>Call in! 1-866-859-2788");
radioArray[2][10] = new schedule(1800,1959,"FOX Across America<br>Call in! 1-866-868-6861");
radioArray[2][11] = new schedule(2000,2059,"Glenn Beck");
radioArray[2][12] = new schedule(2100,2159,"The O'Reilly Factor");
radioArray[2][13] = new schedule(2200,2359,"The Alan Colmes Show<br/>Call in! 1-877-FOR-ALAN");

// Saturday
radioArray[3] = new Array();
radioArray[3][0] = new schedule(0,59,"The Alan Colmes Show<br/>Call in! 1-877-FOR-ALAN");
radioArray[3][1] = new schedule(100,359,"The Alan Colmes Show Encore");
radioArray[3][2] = new schedule(400,459,"Your World with Neil Cavuto");
radioArray[3][3] = new schedule(500,559,"Special Report");
radioArray[3][4] = new schedule(600,659,"The O'Reilly Factor");
radioArray[3][5] = new schedule(700,759,"Hannity");
radioArray[3][6] = new schedule(800,859,"On the Record");
radioArray[3][7] = new schedule(900,959,"Best of Brian &amp; The Judge");
radioArray[3][8] = new schedule(1000,1359,"Fox Business Network LIVE");
radioArray[3][9] = new schedule(1400,1459,"Best of John Gibson");
radioArray[3][10] = new schedule(1500,1759,"Tom Sullivan Replay");
radioArray[3][11] = new schedule(1800,2059,"Best of Fox Across America");
radioArray[3][12] = new schedule(2100,2129,"Beltway Boys");
radioArray[3][13] = new schedule(2130,2159,"FOX News Watch");
radioArray[3][14] = new schedule(2200,2259,"America's News Headquarters");
radioArray[3][15] = new schedule(2300,2359,"FOX News Talk"); 
function returnArray(d){
	switch(d){
		case 0: return radioArray[0]; break;
		case 1: return radioArray[1]; break;
		case 6: return radioArray[3]; break;
		default: return radioArray[2];
	}
}

function writeStreamHeadline(t, d){
	var o = "Listen Live!";
	var currentTime = 1442;
	var currentDay = 1;
	var dayArray = returnArray(currentDay);
	for(var i=0;i<dayArray.length;i++){
		if(currentTime >= dayArray[i].start && currentTime <= dayArray[i].end){
			o = "Now &#151; " + dayArray[i].headline;
			if((i + 1) >= dayArray.length){
				var tempDay = currentDay == 6 ? 0 : currentDay + 1;
				var tempArray = returnArray(tempDay);
				var startNum = 0;
				if(tempArray[startNum].headline == dayArray[i].headline){ startNum++; }
				nextShow = tempArray[startNum];
				laterShow = tempArray[startNum + 1];
			} else {
				nextShow = dayArray[i+1];
				if((i + 2) >= dayArray.length){
					var tempDay = currentDay == 6 ? 0 : currentDay + 1;
					var tempArray = returnArray(tempDay);
					laterShow = tempArray[0].headline == nextShow.headline ? tempArray[1] : tempArray[0];
				} else {
					laterShow = dayArray[i+2];
				}
			}
		}
	}
	return o;
}

function createTime(t){
	var str;
	if(t > 1259){
		str = (t-1200) + "pm";
	} else {
		var time = t < 100 ? "12"+t.toString() : t;
		var suffix = t >= 1200 ? "pm" : "am";
		str = time + suffix;
	}
	str = str.length == 5 ? str.substring(0,1) + ":" + str.substring(1,5) : str.substring(0,2) + ":" + str.substring(2,6)
	return str.replace(":00","");
}

function writeStreamSchedule(){
	nextShow.headline = nextShow.headline.split('<br/>')[0];
	laterShow.headline = laterShow.headline.split('<br/>')[0];
	var s = "<strong>Next &#151; " + createTime(nextShow.start) + ":</strong> " + nextShow.headline + "<br/>";
	s += "<strong>Later &#151; " + createTime(laterShow.start) + ":</strong> " + laterShow.headline;
	return s;
}
