
function zoomActu(){
	zoomPhoto=document.getElementById('zoom_photo');
	if(zoomPhoto.style.display=='none')
	{
		zoomPhoto.style.display='';
	}
	else
	{
		zoomPhoto.style.display='none';
	}
}

function ExportPdf(){
	window.location.href='/web/php/exportPDF.php?url='+window.location.href;
}


/*****************************************
	* insertion d'un flash
	*****************************************/
	function insertFlash(strSrc, intWidth, intHeight, strAlt, strID ){
	    var strFlash = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"' +
	                    'width="' + intWidth + '" height="' + intHeight + '" title="' + strAlt + '"' + (strID != '' ? ' id="' + strID + '"' : '') + '>' +
	                    '<param name="movie" value="' + strSrc + '" />'+
	                    '<param name="quality" value="high" />'+
	                    '<param name="menu" value="false" />'+
	                    '<embed src="' + strSrc + '" width="' + intWidth + '" height="' + intHeight + '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" menu="false" wmode="transparent"' + (strID != '' ? ' name="' + strID + '"' : '') + ' swLiveConnect="true"></embed>'+
	                    '</object>';
	    document.write(strFlash);
	    document.close();
	}



/* Style Switcher */



function setActiveStyleSheet(title) {
  var i, a, main;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {

    if(a.getAttribute("href") && a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title")) {
      a.disabled = true;
      if(a.getAttribute("title") == title) a.disabled = false;
    }
  }
}
function getActiveStyleSheet() {
  var i, a;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title") && !a.disabled) return a.getAttribute("title");
  }
  return null;
}
function getPreferredStyleSheet() {
  var i, a;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("rel").indexOf("style") != -1
       && a.getAttribute("rel").indexOf("alt") == -1
       && a.getAttribute("title")
       ) return a.getAttribute("title");
  }
  return null;
}
function createCookie(name,value,days) {
  if (days) {
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    var expires = "; expires="+date.toGMTString();
  }
  else expires = "";
  document.cookie = name+"="+value+expires+"; path=/";
}
function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}
function SuperOnLoad(){
	setHover();
	var cookie = readCookie("style");
	var title = cookie ? cookie : getPreferredStyleSheet();
	setActiveStyleSheet(title);
	
}
window.onload = function(e) { SuperOnLoad();}
/*window.onload = function(e) {
  var cookie = readCookie("style");
  var title = cookie ? cookie : getPreferredStyleSheet();
  setActiveStyleSheet(title);
}*/
window.onunload = function(e) {
  var title = getActiveStyleSheet();
  createCookie("style", title, 365);
}
var cookie = readCookie("style");
var title = cookie ? cookie : getPreferredStyleSheet();
setActiveStyleSheet(title);


/* Style Switcher */


var TailleEnCours=title;
function TaillePlus(){
	if(TailleEnCours!=5){		
		TailleEnCours++;
		setActiveStyleSheet(TailleEnCours);
	}
}
function TailleMoins(){
	if(TailleEnCours!=1){		
		TailleEnCours--;
		setActiveStyleSheet(TailleEnCours);
	}
}


/* Envoyer � un ami */

function validationSendToAFriend(){
	var msgError;
	var param;
	var paire;
	var idx;
	msgError = "";

	if(document.getElementById('txtNom').value==""){
		msgError+="- le nom de l'exp�diteur\n";
		document.getElementById('spanNom').style.color="red";
	}
	else{
		document.getElementById('spanNom').style.color="#000";
	}

	if(document.getElementById('txtPrenom').value==""){
		msgError+="- le pr�nom de l'exp�diteur\n";
		document.getElementById('spanPrenom').style.color="red";
	}
	else{
		document.getElementById('spanPrenom').style.color="#000";
	}

	if(document.getElementById('txtNomB').value==""){
		msgError+="- le nom du destinataire\n";
		document.getElementById('spanNomB').style.color="red";
	}
	else{
		document.getElementById('spanNomB').style.color="#000";
	}

	if(document.getElementById('txtPrenomB').value==""){
		msgError+="- le pr�nom du destinataire\n";
		document.getElementById('spanPrenomB').style.color="red";
	}
	else{
		document.getElementById('spanPrenomB').style.color="#000";
	}

	if(document.getElementById('txtEmailB').value==""){
		msgError+="- l'email du destinataire\n";
		document.getElementById('spanEmailB').style.color="red";
	}
	else{
		document.getElementById('spanEmailB').style.color="#000";
	}

	if (msgError != "") {
			alert("les champs suivants sont obligatoires : \n\n"+msgError);
	}
	else{
		
		if (!VerifMail(document.getElementById('txtEmailB').value)){
			alert("L'e-mail du destinataire n'est pas correct");
		}
		else{
			document.getElementById('formSendToAFriend').submit();
		}
	}

}

function VerifMail(adresse)
{
	var place = adresse.indexOf("@",1);
	var point = adresse.indexOf(".",place+1);
	if ((place > -1)&&(adresse.length >2)&&(point > 1))
	{
		return(true);
	}
	else
	{
		return(false);
	}
}

function ValidationNewsletterGauche(){
	if(VerifMail(document.getElementById('tbMailNews').value))
		document.getElementById('frmNewsletterMaster').submit();
	else
		alert('Veuillez saisir une adresse mail valide.');
}