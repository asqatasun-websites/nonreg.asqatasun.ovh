/* ONLOAD FUNCTION */

function addLoadEvent(func) { // Manage Load Event
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            oldonload();
            func();
        }
    }
}


/* BROWSER DETECT */

var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},

		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{	// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 	// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();

function getElementsByClass(searchClass,node,tag) {
	var classElements = new Array();
	if ( node == null )
		node = document;
	if ( tag == null )
		tag = '*';
	var els = node.getElementsByTagName(tag);
	var elsLen = els.length;
	var pattern = new RegExp("(^|\\s)"+searchClass+"(\\s|$)");
	for (i = 0, j = 0; i < elsLen; i++) {
		if ( pattern.test(els[i].className) ) {
			classElements[j] = els[i];
			j++;
		}
	}
	return classElements;
}

function insertAfter(newElement, targetElement) {
	var parent = targetElement.parentNode;
	if (parent.lastChild == targetElement) {
		parent.appendChild(newElement);
	}
	else {
		parent.insertBefore(newElement, targetElement.nextSibling);
	}
}

function buttonEndings() {
	if (!document.getElementsByTagName) {
		return false
	}
	if (BrowserDetect.browser == "Safari") {
	var arInputButton = document.getElementsByTagName("input");
	for (var i=0; i<arInputButton.length; i++){
		if (arInputButton[i].className == "button") {
			arInputButton[i].className = "";
		}
		if (arInputButton[i].className == "csc-mailform-submit") {
			arInputButton[i].className = "buttonSafari";
		}
	}
	}
		
	var buttons = getElementsByClass("button");
	for (i=0; i < buttons.length; i++) {
		var div = document.createElement("div");
		div.className = "buttonEnding";
		insertAfter(div, buttons[i]);
	}
	
	var buttonsFormTemplate = getElementsByClass("csc-mailform-submit");
	for (i=0; i < buttonsFormTemplate.length; i++) {
		var div = document.createElement("div");
		div.className = "buttonEnding";
		insertAfter(div, buttonsFormTemplate[i]);
	}
}

function setActive(id) {
	 if (id == "simpleFormContent") { // RECHERCHE SIMPLE
		document.getElementById('simpleSearchTitle').className = 'activeSearch';
		document.getElementById('advancedSearchTitle').className = 'inactiveSearch';
		document.getElementById('simpleFormContent').style.display = 'block';
		document.getElementById('advancedFormContent').style.display = 'none';
	 } else { // RECHERCHE AVANCEE
		document.getElementById('simpleSearchTitle').className = 'inactiveSearch';
		document.getElementById('advancedSearchTitle').className = 'activeSearch';
		document.getElementById('simpleFormContent').style.display = 'none';
		document.getElementById('advancedFormContent').style.display = 'block';	 
	 }
 }