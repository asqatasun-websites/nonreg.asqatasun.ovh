
/* ------------------------------------------------------------
   string helper function
   ------------------------------------------------------------ */
function is_undefined(str)
{
	if(typeof str == 'undefined' || str == 'undefined')
		return true;
	return false;
}


function resizeObjectTo(name,width,height)
{
    var _object = document.getElementById(name);
    
    if(width != undefined)_object.style.width = width;
    
    if(height != undefined)_object.style.height = height;
}

function scrollTop()
{
    window.scroll(0,0);
}

/* --------------------------------------------------------------------
   Check the return url to ensure it's in the list of allowed domains
   -------------------------------------------------------------------- */
function checkReturnUrl(url){
	  var returnUrl = "";
	    
    //List of allowed domains seperated by a '|' - ie (nike.com|test.com)
	  var allowedDomains = "(nike.com)";
    
    var domainRegex = new RegExp("^http[s]?://[^/]*\\." + allowedDomains.replace(".", "\\.") + "(?:/.*|\\?.*|$)", "i");
    var decodedUrl = unescape(url);
    //console.log(decodedUrl);
    var match = decodedUrl.match(domainRegex);
    if(match){
        returnUrl = match[0];
    }
    return returnUrl;  
}

/* ------------------------------------------------------------
   Querystring and hash helper functions
   ------------------------------------------------------------ */
//var queryVars = new Object();
//var qloc = "";

function parseQuery( p_qstr ) 
{
	var queryVars = {};
	
	if (!is_undefined(p_qstr))
	{
		var pairHalves;
		var N_V_pairs = p_qstr.split("&");
		var l = N_V_pairs.length;
		for(i=0; i<l; i++){
			pairHalves = N_V_pairs[i].split("=");
      if(pairHalves[0] == "returnURL"){
        //Validate return url
        if(!checkReturnUrl(pairHalves[1])){
          //Set default returnURL to http://store.nike.com - this should only happen if a restricted or invalid url is passed
          queryVars[pairHalves[0]] = "http://store.nike.com";
          continue;
        }
      }
			queryVars[pairHalves[0]] = pairHalves[1];
			for(d=2;d<pairHalves.length;d++) {
				queryVars[pairHalves[0]] += "=" + pairHalves[d];
			}
		}
	}
	
	return queryVars;
}


function buildQString( p_queryVars )
{
	var results = "";
	for( var item in p_queryVars )
	{
		if ( results.length > 0 )
		{
			results += "&";
		}
	
		if ( item.length > 0 )
		{
			results += item + "=" + p_queryVars[ item ];
		}
	}
	
	return results;
}

function cleanupQString( p_qString )
{
	if(!is_undefined(p_qString))
	{
		if( p_qString.indexOf(";") > -1 ) 
		{
			var temp_q_str = p_qString ;
			p_qString = temp_q_str.substring( 0, p_qString.indexOf(";") );
		}
		
		// remove the question mark
		p_qString = p_qString.substring( 1 );
	} 
	else 
	{
		p_qString = "";
	}
	
	return p_qString;
}


/* ------------------------------------------------------------
   Used for getting the users browser
   ------------------------------------------------------------ */

// browser detection
var _appName = navigator.appName;
var _appVersion = navigator.appVersion;
var _userAgent = navigator.userAgent.toLowerCase();

function getBrowser() {
      
      var browser = '';
      
      if ((_appName.indexOf('Microsoft') != -1) && (_userAgent.indexOf('mac') == -1)) {
		  browser = 'IE';
	  // We're treating MAC IE as a special case anymore
	  } else if ((_appName.indexOf('Microsoft') != -1) && (_userAgent.indexOf('mac') != -1)) {		
	      browser = 'Other';
	  } else if (_userAgent.indexOf('safari') != -1) {
		  browser = 'Safari';
	  } else if (_userAgent.indexOf('firefox') != -1) {
		  browser = 'Firefox';
	  } else {
		  browser = 'Other';
	  }
	  return browser;
}

/* ------------------------------------------------------------
   Cookie helper functions
   ------------------------------------------------------------ */
function setCookie(name, value, path, domain) {
	   var today = new Date();
	   var expires = new Date();
	   expires.setTime( today.getTime() + 3600000 * 24 * 365);
	   var curCookie = name + "=" + escape(value) +
		  ((expires) ? "; expires=" + expires.toGMTString() : "") +
		  ((path) ? "; path=" + path : "") +
		  ((domain) ? "; domain=" + domain : "");
	   document.cookie = curCookie;
 } 
 
 function setCookieExp(name, value, path, domain, expInDays) {
	   var today = new Date();
	   var expires = new Date();
	   expires.setTime( today.getTime() + 3600000 * 24 * expInDays);
	   var curCookie = name + "=" + escape(value) +
		  ((expires) ? "; expires=" + expires.toGMTString() : "") +
		  ((path) ? "; path=" + path : "") +
		  ((domain) ? "; domain=" + domain : "");
	   document.cookie = curCookie;
 } 
 
 function deleteCookie(name, path, domain) {
 		var today = new Date();
 		var expires = new Date();
 		expires.setTime (today.getTime() - (360000 * 24 * 365));
 		var curCookie = name + "=" + escape("") +
 		((expires) ? "; expires=" + expires.toGMTString() : "") +
		  ((path) ? "; path=" + path : "") +
		  ((domain) ? "; domain=" + domain : "");
		  document.cookie = curCookie;
}

function getCookie( name ) {
	var allCookies = document.cookie.split( ';' );
	var tempCookie = '';
	var cookieName = '';
	var cookieValue = '';
	var cookieFound = false; // set boolean t/f default f
	
	for ( i = 0; i < allCookies.length; i++ ){
		tempCookie = allCookies[i].split( '=' );
		cookieName = tempCookie[0].replace(/^\s+|\s+$/g, '');
		if ( cookieName == name ){
			cookieFound = true;
			// we need to handle case where cookie has no value but exists (no = sign, that is):
			if ( tempCookie.length > 1 ){
				cookieValue = unescape( tempCookie[1].replace(/^\s+|\s+$/g, '') );
			}
			return cookieValue;
			break;
		}
		tempCookie = null;
		cookieName = '';
	}
	
	if ( !cookieFound ){
		return null;
	}
}

function getCookieDomain()
{
	var hostname = location.hostname;
	
	if (hostname.indexOf(".nike.com") >= 0)
	{
		return ".nike.com";
	}
	else
	{
		return "";
	}
}

/* ------------------------------------------------------------
   Used for determining if we are on the live site
   ------------------------------------------------------------ */
function isLiveSite() 
{
	var hostname = location.hostname;

	if ((hostname.indexOf("nike-dev2") >= 0)||
	(hostname.indexOf("env") >= 0)||
	(hostname.indexOf("ecn") >= 0))
	{
		return false;
	}
	else 
	{
		return true;
	}
}

/* ------------------------------------------------------------
   Helper function for creating pixel tracking tags
   ------------------------------------------------------------ */
function createPixelTagAsImage(pixelURL) {
	img = new Image();
	img.src = pixelURL;
}

function createPixelTagAsIframe(pixelURL, identifier)
{
	var tmpDiv = null;

	if(document.getElementById(identifier+"Div"))
	{
		tmpDiv=document.getElementById(identifier+"Div");
	}
	else
	{
		tmpDiv=document.body.appendChild(document.createElement("div"));
		tmpDiv.setAttribute("id",identifier+"Div");
		tmpDiv.style.position="absolute";
		tmpDiv.style.visibility="hidden";
		tmpDiv.style.top="0px";
	}
	
	tmpDiv.innerHTML='<iframe id="'+identifier+'Iframe" src="'+pixelURL+'"></iframe>';
}

function collectGarbage()
{
	if (typeof CollectGarbage != 'undefined') {
		//This only works in IE
		CollectGarbage();
		return true;
	}else{
		//alert("garbageCollection failed");
		return false;
	}
}	

function getScrollXY() {
  var scrOfX = 0, scrOfY = 0;
  if( typeof( window.pageYOffset ) == 'number' ) {
    //Netscape compliant
    scrOfY = window.pageYOffset;
    scrOfX = window.pageXOffset;
  } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
    //DOM compliant
    scrOfY = document.body.scrollTop;
    scrOfX = document.body.scrollLeft;
  } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
    //IE6 standards compliant mode
    scrOfY = document.documentElement.scrollTop;
    scrOfX = document.documentElement.scrollLeft;
  }
  return {scrollX: scrOfX, scrollY: scrOfY};
}

function getViewableSize()
{
	 var viewportwidth;
	 var viewportheight;
	 
	 // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
	 
	 if (typeof window.innerWidth != 'undefined')
	 {
	      viewportwidth = window.innerWidth,
	      viewportheight = window.innerHeight
	 }
	 
// IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)

	 else if (typeof document.documentElement != 'undefined'
	     && typeof document.documentElement.clientWidth !=
	     'undefined' && document.documentElement.clientWidth != 0)
	 {
	       viewportwidth = document.documentElement.clientWidth,
	       viewportheight = document.documentElement.clientHeight
	 }
	 
	 // older versions of IE
	 
	 else
	 {
	       viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
	       viewportheight = document.getElementsByTagName('body')[0].clientHeight
	 }
	 
	 return {width: viewportwidth, height: viewportheight };
}
