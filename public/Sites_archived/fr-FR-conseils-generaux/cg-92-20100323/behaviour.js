$(document).ready(function() {
	

	$("#sommaire>ul>li>a").each(function(){
		if($(this).parents("li").find("ul").length)
			$(this).html($(this).html()+" (+)");
		else
		{
			$(this).css("background-image","url(../img/pictos/chevrons-orange.gif)");
		}
		
		if ($("#sommaire>ul>li.actif>a")!=null && ($("#sommaire>ul>li.actif>a")).html()!=null)
		{
			$("#sommaire>ul>li.actif>a").html($("#sommaire>ul>li.actif>a").html().replace("(+)","(-)"));
		}
		
		$(this).click(function(){
			if($(this).parents("li").attr("class") != "actif")
			{
				$("#sommaire>ul>li").removeClass("actif");
				$("#sommaire>ul>li>a").each(function(){
					$(this).html($(this).html().replace("(-)","(+)"));
				});
				$(this).html($(this).html().replace("(+)","(-)"));
				$(this).parents("li").addClass("actif");
			}
			else
			{
				$(this).html($(this).html().replace("(-)","(+)"));
				$("#sommaire>ul>li").removeClass("actif");
			}
			$(this).blur();
			return(false);
		});
	});
	
	
	
	
	if($("#diaporama").length && $("#diaporama #photos li").length > 1)
	{
		$("#diaporama #photos li:first").show();
		var ext;
		var sep;
		if($("#portfolio").length)
		{
			ext = "jpg";
			sep = "";
		}
		else
		{
			ext = "png";		
			sep = "&nbsp;&ndash;";
		}
		$("#diaporama #photos").after('<ul id="menu"><li><a id="precedent" href="#"><img src="img/pictos/precedent.'+ext+'" alt="Photo pr&eacute;c&eacute;dente" title="" /></a></li></ul>');
		var i = 1;
		$("#diaporama #photos li").each(function(){
			$(this).attr("id","photo_"+i);
			if(i == 1)
				$("#diaporama #menu").append('<li><a class="actif roll photo" href="#photo_'+i+'">'+i+'</a></li>');
			else
				$("#diaporama #menu").append('<li>'+sep+' <a class="roll photo" href="#photo_'+i+'">'+i+'</a></li>');
			i++;
		});
		if($("#portfolio").length)
		{
			$("#portfolio #menu a.photo").each(function(){
				$(this).append('<span><img src="'+ $( $(this).attr("href").substring($(this).attr("href").indexOf("#"),$(this).attr("href").length) ).find("img").attr("src").replace(".jpg","_small.jpg") +'" alt="" title="" /></span>');
			});
			$("#portfolio #menu a.photo").hover(function(){
				$(this).children("span").show();
			},function(){
				$(this).children("span").hide();			
			});
		}
		$("#diaporama #menu").append('<li><a id="suivant" href="#"><img src="img/pictos/suivant.'+ext+'"alt="Photo suivante" title="" /></a></li>');	
		$("#diaporama #menu a.photo").click(function(){
			var lienClique = $(this);
			$("#diaporama #photos li:visible").fadeOut("slow",function(){
				$(lienClique.attr("href").substring($(lienClique).attr("href").indexOf("#"),$(lienClique).attr("href").length)).fadeIn();				
			});
			$("#diaporama #menu a").removeClass("actif");
			$(this).addClass("actif");
			$(this).blur();
			return(false);
		});
		$("#suivant").click(function(){
			var lienClique = $(this);
			var photoCourante = $("#diaporama #photos li:visible").attr("id");
			var nbPhotos = $("#diaporama #photos li").length;
			var photoSuivante;
			if(photoCourante == "photo_"+nbPhotos)
				photoSuivante = 1;
			else
				photoSuivante = parseInt(photoCourante.replace("photo_","")) + 1;
			$("#diaporama #menu a").removeClass("actif");
			$("#diaporama #menu a").each(function(){
				if($(this).attr("href").substring($(this).attr("href").indexOf("#"),$(this).attr("href").length) == "#photo_"+photoSuivante)
					$(this).addClass("actif");
			});
			$("#diaporama #photos li:visible").fadeOut("slow",function(){
				$("#photo_"+photoSuivante).fadeIn();				
			});
			$(this).blur();
			return(false);		
		});
		$("#precedent").click(function(){
			var photoCourante = $("#diaporama #photos li:visible").attr("id");
			var nbPhotos = $("#diaporama #photos li").length;
			var photoPrecedente;
			if(photoCourante == "photo_1")
				photoPrecedente = nbPhotos;
			else
				photoPrecedente = parseInt(photoCourante.replace("photo_","")) - 1;
			$("#diaporama #menu a").removeClass("actif");
			$("#diaporama #menu a").each(function(){
				if($(this).attr("href").substring($(this).attr("href").indexOf("#"),$(this).attr("href").length) == "#photo_"+photoPrecedente)
					$(this).addClass("actif");
			});
			$("#diaporama #photos li:visible").fadeOut("slow",function(){
				$("#photo_"+photoPrecedente).fadeIn();				
			});
			$(this).blur();
			return(false);		
		});
	}
	else
	{
		$("#diaporama #photos li:first").show();
	}

	$("#content-entete-menu>li>a").mouseover(function(){
		menuHide();
		$("#content-entete-menu>li").css("z-index","1");
		$(this).parents("li").css("z-index","500");		
		$(this).parent().parent().find("div").hide();
		if($(this).next().is("div") == true) {
			$(this).next("div").show();	
			$(this).next("div").css("z-index","600");	
		}
	});
	$("#content-entete-menu>li>a").focus(function(){
		menuHide();
		$("#content-entete-menu>li").css("z-index","1");
		$(this).parents("li").css("z-index","500");		
		$(this).parent().parent().find("div").hide();
		if($(this).next().is("div") == true) {
			$(this).next("div").show();		
			$(this).next("div").css("z-index","600");	
		}
	});
	function menuHide(){
		$("html").mouseover(function(o){
			o = o.target;
			while(o.parentNode) {
				if(o.id == "content-entete-menu") {
					break;
				}
				if(o.nodeName == "HTML") {
					$("#content-entete-menu div").hide();
					$("html").unbind();
				}
				o = o.parentNode;
			}
		});
	}
	
	if($.browser.msie)
	{
		if($("#menu_4 a.on").length)
		{
			$("#menu_4 div").css("left","9px");
		}
		if($("#menu_6 a.on").length)
		{
			$("#menu_6 div").css("left","-5px");
		}
		if($("#menu_3 a.on").length)
		{
			$("#menu_3 div").css("left","-5px");
		}
		if($("#menu_1 a.on").length)
		{
			$("#menu_1 div").css("left","10px");
		}
	}
	
	
	if($("#affichage").length)
	{
		$("#affichage select").change(function(){
		
			$(this).parents("form").submit();
			return(false);
		});
	}
	// compte google - identifiant : hauts.de.seine.net / mot de passe : 9292929292
	if($("#carte").length)
	{
		$("#carte").prepend('<iframe width="340" height="210" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.fr/maps/ms?hl=fr&amp;ie=UTF8&amp;s=AARTsJpKDTxKB94A2vmkNUW8iN5FEQy-NQ&amp;msa=0&amp;msid=108236716764441474719.00045544ba029481cabb2&amp;ll=48.819976,2.290649&amp;spn=0.094942,0.233459&amp;z=11&amp;output=embed"></iframe>');
	}
	if($("#tri").length)
	{
		$("#tri select").change(function(){
		
			$(this).parents("form").submit();
			return(false);
		});
	}

	if($("#outils").length)
	{
		$("#outils").prepend(''+
			'<li id="diminuer"><a class="roll" href="#"><img src="img/pictos/a_moins.png" alt="Diminuer la taille du texte" title="Diminuer la taille du texte" /></a></li>'+
			'<li id="augmenter"><a class="roll" href="#"><img src="img/pictos/a_plus.png" alt="Augmenter la taille du texte" title="Augmenter la taille du texte" /></a></li>'+
		'');
		$("#envoyer").after(''+
			'<li id="imprimer"><a class="roll" href="#">Imprimer</a></li>'+
		'');
		$("#imprimer").click(function(){
			window.print();
			this.blur();
			return(false);
		});
		$("body").addClass("taille_10");
		$("#diminuer").click(function(){
			var taille = $("body").attr("class");
			var future_taille = parseInt(taille.replace("taille_","")) - 1; 
			if(future_taille > 7)
				$("body").removeClass(taille).addClass("taille_" + future_taille);
			this.blur();
			return(false);
		});
		$("#augmenter").click(function(){
			var taille = $("body").attr("class");
			var future_taille = parseInt(taille.replace("taille_","")) + 1; 
			if(future_taille < 13)
				$("body").removeClass(taille).addClass("taille_" + future_taille);
			this.blur();
			return(false);
		});
	}

	if($("#video").length)
	{
		$("#video").flash({
			width:220,
			height:190,
			wmode:"transparent",
			flashvars:{
				allowfullscreen:true
			},
			src:"http://www.youtube.com/v/Bj1-kiPkAMY&amp;hl=fr&amp;fs=1"},{
				version:8
			}
		);
	}
	if($(".video").length)
	{
		$(".video").flash({
			width:340,
			height:280,
			wmode:"transparent",
			flashvars:{
				allowfullscreen:true
			}},{
				version:8
			},function(htmlOptions){
            $this = $(this);
            htmlOptions.src = $this.children("a").attr("href");
            $this.html($.fn.flash.transform(htmlOptions));						
        }

		);
	}
	if($("#audio_1").length)
	{
		$("#audio_1").flash({
			width:200,
			height:20,
			src:"/portal/templates/YeeYRZZYbcXfdTZcWefXSVWTbWUbTRca/ZbbRQQYfXSRefWacWefXSVWTbWUbTRca/dewplayer.swf?mp3=" + $("#audio_1").attr("href") + "&amp;showtime=1"},{
				version:8
			}
		);
	}
	if($("#audio_2").length)
	{
		$("#audio_2").flash({
			width:200,
			height:20,
			src:"/portal/templates/YeeYRZZYbcXfdTZcWefXSVWTbWUbTRca/ZbbRQQYfXSRefWacWefXSVWTbWUbTRca/dewplayer.swf?mp3=" + $("#audio_2").attr("href") + "&amp;showtime=1"},{
				version:8
			}
		);
	}
	
	$(".actus-defilantes-nav-h img,.actus-defilantes-nav img").hover(function(){
		$(this).attr("src",$(this).attr("src").replace("_off","_on"));
	},function(){
		$(this).attr("src",$(this).attr("src").replace("_on","_off"));		
	});

	$(".accordeon .titre-bulle span").hover(function(){
		$(this).addClass("hover");
	},function(){
		$(this).removeClass("hover");
	});
	
	/* Box Acc�s */
	$("#entete-acces").addClass("js");
	$("#entete-acces li").removeClass("on");
	$(".entete-acces-content").addClass("none");
	
	$("#entete-acces-outils").addClass("on");
	$("#entete-acces-outils .entete-acces-content").removeClass("none");
	//$("#entete-acces-membre").addClass("on");
	//$("#entete-acces-membre .entete-acces-content").removeClass("none");
	
	// AJOUT SOPRA 30/01/2009 : champ recherche accessible sous ie6
	jQuery.each(jQuery.browser, function(i) {
	  if($.browser.msie){
	     $("#entete-acces-outils").css("z-index","50");
		$("#entete-acces-membre").css("z-index","6");
	  }
	});
	
	function AccesOnglets(e) {
		$("#entete-acces li").removeClass("on");
		$(".entete-acces-content").addClass("none");
		$(e).parent("p").parent("li").addClass("on");
		// AJOUT SOPRA 30/01/2009 : champ recherche accessible sous ie6
		jQuery.each(jQuery.browser, function(i) {
		  if($.browser.msie){
			$("#entete-acces-outils").css("z-index","6");
			$("#entete-acces-membre").css("z-index","6");
			$(e).parent("p").parent("li").css("z-index","50");		
		  }
		});			
		$(e).parent("p").next(".entete-acces-content").removeClass("none");
	}
	$(".entete-acces-onglet a").click(function(){ AccesOnglets($(this)); return false; });
	$(".entete-acces-onglet a").focus(function(){ AccesOnglets($(this)) });
	
	
	/* Logo - Retour Accueil */
	$("#content-entete-logo h1 a").hover(function(){
		$(this).find("span").animate({opacity:100}, "slow");
	}, function(){
		$(this).find("span").animate({opacity:0}, "fast");
	});
	
	
	// Pr�remplissage des champs
	$("#membre-identifiant, #membre-password").each(function() {
		t = $(this).prev("label").text();
		$(this).attr("value", t.substring(0, t.length-2));
		$(this).focus(function() {
			$(this).attr("value", "");
			$(this).unbind();
		});
	});
	
	
	// Formulaire de choix
	//$(".form-choix-select").change(function() {
		//$(this).parent("p").parent("form").submit();		
	//});
	
	// Formulaire de sondage
//	$(".form-sondage ul input").click(function() {
//		$(this).parent("li").parent("ul").parent("form").submit();
//	});
//	Mise en commentaire PLA : on gere nous m�me la validation du form

	$("#documents select").change(function() {
		$(this).parents("form").submit();
	});

	
	
	// Accord�on
	$(".accordeon-content").each(function() {
		if($(this).prev("h2").attr("class").indexOf("titre-bulle-pointe") < 0) {
			$(this).hide();
		}
	});
	$(".accordeon h2").click(function() {
		// Animation
		$(".accordeon-content").slideUp();
		$(this).next("div").slideDown();
		// Initialisation des classes
		$(".accordeon h2").removeClass("titre-bulle-pointe");
		$(".accordeon h2").removeClass("titre-bulle-grisfonce");
		$(".accordeon h2").addClass("titre-bulle-grisclair");
		// Ajout des classes statut ouvert
		$(this).addClass("titre-bulle-pointe");
		$(this).removeClass("titre-bulle-grisclair");
		$(this).addClass("titre-bulle-grisfonce");
	});
	


if (jQuery.browser.msie)
{
// Actualit�s d�filantes
	NbActusV = 4;
	NbActusH = 3;
	minheightforactusdefilantesV = 60;
	cptV = cptH = new Array();
	s = new Array();
	
	$(".actus-defilantes-content-v").each(function(r) {
				
		// Identifiant num�rique
		$(this).parent().addClass("js");
		$(this).addClass("DFLV-"+r);
		
		i = cptV[r] = 0;
		s[r] = new Array();
		// Stockage des hauteurs de chaques items
		$(this).find("ul.actus-defilantes-liste li").each(function() {
			s[r][i] = $(this).height() + 1;			
			i++;
		});		
		
		// DEBUG SOPRA : on compte le nombre d'items, si moins que NbActusV IL FAUT EN TENIR COMPTE !
		realNbActusV = 0;
		$(this).find("ul.actus-defilantes-liste li").each(function() {
			realNbActusV++;
		});		
		
		if (realNbActusV > NbActusV)
			realNbActusV = NbActusV;
			
		// Fit � la hauteur des 4 items
		H = 0;		
		for(i=(realNbActusV-1); i>=0; i--) {		
			H = H + s[r][i];				
		}
		if (H < minheightforactusdefilantesV)
			H = minheightforactusdefilantesV;		
		
		$(this).height(H+16);
	});
	
	
	
	
	$(".actus-defilantes-content-h").each(function(r) {
		// Identifiant num�rique
		$(this).parent().addClass("js");
		$(this).addClass("DFLH-"+r);
		// Compteur
		cptH[r] = 0;
		// Largeur des items
		ItLg = $(this).find("ul.actus-defilantes-liste li").width();
		// Adaptaion de la largeur du conteneur
		$(this).find("ul.actus-defilantes-liste").width( (ItLg + 2) * $(this).find("ul.actus-defilantes-liste li").length + 100 );
	});
	
$(".actus-defilantes-nav-down").click(function() {

		
		// Identifiant num�rique
		var c = $(this).parent().parent().attr("class");
		var r = c.match(/DFLV\-[0-9]*$/);
		r = r[0].substring(5, 99);
			
		
				
		if( ($(this).parent().parent().find("ul.actus-defilantes-liste li").length - NbActusV) > cptV[r]) {
			
			// Calcul de l'animation en pixel
			m = 0;
			for(i=cptV[r]; i>=0; i--) {
				m = m + s[r][i];
			}

			cptV[r]++;
			
			// Fit � la hauteur des 4 items
			i = cptV[r];
			H = 0;
			for(i=(NbActusV+cptV[r]-1); i>=cptV[r]; i--) {
				H = eval(H + s[r][i]);
			}
			H = H+16; 
			
			// Animation
			$(this).parent().parent(".actus-defilantes-content-v").animate({ height:H });
			$(this).parent().parent().find(".actus-defilantes-liste").animate({ marginTop:-(m) });
			
			//RG EA-CE-03
	        if($(this).parent().parent().find("ul.actus-defilantes-liste li").length-cptV[r]==NbActusV){
	                 	$(this).css("visibility","hidden"); // on cache la fleche defilement basse			
	        }	
			
			$(this).parent().find(".actus-defilantes-nav-up").css("visibility","visible");	
		}
		
	});
	$(".actus-defilantes-nav-up").click(function() {
	
		// Identifiant num�rique
		var c = $(this).parent().parent().attr("class");
		var r = c.match(/DFLV\-[0-9]*$/);
		r = r[0].substring(5, 99);
		
		if(cptV[r] > 0) {
			
			// Calcul de l'animation en pixel
			m = 0;
			for(i= (cptV[r]-1); i>0; i--) {
				m = m + s[r][(i-1)];
			}
			cptV[r]--;
			
			// Fit � la hauteur des 4 items
			i = cptV[r];
			H = 0;
			for(i=(NbActusV+cptV[r]-1); i>=cptV[r]; i--) {
				H = H + s[r][i];
			}
			H = H+16; 
			
			// Animation
			$(this).parent().parent(".actus-defilantes-content-v").animate({ height:H });
			$(this).parent().parent().find(".actus-defilantes-liste").animate({ marginTop:-(m) });
			
			//RG EA-CE-03
	        if(cptV[r] ==0){
	                 	$(this).css("visibility","hidden"); // on cache la fleche defilement basse			
	        }	
			
			$(this).parent().find(".actus-defilantes-nav-down").css("visibility","visible");	
		}	
		
	});
	
	
	$(".actus-defilantes-nav-right").click(function() {	
		if ($(this).parent().next().find(".actus-defilantes-liste").hasClass("2cols"))		
			NbActusH = 2;
		else
			NbActusH = 3;
		
		// Identifiant num�rique
		var c = $(this).parent().next().attr("class");
		var r = c.match(/DFLH\-[0-9]*$/);
		r = r[0].substring(5, 99);
		
		N = $(this).parent().next().find(".actus-defilantes-liste li").length;		
		if(cptH[r] < (N - NbActusH)) {
			cptH[r]++;
			$(this).parent().next().find(".actus-defilantes-liste").animate({ marginLeft:-(ItLg*cptH[r]) - (cptH[r]*2) });		
			
			if (cptH[r] == (N - NbActusH)) 									
				$(this).css("visibility","hidden"); // on cache la fleche defilement droite			
			
			$(this).parent().find(".actus-defilantes-nav-left").css("visibility","visible");		
		}					
	});
	
	
	$(".actus-defilantes-nav-left").click(function() {		
		if ($(this).parent().next().find(".actus-defilantes-liste").hasClass("2cols"))		
			NbActusH = 2;
		else
			NbActusH = 3;
		
		// Identifiant num�rique
		var c = $(this).parent().next().attr("class");
		var r = c.match(/DFLH\-[0-9]*$/);
		r = r[0].substring(5, 99);
		
		if(cptH[r] > 0) {
			cptH[r]--;
			$(this).parent().next().find(".actus-defilantes-liste").animate({ marginLeft:-(ItLg*cptH[r]) - (cptH[r]*2) });			
			
			if (cptH[r] == 0) 					
				$(this).css("visibility","hidden"); // on cache la fleche defilement droite		
						
			$(this).parent().find(".actus-defilantes-nav-right").css("visibility","visible");	
		}		
	});

	}

/*Actualit�s d�filantes de l'espace presse*/

EpNbActusV = 3;
EpCptV = EpCptH = new Array();
Eps = new Array();

$(".ep-actus-defilantes-content-v").each(function(r) {
	// Identifiant num�rique
	$(this).parent().addClass("js");
	$(this).addClass("DFLV-"+r);
	i = EpCptV[r] = 0;
	Eps[r] = new Array();
	
	// Stockage des hauteurs de chaques items
	$(this).find("ul.actus-defilantes-liste li").each(function() {
		Eps[r][i] = $(this).height() + 1;
		i++;
	});		
	
	// Fit � la hauteur des 3 items
	H = 0;
	for(i=(EpNbActusV-1); i>=0; i--) {
		H = H + Eps[r][i];
	}
	try{
		$(this).height(H-2);
	}
	catch(error){
	}
});
	


$(".ep-actus-defilantes-nav-down").click(function() {
	// Identifiant num�rique
	var c = $(this).parent().parent().attr("class");
	var r = c.match(/DFLV\-[0-9]*$/);
	r = r[0].substring(5, 99);
	
	if( ($(this).parent().parent().find("ul.actus-defilantes-liste li").length - EpNbActusV) > EpCptV[r]) {
		
		// Calcul de l'animation en pixel
		m = 0;
		for(i=EpCptV[r]; i>=0; i--) {
			m = m + Eps[r][i];
		}
		EpCptV[r]++;
		
		// Fit � la hauteur des 3 items
		i = EpCptV[r];
		H = 0;
		for(i=(EpNbActusV+EpCptV[r]-1); i>=EpCptV[r]; i--) {
			H = eval(H + Eps[r][i]);
		}
		H = ($.browser.msie == true) ? H-8 : H-2; 
		
		// Animation
		$(this).parent().parent(".ep-actus-defilantes-content-v").animate({ height:H });
		$(this).parent().parent().find(".actus-defilantes-liste").animate({ marginTop:-(m) });
	}
	
});

$(".ep-actus-defilantes-nav-up").click(function() {
	
	// Identifiant num�rique
	var c = $(this).parent().parent().attr("class");
	var r = c.match(/DFLV\-[0-9]*$/);
	r = r[0].substring(5, 99);
	
	if(EpCptV[r] > 0) {
		
		// Calcul de l'animation en pixel
		m = 0;
		for(i= (EpCptV[r]-1); i>0; i--) {
			m = m + Eps[r][(i-1)];
		}
		EpCptV[r]--;
		
		// Fit � la hauteur des 3 items
		i = EpCptV[r];
		H = 0;
		for(i=(EpNbActusV+EpCptV[r]-1); i>=EpCptV[r]; i--) {
			H = H + Eps[r][i];
		}
		H = ($.browser.msie == true) ? H-8 : H-2; 
		
		// Animation
		$(this).parent().parent(".ep-actus-defilantes-content-v").animate({ height:H });
		$(this).parent().parent().find(".actus-defilantes-liste").animate({ marginTop:-(m) });
	}
	
});


$(".fll").click(function() {
	$(".ep-actus-defilantes-content-v").each(function(r) {
		// Identifiant num�rique
		$(this).parent().addClass("js");
		$(this).addClass("DFLV-"+r);
		
		i = EpCptV[r] = 0;
		Eps[r] = new Array();
		// Stockage des hauteurs de chaques items
		$(this).find("ul.actus-defilantes-liste li").each(function() {
			Eps[r][i] = $(this).height() + 1;
			i++;
		});		
		
		// Fit � la hauteur des 3 items
		H = 0;
		for(i=(EpNbActusV-1); i>=0; i--) {
			H = H + Eps[r][i];
		}
		try{
			$(this).height(H-2);
		}
		catch(error){
	
		}
		
	});
});

$(".flr").click(function() {
	$(".ep-actus-defilantes-content-v").each(function(r) {
		// Identifiant num�rique
		$(this).parent().addClass("js");
		$(this).addClass("DFLV-"+r);
		
		i = EpCptV[r] = 0;
		Eps[r] = new Array();
		// Stockage des hauteurs de chaques items
		$(this).find("ul.actus-defilantes-liste li").each(function() {
			Eps[r][i] = $(this).height() + 1;
			i++;
		});		
		
		// Fit � la hauteur des 3 items
		H = 0;
		for(i=(EpNbActusV-1); i>=0; i--) {
			H = H + Eps[r][i];
		}

		try{
			$(this).height(H-2);
		}
		catch(error){
	
		}
	});
});


/*Fin actualit�s d�filantes de l'espace presse*/
});






if (jQuery.browser.mozilla)
{
$(window).load(function() {


	// Actualit�s d�filantes
	NbActusV = 4;
	NbActusH = 3;
	minheightforactusdefilantesV = 60;
	cptV = cptH = new Array();
	s = new Array();
	
	$(".actus-defilantes-content-v").each(function(r) {
				
		// Identifiant num�rique
		$(this).parent().addClass("js");
		$(this).addClass("DFLV-"+r);
		
		i = cptV[r] = 0;
		s[r] = new Array();
		// Stockage des hauteurs de chaques items
		$(this).find("ul.actus-defilantes-liste li").each(function() {
			s[r][i] = $(this).height() + 1;			
			i++;
		});		
		
		// DEBUG SOPRA : on compte le nombre d'items, si moins que NbActusV IL FAUT EN TENIR COMPTE !
		realNbActusV = 0;
		$(this).find("ul.actus-defilantes-liste li").each(function() {
			realNbActusV++;
		});		
		
		if (realNbActusV > NbActusV)
			realNbActusV = NbActusV;
			
		// Fit � la hauteur des 4 items
		H = 0;		
		for(i=(realNbActusV-1); i>=0; i--) {		
			H = H + s[r][i];				
		}
		if (H < minheightforactusdefilantesV)
			H = minheightforactusdefilantesV;		
		
		$(this).height(H-2);
		
	});
	
	
	
	
	$(".actus-defilantes-content-h").each(function(r) {
		// Identifiant num�rique
		$(this).parent().addClass("js");
		$(this).addClass("DFLH-"+r);
		// Compteur
		cptH[r] = 0;
		// Largeur des items
		ItLg = $(this).find("ul.actus-defilantes-liste li").width();
		// Adaptaion de la largeur du conteneur
		$(this).find("ul.actus-defilantes-liste").width( (ItLg + 2) * $(this).find("ul.actus-defilantes-liste li").length + 100 );
	});
	
$(".actus-defilantes-nav-down").click(function() {
		
		// Identifiant num�rique
		var c = $(this).parent().parent().attr("class");
		var r = c.match(/DFLV\-[0-9]*$/);
		r = r[0].substring(5, 99);
			
		
				
		if( ($(this).parent().parent().find("ul.actus-defilantes-liste li").length - NbActusV) > cptV[r]) {
			
			// Calcul de l'animation en pixel
			m = 0;
			for(i=cptV[r]; i>=0; i--) {
				m = m + s[r][i];
			}
			cptV[r]++;
			
			// Fit � la hauteur des 4 items
			i = cptV[r];
			H = 0;
			for(i=(NbActusV+cptV[r]-1); i>=cptV[r]; i--) {
				H = eval(H + s[r][i]);
			}
			H = ($.browser.msie == true) ? H-8 : H-2; 
			
			// Animation
			$(this).parent().parent(".actus-defilantes-content-v").animate({ height:H });
			$(this).parent().parent().find(".actus-defilantes-liste").animate({ marginTop:-(m) });
			
			//RG EA-CE-03
	        if($(this).parent().parent().find("ul.actus-defilantes-liste li").length-cptV[r]==NbActusV){
	                 	$(this).css("visibility","hidden"); // on cache la fleche defilement basse			
	        }	
			
			$(this).parent().find(".actus-defilantes-nav-up").css("visibility","visible");	
		}
		
	});
	$(".actus-defilantes-nav-up").click(function() {
	
		// Identifiant num�rique
		var c = $(this).parent().parent().attr("class");
		var r = c.match(/DFLV\-[0-9]*$/);
		r = r[0].substring(5, 99);
		
		if(cptV[r] > 0) {
			
			// Calcul de l'animation en pixel
			m = 0;
			for(i= (cptV[r]-1); i>0; i--) {
				m = m + s[r][(i-1)];
			}
			cptV[r]--;
			
			// Fit � la hauteur des 4 items
			i = cptV[r];
			H = 0;
			for(i=(NbActusV+cptV[r]-1); i>=cptV[r]; i--) {
				H = H + s[r][i];
			}
			H = ($.browser.msie == true) ? H-8 : H-2; 
			
			// Animation
			$(this).parent().parent(".actus-defilantes-content-v").animate({ height:H });
			$(this).parent().parent().find(".actus-defilantes-liste").animate({ marginTop:-(m) });
			
			//RG EA-CE-03
	        if(cptV[r] ==0){
	                 	$(this).css("visibility","hidden"); // on cache la fleche defilement basse			
	        }	
			
			$(this).parent().find(".actus-defilantes-nav-down").css("visibility","visible");	
		}	
				
	});
	
	
	$(".actus-defilantes-nav-right").click(function() {	
		if ($(this).parent().next().find(".actus-defilantes-liste").hasClass("2cols"))		
			NbActusH = 2;
		else
			NbActusH = 3;
		
		// Identifiant num�rique
		var c = $(this).parent().next().attr("class");
		var r = c.match(/DFLH\-[0-9]*$/);
		r = r[0].substring(5, 99);
		
		N = $(this).parent().next().find(".actus-defilantes-liste li").length;		
		if(cptH[r] < (N - NbActusH)) {
			cptH[r]++;
			$(this).parent().next().find(".actus-defilantes-liste").animate({ marginLeft:-(ItLg*cptH[r]) - (cptH[r]*2) });		
			
			if (cptH[r] == (N - NbActusH)) 									
				$(this).css("visibility","hidden"); // on cache la fleche defilement droite			
			
			$(this).parent().find(".actus-defilantes-nav-left").css("visibility","visible");		
		}					
	});
	
	
	$(".actus-defilantes-nav-left").click(function() {		
		if ($(this).parent().next().find(".actus-defilantes-liste").hasClass("2cols"))		
			NbActusH = 2;
		else
			NbActusH = 3;
		
		// Identifiant num�rique
		var c = $(this).parent().next().attr("class");
		var r = c.match(/DFLH\-[0-9]*$/);
		r = r[0].substring(5, 99);
		
		if(cptH[r] > 0) {
			cptH[r]--;
			$(this).parent().next().find(".actus-defilantes-liste").animate({ marginLeft:-(ItLg*cptH[r]) - (cptH[r]*2) });			
			
			if (cptH[r] == 0) 					
				$(this).css("visibility","hidden"); // on cache la fleche defilement droite		
						
			$(this).parent().find(".actus-defilantes-nav-right").css("visibility","visible");	
		}		
	});

	/*Actualit�s d�filantes de l'espace presse*/

	EpNbActusV = 3;
	EpCptV = EpCptH = new Array();
	Eps = new Array();

	$(".ep-actus-defilantes-content-v").each(function(r) {
		// Identifiant num�rique
		$(this).parent().addClass("js");
		$(this).addClass("DFLV-"+r);
		i = EpCptV[r] = 0;
		Eps[r] = new Array();
		
		// Stockage des hauteurs de chaques items
		$(this).find("ul.actus-defilantes-liste li").each(function() {
			Eps[r][i] = $(this).height() + 1;
			i++;
		});		
		
		// Fit � la hauteur des 3 items
		H = 0;
		for(i=(EpNbActusV-1); i>=0; i--) {
			H = H + Eps[r][i];
		}
		try{
			$(this).height(H-2);
		}
		catch(error){
		}
	});
		


	$(".ep-actus-defilantes-nav-down").click(function() {
		// Identifiant num�rique
		var c = $(this).parent().parent().attr("class");
		var r = c.match(/DFLV\-[0-9]*$/);
		r = r[0].substring(5, 99);
		
		if( ($(this).parent().parent().find("ul.actus-defilantes-liste li").length - EpNbActusV) > EpCptV[r]) {
			
			// Calcul de l'animation en pixel
			m = 0;
			for(i=EpCptV[r]; i>=0; i--) {
				m = m + Eps[r][i];
			}
			EpCptV[r]++;
			
			// Fit � la hauteur des 3 items
			i = EpCptV[r];
			H = 0;
			for(i=(EpNbActusV+EpCptV[r]-1); i>=EpCptV[r]; i--) {
				H = eval(H + Eps[r][i]);
			}
			H = ($.browser.msie == true) ? H-8 : H-2; 
			
			// Animation
			$(this).parent().parent(".ep-actus-defilantes-content-v").animate({ height:H });
			$(this).parent().parent().find(".actus-defilantes-liste").animate({ marginTop:-(m) });
		}
		
	});

	$(".ep-actus-defilantes-nav-up").click(function() {
		
		// Identifiant num�rique
		var c = $(this).parent().parent().attr("class");
		var r = c.match(/DFLV\-[0-9]*$/);
		r = r[0].substring(5, 99);
		
		if(EpCptV[r] > 0) {
			
			// Calcul de l'animation en pixel
			m = 0;
			for(i= (EpCptV[r]-1); i>0; i--) {
				m = m + Eps[r][(i-1)];
			}
			EpCptV[r]--;
			
			// Fit � la hauteur des 3 items
			i = EpCptV[r];
			H = 0;
			for(i=(EpNbActusV+EpCptV[r]-1); i>=EpCptV[r]; i--) {
				H = H + Eps[r][i];
			}
			H = ($.browser.msie == true) ? H-8 : H-2; 
			
			// Animation
			$(this).parent().parent(".ep-actus-defilantes-content-v").animate({ height:H });
			$(this).parent().parent().find(".actus-defilantes-liste").animate({ marginTop:-(m) });
		}
		
	});


	$(".fll").click(function() {
		$(".ep-actus-defilantes-content-v").each(function(r) {
			// Identifiant num�rique
			$(this).parent().addClass("js");
			$(this).addClass("DFLV-"+r);
			
			i = EpCptV[r] = 0;
			Eps[r] = new Array();
			// Stockage des hauteurs de chaques items
			$(this).find("ul.actus-defilantes-liste li").each(function() {
				Eps[r][i] = $(this).height() + 1;
				i++;
			});		
			
			// Fit � la hauteur des 3 items
			H = 0;
			for(i=(EpNbActusV-1); i>=0; i--) {
				H = H + Eps[r][i];
			}
			try{
				$(this).height(H-2);
			}
			catch(error){
		
			}
			
		});
	});

	$(".flr").click(function() {
		$(".ep-actus-defilantes-content-v").each(function(r) {
			// Identifiant num�rique
			$(this).parent().addClass("js");
			$(this).addClass("DFLV-"+r);
			
			i = EpCptV[r] = 0;
			Eps[r] = new Array();
			// Stockage des hauteurs de chaques items
			$(this).find("ul.actus-defilantes-liste li").each(function() {
				Eps[r][i] = $(this).height() + 1;
				i++;
			});		
			
			// Fit � la hauteur des 3 items
			H = 0;
			for(i=(EpNbActusV-1); i>=0; i--) {
				H = H + Eps[r][i];
			}

			try{
				$(this).height(H-2);
			}
			catch(error){
		
			}
		});
	});


	/*Fin actualit�s d�filantes de l'espace presse*/



	$('#mycarousel a').click(function(e){
		var bigUrl = $(this).attr('href');
		$('#big').attr('src', bigUrl);
		return false;
	});

});
}


/* test package */