// ie pseudo : hover hack
// derived from alistapart 
startList = function() {
	if (document.all&&document.getElementById) {
		divRoot = document.getElementById("menutop");
		for (i=0; i<divRoot.childNodes.length; i++) {
			node = divRoot.childNodes[i];
			if (node.nodeName=="UL") {
				navRoot	= node;
				for (y=0; y<navRoot.childNodes.length; y++) {
					node = navRoot.childNodes[y];
					if (node.nodeName=="LI") {
						node.onmouseover=function() {
							this.className+=" over";
						}
						node.onmouseout=function() {
							this.className=this.className.replace(" over", "");
						}
					}
				}
			}
		}
	}
}