$(document).ready(function() {
	// ============
	// = shopping =
	// ============

		function roundNumber(num, dec) {
			var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
			result = result.toFixed(2);
			return result;
		}

		// $('.prix_ht').each(function(){	
		// 	        var prix_ht = parseFloat($(this).text().replace(',', '.'));
		// 	        var prix_ttc = roundNumber(prix_ht * 1.055,2);
		// 	        prix_ht = prix_ht.toString().replace('.', ',')+"€ HT ";
		// 	        prix_ttc = prix_ttc.toString().replace('.', ',')+"€ TTC ";
		// 	        $(this).after(prix_ttc+" ("+prix_ht+")").remove();
		// });
		// 
		
		$('.short_product,#info_panier').each(function(){	
			var price = $(this).find(".prix_ht").text().replace(',', '.');
			var taxerate = $(this).find(".taxerate").attr('title');
	        var prix_ht = parseFloat(price);
			var prix_ttc = roundNumber(prix_ht+(prix_ht * taxerate)/100,2);
	        prix_ht = prix_ht.toString().replace('.', ',')+"€ HT ";
	        prix_ttc = prix_ttc.toString().replace('.', ',')+"€ TTC ";
	        $(this).find(".prix_ht").after(prix_ttc+" ("+prix_ht+")").remove();
		});




		
		
		
		

		// $('.prix_ttc').each(function(){	
		// 	        var prix_ttc = parseFloat($(this).text().replace(',', '.'));
		// 	        var prix_ht = roundNumber(prix_ttc / 1.055,2);
		// 	        prix_ht = prix_ht.toString().replace('.', ',')+"€";
		// 	        $(this).after("<span class='port_ht'>"+prix_ht+'</span>').remove();
		// });	

		// $('.total_ht').each(function(){	
		// 	var port_ht = parseFloat($('.port_ht').text().replace(',', '.'));
		// 	var total_ht = parseFloat($(this).text().replace(',', '.'));
		// 	total_ht = total_ht+port_ht;
		// 	total_ht = roundNumber(total_ht,2)
		// 	total_ht = total_ht.toString().replace('.', ',')+"€";
		// 	$(this).after(total_ht).remove();
		// });	
		// 
		// var tva_port = 0;
		// $('.port_ht').each(function(){
		// 	var port_ht = parseFloat($(this).text().replace(',', '.'));
		// 	tva_port = (port_ht*1.055)-port_ht;
		// 	tva_port = roundNumber(tva_port,2)
		// });
		// 
		// var total_tva = 0;
		// $('.tva').each(function(){
		// 	var this_tva = parseFloat($(this).text().replace(',', '.'));
		// 	total_tva = total_tva + this_tva;
		// 	total_tva = roundNumber(total_tva,2);
		// });

		// $(".ligne_tva").remove();

		// $(".tva").after(total_tva+tva_port+"€").remove();

	});