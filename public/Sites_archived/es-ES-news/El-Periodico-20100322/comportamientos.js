function limpiarBusqueda(ambito, idioma){
    var txtCAS, txtCAT; 
    txtCAS  = "Buscar en ...";
    txtCAT  = "Buscar a ...";
    if (idioma =='CAT') {
        txtcmp = txtCAT }
    else {
        txtcmp = txtCAS;
    }
    if (ambito.value == txtcmp) ambito.value = ""; // solo limpia si es la palabra por defecto
}

function elegirBusqueda(ambito){
    var formulario      = document.getElementById('formularioDeBusqueda');
	var labels          = document.getElementById('formularioDeBusqueda').getElementsByTagName('label');
	var radioButtons    = document.getElementById('formularioDeBusqueda').getElementsByTagName('input');
		
	for (i=0; i < labels.length; i++) {
		labels[i].className = "";
	}
			
	switch(ambito.id) {

		case "internet"		: 
            radioButtons[0].checked = true;
            formulario.action = "results.asp";
		    formulario.target = "_blank";		    
            break;
            
		case "elPeriodico" 	: 
		    radioButtons[1].checked = true;
            formulario.action = "buscar.asp";		    
		    break;
		    
		case "tienda" 		:
		    radioButtons[2].checked = true;
		    formulario.action = "results_tienda.asp";		    
		    formulario.target = "_blank";		    
		    break;
	}
			
	document.getElementById(ambito.id).className = "seleccionado";
	document.getElementById('palabrasClave').focus();
}

function cambiarDesplegable(caller, destino) {

    var strSeccion,strSeccionOJD;
    var idSeccion;
	var enlaces      	= document.getElementById('navegacion').getElementsByTagName('a');
	var desplegables 	= document.getElementById('desplegables').getElementsByTagName('div');
	var subDesplegables	= document.getElementById(destino).getElementsByTagName('div');
	
	
	
	if (caller.className.indexOf('actual') == -1 ) {
	// El destino no es la Pesta�a actual

		// Oculta todos los desplegables
		for (i=0; i < desplegables.length; i++) {
	
			if (desplegables[i].id != "") {
				document.getElementById(desplegables[i].id).style.display = 'none';
			}
		}

		// Desactiva el estilo de la Pesta�a activa
		for (i=0; i < enlaces.length; i++) {
	
			switch(enlaces[i].className) {
		
				case "primera actual": enlaces[i].className = "primera"; break;
				case "actual"		 : enlaces[i].className = ""; break;
				case "ultima actual" : enlaces[i].className = "ultima"; break;
		
			}
		}
	
		// Asigna el estilo a la Pesta�a activa
		switch(caller.className) {

			case "primera": caller.className = "primera actual"; break;
			case "actual" : caller.className = ""; break;
			case "ultima" : caller.className = "ultima actual"; break;
			default		  : caller.className = "actual";
	
		}
	
		if (destino != 'portada') {
		
			document.getElementById(destino).style.display = "block";
			
			switch(destino) {

				case "opinion"		: document.getElementById('desplegables').className = "conSubMenu"; break;
				case "politica"		: document.getElementById('desplegables').className = "conSubMenu"; break;				
				case "economia"		: document.getElementById('desplegables').className = "conSubMenu"; break;
				case "cosasDeLaVida": document.getElementById('desplegables').className = "conSubMenu"; break;
				case "deportes"		: document.getElementById('desplegables').className = "conSubMenu"; break;
				case "exit"			: document.getElementById('desplegables').className = "conSubMenu"; break;
				default		  		: document.getElementById('desplegables').className = "";
	
			}
			
			idSeccion=0
			switch(destino) {
				case "opinion"		: 
				    strSeccion="opinion";
				    idSeccion=1006;
				    break;
				case "internacional"		: 
				    strSeccion="internacional";
				    idSeccion=1007;
				    break;
				case "politica"		: 
				    strSeccion="politica";
				    idSeccion=1008;
				    break;
				case "elecciones"		: 
				    strSeccion="politica";
				    //idSeccion=1012;
				    idSeccion=1233;
				    break;
				case "economia"		: 
				    strSeccion="economia";
				    idSeccion=1009;
				    break;
				case "catalunya"		: 
				    strSeccion="catalunya";
				    idSeccion=1023;
				    break;
				case "barcelona"		: 
				    strSeccion="barcelona";
				    idSeccion=1022;
				    break;
				case "girona"		: 
				    strSeccion="girona";
				    idSeccion=1049;
				    break;
				case "lleida"		: 
				    strSeccion="lleida";
				    idSeccion=1050;
				    break;
				case "tarragona"		: 
				    strSeccion="tarragona";
				    idSeccion=1051;
				    break;
				case "cosasDeLaVida"		: 
				    strSeccion="barcelona";
				    idSeccion=1022;
				    break;
				case "sociedad":    
				    strSeccion="sociedad";
				    idSeccion=1021;
				    break;
				case "deportes"		: 
				    strSeccion="deportes";
				    idSeccion=1011;
				    break;
				case "tecnologia"		: 
				    strSeccion="tecnologia";
				    idSeccion=1012;
				    break;
				case "verano"		: 
				    strSeccion="verano";
				    idSeccion=1075;
				    break;				    
				case "exit"		: 
				    strSeccion="espectaculos";
				    idSeccion=1013;
				    break;
				case "vayaMundo"		: 
				    strSeccion="vayamundo";
				    idSeccion=1014;
				    break;
				
			}
			//alert(destino);
			
			strSeccionOJD='S_' + strSeccion;
			if (strSeccionOJD=='S_cosasvida')
			   strSeccionOJD='S_sociedad';
			   
			
			if (idSeccion != 0)
			{
		      writeDartCodeIFrame('iframePubli140x140_'+idSeccion+'_1','periodico.cataluna.'+idiomaPubli,strSeccion+'/destacados','medio',140,140);
		          
		      writeOJDNielsenCode(idSeccion+'_1','elperiodico',strSeccionOJD);
			}
			
			
			
		} else {
		
			document.getElementById('desplegables').className = "sinBordeInferior"; 
		
		}

	} else { // Hay que cambiar la visibilidad de la Pesta�a actual

		document.getElementById(destino).style.display = "none";
		document.getElementById('desplegables').className = "sinBordeInferior";
		
		switch(caller.className) {

			case "actual" 		 : caller.className = ""; break;
			case "ultima actual" : caller.className = "ultima"; break;

		}
		
		enlaces[0].className = "primera actual";
		
	}
}

function cambiarSubDesplegable(caller, ordinal,seccion) {
	
	var subDesplegableObjetivo = 'subDesplegable_' + ordinal;
	var subDesplegableActual = caller.parentNode.parentNode.parentNode;
	var otrosEnlaces = caller.parentNode.parentNode.getElementsByTagName('a')
	var subDesplegablesDelActual = document.getElementById(subDesplegableActual.id).getElementsByTagName('div');
	var strSeccion='portada';
	var strSeccionOJD;
	
	var iframeId='iframePubli140x140_'+seccion+'_'+ordinal;	
	
	for (i = 0; i < subDesplegablesDelActual.length; i++) {
		
		if (subDesplegablesDelActual[i].className.indexOf('subDesplegable_') != -1) {

			if (subDesplegablesDelActual[i].className != subDesplegableObjetivo) { subDesplegablesDelActual[i].style.display = 'none'; }
			else 
			{ 
			   subDesplegablesDelActual[i].style.display = 'block'; 
		   		switch(seccion) 
		   		{
					case 1006: 
						strSeccion='opinion';
						break;
					case 1008: 
				    case 1012:
				        strSeccion='tecnologia';
				        break;
					case 1044: 
						strSeccion='politica';						
					case 1045: 
						strSeccion='politica';						
					case 1009: 
						strSeccion='economia';
						break;
					case 1021: 
						strSeccion='sociedad';
						break;
					case 1023: 
						strSeccion='catalunya';
						break;
					case 1022: 
					case 1010: 
						strSeccion='barcelona';
						break;
					case 1049: 
						strSeccion='girona';
						break;
					case 1050: 
						strSeccion='lleida';
						break;
					case 1051: 
						strSeccion='tarragona';
						break;
					case 1011: 
						strSeccion='deportes';
						break;
					case 1013: 
						strSeccion='espectaculos';
						break;
			
						
				}
			//alert(strSeccion);
			//alert(seccion);
			
			//alert(iframeId);
				strSeccionOJD='S_' + strSeccion;
				if (strSeccionOJD=='S_cosasvida')
					strSeccionOJD='S_sociedad';
			
			   writeDartCodeIFrame(iframeId,'periodico.cataluna.'+idiomaPubli,strSeccion+'/destacados','medio',140,140);
			   
			   writeOJDNielsenCode(seccion+'_'+ordinal,'elperiodico',strSeccionOJD);
			
			}

		}
	}
	
	for (i = 0; i < otrosEnlaces.length; i++) {
			
			switch(otrosEnlaces[i].className) {
		
				case "ultima actual": otrosEnlaces[i].className = "ultima"; break;
				case "ultima"		: otrosEnlaces[i].className = "ultima"; break;
				default	     		: otrosEnlaces[i].className = "";
			
			}
	}
	
	if (otrosEnlaces[ordinal - 1].className == "ultima") { otrosEnlaces[ordinal - 1].className = "ultima actual"; }
	else { otrosEnlaces[ordinal - 1].className = "actual"; } 
	
}

function cambiarDesplegableDestacados(caller, destino) {

	var enlaces      	= document.getElementById('navegacionDestacados').getElementsByTagName('a');
	var desplegables 	= document.getElementById('desplegableDestacados').getElementsByTagName('div');
    
    
	// Oculta todos los desplegables
	for (i=0; i < desplegables.length; i++) {

		if (desplegables[i].id != "") {
			document.getElementById(desplegables[i].id).style.display = 'none';
		}
	}

	// Desactiva el estilo de la Pesta�a activa
	for (i=0; i < enlaces.length; i++) {

		switch(enlaces[i].className) {
	
			case "primera actual": enlaces[i].className = "primera"; break;
			case "actual"		 : enlaces[i].className = ""; break;
			case "ultima actual" : enlaces[i].className = "ultima"; break;
	
		}
	}

	// Asigna el estilo a la Pesta�a activa
	switch(caller.className) {

		case "primera": caller.className = "primera actual"; break;
		case "actual" : caller.className = ""; break;
		case "ultima" : caller.className = "ultima actual"; break;
		default		  : caller.className = "actual";

	}
	
	document.getElementById(destino).style.display = "block";
	
	//alert(destino);	
	if (destino != 'contenidosDestacados' && destino != 'editorial' && destino != 'golAGol')  //en esas Pesta�as, no va publi
	{
	  writeDartCodeIFrame('iframePubli140x140_'+destino,'periodico.cataluna.'+idiomaPubli,'servicios/destacados','medio',140,140);
	}		   
	writeOJDNielsenCode(destino,'elperiodico','SV_pestanas');
}

function cambiarDesplegableServicios(caller, destino) {

	var enlaces      	= document.getElementById('navegacionServicios').getElementsByTagName('a');
	var desplegables 	= document.getElementById('desplegableServicios').getElementsByTagName('div');
	
	// Oculta todos los desplegables
	for (i=0; i < desplegables.length; i++) {

		if (desplegables[i].id != "") {
			document.getElementById(desplegables[i].id).style.display = 'none';
		}
	}

	// Desactiva el estilo de la Pesta�a activa
	for (i=0; i < enlaces.length; i++) {

		switch(enlaces[i].className) {
	
			case "primera actual": enlaces[i].className = "primera"; break;
			case "actual"		 : enlaces[i].className = ""; break;
			case "ultima actual" : enlaces[i].className = "ultima"; break;
	
		}
	}

	// Asigna el estilo a la Pesta�a activa
	switch(caller.className) {

		case "primera": caller.className = "primera actual"; break;
		case "actual" : caller.className = ""; break;
		case "ultima" : caller.className = "ultima actual"; break;
		default		  : caller.className = "actual";

	}
	
	document.getElementById(destino).style.display = "block";

	//alert(destino);	
	if (destino != 'promociones')  //en esas Pesta�as, no va publi
	{
	  writeDartCodeIFrame('iframePubli140x140_'+destino,'periodico.cataluna.'+idiomaPubli,'servicios/destacados','medio',140,140);
	}
	writeOJDNielsenCode(destino,'elperiodico','SV_pestanas');
}


function cambiarPestanasColumnaDerecha(caller, destino) {
    /* Modificat per Albert  per reflexar els canvi d'estructura de les pestanyes.  
    getElementsByTagName('div'); no serveix perque els titulars son divs
    
    */
	//var enlaces = document.getElementById('navegacionPestanasColumnaDerecha').getElementsByTagName('a');
	//var pestanas = document.getElementById('pestanasColumnaDerecha').getElementsByTagName('div');
	
	var pestana1 = document.getElementById('pestana_1');	
	var pestana2 = document.getElementById('pestana_2');	
	var pestana3 = document.getElementById('pestana_3');	

    pestana1.style.display = 'none';
    pestana2.style.display = 'none';
    pestana3.style.display = 'none';

	var enlace1 = document.getElementById('enlace1');	
	var enlace2 = document.getElementById('enlace2');	
	var enlace3 = document.getElementById('enlace3');	
    
	// Oculta todas las Pesta�as
	/*
	for (i=0; i < pestanas.length; i++) {
		pestanas[i].style.display = 'none';
	} */
	
	var pestanas    = new Array(pestana1, pestana2, pestana3);
	var enlaces     = new Array(enlace1, enlace2, enlace3);	

	for (i=0; i < enlaces.length; i++) {

		switch (enlaces[i].className) {
			case "primera actual"	: enlaces[i].className = 'primera'; break;
			case "ultima actual"	: enlaces[i].className = 'ultima'; break;
			case "actual"			: enlaces[i].className = ''; break;
		
		}
	}

	switch (destino) {
		case 1 : enlaces[0].className = 'primera actual'; break;
		case 2 : enlaces[1].className = 'actual'; break;
		case 3 : enlaces[2].className = 'ultima actual'; break;
		default: alert("Nada");
	}


	pestanas[destino-1].style.display = 'block';

}

/* Encuestas Albert */
function ContabilizaVoto(valor, idencuesta){
	var FormUrna = document.getElementById('formularioEncuesta'+idencuesta);
	FormUrna.cod_respuesta.value=valor;
	FormUrna.submit();
}
function ContabilizaVoto2(form, valor){
	form.cod_respuesta.value=valor;
	form.submit();
}


function agregar(){
   if ((navigator.appName=="Microsoft Internet Explorer") && 
         (parseInt(navigator.appVersion)>=4)) {
      var url="http://www.elperiodico.com/"; 

      var titulo="El Peri�dico";

      window.external.AddFavorite(url,titulo);
   } else { 
      if(navigator.appName == "Netscape") 
         alert("Presione Crtl+D para agregar este sitio en sus Favoritos"); 
   }
}

  // AUMENTAR Y DISMINUIR TEXTO DE NOTICIA
    
    function actualizaTamanyo(que) {
	if (que == 'mas') {
		switch(document.getElementById('noticia').className) {
			case 'disminuir3':	document.getElementById('noticia').className = 'disminuir2';
								break;
			case 'disminuir2':	document.getElementById('noticia').className = 'disminuir1';
								break;
			case 'disminuir1':	document.getElementById('noticia').className = 'normal';
								break;
			case 'normal':	 	document.getElementById('noticia').className = 'aumentar1';
								break;
			case 'aumentar1':	document.getElementById('noticia').className = 'aumentar2';
								break;
			case 'aumentar2':	document.getElementById('noticia').className = 'aumentar3';
								break;
		}
	} else {
		switch(document.getElementById('noticia').className) {
			case 'disminuir2':	document.getElementById('noticia').className = 'disminuir3';
								break;
			case 'disminuir1':	document.getElementById('noticia').className = 'disminuir2';
								break;
			case 'normal':		document.getElementById('noticia').className = 'disminuir1';
								break;
			case 'aumentar1':	document.getElementById('noticia').className = 'normal';
								break;
			case 'aumentar2':	document.getElementById('noticia').className = 'aumentar1';
								break;
			case 'aumentar3':	document.getElementById('noticia').className = 'aumentar2';
								break;
		}
	}
}


function OJDdownload(target,site,seccion) {
	pixel=new Image();
	window.open(target);
	pixel.src="http://"+site+".ojdinteractiva.com/cgi-bin/ivw/CP/"+seccion;
}



  

