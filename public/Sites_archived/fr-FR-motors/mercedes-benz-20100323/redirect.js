/*
 * Redirect manager api
 */

// (hide everything except the things we make public explicitly)

//-- public constants --
// symbolic names of cms pages


(function() {
var embRedirectManager = new EmbRedirectManager();

// redirect manager is a singleton
window.embGetRedirectManager = function () {
	return embRedirectManager;
};

})();

function EmbRedirectManager() {
	
	var LOGIN = 0; // this is private and no valid symbolic link

	// -- private constansts --
	var SELECTOR_REDIRECT = "redirect";
	var SELECTOR_SETCOOKIES = "setcookies";
	var SELECTOR_LOGIN = "login";
	var SELECTOR_LOGOUT = "logout";
	var SELECTOR_UPDATEPROFILEREF = "updateprofileref";
	
	var PARAMETER_BMCODE = "bmcode";
	var PARAMETER_TARGETURL = "targeturl";
	var PARAMETER_RETURNURL = "returnurl";
	var PARAMETER_SYMBOLIC_NAME = "symbolictarget";
	var PARAMETER_TOKEN1 = "st";
	var PARAMETER_CURRENTURL = "currenturl";
	var PARAMETER_STATE = "state";
	var PARAMETER_FROMSECUREPAGE = "fromsecurepage";
	var PARAMETER_LAYER_NAME = "layername";
	var PARAMETER_ADD_PARAMS = "addparams";
	var PARAMETER_LOCALE = "locale";
	
	var COOKIE_PROFILEREF = "_pr"
	var COOKIE_TOKEN1 = "_st"

	var signonHandle;
	var currentHandle;
	var secureHost;
	var isCurrentUrlSecure;
	var locale;
	
	var getCmsRedirectPageUrl = function(symbolicName, params, isFromSecurePage, bmCode, layerName, targetUrl, state, returnUrl) {
		var url = currentHandle + "." + SELECTOR_REDIRECT + ".html?";
		url += PARAMETER_SYMBOLIC_NAME + "=" + symbolicName;
		url += "&" + PARAMETER_FROMSECUREPAGE + "=" + isFromSecurePage;
		
		if (bmCode) {
			url += "&" + PARAMETER_BMCODE + "=" + bmCode;
		} else if (symbolicName == EmbRedirectManager.CONFIGURATOR 
				||symbolicName == EmbRedirectManager.PRODUCT_ENTRY) {
			if (window.console && window.console.log) {
				window.console.log("*** missing bmCode in redirect ***");
			}
		}
		if (layerName) {
			url += "&" + PARAMETER_LAYER_NAME + "=" + layerName;
		}
		// used for login and targetpath for webclipper targets
		if (targetUrl) {
			url += "&" + PARAMETER_TARGETURL + "=" + encodeURIComponent(targetUrl);
		}
		// only relevant for login
		if (state) {
			url += "&" + PARAMETER_STATE + "=" + state;
		}
		// used for cancel action
		if (returnUrl) {
			if (returnUrl.charAt(0) == '/' && returnUrl.charAt(1) != '/') {
				url += "&" + PARAMETER_RETURNURL + "=" + encodeURIComponent(returnUrl);
			}
		}
		// these are parameters which are given to the redirect target
		if (params || symbolicName == EmbRedirectManager.CURRENT) {
			var addParams = "";
			if (params) {
				for (var param in params) {
				  addParams += param+"="+encodeURIComponent(params[param]) + "&";
				}
			}
			if (symbolicName == EmbRedirectManager.CURRENT) {
				var query = window.location.search;
				if (query.length > 1) {
					query = query.substring(1);
					addParams += query;
				}
			}
			url += "&" + PARAMETER_ADD_PARAMS + "=" + encodeURIComponent(addParams);
		}

		return url;
	};
	
	/**
	 * the page which is requested here must return a 302 with header location
	 * set
	 */
	var redirectToCmsPage = function(symbolicName, params, bmCode, layerName, targetUrl, state, returnUrl) {
		location.href = getCmsRedirectPageUrl(symbolicName, params, isCurrentUrlSecure, bmCode, layerName, targetUrl, state, returnUrl);
	};
	
	var redirectToSignon = function(selector, params) {
		var url = secureHost + signonHandle + "." + selector + ".html?"
		for (var param in params) {
			url += param + "=" + encodeURIComponent(params[param]) + "&";
		}
		location.href = url;
	};
	
	// -- public methods --
	
	/**
	 * Redirects to the standard login page.
	 * 
	 * @param targetUrl
	 *            specifies the targetUrl to which the user is redirected after
	 *            an successfull login
	 * @param state
	 *            one of the predefined states: STATE_NORMAL, STATE_TIMEOUT or
	 *            STATE_NO_TOKEN
	 */
	this.redirectToLogin = function (targetUrl, state, layerName) {
		this.redirectToLoginOptions(targetUrl, {"state": state, "layerName":layerName});
	};
	
	/**
	 * Redirects to the standard login page.
	 * 
	 * @param targetUrl
	 *            specifies the targetUrl to which the user is redirected after
	 *            an successfull login
	 * @param options
	 *            an array of options:
	 *            {
	 *                state: state,
	 *                layername: layername,
	 *                returnUrl: returnURL
	 *            }

	 *            one of the predefined states: STATE_NORMAL, STATE_TIMEOUT or
	 *            STATE_NO_TOKEN
	 */
	this.redirectToLoginOptions = function (targetUrl, options) {
		options = options || {};
		redirectToCmsPage(LOGIN, options.params, options.bmCode, options.layerName, targetUrl, options.state, options.returnUrl);
	};
	
	/**
	 * Redirects to a standard cms page, which is specified by the symbolic name.
	 * 
	 * @param symbolicName
	 *            one of the predefined names: INFO_AND_OVERVIEW, the info and
	 *            overview page (bmCode is not evaluated); CONFIGURATOR, the
	 *            carconfigurator page for the given bmCode; PRODUCT_ENTRY the
	 *            productentry page for the given bmCode.
	 */
	this.redirectToCMSPage = function (symbolicName, bmCode, layerName, params, targetPath) {
		redirectToCmsPage(symbolicName, params, bmCode, layerName, targetPath);
	};
	
	/**
	 * Persists the profileRef in cookies for HTTP and HTTPs, afterwards redirect to the
	 * page specified by symbolicName.
	 * This method should only be called in HTTPs
	 */
	this.updateProfileRef = function (profileRef, symbolicName, bmCode, layerName, params) {
		if (!isCurrentUrlSecure) {
			alert("this method is only allowed in https pages!");
			return;
		}
		
		this.updateProfileRefQuick(profileRef);
		
		var updateProfileRefParams = new Object();
		updateProfileRefParams[PARAMETER_TARGETURL] = getCmsRedirectPageUrl(symbolicName, params, false, bmCode, layerName);
		updateProfileRefParams[PARAMETER_CURRENTURL] = currentHandle;
		updateProfileRefParams[PARAMETER_LOCALE] = locale;
		redirectToSignon(SELECTOR_UPDATEPROFILEREF, updateProfileRefParams);		
	};	


	this.updateProfileRefQuick = function (profileRef) {
		// set profileref cookie
		if(profileRef && profileRef!="null") {
			set_cookie(COOKIE_PROFILEREF, profileRef, undefined, "/", undefined, isCurrentUrlSecure);
		}
		// reload the profile
		if (embGetProfileManager()) {
			embGetProfileManager().updateProfile(function(){
				updateMetaNavigation();
				updateNavigation3();
			});
		}
	}

	/**
	 * Performs login (correctly sets the tokens), afterwards redirect to the
	 * page specified by symbolicName.
	 * This method should only be called in HTTPs
	 */
	this.login = function (token1, symbolicName, bmCode, layerName, params) {
		var loginParams = new Object();
		loginParams[PARAMETER_TOKEN1] = token1;
		loginParams[PARAMETER_LOCALE] = locale;
		loginParams[PARAMETER_CURRENTURL] = currentHandle + ".html";
		if (!symbolicName) {
			if (location.search && -1 != location.search.indexOf("targeturl=")) {
				var search = location.search;
				var start = search.indexOf("targeturl=");
				var end   = search.indexOf("&", start+1);
				if (end == -1) {
					end = search.length;
				}
				
				symbolicName = decodeURIComponent(search.substring(start+"targeturl=".length, end));
			} else {
				symbolicName = EmbRedirectManager.REGISTRATION_SUCCESS;
			}
		}
		if (symbolicName) {
			if (symbolicName.indexOf && symbolicName.indexOf("/") != -1) {
				loginParams[PARAMETER_TARGETURL] = symbolicName;
			} else {
				loginParams[PARAMETER_TARGETURL] = getCmsRedirectPageUrl(symbolicName, params, false, bmCode, layerName);
			}
		}
		redirectToSignon(SELECTOR_LOGIN, loginParams);
	};
	
	this.logout = function(symbolicName, params) {
		var logoutParams = new Object();
		logoutParams[PARAMETER_CURRENTURL] = document.logoutform.currenturl.value;
		logoutParams[PARAMETER_TARGETURL] = getCmsRedirectPageUrl(symbolicName, params, false);
		redirectToSignon(SELECTOR_LOGOUT, logoutParams);
	}
	
	this.init = function(_secureHost, _signonHandle, _currentHandle, _locale) {
		secureHost = _secureHost;
		signonHandle = _signonHandle;
		currentHandle = _currentHandle;
		locale = _locale;
		
		if (location.protocol == "https:") {
			isCurrentUrlSecure = true;
		} else {
			isCurrentUrlSecure = false;
		}
	};
}


EmbRedirectManager.INFO_AND_OVERVIEW = 1;
EmbRedirectManager.CONFIGURATOR = 2;
EmbRedirectManager.PRODUCT_ENTRY = 3;
EmbRedirectManager.CURRENT = 4;
EmbRedirectManager.SHOWROOM = 5;
EmbRedirectManager.MYFAVORITE = 6;
EmbRedirectManager.SHOWROOM_CC = 7;
EmbRedirectManager.HOME_PASSENGER_CARS = 8;
EmbRedirectManager.LANDING_PAGE = 9;
EmbRedirectManager.REGISTRATION_SUCCESS = 10;

// states of the login page (equal to the selectors of the landing page)
EmbRedirectManager.STATE_NORMAL = "";
EmbRedirectManager.STATE_TIMEOUT = "ll2";
EmbRedirectManager.STATE_NO_TOKEN = "ll1";

//layer applications (type of layer within service)
EmbRedirectManager.LAYER_MYPROFILE = "LayerMyProfile";
EmbRedirectManager.LAYER_REGISTRATION_ADDED_VALUE = "LayerRegistrationAddValue";
EmbRedirectManager.LAYER_REGISTRATION = "LayerRegistration";
EmbRedirectManager.LAYER_FORGOTTEN_PASSWORD = "LayerLostPassword";
EmbRedirectManager.LAYER_SAVEDVEHICLES = "LayerSavedVehicles";
EmbRedirectManager.LAYER_OPT_OUT = "LayerOptOut";
EmbRedirectManager.LAYER_KDV_OFF = "LayerKdvOff";
