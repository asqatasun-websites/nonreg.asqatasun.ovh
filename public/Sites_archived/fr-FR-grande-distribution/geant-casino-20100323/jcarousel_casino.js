jQuery(document).ready(function() {

    // JCarroussel - Homepage --------------------------------------------------------		

    $(function() {
        $(".anyClass").jCarouselLite({
            btnNext: ".next",
            btnPrev: ".prev",
            visible: 5,
            start: -2,
            afterEnd: function(a) {
                var idElt = a[2].firstChild.id;
                var idxDebut = idElt.indexOf('.');
                var idxDiv = idElt.substring(idxDebut + 1);
                $(".ServiceTop div.top").hide();
                $(".ServiceBottom div.texte_bas").hide();

                $("#top_" + idxDiv + "").show();
                $("#bottom_" + idxDiv + "").show();
            }
        });

        var maxWidth = 0;
        var maxHeight = 0;
        var lis = $(".MetierCarrousel li");
        lis.css({ float: "left" });
        lis.each(function(i) {
            if ($(this).width() > maxWidth) {
                maxWidth = $(this).width();
            }
            if ($(this).height() > maxHeight) {
                maxHeight = $(this).height();
            }
        });
        lis.width(maxWidth);
        lis.height(maxHeight);
        $(".carrouselMetier-container").width(maxWidth * 4 + 64);

        $(".MetierCarrousel").jCarouselLite({
            btnNext: ".nextMetier",
            btnPrev: ".prevMetier",
            visible: 4
        }); 

        if (typeof(DD_belatedPNG)!='undefined') {
            $(".MetierCarrousel img").each(function(i) {
                DD_belatedPNG.fixPng(this);
            });
        }
    });
});

	
