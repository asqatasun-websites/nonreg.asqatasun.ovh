User-agent: *
Disallow:/adminsite/
Disallow:/html/
Disallow:/kosmos/
Disallow:/ksup/
Disallow:/META-INF/
Disallow:/piwik/
Disallow:/piwik_ancienne_version/
Disallow:/specific/
Disallow:/supxml/
Disallow:/WEB-INF/
Disallow:/webalizer/