window.undefined;

this.isUndefined = function(obj) {return(typeof(obj)=="undefined");};
this.isNull = function(obj) {return((typeof(obj)=="object")&&(obj===null));};
this.isNative = function(obj) {return(!isUndefined(obj)&&!isNull(obj)&&(typeof(obj.constructor)=="function"));};
this.isAlien = function(obj) {return(isObject(obj)&&(typeof(obj.constructor)!="function"));};
this.isNode = function(obj) {return(((isAlien(obj)&&isUndefined(obj.prototype)&&isUndefined(obj.constructor))||(isAlien(obj)&&isUndefined(obj.prototype)&&(typeof(obj.constructor)=="object"))||(isNative(obj)&&isUndefined(obj.prototype)&&(obj.constructor==Object)))?((obj.nodeType&&obj.cloneNode)?(true):(false)):(false));};
this.isBoolean = function(obj) {return(isNative(obj)&&(obj.constructor==Boolean));};
this.isNumber = function(obj) {return(isNative(obj)&&(obj.constructor==Number)&&isFinite(obj));};
this.isString = function(obj) {return(isNative(obj)&&(obj.constructor==String));};
this.isPrimitive = function(obj) {return(isString(obj)||isNumber(obj)||isBoolean(obj));};
this.isObject = function(obj) {return(!isUndefined(obj)&&!isNull(obj)&&!isPrimitive(obj));};
this.isDate = function(obj) {return(isNative(obj)&&(obj.constructor==Date));};
this.isError = function(obj) {return(isNative(obj)&&(obj.constructor==Error));};
this.isRegExp = function(obj) {return(isNative(obj)&&(obj.constructor==RegExp));};
this.isFunction = function(obj) {return(isNative(obj)&&(obj.constructor==Function));};
this.isArray = function(obj) {return(isNative(obj)&&(obj.constructor==Array));};
this.isObjectObject = function(obj) {return(isNative(obj)&&!isNode(obj)&&(obj.constructor==Object));};
this.isConstructedObject = function(obj) {return(isNative(obj)&&!isNode(obj)&&(obj.constructor!=String)&&(obj.constructor!=Number)&&(obj.constructor!=Boolean)&&(obj.constructor!=Object)&&(obj.constructor!=Array)&&(obj.constructor!=Function)&&(obj.constructor!=RegExp)&&(obj.constructor!=Error)&&(obj.constructor!=Date));};

String.prototype.removeWhiteSpaces = function() {return(this.replace(/\s+/g,""));};

String.prototype.leftTrim = function() {return(this.replace(/^\s+/,""));};
String.prototype.rightTrim = function() {return(this.replace(/\s+$/,""));};
String.prototype.basicTrim = function() {return(this.replace(/\s+$/,"").replace(/^\s+/,""));};
String.prototype.superTrim = function() {return(this.replace(/\s+/g," ").replace(/\s+$/,"").replace(/^\s+/,""));};

String.prototype.globalReplace = function(searchValue,replaceValue,behaviour) {var regExpMaskArray=new Array("^","$",".","*","+","?","=","!",":","|","\\","/","(",")","[","]","{","}");for(var i=0;i<regExpMaskArray.length;i++){searchValue=searchValue.replace(new RegExp("\\"+regExpMaskArray[i],"g"),("\\"+regExpMaskArray[i]));}return this.replace(((behaviour=="strictly")?(new RegExp(searchValue,"g")):(new RegExp(searchValue.replace(/\s+/g,"\\\s+"),"gi"))),replaceValue);};

String.prototype.parseEmail = function() {var tldString=".aero.arpa.biz.com.coop.edu.gov.info.int.mil.museum.name.net.org.pro.ac.ad" + ".ae.af.ag.ai.al.am.an.ao.aq.ar.as.at.au.aw.az.ba.bb.bd.be.bf.bg.bh.bi.bj.bm.bn.bo.br.bs.bt.bv.bw.by.bz.ca.cc.cd.cf.cg.ch.ci.ck.cl.cm.cn.co.cr.cu.cv.cx.cy.cz.de.dj.dk.dm.do.dz.ec.ee.eg.eh.er.es.et.fi.fj.fk.fm.fo.fr.ga.gd.ge.gf.gg.gh.gi.gl.gm.gn.gp.gq.gr.gs.gt.gu.gw.gy.hk.hm.hn.hr.ht.hu.id.ie.il.im.in.io.iq.ir.is.it.je.jm.jo.jp.ke.kg.kh.ki.km.kn.kp.kr.kw.ky.kz.la.lb.lc.li.lk.lr.ls.lt.lu.lv.ly.ma.mc.md.mg.mh.mk.ml.mm.mn.mo.mp.mq.mr.ms.mt.mu.mv.mw.mx.my.mz.na.nc.ne.nf.ng.ni.nl.no.np.nr.nu.nz.om.pa.pe.pf.pg.ph.pk.pl.pm.pn.pr.ps.pt.pw.py.qa.re.ro.ru.rw.sa.sb.sc.sd.se.sg.sh.si.sj.sk.sl.sm.sn.so.sr.st.sv.sy.sz.tc.td.tf.tg.th.tj.tk.tm.tn.to.tp.tr.tt.tv.tw.tz.ua.ug.uk.um.us.uy.uz.va.vc.ve.vg.vi.vn.vu.wf.ws.ye.yt.yu.za.zm.zw.";var protocoll="(mailto\:)";var userName="([a-zA-Z0-9][a-zA-Z0-9_\\.-]*|\"([^\\\\\x80-\xff\015\012\"]|\\\\[^\x80-\xff])+\")";var domainName="(([a-zA-Z0-9][a-zA-Z0-9\\._-]*\\.)*[a-zA-Z0-9][a-zA-Z0-9\\._-]*)";var tldName="([a-zA-Z]{2,6})";var regX=new RegExp("^"+protocoll+"?"+userName+"@"+domainName+"\\."+tldName+"$","");if(regX.test(this)){regX.exec(this);protocoll=RegExp.$1;userName=RegExp.$2;domainName=RegExp.$4;tldName=RegExp.$6;if(tldString.search(new RegExp("\\."+tldName+"\\.","i"))>=0){return{protocoll:protocoll,user:userName,domain:domainName,tld:tldName};}else{return null;}}else{return null;}};

location.query = {};
location.getQuery = function() {var parameters=location.search;if(parameters.indexOf("=")>=0){parameters=parameters.substring(1).split("&");for(var i=0;i<parameters.length;i++){parameters[i]=parameters[i].split("=");location.query[parameters[i][0]]=unescape(parameters[i][1]);}}};
location.getQuery();

function importImage(pathName,width,height) {width=((isNative(width))?(parseInt(width)):(null));height=((isNative(height))?(parseInt(height)):(null));width=((isNumber(width))?(width):(null));height=((isNumber(height))?(height):(null));var imgObj;if(width&&height&&isString(pathName)){if(isString(pathName)){imgObj=new Image(width,height);imgObj.src=pathName;}}else if(isString(pathName)){imgObj=new Image();imgObj.src=pathName;}else{imgObj=new Image();}return imgObj;}

var pictures = {};
pictures.activeSuffix = "-a";
pictures.complete = false;

pictures.addImage = function(imgName,defaultSrc,highlightSrc) {if(isString(imgName)){if((isString(defaultSrc))&&(isString(highlightSrc))){pictures[imgName]=new ActiveImage(defaultSrc,highlightSrc);}else if(isString(defaultSrc)){pictures[imgName]={};pictures[imgName].regular=importImage(defaultSrc);}}};

function ActiveImage(defaultSrc,highlightSrc) {this.regular=importImage(defaultSrc);this.highlight=importImage(highlightSrc);}
function preloadActiveImages() {var addToPictures=function(docImages){var regX=/[-_a-zA-Z0-9]*\.([a-zA-Z]*)$/i;var imgName="",imgSource="",imgSourcePath="",imgSourceName="",imgSourceExtension="";for(var i=0;i<docImages.length;i++){imgName=docImages[i].name;imgSource=docImages[i].src;if(imgName&&(imgName!=="")){if(!pictures[imgName]&&regX.test(imgSource)){imgSourcePath=((imgSource.lastIndexOf("/")>=0)?(imgSource.substring(0,(imgSource.lastIndexOf("/")+1))):(""));imgSourceName=((imgSource.lastIndexOf("/")>=0)?(imgSource.substring((imgSource.lastIndexOf("/")+1),imgSource.lastIndexOf("."))):(imgSource.substring(0,imgSource.lastIndexOf("."))));regX.exec(imgSource);imgSourceExtension=RegExp.$1;pictures[imgName]=new ActiveImage(imgSource,(imgSourcePath+imgSourceName+pictures.activeSuffix+"."+imgSourceExtension));}}}};if(document.layers){var getAllImages=function(doc){addToPictures(doc.images);for(var i=0;i<doc.layers.length;i++){getAllImages(doc.layers[i].document);}};getAllImages(document);}else{addToPictures(document.images);}pictures.complete=true;}

function setImageSrc(imgName,imgSrc) {if(isString(imgName)&&isString(imgSrc)){var imgObj=document.getImageByName(imgName);if(imgObj){imgObj.src=imgSrc;}}}

if (document.layers) {
  document.getImageByName = function(imgName) {var imgObj=null;var findImage=function(docImages){var imgFound=false;for(var i=0;i<docImages.length;i++){if(docImages[i].name==imgName){imgObj=docImages[imgName];imgFound=true;break;}}return imgFound;};var browseLayers=function(doc){if(findImage(doc.images)){return;}for(var i=0;i<doc.layers.length;i++){browseLayers(doc.layers[i].document);}};browseLayers(document);return imgObj;};
} else {
  document.getImageByName = function(imgName) {return document.images[imgName];};
}

function mouseOver() {var imgName="";for(var i=0;i<arguments.length;i++){imgName=arguments[i];if(isString(imgName)){if(pictures.complete&&pictures[imgName]){document.getImageByName(imgName).src=pictures[imgName].highlight.src;}}else if(isNode(imgName)&&(imgName.nodeType)&&(imgName.nodeType==1)){imgName=imgName.getAttribute("name");if(imgName&&pictures.complete&&pictures[imgName]){document.getImageByName(imgName).src=pictures[imgName].highlight.src;}}}}
function mouseOut() {var imgName="";for(var i=0;i<arguments.length;i++){imgName=arguments[i];if(isString(imgName)){if(pictures.complete&&pictures[imgName]){document.getImageByName(imgName).src=pictures[imgName].regular.src;}}else if(isNode(imgName)&&(imgName.nodeType)&&(imgName.nodeType==1)){imgName=imgName.getAttribute("name");if(imgName&&pictures.complete&&pictures[imgName]){document.getImageByName(imgName).src=pictures[imgName].regular.src;}}}}

function setPermanentHigh() {var imgName="";for(var i=0;i<arguments.length;i++){imgName=arguments[i];if(isString(imgName)){if(pictures.complete&&pictures[imgName]){pictures[imgName].regular.src=pictures[imgName].highlight.src;document.getImageByName(imgName).src=pictures[imgName].highlight.src;}}else if(isNode(imgName)&&(imgName.nodeType)&&(imgName.nodeType==1)){imgName=imgName.getAttribute("name");if(imgName&&pictures.complete&&pictures[imgName]){pictures[imgName].regular.src=pictures[imgName].highlight.src;document.getImageByName(imgName).src=pictures[imgName].highlight.src;}}}}
function dropPermanentHigh() {var imgName="";for(var i=0;i<arguments.length;i++){imgName=arguments[i];if(isString(imgName)){if(pictures.complete&&pictures[imgName]){pictures[imgName].regular.src=pictures[imgName].regular.src.replace(new RegExp((pictures.activeSuffix+"\\."),"i"),".");document.getImageByName(imgName).src=pictures[imgName].regular.src;}}else if(isNode(imgName)&&(imgName.nodeType)&&(imgName.nodeType==1)){imgName=imgName.getAttribute("name");if(imgName&&pictures.complete&&pictures[imgName]){pictures[imgName].regular.src=pictures[imgName].regular.src.replace(new RegExp((pictures.activeSuffix+"\\."),"i"),".");document.getImageByName(imgName).src=pictures[imgName].regular.src;}}}}

if (document.all && !window.opera) {
  this.setClass = function() {var argumentsLength=arguments.length;if((argumentsLength>=2)&&isString(arguments[argumentsLength-1])&&!isNode(document.all(arguments[argumentsLength-1]))){var cssClassName=arguments[argumentsLength-1],objectId=null,i;for(i=0;i<argumentsLength-1;i++){objectId=arguments[i];if(isString(objectId)){document.all(objectId).className=cssClassName;}else if(isNode(objectId)&&(objectId.nodeType)&&(objectId.nodeType==1)){objectId.className=cssClassName;}}}};
} else if (document.getElementById) {
  this.setClass = function() {
    var argumentsLength=arguments.length;if((argumentsLength>=2)&&isString(arguments[argumentsLength-1])&&!isNode(document.getElementById(arguments[argumentsLength-1]))){
    var cssClassName=arguments[argumentsLength-1],objectId=null,i;
    for(i=0;i<argumentsLength-1;i++){
      objectId=arguments[i];
//      alert(isString(objectId));
//      alert(isNode(objectId));
    if(isString(objectId)){
      //alert("document.getElementById(\"" + objectId + "\") = " + document.getElementById(objectId));
        document.getElementById(objectId).className=cssClassName;
    }
      else if(isNode(objectId)&&(objectId.nodeType)&&(objectId.nodeType==1)){
      objectId.className=cssClassName;
      }
    }
  }
};
} else {
  
  this.setClass = function() {return;};
}
function getAbsoluteLeft(obj) {var x=0;while(obj.offsetParent!==null){x+=obj.offsetLeft;obj=obj.offsetParent;}x+=obj.offsetLeft;return x;}
function getAbsoluteTop(obj) {var y=0;while(obj.offsetParent!==null){y+=obj.offsetTop;obj=obj.offsetParent;}y+=obj.offsetTop;return y;}
function getCurrentStyle(nodeObject,propertyName) {var propertyValue;if(document.documentElement&&document.defaultView){propertyValue=document.defaultView.getComputedStyle(nodeObject,"").getPropertyValue(propertyName);}else if(document.documentElement&&document.documentElement.currentStyle){var regX=/([a-z]*)\-([a-z])([a-z]*)/;while(regX.test(propertyName)){regX.exec(propertyName);propertyName=RegExp.$1+RegExp.$2.toUpperCase()+RegExp.$3;}propertyValue=nodeObject.currentStyle[propertyName];}return propertyValue;}