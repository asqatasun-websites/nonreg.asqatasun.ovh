// http://occidentsystems.com/code/slideshow (c) Occident Systems 2008 / LGPL
SlideShow = Class.create({
	initialize: function(e,a) {
	 this.el=$(e);
	 this.paused=0;
	 this.big=0;
	 this.big_type='lightview';
	 (this._oiu(a['fade']))?this.fade=1:this.fade=a['fade'];
	 (this._oiu(a['direction']))?this.direction=1:this.direction=a['direction'];
	 this.slide=0;
	 this.slides=a['slides'];
	 (this._oiu(a['prefix']))?this.prefix='':this.prefix=a['prefix'];
	 (this._oiu(a['suffix']))?this.suffix='':this.suffix=a['suffix'];
	 (this._oiu(a['delay']))?this.delay=3:this.delay=a['delay'];
	 (this._oiu(a['mousepause']))?this.mp=1:this.mp=a['mousepause'];
	 var pl=[];
	 for(var i=0;i<this.slides.length;i++){
	  pl[i]=new Image(1,1);
	  pl[i].src=this.prefix+this.slides[i]+this.suffix;
	 }
	 if(this.mp){
	  Event.observe(this.el,'mouseover',function(){this.pause();}.bind(this));
	  Event.observe(this.el,'mouseout',function(){this.unpause();}.bind(this));
	 }
	 if(!this._oiu(a['big_suffix'])||!this._oiu(a['big_prefix'])) {
	  this._oiu(a['big_suffix'])?this.big_suffix=this.suffix:this.big_suffix=a['big_suffix'];
	  this._oiu(a['big_prefix'])?this.big_prefix=this.prefix:this.big_prefix=a['big_prefix'];
	  this.big=1;
	  this.el.style['cursor']='pointer';
	  Event.observe(this.el,'click',function(){
	   var url=this.big_prefix+this.slides[this.slide]+this.big_suffix;
	   this._pi(url);
	  }.bind(this));
          var bpl=[];
          for(var i=0;i<this.slides.length;i++){
           bpl[i]=new Image(1,1);
           bpl[i].src=this.big_prefix+this.slides[i]+this.big_suffix;
          }
	 }
	},
	_oiu:function(x){return Object.isUndefined(x);},
	next: function(){
	 if(!this.paused){
	  this.slide++;
	  this._slide_check();
	  this._update();
	 }
	},
	prev: function(){
	 if(!this.paused){
	  this.slide--;
	  this._slide_check();
	  this._update();
	 }
	},
	_slide_check: function() {
	 if(this.direction) {
	  if(this.slide>=this.slides.length||this.slide<0){ this.slide=0; }
	 }
	 else {
	  if(this.slide<0||this.slide>=this.slides.length) { this.slide=this.slides.length-1; }
	 }
	},
	_update: function() {
	 if(this.fade) {
	  /*
	  this.stop();
	  new Effect.Fade(this.el,{onComplete:function(){
	   alert('faded');
	   */
	   this.el.src=this.prefix+this.slides[this.slide]+this.suffix;
	   /*
	   //new Effect.Appear(this.el,{onComplete:function(){alert('back');}});
	   this.start();
	  }.bind(this)});
	  */
	 }
	 else {
	  this.el.src=this.prefix+this.slides[this.slide]+this.suffix;
	 }
	},
	start: function(){
	 this._executor = new PeriodicalExecuter(function(){
	  if(this.direction) { this.next(); } else { this.prev(); }
	 }.bind(this),this.delay);
	},
	stop: function(){
	 if(this._executor) { this._executor.stop(); }
	},
	toggle: function(){
	 this.paused=!this.paused;
	},
	pause: function(){
	 this.paused=1;
	},
	unpause: function(){
	 this.paused=0;
	},
	// from http://www.howtocreate.co.uk/perfectPopups.html
	_pi: function(url) {
	 AutoClose=false;
	 var imgWin = window.open('','_blank','scrollbars=no,resizable=1,width='+600+',height='+400+',left='+10+',top='+10+',statusbar=false');
	 if( !imgWin ) { return true; }
	 imgWin.document.write('<html><head><title><\/title><script type="text\/javascript">\n'+
	 'function resizeWinTo() {\n'+
	 'if( !document.images.length ) { document.images[0] = document.layers[0].images[0]; }'+
	 'var oH = document.images[0].height, oW = document.images[0].width;\n'+
	 'if( !oH || window.doneAlready ) { return; }\n'+
	 'window.doneAlready = true;\n'+
	 'var x = window; x.resizeTo( oW + 200, oH + 200 );\n'+
	 'var myW = 0, myH = 0, d = x.document.documentElement, b = x.document.body;\n'+
	 'if( x.innerWidth ) { myW = x.innerWidth; myH = x.innerHeight; }\n'+
	 'else if( d && d.clientWidth ) { myW = d.clientWidth; myH = d.clientHeight; }\n'+
	 'else if( b && b.clientWidth ) { myW = b.clientWidth; myH = b.clientHeight; }\n'+
	 'if( window.opera && !document.childNodes ) { myW += 16; }\n'+
	 'x.resizeTo( oW = oW + ( ( oW + 200 ) - myW ), oH = oH + ( (oH + 200 ) - myH ) );\n'+
	 'var scW = screen.availWidth ? screen.availWidth : screen.width;\n'+
	 'var scH = screen.availHeight ? screen.availHeight : screen.height;\n'+
	 'if( !window.opera ) { x.moveTo(Math.round((scW-oW)/2),Math.round((scH-oH)/2)); }\n'+
	 '}\n'+
	 '<\/script>'+
	 '<\/head><body onload="resizeWinTo();"'+(AutoClose?' onblur="self.close();"':'')+'>'+
	 (document.layers?('<layer left="0" top="0">'):('<div style="position:absolute;left:0px;top:0px;display:table;">'))+
	 '<img src="'+url+'" title="" onload="resizeWinTo();">'+
	 (document.layers?'<\/layer>':'<\/div>')+'<\/body><\/html>');
	 imgWin.document.close();
	 if( imgWin.focus ) { imgWin.focus(); }
	 return false;
	}
});
