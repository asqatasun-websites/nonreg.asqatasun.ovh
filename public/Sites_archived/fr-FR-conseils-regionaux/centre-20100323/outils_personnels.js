//checkboxes functions
function showhide(checkBox,rubrique)
{
	
	if(checkBox.checked)
	{
		showHideRubriques();
		jQuery('#'+rubrique).slideDown("normal");
	}	
	else
	{
		jQuery('#'+rubrique).slideUp("normal",showHideRubriques);
	}

}
function initCheckBoxes()
{
	if(	jQuery("#cb_favoris").is(":not(:checked)")	)jQuery("#favoris").hide();
	if(	jQuery("#cb_rss").is(":not(:checked)") 		)jQuery("#rss").hide();
	if(	jQuery("#cb_article").is(":not(:checked)")	)jQuery("#articles").hide();
	if(	jQuery("#cb_meteo").is(":not(:checked)")		)jQuery("#meteo").hide();
	showHideRubriques();
} 
function showHideRubriques()
{
	if(jQuery('div#rubriques_choix input:checked').size() == 0 )
		jQuery('div#rubriques').hide();
	else
		jQuery('div#rubriques').show();
}
//end

//utils
function razInputs(elem)
{
	row = jQuery(elem).parent().parent();		
	var input	= row.children("div").children("div").children("input");
	input.attr("value","");
}
function razSelect(elem)
{
	var row = undefined;	
	if(jQuery(elem).is("input[type=text]")  )
		row = jQuery(elem).parent().parent().parent();
	else
		row = jQuery(elem).parent().parent();
		
	var select	= row.children("div").children("select");
	select.children(":first-child").attr("selected","selected");
}
function razRow(elem)
{
	razInputs(elem);
	razSelect(elem);
}
//end utils