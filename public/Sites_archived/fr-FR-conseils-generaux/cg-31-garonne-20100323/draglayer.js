
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_dragLayer(objName,x,hL,hT,hW,hH,toFront,dropBack,cU,cD,cL,cR,targL,targT,tol,dropJS,et,dragJS) { //v4.32
  //Copyright 1998 Macromedia, Inc. All rights reserved.
  var i,j,s,aLayer,retVal,curDrag=null,curLeft,curTop,OP=window.opera;
  var d=document,IE=(d.all),NS4=d.layers;NS6=(!IE&&d.getElementById&&!OP),NS=(NS4||NS6);
  if (!IE&&!NS) return false;
  retVal = true; if(!OP&&IE&&event)event.returnValue=true;
  if (MM_dragLayer.arguments.length > 1) {
    curDrag = MM_findObj(objName); if (!curDrag) return false;
    if (!d.allLayers) { d.allLayers = new Array();
      with (d) if (NS4) { for (i=0; i<layers.length; i++) allLayers[i]=layers[i];
        for (i=0; i<allLayers.length; i++) if (allLayers[i].document && allLayers[i].document.layers)
          with (allLayers[i].document) for (j=0; j<layers.length; j++) allLayers[allLayers.length]=layers[j];
      } else {
       if (NS6) { var all = getElementsByTagName("span");
          for (i=0;i<all.length;i++) {s=all[i].style;if(s&&(s.position||s.pixelTop))allLayers[allLayers.length]=all[i]}
          all = getElementsByTagName("div");}
        for (i=0;i<all.length;i++) {s=all[i].style;if(s&&(s.position||s.pixelTop))allLayers[allLayers.length]=all[i]}
    } }
    curDrag.MM_dragOk=true; curDrag.MM_targL=targL; curDrag.MM_targT=targT;
    curDrag.MM_tol=Math.pow(tol,2); curDrag.MM_hLeft=hL; curDrag.MM_hTop=hT;
    curDrag.MM_hWidth=hW; curDrag.MM_hHeight=hH; curDrag.MM_toFront=toFront;
    curDrag.MM_dropBack=dropBack; curDrag.MM_dropJS=dropJS;
    curDrag.MM_everyTime=et; curDrag.MM_dragJS=dragJS;
    curDrag.MM_oldZ = (NS4)?curDrag.zIndex:curDrag.style.zIndex;
    curLeft= (NS4)?curDrag.left:(NS6)?parseInt(curDrag.style.left):curDrag.style.pixelLeft;
    if (String(curLeft)=="NaN") curLeft=0; curDrag.MM_startL = curLeft;
    curTop = (NS4)?curDrag.top:(NS6)?parseInt(curDrag.style.top):curDrag.style.pixelTop;
    if (String(curTop)=="NaN") curTop=0; curDrag.MM_startT = curTop;
    curDrag.MM_bL=(cL<0)?null:curLeft-cL; curDrag.MM_bT=(cU<0)?null:curTop-cU;
    curDrag.MM_bR=(cR<0)?null:curLeft+cR; curDrag.MM_bB=(cD<0)?null:curTop+cD;
    curDrag.MM_LEFTRIGHT=0; curDrag.MM_UPDOWN=0; curDrag.MM_SNAPPED=false; //use in your JS!
    d.onmousedown = MM_dragLayer; d.onmouseup = MM_dragLayer;
    if(NS)d.captureEvents(Event.MOUSEDOWN|Event.MOUSEUP);
  } else {
    var theEvent=((NS)?objName.type:event.type);
    if (theEvent=='mousedown') {
      var mouseX=(NS)?objName.pageX:((OP)?event.x+pageXOffset:event.clientX+d.body.scrollLeft);
      var mouseY=(NS)?objName.pageY:((OP)?event.y+pageYOffset:event.clientY+d.body.scrollTop);
      var maxDragZ=null; d.MM_maxZ = 0;
      for (i=0; i<d.allLayers.length; i++) { aLayer = d.allLayers[i];
        var aLayerZ = (NS4)?aLayer.zIndex:parseInt(aLayer.style.zIndex);
        if (aLayerZ > d.MM_maxZ) d.MM_maxZ = aLayerZ;
        var isVisible = (((NS4)?aLayer.visibility:aLayer.style.visibility).indexOf('hid') == -1);
        if (aLayer.MM_dragOk != null && isVisible) with (aLayer) {
          var parentL=0; var parentT=0;
          if (!NS4) { parentLayer = (NS6)?aLayer.parentNode:aLayer.parentElement;
            while (parentLayer != null && (parentLayer.style.position || OP)) {
              parentL += parseInt(parentLayer.offsetLeft); parentT += parseInt(parentLayer.offsetTop);
              parentLayer = (NS6)?parentLayer.parentNode:parentLayer.parentElement;
          } }
          var tmpX=mouseX-(((NS4)?pageX:((NS6)?parseInt(style.left):style.pixelLeft)+parentL)+MM_hLeft);
          var tmpY=mouseY-(((NS4)?pageY:((NS6)?parseInt(style.top):style.pixelTop) +parentT)+MM_hTop);
          if (String(tmpX)=="NaN") tmpX=0; if (String(tmpY)=="NaN") tmpY=0;
          var tmpW = MM_hWidth;  if (tmpW <= 0) tmpW += ((NS4)?clip.width :((OP)?style.pixelWidth:offsetWidth));
          var tmpH = MM_hHeight; if (tmpH <= 0) tmpH += ((NS4)?clip.height:((OP)?style.pixelHeight:offsetHeight));
          if ((0 <= tmpX && tmpX < tmpW && 0 <= tmpY && tmpY < tmpH) && (maxDragZ == null
              || maxDragZ <= aLayerZ)) { curDrag = aLayer; maxDragZ = aLayerZ; } } }
      if (curDrag) {
        d.onmousemove = MM_dragLayer; if (NS4) d.captureEvents(Event.MOUSEMOVE);
        curLeft = (NS4)?curDrag.left:(NS6)?parseInt(curDrag.style.left):curDrag.style.pixelLeft;
        curTop = (NS4)?curDrag.top:(NS6)?parseInt(curDrag.style.top):curDrag.style.pixelTop;
        if (String(curLeft)=="NaN") curLeft=0; if (String(curTop)=="NaN") curTop=0;
        MM_oldX = mouseX - curLeft; MM_oldY = mouseY - curTop;
        d.MM_curDrag = curDrag;  curDrag.MM_SNAPPED=false;
        if(curDrag.MM_toFront) {
          eval('curDrag.'+((NS4)?'':'style.')+'zIndex=d.MM_maxZ+1');
          if (!curDrag.MM_dropBack) d.MM_maxZ++; }
        retVal = false; if(!NS4&&!NS6) event.returnValue = false;
    } } else if (theEvent == 'mousemove') {
      if (d.MM_curDrag) with (d.MM_curDrag) {
        var mouseX = (NS)?objName.pageX :((OP)?event.x+pageXOffset: event.clientX + d.body.scrollLeft);
        var mouseY = (NS)?objName.pageY :((OP)?event.y+pageYOffset: event.clientY + d.body.scrollTop);
        newLeft = mouseX-MM_oldX; newTop  = mouseY-MM_oldY;
        with (Math){if (MM_bL!=null)newLeft=max(newLeft,MM_bL);if(MM_bR!=null)newLeft=min(newLeft,MM_bR);
            if (MM_bT!=null)newTop=max(newTop ,MM_bT);if(MM_bB!=null)newTop=min(newTop ,MM_bB);}
        MM_LEFTRIGHT = newLeft-MM_startL; MM_UPDOWN = newTop-MM_startT;
        if (NS4) {left = newLeft; top = newTop;}
        else if (NS6){style.left = newLeft; style.top = newTop;}
        else {style.pixelLeft = newLeft; style.pixelTop = newTop;}
        if (MM_dragJS) eval(MM_dragJS);
        retVal = false; if(!NS) event.returnValue = false;
    } } else if (theEvent == 'mouseup') {
      d.onmousemove = null;
      if (NS){d.releaseEvents(Event.MOUSEMOVE);d.captureEvents(Event.MOUSEDOWN);}
      if (d.MM_curDrag) with (d.MM_curDrag) {
        if (typeof MM_targL =='number' && typeof MM_targT == 'number' &&
            (Math.pow(MM_targL-((NS4)?left:(NS6)?parseInt(style.left):style.pixelLeft),2)+
             Math.pow(MM_targT-((NS4)?top:(NS6)?parseInt(style.top):style.pixelTop),2))<=MM_tol) {
          if (NS4) {left = MM_targL; top = MM_targT;}
          else if (NS6) {style.left = MM_targL; style.top = MM_targT;}
          else {style.pixelLeft = MM_targL; style.pixelTop = MM_targT;}
          MM_SNAPPED = true; MM_LEFTRIGHT = MM_startL-MM_targL; MM_UPDOWN = MM_startT-MM_targT; }
        if (MM_everyTime || MM_SNAPPED) eval(MM_dropJS);
        if(MM_dropBack) {if (NS4) zIndex = MM_oldZ; else style.zIndex = MM_oldZ;}
        retVal = false; if(!NS) event.returnValue = false; }
      d.MM_curDrag = null;
    }
    if (NS&&!OP) d.routeEvent(objName);
  } return retVal;
}
