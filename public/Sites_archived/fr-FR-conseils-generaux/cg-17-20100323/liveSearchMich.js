	/*
// +----------------------------------------------------------------------+
// | Copyright (c) 2004 Bitflux GmbH                                      |
// +----------------------------------------------------------------------+
// | Licensed under the Apache License, Version 2.0 (the "License");      |
// | you may not use this file except in compliance with the License.     |
// | You may obtain a copy of the License at                              |
// | http://www.apache.org/licenses/LICENSE-2.0                           |
// | Unless required by applicable law or agreed to in writing, software  |
// | distributed under the License is distributed on an "AS IS" BASIS,    |
// | WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or      |
// | implied. See the License for the specific language governing         |
// | permissions and limitations under the License.                       |
// +----------------------------------------------------------------------+
// | Author: Bitflux GmbH <devel@bitflux.ch>                              |
// +----------------------------------------------------------------------+

*/
var liveSearchReq1 = false;
var t1 = null;
var liveSearchLast1 = "";
	
var isIE = false;
// on !IE we only have to initialize it once
if (window.XMLHttpRequest) {
	liveSearchReq1 = new XMLHttpRequest();
}

function liveSearchInit() {
	if (navigator.userAgent.indexOf("Safari") > 0) {
		document.getElementById('livesearch1').addEventListener("keydown",liveSearchKeyPress,false);
//		document.getElementById('livesearch').addEventListener("blur",liveSearchHide,false);
	} else if (navigator.product == "Gecko") {
		
		document.getElementById('livesearch1').addEventListener("keypress",liveSearchKeyPress,false);
		document.getElementById('livesearch1').addEventListener("blur",liveSearchHideDelayed,false);
		
	} else {
		document.getElementById('livesearch1').attachEvent('onkeydown',liveSearchKeyPress);
//		document.getElementById('livesearch').attachEvent("onblur",liveSearchHide,false);
		isIE = true;
	}
	
	document.getElementById('livesearch1').setAttribute("autocomplete","off");

}

function liveSearchHideDelayed() {
	window.setTimeout("liveSearchHide()",400);
}
	
function liveSearchHide() {
	document.getElementById("LSResult1").style.display = "none";
	var highlight = document.getElementById("LSHighlight1");
	/*if (highlight) {
		highlight.removeAttribute("id1");
	}*/
}

function liveSearchKeyPress(event) {
	
	if (event.keyCode == 40 )
	//KEY DOWN
	{
		highlight = document.getElementById("LSHighlight1");
		if (!highlight) {
			highlight = document.getElementById("LSShadow1").firstChild.firstChild;
		} else {
			highlight.removeAttribute("id1");
			highlight = highlight.nextSibling;
		}
		if (highlight) {
			highlight.setAttribute("id1","LSHighlight1");
		} 
		if (!isIE) { event.preventDefault(); }
	} 
	//KEY UP
	else if (event.keyCode == 38 ) {
		highlight = document.getElementById("LSHighlight1");
		if (!highlight) {
			highlight = document.getElementById("LSResult1").firstChild.firstChild.lastChild;
		} 
		else {
			highlight.removeAttribute("id1");
			highlight = highlight.previousSibling;
		}
		if (highlight) {
				highlight.setAttribute("id1","LSHighlight1");
		}
		if (!isIE) { event.preventDefault(); }
	} 
	//ESC
	else if (event.keyCode == 27) {
		highlight = document.getElementById("LSHighlight1");
		if (highlight) {
			highlight.removeAttribute("id1");
		}
		document.getElementById("LSResult1").style.display = "none";
	} 
}
function liveSearchStart(nomFormulaire) {
	if (t1) {
		window.clearTimeout(t1);
	}
	t1 = window.setTimeout("liveSearchDoSearch('"+nomFormulaire+"')",200);
}

function liveSearchDoSearch(nomFormulaire) {

	if (typeof liveSearchRoot == "undefined") {
		liveSearchRoot = "";
	}
	if (typeof liveSearchRootSubDir == "undefined") {
		liveSearchRootSubDir = "";
	}
	if (typeof liveSearchParams == "undefined") {
		liveSearchParams = "";
	}
	if (liveSearchLast1 != eval('document.'+nomFormulaire+'.c1.value')) {
	if (liveSearchReq1 && liveSearchReq1.readyState < 4) {
		liveSearchReq1.abort();
	}
	if ( eval('document.'+nomFormulaire+'.c1.value') == "") {
		liveSearchHide();
		return false;
	}
	if (window.XMLHttpRequest) {
	// branch for IE/Windows ActiveX version
	} else if (window.ActiveXObject) {
		liveSearchReq1 = new ActiveXObject("Microsoft.XMLHTTP");
	}
	liveSearchReq1.onreadystatechange= liveSearchProcessReqChange;
	liveSearchReq1.open("GET", "liveSearchCommunesMich.php?nomFormu="+nomFormulaire+"&c1=" + eval('document.'+nomFormulaire+'.c1.value') + liveSearchParams);
	liveSearchLast1 = eval('document.'+nomFormulaire+'.c1.value');
	liveSearchReq1.send(null);
	}
}

function liveSearchProcessReqChange() {
	
	if (liveSearchReq1.readyState == 4) {
		var  res1 = document.getElementById("LSResult1");
		res1.style.display = "block";
		var  sh1 = document.getElementById("LSShadow1");
	//	alert(document.getElementById("LSShadow2"));
		sh1.innerHTML = liveSearchReq1.responseText;		 
	//	alert(sh2.innerHTML);
	}
}

function liveSearchSubmit() {
	var highlight = document.getElementById("LSHighlight1");
	if (highlight && highlight.firstChild) {
		window.location = liveSearchRoot + liveSearchRootSubDir + highlight.firstChild.nextSibling.getAttribute("href");
		return false;
	} 
	else {
		return true;
	}
}

function myLiveSearchStart(objet, nomFormulaire){
	var reg=new RegExp(" ");
	var valeur= objet.value.replace(reg,"");
	if(valeur.length>=1){
		liveSearchStart(nomFormulaire);
	}else{
		liveSearchHide();
	}
}


