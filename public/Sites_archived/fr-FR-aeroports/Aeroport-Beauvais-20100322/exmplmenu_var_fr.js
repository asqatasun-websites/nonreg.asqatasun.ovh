/***********************************************************************************
*	(c) Ger Versluis 2000 version 5.411 24 December 2001 (updated Jan 31st, 2003 by Dynamic Drive for Opera7)
*	For info write to menus@burmees.nl		          *
*	You may remove all comments for faster loading	          *		
***********************************************************************************/

	var NoOffFirstLineMenus=6;		// Number of first level items
	var LowBgColor='#22337D';		// Background color when mouse is not over
	var LowSubBgColor='#A8C0F0';	// Background color when mouse is not over on subs
	var HighBgColor='#EEBE3F';		// Background color when mouse is over
	var HighSubBgColor='#B8E0FF';	// Background color when mouse is over on subs
	var FontLowColor='#FFFFFF';		// Font color when mouse is not over
	var FontSubLowColor='#22337D';	// Font color subs when mouse is not over
	var FontHighColor='#3877A7';	// Font color when mouse is over
	var FontSubHighColor='#C80000';	// Font color subs when mouse is over
	var BorderColor='#22337D';		// Border color
	var BorderSubColor='#22337D';	// Border color for subs
	var BorderWidth=2;				// Border width
	var BorderBtwnElmnts=1;			// Border between elements 1 or 0
	var FontFamily="arial,comic sans ms,technical"	// Font family menu items
	var FontSize=9;					// Font size menu items
	var FontBold=1;					// Bold menu items 1 or 0
	var FontItalic=0;				// Italic menu items 1 or 0
	var MenuTextCentered='left';	// Item text position 'left', 'center' or 'right'
	var MenuCentered='left';		// Menu horizontal position 'left', 'center' or 'right'
	var MenuVerticalCentered='top';	// Menu vertical position 'top', 'middle','bottom' or static
	var ChildOverlap=0;				// horizontal overlap child/ parent
	var ChildVerticalOverlap=0;		// vertical overlap child/ parent
	var StartTop=120;				// Menu offset x coordinate
	var StartLeft=5;				// Menu offset y coordinate
	var VerCorrect=0;				// Multiple frames y correction
	var HorCorrect=0;				// Multiple frames x correction
	var LeftPaddng=3;				// Left padding
	var TopPaddng=2;				// Top padding
	var FirstLineHorizontal=0;		// SET TO 1 FOR HORIZONTAL MENU, 0 FOR VERTICAL
	var MenuFramesVertical=1;		// Frames in cols or rows 1 or 0
	var DissapearDelay=500;			// delay before menu folds in
	var TakeOverBgColor=1;			// Menu frame takes over background color subitem frame
	var FirstLineFrame='navig';		// Frame where first level appears
	var SecLineFrame='space';		// Frame where sub levels appear
	var DocTargetFrame='space';		// Frame where target documents appear
	var TargetLoc='';				// span id for relative positioning
	var HideTop=0;					// Hide first level when loading new document 1 or 0
	var MenuWrap=1;					// enables/ disables menu wrap 1 or 0
	var RightToLeft=0;				// enables/ disables right to left unfold 1 or 0
	var UnfoldsOnClick=0;			// Level 1 unfolds onclick/ onmouseover
	var WebMasterCheck=0;			// menu tree checking on or off 1 or 0
	var ShowArrow=0;				// Uses arrow gifs when 1
	var KeepHilite=1;				// Keep selected path highligthed
	var Arrws=['img/menu/tri.gif',5,9,'img/menu/tridown.gif',10,5,'img/menu/trileft.gif',5,10];	// Arrow source, width and height

function BeforeStart(){return}
function AfterBuild(){return}
function BeforeFirstOpen(){return}
function AfterCloseAll(){return}


// Menu tree
//	MenuX=new Array(Text to show, Link, background image (optional), number of sub elements, height, width);
//	For rollover images set "Text to show" to:  "rollover:Image1.jpg:Image2.jpg"

Menu1=new Array("rollover:img/menu/horaire01.gif:img/menu/horaire02.gif","javascript:void(0)","",5,20,110);
	Menu1_1=new Array("Etat des vols","etatvols.php?lang=fr","",0,20,270);
	Menu1_2=new Array("R�guliers","rvols_regul.php","",0,20);
	Menu1_3=new Array("Charters","rvols_charter.php","",0,20);
//	Menu1_4=new Array("Guide horaire","telecharge.php?nomFichier=guide_horaire.pdf&urlComplete=doc/guide_horaire_fr.pdf","",0,20);
	Menu1_4=new Array("Guide du passager","telecharge.php?nomFichier=guide.pdf&urlComplete=doc/guide.pdf","",0,20);
	Menu1_5=new Array("Horaires des navettes A�roport-Centre ville","telecharge.php?nomFichier=navette.pdf&urlComplete=doc/navette.pdf","",0,20);		
	
Menu2=new Array("rollover:img/menu/dest01.gif:img/menu/dest02.gif","javascript:void(0)","",2,20,100);
	Menu2_1=new Array("A�roports desservis","dest.php?lang=fr","",0,20,180);
	Menu2_2=new Array("Les compagnies a�riennes","contacts_cie.php?lang=fr","",3,20,180);
		Menu2_2_1=new Array("Ryanair","contacts_cie.php?lang=fr#ryanair","",0,20,180);
		Menu2_2_2=new Array("Wizzair","contacts_cie.php?lang=fr#wizzair","",0,20,180);
		Menu2_2_3=new Array("Blue Air","contacts_cie.php?lang=fr#blueair","",0,20,180);

Menu3=new Array("rollover:img/menu/infos01.gif:img/menu/infos02.gif","javascript:void(0)","",8,20,100);
	Menu3_1=new Array("Contacts","javascript:void(0)","",3,20,150);
		Menu3_1_1=new Array("A�roport Paris/Beauvais","contacts_aero.php","",0,20,180);
		Menu3_1_2=new Array("Compagnies a�riennes","contacts_cie.php","",0,20,180);
		Menu3_1_3=new Array("Services officiels","contacts_off.php","",0,20,180);
	Menu3_2=new Array("Liaisons","acces.php","",9,20,150);
		Menu3_2_1=new Array("Acces depuis Paris","depuis_paris.php","",0,20,220);
		Menu3_2_2=new Array("Acces depuis Beauvais","depuis_bvs.php","",0,20,200);			
		Menu3_2_3=new Array("Transfert bus Paris/Beauvais","bus.php","",0,20,200);
		Menu3_2_4=new Array("Horaire des bus Paris/Beauvais","horaire_bus.php","",0,20,200);
		Menu3_2_5=new Array("Par le train","acces.php#train","",0,20,200);
		Menu3_2_6=new Array("En taxi","acces.php#taxi","",0,20,200);
		Menu3_2_7=new Array("Navettes porte � porte SuperShuttle","acces.php#supershuttle","",0,20,200);
		Menu3_2_8=new Array("Avec Disneyland Paris","acces.php#disney","",0,20,200);
		Menu3_2_9=new Array("Avec Roissy Charles de Gaulle","acces.php#cdg","",0,20,200);
	Menu3_3=new Array("H�tels","hotels.php","",0,20,150);
	Menu3_4=new Array("Stationnement","parking.php","",0,20,150);
	Menu3_5=new Array("Location v�hicules","location_voi.php","",0,20,150);
	Menu3_6=new Array("Taxi","taxi.php","",0,20,150);
	Menu3_7=new Array("Guide du voyageur","guide_voyageur.php","",0,20,150);
	Menu3_8=new Array("Location de salles","locationsalle.php","",0,20,150);
	
Menu4=new Array("rollover:img/menu/tourisme01.gif:img/menu/tourisme02.gif","http://www.destination-beauvais-paris.com/","",0,20,100);
	/*Menu4_1=new Array("Contact","contact_tourisme.php?lang=fr","",0,20,200);
	Menu4_2=new Array("Golf Pass","golf_pass.php?lang=fr","",0,20,200);
	Menu4_3=new Array("D�couvrez la Picardie","accueil_tourisme.php?lang=fr","",0,20,200);
	Menu4_4=new Array("Agences de voyages","agences.php","",0,20,150);	
	Menu4_5=new Array("Produits touristiques","javascript:ouvrir_fen(\'http://www.aeroportbeauvais.com/tourisme/accueil.php?lang=fr\',\'800\',\'600\')","",0,20,150);*/

Menu5=new Array("rollover:img/menu/environnement01.gif:img/menu/environnement02.gif","javascript:void(0)","",5,20,100);
	Menu5_1=new Array("Le service Environnement � votre �coute","contact_enviro.php?lang=fr","",0,20,250);
	Menu5_2=new Array("La d�marche de l'a�roport","enviro.php?lang=fr","",0,20,250);
	Menu5_3=new Array("Le bilan environnemental 2009","telecharge.php?nomFichier=rapport_environnemental_2009.pdf&urlComplete=doc/rap_enviro_2009.pdf","",0,20,250);
	Menu5_4=new Array("Politique environnementale de la SAGEB","pol_enviro.php?lang=fr","",0,20,250);
	Menu5_5=new Array("Travaux de r�am�nagement","trav_reamenagement.php?lang=fr","",0,20,250);

Menu6=new Array("rollover:img/menu/presse01.gif:img/menu/presse02.gif","javascript:void(0)","",2,20,100);
	Menu6_1=new Array("Publications","com_presse.php?lang=fr","",0,20,200);
	Menu6_2=new Array("Relations presse","presse.php?lang=fr","",0,20,200);
	