function JSFCommunicator(flashMovie)
{	
	this.init(flashMovie);
}
//=======================
JSFCommunicator.prototype.init = function(flashMovie) {

	if(flashMovie=="undefined") {
		var flashMovie = null;
	 }
	this.setMovie(flashMovie);
	this.functionToCall = null;
	this.functionLocationinFlash = null;
	this.functionArgs = null;
}


/*====================*/

JSFCommunicator.prototype.setMovie = function(flashMovie)
{
	this.flashMovie = flashMovie;
}


/**
===========================
*/
JSFCommunicator.prototype.setVariable  = function(propName, propValue) {
	this.flashMovie.SetVariable(propName,propValue);
}



/**
 =========================
*/
JSFCommunicator.prototype.getVariable  = function(propName) {

	var result = this.flashMovie.GetVariable(propName);
	return result;
}


/**
=========================
*/
JSFCommunicator.prototype.callFunction = function(fnLocation,fnName,fnArgs) {

	if(this.flashMovie==null) {	return false; }
	
//	get the current value of triggerFn from flash
	var flag = this.getVariable("/:triggerFn");
	var result = false;

//	if no function name passed, return false
	if(fnName=="") {return false; }
//	if 	fnLocation is not proper, set it to _level0 as default
	if(fnLocation=="") {
		var fnLocation = "_level0";
	}

	this.setVariable("/:fnLocation",fnLocation);
	this.setVariable("/:fnName",fnName);
	
//	check if fnArgs is an array
	if(typeof(fnArgs)=="object") {
//		convert it to $@$$-delemited string and pass to flash
		this.setVariable("/:fnArgs",fnArgs.join("$@$$"));
	}else if(typeof(fnArgs)=="number" || typeof(fnArgs)=="string") {
		this.setVariable("/:fnArgs",fnArgs);
	}
	
//	change triggerFn property in flash which being watched
	this.setVariable("/:triggerFn",!flag);

//	check if function in flash called successfully or not.
	result = this.getVariable("triggerFnStatus");
	
//	set triggerFnStatus false again.
	this.setVariable("/:triggerFnStatus",false);

//	return result of call.
	return result;

	
}

//======================================================
