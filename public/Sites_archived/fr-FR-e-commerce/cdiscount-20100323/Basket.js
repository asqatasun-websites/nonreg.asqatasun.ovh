function ShowServiceConfirmationDiv(elementID, display, e) {
    document.getElementById(elementID).style.display = display;

    if (e && e.preventDefault)
        e.preventDefault(); // DOM style
    e.returnValue = false;
    return e.returnValue; // IE style
}
