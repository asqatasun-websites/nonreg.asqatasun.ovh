function loadXML(url, _curDate) {
	var ajax = new Ajax(url + '?rnd=' + Math.random(), {
		method:'get',
		onSuccess: function(responseText, responseXML) {						
			var xml2html = '';
			var xml = responseXML;
			var chats = xml.getElementsByTagName('chat');
			var curDate;
			if($type(_curDate) == false){
				curDate = requestServerDate();//new Date();//
				//curDate = new Date('11/16/2009 09:16:00');
			}
			else{
				curDate = _curDate;
			}
		
			var _firstDate, _firstNom, _firstHeure_debut, _firstHeure_fin, counter = 0;
			var __firstDate, __firstNom, __firstHeure_debut, __counter = 0;
			var _limit8 = 0;
			var _isChatInfo = false;
			
			for(var i = 0; i < chats.length; i++){
				var heure_debut = chats[i].getElementsByTagName('heure_debut')[0].firstChild;
				if(heure_debut == null){heure_debut = '';}else{heure_debut = heure_debut.nodeValue;}
				
				var heure_fin = chats[i].getElementsByTagName('heure_fin')[0].firstChild;
				if(heure_fin == null){heure_fin = '';}else{heure_fin = heure_fin.nodeValue;}
				
				var introduction = chats[i].getElementsByTagName('introduction')[0].firstChild;
				if(introduction == null){introduction = '';}else{introduction = introduction.nodeValue;}
				
				var logo = chats[i].getElementsByTagName('logo')[0].firstChild;
				if(logo == null){logo = '';}else{logo = logo.nodeValue;}
				
				var nom = chats[i].getElementsByTagName('nom')[0].firstChild;
				if(nom == null){nom = '';}else{nom = nom.nodeValue;}
				
				var intervenants = chats[i].getElementsByTagName('intervenants')[0].firstChild;
				if(intervenants == null){intervenants = '';}else{intervenants = intervenants.nodeValue;}
				
				var link = chats[i].getElementsByTagName('link')[0].firstChild;
				if(link == null){link = '';}else{link = link.nodeValue;}
				
				var date = chats[i].getElementsByTagName('date')[0].firstChild;
				if(date == null){date = '';}else{date = date.nodeValue;}
				
				if(date == '' || heure_debut == '' || heure_fin == ''){
					continue;
				}
				
				var _includeText = '';
				var _isValidDate = checkDate(curDate, date, heure_debut, heure_fin);
				var _isActiveChat = 0;
				if (_isValidDate == 0){
					_includeText = '<a href="' + link + '" title="Programme en cours" target="_blank">Programme en cours</a>';
					_isActiveChat = 1;
					if(counter == 0){
						_firstNom = nom;
						_firstDate = date;
						_firstHeure_debut = heure_debut;
						_firstHeure_fin = heure_fin;
						if ($('chatsInfo')){
							$('chatsInfo').setHTML("<strong>Programme en cours :</strong> " + _firstNom + ", jusqu'� " + _firstHeure_fin);
							_isChatInfo = true;
						}
						if ($('linkChats')){
							$('linkChats').setHTML("Participez");
							$('linkChats').href = link;
						}
					}
					counter++;
				}				
				else if(_isValidDate == 1){
					_includeText = '';
					_isActiveChat = 0;
					if(counter == 0){						
						_firstNom = nom;
						_firstDate = date;
						_firstHeure_debut = heure_debut;
						_firstHeure_fin = heure_fin;
						if ($('chatsInfo')){
							$('chatsInfo').setHTML("<strong>Prochain chat :</strong> " + _firstNom +  ", le " + toDateFr(_firstDate) + " � " + _firstHeure_debut);
							_isChatInfo = true;							
						}
						if ($('linkChats')){
							$('linkChats').setHTML("Consultez le programme");
						
							$('linkChats').href = "http://www.handichat.fr/";
						}
					}
					counter++;					
				}
				else if(_isValidDate == 2){
					if(__counter == 0){
						_isActiveChat = 0;
						__firstNom = nom;
						__firstDate = date;
						__firstHeure_debut = heure_debut;						
					}
					__counter++;
					continue;
				}
				else{
					continue;
				}				//' + (_isActiveChat==1)?'class="aciveChat"':null +'
				var ac = (_isActiveChat==1)?'class="aciveChat"':'';
				
				if(_isActiveChat==1){
				
					xml2html += '<dt '+ ac +'><p>' + _includeText + heure_debut +'</p></dt>' +
							'<dd '+ ac +'> <a href="' + link + '" class="logoActive" title="' + nom + '" target="_blank">' +
								'<img src="seph_2009/images/' + logo +'" alt="' + nom + '" width="76" height="30" /></a>' +
								'<div><h5><a href="' + link + '" target="_blank">'+ introduction + '</a></h5>'+
								'</div><p class="participez"><a href="' + link + '" target="_blank">PARTICIPEZ !</a></p>'+
							'</dd>';
				
				}else{
					xml2html += '<dt><p>' + _includeText + heure_debut +'</p></dt>' +
							'<dd> <a href="' + link + '" title="' + nom + '" target="_blank">' +
								'<img src="seph_2009/images/' + logo +'" alt="' + nom + '" width="76" height="30" /></a>' +
								'<div><h5><a href="' + link + '" target="_blank">'+ introduction + '</a></h5>'+
								'</div>'+
							'</dd>';
				}
				
				

				
				if(++_limit8 == 20){
					break;
				}				
			}
			if(!_isChatInfo){				
				if ($('chatsInfo')){
					$('chatsInfo').setHTML("<strong>Prochain chat :</strong> " + __firstNom +  ", le " + toDateFr(__firstDate) + " � " + __firstHeure_debut);
					_isChatInfo = true;	
					if ($('linkChats')){
							$('linkChats').setHTML("Consultez le programme");
						
							$('linkChats').href = "http://www.handichat.fr/";
						}					
				}
			}
			if($('chatContent')){
				$('chatContent').setHTML(xml2html);	
			}
//			console.log(_isActiveChat);
			if($('typeChat4')){				
				if(xml2html == ''){	
				
					$('typeChat4').setHTML(toDateFr(__firstDate));
					var _firstDateArr = __firstDate.split('/');
					//var _instanceFirstDate = new Date(Date.parse(_firstDateArr[1] + '/' + _firstDateArr[0] + '/' + _firstDateArr[2]));
					
					var _instanceFirstDate = new Date(_firstDateArr[1] + '/' + _firstDateArr[0] + '/' + _firstDateArr[2]);
					
					loadXML('seph_2009/xml/chats.xml', _instanceFirstDate);	
					return false;
				}
				else{
					//var currentDate = new Date();
					var currentDate = curDate;
					var _currentDate = currentDate.getDate();
					var _currentMonth = currentDate.getMonth() + 1;				
					var _currentYear = currentDate.getFullYear();
					var _currentHours = currentDate.getHours();
					var _currentMinutes = currentDate.getMinutes();
					$('typeChat4').setHTML(toDateFr(_currentDate + '/' + _currentMonth + '/' + _currentYear));				
				}				
			}			
			initSmScroller();
		}
	}).request();	
}

function requestServerDate(){
	var curDate = null;
	var url = 'seph_2009/time.php';
	ajx = new Ajax(url, {
			method: 'get',
			onSuccess: function(text,xml){
				tempArr = text.split(' ');
				//currentDate = tempArr[0].split('/');
				//console.log(tempArr);
				//console.log(currentDate);
				curDate = new Date(parseInt(tempArr[0]) , parseInt(tempArr[1]) -1 ,parseInt(tempArr[2]),parseInt(tempArr[3]),parseInt(tempArr[4]),parseInt(tempArr[5])   );
				//console.log(curDate.toString());
				
				
			}
	}).request();

	curDate2 = new Date();
	//console.log(curDate2.toString());
	return curDate; 
	
/*	
	
	var xmlhttp = null;
	if (window.XMLHttpRequest) {// code for Firefox, Mozilla, IE7, etc.
		xmlhttp=new XMLHttpRequest();
	} else if (window.ActiveXObject) {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	if (xmlhttp != null) {
		xmlhttp.open("GET", "seph_2009/js/scroller.js?nocache=" + new Date().getTime(), false);
		xmlhttp.send(null);
		
		var arr = xmlhttp.getAllResponseHeaders().split('\n');
		var i = 0;
		while (i < arr.length) {
			var str = arr[i++].toLowerCase();			
			if (str.indexOf('date:') != -1) {
				str = str.split('date:')[1];
				console.log(Date.parse(str));
				curDate = new Date(Date.parse(str));
				//console.log(curDate.toString());				
			}
		}
	}
	//console.log( curDate.response() );
*/	
	
}

function toDateFr(date){
	var _date = date;
	var _dateArr = _date.split('/');
	var _instanceDate = new Date(Date.parse(_dateArr[1] + '/' + _dateArr[0] + '/' + _dateArr[2]));
	
	//get day, date, month, year
	var _insDay = _instanceDate.getDay();
	var _insDate = _instanceDate.getDate();
	var _insMonth = _instanceDate.getMonth() + 1;
	var _insYear = _instanceDate.getFullYear();
	
	var __day = '', __month = '';
	switch(_insDay){
		case 0:
			__day = 'Dimanche';
			break;
		case 1:
			__day = 'Lundi';
			break;
		case 2:
			__day = 'Mardi';
			break;
		case 3:
			__day = 'Mercredi';
			break;
		case 4:
			__day = 'Jeudi';
			break;
		case 5:
			__day = 'Vendredi';
			break;
		case 6:
			__day = 'Samedi';
			break;
	}
	switch(_insMonth){
		case 1:
			__month = 'Janvier';
			break;
		case 2:
			__month = 'F�vrier';
			break;
		case 3:
			__month = 'Mars';
			break;
		case 4:
			__month = 'Avril';
			break;
		case 5:
			__month = 'Mai';
			break;
		case 6:
			__month = 'Juin';
			break;
		case 7:
			__month = 'Juillet';
			break;
		case 8:
			__month = 'Ao�t';
			break;
		case 9:
			__month = 'Septembre';
			break;
		case 10:
			__month = 'Octobre';
			break;
		case 11:
			__month = 'Novembre';
			break;
		case 12:
			__month = 'D�cembre';
			break;
	}
	return __day + ' ' + _insDate + ' ' + __month;
}

function checkDate(curDate, date, heureDebut, heureFin){
	var _curDate = curDate;
	var _date = date;
	var _dateArr = _date.split('/');
	var _chatDate = new Date(Date.parse(_dateArr[1] + '/' + _dateArr[0] + '/' + _dateArr[2]));
	
	//get date, month, year
	var _curDay = _curDate.getDate();
	var _curMonth = _curDate.getMonth() + 1;
	var _curYear = _curDate.getFullYear();
	var _curTime = _curDate.getTime();
	//get date, month, year
	var _chatDay = _chatDate.getDate();
	var _chatMonth = _chatDate.getMonth() + 1;
	var _chatYear = _chatDate.getFullYear();
	var _chatTime = _chatDate.getTime();
	
	if(_curYear == _chatYear && _curMonth == _chatMonth && _curDay == _chatDay){
		// get hours, minutes
		var _curHours = _curDate.getHours();
		var _curMinutes = _curDate.getMinutes();
		var _curTimes = _curHours * 60 + _curMinutes;
		// get hours, minutes
		
		var _chatHoursDebut = Number(heureDebut.split('h')[0]);
		var _chatMinutesDebut = Number(heureDebut.split('h')[1]);
		var _chatTimeDebut = _chatHoursDebut * 60 + _chatMinutesDebut;
		
		var _chatHoursFin = Number(heureFin.split('h')[0]);
		var _chatMinutesFin = Number(heureFin.split('h')[1]);
		var _chatTimesFin = _chatHoursFin * 60 + _chatMinutesFin;
		
		if(_curTimes > _chatTimesFin){
			return -1;
		}
		else if(_curTimes >= _chatTimeDebut && _curTimes <= _chatTimesFin){
			return 0;
		}
		else{
			return 1;
		}		
	}
	else if(_chatTime > _curTime){
		return 2;	
	}
	return -1;
}


window.addEvent('load', function(){

	var curDate = null;
	var url = 'seph_2009/time.php';
	ajx = new Ajax(url, {
			method: 'get',
			onSuccess: function(text,xml){
				tempArr = text.split(' ');
				curDate = new Date(parseInt(tempArr[0]) , parseInt(tempArr[1]) -1 ,parseInt(tempArr[2]),parseInt(tempArr[3]),parseInt(tempArr[4]),parseInt(tempArr[5])   );
				loadXML('seph_2009/xml/chats.xml',curDate);	
			}
	}).request();

	
});