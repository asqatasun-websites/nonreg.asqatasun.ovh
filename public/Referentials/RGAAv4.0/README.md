# RGAA v4 Testcases from the real internet

## How to save a web page

1. Open the page in Firefox
2. File > Save page as... (CTRL-S)
3. Place the files in the suitable directory 
4. :warning: In the "Save as..." dialog, be sure to select *Web Page, complete* (lower right of the dialog)
5. Verify the download is OK: if an orange pill appears on the download icon for Firefox, you should retry.

## Structure of directories

```
Referentials
    RGAAv4.0
        01.Images
            1.1.1-1Passed
                Testcase-with-any-filename-01.html
                Testcase-with-any-filename-02.html
            1.1.1-2Fail
                Testcase-with-any-filename-03.html
                Testcase-with-any-filename-04.html
            1.1.1-3NMI
                Testcase-with-any-filename-05.html
                Testcase-with-any-filename-06.html
            1.1.1-4NA
                Testcase-with-any-filename-07.html
                Testcase-with-any-filename-08.html
```
