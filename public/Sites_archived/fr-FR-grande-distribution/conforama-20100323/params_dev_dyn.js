/**
 * élément dynamiques de personnalisation des encarts de dev front
 * certains éléments doivent être mis a jour en fonction du contexte d'utilisation tel que les appels aux images
 * il est donc nécessaire de générer dynamiquement coté serveur cette feuille de script.
 * 
**/

var SetConstant={
	_Init: function(){
		
		// personnalisation de la farandole
		SetConstant.currentPageFarandoles=1; // page à afficher pas defaut
		SetConstant.tamponFarandoles= "mainFarandole"; // tampon de classe des farandoles
		SetConstant.tamponFarandoleElement= "farandoleElement"; // tampon de classe d'un élément de farandole
		SetConstant.nbElementsByRowFarandole= 4; // Nombres d'éléments par ligne
		SetConstant.tamponFarandoleElementDescript="farandoleDescriptPdt"; // tampon de class de l'encart de description
		
		// personnalisation de la mini-farandole
		SetConstant.currentPageMiniFarandoles=1; // page à afficher pas defaut
		SetConstant.tamponMiniFarandoles="complemataryProducts";//tampon des éléments de miniFarandoles
		SetConstant.tamponMiniFarandoleElementDescript="farandoleDescriptPdt"; // tampon de class de l'encart de description
		
		
		// personnalisation des articles complémentaires
		SetConstant.currentPageArtComp= 1; // page à afficher pas defaut
		SetConstant.tamponArtComp= "mainArtComp"; // tampon de classe du module d'articles complémentaires
		SetConstant.tamponArtCompElement= "ArtCompElement"; // tampon de classe d'un élément du module d'articles complémentaires
	}
}

 $(document).ready(function() {
	SetConstant._Init();//initialisation des constantes dhtml
 });//initialisation du document
 
 
 
 
