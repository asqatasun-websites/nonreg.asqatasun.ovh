/******************************************************************************
* cmScript.js
*******************************************************************************

*******************************************************************************
*                                                                             *
* Copyright 2006									                          *
*                                                                             *
******************************************************************************/

function MM_findObj(n, d) { 
	var p,i,x;
	if(!d)
	{
		d=document;
	}
	if((p=n.indexOf("?"))>0&&parent.frames.length)
	{
		d=parent.frames[n.substring(p+1)].document;
		n=n.substring(0,p);
	}
	if(!(x=d[n])&&d.all)
	{
		x=d.all[n];
	}
	for (i=0;!x&&i<d.forms.length;i++)
	{
		x=d.forms[i][n];
	}
	for(i=0;!x&&d.layers&&i<d.layers.length;i++)
	{
		x=MM_findObj(n,d.layers[i].document);
	}
	if(!x && d.getElementById){
		x=d.getElementById(n);
	}
	return x;
}

function showHideLayers(p_element, p_visible)
{
	document.getElementById(p_element).style.visibility=p_visible;
	//alert(p_element + " : " + document.getElementById(p_element).style.visible);
}


function change(p_element, p_position)
{
	document.getElementById(p_element).style.position=p_position;
	//alert(p_element + " : " + document.getElementById(p_element).style.position);
}

function closeElement(p_element){
	//declaration des variables
	var i, j, k, cpt, fin, lastSelected, suite, selected;
	lastSelected = document.cmFormulaire.lastSelected.value;
	//alert("dernier cliqu� : " + lastSelected);
	selected = p_element.length;
	suite = lastSelected.substring(selected, lastSelected.length - 1);
	var tab = suite.split('.');
	suite = "";
	cpt = 0;
	for (i=0; i<15; i++){
		
		for (j=1; j<15; j++)
		{
			if(MM_findObj(p_element + j))
			{
				cpt ++;
			}
		}
		fin = cpt + 1;
		if(fin > 1)
		{
			for (k=1; k<fin+1; k++)
			{
				if(MM_findObj(p_element + k))
				{
					//alert("dernier supprim� : " + p_element + k);
					showHideLayers((p_element + k), 'hidden');
					change((p_element + k), 'absolute');
				}
			}
		}
		p_element += tab[i] + ".";
	}
}

function buildLayers(p_element_show, p_element_hide, p_position){
	var i, fin, cpt, affiche;
	cpt = 0;
	affiche = "true";
	if((p_element_hide.length == 6) && (p_position == "0"))
	{
		affiche = atrribNiveauUn(p_element_show);
	}
	if(affiche == "true")
	{
		closeElement(p_element_hide);
		if(MM_findObj(p_element_show))
		{
			showHideLayers(p_element_show,'visible');
			if(p_position == "1")
			{
				change(p_element_show, 'relative');
			}
			else
			{
				change(p_element_show, 'absolute');
			}
		}
	}
	document.cmFormulaire.lastSelected.value = p_element_show.substring(0, p_element_show.length-1);
	
}

function atrribNiveauUn(p_para)
{
	var nom = document.cmFormulaire.nameFirstClick.value;
	var retour = "";
	if(nom == p_para)
	{
		//alert(p_para);
		//showHideLayers(p_para, 'hidden');
		//change(p_para, 'absolute');
		closeElement(p_para.substring(0, p_para.length-1));
		document.cmFormulaire.nameFirstClick.value = "";
		retour = "false";
	}
	else
	{
		document.cmFormulaire.nameFirstClick.value = p_para;
		retour = "true";
	}
	return retour;
}