(function($) {
    Smirnoff.UI.ContestDialog = (function() {
        var
        _imageSubmitted = false;
        return {
            /**
             * Show the login dialog if required.
             *
             * @param {Object} force Force the login dialog to show.
             */
            authenticate: function(force, msg) {
                Smirnoff.util.log.debug('authenticate()');
                var S = Smirnoff.Session;
                if (!force) var force = false;

                if (force == false && S.loggedIn() || S.loggedInToECM()) return true;
                Smirnoff.UI.LoginDialog.show(true, msg);

                return false;
            },
            /**
             * Add event handlers and UI components
             */
            bind: function() {
                Smirnoff.util.log.debug('bind()');
                var D = Smirnoff.UI.Dialogs,
					CD = Smirnoff.UI.ContestDialog;
					
                CD.form
	                .unbind('submit')
	                .submit(function(e) {
	                    e.preventDefault();
	                    CD.submitContest();
	                    return false;
	                });
                D.bindFileInputs(CD.form);
            },

            loadSuccess: function(txt, stat, xhr) {
                var CD = Smirnoff.UI.ContestDialog;
                CD.container = Smirnoff.UI.Dialogs.getModal();
                // TODO - pull container elements from the content manager
                CD.form = $('#modal-content-contest > form');
                CD.bind();
            },

            /**
             * Clear form elements
             */
            resetForm: function() {
                var D = Smirnoff.UI.Dialogs,
				    CD = Smirnoff.UI.ContestDialog;

                if (!CD.form) {
                    return;
                }

                CD.form[0].reset();
                D.hidePreviews(CD.form);
            },

            /**
             * Temporarily replaces LoginDialog.afterLogin and RegistrationDialog.afterRegister
             */
            afterAuthentication: function() {
                Smirnoff.util.log.debug('ContestDialog.afterAuthentication()');
                var D = Smirnoff.UI.Dialogs,
				    CD = Smirnoff.UI.ContestDialog;

                CD.submitContest();
                //CD.showConfirmation();
                D.restoreCallbacks();
            },
            /**
             * Runs after the contest form is successfully processed.
             */
            afterSubmitSuccess: function() {
                Smirnoff.util.log.debug('ContestDialog.afterSubmitSuccess()');
                var CD = Smirnoff.UI.ContestDialog,
                CM = Smirnoff.UI.Dialogs.ContentManager;
                CD.resetForm();
                CM.hideMsg();
            },
            showConfirmation: function() {
                Smirnoff.util.log.debug('ContestDialog.showConfirmation()');
                Smirnoff.UI.Dialogs.ContentManager.show({
                    id: 'modal-content-contest-signup-complete',
                    url: '/dialogs/contest/complete.aspx?id=' + id,
                    containerClass: 'dialog_signup_complete_container',
                    dataType: 'html',
                    afterShow: function() {
                        setTimeout(function() {
                            Smirnoff.UI.Dialogs.fadeOut();
                        },
                        3000);
                    }
                });
            },
            submitContest: function() {
                var CD = Smirnoff.UI.ContestDialog,
                D = Smirnoff.UI.Dialogs,
                CM = D.ContentManager;
                Smirnoff.util.log.debug('ContestDialog.submitContest()');
                D.saveCallbacks(CD.afterAuthentication, CD.afterAuthentication);

                // Prevent double submission
                D.disableButtons(CD.form);

                // Do the file upload first
                if (D.uploadInProgress(CD.form)) {
                    $(CD.form).bind('uploads-complete', CD.submitContest);
                }
                D.showLoader(CD.form);
                // Submit the contest
                CD.form.ajaxSubmit({
                    dataType: 'json',
                    complete: function() {
                        D.hideLoader(CD.form);
                        // Re-enable submission
                        D.enableButtons(CD.form);
						$(CD.form).unbind('uploads-complete');
                    },
                    success: function(data, status) {
                        Smirnoff.util.log.debug('ContestDialog.submitContest(): ajax success!');

                        // data.loggedIn indicates a valid user session
                        // data.success indicates whether the form is valid
                        if (data.loggedin === false) {

                            Smirnoff.util.log.debug('ContestDialog.submitContest(): FAIL: not logged in');
                            // User is not logged in, show login dialog
                            D.authenticate(true, data.errors);
                        } else {
                            if (data.success && data.success === true) {
                                Smirnoff.util.log.debug('ContestDialog.submitContest(): AOK!');
                                CD.afterSubmitSuccess();
                                CM.clearCurrentForm();

                                // Success: user is logged in
                                CD.showConfirmation();
                            }
                            else {
                                Smirnoff.UI.Dialogs.ContentManager.show('modal-content-contest');
                                CD.form.find('.status_message').html(data.errors).show();
                            }
                        }
                    },
                    error: function(data, status, e) {
                        }
                });
            }
        }
    })();
    $(function() {
        var CD = Smirnoff.UI.ContestDialog;
        $('.contest-link').click(function(e) {
            e.preventDefault();

            CD.resetForm();

            var href = $(this).attr('href');
            $.scrollTo('body', 800, {
                easing: 'swing'
            });
            Smirnoff.UI.Dialogs.ContentManager.show({
                id: 'modal-content-contest',
                url: href,
                containerClass: 'dialog_contest_container',
                width: 525,
                afterLoad: function() {
                    var CD = Smirnoff.UI.ContestDialog;
                    CD.loadSuccess.apply(CD, arguments);
                },
                beforeUnload: function() {}
            });
            return false;
        });
    });
	
	
	
		$('div.story.video .imgAvailable a').click(function(e) {
			e.preventDefault();
			
			var link = $(this), 
			     url = link.attr('href'),
				 matches = url.match(/id=([0-9]+)$/), 
				 id = matches[1];
			
			Smirnoff.UI.Dialogs.ContentManager.show({
				id: 'modal-content-video',
				url: url,
                onClose: function() {
                    
                },
                afterLoad: function() {
                    
                    
                },
				width: 500,
				cache: false
			})
			
			return false;
		});
	
	
})(jQuery);
