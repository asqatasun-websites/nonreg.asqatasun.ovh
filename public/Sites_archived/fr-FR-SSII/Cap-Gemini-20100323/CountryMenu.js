change_title = 'changer';
close_title = 'fermer';
ms = 1000;
timer = null;
link_timeout = null;
countryname_timeout = null;
changelink_timeout = null;
z_index_visible = 20;
z_index_unvisible = -1;

var toggleIt = {
	/**
	 * toggleVisibility
	 * 
	 * Switches visibility on and off.
	 * 
	 * @param	object	id, element id of the element to be toggled
	 * @return	true
	 * 
	 */
	toggleVisibility : function( id ) {
		var element = document.getElementById(id);
		if(element.style.display == 'none')
			element.style.display = 'block';
		else
			element.style.display = 'none';	
	},
	/**
	 * hideDiv
	 * 
	 * Sets display of an element to none after a configured timeout.
	 * 
	 * @param	object	id, element id of the element to be hidden
	 * @return	true
	 * 
	 */
	hideDivs : function() {
    	for (i = 0; i < element_ids.length; i++) {
			var element = document.getElementById(element_ids[i]);
			element.style.display = 'none';
    	}
	},
	startHide : function( ids ) {
			element_ids = ids;
			timer = setTimeout("toggleIt.hideDivs()", ms);				
	},
	/**
	 * showDiv
	 * 
	 * Sets display of an element to block.
	 * 
	 * @param	object	id, element id of the element to be displayed
	 * @return	true
	 * 
	 */
	showDiv : function( id ) {
		if (timer) clearTimeout(timer);
		if (link_timeout) clearTimeout(link_timeout);
		if (countryname_timeout) clearTimeout(countryname_timeout);
		if (changelink_timeout) clearTimeout(changelink_timeout);
		var element = document.getElementById(id);
		element.style.display = 'block';		
	},
	/**
	 * changeLink
	 * 
	 * Switches link title after a configured timeout (if delay is set).
	 * 
	 * @param	object	id, element id of the element to be displayed
	 * @param	integer	delay, 1 if delay is to be used and 0 if not
	 * @return	true
	 * 
	 */
	changeLink : function( id, delay) {		
		var changelink = document.getElementById(id);
		var countryname = document.getElementById('country');
		if (delay){
			link_timeout = setTimeout(function(){ changelink.innerHTML = (changelink.innerHTML == change_title) ? close_title : change_title; }, ms);
		}else{
			changelink.innerHTML = (changelink.innerHTML == change_title) ? close_title : change_title;
		}
	}
}