var MenuId = new Array();
var nomMenuId = new Array();

var InMenu = false;
var timeOutMenu = null;

function chargeImgMenu() {
	MenuId.push(document.getElementById('groupe'));
	MenuId.push(document.getElementById('metier'));
	MenuId.push(document.getElementById('finance'));
	MenuId.push(document.getElementById('emploi'));
	MenuId.push(document.getElementById('press'));
	MenuId.push(document.getElementById('partenaire'));

	nomMenuId.push('groupe');
	nomMenuId.push('metier');
	nomMenuId.push('finance');
	nomMenuId.push('emploi');
	nomMenuId.push('press');
	nomMenuId.push('partenaire');
	
	showMenu(document.getElementById(currentMenu), currentMenu);
};

function showMenu(menuToShow,nomMenu){
	if(InMenu == true){
		 clearTimeout(timeOutMenu);
		 timeOutMenu = null;
	}
	for (var i = 0, l = MenuId.length; i < l; i++) {
		MenuId[i].style.display = "none";
		MenuId[i].style.visibility = "visible";
		
		if (nomMenu != '' && nomMenu == nomMenuId[i])
		{
			document.getElementById('menu_'+nomMenuId[i]).src="images/common/menu_"+nomMenuId[i]+"_on.jpg";
		}
		else
		{
			if (nomMenuId[i] != currentMenu)
			{
				document.getElementById('menu_'+nomMenuId[i]).src="images/common/menu_"+nomMenuId[i]+"_off.jpg";	
			}
		}
	}	
	
	menuToShow.style.display = "block";
};