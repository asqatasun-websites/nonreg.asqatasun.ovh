$("input[type='button']").ready(function(){
	/* au chargement le bouton est active (/!\ ce javascript marche pas si accent) */
	$("input[type='button']#btnsubmit_form").attr("disabled",false);
});

$("input[type='checkbox']").ready(function(){

	$("input[type='checkbox']#ages0").click(function () {
		initAges();
		$("input[type='checkbox']#ages0").attr('checked', true );
	}) ;
	$("input[type='checkbox']#ages1").click(function () {
		initAges();
		$("input[type='checkbox']#ages1").attr('checked', true );	
		}) ;
	$("input[type='checkbox']#ages2").click(function () {
		initAges();
		$("input[type='checkbox']#ages2").attr('checked', true );
	}) ;
	$("input[type='checkbox']#ages3").click(function () {
		initAges();
		$("input[type='checkbox']#ages3").attr('checked', true );
	}) ;
	$("input[type='checkbox']#ages4").click(function () {
		initAges();
		$("input[type='checkbox']#ages4").attr('checked', true );
	}) ;
	$("input[type='checkbox']#ages5").click(function () {
		initAges();
		$("input[type='checkbox']#ages5").attr('checked', true );
	}) ;
	$("input[type='checkbox']#ages6").click(function () {
		initAges();
		$("input[type='checkbox']#ages6").attr('checked', true );
	}) ;
	
	$("input[type='checkbox']#projet_type0").click(function () {
		initProjetType();
		$("input[type='text']#autreCadre").attr("disabled",true);
		$("input[type='checkbox']#projet_type0").attr('checked', true );
	}) ;
	$("input[type='checkbox']#projet_type1").click(function () {
		initProjetType();
		$("input[type='text']#autreCadre").attr("disabled",true);
		$("input[type='checkbox']#projet_type1").attr('checked', true );
	}) ;
	$("input[type='checkbox']#projet_type2").click(function () {
		initProjetType();
		$("input[type='text']#autreCadre").attr("disabled",true);
		$("input[type='checkbox']#projet_type2").attr('checked', true );
	}) ;
	$("input[type='checkbox']#projet_type3").click(function () {
		initProjetType();
		$("input[type='text']#autreCadre").attr("disabled",true);
		$("input[type='checkbox']#projet_type3").attr('checked', true );
	}) ;
	$("input[type='checkbox']#projet_type4").click(function () {
		initProjetType();
		$("input[type='text']#autreCadre").attr("disabled",false);
		$("input[type='text']#autreCadre").focus();
		$("input[type='checkbox']#projet_type4").attr('checked', true );
	}) ;

	$("input[type='checkbox']#factureO").click(function () {
		initFacture();
		$("input[type='checkbox']#factureO").attr('checked', true );
	}) ;
	$("input[type='checkbox']#factureN").click(function () {
		initFacture();
		$("input[type='checkbox']#factureN").attr('checked', true );
	}) ;

	$("input[type='checkbox']#activite0").click(function () {
		initActivite();
		$("input[type='checkbox']#activite0").attr('checked', true );
	}) ;
	$("input[type='checkbox']#activite1").click(function () {
		initActivite();
		$("input[type='checkbox']#activite1").attr('checked', true );
	}) ;
	$("input[type='checkbox']#activite2").click(function () {
		initActivite();
		$("input[type='checkbox']#activite2").attr('checked', true );
	}) ;
	$("input[type='checkbox']#activite3").click(function () {
		initActivite();
		$("input[type='checkbox']#activite3").attr('checked', true );
	}) ;
	$("input[type='checkbox']#activite4").click(function () {
		initActivite();
		$("input[type='checkbox']#activite4").attr('checked', true );
	}) ;
	$("input[type='checkbox']#activite5").click(function () {
		initActivite();
		$("input[type='checkbox']#activite5").attr('checked', true );
	}) ;
	$("input[type='checkbox']#activite6").click(function () {
		initActivite();
		$("input[type='checkbox']#activite6").attr('checked', true );
	}) ;
	$("input[type='checkbox']#activite7").click(function () {
		initActivite();
		$("input[type='checkbox']#activite7").attr('checked', true );
	}) ;
	$("input[type='checkbox']#activite8").click(function () {
		initActivite();
		$("input[type='checkbox']#activite8").attr('checked', true );
	}) ;

	$("input[type='checkbox']#mode_paiement0").click(function () {
		initModePaiement();
		$("input[type='checkbox']#mode_paiement0").attr('checked', true );
	}) ;
	$("input[type='checkbox']#mode_paiement1").click(function () {
		initModePaiement();
		$("input[type='checkbox']#mode_paiement1").attr('checked', true );
	}) ;
	$("input[type='checkbox']#mode_paiement2").click(function () {
		initModePaiement();
		$("input[type='checkbox']#mode_paiement2").attr('checked', true );
	}) ;

	
});

function initAges() {

	$("input[type='checkbox']#ages0").attr('checked', false );
	$("input[type='checkbox']#ages1").attr('checked', false );
	$("input[type='checkbox']#ages2").attr('checked', false );
	$("input[type='checkbox']#ages3").attr('checked', false );
	$("input[type='checkbox']#ages4").attr('checked', false );
	$("input[type='checkbox']#ages5").attr('checked', false );
	$("input[type='checkbox']#ages6").attr('checked', false );
}

function initProjetType() {

	$("input[type='checkbox']#projet_type0").attr('checked', false );
	$("input[type='checkbox']#projet_type1").attr('checked', false );
	$("input[type='checkbox']#projet_type2").attr('checked', false );
	$("input[type='checkbox']#projet_type3").attr('checked', false );
	$("input[type='checkbox']#projet_type4").attr('checked', false );
}
function initActivite() {

	$("input[type='checkbox']#activite0").attr('checked', false );
	$("input[type='checkbox']#activite1").attr('checked', false );
	$("input[type='checkbox']#activite2").attr('checked', false );
	$("input[type='checkbox']#activite3").attr('checked', false );
	$("input[type='checkbox']#activite4").attr('checked', false );
	$("input[type='checkbox']#activite5").attr('checked', false );
	$("input[type='checkbox']#activite6").attr('checked', false );
	$("input[type='checkbox']#activite7").attr('checked', false );
	$("input[type='checkbox']#activite8").attr('checked', false );
}

function initFacture() {
	$("input[type='checkbox']#factureO").attr('checked', false );
	$("input[type='checkbox']#factureN").attr('checked', false );
}

function initModePaiement() {
	$("input[type='checkbox']#mode_paiement0").attr('checked', false );
	$("input[type='checkbox']#mode_paiement1").attr('checked', false );
	$("input[type='checkbox']#mode_paiement2").attr('checked', false );
}

function valid_communs(){
	if (!isValidDate()) {
		return false;
	}
	if (!isValidCodePostal()) {
		alert("Veuillez saisir code postal de 5 chiffres.");
		return false;
	}
	if (!isValidPhoneNumber(document.formMuseum.telephone.value)) {
		alert("Veuillez saisir un num\351ro de t\351l\351phone valide.");
		return false;
	}
	if (document.formMuseum.telecopie.value != "") {
		if (!isValidPhoneNumber(document.formMuseum.telecopie.value)) {
			alert("Veuillez saisir un num\351ro de t\351l\351copie valide.");
			return false;
		}
	}
	if (!isValidMail()) {
		alert("Veuillez saisir un e-mail valide.");
		return false;
	}
	if (!isValidNbAccomp()) {
		alert("Veuillez saisir un nombre d'accompagnateur valide.");
		return false;
	}
	if (!isValideLength()) {
		alert("Vous avez d\351pass\351 le nombre maximal de caract\350res dans une zone de texte.");
		return false;
	}
	if (!isValidModePaiement()) {
		alert("Veuillez s\351lectionner un mode de paiement.");
		return false;
	}
	if (!isValidFacture()) {
		alert("Veuillez cocher une r\351ponse pour la facture souhait\351e.");
		return false;
	}
	return true;
}

function valid_scolaires(){
	//	* e aigu : \351
    //	* e grave : \350
    //	* e circonflexe : \352
    //	* a grave� : \340
	if (document.formMuseum.horaire.value=="" ||
		document.formMuseum.nom_etablissement.value=="" ||
		document.formMuseum.code_postal.value=="" ||
		document.formMuseum.ville.value==""  ||
		document.formMuseum.nom_resp.value==""  ||
		document.formMuseum.telephone.value=="" ||
		document.formMuseum.email.value==""  ||
		document.formMuseum.cycle.value=="" ||
		document.formMuseum.classe.value=="" ||
		document.formMuseum.nb_eleves.value=="" ) {
		
		alert ("Les champs marqu\351s d'une * sont obligatoires");
		return false;
	}
	if (!isValidNbEleve()) {
		alert("Veuillez saisir un nombre d'\351l\350ve valide.");
		return false;
	}
	if (!isValidProjet()) {
		alert("Veuillez s\351lectionner un projet.");
		return false;
	}
	else {
		if (($("input[type='text']#autreCadre").val()=="")
				&& ($("input[type='text']#autreCadre").attr("disabled")==false)) {
			alert("Veuillez saisir un projet.");
			return false;
		}
	}
	if (!isValidActiviteScolaires()) {
		alert("Veuillez s\351lectionner une activit\351.");
		return false;
	}
	return true;
}

function valid_assocs(){
	if (document.formMuseum.horaire.value=="" ||
		document.formMuseum.nom_structure.value=="" ||
		document.formMuseum.adresse.value=="" ||
		document.formMuseum.code_postal.value=="" ||
		document.formMuseum.ville.value==""  ||
		document.formMuseum.nom_directeur.value=="" ||
		document.formMuseum.telephone_directeur.value=="" ||
		document.formMuseum.nom_resp.value==""  ||
		document.formMuseum.telephone.value=="" ||
		document.formMuseum.email.value=="" ||
		document.formMuseum.nb_enfants.value=="" ) {
		alert ("Les champs marqu\351s d'une * sont obligatoires");
		return false;
	}
	if (!isValidNbEnfants()) {
		alert("Veuillez saisir un nombre d'enfants valide.");
		return false;
	}
	if (!isValidPhoneNumber(document.formMuseum.telephone_directeur.value)) {
		alert("Veuillez saisir un num\351ro de t\351l\351phone valide pour le directeur.");
		return false;
	}

	if (!isValidActiviteAssocs()) {
		alert("Veuillez s\351lectionner une activit\351.");
		return false;
	}
	/*
	if (!isValidAges()) {
		alert("Veuillez s\351lectionner une tranche d'ages.");
		return false;
	}*/
	
	return true;
}

function isValidNbEleve() {
	var nbEleve = document.formMuseum.nb_eleves.value;
	var reg = new RegExp( "^[0-9]*$" ) ;
	return (nbEleve.match(reg)!=null);
}

function isValidNbEnfants() {
	var nbEnfants = document.formMuseum.nb_enfants.value;
	var reg = new RegExp( "^[0-9]*$" ) ;
	return (nbEnfants.match(reg)!=null);
}

function isValidNbAccomp() {
	var nbAccomp = document.formMuseum.nb_accomp.value;
	var reg = new RegExp( "^[0-9]*$" ) ;
	return (nbAccomp.match(reg)!=null);
}

function isValidAges() {
	return (
			$("input[type='checkbox']#ages0").attr('checked') ||
			$("input[type='checkbox']#ages1").attr('checked') ||
			$("input[type='checkbox']#ages2").attr('checked') ||
			$("input[type='checkbox']#ages3").attr('checked') ||
			$("input[type='checkbox']#ages4").attr('checked') ||
			$("input[type='checkbox']#ages5").attr('checked') ||
			$("input[type='checkbox']#ages6").attr('checked')
			);
}

function isValidProjet() {
	return (
			$("input[type='checkbox']#projet_type0").attr('checked') ||
			$("input[type='checkbox']#projet_type1").attr('checked') ||
			$("input[type='checkbox']#projet_type2").attr('checked') ||
			$("input[type='checkbox']#projet_type3").attr('checked') ||
			$("input[type='checkbox']#projet_type4").attr('checked')
			);
}
function isValidActiviteScolaires() {
	return (
			$("input[type='checkbox']#activite0").attr('checked') ||
			$("input[type='checkbox']#activite1").attr('checked') ||
			$("input[type='checkbox']#activite2").attr('checked') ||
			$("input[type='checkbox']#activite3").attr('checked') ||
			$("input[type='checkbox']#activite4").attr('checked') ||
			$("input[type='checkbox']#activite5").attr('checked') ||
			$("input[type='checkbox']#activite6").attr('checked') ||
			$("input[type='checkbox']#activite7").attr('checked') ||
			$("input[type='checkbox']#activite8").attr('checked')
			);
}
function isValidActiviteAssocs() {
	return (
			$("input[type='checkbox']#activite0").attr('checked') ||
			$("input[type='checkbox']#activite1").attr('checked') ||
			$("input[type='checkbox']#activite2").attr('checked') ||
			$("input[type='checkbox']#activite3").attr('checked') ||
			$("input[type='checkbox']#activite4").attr('checked')
			);
}
function isValidModePaiement() {
	return ($("input[type='checkbox']#mode_paiement0").attr('checked') || 
			$("input[type='checkbox']#mode_paiement1").attr('checked') ||
			$("input[type='checkbox']#mode_paiement2").attr('checked')
			);
}
function isValidFacture() {
	return ($("input[type='checkbox']#factureO").attr('checked') || 
			$("input[type='checkbox']#factureN").attr('checked')
			);
}

function isValidMail() {
	var email = document.formMuseum.email.value;
	var reg = new RegExp( ".+@.+\\.[a-z]+" ) ;
	return (email.match(reg)!=null);
}

function isValidPhoneNumber(telephone) {
	var reg1 = new RegExp( "^([0-9]{2}[ ]){4}[0-9]{2}$" ) ;
	var reg2 = new RegExp("^([0-9]{10})$");
	return ((telephone.match(reg1)!=null)||(telephone.match(reg2)!=null));
}

function isValidPhoneNumberDirecteur() {
	var telephone = document.formMuseum.telephone_directeur.value;
	var reg1 = new RegExp( "^([0-9]{2}[ ]){4}[0-9]{2}$" ) ;
	var reg2 = new RegExp("^([0-9]{10})$");
	return ((telephone.match(reg1)!=null)||(telephone.match(reg2)!=null));
}


function isValidDate() {
	var j = document.formMuseum.visiteMuseumJour.value;
	var m = document.formMuseum.visiteMuseumMois.value;
	var a = document.formMuseum.visiteMuseumAnnee.value;
	
	if (validDate(j,m-1,a)==false) {
		alert("Veuillez saisir une date de visite au format JJ/MM/AAAA.");
		return false;
	}
	
	jour = new Date();
	visite = new Date(a,m-1,j);
	if (visite < jour) {
		alert("La date de visite doit \352tre sup\351rieure \340 la date du jour.");
		return false;
	}
	
	return true;
}

function isValidCodePostal() {
	var code_postal = document.formMuseum.code_postal.value;
	var reg = new RegExp("^([0-9]{5})$");
	return (code_postal.match(reg)!=null);
}

function isValideLength()
{
	var texteMessage1 = document.formMuseum.contraintes;
	return (texteMessage1.value.length <= texteMessage1.getAttribute('maxlength'));
}


function gomail_scolaires()
{
	if ((valid_scolaires()==true) && (valid_communs()==true))
	{
		document.formMuseum.target="_self";
		document.formMuseum.action='';
		document.formMuseum.submit();
	}
	else {
		// le formulaire n'est pas encore valid�, le bouton doit etre cliquable
		$("input[type='button']#btnsubmit_form").attr("disabled",false);
	}
}
function gomail_assocs()
{
	if ((valid_assocs()==true) && (valid_communs()==true))
	{
		document.formMuseum.target="_self";
		document.formMuseum.action='';
		document.formMuseum.submit();
	}
	else {
		// le formulaire n'est pas encore valid�, le bouton doit etre cliquable
		$("input[type='button']#btnsubmit_form").attr("disabled",false);
	}
}