	function __doPostBack(nameForm, eventTarget, eventArgument) {
		var theform = document.forms[nameForm];
		theform.EVENTTARGET.value = eventTarget.split("$").join(":");
		theform.EVENTARGUMENT.value = eventArgument;
		theform.submit();
	}

	function changePlugin(obj){
		window.location.href = "?plugin=" + obj.value;
	}
	
    function visualiserMot(plugs){
	   window.open('/plugins/thesaurus/popupMotcles.php?lePlug='+plugs+ '&amp;D=' + (new Date()).toString(),'MotCles','width=500,height=600,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes');
    }
    
	function getHTTPObject() {
			var xmlhttp;

			try {
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (E) {
						xmlhttp = false;
					}
			}

			if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
				try {
						xmlhttp = new XMLHttpRequest();
					} catch (e) {
						xmlhttp = false;
					}
			}

			return xmlhttp;
		}

	function getServerName(){
		var url = window.location.href;
		url = url.substr(7);
		url = url.substr(0,url.indexOf('/'));
		return 'http://' + url;
	}

	function initValidation(){
		for(var i in validation){
			document.getElementById('val' + i).style.color = 'black';
		}
	}

	function requiredFieldValidator(theform,obj){
		if(theform[obj].value == ''){
			document.getElementById('val' + obj).style.color = 'red';
			return false;
		}else{
			return true;
		}
	}

	function showPopup(html,fermeture){
		showCache();
		document.getElementById('InfoPopupContenu').innerHTML = html;
		if(fermeture)
			document.getElementById('InfoPopupFermeture').style.display = '';
		else
			document.getElementById('InfoPopupFermeture').style.display = 'none';
		document.getElementById('InfoPopup').style.display = '';
		
		document.getElementById('divDeContenuMaster').style.display='none';
		hideCache();
	}

	function hidePopup(){
		document.getElementById('InfoPopup').style.display = 'none';
		if(document.getElementById('InfoPopupInsertionBoite').style.display != '')
		    document.getElementById('divDeContenuMaster').style.display='';
		hideCache();
	}
	
	
	
	//M�thode utilis�e pour l'insertion d'une bo�te
	function showPopupInsertionBoite(html,fermeture){
		document.getElementById('InfoPopupContenuInsertionBoite').innerHTML = html;
		if(fermeture)
			document.getElementById('InfoPopupFermetureInsertionBoite').style.display = '';
		else
			document.getElementById('InfoPopupFermetureInsertionBoite').style.display = 'none';
		document.getElementById('InfoPopupInsertionBoite').style.display = '';
		
		document.getElementById('divDeContenuMaster').style.display='none';
		
		showCacheInsertionBoite();
	}

    //M�thode utilis�e pour l'insertion d'une bo�te
	function hidePopupInsertionBoite(){
		document.getElementById('InfoPopupInsertionBoite').style.display = 'none';
		
		document.getElementById('divDeContenuMaster').style.display='';
		hideCacheInsertionBoite();
	}
	
    //M�thode utilis�e pour l'insertion d'une bo�te
	function showCacheInsertionBoite(){
		document.getElementById('cacheInsertionBoite').style.display = 'block';
	}

    //M�thode utilis�e pour l'insertion d'une bo�te
	function hideCacheInsertionBoite(){
		document.getElementById('cacheInsertionBoite').style.display = 'none';
	}
	
	
	
	
	
	function showCache(){
		document.getElementById('cache').style.display = 'block';
	}

	function hideCache(){
		document.getElementById('cache').style.display = 'none';
	}

	var sAddress = '';
	function memoriseAddress(obj){
		sAddress = obj.value;
	}
	
		

function FormaterText(chaine)
{
	tableauAccent =     ["�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�"," ","_"];
	tableauSansAccent = ["A","A","A","A","A","A","a","a","a","a","a","a","O","O","O","O","O","O","o","o","o","o","o","o","E","E","E","E","e","e","e","e","I","I","I","I","i","i","i","i","U","U","U","U","u","u","u","u","y","N","n","C","c","-","-"];
	
	for(var i=0; i<tableauAccent.length; i++)
	{
		var exp = new RegExp(tableauAccent[i], "g");
		chaine = chaine.replace(exp, tableauSansAccent[i]);
	}
	
	chaine = chaine.replace(/[^a-zA-Z0-9\-\.]*/g,'');

	return chaine;
}


	function validAddress(obj){
		if(obj.value == sAddress) return;
		/*var val = "";
		for(i=0;i<obj.value.length;i++){
			var code = obj.value.charCodeAt(i)
			if(code == 95 || (code >64 && code < 91) || (code >96 && code < 123) || (code >47 && code < 58)) val += obj.value.charAt(i);
		}
		obj.value = val;*/
		
		obj.value = FormaterText(obj.value);
	}
	
	/*****************************************
	* insertion des image depuis la mediatheque
	*****************************************/
	var winPop;
	var fieldPop;
	var bEditeur;
	var fInsertionImageRetour;
	function setValue(value,id)
	{
		if(bEditeur){
			winPop.document.forms[0].elements[fieldPop].value=value;
			if(winPop.document.forms[0].elements[fieldPop].id=="src")
			     winPop.showPreviewImage(value);
		}
		else{
			fInsertionImageRetour(value,id);
			}
	}


	/*****************************************
	* insertion d'un flash
	*****************************************/
	function insertFlash(strSrc, intWidth, intHeight, strAlt, strID ){
	    var strFlash = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"' +
	                    'width="' + intWidth + '" height="' + intHeight + '" title="' + strAlt + '"' + (strID != '' ? ' id="' + strID + '"' : '') + '>' +
	                    '<param name="movie" value="' + strSrc + '" />'+
	                    '<param name="quality" value="high" />'+
	                    '<param name="wmode" value="transparent" />'+
	                    '<param name="menu" value="false" />'+
	                    '<embed src="' + strSrc + '" width="' + intWidth + '" height="' + intHeight + '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" menu="false" wmode="transparent"' + (strID != '' ? ' name="' + strID + '"' : '') + ' swLiveConnect="true"></embed>'+
	                    '</object>';
	    document.write(strFlash);
	    document.close();
	}

	// permet de recuperer le contenu HTML d'une div en XHTML
	// innerHTML de IE6 ne marche pas
	innerXHTML = function($source,$string) {
	      // (v0.3) Written 2006 by Steve Tucker, http://www.stevetucker.co.uk
	      if (!($source.nodeType == 1)) return false;
	      var $children = $source.childNodes;
	      var $xhtml = '';
	      if (!$string) {
	            for (var $i=0; $i<$children.length; $i++) {
	                  if ($children[$i].nodeType == 3) {
	                        var $text_content = $children[$i].nodeValue;
	                        $text_content = $text_content.replace(/</g,'&lt;');
	                        $text_content = $text_content.replace(/>/g,'&gt;');
	                        $xhtml += $text_content;
	                  }
	                  else if ($children[$i].nodeType == 8) {
	                        $xhtml += '<!--'+$children[$i].nodeValue+'-->';
	                  }
	                  else {
	                        $xhtml += '<'+$children[$i].nodeName.toLowerCase();
	                        var $attributes = $children[$i].attributes;
	                        for (var $j=0; $j<$attributes.length; $j++) {
	                             var $attName = $attributes[$j].nodeName.toLowerCase();
	                             var $attValue = $attributes[$j].nodeValue;
	                             if ($attName == 'style' && $children[$i].style.cssText) {
	                                   $xhtml += ' style="'+$children[$i].style.cssText.toLowerCase()+'"';
	                             }
	                             else if ($attValue && $attName != 'contenteditable') {
	                                   $xhtml += ' '+$attName+'="'+$attValue+'"';
	                             }
	                        }
	                        $xhtml += '>'+innerXHTML($children[$i]);
	                        $xhtml += '</'+$children[$i].nodeName.toLowerCase()+'>';
	                  }
	            }
	      }
	      else {
	            while ($children.length>0) {
	                  $source.removeChild($children[0]);
	            }
	            $xhtml = $string;
	            while ($string) {
	                  var $returned = translateXHTML($string);
	                  var $elements = $returned[0];
	                  $string = $returned[1];
	                  if ($elements) $source.appendChild($elements);
	            }
	      }
	      return $xhtml;
	}
	function translateXHTML($string) {
	      var $match = /^<\/[a-z0-9]{1,}>/i.test($string);
	      if ($match) {
	            var $return = Array;
	            $return[0] = false;
	            $return[1] = $string.replace(/^<\/[a-z0-9]{1,}>/i,'');
	            return $return;
	      }
	      $match = /^<[a-z]{1,}/i.test($string);
	      if ($match) {
	            $string = $string.replace(/^</,'');
	            var $element = $string.match(/[a-z0-9]{1,}/i);
	            if ($element) {
	                  var $new_element = document.createElement($element[0]);
	                  $string = $string.replace(/[a-z0-9]{1,}/i,'');
	                  var $attribute = true;
	                  while ($attribute) {
	                        $string = $string.replace(/^\s{1,}/,'');
	                        $attribute = $string.match(/^[a-z1-9_-]{1,}="[^"]{0,}"/i);
	                        if ($attribute) {
	                             $attribute = $attribute[0];
	                             $string = $string.replace(/^[a-z1-9_-]{1,}="[^"]{0,}"/i,'');
	                             var $attName = $attribute.match(/^[a-z1-9_-]{1,}/i);
	                             $attribute = $attribute.replace(/^[a-z1-9_-]{1,}="/i,'');
	                             $attribute = $attribute.replace(/;{0,1}"$/,'');
	                             if ($attribute) {
	                                   var $attValue = $attribute;
	                                   if ($attName == 'value') {
	                                         $new_element.value = $attValue;
	                                   }
	                                   else if ($attName == 'class') {
	                                         $new_element.className = $attValue;
	                                   }
	                                   else if ($attName == 'style') {
	                                         var $style = $attValue.split(';');
	                                         for (var $i=0; $i<$style.length; $i++) {
	                                               var $this_style = $style[$i].split(':');
	                                               $this_style[0] = $this_style[0].toLowerCase().replace(/(^\s{0,})|(\s{0,1}$)/,'');
	                                               $this_style[1] = $this_style[1].toLowerCase().replace(/(^\s{0,})|(\s{0,1}$)/,'');
	                                               if (/-{1,}/g.test($this_style[0])) {
	                                                     var $this_style_words = $this_style[0].split(/-/g);
	                                                     $this_style[0] = '';
	                                                     for (var $j=0; $j<$this_style_words.length; $j++) {
	                                                           if ($j==0) {
	                                                                 $this_style[0] = $this_style_words[0];
	                                                                 continue;
	                                                           }
	                                                           var $first_letter = $this_style_words[$j].toUpperCase().match(/^[a-z]{1,1}/i);
	                                                           $this_style[0] += $first_letter+$this_style_words[$j].replace(/^[a-z]{1,1}/,'');
	                                                     }
	                                               }
	                                               $new_element.style[$this_style[0]] = $this_style[1];
	                                         }
	                                   }
	                                   else {
	                                         $new_element.setAttribute($attName,$attValue);
	                                   }
	                             }
	                             else $attribute = true;
	                        }
	                  }
	                  $match = /^>/.test($string);
	                  if ($match) {
	                        $string = $string.replace(/^>/,'');
	                        var $child = true;
	                        while ($child) {
	                             var $returned = translateXHTML($string,false);
	                             $child = $returned[0];
	                             if ($child) $new_element.appendChild($child);
	                             $string = $returned[1];
	                        }
	                  }
	                  $string = $string.replace(/^\/>/,'');
	            }
	      }
	      $match = /^[^<>]{1,}/i.test($string);
	      if ($match && !$new_element) {
	            var $text_content = $string.match(/^[^<>]{1,}/i)[0];
	            $text_content = $text_content.replace(/&lt;/g,'<');
	            $text_content = $text_content.replace(/&gt;/g,'>');
	            var $new_element = document.createTextNode($text_content);
	            $string = $string.replace(/^[^<>]{1,}/i,'');
	      }
	      $match = /^<!--[^<>]{1,}-->/i.test($string);
	      if ($match && !$new_element) {
	            if (document.createComment) {
	                  $string = $string.replace(/^<!--/i,'');
	                  var $text_content = $string.match(/^[^<>]{0,}-->{1,}/i);
	                  $text_content = $text_content[0].replace(/-->{1,1}$/,'');
	                  var $new_element = document.createComment($text_content);
	                  $string = $string.replace(/^[^<>]{1,}-->/i,'');
	            }
	            else $string = $string.replace(/^<!--[^<>]{1,}-->/i,'');
	      }
	      var $return = Array;
	      $return[0] = $new_element;
	      $return[1] = $string;
	      return $return;
	}