//***Code Is Intellectual Property Of crmmetrix inc.(Copyright 2006).
//***The use os this code is permitted for this project only as per the license agreement.
//***Code Has Been Developed For The Sole Purpose Of crmmetrix inc./www.airliquide.com Project.
//***Licensed For crmmetrix inc./www.?????????.com Project.



//-------Declare the intercept rates (Should add up to 100%)

//*** we change from relative URLs :
//urldir="/sitecrm";
//*** to absolute URLs for site accessibility
urldir="http://rapport-interactif.carrefour.net/sitecrm";

var exit=20;     //Enter percent
var entry=0;    //Enter percent
var nosurvey=80; //Enter percent
var exiturl=urldir+"/stealth.htm"; //enter the stealth.htm path
var entryurl=urldir+"/entry.htm"; //enter the entry.htm path


function writeCookie(name, value, hours)
{
  var expire = "";
  if(hours != null)
  {
    expire = new Date((new Date()).getTime() + hours * 3600000);
    expire = "; expires=" + expire.toGMTString();
  }
  document.cookie = name + "=" + escape(value) + expire +";path=/;";
}


function readCookie(name)
{
  var cookieValue = "";
  var search = name + "=";
  if(document.cookie.length > 0)
  { 
    offset = document.cookie.indexOf(search);
    if (offset != -1)
    { 
      offset += search.length;
      end = document.cookie.indexOf(";", offset);
      if (end == -1) end = document.cookie.length;
      cookieValue = unescape(document.cookie.substring(offset, end))
    }
  }
  return cookieValue;
}


//Note : To deactivate project assign sniffernet=2;
//The below sniffernet code is to centrally activate,deactivate the project.
//The survey is by default disabled and its only enabled if the site visitor has cookies enabled
var sniffernet;
sniffernet=1;



writeCookie("crm_cookieEnabled","1",20);	//writing cookie to check if cookies are enabled or disabled.
var cookieEnabled=readCookie("crm_cookieEnabled");	//reading the cookie value to see if cookie is written or not.

if(cookieEnabled=="1")
{

	//-------Determining whether this is the first visit to tagged page. Tracking the referer url.
	var visitctr=readCookie("ctr");
		
	if (visitctr!="1")
	{
		//-------Capturing Referring URL
		if (document.referrer&&document.referrer!="")
		{
			writeCookie("refer",document.referrer,20);			
		}
		writeCookie("ctr","1",20);
	}

}


//Browser Sniffer
var mozilla=document.getElementById && !document.all
var ie=document.all

if (sniffernet==1)
{	
	
	/*
	if (ie)
	{		
		document.attachEvent("onmousemove",recontact_crmfx);
	}else if (mozilla){		
		window.addEventListener("onmousemove",recontact_crmfx, true);
	}
	
	*/
	
	document.write("<body onclick='javascript:recontact_crmfx()'>");
	recontact_crmfx();
}




function recontact_crmfx()
{

	//Browser Sniffer
	//var agt=navigator.userAgent.toLowerCase();
	//var client=(agt.indexOf("msie"));

	//reading popseen cookie to make sure the user hasn't taken the survey within past 6 months
	var thiscookie=readCookie("crmseen");

	//if((thiscookie!="seen")&&(client>0))
	if(thiscookie!="seen")
	{

		//***Random Number Generated For Control Sample / Test Sample.
		rndNumber=readCookie("rndNumber");
		if(rndNumber=='')
		{
			var x;
			x=Math.random()*100;
			writeCookie("rndNumber",x,1);
		}
		else
		{
			var x;
			x=parseFloat(rndNumber);
		}
		
		
		//*************
		//***Exit Survey Interception Rate
		if (x>(100-exit))
		{
			var win2=window.open(exiturl,'stealth','height=4,width=4,scrollbars=yes,toolbars=no,location=no,resizable=yes,menubar=yes');
			if (ie) {win2.blur();}
			window.focus();			
		}
		
		
		//****************
		//***Entry Survey Interception Rate. (30% Interception Rate)		
		if (x<=entry)
		{
			window.open(entryurl,'entry','top=5,left=5,height=400,width=600,scrollbars=yes,toolbars=no,location=no,resizable=yes');
		}
			
		
		//****************
		//***NO Survey Interception Rate (10% Interception Rate) 
		//(Cookie Expires In 20 Hrs)
		
		if (x>entry && x<=(100-exit))
		{
			//writing cookie which expires in 20 hrs
			writeCookie("crmseen","seen",20);
		
		}
	}
}
//End bracket for function recontact_crmfx()





