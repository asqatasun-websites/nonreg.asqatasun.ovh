/*
	Fichier : default.js
	Livraison : Mardi 28 novembre 2008 - V2.9
	Modification de la v2.9 : suppression du pr�-remplissage du mot de passe
*/

/*################################## START modif pour hp_bons_plans random #################*/
var loopTabs2 = function(idContainer, tps) {
	var container = $(idContainer);
	var time = tps;
	
	if (container)
		this.launch(container,tps);
}

loopTabs2.prototype.launch = function(container, timer, tabIndex) {
	var tabs = container.getElements('ul.tabs li');
	var self = this;
	
	if (!tabIndex) {
		tabs.each(function(tab, index) {
			tab = $(tab);
			if (tab.hasClass('current'))
				tabIndex = index;
			tab.getElement('a').addEvent('click', function() {
				self.stop();
			});
		})
	} else {
		if (tabIndex>=tabs.length)
			tabIndex = 0;
		tabs[tabIndex].onclick();
	}
	var tabsnumber = tabs.length+1;
	var randomnumber=Math.floor(Math.random()*tabsnumber)
	this.timer = setTimeout(function() {
		self.launch(container,timer,randomnumber);
	},timer);
}

loopTabs2.prototype.stop = function() {
	clearTimeout(this.timer);
}
/*################################## END modif pour hp_bons_plans random #################*/



var loopTabs = function(idContainer, tps) {
	var container = $(idContainer);
	var time = tps;
	
	if (container)
		this.launch(container,tps);
}

loopTabs.prototype.launch = function(container, timer, tabIndex) {
	var tabs = container.getElements('ul.tabs li');
	var self = this;
	
	if (!tabIndex) {
		tabs.each(function(tab, index) {
			tab = $(tab);
			if (tab.hasClass('current'))
				tabIndex = index;
			tab.getElement('a').addEvent('click', function() {
				self.stop();
			});
		})
	} else {
		if (tabIndex>=tabs.length)
			tabIndex = 0;
		tabs[tabIndex].onclick();
	}
	
	this.timer = setTimeout(function() {
		self.launch(container,timer,tabIndex+1);
	},timer);
}

loopTabs.prototype.stop = function() {
	clearTimeout(this.timer);
}

function pngTrans(elm) {
   if (document.compatMode=="BackCompat" || /MSIE [56]/.test(navigator.userAgent)) {
       var imgurl = elm.currentStyle.backgroundImage.match(/url\([\"\'](.*)[\"\']\)/);
       if (imgurl) {
			alert(imgurl)
           elm.style.backgroundImage="none";
          elm.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true', sizingMethod='crop' src='"+imgurl[1]+"')";
       } else {
           elm.style.filter=" ";
       }
   } else {
       elm.style.filter=" ";
   }
}

function imgPngTrans(img) {
   if (document.compatMode=="BackCompat" || /MSIE [56]/.test(navigator.userAgent)) {
		var span = document.createElement('span');
		img.parentNode.insertBefore(span, img);
		span.appendChild(img);
		with(span.style) {
			display = 'inline-block';
			filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=crop src='"+img.src+"')";
		}
		img.style.visibility = 'hidden';
	}
} 

function navH_hover (){
	if(!/MSIE [56]/.test(navigator.userAgent)) return;
	if(!$('navH')) return;
	var lis = $("navh").getElementsByTagName('li');
	for(var i = 0; i<lis.length;i++){
		addHover(lis[i], "hover",false);
		i = i + lis[i].getElementsByTagName("li").length; // tueur de boucle
	}

}

var blockScroll = new Class({
	/**
			init() :
				- showElms : r�cup�re la classe plac� sur le blockScroll permettant de savoir combien d'�l�ments doivent �tre affich�
				- scrollWidth : r�cup�re la largeur du body du blockScroll sans les paddings
				- sep : r�cup�re la largeur des s�parateur (ici, la marge plac�e sur les li);
				- totalElms : r�cup�re le nombre d'�l�ment constituant le scroll
			
			clone():
				- cloner en fonction du nombre d'element visible, les derniers �l�ment du ul en les pla�ants au debut et les premiers �l�ment � la fin, pour simuler un scroll continue
				- totalElms += showElms*2;
			
			sizing()
				- blockWidth : calcule la taille d'un block en fonction de (scrollWidth/showElms)-sep;
				- appliquer blockWidth sur tous les li
				- ulWidth : calcule la taille total du UL, connaissant la taille de chaque li + le s�parateur = ((blockWidth+sep)*totalElms)-sep;
			
			createButton()
				- creation des �l�ment div(left | right)>a>span pour les boutons de scroll injectInside(body)
			
			plugEvents()
				- ajout des �v�nement au click sur les boutons (1 parametre peut sp�cifier de combien je me d�pace d'�l�ment en cliquant)
				- on click � gauche je scroll vers la gauche de 1 �l�ment � chaque fois ou de param �l�ments
				- on click � droite je scroll vers la droite de 1 �l�ment � chaque fois ou de param �l�ments
	**/
	options:{
		type:'H', //type du scroll, H=Horizontal, V=Vertical
		infinity:'true' //is the scroll is a loop ?
	},
	
	initialize: function(blockScroll,options){
		this.setOptions(options);
		if (!blockScroll) return;
		this.block = blockScroll;
		
		this.type = this.options.type;
		this.moveBlocks = 1;
		
		['scroll1','scroll2','scroll3','scroll4','scroll5'].each(function(cn,i){
			if (this.block.hasClass(cn)){
				this.showElms = i+1;
			}
		},this);
		
		['move2','move3','move4','move5'].each(function(cn,i){
			if (this.block.hasClass(cn)){
				this.moveBlocks = i+2;
			}
		},this);
		
		this.blockBody = this.block.getElement('.body');
		this.ul =  this.block.getElement('ul');
		this.lis = this.block.getElements('li');
		if (!this.ul || this.lis.length==0) return;
		this.scrollMask = this.block.getElement('.scrollMask');
		this.scrollWidth = this.scrollMask.offsetWidth;
		this.sep = this.lis[0].getFirst()?this.lis[0].getFirst().getStyle('margin-right').toInt():0;
		this.totalElms = this.lis.length;
		
		
		if (this.lis.length>this.showElms){
			if (this.options.infinity) 
				this.clone();	
			if (this.lis.length>this.showElms) this.createButton();
			if (this.type=='H'){
				this.sizingH();
				this.plugEventH();
			}else if(this.type=='V'){
				this.sizingV();
				this.plugEventV();
			}
		}else{
			this.showElms = this.lis.length;
		}
		
	},
	
	clone:function(){
		var today, timeStamp,
		flash = false;
		first = this.lis[0];
		last = this.lis[this.lis.length-1];
		
		var cloneFlash = function(i,pos){
			if (!this.lis[i]) return true;
			var li = this.lis[i].innerHTML;
			var res = li.match(/loadFlash\.push\([a-zA-Z0-9_]+\s*\,\s*['"](.*)['"]\)/);
			
			if (!res)return false;
			
			today = new Date();
			timeStamp = Date.UTC(today.getFullYear(), today.getMonth(), today.getDate(), today.getHours(), today.getMinutes(), today.getSeconds());
			
			switch(pos){
				case 'before' :
					li = li.replace(new RegExp(res[1], 'g'),'id_'+timeStamp+'B'+i);
					first = new Element('li').setHTML(li).injectBefore(first);
					eval(first.getElement('script').innerHTML);
					break;
				case 'after' :
					li = li.replace(new RegExp(res[1], 'g'),'id_'+timeStamp+'L'+i);
					last = new Element('li').setHTML(li).injectAfter(last);
					eval(last.getElement('script').innerHTML);
					break;
			}
			
			return true;
		}.bind(this);
		
		var simpleClone = function(i,pos){
			if (!this.lis[i]) return true;
			var li = this.lis[i];
			var liClone = li.clone();
			var ids = liClone.getElements('*');
			ids.each(function(elm,i){
				elm.removeProperty('id');
			});
			
			switch(pos){
				case 'before' :
					first = liClone.injectBefore(first);
					break;
				case 'after' :
					last = liClone.injectAfter(last);
					break;
			}
		}.bind(this)
		
		for (var i=0, j=this.showElms; i<j; i++){
			
			flash = cloneFlash(i,'after');
			if (!flash){
				simpleClone(i,'after');
			}
		}
		
		for (var i=this.lis.length-1, j=(this.lis.length-this.showElms); i>=j; i--){
			
			flash = cloneFlash(i,'before');
			if (!flash){
				simpleClone(i,'before');
			}
		}
		
		this.lis = this.block.getElements('li');
		this.totalElms = this.lis.length;
	},
	
	createButton:function(){
		/*
		- creation des �l�ment div(left | right)>a>span pour les boutons de scroll injectInside(body)
		*/
		
		var btnLeft = new Element('div',{'class':'btnLeft'}).adopt(
			new Element('a').adopt(
				new Element('span').setText('pr\351c\351dent')
			)
		);
		
		var btnRight = new Element('div',{'class':'btnRight'}).adopt(
			new Element('a').adopt(
				new Element('span').setText('Suivant')
			)
		);
		
		this.btnLeft = btnLeft.injectBefore(this.blockBody.getFirst());
		this.btnRight = btnRight.injectInside(this.blockBody);
	},
	
	sizingH:function(){
		/*
		- block: calcule la taille d'un block en fonction de (scrollWidth/showElms)-sep;
		- appliquer blockWidth sur tous les li
		- ulWidth : calcule la taille total du UL, connaissant la taille de chaque li + le s�parateur = ((blockWidth+sep)*totalElms)-sep;
		*/
		this.fx = new Fx.Style(this.ul,'left',{duration:500,wait:true});
		
		this.scrollMask.setStyle('width',this.scrollWidth);
		this.blockWidth = Math.floor(this.scrollWidth/this.showElms);
		//this.scrollMask.setStyle('width',this.scrollWidth);
		
		this.lis.each(function(li,i){
			li.setStyle('width',this.blockWidth);
		},this);
		
		var ulWidth = ((this.blockWidth+(this.sep*2))*this.totalElms);
		this.ulWidth = this.ul.setStyle('width', ulWidth + 100);// permet d'�viter les calculs trop justes
		
		this.liWidth = this.lis[0].offsetWidth;
		if (this.options.infinity){
			this.startPos = this.currentLeft = -(this.liWidth*this.showElms);
			this.endPos = -(this.liWidth*(this.totalElms-(this.showElms*2)));
			this.maxPos = -this.liWidth*(this.totalElms-this.showElms);
		}else{
			this.startPos = this.currentLeft = 0;
			this.endPos = this.maxPos = -this.liWidth*(this.totalElms-this.showElms);
		}
		
		this.ul.setStyle('left', this.startPos);
		
		var iUlHeight = this.ul.offsetHeight;
		var iAppliedHeight = 0;
		this.lis.each(function(li,i){
			var oBody = li.getElement('div.body');
			if (oBody){
				iAppliedHeight = iAppliedHeight==0?oBody.offsetHeight+iUlHeight-li.offsetHeight:iAppliedHeight;
				oBody.setStyle('height',iAppliedHeight);
			}
		},this);
	},
	
	plugEventH:function(){
		
		/*
		- ajout des �v�nement au click sur les boutons (1 parametre peut sp�cifier de combien je me d�pace d'�l�ment en cliquant)
		- on click � gauche je scroll vers la gauche de 1 �l�ment � chaque fois ou de param �l�ments
		- on click � droite je scroll vers la droite de 1 �l�ment � chaque fois ou de param �l�ments
		*/

		this.btnLeft.setStyles({
			'height':this.scrollMask.offsetHeight,
			'display': !this.options.infinity ?'none':''
		});
		this.btnRight.setStyle('height',this.scrollMask.offsetHeight);
		tmpObj = {
			'btnLeft':this.btnLeft,
			'scrollMask':this.scrollMask,
			'btnRight':this.btnRight
		}

		        function isArray(o) {
            return Object.prototype.toString.call(o) === '[object Array]';
        }
        
        
        if (window.Retraitement && isArray(window.Retraitement)) {
            window.Retraitement.push(tmpObj);
        }
        else { 
            window.Retraitement = [];
                   window.Retraitement.push(tmpObj);
       }
	
	
		this.ok=true;
		
		this.fx.setOptions({
				onComplete:function(){
					if (this.options.infinity){
						if (this.currentLeft<=this.maxPos) {
							this.ul.setStyle('left',this.startPos);
							this.currentLeft = this.startPos;
						}else if (this.currentLeft>=0){
							this.ul.setStyle('left',this.endPos);
							this.currentLeft = this.endPos;
						}
					}else{
						switch(this.currentLeft){
							case this.endPos:
								this.btnRight.setStyle('display','none');
								this.btnLeft.setStyle('display','');
							break;
							case this.startPos:
								this.btnLeft.setStyle('display','none');
								this.btnRight.setStyle('display','');
							break;
							default:
								this.btnLeft.setStyle('display','');
								this.btnRight.setStyle('display','');
							break;
						}
					}
					this.ok = true;
				}.bind(this)
			});
		
		this.btnRight.addEvent('click', function(e){
			e = new Event(e); e.stop();
			if (this.ok){
				this.currentLeft = this.currentLeft-(this.liWidth*this.moveBlocks);
				this.ok = false;
				this.fx.start(this.currentLeft);
			}
		}.bind(this));

		this.btnLeft.addEvent('click', function(e){
			e = new Event(e); e.stop();
			if (this.ok){
				this.currentLeft = this.currentLeft+(this.liWidth*this.moveBlocks);
				this.ok = false;
				this.fx.start(this.currentLeft);
			}
		}.bind(this));
		
	},
	
	sizingV:function(){
		/*
		- blockWidth : calcule la taille d'un block en fonction de (scrollWidth/showElms)-sep;
		- appliquer blockWidth sur tous les li
		- ulWidth : calcule la taille total du UL, connaissant la taille de chaque li + le s�parateur = ((blockWidth+sep)*totalElms)-sep;
		*/
		
		this.fx = new Fx.Style(this.ul,'top',{duration:500,wait:true});
		
		if (window.ie){
			this.lis.each(function(li,i){
				li.setStyle('margin-bottom',-3);
			});
		}
		this.liHeight = window.ie?this.lis[this.showElms].offsetHeight-3:this.lis[this.showElms].offsetHeight;
		this.scrollMask.setStyle('height',(this.liHeight*this.showElms));
		
		if (this.options.infinity){
			this.startPos = this.currentTop = -(this.liHeight*this.showElms);
			this.endPos = -(this.liHeight*(this.totalElms-(this.showElms*2)));
			this.maxPos = -this.liHeight*(this.totalElms-this.showElms);
		}else{
			this.startPos = this.currentTop = 0;
			this.endPos = this.maxPos = -this.liHeight*(this.totalElms-this.showElms);
		}
		
		this.ul.setStyle('top', this.startPos);
	},
	
	plugEventV:function(){
		/*
		- ajout des �v�nement au click sur les boutons (1 parametre peut sp�cifier de combien je me d�pace d'�l�ment en cliquant)
		- on click � gauche je scroll vers la gauche de 1 �l�ment � chaque fois ou de param �l�ments
		- on click � droite je scroll vers la droite de 1 �l�ment � chaque fois ou de param �l�ments
		*/
		
		if (!this.options.infinity) this.btnLeft.setStyle('visibility', 'hidden');
		this.ok=true;
		
		this.fx.setOptions({
				onComplete:function(){
					if (this.options.infinity){
						if (this.currentTop<=this.maxPos) {
							this.ul.setStyle('top',this.startPos);
							this.currentTop = this.startPos;
						}else if (this.currentTop>=0){
							this.ul.setStyle('top',this.endPos);
							this.currentTop = this.endPos;
						}
					}else{
						switch(this.currentTop){
							case this.endPos:
								this.btnRight.setStyle('visibility','hidden');
								this.btnLeft.setStyle('visibility','');
							break;
							case this.startPos:
								this.btnLeft.setStyle('visibility','hidden');
								this.btnRight.setStyle('visibility','');
							break;
							default:
								this.btnLeft.setStyle('visibility','');
								this.btnRight.setStyle('visibility','');
							break;
						}
					}
					this.ok = true;
				}.bind(this)
		});
		
		this.btnRight.addEvent('click', function(e){
			e = new Event(e); e.stop();
			if (this.ok && this.btnRight.getStyle('visibility') != 'hidden'){
				this.currentTop = this.currentTop-(this.liHeight*this.moveBlocks);
				this.ok = false;
				this.fx.start(this.currentTop);
			}
		}.bind(this));
		
		this.btnLeft.addEvent('click', function(e){
			e = new Event(e); e.stop();
			if (this.ok && this.btnLeft.getStyle('visibility') != 'hidden'){
				this.currentTop = this.currentTop+(this.liHeight*this.moveBlocks);
				this.ok = false;
				this.fx.start(this.currentTop);
			}
		}.bind(this));
	},
	
	gotoId:function(id){
		if (!id) return;
		var nbrLi=0;
		if ($type(id) == 'string'){
			var parentLi = $(id);
			if (!parentLi) return;
			if (parentLi.nodeName != 'LI'){
				parentLi = parentLi.parentNode;
				while(parentLi.nodeName!='LI'){
					parentLi = parentLi.parentNode;
				}
			}
			var parentUl = parentLi.parentNode;
			parentUl.getChildren().each(function(elm,i){
				if (elm === parentLi)
					nbrLi = i+1;
			});
		}else if ($type(id) == 'number'){
			if (id<=0 || id > this.lis.length) return;
			nbrLi = id;
		}
		
		if (this.type=='H'){
			this.currentLeft = this.startPos-(this.liWidth*(nbrLi-1));
			if (this.fx)
				this.fx.start(this.currentLeft);
		}else if(this.type=='V'){
			this.currentTop = this.startPos-(this.liHeight*(nbrLi-1));
			if (this.fx)
				this.fx.start(this.currentTop);
		}
	}
});
blockScroll.implement(new Options);

function resizeTD(){
	if($E('body').hasClass('home'))return;
	var tables = $$('table.mainStructure');
	tables.extend($$('table.tableStruct'));
	
	tables.each(function(table,i){
		var tWidth = table.parentNode.offsetWidth;
		var trs = table.getElements('tr');
		var tds = trs[0].getChildren();
		var seps = [];
		tds.each(function(td,k){
			if(td && (td.hasClass('sep') || td.hasClass('sepLg') || td.hasClass('sepExlg') || td.hasClass('sepSm'))){
				seps.push(td);
				tWidth -= td.getStyle('width').toInt();
				tds.remove(td);
			}
		});
		var tdLength = tds.length;
		tds.each(function(td,k){
			tdWidth = Math.floor(tWidth/tdLength);
			if(!td.style.width && !!!td.className.match(/\b^size.*/)){
				td.style.width = tdWidth+'px';
			}
		})
	});
}

/*************************************************
********* nav left v2.0
****************************************************/

var myLeftNav = new Class({
	initialize : function(id) {
		if(!$(id)) return;
		var that = this;
		this.UL = $(id);
		this.LIS = $$( "#" + id + " li.cat");
		this.objs = [];
		this.LIS.each(function (elm, i){
			var obj = new navItem(elm, i);
			that.objs.push(obj);
		})
		$(id).setStyle('visibility', 'visible');
	},
	
	dispatch : function (obj, li){
		if(li.className.match(/open/)) {
			obj.hideOne(li);
			return;
		}
		this.objs.each(function (ob, i){
			ob.hideAll();
		})
		obj.openOne(li)
		
	}
});


var depliable = new Class({
	initialize : function(id) {
		if(!$(id)) return;
		var that = this;
		this.UL = $(id);
		this.LIS = $$( "#" + id + " li.cat");
		this.objs = [];
		this.LIS.each(function (elm, i){
			var obj = new navItem(elm, i);
			that.objs.push(obj);
		})
		$(id).setStyle('visibility', 'visible');
	},
	
	dispatch : function (obj, li){
		if(li.className.match(/open/)) {
			obj.hideOne(li);
			return;
		}
		this.objs.each(function (ob, i){
			ob.hideAll();
		})
		obj.openOne(li)
		
	}
});

var navItem = new Class({
	initialize : function(elm, i) {
		if(!elm) return;
		var that = this;
		this.index = i;
		this.btns = elm.getElements('span.navBtn');
		this.lastLevels = elm.getElements('ul');
		this.lis = elm.getElements('li');
		this.as = elm.getElements('a');
		this.activeLis = [];
		// evts
/*		this.lastLevels.each(function(el, i){
			// ul de dernier niveau
			if(el.getElements('ul').length == 0) {
				el.className += " lastLevel";
			}
		});*/
		this.lis.each(function (el, i){
			if(!el.parentNode.className.match(/lastLevel/)) {
				that.activeLis.push(el);
			}
			else {
				var a = el.getElements("a")[0];
				if(a.title == "" || !a.title) a.title = a.innerHTML;
				// troncature � 16 si >18
				if(a && a.innerHTML.length > 20) a.innerHTML = a.innerHTML.slice(0,17)+"[...]";
			}
		})
		this.btns.each(function(el, i){
			el.addEvent("click", function(e){
				new Event(e).stop();
				var li = el.getParent();
				navManager.dispatch(that, li);
			})
		});
		this.as.each(function(el, i){
			el.addEvent("focus", function(e){
				//new Event(e).stop();
				var li = el.getParent();
				navManager.dispatch(that, li);
			})
		});
		// chk
		this.checkCurrent();
	},
	
	hideOne : function (li){
		li.className = li.className.replace(/\bopen\b/, "close");
	},
	
	openOne : function (li){
		var that = this;
		li.className = li.className.replace(/\bclose\b/, "open");
		var parent = li.getParent().getParent();
		if(parent && !li.className.match("cat")){that.openOne(parent);}
		
	},

	hideAll : function (){
		this.activeLis.each(function (el, i){
			if(!el.className.match(/\bclose\b/)) el.className = el.className.replace(/\bopen\b/, "close");
		})
	},
	
	dispatch : function (li){
		var that = this;
		this.hideAll();
		this.openOne(li);
		return;
		this.activeLis.each(function(elm){
			if(elm != li )that.hideOne(li);
			else that.openOne(li);			
		})
	},
	
	checkCurrent : function (){
		
		this.as.each(function (el, i){
			if(el.className.match(/current/)) this.openOne(el.getParent());
		}.bind(this));
	}
});




/*******************************************************************************************************************
/ ------------------------------------------		tooltip
********************************************************************************************************************/

var tooltip = {
	init: function (){
		tooltip.nodes = $$('.tips');
		tooltip.target = $('tooltip');
		if(!tooltip.taregt) return;
		tooltip.btn = $('tooltipClose');
		tooltip.target.setOpacity(0);
		this.fx = new Fx.Style(tooltip.target, 'opacity', {wait:false, duration:300});
		//
		tooltip.nodes.each(function(elm){
			elm.addEvent("mouseover", function(e){
				new Event(e).stop();
				tooltip.initialize(this)
			})
			elm.addEvent("mousemove", function(e){
				new Event(e).stop();
				tooltip.animate(this)
			})
			elm.addEvent("mouseout", function(e){
				new Event(e).stop();
				tooltip.unanimate()
			})
		});
		tooltip.target.addEvent('mouseover', function(){
			tooltip.over = true;
		})
		tooltip.target.addEvent('mouseout', function(){
			tooltip.unanimate()
			tooltip.over = false;
		})
		tooltip.btn.addEvent('click', function (e){
			new Event(e).stop();
			tooltip.over = false;
			tooltip.unanimate();			
		});
	},

	initialize : function (elm){
		tooltip.target.setStyle('display', "block");
		$$("#tooltipInner p")[0].innerHTML = elm.parentNode.getElements("div.tooltipHide")[0].innerHTML;
		tooltip.target.style.left = getLeft(elm)+10+"px";
		tooltip.target.style.top = getTop(elm)-$('tooltip').offsetHeight+"px";
		this.fx.start('1');
	},

	animate : function (e, elm){
		//tooltip.target.style.left = e.clientX-69+"px";
	},

	unanimate: function (){
		setTimeout(function(){
			if(!tooltip.over) tooltip.fx.start('0');
		}, 200)
		//myAnim.fadeOut("tooltip", 0.5);
		//bytefx.fade(tooltip.target, 100, 0, 10);
	}
}
/**
/ renvoie le left et le top d'un elm
**/
function getLeft(MyObject){
    if (MyObject.offsetParent)
	return (MyObject.offsetLeft + getLeft(MyObject.offsetParent));
    else
	return (MyObject.offsetLeft);
    }
function getTop(MyObject){
    if (MyObject.offsetParent)
	return (MyObject.offsetTop + getTop(MyObject.offsetParent));
    else
	return (MyObject.offsetTop);
    }

    
/*******************************************************************************************************************
/ ------------------------------------------		layer
********************************************************************************************************************/

var Layer = new Class({
	initialize : function() {
		if(!$('layer')) return;
		this.targetBKG = $('layerMask');
		this.targetBKG.setStyle('width', document.documentElement.scrollWidth);
		this.targetBKG.setStyle('height', document.documentElement.scrollHeight);
		this.targetBKG.setOpacity(0.8);
	},
	show : function(obj){
		if ($(obj) && $(obj).hasClass('hidden')){
			$(obj).removeClass('hidden');
		}
	},
	hide : function(obj){
		if ($(obj) && !$(obj).hasClass('hidden')){
			$(obj).addClass('hidden');
		}
	}
});

/********************************
*** position de la nav gauche MDR
*********************************/
function positionLeftColMDR(){
	if(!$$('.mdr #leftCol')[0]) return;
	var a = $$('#leftCol div.block')[0].getCoordinates().top;
	if ($$('.blockTabs')[0]) {
		var b = $$('div.blockTabs')[0].getCoordinates().top;
		$$('#leftCol div.block')[0].setStyle('margin-top', (b-a)-2);
	}
}

/********************************
*** function write the flash
*********************************/
var loadFlash = {
	o : [],
	push : function(obj, id){
		loadFlash.o.push({'id':id,'so':obj});
	},
	load : function(){
		for (var i=0, j=loadFlash.o.length; i<j; i++){
			loadFlash.o[i].so.write(loadFlash.o[i].id);
		}
	}
}

/**
***********************************************************************************
priceSlider:Object,
priceSlider recoit un tableau de prix ordonn� ou non
renvoie le nbre de pdts correspondants � la fourchette + la fourchette basse + la fourchette haute
***********************************************************************************
**/

var priceSlider = {
    init: function(id, min, max){
		var so = new SWFObject("../swf/priceSlider.swf", 'priceSlider_swf', '158', '50', "7");
		so.addParam("wmode", "transparent");
		so.addParam("AllowScriptAccess", "always");
		so.addVariable("minArray", min);
		so.addVariable("maxArray", max);
		so.write(id);
		this.SWF = (window.ie) ? window["priceSlider_swf"] : document["priceSlider_swf"]; // mootools window.ie
	},
	// feed AS
	load: function(arr){this.SWF.LOAD(arr)}, // envoie un nouveau tableau au slider qui se rafraichit
	// feedback AS
	getInfos : function (length, min, max){
		$('num_found').setText(length+" produits trouv\351s entre "+min+" Euros et "+max+" Euros");
	}
}

/**
***********************************************************************************
filterMDR:Object
gere les filtres du Moteur de recherche
different du tri du MDR
***********************************************************************************
**/

var filterMDR = {
	init: function (){
		this.targets = $$('div.filtreur');
		this.divs = $$('div.filtreur div');
		this.btnsUp = $$('a.up');
		this.btnsDw = $$('a.down');
		var filtreur = this;
		this.targets.each(function(elm,i){
			elm.cursorOut = true;
			var span = elm.getElements('span')[0];
			var div = elm.getElements('div')[0];
			elm.addEvent('click', function (e){
				filtreur.maskOther(div);
				div.removeClass('hidden');
			})
			elm.addEvent('mouseenter', function (e){
				elm.cursorOut = false;
			})
			elm.addEvent('mouseleave', function (e){
				(new Event(e)).stop();
				elm.cursorOut = true;
				timeout = setTimeout(function(){if(elm.cursorOut) div.addClass('hidden')}, 1000);
			})
		})
		this.btnsUp.each(function(elm){
			elm.addEvent('click', function (){
				//filtreur.checkSort(this, 1);
			})
		});
		this.btnsDw.each(function(elm){
			elm.addEvent('click', function (){
				//filtreur.checkSort(this, -1);
			})
		});
	},
	
	maskOther : function (elm){
		this.divs.each(function(el){
			if (el != elm) el.addClass('hidden');
		});
	}
}

var setProduct = {
	init: function(){
		var inputs = $$('input.nbrProduit');
		var span = new Element('span',{'class':'txtM'});
		
		inputs.each(function(input,i){
			if (input.getPrevious() && input.getPrevious().hasClass('btnMoinsProduit')){
				input.getPrevious().remove();
			}
			if (input.getNext() && input.getNext().hasClass('btnPlusProduit')){
				input.getNext().remove();
			}
			var btnPlus = span.clone().setHTML("<a href='#'>+</a>");
			var btnMoins = span.clone().setHTML("<a href='#'>-</a>");
			btnPlus.addClass('btnPlusProduit');
			btnMoins.addClass('btnMoinsProduit');
			
			btnPlus.addEvent('click',function(e){
				e = new Event(e); e.stop();
				if (input.value<20){
					input.value++;
				}
			});
			
			btnMoins.addEvent('click',function(e){
				e = new Event(e); e.stop();
				if (input.value>1){
					input.value--;
				}
			});
			
			btnMoins.injectBefore(input);
			btnPlus.injectAfter(input);
		});
	}
}
function eraseMail(oInput){
	if(oInput.value=='Votre email'||oInput.value=='Email'||oInput.value=='mdp'||oInput.value==''||oInput.value==''||oInput.value=='') {
		oInput.value='';
	}
}
function fillTextMail(oInput){
	if(oInput.value==='') {
		if(oInput.name==='logon'){
			oInput.value='Votre email';
		}
		else if(oInput.name==='password'){
			oInput.value='';
		}
	}
}
function addTextLoginInput(){
	var loginUsername = $('username');
	var loginPassword = $('password');
	if(loginUsername){
		fillTextMail(loginUsername);
		loginUsername.addEvent('focus', function() {eraseMail(loginUsername);});
		loginUsername.addEvent('blur' , function() {fillTextMail(loginUsername);});
	}
	if(loginPassword){
		fillTextMail(loginPassword);
		loginPassword.addEvent('focus', function() {eraseMail(loginPassword);});
		loginPassword.addEvent('blur' , function() {fillTextMail(loginPassword);});
	}
}

function checkPopLayer(){
	var cloneElm;
	var layers = function(element){
		var elm = $(element);
		if (elm){
			cloneElm = elm.clone();
			elm.remove();
			return layers(element);
		}else{
			return cloneElm;
		}
	}
	
	var layer, newLayer, injected;
	if($('popLayerMask')){
		layer = layers('popLayerMask');
		newLayer = layer.clone();
		injected = newLayer.injectInside($(document.body));
		if ($('pied')){
			var coord = $('pied').getCoordinates();
			if (coord.bottom<=document.documentElement.clientHeight){
				injected.setStyles({
					'width':'100%',
					'height':'100%'
				});
			}else{
				injected.setStyles({
					'width':'100%',
					'height': coord.bottom
				});
			}
		}
	}
	
	if($('popLayer')){
		layer = layers('popLayer');
		newLayer = layer.clone();
		newLayer.injectInside($(document.body));
	}
}

var navUltraH = {
	obj: {},
	hoverClassName: 'hover',
	timer: null,
	show: function(oItem)
	{
		clearTimeout(oItem.interval);
		oItem.item.className+= ' ' + navUltraH.hoverClassName;
		ifrlayer.make(oItem.item.getElementsByTagName('div')[0]);
	},
	hide: function(oItem)
	{
		oItem.interval = setTimeout(
			function()
			{
				return function(){
					oItem.item.className = oItem.item.className.replace(new RegExp("\\b" + navUltraH.hoverClassName + "\\b", "g"), "");
					ifrlayer.hide(oItem.item.getElementsByTagName('div')[0]);
				}
			}(oItem)
			,250
		);
	},
	init: function(obj)
	{
		if(!obj)
		{
			navUltraH.obj.nav = document.getElementById('navUltraHaute');
		}
		else
		{
			navUltraH.obj.nav = obj;
		}
		if(navUltraH.obj.nav)
		{
			navUltraH.obj.oItem = {};
			navUltraH.obj.oItem[navUltraH.obj.nav.id] = {};
			navUltraH.obj.oItem[navUltraH.obj.nav.id].aItem = [];
			navUltraH.obj.aNavItem = $(navUltraH.obj.nav).getElements('.nuhItem');
			navUltraH.obj.aNavItem.each(
				function(item,index)
				{
					navUltraH.obj.oItem[navUltraH.obj.nav.id].aItem[index] = {'item':item};
					navUltraH.obj.oItem[navUltraH.obj.nav.id].aItem[index].item.addEvent(
						'mouseenter',
						function(oTmp)
						{
						  return function()
							{
								navUltraH.show(oTmp);
							}
						}(navUltraH.obj.oItem[navUltraH.obj.nav.id].aItem[index])
					);
					navUltraH.obj.oItem[navUltraH.obj.nav.id].aItem[index].item.addEvent(
						'mouseleave',
						function(oTmp)
						{
						  return function()
							{
								navUltraH.hide(oTmp);
							}
						}(navUltraH.obj.oItem[navUltraH.obj.nav.id].aItem[index])
					);
				}
			);
		}
	}
};

// ENCAPSULATION DU SCRIPT
(function() {

	// FONCTION D'AJOUT DE GESTIONNAIRE D'EVENEMENT
	var connect = function(oEl, sEvType, fn, bCapture) {
		return document.addEventListener ?
			oEl.addEventListener(sEvType, fn, bCapture || false) :
				oEl.attachEvent ?
					oEl.attachEvent('on' + sEvType, fn) :
					false;
	};

	// STOPPE LA PROPAGATION D'UN EVENEMENT ET ANNULE L'ACTION PAR DEFAUT DE LA CIBLE
	var stop = function(e) {
		if(e && e.stopPropagation && e.preventDefault) {
			e.stopPropagation();
			e.preventDefault();
		}
		else if(e && window.event) {
			window.event.cancelBubble = true
			window.event.returnValue = false;
		}
		return false;
	};

	// INSTANCE DE GESTION DE CLASSES CSS
	var css = {
		remove: function(oEl, sClass) {
			var rep = oEl.className.match(' ' + sClass) ?
				' ' + sClass :
				sClass;
			oEl.className = oEl.className.replace(rep, '');
		},
		add: function(oEl, sClass) {
			if(!css.has(oEl, sClass)) {
				oEl.className += oEl.className ?
					' ' + sClass :
					sClass;
			}
		},
		has: function(oEl, sClass) {
			return new RegExp('\\b' + sClass + '\\b').test(oEl.className);
		}
	};

	// CLASSE MENU - NAVH DE CARREFOUR
	var Menu = function() {};

	// PROTOTYPE DE LA CLASSE MENU
	var Mp = Menu.prototype = {
		menus: [],
		show: 'show',
		active: 'active',
		// IDENTIFIE TOUS LES MENUS AYANT LA CLASSE MP.LABEL ET LES AJOUTE AU TABLEAU MP.MENUS
		identify: function(oMenu) {
			Mp.menus.push(oMenu);
			return Mp;
		},
		// VERIFIE SI UN ELEMENT EST CONTENU DANS UN AUTRE
		contains: function(container, containee) {
			if(!container || !containee) {
				return;
			}
			for(var n = containee; n && n != container; n = n.parentNode);
			return n;
		},
		// INITIALISE UN MENU
		init: function(oMenu) {
			var aLis = oMenu.getElementsByTagName('li');
			if(aLis)
			{
				var iLi = aLis.length;
				while(iLi-- > 0) {
					css.remove(aLis[iLi], Mp.show);
					css.remove(aLis[iLi].getElementsByTagName('a')[0], Mp.active);
				}
			}
		},
		
		addBehaviour: function(waza) {
		    /* :PUBLICIS: */
		    var _ul = document.getElementById('navh').getElementsByTagName('ul')[0];
		    var _li = $(_ul).getChildren('li');
            var _div = [];
            
		    _li.addEvent('mouseover',function(){
		        var current = this;
		        var content = current.getElements('.contenu')[0];
		        current.addClass('show');
		        content.addClass('open');
		    });
		    
		    _li.addEvent('mouseout',function(){
		        var current = this;
		        var content = current.getElements('.contenu')[0];
		        current.removeClass('show');
		        content.removeClass('open');
		    });
		    
		}
	};

	// CLASSE D'ANIMATION DE LA HAUTEUR
	var AnimHeight = function() {};
	
	// PROTOTYPE DE LA CLASSE ANIMHEIGHT
	var Hp = AnimHeight.prototype = {
		start: function(oEl, oParam, sAnim) {
			if(!oParam.start) {
				oParam.end = oEl.scrollHeight;
				oParam.start = (new Date()).getTime();
			}
			return Hp.middle(oEl, oParam, sAnim);
		},
		middle: function(oEl, oParam, sAnim) {
			if(!oParam.start) return;
			var iNow = (new Date()).getTime();
			var iDiff = oParam.start - iNow;
			if(oParam.onStart == 0 && iNow < (oParam.start + oParam.interval) && parseInt(oEl.style[oParam.prop]) < oParam.end) {
				var iValue = Math.abs(parseInt(iDiff * oParam.end / oParam.interval));
				oEl.style[oParam.prop] = iValue + 'px';
			}
			else if(oParam.onStart != 0 && iNow < (oParam.start + oParam.interval) && parseInt(oEl.style[oParam.prop]) > 0) {
				var iValue = Math.abs(parseInt(iDiff * oParam.end / oParam.interval));
				oEl.style[oParam.prop] = (parseInt(oEl.style[oParam.prop]) > iValue ? parseInt(oEl.style[oParam.prop]) - iValue : 0) + 'px';
			}
			else Hp.end(oEl, oParam, sAnim);
		},
		end: function(oEl, oParam, sAnim) {
			oEl.style[oParam.prop] = oParam.onComplete;
			if(parseInt(oEl.style[oParam.prop]) == 0) {
				css.remove(oEl.parentNode, Mp.show);
				css.remove(oEl.parentNode.getElementsByTagName('a')[0], Mp.active);
			}
			else if(parseInt(oEl.style[oParam.prop]) != 0) {
				css.add(oEl.parentNode, Mp.show);
				css.add(oEl.parentNode.getElementsByTagName('a')[0], Mp.active);
			}
			return clearInterval(oEl[sAnim]);
		},
		initialize: function(oEl, oParam, iKey) {
			var sAnim = 'anim' + iKey;
			oEl.style[oParam.prop] = oParam.onStart + 'px';
			if(oEl[sAnim]) clearInterval(oEl[sAnim]);
			return oEl[sAnim] = setInterval(function() { return new Hp.start(oEl, oParam, sAnim); }, 1);
		}
	};

	// CLASSE D'ANIMATION DE LA LARGEUR
	var AnimWidth = function() {};
	
	// PROTOTYPE DE LA CLASSE ANIMWIDTH
	var Wp = AnimWidth.prototype = {
		start: function(oEl, oParam, sAnim) {
			if(!oParam.start) {
				oParam.start = (new Date()).getTime();
				oParam.end = parseInt(oEl.style[oParam.prop]);
			}
			return Wp.middle(oEl, oParam, sAnim);
		},
		middle: function(oEl, oParam, sAnim) {
			if(!oParam.start) return;
			var iNow = (new Date()).getTime();
			var iDiff = oParam.start - iNow;
			if(iNow < (oParam.start + oParam.interval) && parseInt(oEl.style[oParam.prop]) > 0) {
				var iValue = Math.abs(parseInt(iDiff * oParam.end / oParam.interval));
				oEl.style[oParam.prop] = (parseFloat(oEl.style[oParam.prop]) > iValue ? parseFloat(oEl.style[oParam.prop]) - iValue : 0) + 'px';
			}
			else Wp.end(oEl, oParam, sAnim);
		},
		end: function(oEl, oParam, sAnim) {
			oEl.style[oParam.prop] = '-1' + 'px';
			oEl.style.zoom = '1';
			return clearInterval(oEl[sAnim]);
		},
		initialize: function(oEl, oParam, iKey) {
			var sAnim = 'anim' + iKey;
			oEl.style[oParam.prop] = oParam.onStart + '%';
			if(oEl[sAnim]) clearInterval(oEl[sAnim]);
			return oEl[sAnim] = setInterval(function() { return new Wp.start(oEl, oParam, sAnim); }, 1);
		}
	};
	
	// CHOIX D'ARTICLE (SYSTEME A GLISSIERE) - NAV SECONDAIRE DE OOSHOP
	var ChoiceArticle = function() {};
	
	// PROTOTYPE DE LA CLASSE CHOICEARTICLE
	var Cp = ChoiceArticle.prototype = {
		initialize: function() {
			var aDivs = document.getElementsByTagName('div'), iDiv = aDivs.length;
			while(iDiv-- > 0) if(css.has(aDivs[iDiv], 'choiceArticle')) Cp.initBloc(aDivs[iDiv]);
		},
		initBloc: function(oCont) {
			if(!css.has(oCont.getElementsByTagName('div')[0], 'animPos')) return;
			return Cp.getCtrl(oCont);
		},
		getCtrl: function(oCont) {
			var aDivs = oCont.getElementsByTagName('div'), iDiv = aDivs.length, aConts = [];
			while(iDiv-- > 0) {
				if(css.has(aDivs[iDiv], 'second')) {
					var oCtrlCont = aDivs[iDiv];
				}
				if(css.has(aDivs[iDiv], 'third')) {
					aConts.push(aDivs[iDiv]);
				}
			}
			aConts.reverse();
			if(!oCtrlCont) return;
			var aAs = oCtrlCont.getElementsByTagName('a'), iA = aAs.length;
			if(iA != aConts.length) return;
			while(iA-- > 0) connect(aAs[iA], 'click', Cp.openCont(aConts, oCtrlCont, aAs[iA], iA));
		},
		initHeight: function(oCurrentCont) {
			var oBody = oCurrentCont.parentNode;
			var aDivs = oBody.getElementsByTagName('div'), iDiv = aDivs.length;
			while(iDiv-- > 0)
				if(css.has(aDivs[iDiv], 'first')) {
					var iFirst = aDivs[iDiv];
					aDivs[iDiv].removeAttribute("style");
					var iFirstHeight = aDivs[iDiv].scrollHeight+$(aDivs[iDiv]).getStyle("margin-bottom").toInt();
				}
				else if(css.has(aDivs[iDiv], 'second')) {
					var iSecond = aDivs[iDiv];
					aDivs[iDiv].removeAttribute("style");
					var iSecondHeight = aDivs[iDiv].scrollHeight+$(aDivs[iDiv]).getStyle("margin-bottom").toInt();
				}
			if(iFirstHeight && iSecondHeight && oCurrentCont) {
				oCurrentCont.removeAttribute("style");
				var iCurrentHeight = oCurrentCont.scrollHeight+$(oCurrentCont).getStyle("margin-bottom").toInt();
				iHeight = iFirstHeight > iSecondHeight ? iFirstHeight : iSecondHeight;
				if(iHeight < iCurrentHeight) iHeight = iCurrentHeight;
				iFirst.style.height = iHeight+ 'px';
				iSecond.style.height = iHeight + 'px';
				oCurrentCont.style.height = iHeight + 'px';
				return iHeight;
			}
			return false;
		},
		openCont: function(aConts, oCtrlCont, oCtrl, iKey) {
			return function(e) {
				var aLis = oCtrlCont.getElementsByTagName('li'), iLi = aLis.length;
				while(iLi-- > 0) css.remove(aLis[iLi], 'current');

				var iCont = aConts.length;
				while(iCont-- > 0) css.add(aConts[iCont], 'hiddenBlock');

				var oOpenCont = aConts[iKey];
				css.add(oCtrl.parentNode, 'current');
				// Cp.initHeight(aConts[iKey]);
				oOpenCont.parentNode.parentNode.style.height = Cp.initHeight(aConts[iKey]) + 'px';
				oOpenCont.style.position = 'absolute';
				oOpenCont.style.right = 66.6 * (oOpenCont.parentNode.scrollWidth) / 100 + 'px';
				oOpenCont.style.zIndex = '2';
				css.remove(oOpenCont, 'hiddenBlock');
				var aParams = [{'prop':'right', 'onStart':parseInt(oOpenCont.style.right), 'onComplete':0, 'interval':1000}];
				var iParam = aParams.length;
				oOpenCont.parentNode.parentNode.style.height = iHeight + 'px';
				while(iParam-- > 0) (new AnimWidth()).initialize(oOpenCont, aParams[iParam], iParam);

				return stop(e);
			}
		}
	};

	// TOOLTIPS VOLANTES
	var FlyTips = function() {};
	
	// PROTOTYPE DE LA CLASSE FLYTIPS
	var Fp = FlyTips.prototype = {
		'label': 'flytip',
		'label2': 'lienTips',
		initialize: function() {
			var sLabel = Fp.label;
			var sLabel2 = Fp.label2;
			var aAs = document.getElementsByTagName('a'), iA = aAs.length;
			while(iA-- > 0) {
				if(css.has(aAs[iA], sLabel)) {
					connect(aAs[iA], 'mouseover', Fp.displayFlyTip(aAs[iA].getElementsByTagName('img')[0].alt));
					connect(aAs[iA], 'mousemove', Fp.moveTip());
					connect(aAs[iA], 'mouseout', Fp.hideTip());
				}
				if(css.has(aAs[iA], sLabel2)) {
					var aDivs = document.getElementsByTagName('div'), iDiv = aDivs.length;
					while(iDiv-- > 0)
						if(css.has(aDivs[iDiv], aAs[iA].rel)) {
							var oFlyTip = aDivs[iDiv];
							break;
						}
					if(!oFlyTip) continue;
					css.add(oFlyTip, 'flyTips');
					oFlyTip.style.top = $(aAs[iA]).getCoordinates().top + 'px';
					oFlyTip.style.left = $(aAs[iA]).getCoordinates().left + 'px';
					var oP = document.createElement('p');
					css.add(oP, 'pointe');
					var oSpan = document.createElement('span');
					oP.appendChild(oSpan);
					oP.style.marginBottom = '-1px';
					oFlyTip.appendChild(oP);
					connect(aAs[iA], 'mouseover', Fp.displayTip(oFlyTip));
					connect(aAs[iA], 'mouseout', Fp.removeTip(oFlyTip));
					connect(oFlyTip, 'mouseover', Fp.killTimer(oFlyTip));
					connect(oFlyTip, 'mouseout', Fp.revangeTimer(oFlyTip));
				}
			}
		},
		displayTip: function(oFlyTip) {
			return function(e) {
				var oSrc = e.target || e.srcElement;
				if(oFlyTip) {
					try {
						if(oFlyTip.timer) clearTimeout(oFlyTip.timer);
						if(css.has(oFlyTip, 'hidden')) css.remove(oFlyTip, 'hidden');
						// add
						if(css.has(oFlyTip, 'flyBottom')) {
							var coo = $(oSrc).getCoordinates();
							oFlyTip.style.marginTop =  coo.height + 15 + 'px';
							if(window.gecko) oFlyTip.style.marginTop = coo.height * 2 + 20 + 'px';
							//oFlyTip.style.top =  coo.top + 'px';
						}
						else {
							oFlyTip.style.marginTop = - oFlyTip.offsetHeight - 5 + 'px';
						}
						// oFlyTip.style.marginTop = - oFlyTip.offsetHeight + 'px';
					} catch(e) {};
				}
			};
		},
		removeTip: function(oFlyTip) {
			return function(e) {
				var oSrc = e.target || e.srcElement;
				if(oFlyTip) oFlyTip.timer = setTimeout(function() { return css.add(oFlyTip, 'hidden'); }, 1000);
			};
		},
		killTimer: function(oFlyTip) {
			return function() {
				if(oFlyTip && oFlyTip.timer) clearTimeout(oFlyTip.timer);
			};
		},
		revangeTimer: function(oFlyTip) {
			return function() {
				oFlyTip.timer = setTimeout(function() { return css.add(oFlyTip, 'hidden'); }, 1000);
			};
		},
		displayFlyTip: function(sMsg) {
			if(!document.getElementById('flytip')) {
				var oCont = document.createElement('div');
				oCont.id = 'flytip';
				css.add(oCont, 'hidden');
				var oP = document.createElement('p');
				var oTxt = document.createTextNode(sMsg);
				var oP2 = document.createElement('p');
				var oSpan = document.createElement('span');
				css.add(oP2, 'pointe');
				oP.appendChild(oTxt);
				oP2.appendChild(oSpan);
				oCont.appendChild(oP);
				oCont.appendChild(oP2);
				document.body.appendChild(oCont);
			}
			return function(e) {
				var oCont = document.getElementById('flytip');
				css.remove(oCont, 'hidden');
				return oCont.getElementsByTagName('p')[0].innerHTML = sMsg;
			}
		},
		moveTip: function() {
			return function(e) {
				var oCont = document.getElementById('flytip');
				oCont.getElementsByTagName('p')[1].style.left = oCont.scrollWidth / 2 - 7 + 'px';
				e = new Event(e);// Necessite Mootools
				oCont.style.top = e.page.y - 60 + 'px';
				oCont.style.left = e.page.x - oCont.scrollWidth / 2 + 3 + 'px';
			}
		},
		hideTip: function() {
			var oCont = document.getElementById('flytip');
			return function() {
				return css.add(oCont, 'hidden');
			};
		}
	};

	// CLASSE DE REDIMENSIONNEMENT VERTICAL DE BLOCS
	// Mettre une classe "noresize" sur les blocs de classe "body" qui ne doivent pas etre redimensionnes.
	// Mettre une classe "offset" sur un element interne a un bloc de classe "body" pour que le redimensionnement du bloc s'effectue sur cet element via l'ajout d'un padding-top 
	var VerticalAlign = new Class({
		initialize: function() {
			this.maestro();
		},
		maestro: function() {}
	});
	
	VerticalAlign.implement({
		maestro: function() {
			this.lines = this.getLines();
			if(this.lines && !$('carrefourFr')) {
				this.setPaddings(this.lines);
			}
			this.trs = this.getTrs();
			if(this.trs) {
				this.setSpaces(this.trs);
			}
		},
		getLines: function() {
			if($('main')) {
				return $('main').getElements('div.line');
			}
		},
		getTrs: function() {
			if($('main')) {
				return $('main').getElements('table tr');
			}
		},
		// Pour les structures basees sur les div line
		setPaddings: function(aLines) {
			aLines.each(function(oLine) {
				oLine.height = oLine.getStyle('height').toInt();
				oLine.getElements('div.unit').each(function(oUnit) {
					oUnit.height = oUnit.getStyle('height').toInt();
					if(oUnit.height < oLine.height) {
						//alert(oUnit.height + ' ' + oLine.height);
						var iOffset = oLine.height - oUnit.height;
						var aBodies = oUnit.getElements('div.body');
						var iBodies = aBodies.length;
						var iPx = iOffset / iBodies;
						aBodies.each(function(oBody) {
							if(oBody.hasClass('mediaFullSize') || oBody.hasClass('noresize')) {
								return;
							}
							var oForm = oBody.getElement('div.offset');
							if(oForm) {
								oForm.setStyle('padding-top', (oForm.getStyle('padding-top')).toInt() + iOffset + 'px');
							}
							else {
								oBody.setStyle('padding-bottom', ((oBody.getStyle('padding-bottom')).toInt() + iPx).toInt() + 'px');
							}
						});
					}
				});
			});
		},
		// Pour les structures basees sur les tableaux
		setSpaces: function(aTrs) {
			aTrs.each(function(oTr) {
				if(oTr.hasClass('resizeByPTOnFooter')) {
					var aTds = oTr.getChildren();
					var iHeight = 0;
					aTds.each(function(oTd) {
						var aEls = oTd.getChildren();
						var iCurrHeight = 0;
						if(aEls.length > 1) {
							aEls.each(function(oEl) {
								iCurrHeight += oEl.getStyle('height').toInt();
							});
						}
						else if(aEls.length == 1) {
							iCurrHeight = aEls[0].getStyle('height').toInt();
						}
						iHeight = iCurrHeight > iHeight ? iCurrHeight : iHeight;
					});
					oTr.height = oTr.getStyle('height').toInt();
					
					var aTds = oTr.getElements('td');
					aTds.each(function(oTd) {
						var aTdChilds = oTd.getChildren();
						if(aTdChilds.length == 0 || oTd.getElements('table').length > 0) {
							return;
						}
						var iHeight = 0;
						var iOffset = 0;
						aTdChilds.each(function(oChild) {
							if($type(oChild) != 'element' || !oChild.hasClass('block')) {
								return;
							}
							iHeight += oChild.getStyle('height').toInt();
						});
						if(iHeight < oTr.height) {
							var iChild = aTdChilds.length;
							if(iChild == 0) {
								return;
							}
							if(iChild > 1) {
								iOffset = ((oTr.height - iHeight) / iChild).toInt();
								aTdChilds.each(function(oChild, iChild) {
									oChild.setStyle(
										'margin-bottom',
										(iChild == aTdChilds.length - 1 ? iOffset + iHeight % iChild + 'px' : iOffset + 'px')
									);
								});
							}
							else {
								/*if(aTdChilds[0].hasClass('blockSimple')) {
									return;
								}*/
								iOffset = (oTr.height - iHeight).toInt();
								if(aTdChilds[0].hasClass('noresize')) {
									return;
								}
								aTdChilds[0].setStyle('padding-bottom', iOffset + 'px');
							}
						}
					});
				}
			});
		}
	});
	
	window.addEvent('load', function() { new VerticalAlign(); });

	// CONTROLE DES PRERECQUIS ET INSTANCE DE MENU
	if(document.getElementsByTagName && document.getElementById && document.createElement) {
	// AJOUT DE LA CLASSE HASJS SUR L'ELEMENT HTML
		css.add(document.documentElement, 'hasJS');
		return connect(window, 'load', function() {
			// VERIFICATION DE LA PRESENCE DE CSS
			var oNewDiv = document.createElement('div');
			document.body.appendChild(oNewDiv);
			oNewDiv.style.visibility = 'hidden';
			oNewDiv.style.width = '20px';
			oNewDiv.style.padding = '10px';
			if(oNewDiv.offsetWidth != 40) {
				document.body.removeChild(oNewDiv);
				return css.remove(document.documentElement, 'hasJS');
			}
			document.body.removeChild(oNewDiv);
			if(document.getElementById('cDirect')) {
				// NAV SECONDAIRE OOSHOP
				(new ChoiceArticle()).initialize();
			}
			else {
				// NAVH CARREFOUR
				var oNavH = document.getElementById('navh');
				if (oNavH)
				{
					var oMenu = oNavH.getElementsByTagName('ul')[0];
                    (new Menu()).identify(oMenu).addBehaviour();
				}
			}
			// TOOLTIPS VOLANTES
			(new FlyTips()).initialize();
		});
	}

})();

/** FIN DE LA PARTIE SCRIPTATOUFER ****************************************************************************************************************************/

// Effacer toutes les checkboxes d'un formulaire

// id formulaire
function clearCheckboxes(){

if(!$('formToClear')) return;
var form=$('formToClear');
var fbody=form.getFirst();
var arr=fbody.getElementsByTagName('input');
var clearer=$('clearer');

var listProducts=$('listProducts');
if(!listProducts) return;

var msg=listProducts.defaultValue;
listProducts.value=msg;

listProducts.addEvent('focus',function(e){
	(this.value == msg || this.value == "" ||this.value==null || this.value==undefined) ? this.value="" : this.value=this.value;
	this.addEvent('blur',function(){
		(this.value == "" ||this.value==null || this.value==undefined) ? this.value=msg : this.value=this.value ;
	});
});
clearer.addEvent('click',function(e){
	new Event(e).stop();
	for(i=0;i<arr.length;i++){
	arr[i].checked=false;
	
	}
	listProducts.value=msg;
});
}


//tooltip d'auto-compl�tion recherche toolbar
function tooltipHelpSearch(){
	var helpBar=$('searchHelper');
	if(!helpBar) return;

	var txt=$('search');
	var txtCoord = txt.getCoordinates();
	txt.addEvents({
		'focus': function() {
			helpBar.setStyle('left',txtCoord.left);
			helpBar.setStyle('top',(txtCoord.bottom-1));
		},
		'keyup': function(e) {
			if (txt.value.length<3 && !helpBar.hasClass('hidden')){
				helpBar.addClass('hidden');
			}else if(txt.value.length>=3){
				helpBar.removeClass('hidden');
			}
		},
		'blur':function(){
			helpBar.addClass('hidden');
		}
	});

	window.onresize=function(){
		txtCoord = txt.getCoordinates();
		helpBar.setStyle('left',txtCoord.left);
		helpBar.setStyle('top',(txtCoord.bottom-1));
	}
}


function fixHeightBlocks(){
// ATTENTION CETTE FONCTION NE S'APPLIQUE PAS ENCORE AUX LAYERS

	var arr=[".modeVignette ul.line li.unit", "div.blockAlign"];
	arr.each(function(el,i){
		str=arr[i];
		if(!$$(str).length==0) minHeightBlocks(str);
	});



}

function minHeightBlocks(str){
	var arrBlocks=$$(str);
	var h=0;// d�clare Height pour faire comparatif entre blocks
	arrBlocks.each(function(el,i){
		var eachHeight=el.getCoordinates().height;
		if(eachHeight>h){
			h=eachHeight;
		}
	});
	arrBlocks.each(function(el,i){
	
		el.setStyle('height',h);
	});

}



// affichage des bolcs par case coch�e dans resultats moteur de recherche OOSHOP
function resultsMDR(){
	if($$('.triage').length==0)return false;
	var arrCheck=$$('.triage input[type^=check]');
	var arrBlocks=$$('.displayArticles');
	arrCheck.each(function(elm,i){
		elm.checked=true;
		elm.addEvent('click',function(){
			if(elm.checked){
				arrBlocks[i].removeClass('hidden');
			}else{
				arrBlocks[i].addClass('hidden');
				if (arrBlocks[i].hasClass('toggleClosed')){
					arrBlocks[i].getElement('.head a').onclick();
				}
			}
		})
	})
}

// changer l'identifiant des cartes dans des formulaires
function appliIdCarte(idCarte, oInput){
	var oInput=$(oInput);
	var oIdCarte=$(idCarte);
	var iIdCarte=$(idCarte).value;
	oInput.value=iIdCarte;
	oInput.disabled="disabled";
	oIdCarte.checked="checked";
}
/* switch entre les formulaires

	switchForm($$('.blocFormSwitchable01'),$$('.blocFormSwitchable02'))
	si on veut afficher 1 ou 2 �l�ments, il suffit de faire un tableau :
	switchForm($$('.blocFormSwitchable01, .blocFormSwitchable11'),$$('.blocFormSwitchable02, .blocFormSwitchable22'))

*/
function switchForm(aToActiv,aToHide){
// aFrom = �l�ments de formulaire activant les �l�ments : utile ?
// aToActiv = tableau d'�l�ments � montrer
// aToHide = tableau d'�l�ments � cacher
	if(aToActiv==null){}
	else{
		for(var i=0;aToActiv.length>i;i++){
			if(aToActiv[i].className.match(/hide/)){
				removeClass(aToActiv[i], "hide");

			}
		}
	}
	if(aToHide==null){}
	else{
		for(i=0;aToHide.length>i;i++){
			aToHide[i].className+=" hide";
		}
	}
}

function linksTabs() {
    var dls = document.getElementsByTagName('dl');
    var products = [];
    for (var i = 0, n = dls.length; i < n; i++) {
        var current = dls[i];
        if (current.className.match(/produit/)) {
            products.push(current);
        }
    }
    
    for (var i = 0, n = products.length; i < n; i++) {
        var current = products[i];
        var link = current.getElementsByTagName('a')[0].href;
        var img  = current.getElementsByTagName('span')[0];
        img.href = link;
        img.onclick = function(){
            window.location = this.href;
        };
    }
}

/************
* 3.2.1 GO !
************/
$e.add(window,'resize',function(){
	popLayer.refixSize();
});
//	trace.log((new Date()).getTime()-timeStart + 'ms avant domready');

window.addEvent('domready', function() {
//	resizeTD();
	if(document.getElementById('navG_mdr')){navManager = new myLeftNav("navG_mdr");}
//	trace.log((new Date()).getTime()-timeStart + 'ms apres navManager');
	addTextLoginInput();//ajout du texte sur les inputs de login/passw
//	trace.log((new Date()).getTime()-timeStart + 'ms apres addTextLoginInput');
//	positionLeftColMDR();
	tooltip.init();
//	trace.log((new Date()).getTime()-timeStart + 'ms apres tooltip init');
	new Layer();
//	trace.log((new Date()).getTime()-timeStart + 'ms apres new layer');
//	resultsMDR();
	tooltipHelpSearch();
//	trace.log((new Date()).getTime()-timeStart + 'ms apres tooltip');
	clearCheckboxes();
//	trace.log((new Date()).getTime()-timeStart + 'ms apres clearcheckbox');
	navUltraH.init();
//	trace.log((new Date()).getTime()-timeStart + 'ms apres navH');
	generateElements();
});
window.addEvent('load', function() {
//	trace.log((new Date()).getTime()-timeStart + 'ms start load');
//	$(document.body).addClass('initBlocks'); //cette classe est utilisee pour forcer l'afficher des contenus de certains blocks (dont blockTabs), et elle est supprim�e apr�s les traitements graphiques
//	trace.log((new Date()).getTime()-timeStart + 'ms add class initBlocks');
	navH_hover();
//	trace.log((new Date()).getTime()-timeStart + 'ms navH_hover');
	setProduct.init();
//	trace.log((new Date()).getTime()-timeStart + 'ms setProduct');
	loadFlash.load();
//	trace.log((new Date()).getTime()-timeStart + 'ms loadFlash');
//	filterMDR.init(); //a tester sur un environement global
//	trace.log((new Date()).getTime()-timeStart + 'ms filterMDR');
//	fixHeightBlocks(); // non applicable
//	$(document.body).removeClass('initBlocks');
//	trace.log((new Date()).getTime()-timeStart + 'ms del class initBlocks');
//	sizeBlocks(); // pour line/unit
	var timer1 = new loopTabs('defile', 3000);
	var timer2 = new loopTabs('defile2', 3000);
	var timer3 = new loopTabs2('defile3', 3000);
	checkPopLayer();
	retraitement();
	document.documentElement.className += " pageLoad";
//	trace.log((new Date()).getTime()-timeStart + 'ms checkPopLayer');
  linksTabs();
});
//trace.log((new Date()).getTime()-timeStart + 'ms fin lecture JS');

