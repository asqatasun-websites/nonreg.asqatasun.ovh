function popup(url, type, full, height, width){
	var nsNav = (document.layers) ? 1 : 0;
	var ieNav = (document.all) ? 1 : 0;
	var left = 0;
	var top = 0;

	// popup plein ecran
	if(full != 'no') {
		height = screen.availHeight - 30;
		width = screen.availWidth - 10;
	}
	//sinon on la centre
	else {
		var posX = (screen.width / 2) - (width / 2);
		var posY = (screen.height / 2) - (height / 2);
	}

	wintype="toolbar="+type+",menubar="+type+",location='no',scrollbars="+type+",top=0,left=0";
	wintype=wintype + ",height=" + height + ",width=" + width + ", left=" + posX + ",top=" + posY + "";
	var newwin = window.open(url,"popup"+type,wintype);
	newwin.focus();
}

function showMe(portlet) {
	if (document.getElementById(portlet)) {
		document.getElementById(portlet).style.display="";
	}
}

function hideMe(portlet) {
	if (document.getElementById(portlet)) {
		document.getElementById(portlet).style.display="none";
	}
}

function deleteArboLineQte(fieldQteIdId) {
	document.getElementById(fieldQteIdId).value = '';
}

function selectArboElement(fieldId,arboElementId,arboElementName) {
	document.getElementById(fieldId).value = arboElementName;
	document.getElementById(fieldId+'_id').value = arboElementId;
	hideMe('portletArbo');
	showMe('selectPortletArbo');
}

function setAndExecute(divId,innerHTML){
	var div=document.getElementById(divId);
	div.innerHTML=innerHTML;
	var x=div.getElementsByTagName("script");

	for(var i=0;i<x.length;i++){
		eval(x[i].text);
	}
}

//Fonction qui ouvre les champs arborescence
function showFormGeneratorArbo(formArboId,fieldId) {
	xhr_object = createXMLHttpRequest();
	url = reverse_path+"service/ajax/index.php?component=form_generator&action=show_arbo&form_arbo_id="+formArboId+"&field_id="+fieldId
	xhr_object.open("GET",url,true);
	xhr_object.onreadystatechange=onreadystatechange = function() {
		if ( xhr_object.readyState == 4 && xhr_object.status == 200 ) {
			setAndExecute('portletArbo',xhr_object.responseText);
			showMe('portletArbo');
			hideMe('selectPortletArbo');
		}
	}
	xhr_object.send(null);
}

function createXMLHttpRequest() {
   try { return new ActiveXObject("Msxml2.XMLHTTP"); } catch (e) {}
   try { return new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) {}
   try { return new XMLHttpRequest(); } catch(e) {}
   alert("XMLHttpRequest n'est pas supporté, mettez à jour votre navigateur, merci.");
   return null;
}


function deleteSharp(str) {
	var liLength = str.length;
	if ( str.substring((liLength-1),liLength) == '#' ) {
		str = str.substring(0,(liLength-1));
	}
	return str;
}

/****************************
DEBUT  Anciennement global.js
*****************************/
function MM_swapImgRestore()
{
	var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n,d)
{
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage()
{
	var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	if((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function popupscroll(url, height, width,full,name){

	var top=(screen.height-height)/2;
	var left=(screen.width-width)/2;
	var nsNav = (document.layers) ? 1 : 0;
	var ieNav = (document.all) ? 1 : 0;

	// popup plein ecran
	if(full)
	{
		height = screen.availHeight - 30;
		width = screen.availWidth - 10;
		top=0;
		left=0;
	}
    var winname = "";
	if (name) {
        winname = name;
    } else {
        winname = "popupscroll";
    }
	var wintype="toolbar=no,menubar=no,location='no',scrollbars=yes,top="+top+",left="+left;
	wintype=wintype + ",height=" + height + ",width=" + width;
	var newwin = window.open(url,winname,wintype);
	//newwin.focus();
}

function popupImg(url, height, width)
{
	Fenetre = window.open("",'_blank','width='+width+'px,height='+height+'px,top=10,left=10,scrollbars=no');
	Fenetre.document.write("<html><head><title>");
	Fenetre.document.write("</title></head><body leftmargin=4 topmargin=4><a href=\"\" onclick=window.close()><img src=");
	Fenetre.document.write(url);
	Fenetre.document.write(" alt=\"Cliquez pour fermer la fen�tre\" border=0></a></body>");
}

function search(obj,value)
{
	for (var i=0; i < obj.length; i++)
	{
		if(obj[i].value==value) {
			return i;
		}
	}
}

function postform()
{
	postwizard();
}

function webEditor(field)
{
	window.open('services/webeditor/word.php?name='+field,'WebEditor','personalbar=no,toolbar=no,status=no,scrollbars=no,location=no,resizable=no,menubar=no,titlebar=no,Top='+20+',Left='+20+',Width='+621+',Height='+638);
}

function webColor(value)
{
	return showModalDialog('services/color/color.htm',value,'status=no,toolbar=no,scroll:no;resizable:no;dialogWidth:248px;dialogHeight:280px');
}

function webCalendar(name,hidden,form,theme)
{
	if (form==null){form='wizard';}
	if (theme==null){theme='';}
	window.open('services/calendar'+theme+'/calendar.php?name='+name+'&hidden='+hidden+'&form='+form+'&theme='+theme,'calendar_'+name,'status=no,toolbar=no,scrollbars=no,resizable=no,width=172,height=153,left=20,top=20');
}

function validation()
{
	alert(document.form.webeditor.value);
}

function imageGallery(obj,img)
{
  document.wizard.obj.value=img;
}

function display()
{
	var name = document.wizard.template.value;
	if(name) document.image.src = "layers/images/"+name+".gif";
	else document.image.src = "layers/images/empty.gif";
}

//function replace
function replace(string,text,by) {
// Replaces text with by in string
    var strLength = string.length, txtLength = text.length;
    if ((strLength == 0) || (txtLength == 0)) return string;

    var i = string.indexOf(text);
    if ((!i) && (text != string.substring(0,txtLength))) return string;
    if (i == -1) return string;

    var newstr = string.substring(0,i) + by;

    if (i+txtLength < strLength)
        newstr += replace(string.substring(i+txtLength,strLength),text,by);

    return newstr;
}

//Paste the textareavalue in another area value field1 => field2
function paste(field1, field2, size){
	var var2 = eval("document.wizard."+field2+".value");
	if(var2 == "") {
		var1 = eval("document.wizard."+field1+".value");
		var1 = replace(var1,'"',"''");
		var1 = replace(var1,'\r',"<br>");
		var1 = replace(var1,'\n',"<br>");
		if(size!="") {
			if(var1.length > size){
				var3 = var1.substring(0,size-4)+"...";
			}else{
				var3 = var1;
			}
		}
		else {
			var3 = var1;
		}
		var vareval = "document.wizard."+field2+".value=\""+var3+"\";";
		eval(vareval);
	}

}

//format the date of the art_date_begin of the day if null
function formatdatebegin(){
	var today = new Date();

	datebegin = document.wizard.art_date_begin.value;
	if (datebegin == ""){
		var daY = today.getYear();
		var daM = today.getMonth()+1;
		var daD = today.getDate();
		if(daM < 10){
	      	daM ="0" + daM;
   		}
	   	if(daD < 10){
    	  	daD ="0" + daD;
	   	}
			document.wizard.art_date_begin.value = daY+"-"+daM+"-"+daD;
	}
}

//format the date of the art_date_begin of the day if null
function formatdateend(){
	var dateend;
	dateend = document.wizard.art_date_end.value;
	if (dateend == ""){
		document.wizard.art_date_end.value = "2030-12-31";
	}
}

function listBoxValid()
{
  var rub = document.wizard.dir_name.value;
  var tem = document.wizard.art_form.value;
  if (rub != "" && tem != "") {
  	document.wizard.submit();
  }else{
  	alert("Le gabarit ou la rubrique ne sont pas sélectionnés");
  }
}

//format the number if the value is a number
function formatNumeric(fieldname){
	var val = StrString = eval("document.wizard."+ fieldname +".value");
	var valreturn = "";
	if(numericValid(val)){
		if(val == 0 || val == "00" || val == "0 " || val == " 0"){
			valreturn = '';
			eval("document.wizard."+ fieldname +".value='';");
		}else{
			if(val < 10 && val.length==1){
				valreturn = "0"+val;
			}else{
				valreturn = val;
			}
			eval("document.wizard."+ fieldname +".value='"+valreturn+"';");
		}
	}
}

function getRadioIndex(obj)
{
	for (var i=0; i < obj.length; i++)
	{
		if(obj[i].checked==true)
		return i;
	}
	return -1;
}

//add your validation function here
function myValid(value)
{
    if(value=="")
    {
        return false;
    }else
    {
        return true;
    }
}
//END OF FORM VALIDATION

//SUBMIT forms
//list_rubrik
function postformrubrik(action)
{
	document.list_rubrik.action = action;
	document.list_rubrik.submit();
}

function checkallsearch(formulaire,chaine)
{
	var nom_champ = chaine.substring(0,chaine.length - 1);
	var init_field = formulaire.elements[nom_champ];

	for (var i=0; i<formulaire.elements.length; i++)
	{
    	var e = formulaire.elements[i];
	    if (e.name.search(chaine) != -1)
		{
			if( init_field.checked>0  )
			{
				e.checked = 1;
			}
			else {
				e.checked = 0;
			}
		}
	}
}

function checkRights(formulaire,init_field,tab_id)
{
	for (var i=0;i<tab_id.length;i++)
	{
		var e = formulaire.elements[tab_id[i]];
		if( formulaire.elements[init_field].checked>0  ) {
			e.checked = 1;
		}
		else {
			e.checked = 0;
		}
	}
}

function checkRightsForRight(formulaire,init_field,tab_id,parent)
{
	if (formulaire.elements[init_field].checked>0 && init_field!='ch_1')
	{
		var e = formulaire.elements['ch_1'];
		e.checked=0;

		if(parent && parent!='1')
		{
			var e = formulaire.elements['ch_'+parent];
			e.checked=0;
		}
	}

	for (i=0;i<tab_id.length;i++)
	{
		var e = formulaire.elements[tab_id[i]];

		if(formulaire.elements[init_field].checked>0) e.checked = 0;
	}

}

function hide(element,is_minimizable,id,couleur)
{
	if(is_minimizable==1)
	{
		var comp=document.getElementById(element);
		var label=document.getElementById('comp_'+id+'_minimized_label');
		if(comp.style.display == "")
		{
			label.innerHTML="<img src=\""+images_path+"portlet/ouvrir_"+couleur+".gif\" border=\"0\" width=\"11\" height=\"22\">";
			comp.style.display = "none";
		}else
		{
			label.innerHTML="<img src=\""+images_path+"portlet/fermer_"+couleur+".gif\" border=\"0\" width=\"11\" height=\"22\">";
			comp.style.display = "";
		}
	}
}

function setUp() {
obj1 = new SelObj('selection','commune_id','entry');
obj1.bldInitial();
}

function SelObj(formname,selname,textname,str) {
this.formname = formname;
this.selname = selname;
this.textname = textname;
this.select_str = str || '';
this.selectArr = new Array();
this.initialize = initialize;
this.bldInitial = bldInitial;
this.bldUpdate = bldUpdate;
}

function bldInitial() {
this.initialize();
for(var i=0;i<this.selectArr.length;i++)
document.forms[this.formname][this.selname].options[i] = this.selectArr[i];
document.forms[this.formname][this.selname].options.length = this.selectArr.length;
return;
}

function initialize() {
if (this.select_str =='') {
for(var i=0;i<document.forms[this.formname][this.selname].options.length;i++) {
this.selectArr[i] = document.forms[this.formname][this.selname].options[i];
this.select_str += document.forms[this.formname][this.selname].options[i].value+":"+
document.forms[this.formname][this.selname].options[i].text+",";
}
}
else {
var tempArr = this.select_str.split(',');
for(var i=0;i<tempArr.length;i++) {
var prop = tempArr[i].split(':');
this.selectArr[i] = new Option(prop[1],prop[0]);
}
}
return;
}

function bldUpdate() {
var str = document.forms[this.formname][this.textname].value.replace('^\\s*','');
if(str == '') {this.bldInitial();return;}
this.initialize(); //lwf
var j = 0;
pattern1 = new RegExp("^"+str,"i");
for(var i=0;i<this.selectArr.length;i++)
if(pattern1.test(this.selectArr[i].text))
document.forms[this.formname][this.selname].options[j++] = this.selectArr[i];
document.forms[this.formname][this.selname].options.length = j;
if(j==1){
document.forms[this.formname][this.selname].options[0].selected = true;
}
}

function MM_jumpMenu(targ,selObj,restore){
	lien = selObj.options[selObj.selectedIndex].value;
	if (lien.substr(0,3)!='_�_')
	{
		eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
	}
	if (restore) selObj.selectedIndex=0;
}
//*********************************
//	TEXTSIZER
//*********************************

//Specify affected tags. Add or remove from list:
tgs = new Array('div','table','p','td','font','span','a');

//Specify spectrum of different font sizes:
szs = new Array('10px','12px','14px','18px','23px');

var startSz = 0;

function ts( trgts,inc ) {
	if (!document.getElementById) return
	var d = document;
	var cEl = null;
	var sz = startSz;
	var i;
	var j;
	var cTags;

	sz += inc;
	if ( sz < 0 ) sz = 0;
	if ( sz > 4 ) sz = 4;
	startSz = sz;

	if(isArray(trgts)) {
		for ( i = 0 ; i < trgts.length ; i++ ) {

			var trgt = trgts[i];

			if ( !( cEl = d.getElementById( trgt ) ) ) cEl = d.getElementsByTagName( trgt )[ 0 ];

			cEl.style.fontSize = szs[ sz ];

			for ( j = 0 ; j < tgs.length ; j++ ) {
				cTags = cEl.getElementsByTagName( tgs[ j ] );
				for ( k = 0 ; k < cTags.length ; k++ ) cTags[ k ].style.fontSize = szs[ sz ];
			}

		}
	}
	else {
		if ( !( cEl = d.getElementById( trgts ) ) ) cEl = d.getElementsByTagName( trgts )[ 0 ];

		cEl.style.fontSize = szs[ sz ];

		for ( i = 0 ; i < tgs.length ; i++ ) {
			cTags = cEl.getElementsByTagName( tgs[ i ] );
			for ( j = 0 ; j < cTags.length ; j++ ) cTags[ j ].style.fontSize = szs[ sz ];
		}
	}
}

function isArray(object)
{
    return (object instanceof Array);
}

/****************************
FIN    Anciennement global.js
*****************************/