$(document).ready(function(){
       
    $("#menu_principal li").each(function(){
      $(this).mouseover(function(){
        
        $(this).children("ul").show();
        $(this).prev().children("ul").fadeOut("fast");
        $(this).siblings().children("ul").fadeOut("fast");
      });
      $(this).mouseleave(function(){

        
        $(this).children("ul").hide();
      });
    });
    $("body").click(function(){
      $("#menu_principal li ul").fadeOut("fast");
    });
  })
  
  
$(function() { 
    var ua = navigator.userAgent.toLowerCase(); 
    var myBrowsers = { 
        // Déjà détectés en natif par jQuery 
        mozilla: $.browser.mozilla, 
        safari:  $.browser.safari, 
        opera:   $.browser.opera, 
        ie:      $.browser.msie, 
        // Quelques améliorations et ajouts 
        ie6:     $.browser.msie && ($.browser.version < 7), 
        ie7:     $.browser.msie && ($.browser.version == 8), 
        ie8:     $.browser.msie && ($.browser.version > 7), 
        iphone:  /iphone/.test(ua), 
        chrome:  /chrome/.test(ua), 
        firefox: /firefox/.test(ua), 
        webkit:  /webkit/.test(ua), 
        // Détection de plateformes 
        osx:     /mac os x/.test(ua), 
        win:     /win/.test(ua), 
        linux:   /linux/.test(ua) 
    }; 
    $.each(myBrowsers, function(a, b) { 
        if (b) $('html').addClass(a); 
    }); 
}); 