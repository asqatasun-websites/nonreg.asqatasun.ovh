var isInternetExporer  = (navigator.appVersion.indexOf("MSIE") != -1) ? true : false;
var isWin = (navigator.appVersion.toLowerCase().indexOf("win") != -1) ? true : false;
var isOpera = (navigator.userAgent.indexOf("Opera") != -1) ? true : false;

function ControlVersion()
{
	var version;
	var axo;
	var e;


	try {
		axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");
		version = axo.GetVariable("$version");
	} catch (e) {
	}

	if (!version)
	{
		try {
			axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");
		
			version = "WIN 6,0,21,0";
			axo.AllowScriptAccess = "always";
			version = axo.GetVariable("$version");

		} catch (e) {
		}
	}

	if (!version)
	{
		try {
			axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.3");
			version = axo.GetVariable("$version");
		} catch (e) {
		}
	}

	if (!version)
	{
		try {
			axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.3");
			version = "WIN 3,0,18,0";
		} catch (e) {
		}
	}

	if (!version)
	{
		try {
			axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
			version = "WIN 2,0,0,11";
		} catch (e) {
			version = -1;
		}
	}
	
	return version;
}

function GetSwfVer(){
	var flashVer = -1;
	
	if (navigator.plugins != null && navigator.plugins.length > 0) {
		if (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]) {
			var swVer2 = navigator.plugins["Shockwave Flash 2.0"] ? " 2.0" : "";
			var flashDescription = navigator.plugins["Shockwave Flash" + swVer2].description;			
			var descArray = flashDescription.split(" ");
			var tempArrayMajor = descArray[2].split(".");
			var versionMajor = tempArrayMajor[0];
			var versionMinor = tempArrayMajor[1];
			if ( descArray[3] != "" ) {
				tempArrayMinor = descArray[3].split("r");
			} else {
				tempArrayMinor = descArray[4].split("r");
			}
			var versionRevision = tempArrayMinor[1] > 0 ? tempArrayMinor[1] : 0;			
			var flashVer = versionMajor + "." + versionMinor + "." + versionRevision;
		}
	}
	else if (navigator.userAgent.toLowerCase().indexOf("webtv/2.6") != -1) flashVer = 4;
	else if (navigator.userAgent.toLowerCase().indexOf("webtv/2.5") != -1) flashVer = 3;
	else if (navigator.userAgent.toLowerCase().indexOf("webtv") != -1) flashVer = 2;
	else if ( isInternetExporer && isWin && !isOpera ) {
		flashVer = ControlVersion();
	}	
	return flashVer;
}

function DetectFlashVer(reqMajorVer, reqMinorVer, reqRevision)
{
	versionStr = GetSwfVer();
	if (versionStr == -1 ) {
		return false;
	} else if (versionStr != 0) {
		if(isInternetExporer && isWin && !isOpera) {
			tempArray         = versionStr.split(" "); 	// ["WIN", "2,0,0,11"]
			tempString        = tempArray[1];			// "2,0,0,11"
			versionArray      = tempString.split(",");	// ['2', '0', '0', '11']
		} else {
			versionArray      = versionStr.split(".");
		}
		var versionMajor      = versionArray[0];
		var versionMinor      = versionArray[1];
		var versionRevision   = versionArray[2];

		if (versionMajor > parseFloat(reqMajorVer)) {
			return true;
		} else if (versionMajor == parseFloat(reqMajorVer)) {
			if (versionMinor > parseFloat(reqMinorVer))
				return true;
			else if (versionMinor == parseFloat(reqMinorVer)) {
				if (versionRevision >= parseFloat(reqRevision))
					return true;
			}
		}
		return false;
	}
}
function flashstr(par)
{
	var str="<object id='"+par.id+"' classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0' width='"+par.x+"' height='"+par.y+"' >";
	str+="<param name='movie' value='"+par.url+"'>";

	if (par.wmode!=null) str+="<param name='wmode' value='"+par.wmode+"'>"; 		
	if (par.base!=null) str+="<param name='base' value='"+par.base+"'>";
	if (par.allowscriptaccess!=null) str+="<param name='allowscriptaccess' value='"+par.allowscriptaccess+"'>";
	if (par.swliveconnect!=null) str+="<param name='swliveconnect' value='"+par.swliveconnect+"'>";
	if (par.flashvars!=null) str+="<param name='FlashVars' value='"+par.flashvars+"'>";
	
	str+="<param name='quality' value='high'>";
	
	
	str+="<embed src='"+par.url+"' ";
	str+="width='"+par.x+"' height='"+par.y+"' ";	
		
	if (par.wmode!=null) str+="wmode='"+par.wmode+"' ";
	if (par.base!=null) str+="base='"+par.base+"' ";		
	if (par.allowscriptaccess!=null) str+="allowscriptaccess='"+par.allowscriptaccess+"' ";		
	if (par.swliveconnect!=null) str+="swliveconnect='"+par.swliveconnect+"' ";		
	if (par.flashvars!=null) str+="FlashVars='"+par.flashvars+"' ";		
	
	
	str+="quality='high' ";	
	str+="pluginspage='http://www.macromedia.com/go/getflashplayer' type='application/x-shockwave-flash'>";
	str+="</embed>";
	str+="</object>";	
	return str;
}

function codeflashs(par)
{
	var str=par.rempcode;
	if (par.vers==null) par.vers=8;	
	if (DetectFlashVer(par.vers,0,0))
	{
		str=flashstr(par);
	}
	return str;
}
function rempimagestr(par)
{
	if (par.imurl!=null)
	{
		var str="<img src="+par.imurl;
		if (par.x!=null)	str+=" width="+par.x;
		if (par.y!=null)	str+=" height="+par.y;
		str+=">";
	} else
	{
		var str="<div" 
		if (par.x!=null)	str+=" width="+par.x;
		if (par.y!=null)	str+=" height="+par.y;		
		str+=" align='center' style='vertical-align:middle'><a href='http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash'>Vous devez installer le plug-in Flash-Player v8<br/>pour pouvoir visualiser ce contenu.</a></div>";
	}
	return str;
	
	
}

function codeflash(par)
{
	if (par.rempcode==null) par.rempcode=rempimagestr(par);		
	return codeflashs(par);
}
function writeflash(par)
{
	document.write(codeflash(par));	
}
