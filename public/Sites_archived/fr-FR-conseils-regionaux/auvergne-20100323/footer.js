<!--
function getWindowHeight() {
	var windowHeight = 0;
	if (typeof(window.innerHeight) == 'number') {
		windowHeight = window.innerHeight;
	}
	else {
		if (document.documentElement && document.documentElement.clientHeight) {
			windowHeight = document.documentElement.clientHeight;
		}
		else {
			if (document.body && document.body.clientHeight) {
				windowHeight = document.body.clientHeight;
			}
		}
	}
	return windowHeight;
}
function setFooter() {
	if (document.getElementById) {
		var windowHeight = getWindowHeight();
		if (windowHeight > 0) {
			var headerHeight = document.getElementById('nav').offsetHeight 
								+ document.getElementById('bandeau').offsetHeight 
								+ document.getElementById('header').offsetHeight;
			var contentElement = document.getElementById('contenu');
			var droiteHeight = document.getElementById('col_droite').offsetHeight;
			var gaucheHeight = document.getElementById('col_gauche').offsetHeight;
			var contentHeight = contentElement.offsetHeight;
			var footerElement = document.getElementById('footer');
			var maxHeight = Math.max(droiteHeight,gaucheHeight);
			maxHeight = Math.max(maxHeight,contentHeight);
			footerElement.style.top = maxHeight + headerHeight + 10 + 'px';
			contentElement.style.height = maxHeight + 'px';	
		}
		footerElement.style.visibility = 'visible';
	}
}
window.onresize = function() {
	setFooter();
}
//-->