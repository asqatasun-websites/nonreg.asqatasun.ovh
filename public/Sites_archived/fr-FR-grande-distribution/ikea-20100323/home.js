// 070117 Created by Jake
// Functions for showing/hiding flashcontent (if present) on Home page

// ### NOTE!!!!
// This flash detection should be placed in its own global script to be used for all flash detections on the site
// There is also a small vbScript that is needed for IE browsers (now included directly on the Home page)
// ###
var flashinstalled = 0;
var flashversion = 0;
var reqflashversion = 8;

MSDetect = "false";
if (navigator.plugins && navigator.plugins.length)
{
    x = navigator.plugins["Shockwave Flash"];
    if (x)
    {
        flashinstalled = 2;
        if (x.description)
        {
            y = x.description;
            flashversion = y.charAt(y.indexOf('.')-1);
        }
    }
    else
        flashinstalled = 1;
    if (navigator.plugins["Shockwave Flash 2.0"])
    {
        flashinstalled = 2;
        flashversion = 2;
    }
}
else if (navigator.mimeTypes && navigator.mimeTypes.length)
{
    x = navigator.mimeTypes['application/x-shockwave-flash'];
    if (x && x.enabledPlugin)
        flashinstalled = 2;
    else
        flashinstalled = 1;
}
else
    MSDetect = "true";

// ### End of flash detect    

function hideContent(strName){
    document.getElementById(strName).style.display = "none";
}

function showContent(strName){
    document.getElementById(strName).style.display = "block";
}

function showMainPromo(leftFlash, leftHtml, rightFlash, rightHtml){
    var leftObj = document.getElementById(leftFlash);
    var rightObj = document.getElementById(rightFlash);
    // check if left or right flash tag exists
    if(leftObj || rightObj){
        //alert('One (or more) flash tag exists!\nHide html tag');
        
        // since flash tag exists, also check if user has flash player installed
        if (flashinstalled == 2)
        {
            if (flashversion >= reqflashversion){
                //document.write("You have Flash version " + flashversion + " installed. You should be redirected to the flash page in a moment...");
                //location.href = 'flash.html';  // goto page with flash
                if(leftObj){
                    hideContent(leftHtml);
                    showContent(leftFlash);
                }
                if(rightObj){
                    hideContent(rightHtml);
                    showContent(rightFlash);
                }
            }else{
                //document.write("You have Flash installed, but I can't detect the version.");
                //location.href = 'flash_no.html';  // goto page with no flash
                if(leftObj){
                    hideContent(leftFlash);
                }
                if(rightObj){
                    hideContent(rightFlash);
                }
            }
        }
        else
        {
            //document.write("You don't have Flash installed.");
            //location.href = 'flash_no.html';  // goto page with no flash
            if(leftObj){
                hideContent(leftFlash);
            }
            if(rightObj){
                hideContent(rightFlash);
            }
        }
    }
}