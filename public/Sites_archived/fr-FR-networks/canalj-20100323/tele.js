function updateGrillePerso(){
	var balise = $$('#scrollbox_programmes li');
	for(var i=0; i<balise.length; i++){
		balise[i].addClassName('alpha');
	}
	new Ajax.Request(sHost + '/import_grille_xml/array_prog_perso',
		{
			onComplete:	function(t){
				eval(t.responseText);
				var li,classPath;
				
				for(var j=0; j < arrayProgPerso.length;j++){
					classPath = '#scrollbox_programmes li.id_prog_' + arrayProgPerso[j];
					li = $$(classPath);
					for(var k=0; k < li.length; k++){
						
						li[k].addClassName('nonalpha');
						li[k].removeClassName('alpha');
					}
				}
			},
			asynchronous:true,
			evalScripts:true
		}
	);
}

function updateProgPerso(){

	new Ajax.Request(sHost + '/import_grille_xml/array_prog_perso',
			{
				onComplete:	function(t){
					eval(t.responseText);
					var li,li2,classPath,classPath2;

					for(var j=0; j < arrayProgPerso.length;j++){
						classPath = '.liste_programmes_selection li#perso_' + arrayProgPerso[j];

						li = $$(classPath);
							for(var k=0; k < li.length; k++){											
							li[k].show();
						}
						classPath2 = '.liste_programmes li#' + arrayProgPerso[j];
						li2 = $$(classPath2);
						for(var l=0; l < li2.length; l++){											
							li2[l].hide();
						}
					}

				},
				asynchronous:true,
				evalScripts:true
			}
	); 
}

function transfert(balise,source)
{
	var limite = 30;
	// on compte le nombre d'elements selectionnés
	var classPath = '.liste_programmes_selection li';
	var li = $$(classPath);
	var cpt =0;
	
	for(var i=0; i < li.length; i++)
	{											
		// si l'element est affiché, on le compte
		if (li[i].style.display != 'none')
		{
			cpt++;
		}
	}

	var nbElementsPerso = cpt;
	cpt = 0

	//liste -> perso
	if (source == 'liste')
	{
		if (nbElementsPerso != limite)
		{
			var baliselisrc = document.getElementById(balise);
			Element.hide(baliselisrc);		
	
			var baliselidest = document.getElementById('perso_'+balise);
			Element.show(baliselidest);
		}
		
		else alert('Tu ne peux enregistrer que ' + limite + ' programmes !');
	}
	else
	{
		var baliselisrc = document.getElementById('perso_'+balise);
		Element.hide(baliselisrc);
		
		var baliselidest = document.getElementById(balise);
		Element.show(baliselidest);
	}
}

function envoyer()
{
	//Modalbox.show(null,{height:0,width:0});
	var li,classPath;
	var aProgrammes = new Array();
	var cpt = 0;

 //on recupere tous les elements affichés dans liste_programmes_selection

	classPath = '.liste_programmes_selection li';
	li = $$(classPath);
	
	for(var i=0; i < li.length; i++)
	{
		// si l'element est affiché, on le stocke
		if (li[i].style.display != 'none')
		{
			var str = li[i].id
			aProgrammes[cpt]=str.substring(6);
			cpt++;
		}
	}
	
	new Ajax.Request(sHost + '/import_grille_xml/insert_prog_perso',
		{
			onComplete:	function(t){
				alert(t.responseText);
				//Modalbox.hide();
			},
		 	method: 'post',
		 	parameters:'aProgrammes=' + escape(aProgrammes),
		 	asynchronous:true,
			evalScripts:true							
		});	

}


function imgProgPerso()
{
	if (document.cookie) {
		if (GetCookie("user_logged") == "oui")
		{
			$('img_perso').className = 'modifie_prog_perso';
		}
		else
		{
			$('img_perso').className = 'fais_prog_perso';
		}
	}
}


function display_en_ce_moment()
{
	var timestamp = Math.floor( new Date().getTime()/1000 );
	timestamp = timestamp + decalage_horaire;
	var balise = $$('div.cache');
	var mode = 'recherche_current';
	
	for(var i=0; i<balise.length; i++){
		if (
		mode == 'recherche_next' 
		&& balise[i].hasClassName('next')
		)
		{
			balise[i].show();
			mode = 'efface_le_reste';
		}
		else if (
		mode == 'recherche_current' 
		&& balise[i].hasClassName('current')
		&& balise[i].readAttribute('ts') <= timestamp
		&& balise[i].readAttribute('tsend') >= timestamp
		)
		
		{
			balise[i].show();
			mode = 'recherche_next';
		}
		else
		{
			balise[i].hide();
		}
	}
	window.setTimeout('display_en_ce_moment();',6000);
}

/* dans modal episode */
function switchTab(div1,div2,classe)
{
		$(div1).addClassName(classe);
		$(div2).removeClassName(classe);
}

function showHide(div1,div2)
{
		$(div1).show();
		$(div2).hide();
}