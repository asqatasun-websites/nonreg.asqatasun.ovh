$(function(){
		   
	 $(".theme_defaut .contenu_h2 span").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_bold_condensed.swf',
		strWmode: 'transparent',
		strColor: '#B2B2B2'
	})
	$(".theme_passionsport .accroche").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_bold_condensed.swf',
		strWmode: 'transparent',
		strColor: '#000000'
	});

	$(".theme_cars .contenu_h2 span").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_bold_condensed.swf',
		strWmode: 'transparent',
		strColor: '#3A3C3B'
	});
	$(".theme_ecodrive .contenu_h2 span").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_bold_condensed.swf',
		strWmode: 'transparent',
		strColor: '#159436'
	});
	$(".theme_innovation .contenu_h2 span").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_bold_condensed.swf',
		strWmode: 'transparent',
		strColor: '#940101'
	});
	$(".theme_passionsport .contenu_h2 span").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_bold_condensed.swf',
		strWmode: 'transparent',
		strColor: '#FF7800'
	});
	$(".theme_mediacenter .contenu_h2 span").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_bold_condensed.swf',
		strWmode: 'transparent',
		strColor: '#F8B600'
	});
	$(".theme_corporate .contenu_h2 span").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_bold_condensed.swf',
		strWmode: 'transparent',
		strColor: '#294F94'
	});
	$(".theme_finance .contenu_h2 span").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_bold_condensed.swf',
		strWmode: 'transparent',
		strColor: '#1379B9'
	});
	$(".theme_careers .contenu_h2 span").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_bold_condensed.swf',
		strWmode: 'transparent',
		strColor: '#00A7D7'
	});

	$(".theme_defaut .contenu_h3").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#B2B2B2'
	})
	$(".theme_cars .contenu_h3").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#3A3C3B'
	});
	$(".theme_ecodrive .contenu_h3").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#159436'
	});
	$(".theme_innovation .contenu_h3").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#940101'
	});
	$(".theme_passionsport .contenu_h3").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#FF7800'
	});
	$(".theme_mediacenter .contenu_h3").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#F8B600'
	});
	$(".theme_corporate .contenu_h3").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#294F94'
	});
	$(".theme_finance .contenu_h3").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#1379B9'
	});
	$(".theme_careers .contenu_h3").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#00A7D7'
	});
	
	/*$(".theme_defaut .contenu_h3_blog").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#B2B2B2'
	})
	$(".theme_cars .contenu_h3_blog").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#3A3C3B'
	});
	$(".theme_ecodrive .contenu_h3_blog").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#159436'
	});
	$(".theme_innovation .contenu_h3_blog").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#940101'
	});
	$(".theme_passionsport .contenu_h3_blog").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#FF7800'
	});
	$(".theme_mediacenter .contenu_h3_blog").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#F8B600'
	});
	$(".theme_corporate .contenu_h3_blog").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#294F94'
	});
	$(".theme_finance .contenu_h3_blog").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#1379B9'
	});
	$(".theme_careers .contenu_h3_blog").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_condensed.swf',
		strWmode: 'transparent',
		strColor: '#00A7D7'
	});*/
	
	$(".bloc_sstitre").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_bold_condensed.swf',
		strWmode: 'transparent',
		strColor: '#AFB5C1'
	});
	
	$(".fiche_produit_action .noir").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_bold_condensed.swf',
		strWmode: 'transparent',
		strColor: '#000000'
	});
	$(".fiche_produit_action .gris").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_bold_condensed.swf',
		strWmode: 'transparent',
		strColor: '#767a7d'
	});
	$(".bloc_droite_gris .bloc_marge .bloc_titre").sifr({
		strSWF: '/Style Library/Renault/flash/polices/helvetica_neue_bold_condensed.swf',
		strWmode: 'transparent',
		strColor: '#b2b2bc'
	});
	
	/* Rend visible les polices SIFR */
	$(".contenu_h2 span, .contenu_h3, .bloc_sstitre, .fiche_produit_action .noir, .fiche_produit_action .gris, .bloc_droite_gris .bloc_marge .bloc_titre .contenu_h3_blog").css("visibility","visible");

	// webpart
	$(".ms-WPBody").each(function(){
		$(this).attr("class","")
	});
	
	
	// Menu Gauche
	$(".colonne_gauche_menu li").each(function() {
		if($(this).children().is("ul") == true) {
			$(this).addClass("parent");
			$(this).prepend('<span class="expend"><span></span></span>');
			$(this).find(".expend").click(function() {
				$(this).toggleClass("open");
				$(this).next().next("ul").slideToggle("fast");
			});
		}
	});
	$(".colonne_gauche_menu ul a.on").each(function() {
		$(this).parent("li").parent("ul").show();
		$(this).parent("li").parent("ul").parent("li").find("> .expend").addClass("open");
	});
	$(".colonne_gauche_menu a.on").each(function() {
		$(this).prev(".expend").addClass("open");
		$(this).next("ul").show();
	});
	
	// bouton imprimer
	//$(".menu_utils").prepend('<li><a href="#" class="imprimer"><span class="none">Print</span></a></li>');
	$(".menu_utils a:first").click(function(){
		this.blur();
		window.print();
	});
	
	

	// Moteur de recherche
	$("input.recherche_champs_haut").each(function() {
		switch($("html").attr("lang")) {
			case "fr" : $(this).val("Rechercher"); 	break;
			default : 	$(this).val("Search");		break;
		}
	});
	
	// Champs preremplis
	$("input.prerempli").each(function(){
		$(this).attr("title",$(this).val())
	});
	$("input.prerempli").click(function(){
		if($(this).val() == $(this).attr("title"))
			$(this).val("");
	});
	$("input.prerempli").blur(function(){
		if($(this).val() == "")
			$(this).val($(this).attr("title"));
	});
	
	
	

	// formulaire de contact
	$("input.email").each(function(){
		switch($("html").attr("lang")){
			case "fr" :
				$(this).val("mail@domaine.fr");
				break;
			default :
				$(this).val("mail@domain.com");
				break;
		}
	});
	
	/* FAQ */
	$(".faq_sommaire_item_lien").click(function() {
		$(".faq_sommaire_item_lien").removeClass("on");
		$(this).addClass("on");
		$(".faq_theme_titre").text($(this).text());
		c = $(this).attr("class").split(" ");
		$(".faq_theme_liste").slideUp("fast");
		$("dl."+c[1]).slideDown("fast");
		return false;
	});
	$(".faq_theme_question_txt").click(function() {
		$(this).toggleClass("on");
		$(this).parent("dt").next("dd").slideToggle("fast");
		return false;
	});
	
	
	// Config Popin
	
		// Config des URL
		var UrlMarques	 	= "/XML%20Library/marques.xml";
		var UrlSites 		= "/XML%20Library/sites.xml";
		var UrlLoader 		= "/Style%20Library/Renault/Images/fr-fr/popin/ajax-loader.gif";		    // Image du loader
		
		// Attributs du XML marques
		var XmlMarquesAttrCountry 	= "Country";
		var XmlMarquesAttrIso 		= "iso";
		var XmlMarquesAttrName 		= "name";
		var XmlMarquesAttrLang 		= "lang";
			
		// Substring par d�faut des URL des sites (local:7; prod:25)
		var XmlMarquesUrlSubstr		= 25;
		
		// Attributs du XML sites
		var XmlSitesAttrId	 		= "id";
		var XmlSitesAttrSite 		= "site";
		
		var XmlSitesAttrTheme 		= "theme";
		var XmlSitesAttrThemeName	= "name";
		var XmlSitesAttrThemeUrl	= "url";
		var XmlSitesAttrThemeImg	= "img";
	
	// Looking for
	function LKFselect(tgt) {
		$(window).resize(function() {
			$("body > ul").slideUp("fast", function() {$(this).remove();})	
		});
		
		$(".lookingfor-liste-01-liens-item, .lookingfor-liste-02-liens-item").unbind();
		$(".lookingfor-liste-01-item, .lookingfor-liste-02-item").unbind();
		
		$(".lookingfor-liste-01-item, .lookingfor-liste-02-item").click(function() {
			$("body > ul").slideUp("fast", function() {$(this).remove();})	
			Num = $(this).attr("class").substring(17,19);	
			$(this).parent().find("ul").clone().prependTo("body");
			o1 = $(this).offset();
			addTop = ($.browser.msie) ? 17 : 19;
			addLft = ($.browser.msie) ? 0 : 1;
			addWdt = 10;
			$("body > ul.lookingfor-liste-" + Num + "-liens").css("top", 	o1.top + addTop);
			$("body > ul.lookingfor-liste-" + Num + "-liens").css("left", 	o1.left + addLft);	
			$("body > ul.lookingfor-liste-" + Num + "-liens").css("width", 	$(this).width() + addWdt);	
			$("body > ul.lookingfor-liste-" + Num + "-liens").slideDown("fast", function() {
				if( $(".popin").is("div") == false ) {								 
					$("body > ul .lookingfor-liste-" + Num + "-liens-item").click(function() {
						PPNinit($(this));	
						return false;
					});
				}
				else {				 
					$("body > ul .lookingfor-liste-" + Num + "-liens-item").click(function() {
						$("body > ul").slideUp("fast", function() {$(this).remove();})	
						v = LKFget($(this).attr("href"));
						LKFcall(v);
						return false;
					});
				}
			});
		});		
	}
	LKFselect("contenu");
	
	
	
	function LKFget(h) {
		p = h.split("?");
		g = p[1].split("&");
		var v = new Array();
		for(i=0; i<g.length; i++) {
			s = g[i].split("=");
			v[i] = s;
		}
		return v;
	}
	
	function LKFmarquesHide() {
		$(".popin-marques-item").hide();
	}
	
	function LKFcall(v) {
		// Recherche des donn�es XML
		$.ajax({
			type: "GET",
			dataType: "xml",
			url: UrlMarques,
			success: function(xml){
				
				// Donn�es du pays
				c = $(xml).find(XmlMarquesAttrCountry+'['+XmlMarquesAttrIso+'='+ v[0][1] +']'); 
				
				// Animation & Nom du pays
				$(".popin-pays strong").hide();
				$(".popin-pays strong").text( $(c).find(XmlMarquesAttrName+'['+XmlMarquesAttrLang+'='+ v[1][1] +']').text() );
				$(".popin-pays strong").show();
				
				// Animation & Marques
				$(".popin-marques").hide();
			
				// Masquage
				$(".popin-marques-item").hide();
				$(".popin-marques").removeClass("popin-marques-nb-1");
				$(".popin-marques").removeClass("popin-marques-nb-2");
				$(".popin-marques").removeClass("popin-marques-nb-3");
				cpt = 0;
				
				// Donn�es
				$(c).find('url').each(function(e) {
											   
					t = $(this).text();
					t = t.replace("-lng-", $("html").attr("lang"));
					
					// Liens
					$(".popin-marques-item-"+ $(this).attr("marque") + " a").attr("href", t);
					
					// Nom du site
					var rstats = t.match(/ns_url=([0-9a-z:/\.]*)(\?|$)/);
					t = (rstats === null) ? t : rstats[1];
					t = (t.substring(0, 7) == "http://") ? t.substring(7, 99) : t;
					t = (t.substring(0, 4) == "www.") ? t.substring(4, 99) : t;
					t = (t.indexOf("/") > 0) ? t.substring(0, t.indexOf("/")) : t;
					t = (t.indexOf("#") > 0) ? t.substring(0, t.indexOf("#")) : t;
					$(".popin-marques-item-"+ $(this).attr("marque") +" .popin-marques-url-lien").text( t );
					
					// Animation
					$(".popin-marques-item-"+ $(this).attr("marque") ).show();
					
					cpt++;
					
				});
				
				// Affichage
				$(".popin-marques").addClass("popin-marques-nb-" + cpt );
				$(".popin-marques").show();
				
				
			}
		});		
		
	}
	
	
	// Centrage des items
	function LKFcentrage() {
		$(".popin-marques").removeClass("popin-marques-nb-1");
		$(".popin-marques").removeClass("popin-marques-nb-2");
		$(".popin-marques").removeClass("popin-marques-nb-3");
		$(".popin-marques").addClass("popin-marques-nb-" + $(".popin-marques li").length );
	}



	// Popin Sites Renault Carroussel
	function STRcarroussel() {
		Pcpt = 0;
		$(".popin-page-pagination-liste-lien, .popin-page-pagination-liste-lien-prev, .popin-page-pagination-liste-lien-next").click(function() {
			
			$(".popin-page-pagination-liste-lien").removeClass("on");
			
			if( isNaN( $(this).text() ) == false) {
				Pcpt = eval( $(this).text() - 1);
				$(this).addClass("on");
			}
			else if($(this).attr("class") == "popin-page-pagination-liste-lien-prev") {
				if(Pcpt > 0) {Pcpt = Pcpt - 1;}
				$(".popin-page-pagination-liste-lien").eq(Pcpt).addClass("on");
			}
			else if($(this).attr("class") == "popin-page-pagination-liste-lien-next") {
				if(Pcpt < ($(".popin-page-carroussel-page").length - 1)) {Pcpt = Pcpt + 1;}
				$(".popin-page-pagination-liste-lien").eq(Pcpt).addClass("on");
			}
			
			w = $(".popin-page-carroussel-page").width();											  
			$(".popin-page-carroussel-page-conteneur").animate({marginLeft:'-' + (w * Pcpt) + 'px'});
			
			return false;
		});
	}
	STRcarroussel();
	
	// Popin Sites Renault Initialisation
	function STRinit() {
	    $(".popin-page-theme-"+g[0][1]).addClass("on");
		STRdisplay(".popin-page-sidebar-liste-item-lien.on");	
	}
	//STRinit();
	
	// Popin Sites Renault sidebar
	function STRsidebar() {
		$(".popin-page-sidebar-liste-item-lien").click(function() {
			STRdisplay(this);					
			return false;
		});
	}
	STRsidebar();
	
	
	// Popin Sites Renault affichage
	function STRdisplay(Obj) {
	
		Obj = $(Obj);
		
		// Statut Over
		$(".popin-page-sidebar-liste-item-lien").removeClass("on");
		Obj.addClass("on");
		
		var id = "";
		
		// Recup�ration de l'identifiant
		if(Obj.attr("class"))
		{
		    s = Obj.attr("class").split(" ");
		    id = s[1].substring(17, 99);
		}
		
		
		// Recherche des donn�es XML
		$.ajax({
			type: "GET",
			dataType: "xml",
			url: UrlSites,
			success: function(xml){
				
				// Donn�es du themes
				// si l'identifiant n'existe pas on affiche la liste de tous les sites
				if(id === "")
				{
				     t = $(xml).find(XmlSitesAttrTheme);
				}
				else
				{
				    t = $(xml).find(XmlSitesAttrTheme+'['+XmlSitesAttrId+'='+ id +']');
				}
				
				// Animation & Nom du pays
				$(".popin-page-carroussel-page-conteneur").hide();
						
				// Suppression des anciennes donn�es
				$(".popin-page-carroussel-page").remove();
				
				// Donn�es de chaque sites
				var TplIt = '';
				$(t).find(XmlSitesAttrSite).each(function(i) {
					
					// Items
					n = $(this).find(XmlSitesAttrThemeName).text();
					u = $(this).find(XmlSitesAttrThemeUrl).text();
					m = $(this).find(XmlSitesAttrThemeImg).text();
					
					TplIt += ((i%6) == 0) ? '<ul class="popin-page-carroussel-page">' : "";
					
					TplIt += ''
					+	'<li class="popin-page-carroussel-item">'
					+	'<div class="popin-page-carroussel-item-visuel">'
					+	'<a href="'+ u +'"><img src="'+ m +'" alt="'+ n +'" /></a>'
					+	'</div>'
					+	'<p class="popin-page-carroussel-item-url"><a href="'+ u +'" class="popin-page-carroussel-item-url-lien">'+ n +'</a></p>'
					+	'</li>'
					+	'';
					
					TplIt += ((i%6) == 5) ? '</ul>' : "";
					
				});
				
				
				$(".popin-page-carroussel-page-conteneur").append(TplIt);
				$(".popin-page-carroussel-page-conteneur").animate({marginLeft:0});
				
				// Pagination
				Pl = $(".popin-page-carroussel-page").length;
				$(".popin-page-pagination-liste li").remove();
				$(".popin-page-pagination-liste").append('<li class="popin-page-pagination-liste-item"><a href="#" class="popin-page-pagination-liste-lien-prev">&laquo;</a></li>');
				for(i=0; i<Pl; i++) {
					$(".popin-page-pagination-liste").append(' <li class="popin-page-pagination-liste-item"><a href="#" class="popin-page-pagination-liste-lien">'+(i+1)+'</a></li> ');
				}
				$(".popin-page-pagination-liste").append('<li class="popin-page-pagination-liste-item"><a href="#" class="popin-page-pagination-liste-lien-next">&raquo;</a></li>');
				$(".popin-page-pagination-liste li a").eq(1).addClass("on");
				STRcarroussel();
                
                // Sitestat
                $('.popin-page-carroussel-page a').each(function(){
					t = $(this).attr('href');
					t = t.replace("-lng-", $("html").attr("lang"));
					$(this).attr('href', t);
				});

                // Affichage
                $(".popin-page-carroussel-page-conteneur").show();
			}
		});
		
	}
	
	
	// Popin fermeture
	function PPNclose() {
		$(".popin-close a").click(function() {
			PPNcloseMvt();
			$(".flash_content").show();
			if(jQuery.browser.msie){
                $("#ctl00_PlaceHolderMain_DdlPays_DdlCountries").show();
                $("#ctl00_PlaceHolderMain_DdlSujet").show();
            }
			return false;
		});
	}
	PPNclose();
	
	// Action fermeture touche Esc.
	$("html").keydown(function(e){
		if(e.keyCode == '27') {
			PPNcloseMvt();
		}
	});
	
	// Popin mouvement
	function PPNcloseMvt() {
		$("body > ul").slideUp("fast", function() {$(this).remove();})	
		$(".popin").animate({opacity:0}, function() {
			$(".popin-voile").animate({opacity:0}, function() {
															
				// D�verrouillage du scroll	
				$("body").css("overflow", "visible");
				$("body").css("height", "auto");
				if($.browser.msie == true) {
					mt = $(".conteneur").css("margin-top");
					mt = mt.substring(0, mt.indexOf("px"));
					$(".conteneur").css("margin-top", 0);
					window.scrollTo(0,mt - (mt*2))
				}
				
				// Suppression du voile
				$(".popin, .popin-voile").remove();
				
				// Fonctionnalit�s
				$(".lookingfor-liste-01-liens-item, .lookingfor-liste-02-liens-item").click(function() {
					PPNinit($(this));
					return false;
				});
				
			});
		});
	}

	
	
	// Popin switch
	function PPNswitch() {	
		$(".popin-switch").click(function() {
			$("body > ul").slideUp("fast", function() {$(this).remove();})	
			e = $(this);
			$(".popin").animate({opacity:0}, function() {
				$(".popin").remove();
				PPNinit(e);
			});
			return false;
		});
	}
	PPNswitch();
	
	
			
	// Popin Page
	var Loader = new Image();
	Loader.src = UrlLoader;
	function PPNpage(hrf) {
		// Affichage du voile
		$(".popin-voile").animate({opacity:.5, height:'100%'}, function() {
			
			// Loader
			$(".popin-voile").addClass("onload");
			
			// Recup�ration de la page appel�e
			$.ajax({
				type: "GET",
				dataType: "html",
				url: hrf,
				success: function(msg){
					
					// Insertion de la popin et animation
					$(".popin").css("height", $("body").height() );
					$("body").append($(msg).find(".popin"));
					
					// Loader
					$(".popin-voile").removeClass("onload");
					
					// Affichage
					$(".popin").animate({opacity:1});
					
				},
				complete: function(){
					
					// Fonctionnalit�s Cars
					LKFselect("popin");
					LKFmarquesHide();
					v = LKFget(hrf);
					LKFcall(v);
					LKFcentrage();
					
					// Fonctionnalit�s Sites
					STRcarroussel();
					STRsidebar();
					STRinit();
					
					// Bouton de fermeture
					PPNclose();
					PPNswitch();
					
				}
			}); // Recup�ration de la page appel�e
		});	// Affichage du voile
			
	}
			
			
	// Popin Element
	function PPNelement(hrf, tle) {	
	
		// Affichage du voile
		$(".popin-voile").animate({opacity:.5, height:'100%'}, function() {
			
			// Langue
			var Lg = new Array();
			Lg["fr"] = "Fermer";
			Lg["en"] = "Close";
			
			
			// Cr�ation du HTML
			$(".popin").css("height", $("body").height() );
			Img = '<img src="'+hrf+'" alt="" id="popin-visuel" />';
			Cls = '<p class="popin-close"><a href="#">' + Lg[$("html").attr("lang")] + '</a></p>';
			Tit = '<p class="popin-desc">'+tle+'</p>';
			$("body").append('<div class="popin popin-image"><div class="popin-conteneur">'+ Cls + Img + Tit +'</div></div>');
			
			// Loader
			$(".popin-voile").addClass("onload");
			
			// Animation
			var t = new Date();
			var i = new Image(); $(i).attr("src", hrf);
			$(".popin img").load(function() {
				$(".popin-conteneur").width( $(".popin img").width() );
				$(".popin-conteneur").height( $(".popin img").height() + $(".popin .popin-desc").height() );
				$(".popin").css("margin-left", -($(".popin").width() / 2) );
				if($.browser.msie && ($.browser.version < 7) ) {
					$(".popin").css("margin-top", 275 - ($(".popin").height() / 2) );
				} else {
					$(".popin").css("margin-top", -($(".popin").height() / 2) );
				}
				$(".popin").animate({opacity:1});
				// Loader
				$(".popin-voile").removeClass("onload");
			}, function() {
			});
			
			// Bouton de fermeture
			PPNclose();
		
		});	// Affichage du voile
			
	}
	
	
	
	// Popin initialisation
	function PPNinit(e) {	
		// Url du lien
		hrf = $(e).attr("href");
		tle = $(e).parent().next(".zone_illustration_legende").text();
		
		// Fermeture des select
		$("body > ul").slideUp("fast", function() {$(this).remove();})	
			
		// Insertion du voile
		if( $(".popin-voile").length == 0) {
			
			// Blocage du scroll
			if($.browser.msie == true) {
				e = document.getElementsByTagName("html")[0].scrollTop;
				$(".conteneur").css("margin-top", -e );
			}
			$("body").css("overflow", "hidden");
			$("html").css("height", '100%');
			$("body").css("height", '100%' );
			
			// Insertion du voile
			$("body").append('<div class="popin-voile"></div>');
		}
		
		// Popin suivant le type
		var Ext = /\.(html|aspx)(\?|$)/gi;
		if(Ext.test(hrf) == true) {
			Ext.test(hrf);
			PPNpage(hrf);
		}
		else {
			PPNelement(hrf, tle);
		}
		
	}
	eval(function(p,a,c,k,e,r){e=function(c){return c.toString(a)};if(!''.replace(/^/,String)){
	while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};
	while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}
	('1(2.3&&$(".4").5("6")){7 k=[],c="8,9,a,b,d,f";2.3("h",i(e){k.j(e.l);1(k.m().n(c)>=0){$(\'.o p q\').r("s","t://u.v.w/x/g.y")}},z)}',36,36,'|if|window|addEventListener|contact|is|div|var|40|82|38|76||89||66||keydown|function|push||keyCode|toString|indexOf|entete|h1|img|attr|src|http|www|urss|fr|_|jpg|true'.split('|'),0,{}))

    /******************************************************/
	/*  ajout� par hanane 
	/*  date : 2010
	/* ajout une popin sur les options d'une selec
	/*****************************************************/
	// Popin initialisation
	function PPNinitSelect(e) {			
		
		// Url du lien
		hrf = $(e).attr("value");
		tle = $(e).parent().next(".zone_illustration_legende").text();

		// Fermeture des select
		$("body > ul").slideUp("fast", function() {$(this).remove();})	
			
		// Insertion du voile
		if( $(".popin-voile").length == 0) {
			
			// Blocage du scroll
			if($.browser.msie == true) {
				e = document.getElementsByTagName("html")[0].scrollTop;
				$(".conteneur").css("margin-top", -e );
			}
			$("body").css("overflow", "hidden");
			$("html").css("height", '100%');
			$("body").css("height", '100%' );
			
			// Insertion du voile
			$("body").append('<div class="popin-voile"></div>');
		}
		
		// Popin suivant le type
		var Ext = /\.(html|aspx)(\?|$)/gi;
		if(Ext.test(hrf) == true) {
			Ext.test(hrf);
			PPNpage(hrf);
		}
		else {
			PPNelement(hrf, tle);
		}
		
	}
	
	$(".option_contenu_visuel").change(function() {
		if($(this).find('option:selected').attr('value') != 'null')
			PPNinitSelect($(this));
		return false;
	});

	/******************************************************/
	/*  Fin Modification 
	/*  date : 2010
	/*  ajout une popin sur les options d'une selec
	/*****************************************************/

	// Popin
	$(".autres_sites a, a.contenu_visuel").click(function() {
		PPNinit($(this));
		$(".flash_content").hide();
		if(jQuery.browser.msie){
            $("#ctl00_PlaceHolderMain_DdlPays_DdlCountries").hide();
            $("#ctl00_PlaceHolderMain_DdlSujet").hide();
        }
		return false;
	});
	
    // Popin
    // Evolution 1881
	$("a.lookingfor-tous-les-sites").click(function() {
		PPNinit($(this));
		$(".flash_content").hide();
		if(jQuery.browser.msie){
            $("#ctl00_PlaceHolderMain_DdlPays_DdlCountries").hide();
            $("#ctl00_PlaceHolderMain_DdlSujet").hide();
        }
		return false;
	});
	
	
		
	
	
	// Flash
	/*$(".home_flash").flash({
		src:"../flash/home.swf",
		width:937,
		height:339,
		wmode:'transparent',
		flashvars:{
			image:"../flash/home.jpg"
		}},{
			version:8
		}
	);*/
	/*
	$(".flash_mp_rub").flash({
		src:"../flash/mp_rub.swf",
		width:720,
		height:284,
		wmode:'transparent',
		flashvars:{
			image:"../flash/mp_rub.jpg"

		}},{
			version:8
		}
	);*/
	$(".flash_col_menu").flash({
		src:"../flash/col_menu.swf",
		width:251,
		height:147,
		wmode:'transparent',
		flashvars:{
			image:"../flash/col_menu.jpg"
		}},{
			version:8
		}
	);
	$(".flash_3col_principale").flash({
		src:"../flash/col_3col_principale.swf",
		width:457,
		height:263,
		wmode:'transparent',
		flashvars:{
			image:"../flash/col_3col_principale.jpg"
		}},{
			version:8
		}
	);
	$(".flash_3col_principale_big").flash({
		src:"../flash/col_3col_principale_big.swf",
		width:747,
		height:421,
		wmode:'transparent',
		flashvars:{
			image:"../flash/col_3col_principale_big.jpg"
		}},{
			version:8
		}
	);
	$(".flash_interview").flash({
		src:"../flash/interview.swf",
		width:188,
		height:92,
		wmode:'transparent',
		flashvars:{
			image:"../flash/interview.jpg"
		}},{
			version:8
		}
	);	
	$(".conteneur_video").flash({
		src:"../flash/visio.swf",
		width:631,
		height:355,
		wmode:'transparent',
		flashvars:{
			image:"../flash/visio_video.jpg"
		}},{
			version:8
		}
	);
	$(".flash_car_selector").flash({
		src:"../flash/flash_car_selector.swf",
		width:937,
		height:590,
		wmode:'transparent',
		flashvars:{
			image:"../flash/flash_car_selector.jpg"

		}},{
			version:8
		}
	);
	$(".flash_fiche_produit").flash({
		src:"../flash/flash_fiche_produit.swf",
		width:936,
		height:432,
		wmode:'transparent',
		flashvars:{
			image:"../flash/flash_fiche_produit.jpg"
		}},{
			version:8
		}
	);		
	
	

	
	
	
	
	// Masquage
	$("html").click(function(e) {
		e = e.target;
		if(
		   		($(e).attr("class") != "lookingfor-liste-01-item")
		   	&&	($(e).attr("class") != "lookingfor-liste-02-item")
		   	&&	($(e).attr("class") != "acces_vehicule")
		   	&&	($(e).attr("class") != "fiche_produit_action_liste-01-item")
		  ) {
			$("body > ul").slideUp("fast", function() {$(this).remove();})	
			$("ul.sous_liste, ul.fiche_produit_action_liste-01-liens").slideUp();		
		}
	});
	
	
	
	
	// Plier-Deplier Archives
	$(".archives_liste_box .archives_liste_links").css("display","none");
	$(".archives_liste_box p").addClass("close");
	$(".archives_liste_box .contenu_p a").click(function() {
		$(this).parent().parent().children(".archives_liste_links").toggle();
		$(this).toggleClass("toggle");
		if($(this).is(".toggle")){
			$(this).parent().removeClass("close");
			$(this).parent().addClass("open");
		}
		else {
			$(this).parent().removeClass("open");
			$(this).parent().addClass("close");
		}
		
		return false;
	});


	// Acc�s direct vehicule
	$(".liste_acces").each(function() {
		$(".liste_acces .sous_liste").clone().prependTo($(this).parent());
	});
	$(".liste_acces .sous_liste").remove();
	$(".liste_acces li span").click(function() {
		$(".liste_acces ul").hide();
		$("ul." + $(this).attr("class") ).slideDown();
		return false;
	});


	// Scroll automatique du Car Selector
	if($(".car_selector").is("div") == true) {
		o = $(".car_selector").offset();
		window.scrollTo(0, (o.top - 5));
	}
	
	
	// Fiche produit actions
	$(".fiche_produit_action_item_titre").click(function() {
		$("body > .fiche_produit_action_liste-01-liens").remove();
		$(".fiche_produit_action_item").removeClass("open");
		$(this).parent().addClass("open");
		$(".fiche_produit_action_item dl").hide();
		$(this).next("dl").show();
	});
	$(".fiche_produit_action_liste-01-select").click(function() {
		o = $(this).offset();
		$(this).find(".fiche_produit_action_liste-01-liens").clone().prependTo("body");
		//$(this).find(".fiche_produit_action_liste-01-liens").remove();
		$(".fiche_produit_action_liste-01-liens").css("top", (o.top + 17) );
		$(".fiche_produit_action_liste-01-liens").css("left", (o.left + 1) );
		$(".fiche_produit_action_liste-01-liens").slideDown();
	});
	
	// Gestion loupe
	$(".zone_illustration_visuel .contenu_visuel").mouseover(function() {
		$(this).parent("p").prev(".loupe").show();
	});
	$(".zone_illustration_inner").mouseout(function() {
		$(".loupe").hide();
	});
	
	
	
});