/*@cc_on @*/
/*@if (@_win32)
	var ua = navigator.userAgent;
	var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
	if (re.exec(ua) != null) {
		ie_rv = parseFloat( RegExp.$1 );
	}
/*@end @*/
function fnOpenWindow(url,parameters)
{
	parameters=parameters.replace(/\s+/g,'');

	var aParameters=parameters.split(',');
	var data=new Array();
	for (var i=0;i<aParameters.length;i++) {
		var pr=aParameters[i];
		var key = pr.substr(0,pr.indexOf('='));
		var value=pr.substr(pr.indexOf('=')+1);
		data[key]=value;
	}

	if ( (data['hposition'])&&(data['width'])&&(data['hposition']!='system') )
	{
		var posX=null;
		if (data['hposition']=='left')
		{
			posX=0;
		}
		if ( (window.screen)&&(window.screen.availWidth) )
		{
			if (data['hposition']=='center')
				posX=(window.screen.availWidth-data['width'])/2;
			if (data['hposition']=='right')
				posX=window.screen.availWidth-data['width'];
		}

		if (posX!=null)
			parameters+=",left="+parseInt(posX);
	}

	if ( (data['vposition'])&&(data['height'])&&(data['vposition']!='system') )
	{
		var posY=null;
		if (data['vposition']=='top')
		{
			posY=0;
		}
		if ( (window.screen)&&(window.screen.availHeight) )
		{
			if (data['vposition']=='center')
				posY=(window.screen.availHeight-data['height'])/2;
			if (data['vposition']=='bottom')
				posY=window.screen.availHeight-data['height'];
		}

		if (posY!=null)
			parameters+=",top="+parseInt(posY);
	}


	var oWnd=window.open(url,'',parameters);
	oWnd.focus();
}

function getById(el) {
	return document.getElementById(el);
}

function getByTag(par,el) {
	var par = (par == '') ? document.body : par;
	if (!par) return new Array();
	return par.getElementsByTagName(el);
}

function createEl(tag,classN,content) {
	var el = document.createElement(tag);
	if(classN&&(classN!='')) el.className = classN;
	if(content&&(content!='')) el.innerHTML = content;
	return el;
}

function addClass(obj,newClass) {
	if(!obj.className.match(new RegExp(newClass)))
		obj.className+=(obj.className.length>0? " ": "") + newClass;
}
	
function removeClass(obj,oldClass) {
	obj.className=obj.className.replace(new RegExp("( ?|^)"+oldClass+"\\b"), "");
}

function addCSSRule(sheet,selector,val) {
	if(sheet.insertRule)
		sheet.insertRule(selector + ' {'+val+';}',sheet.cssRules.length);
	else if(sheet.addRule)
		sheet.addRule(selector, val)
}

function getByClass(cla,par,el) {
	var getEls;
	if (/.*native code.*/.test(document.getElementsByClassName)) {
		getEls = function(cla,par,el) { 
			return par.getElementsByClassName(cla);
		}
	}
	else { 
		getEls = function(cla,par,el) { 
			var tagColl = par.getElementsByTagName(el);
			trimedColl = new Array;
			for (var i = 0; tagColl[i]; i++) {
				if(tagColl[i].className.match(new RegExp("( ?|^)"+cla+"\\b")))
					trimedColl[trimedColl.length]=tagColl[i];
			}
			return trimedColl;
		} 
	} 
	getByClass = function(cla,par,el) {
		var par = (!par||(par == '')) ? document.body : par;
		var el = (el == '') ? '*' : el;
		return getEls(cla,par,el); 
	}
	return getByClass(cla,par,el);
}

function getStyle(obj,cssRule) {
	if (document.defaultView && document.defaultView.getComputedStyle) {
		getStyle = function(obj,cssRule) {
			return document.defaultView.getComputedStyle(obj, "").getPropertyValue(cssRule);
		};
	}
	else {
		getStyle = function(obj,cssRule) {
			if (obj.currentStyle) {
				cssRule = cssRule.replace(/\-(\w)/g, function (match, p1) {
					return p1.toUpperCase();
				});
				return obj.currentStyle[cssRule];
			}
		};
	}
	return getStyle(obj,cssRule);
}

function findPosition( oElement ) {
  if( typeof( oElement.offsetParent ) != 'undefined' ) {
    for( var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent ) {
      posX += oElement.offsetLeft;
      posY += oElement.offsetTop;
    }
    return [ posX, posY ];
  } else {
    return [ oElement.x, oElement.y ];
  }
}

function getPageY() {
	var docHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
	// Order maters here as for IE 6 documentElement.clientHeight != body.clientHeight
	var docScrollY = document.documentElement.scrollTop || document.body.scrollTop;
	return [docHeight,docScrollY];
}

var addEvent = function() {
  if (window.addEventListener) {
    return function(el, type, fn) {
      el.addEventListener(type, fn, false);
    };
  } else if (window.attachEvent) {
    return function(el, type, fn) {
      var f = function() {
        fn.call(el, window.event);
      };
      el.attachEvent('on' + type, f);
    };
  }
}();

function GET_XMLHTTPRequest() {
	var request;
	try{
		request = new ActiveXObject("Microsoft.XMLHTTP");
	}
	catch(ex1){
		try{
			request = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch(ex2){
			try{
				request = new ActiveXObject("Msxml3.XMLHTTP");
			}
			catch(ex3){
				request = null;
			}
		}
	}
	if(!request && typeof XMLHttpRequest != "undefined"){
		request = new XMLHttpRequest();
	}
	return request;
}

function setLinks(linkTags,prt){
	var linkCollection = [];
	for (var j = 0; linkTags[j]; j++) {
		linkCollection[j] = getByTag(prt,linkTags[j]);
		for (var i = 0; linkCollection[j][i]; i++) {
			if ( linkCollection[j][i].href.indexOf('/link/dl')!=-1 ) {
				linkCollection[j][i].onclick=trackBinaryDocument;
			}
			else if(/zoom/.test(linkCollection[j][i].getAttribute('rel'))) {
				new Zoom(linkCollection[j][i]);
			}
			else if(/bookmark|external|corporate|download/.test(linkCollection[j][i].getAttribute('rel'))) {
				linkCollection[j][i].onclick = function(e) {
					if (!e) var e = window.event;
					if (e.shiftKey || e.altKey || e.ctrlKey || e.metaKey)
						return true;
					window.open(this.href);
					return false;
				}
			}
		}
	}
}

trackBinaryDocumentRequest=null;
function trackBinaryDocument(e)
{	
	var hr=this.href;
	if ( hr.indexOf("?")==-1) return true;
	hr=hr.substr( hr.indexOf("?")+1 );


	trackBinaryDocumentRequest = null;
	try {
		trackBinaryDocumentRequest = new XMLHttpRequest();
	} catch (trymicrosoft) {
		try {
			trackBinaryDocumentRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (othermicrosoft) {
		try {
			trackBinaryDocumentRequest = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (failed) {
			trackBinaryDocumentRequest = null;
		}
		}
	}
	
	if (!trackBinaryDocumentRequest) return true;

	var url="/statDocument?"+hr;
	trackBinaryDocumentRequest.open("GET", url, true);
	trackBinaryDocumentRequest.onreadystatechange = onTrackBinaryDocumentReady;
	trackBinaryDocumentRequest.send(null);

	if (!e) var e = window.event;
	if (e.shiftKey || e.altKey || e.ctrlKey || e.metaKey)
		return true;

	window.open(this.href);
	return false;
}

function onTrackBinaryDocumentReady()
{
	if (!trackBinaryDocumentRequest) return;
	if (trackBinaryDocumentRequest.readyState != 4) return;
	if (trackBinaryDocumentRequest.status != 200) return;

	eval("binaryStats="+trackBinaryDocumentRequest.responseText);
	if (binaryStats.name) {
		stat('veolia','','','documents_pdf;'+binaryStats.name);
	}
}

function fixIE() {
	separateTools();
	separateNav();
	separateFooter();
	addToolTipIco();
}

function fixLowIE() {
	toolsHover();
	navHover();
}

function addToolTipIco() {
	var tColl = getByClass('tip',getById('content'),'a');
	for(var i = 0; tColl[i]; ++i) {
		tColl[i].innerHTML = tColl[i].innerHTML + '<span class="tipIcon"> </span>';
		// tColl[i].onmouseover = function() {
		// 	var t = getByClass('tooltip',this,'span');
		// 	if(t&&t[0])
		// 		t[0].style.zIndex = 9999;
		// 	this.style.zIndex = 9999;
		// }
	}
}

function separateFooter() {
	var f = getById('footer');
	if(!f) return;
	var ulColl = getByTag(f,'ul');
	for(var i = 0; ulColl[i]; ++i) {
		var liColl = getByTag(ulColl[i],'li');
		for(var j = 1; liColl[j]; j++) {
			liColl[j].innerHTML = ' | ' + liColl[j].innerHTML;
		}
	}
}

function separateTools() {
	var t = getById('tools');
	if(!t) return;
	var ulColl = getByTag(t,'ul');
	for(var i = 0; ulColl[i]; ++i) {
		var liColl = getByTag(ulColl[i],'li');
		for(var j = 1; liColl[j]; j++) {
			if(/corp|directAccess|lang|search/.test(liColl[j].className))
				liColl[j].innerHTML = '|   ' + liColl[j].innerHTML;
		}
		for(var j = 0; liColl[j]; j++) {
			liColl[j].style.zIndex = 999;
		}
	}
}

function separateNav() {
	var n = getById('navigation');
	if(!n) return;
	var ulColl = getByTag(n,'ul');
	for(var i = 0; ulColl[i]; ++i) {
		var liColl = getByTag(ulColl[i],'li');
		for(var j = 1; liColl[j]; j++) {
			if(liColl[j].parentNode.parentNode==n)
				liColl[j].innerHTML = ' | ' + liColl[j].innerHTML;
		}
		if(liColl[0].parentNode.parentNode!=n)
			addClass(liColl[0],'first-child');
		for(var j = 0; liColl[j]; j++) {
				liColl[j].style.zIndex = 998;
		}
	}
}

function navHover() {
	var n = getById('navigation');
	if(!n) return;
	var liColl = getByTag(n,'li');
	for(var i = 0; liColl[i]; ++i) {
		var s = liColl[i].getElementsByTagName('div')[2];
		if(s)
			s.appendChild(document.createElement('iframe'));
		liColl[i].onmouseover = function() {
			addClass(this,'hover');
			var ifr = this.getElementsByTagName('iframe')[0];
			if(ifr)
				ifr.style.height=ifr.parentNode.offsetHeight+7+'px';
		}
		liColl[i].onmouseout = function() {
			removeClass(this,'hover');
		}
	}
}

function toolsHover() {
	var t = getById('tools');
	if(!t) return;
	var liColl = getByTag(t,'li');
	for(var i = 0; liColl[i]; ++i) {
		var s = liColl[i].getElementsByTagName('div')[2];
		if(s)
			s.appendChild(document.createElement('iframe'));
		liColl[i].onmouseover = function() {
			addClass(this,'hover');
			var ifr = this.getElementsByTagName('iframe')[0];
			if(ifr)
				ifr.style.height=ifr.parentNode.offsetHeight+7+'px';
		}
		liColl[i].onmouseout = function() {
			removeClass(this,'hover');
		}
	}
}

/* Overlay
---------------------- */
function setFlashZoom(l){
	new FlashZoom(l);
	return false;
}

function FlashZoom(l){
	initOverlay();
	this.overlay = getById('overlay');
	this.container = getById('overlayContainer');
	this.container.w = 400;
	this.container.h = 250;
	this.click(l);
	return false;
}

FlashZoom.prototype = {
	click: function (file) {
		this.file = file + '&js=true';
		this.overlay.innerHTML = '<span id="loading"></span>';
		getById('loading').style.top = ((getPageY()[0] - 36) / 2) + getPageY()[1] + 'px';

		if(getById('ie6overlay'))
			removeClass(getById('ie6overlay'),'hidden');
			
		this.overlay.style.height = getById('page').offsetHeight + 'px';

		removeClass(this.overlay,'hidden');
		addClass(this.container,'transparent');
		removeClass(this.container,'hidden');

		var req = GET_XMLHTTPRequest();
		if (req) {
			req.open("GET", this.file, true);
			req.setRequestHeader('User-Agent','XMLHTTP/1.0');
			req.onreadystatechange = function (that) {
		        return function (aEvt) {
					if (req.readyState != 4) return;
					if (req.status != 200 && req.status != 304) {
						that.handleError(req);
						return;
					}
		            if(req.readyState == 4){
						that.handleRequest(req);
					}
		        }
		    }(this);
			req.send(null);
		}
		else {
			getById('overlayContainer').innerHTML = 'Unable to load content';
			this.createNav();
			this.changePosition();
		}
		return false;
	},
	changePosition: function () {
		this.container.style.width = this.container.w + 'px';
		this.container.style.height = this.container.h + 'px';
		this.container.style.marginLeft = -(this.container.w)/2 + 'px';
		var t = ((getPageY()[0] - this.container.h) / 2) + getPageY()[1];
		if(t < 0 ) {t = 0;}
		this.container.style.top = t + 'px';
		this.overlay.innerHTML = '';
		removeClass(this.container,'transparent');
	},
	handleRequest: function(req) {
		this.container.innerHTML = req.responseText;
		var innerContent = getById('overlayInnerContent');
		if(innerContent) {
			this.createNav();
			this.container.w = innerContent.offsetWidth;
			var d = getByClass('diaporama',this.container,'div');
			if(d&&d[0]) {
				setDiaporama(innerContent,'overlay');
				setLinks(['a','area'],d[0]);
			}
			this.container.h = innerContent.offsetHeight;
			this.changePosition();
			var c = getByClass('content',this.container,'div');
			if(c&&c[0]) {
				setLinks(['a','area'],c[0]);
				initSwfObjects(c[0]);
			}
		}
	},
	handleError: function(req) {
		this.container.innerHTML = '<strong>Data error :</strong> HTTP error' + req.status + '';
		this.container.w = 400;
		this.container.h = 250;
		this.createNav();
		this.changePosition();
	},
	createNav: function () {
		if(!getById('overlayNav')){
			var div = document.createElement('div');
			div.id = 'overlayNav';
			this.container.appendChild(div);
		}
		this.nav = getById('overlayNav');
		this.nav.items = getByTag(this.nav,'a');
		for(var i = 0; this.nav.items[i]; i++) {
			new Zoom(this.nav.items[i]);
		}
		var c = createEl('a','closer','');
		c.href= "#";
		c.onclick = function (that) {
	        return function () {
				that.close();
				return false;
			}
		}(this);
		this.closer = this.nav.appendChild(c);
	},
	close: function () {
		addClass(this.container,'hidden');
		addClass(this.overlay,'hidden');
		if(getById('ie6overlay'))
			addClass(getById('ie6overlay'),'hidden');
		this.container.innerHTML = '';
		return false;
	}
}


function Zoom(a) {
	initOverlay();
	this.a = a;
	this.overlay = getById('overlay');
	this.container = getById('overlayContainer');
	this.container.w = 400;
	this.container.h = 250;
	this.a.onclick = function (that) {
        return function () {
            that.click(this.href);
			return false;
        }
    }(this);
}

Zoom.prototype = {
	click: function (file) {
		this.file = file + '&js=true';
		this.overlay.innerHTML = '<span id="loading"></span>';
		getById('loading').style.top = ((getPageY()[0] - 36) / 2) + getPageY()[1] + 'px';

		if(getById('ie6overlay'))
			removeClass(getById('ie6overlay'),'hidden');
			
		this.overlay.style.height = getById('page').offsetHeight + 'px';

		removeClass(this.overlay,'hidden');
		addClass(this.container,'transparent');
		removeClass(this.container,'hidden');

		var req = GET_XMLHTTPRequest();
		if (req) {
			req.open("GET", this.file, true);
			req.setRequestHeader('User-Agent','XMLHTTP/1.0');
			req.onreadystatechange = function (that) {
		        return function (aEvt) {
					if (req.readyState != 4) return;
					if (req.status != 200 && req.status != 304) {
						that.handleError(req);
						return;
					}
		            if(req.readyState == 4){
						that.handleRequest(req);
					}
		        }
		    }(this);
			req.send(null);
		}
		else {
			getById('overlayContainer').innerHTML = 'Unable to load content';
			this.createNav();
			this.changePosition();
		}
	},
	changePosition: function () {
		this.container.style.width = this.container.w + 'px';
		this.container.style.height = this.container.h + 'px';
		this.container.style.marginLeft = -(this.container.w)/2 + 'px';
		var t = ((getPageY()[0] - this.container.h) / 2) + getPageY()[1];
		if(t < 0 ) {t = 0;}
		this.container.style.top = t + 'px';
		this.overlay.innerHTML = '';
		removeClass(this.container,'transparent');
	},
	handleRequest: function(req) {
		this.container.innerHTML = req.responseText;
		var innerContent = getById('overlayInnerContent');
		if(innerContent) {
			this.createNav();
			this.container.w = innerContent.offsetWidth;
			var d = getByClass('diaporama',this.container,'div');
			if(d&&d[0]) {
				setDiaporama(innerContent,'overlay');
				setLinks(['a','area'],d[0]);
			}
			this.container.h = innerContent.offsetHeight;
			this.changePosition();
			var c = getByClass('content',this.container,'div');
			if(c&&c[0]) {
				setLinks(['a','area'],c[0]);
				initSwfObjects(c[0]);
			}
		}
	},
	handleError: function(req) {
		this.container.innerHTML = '<strong>Data error :</strong> HTTP error' + req.status + '';
		this.container.w = 400;
		this.container.h = 250;
		this.createNav();
		this.changePosition();
	},
	createNav: function () {
		if(!getById('overlayNav')){
			var div = document.createElement('div');
			div.id = 'overlayNav';
			this.container.appendChild(div);
		}
		this.nav = getById('overlayNav');
		this.nav.items = getByTag(this.nav,'a');
		for(var i = 0; this.nav.items[i]; i++) {
			new Zoom(this.nav.items[i]);
		}
		var c = createEl('a','closer','');
		c.href= "#";
		c.onclick = function (that) {
	        return function () {
				that.close();
				return false;
			}
		}(this);
		this.closer = this.nav.appendChild(c);
	},
	close: function () {
		addClass(this.container,'hidden');
		addClass(this.overlay,'hidden');
		if(getById('ie6overlay'))
			addClass(getById('ie6overlay'),'hidden');
		this.container.innerHTML = '';
		return false;
	}
}

function initOverlay () {
	if(!getById('overlay')) {
		var ov = createEl('div','hidden','');
		ov.id = "overlay";
		ov.style.height = getById('page').offsetHeight + 'px';
		document.getElementsByTagName('body')[0].appendChild(ov);
		/*@if (@_win32)
			if(ie_rv < 7) {
				var f = document.createElement('iframe');
				f.id= 'ie6overlay';
				f.className = 'hidden';
				document.getElementsByTagName('body')[0].appendChild(f);
			}
		/*@end @*/
	}

	if(!getById('overlayContainer')) {
		var ovCt = createEl('div','hidden','');
		ovCt.id = "overlayContainer";
		document.getElementsByTagName('body')[0].appendChild(ovCt); 
	}
}

/* MiniMenu
----- */
function setMiniMenus() {
	var m = getByClass('miniSelect',getById('content'),'div');
	if(m&&m[0]) {
		for(var i = 0; m[i]; ++i) {
			var itms = getByClass('item',m[i],'div');
			if(itms&&itms.length>1) {
				if(m[i].firstChild.nodeType==8){
					var selDefaultTxt = m[i].firstChild.data;
					var it = createEl('div','item defaultItem','<h3>'+selDefaultTxt+'</h3><div class="data">&nbsp;</div>');
					m[i].insertBefore(it,itms[0]);
					itms = getByClass('item',m[i],'div');
				}
				new MiniMenu(m[i],itms);
			}
		}
	}
}

function MiniMenu(prt,itms) {
	addClass(prt,'minified');
	this.prt = prt;
	for(var i = 0; itms[i]; ++i) {
		var t = getByTag(itms[i],'h3');
		itms[i].tit = (t&&t[0]) ? t[0] : false;
		var d = getByClass('data',itms[i],'div');
		itms[i].txt = (d&&d[0]) ? d[0] : false;
		itms[i].cId = i;
	}
	this.items = itms;
	this.cId = -1;
	var s = document.createElement('select');
	prt.insertBefore(s,itms[0]);
	var st = '';
	for(var i = 0; itms[i]; ++i) {
		if(itms[i].tit&&itms[i].tit.innerHTML&&itms[i].txt&&itms[i].txt.innerHTML){
			var o = document.createElement('option');
			o.value = i;
			o.innerHTML = itms[i].tit.innerHTML;
			if(this.cId == -1) {
				this.cId = i;
				o.setAttribute('selected',"selected");
			}
			s.appendChild(o);
		}
	}
	s.onchange = function(that) {
		return function() {
			that.change(this[this.selectedIndex].value);
		}
	}(this);
	this.change(this.cId);
}

MiniMenu.prototype = {
	change: function (cId) {
		removeClass(this.items[this.cId],'current');
		addClass(this.items[cId],'current');
		this.cId = cId;
	}
}


function initSwfObjects(prt) {
	if(prt=='') prt = document.getElementsByTagName('body')[0];
	var obs = getByTag(prt,'object');
	if(obs&&obs[0]) {
		for(var i = 0; obs[i]; ++i) {
			if(obs[i].id&&obs[i].id!='') {
				var io = getByTag(obs[i],'object');
				if(io&&io[0]&&io[0].type&&/flash/.test(io[0].type)) {
					if(obs[i].id){
						var fv = obs[i].className.split('flashVersion-')[1];
						if(fv&&fv!='') {
							var reg=new RegExp("-", "g");
							fv = fv.replace(reg,'.');
						}
						else fv = '8.0.0';
						swfobject.registerObject(obs[i].id, fv, "/static/media/swf/expressInstall.swf");
					}
				}
			}
		}
	}
}

/* 
----- */
function setDiaporama(prt,type) {
	if(!prt) var prt = getById('content');
	var d = getByClass('diaporama',prt,'div');
	if(!d||!d[0]) return;
	for(var i = 0; d[i]; ++i) {
		var di = getByClass('diapo',d[i],'div');
		var id = (type&&(type=='overlay')) ? i + '_o': i;
		if(di&&di[1]) new Diapo(d[i],di,id,type);
	}
}

function Diapo(container,diapos,id,type) {
	this.container = container;
	addClass(this.container,'diapo_'+id+'_');
	this.dClass = '.diapo_'+id+'_';
	this.cId = 0;
	this.data_h = 20;
	var s = document.styleSheets;
	var lastS = -1;
	for(var i = 0; i < s.length; ++i) {
		if(s[i].disabled==false)
			if(!/print|text/.test(s[i].href)) lastS = i;
	}
	for(var i = 0; diapos[i]; ++i) {
		if(/current/.test(diapos[i].className))
			this.cId = i;
		var d = getByClass('data',diapos[i],'div');
		if(d&&d[0]) {
			if(i==0) {
				var w = this.container.clientWidth - parseInt(getStyle(d[0],'padding-left'),10) - parseInt(getStyle(d[0],'padding-right'),10);
				if(lastS!=-1)
					addCSSRule(s[lastS],this.dClass+' .data','width: '+w+'px')
			}
			if(d[0].clientHeight > this.data_h) this.data_h = d[0].clientHeight;
		}
	}
	var fs = 14;
	if (document.defaultView && document.defaultView.getComputedStyle)
		fs = parseInt(getStyle(this.container,'font-size'),10);
	else {
		var t = createEl('span','wai','X');
		this.container.appendChild(t);
		t.style.left = '10em';
		fs = t.style.pixelLeft / 10;
		this.container.removeChild(t);
	}
	var h = parseInt(this.data_h,10) / fs;
	if(lastS!=-1)
		addCSSRule(s[lastS],this.dClass+' .data','height: '+h+'em');
	this.diapos = diapos;
	this.max = diapos.length;
	this.addNav(type);
	this.state = 'play';
	this.ppTxt = ['<strong>||</strong>', '►'];
	this.rotate();
}

Diapo.prototype = {
	addNav: function (type) {
		var dn = createEl('div','diapoNav','');
		this.container.appendChild(dn);
		this.nav_r = dn.appendChild(createEl('button','reposition','Focus'));
		this.nav_pp = dn.appendChild(createEl('button','playPause','<strong>||</strong>'));
		this.nav_p = dn.appendChild(createEl('button','prev','&laquo;'));
		this.count_d = dn.appendChild(createEl('span','',this.cId+1+'/'+this.max));
		this.nav_n = dn.appendChild(createEl('button','next','&raquo;'));
		this.nav_r.onclick = function (that) {
	        return function () {
				that.focusOn();
				return false;
			}
		}(this);
		this.nav_p.onclick = function (that) {
			return function () {
				that.budge('prev');
				return false;
			}
		}(this);
		this.nav_n.onclick = function (that) {
	        return function () {
				that.budge('next');
				return false;
			}
		}(this);
		this.nav_pp.onclick = function (that) {
	        return function () {
				that.toggleState();
				return false;
			}
		}(this);
		/*@if (@_win32)
			if(ie_rv < 8) {
				var bs = getByTag(this.container,'button');
				if(bs&&bs[0]){
					for(var i = 0; bs[i]; ++i) {
						bs[i].onmouseover = function() {addClass(this,'hover');}
						bs[i].onmouseout = function() {removeClass(this,'hover');}
						bs[i].onfocus = function() {addClass(this,'focus');}
						bs[i].onblur = function() {removeClass(this,'focus');}
					}
				}
			}
		/*@end @*/
		if(type&&(type=='overlay')) {
			var c = getByClass('closer',getById('overlayContainer'),'a');
			if(c&&c[0]){
				this.shortcuts = 'on';
				this.addShortcuts();
				this.close = c[0].onclick;
				c[0].onclick = function(that) {
					return function() {
						document.onkeydown = function () {}
						that.close();
						return false;
					}
				}(this);

			}
			this.reFocus();
		}
		
	},
	addShortcuts: function() {
		document.onkeydown = function (that) {
	        return function (e) {
				if (!e) var e = window.event;
				if (e.metaKey || e.ctrlKey) return;
				var pressed = false;
				switch(e.keyCode) {
					case 27: // escape
						document.onkeydown = function () {}
						that.close();
						pressed = true;
					break;
					
					case 32: // space
						that.toggleState('keypress');
						pressed = true;
					break;

					case 37: // left
						that.budge('prev');
						pressed = true;
					break;

					case 39: // right
						that.budge('next');
						pressed = true;
					break;
				}
				if (pressed && e.preventDefault)
					e.preventDefault();
			}
		}(this);
	},
	shift: function(dir) {
		var n = (dir=='next') ? ( (this.cId+1 == this.max) ? 0 : this.cId+1 ) : ( (this.cId-1 < 0) ? this.max-1 : this.cId-1 );
		removeClass(this.diapos[this.cId],'current');
		addClass(this.diapos[n],'current');
		this.cId = n;
		this.count_d.innerHTML = this.cId+1+'/'+this.max;
	},
	rotate: function() {
		var speed = (this.rotate.arguments.length>0) ? this.rotate.arguments[0] : 5000;
		this.rTimeout = setTimeout(function (that) {
			return function () {
				that.shift('next');
				that.rotate();
			}
		}(this), speed);
	},
	toggleState: function(t) {
		var p = (this.state=='play');
		if(p) clearTimeout(this.rTimeout);
		else this.rotate(300);
		if(t=='keypress') {
			this.nav_pp.innerHTML = (p) ? this.ppTxt[0] : this.ppTxt[1];
			clearTimeout(this.kTimeout);
			this.kTimeout = setTimeout(function (that) {
				return function () {
					that.nav_pp.innerHTML = (that.state!='play') ? that.ppTxt[1] : that.ppTxt[0];
				}
			}(this), 1600);
		}
		else
			this.nav_pp.innerHTML = (p) ? this.ppTxt[1] : this.ppTxt[0];
		this.state = (p) ? 'pause' : 'play';
		this.reFocus();
	},
	budge: function(d) {
		clearTimeout(this.rTimeout);
		this.shift(d);
		if(this.state=='play') this.rotate();
		this.reFocus();
	},
	reFocus: function() {
		if(window.focus) this.nav_r.focus();
	},
	focusOn: function() {
		if(window.focus) {
			this.nav_pp.focus();
			if(this.shortcuts=='on') {
				clearTimeout(this.fTimeout);
				this.fTimeout = setTimeout(function (that) {
					return function () {
						that.nav_r.focus();
					}
				}(this), 1500);
			}
		}
	}
}

/* Challenges
----- */
function setChallenges() {
	var c = getById('challenges');
	var ac = getById('challengeAnchors');
	if(!c||!ac) return;
	var a = getByTag(ac,'a');
	if(a&&a[0])
		new Challenges(c,a);
}

function Challenges(c,a) {
	this.container = c;
	this.challenges = [];
	this.cId = 0;
	this.max = 0;
	this.timeOut = [];
	this.nxt = 0;
	this.prev = 0;
	this.step = 50;
	this.direction = '';
	this.animType = '';
	this.timer = 0;
	this.doubledUp = false;
	this.moving=-1;
	var h1 = getByTag(getById('intro'),'h1');
	var tTxt = (h1&&h1[0]) ? h1[0].innerHTML : '';
	for(var i = 0; a[i]; ++i) {
		var h = a[i].href.split('#')[1];
		if(h&&(h!='')){
			var t = getById(h);
			if(t&&/challenge/.test(t.className)) {
				a[i].cId = t.cId = i;
				this.challenges[i] = t;
				// var ti = getByClass('title',t,'div');
				// if(ti&&ti[0])
				// 	ti[0].insertBefore(createEl('span','meta',tTxt),getByTag(t,'h2')[0]);
				if(/current/.test(t.className)) this.cId = i;
				this.max++;
				a[i].onclick = function(that) {
					return function() {
						that.show(this.cId);
						return false;
					}
				}(this);
			}
		}
	}
	for(var i = 0; this.challenges[i]; ++i) {
		var ovs = getByTag(this.challenges[i],'a');
		for(var j = 0; ovs[j]; ++j) {
			ovs[j].cId = j;
			ovs[j].challengeId = this.challenges[i].cId;
		}
		this.challenges[i].overlays = ovs;
		this.challenges[i].ovMax = ovs.length;
	}
	if(this.max>0)
		this.setNav();
		
		
	var l = document.location.href.split('?');
	
	if(l&&l[1]) {
		var defi = false;
		var item = false;
		var params = l[1].split('&');
		for(var i = 0; params[i]; ++i) {
			var param = params[i].split('=');
			if(param&&param[1]) {
				if(/defi/.test(param[0]))
					defi = param[1];
				else if (/item/.test(param[0]))
					item = parseInt(param[1]);
			}
		}
		if(defi) {
			for(var i = 0; this.challenges[i]; ++i) {
				if(this.challenges[i].id==defi){
					this.show(this.challenges[i].cId);
					var chaId = i;
				}
			}
		}
		if(defi&&item&&(item>0)) {
			if(this.challenges[chaId].overlays[item-1]) {
				this.overlay(item-1,chaId);
				this.addOverlayNav(item-1,chaId);
			}
		}
	}
}
Challenges.prototype = {
	setNav: function() {
		var bTxt = 'Summary';
		var cTxt = 'Close';
		var nTxt = 'Next';
		var pTxt = 'Previous';
		if (/i18n-fr/.test(document.getElementsByTagName('body')[0].className)) {
			bTxt = 'Sommaire'; cTxt = 'Fermer'; nTxt = 'Suivant'; pTxt = 'Précédent';
		}
		this.back = this.container.appendChild(createEl('button','summaryBtn',bTxt));
		this.back.onclick = function(that) {
			return function(){
				that.hide();
			}
		}(this);
		this.nxt = this.container.appendChild(createEl('button','nextBtn',nTxt));
		this.nxt.onclick = function(that) {
			return function(){
				that.shift('next');
			}
		}(this);
		this.prev = this.container.appendChild(createEl('button','prevBtn',pTxt));
		this.prev.onclick = function(that) {
			return function(){
				that.shift('prev');
			}
		}(this);
		var highlight = createEl('div','','');
		highlight.id = 'highlight';
		this.highlight = this.container.appendChild(highlight);
		this.highlightHTML = this.highlight.appendChild(createEl('div','',''));
		this.highlightHTML.id = 'highlightInner';
		this.closer = this.highlight.appendChild(createEl('button','closeBtn',cTxt));
		this.closer.onclick = function(that) {
			return function(){
				that.close();
			}
		}(this);
		var aColl = getByTag(this.container,'a');
		for(var i = 0; aColl[i]; ++i) {
			aColl[i].onclick = function(that) {
				return function() {
					that.overlay(this.cId,this.challengeId);
					that.addOverlayNav(this.cId,this.challengeId);
					return false;
				}
			}(this);
		}
		var highlight2 = createEl('div','','');
		highlight2.id = 'highlight2';
		highlight2.className='type2';
		this.highlight2 = this.container.appendChild(highlight2);
		this.highlight2HTML = this.highlight2.appendChild(createEl('div','',''));
		this.highlight2HTML.id = 'highlight2Inner';
		this.closer2 = this.highlight2.appendChild(createEl('button','closeBtn',cTxt));
		this.closer2.onclick = function(that) {
			return function(){
				that.close2();
			}
		}(this);
	},
	show: function(cId){
		this.reveal(cId,'static');
		addClass(this.container,'challenging');
	},
	hide: function(){
		this.doubledUp = false;
		this.close();
		this.close2();
		removeClass(this.container,'challenging');
	},
	reveal: function(cId,dir) {
		
		this.direction = dir;
		this.animType = 'challenge';

		if(dir!='static') {
			if(this.moving==-1)
				this.moving=cId;
			if(this.moving!=cId) return;
			this.step = 50;
			this.challenges[cId].style.left = (this.direction=='next') ? 980+'px' : -980+'px';
			this.timeOut[cId] = setTimeout(function (that) {
				return function () {
					that.animate(that.cId);
				}
			}(this), 10);
			addClass(this.challenges[this.cId],'moving');
			this.moveOut(this.cId);
			addClass(this.challenges[cId],'moving');
		}
		else {removeClass(this.challenges[this.cId],'current');}
		addClass(this.challenges[cId],'current');
		this.cId=cId;
	},
	animate: function(cId) {
		this.nxt = cId;
		var tgt = (this.animType == 'challenge') ? this.challenges[cId] : this.challenges[this.chId].overlays[cId];
		var current = parseInt(getStyle(tgt,'left'),10);
		if(this.direction=='next')
			this.step = (current < 920) ? this.step + 10 : this.step - 10;
		else
			this.step = (current > 60) ? this.step - 10 : this.step + 10;
		if(typeof(this.timeOut[cId])!='undefined') clearTimeout(this.timeOut[cId]);
		var n = (this.direction=='next') ? current - this.step : current + this.step;
		if(this.direction=='next') {
			if(n<=67) {
				tgt.style.left = 67 +'px';
				this.moving=-1;
				removeClass(tgt,'moving');
				return;
			}
		}
		else {
			if(n>=67) {
				tgt.style.left = 67 +'px';
				this.moving=-1;
				removeClass(tgt,'moving');
				return;
			}
		}
		tgt.style.left = n +'px';
		this.timeOut[cId] = setTimeout(function (that) {
			return function () {
				that.animate(that.nxt);
			}
		}(this), 10);
	},
	moveOut: function(cId) {
		this.prev = cId;
		var tgt = (this.animType == 'challenge') ? this.challenges[cId] : this.challenges[this.chId].overlays[cId];
		var current = parseInt(getStyle(tgt,'left'),10);
		if(typeof(this.timeOut[cId])!='undefined') clearTimeout(this.timeOut[cId]);
		var n = (this.direction=='next') ? current - this.step : current + this.step;
		if(this.direction=='next') {
			if(n<=-980) {
				removeClass(tgt,'moving');
				removeClass(tgt,'current');
				tgt.style.left = 67 +'px';
				return;
			}
		}
		else {
			if(n>=980) {
				removeClass(tgt,'moving');
				removeClass(tgt,'current');
				tgt.style.left = 67 +'px';
				return;
			}
		}
		tgt.style.left = n +'px';
		this.timeOut[cId] = setTimeout(function (that) {
			return function () {
				that.moveOut(that.prev);
			}
		}(this), 10);
	},
	shift: function(dir) {
		var n = (dir=='next') ? ( (this.cId+1 == this.max) ? 0 : this.cId+1 ) : ( (this.cId-1 < 0) ? this.max-1 : this.cId-1 );
		this.reveal(n,dir);
	},
	overlay: function(cId,challengeId,prtClass) {
		
		this.fadeOut();
		
		if(/type2/.test(this.challenges[challengeId].overlays[cId].className)) {
			addClass(this.highlight,'type2');
		}
		else {
			removeClass(this.highlight,'type2');
		}
		
		this.file = this.challenges[challengeId].overlays[cId].href + '&js=true';
		
		var req = GET_XMLHTTPRequest();
		if (req) {
			req.open("GET", this.file, true);
			req.setRequestHeader('User-Agent','XMLHTTP/1.0');
			req.onreadystatechange = function (that) {
		        return function (aEvt) {
					if (req.readyState != 4) return;
					if (req.status != 200 && req.status != 304) {
						that.handleError(req);
						return;
					}
		            if(req.readyState == 4){
						that.handleRequest(req,'simple');
					}
		        }
		    }(this);
			req.send(null);
		}
		else {
			this.highlightHTML.innerHTML = 'Unable to load content';
			this.fadeIn();
		}
		
		addClass(this.highlight,'on');
		
		
	},
	fadeIn: function() {
		new Fader(getById('highlightInner'),10,100,1,10);
		
	},
	fadeOut: function() {
		new Fader(getById('highlightInner'),100,10,3,10);
	},
	addOverlayNav: function(cId,challengeId) {
		var ova = this.challenges[challengeId].overlays;
		var nTxt = 'Next';
		var pTxt = 'Previous';
		this.ovId = cId;
		this.chId = challengeId;
		this.ovMax = this.challenges[challengeId].ovMax;
		this.ovaN = this.highlight.appendChild(createEl('button','ovNextBtn',nTxt));
		this.ovaN.id = 'ovaN';
		this.ovaN.onclick = function(that) {
			return function(){
				that.shiftOv('next');
			}
		}(this);
		this.ovaP = this.highlight.appendChild(createEl('button','ovPrevBtn',pTxt));
		this.ovaP.id = 'ovaP';
		this.ovaP.onclick = function(that) {
			return function(){
				that.shiftOv('prev');
			}
		}(this);
	},
	shiftOv: function(dir) {
		var n = (dir=='next') ? ( (this.ovId+1 == this.ovMax) ? 0 : this.ovId+1 ) : ( (this.ovId-1 < 0) ? this.ovMax-1 : this.ovId-1 );
		this.animType = 'overlay';
		this.ovId = n;
		
		if(dir=='next') {
			if(n==0) {
				if(this.challenges[(this.chId+1)])
					this.chId+=1;
				else if(this.challenges[0])
					this.chId=0;
			}
		}
		else {
			if(n==(this.ovMax-1)){
				if(this.challenges[(this.chId-1)])
					this.chId-=1;
				else if(this.challenges[(this.max-1)])
					this.chId=(this.max-1);
			}
		}
		
		this.overlay(n,this.chId);
	},
	removeOverlayNav: function() {
		if(!getById('highlight') || !getById('ovaP') || !getById('ovaN')) return;
		this.highlight.removeChild(this.ovaN);
		this.highlight.removeChild(this.ovaP);
	},
	close: function() {
		removeClass(this.highlight,'on');
		this.highlightHTML.innerHTML = '';
		this.removeOverlayNav();
		this.animType = 'challenge';
	},
	close2: function() {
		if(this.ovaN)
			removeClass(this.ovaN,'hidden');
		if(this.ovaP)
			removeClass(this.ovaP,'hidden');
		if(this.highlight2)
			removeClass(this.highlight2,'on');
		if(this.highlight2HTML)
			this.highlight2HTML.innerHTML = '';
	},
	handleRequest: function(req,type) {
		if(type=='simple') {
			this.highlightHTML.innerHTML = req.responseText;

			var stripe = getByClass('stripe',getById('highlightMedia'),'div');
			var iNav = getByClass('innerNav',getById('highlightInner'),'ul');
			if(stripe&&stripe[0]) {
				var aColl = getByTag(stripe[0],'a');
				var aMax = aColl.length/2;
				for(var i = 0; aColl[i]; ++i) {
					
					if(aColl[i].parentNode.nodeName=='LI') {
						var t = createEl('span','tooltip','<span>'+aColl[i].innerHTML+'</span>');
						aColl[i].tgt = aColl[i+aMax];
						aColl[i].parentNode.appendChild(t);
						aColl[i].tooltip = t;
						aColl[i].onmouseover = function(that) {
							return function() {
								that.tooltip(this,'on');
								that.videotip(this.tgt,'on');
								return false;
							}
						}(this);
						aColl[i].onmouseout = function(that) {
							return function() {
								that.tooltip(this,'off');
								that.videotip(this.tgt,'off');
								return false;
							}
						}(this);
					}
					else {
						aColl[i].tgt = aColl[i-aMax];
						aColl[i].onmouseover = function(that) {
							return function() {
								that.remoteTooltip(this.tgt,'on');
								return false;
							}
						}(this);
						aColl[i].onmouseout = function(that) {
							return function() {
								that.remoteTooltip(this.tgt,'off');
								return false;
							}
						}(this);
					}
					aColl[i].onclick = function(that) {
						return function() {
							that.doubleOverlay(this);
							return false;
						}
					}(this);
				}
			}
			this.fadeIn();
		}
		else {
			this.highlight2HTML.innerHTML = req.responseText;
			var iNav = getByClass('innerNav',getById('highlight2Inner'),'ul');
			if(iNav&&iNav[0]) {
				var aColl = getByTag(iNav[0],'a');
				for(var i = 0; aColl[i]; ++i) {
					
					var t = createEl('span','tooltip','<span>'+aColl[i].innerHTML+'</span>');
					aColl[i].parentNode.appendChild(t);
					aColl[i].tooltip = t;
					
					aColl[i].onclick = function(that) {
						return function() {
							that.doubleOverlay(this);
							return false;
						}
					}(this);
					aColl[i].onmouseover = function(that) {
						return function() {
							that.tooltip(this,'on');
							return false;
						}
					}(this);
					aColl[i].onmouseout = function(that) {
						return function() {
							that.tooltip(this,'off');
							return false;
						}
					}(this);
				}
			}

			addClass(this.highlight2,'on');
			this.fadeIn();
		}
	},
	tooltip: function(a,typ) {
		if(typ=='on')
			addClass(a.tooltip,'on');
		else
			removeClass(a.tooltip,'on');
	},
	remoteTooltip: function(a,typ) {
		if(typ=='on') {
			addClass(a,'on');
			addClass(a.tooltip,'on');
		}
		else {
			removeClass(a,'on');
			removeClass(a.tooltip,'on');
		}
	},
	videotip: function(a,typ) {
		if(typ=='on')
			addClass(a,'on');
		else
			removeClass(a,'on');
	},
	handleError: function(req) {
		this.highlightHTML.innerHTML = '<strong>Data error :</strong> HTTP error' + req.status + '';
		this.fadeIn();
	},
	doubleOverlay: function(a) {

		var h = a.href;
		if(!h||(h=='')) return;

		this.doubledUp = true;
		addClass(this.ovaN,'hidden');
		addClass(this.ovaP,'hidden');
		
		this.file = h + '&js=true';
		
		var req = GET_XMLHTTPRequest();
		if (req) {
			req.open("GET", this.file, true);
			req.setRequestHeader('User-Agent','XMLHTTP/1.0');
			req.onreadystatechange = function (that) {
		        return function (aEvt) {
					if (req.readyState != 4) return;
					if (req.status != 200 && req.status != 304) {
						that.handleError(req);
						return;
					}
		            if(req.readyState == 4){
						that.handleRequest(req,'double');
					}
		        }
		    }(this);
			req.send(null);
		}
		else {
			this.highlightHTML.innerHTML = 'Unable to load content';
			this.fadeIn();
		}
	}
}

/* 
----- */
function setOpacity(opacity,obj) {
	obj.style.opacity = (opacity / 100);
	obj.style.MozOpacity = (opacity / 100);
	obj.style.KhtmlOpacity = (opacity / 100);
	obj.style.filter = "alpha(opacity=" + opacity + ")";
}

function Fader(obj,start,end,step,speed) {
	this.obj = obj;
	this.speed = speed;
	this.opacity = start;
	this.end = end;
	this.direction = (start>end) ? 'out' : 'in';
	this.step = step;
	this.fade();
}

Fader.prototype = {
	fade: function() {
		if(typeof(this.timeOut)!='undefined') clearTimeout(this.timeOut);
		var o = (this.direction == 'in') ? this.opacity + this.step : this.opacity - this.step;
		if ( ((this.direction == 'in') && (o>this.end)) || ((this.direction == 'out') && (o<this.end)) ){
			o = this.end;
			setOpacity(o,this.obj);
		}
		else {
			setOpacity(o,this.obj);
			this.opacity = o;
			this.step += 1;
			this.timeOut = setTimeout(function (that) {
				return function () {
					that.fade();
				}
			}(this), this.speed);
		}
	}
}


/* Rotater
----- */
function setRotator(prtClass,itmsClass) {
	var prtColl = getByClass(prtClass,'','*');
	if(!prtColl||!prtColl[0]) return;
	for(var i = 0; prtColl[i]; ++i) {
		var itms = getByClass(itmsClass,prtColl[i],'*');
		if(itms&&itms[1])
			new Rotator(prtColl[i],itms)
	}
}

function Rotator(prt,itms) {
	this.container = prt;
	this.speed = 5000;
	this.rType = 'rotating';
	if(/speed/.test(prt.className)){
		var reg = new RegExp("speed-([0-9]*)", "gi");
		var s = prt.className.match(reg)[0];
		s = s.split('speed-')[1];
		this.speed = parseInt(s,10);
	}
	if(/rotatorType/.test(prt.className)){
		var reg = new RegExp("rotatorType-([a-zA-Z]*)", "gi");
		var r = prt.className.match(reg)[0];
		r = r.split('rotatorType-')[1];
		this.rType = r;
	}
	this.cId = 0;
	for(var i = 0; itms[i]; ++i) {
		if(/current/.test(itms[i].className)) this.cId = i;
		itms[i].cId = i;
	}
	this.itms = itms;
	this.max = itms.length;
	
	this.setNav(this.rType);
	
	if(this.rType=='rotating')
		this.rotate();
}

Rotator.prototype = {
	setNav: function(t) {
		if(t=='numbered') {
			var rNav = this.container.appendChild(createEl('div','rnNav',''));
			for(var i = 0; this.itms[i]; ++i) {
				var c = (i == this.cId) ? 'rnBtn current' : 'rnBtn';
				var btn = rNav.appendChild(createEl('button',c,parseInt(i+1,10)));
				btn.cId = i;
				btn.onclick = function(that) {
					return function() {
						that.change(this.cId);
					}
				}(this);
			}
			this.btns = getByTag(rNav,'button');
		}
	},
	change: function(cId) {
		removeClass(this.itms[this.cId],'current');
		addClass(this.itms[cId],'current');
		if(this.btns) {
			removeClass(this.btns[this.cId],'current');
			addClass(this.btns[cId],'current');
		}
		this.cId = cId;
	},
	shift: function(dir) {
		var n = (dir=='next') ? ( (this.cId+1 == this.max) ? 0 : this.cId+1 ) : ( (this.cId-1 < 0) ? this.max-1 : this.cId-1 );
		removeClass(this.itms[this.cId],'current');
		addClass(this.itms[n],'current');
		this.cId = n;
	},
	rotate: function() {
		var speed = this.speed;
		this.rTimeout = setTimeout(function (that) {
			return function () {
				that.shift('next');
				that.rotate();
			}
		}(this), speed);
	}
}

/* 
----- */
function setAddendas() {
	var adColl = getByClass('addended',getById('page'),'div');
	if(!adColl || adColl.length==0) return;
	for(var i=0; adColl[i]; ++i) {
		var p = getByClass('more',adColl[i],'p');
		var d = getByClass('addenda',adColl[i],'div');
		if(p && d && p[0] && d[0]) {
			var a = getByTag(p[0],'a');
			if(a && a[0]){
				var s = getByClass('plus',a[0],'strong');
				if(!s || !s[0]) s = 'unset';
					new Addenda(a[0],d[0],s[0]);
			}
			
		}
	}
}

function Addenda(a,t,d) {
	this.call = a;
	this.tgt = t;
	this.deco = d;
	this.state = 'x';
	this.call.onclick = function (that) {
		return function () {
			if(that.state=='x') {
				addClass(that.tgt,'deployed');
				that.deco.innerHTML = '-';
				that.state='o';
			}
			else {
				removeClass(that.tgt,'deployed');
				that.deco.innerHTML = '+';
				that.state='x';
			}
			return false;
		}
	}(this);
}

/* LangSwitch
----- */
function setRHlang() {
	var c = getByClass('langSwitch',getById('content'),'ul');
	if(c&&c[0]) {
		for(var i = 0; c[i]; ++i) {
			var aC = getByTag(c[i],'a');
			
			var t = c[i].parentNode.nextSibling;
			while(t.nodeType!=1){
				t=t.nextSibling;
			}
			if(/IFRAME/.test(t.nodeName)) {
				new RHlang(t,aC);
			}
		}
	}
}

function RHlang(ifr,aC) {
	this.tgt = ifr;
	this.aColl = aC;
	for(var i = 0; aC[i]; ++i) {

		aC[i].onclick= function(that) {
			return function() {
				that.toggle(this);
				return false;
			}
		}(this);
	}
}

RHlang.prototype={
	toggle: function(a) {
		if(a.href==this.tgt.src)
			return;
		this.tgt.src=a.href;
		for(var i = 0; this.aColl[i]; ++i) {
			removeClass(this.aColl[i],'on');
			addClass(a,'on');
		}
	}
}

function repositionIframes() {
	var rhFrm = getById('fltContainer');
	if(rhFrm) {
		rhFrm.onload = function() {
			window.scroll(0,0);
		}
	}
}

/* 
----- */
function setFolds() {
	var c = getByClass('call',getById('page'),'*');
	if(c&&c[0]) {
		for(var i = 0; c[i]; ++i) {
			
			var t = (c[i].parentNode.nodeName == 'STRONG') ? c[i].parentNode.nextSibling : c[i].nextSibling;
			while(t.nodeType!=1){
				t=t.nextSibling;
			}
			if(t&&/tgt/.test(t.className)) {
				new Fold(c[i],t);
			}
		}
	}
}

function Fold(c,t) {
	this.call = c;
	if(c.nodeName=="SPAN") {
		if(!getByTag(c,'a')[0]) {
			var b = createEl('button','call',c.innerHTML);
			b.setAttribute('type','button')
			c.parentNode.replaceChild(b,c);
			this.call=b;
		}
	}
	this.tgt = t;
	this.call.onclick = function(that) {
		return function() {
			that.toggle();
			return false;
		}
	}(this);
}

Fold.prototype = {
	toggle: function(){
		if(/deployed/.test(this.tgt.className))
			removeClass(this.tgt,'deployed');
		else
			addClass(this.tgt,'deployed');
	}
}

/* Video
-------- */
function VeoliaVideoCallback(action, video) {
	var req = GET_XMLHTTPRequest();
	if (req) {
		var url="/api/webtv/log?action="+action+"&video="+video;

		req.open("GET", url, true);
		req.setRequestHeader('User-Agent','XMLHTTP/1.0');
		req.send(null);
	}
}

/* 
----- */
function setCode() {
	var c = getByClass('code',getById('page'),'*');
	for(var i =0; c[i]; ++i) {
		c[i].onclick = function() {
			this.select();
		}
	}
}



/* 
----- */
function setTooltips() {
	var tips = getByClass('tip',getById('content'),'a');
	if(tips&&tips[0]) new Tips(tips);
}

function Tips(tips) {
	for (var i = 0; tips[i]; ++i) {
		if(tips[i].lastChild.nodeType==8){
			var tip = tips[i].lastChild.nodeValue;
			tips[i].tip = '<span class="tooltip">'+tip+'</span>';
			tips[i].cId = i;
			tips[i].innerHTML = tips[i].innerHTML + '<span class="tipC"></span>';

			if( findPosition(tips[i])[1] > (getById('page').offsetHeight-250))
				addClass(tips[i],'reversed');
			tips[i].onmouseover = function(that) {
				return function() {
					that.reveal(this);
				}
			}(this);
			tips[i].onmouseout = function(that) {
				return function() {
					that.remove(this);
				}
			}(this);
		}
	}
	this.tips = tips;
	this.state = 'idle';
	this.cId = -1;
}

Tips.prototype = {
	reveal: function(a) {
		var tt = getByClass('tooltip',a,'span');
		if(tt&&tt[0]) return;
		var tI = getByClass('tipC',a,'span');
		if(tI&&tI[0]) {
			tI[0].innerHTML = a.tip;
			tI[0].style.zIndex = 9999;
		}
	},
	remove: function(a) {
		var tI = getByClass('tipC',a,'span');
		if(tI&&tI[0]) {
			tI[0].innerHTML = '';
		}
	}
}

/* Taleo
-------------------- */
var ftlDocumentLoaded=function(d) {
	window.scroll(0,0);
}

/* Init
-------------------- */
var init = function() {
	if (arguments.callee.done) return;
	arguments.callee.done = true;
	/*@if (@_win32)
		if(ie_rv < 6) return;
	/*@end @*/
	if(!document.getElementsByTagName) return;
	
	addClass(document.getElementsByTagName('body')[0],'scripted');

	setLinks(['a','area'],'');
	setMiniMenus();
	setChallenges();
	setAddendas();
	setFolds();
	setRotator('rotator','item');
	setDiaporama();
	initSwfObjects('');
	setCode();
	setRHlang();
	//repositionIframes();
	//setTooltips();
	/*@if (@_win32)
		if(ie_rv < 7) fixLowIE();
		if(ie_rv < 8) fixIE();
	/*@end @*/
}

if (document.addEventListener) {
	document.addEventListener( "DOMContentLoaded", function(){
		document.removeEventListener("DOMContentLoaded",arguments.callee,false);
		init();
	}, false );
} else if (document.attachEvent) {
	document.attachEvent("onreadystatechange", function(){
		if (document.readyState === "complete") {
			document.detachEvent("onreadystatechange",arguments.callee);
			init();
		}
	});
	if (document.documentElement.doScroll && window == window.top) (function(){
		if (arguments.callee.done) return;
		try {
			document.documentElement.doScroll("left");
		} catch(error) {
			setTimeout(arguments.callee, 0);
			return;
		}
		init();
	})();
}
window.onload = init;

/* IE stuff
----- */

/*@cc_on @*/
/*@if (@_win32)
document.write("<script id=__ie6_onload defer src=javascript:void(0)><\/script>");
var script = document.getElementById("__ie6_onload");
script.onreadystatechange = function() {
	if (this.readyState == "complete") {
		var b = document.getElementsByTagName('body')[0];
		b.className = b.className + ' scripted';
	}
};
/*@end @*/



/*	SWFObject v2.2 <http://code.google.com/p/swfobject/> 
	is released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/
var swfobject=function(){var D="undefined",r="object",S="Shockwave Flash",W="ShockwaveFlash.ShockwaveFlash",q="application/x-shockwave-flash",R="SWFObjectExprInst",x="onreadystatechange",O=window,j=document,t=navigator,T=false,U=[h],o=[],N=[],I=[],l,Q,E,B,J=false,a=false,n,G,m=true,M=function(){var aa=typeof j.getElementById!=D&&typeof j.getElementsByTagName!=D&&typeof j.createElement!=D,ah=t.userAgent.toLowerCase(),Y=t.platform.toLowerCase(),ae=Y?/win/.test(Y):/win/.test(ah),ac=Y?/mac/.test(Y):/mac/.test(ah),af=/webkit/.test(ah)?parseFloat(ah.replace(/^.*webkit\/(\d+(\.\d+)?).*$/,"$1")):false,X=!+"\v1",ag=[0,0,0],ab=null;if(typeof t.plugins!=D&&typeof t.plugins[S]==r){ab=t.plugins[S].description;if(ab&&!(typeof t.mimeTypes!=D&&t.mimeTypes[q]&&!t.mimeTypes[q].enabledPlugin)){T=true;X=false;ab=ab.replace(/^.*\s+(\S+\s+\S+$)/,"$1");ag[0]=parseInt(ab.replace(/^(.*)\..*$/,"$1"),10);ag[1]=parseInt(ab.replace(/^.*\.(.*)\s.*$/,"$1"),10);ag[2]=/[a-zA-Z]/.test(ab)?parseInt(ab.replace(/^.*[a-zA-Z]+(.*)$/,"$1"),10):0}}else{if(typeof O.ActiveXObject!=D){try{var ad=new ActiveXObject(W);if(ad){ab=ad.GetVariable("$version");if(ab){X=true;ab=ab.split(" ")[1].split(",");ag=[parseInt(ab[0],10),parseInt(ab[1],10),parseInt(ab[2],10)]}}}catch(Z){}}}return{w3:aa,pv:ag,wk:af,ie:X,win:ae,mac:ac}}(),k=function(){if(!M.w3){return}if((typeof j.readyState!=D&&j.readyState=="complete")||(typeof j.readyState==D&&(j.getElementsByTagName("body")[0]||j.body))){f()}if(!J){if(typeof j.addEventListener!=D){j.addEventListener("DOMContentLoaded",f,false)}if(M.ie&&M.win){j.attachEvent(x,function(){if(j.readyState=="complete"){j.detachEvent(x,arguments.callee);f()}});if(O==top){(function(){if(J){return}try{j.documentElement.doScroll("left")}catch(X){setTimeout(arguments.callee,0);return}f()})()}}if(M.wk){(function(){if(J){return}if(!/loaded|complete/.test(j.readyState)){setTimeout(arguments.callee,0);return}f()})()}s(f)}}();function f(){if(J){return}try{var Z=j.getElementsByTagName("body")[0].appendChild(C("span"));Z.parentNode.removeChild(Z)}catch(aa){return}J=true;var X=U.length;for(var Y=0;Y<X;Y++){U[Y]()}}function K(X){if(J){X()}else{U[U.length]=X}}function s(Y){if(typeof O.addEventListener!=D){O.addEventListener("load",Y,false)}else{if(typeof j.addEventListener!=D){j.addEventListener("load",Y,false)}else{if(typeof O.attachEvent!=D){i(O,"onload",Y)}else{if(typeof O.onload=="function"){var X=O.onload;O.onload=function(){X();Y()}}else{O.onload=Y}}}}}function h(){if(T){V()}else{H()}}function V(){var X=j.getElementsByTagName("body")[0];var aa=C(r);aa.setAttribute("type",q);var Z=X.appendChild(aa);if(Z){var Y=0;(function(){if(typeof Z.GetVariable!=D){var ab=Z.GetVariable("$version");if(ab){ab=ab.split(" ")[1].split(",");M.pv=[parseInt(ab[0],10),parseInt(ab[1],10),parseInt(ab[2],10)]}}else{if(Y<10){Y++;setTimeout(arguments.callee,10);return}}X.removeChild(aa);Z=null;H()})()}else{H()}}function H(){var ag=o.length;if(ag>0){for(var af=0;af<ag;af++){var Y=o[af].id;var ab=o[af].callbackFn;var aa={success:false,id:Y};if(M.pv[0]>0){var ae=c(Y);if(ae){if(F(o[af].swfVersion)&&!(M.wk&&M.wk<312)){w(Y,true);if(ab){aa.success=true;aa.ref=z(Y);ab(aa)}}else{if(o[af].expressInstall&&A()){var ai={};ai.data=o[af].expressInstall;ai.width=ae.getAttribute("width")||"0";ai.height=ae.getAttribute("height")||"0";if(ae.getAttribute("class")){ai.styleclass=ae.getAttribute("class")}if(ae.getAttribute("align")){ai.align=ae.getAttribute("align")}var ah={};var X=ae.getElementsByTagName("param");var ac=X.length;for(var ad=0;ad<ac;ad++){if(X[ad].getAttribute("name").toLowerCase()!="movie"){ah[X[ad].getAttribute("name")]=X[ad].getAttribute("value")}}P(ai,ah,Y,ab)}else{p(ae);if(ab){ab(aa)}}}}}else{w(Y,true);if(ab){var Z=z(Y);if(Z&&typeof Z.SetVariable!=D){aa.success=true;aa.ref=Z}ab(aa)}}}}}function z(aa){var X=null;var Y=c(aa);if(Y&&Y.nodeName=="OBJECT"){if(typeof Y.SetVariable!=D){X=Y}else{var Z=Y.getElementsByTagName(r)[0];if(Z){X=Z}}}return X}function A(){return !a&&F("6.0.65")&&(M.win||M.mac)&&!(M.wk&&M.wk<312)}function P(aa,ab,X,Z){a=true;E=Z||null;B={success:false,id:X};var ae=c(X);if(ae){if(ae.nodeName=="OBJECT"){l=g(ae);Q=null}else{l=ae;Q=X}aa.id=R;if(typeof aa.width==D||(!/%$/.test(aa.width)&&parseInt(aa.width,10)<310)){aa.width="310"}if(typeof aa.height==D||(!/%$/.test(aa.height)&&parseInt(aa.height,10)<137)){aa.height="137"}j.title=j.title.slice(0,47)+" - Flash Player Installation";var ad=M.ie&&M.win?"ActiveX":"PlugIn",ac="MMredirectURL="+O.location.toString().replace(/&/g,"%26")+"&MMplayerType="+ad+"&MMdoctitle="+j.title;if(typeof ab.flashvars!=D){ab.flashvars+="&"+ac}else{ab.flashvars=ac}if(M.ie&&M.win&&ae.readyState!=4){var Y=C("div");X+="SWFObjectNew";Y.setAttribute("id",X);ae.parentNode.insertBefore(Y,ae);ae.style.display="none";(function(){if(ae.readyState==4){ae.parentNode.removeChild(ae)}else{setTimeout(arguments.callee,10)}})()}u(aa,ab,X)}}function p(Y){if(M.ie&&M.win&&Y.readyState!=4){var X=C("div");Y.parentNode.insertBefore(X,Y);X.parentNode.replaceChild(g(Y),X);Y.style.display="none";(function(){if(Y.readyState==4){Y.parentNode.removeChild(Y)}else{setTimeout(arguments.callee,10)}})()}else{Y.parentNode.replaceChild(g(Y),Y)}}function g(ab){var aa=C("div");if(M.win&&M.ie){aa.innerHTML=ab.innerHTML}else{var Y=ab.getElementsByTagName(r)[0];if(Y){var ad=Y.childNodes;if(ad){var X=ad.length;for(var Z=0;Z<X;Z++){if(!(ad[Z].nodeType==1&&ad[Z].nodeName=="PARAM")&&!(ad[Z].nodeType==8)){aa.appendChild(ad[Z].cloneNode(true))}}}}}return aa}function u(ai,ag,Y){var X,aa=c(Y);if(M.wk&&M.wk<312){return X}if(aa){if(typeof ai.id==D){ai.id=Y}if(M.ie&&M.win){var ah="";for(var ae in ai){if(ai[ae]!=Object.prototype[ae]){if(ae.toLowerCase()=="data"){ag.movie=ai[ae]}else{if(ae.toLowerCase()=="styleclass"){ah+=' class="'+ai[ae]+'"'}else{if(ae.toLowerCase()!="classid"){ah+=" "+ae+'="'+ai[ae]+'"'}}}}}var af="";for(var ad in ag){if(ag[ad]!=Object.prototype[ad]){af+='<param name="'+ad+'" value="'+ag[ad]+'" />'}}aa.outerHTML='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"'+ah+">"+af+"</object>";N[N.length]=ai.id;X=c(ai.id)}else{var Z=C(r);Z.setAttribute("type",q);for(var ac in ai){if(ai[ac]!=Object.prototype[ac]){if(ac.toLowerCase()=="styleclass"){Z.setAttribute("class",ai[ac])}else{if(ac.toLowerCase()!="classid"){Z.setAttribute(ac,ai[ac])}}}}for(var ab in ag){if(ag[ab]!=Object.prototype[ab]&&ab.toLowerCase()!="movie"){e(Z,ab,ag[ab])}}aa.parentNode.replaceChild(Z,aa);X=Z}}return X}function e(Z,X,Y){var aa=C("param");aa.setAttribute("name",X);aa.setAttribute("value",Y);Z.appendChild(aa)}function y(Y){var X=c(Y);if(X&&X.nodeName=="OBJECT"){if(M.ie&&M.win){X.style.display="none";(function(){if(X.readyState==4){b(Y)}else{setTimeout(arguments.callee,10)}})()}else{X.parentNode.removeChild(X)}}}function b(Z){var Y=c(Z);if(Y){for(var X in Y){if(typeof Y[X]=="function"){Y[X]=null}}Y.parentNode.removeChild(Y)}}function c(Z){var X=null;try{X=j.getElementById(Z)}catch(Y){}return X}function C(X){return j.createElement(X)}function i(Z,X,Y){Z.attachEvent(X,Y);I[I.length]=[Z,X,Y]}function F(Z){var Y=M.pv,X=Z.split(".");X[0]=parseInt(X[0],10);X[1]=parseInt(X[1],10)||0;X[2]=parseInt(X[2],10)||0;return(Y[0]>X[0]||(Y[0]==X[0]&&Y[1]>X[1])||(Y[0]==X[0]&&Y[1]==X[1]&&Y[2]>=X[2]))?true:false}function v(ac,Y,ad,ab){if(M.ie&&M.mac){return}var aa=j.getElementsByTagName("head")[0];if(!aa){return}var X=(ad&&typeof ad=="string")?ad:"screen";if(ab){n=null;G=null}if(!n||G!=X){var Z=C("style");Z.setAttribute("type","text/css");Z.setAttribute("media",X);n=aa.appendChild(Z);if(M.ie&&M.win&&typeof j.styleSheets!=D&&j.styleSheets.length>0){n=j.styleSheets[j.styleSheets.length-1]}G=X}if(M.ie&&M.win){if(n&&typeof n.addRule==r){n.addRule(ac,Y)}}else{if(n&&typeof j.createTextNode!=D){n.appendChild(j.createTextNode(ac+" {"+Y+"}"))}}}function w(Z,X){if(!m){return}var Y=X?"visible":"hidden";if(J&&c(Z)){c(Z).style.visibility=Y}else{v("#"+Z,"visibility:"+Y)}}function L(Y){var Z=/[\\\"<>\.;]/;var X=Z.exec(Y)!=null;return X&&typeof encodeURIComponent!=D?encodeURIComponent(Y):Y}var d=function(){if(M.ie&&M.win){window.attachEvent("onunload",function(){var ac=I.length;for(var ab=0;ab<ac;ab++){I[ab][0].detachEvent(I[ab][1],I[ab][2])}var Z=N.length;for(var aa=0;aa<Z;aa++){y(N[aa])}for(var Y in M){M[Y]=null}M=null;for(var X in swfobject){swfobject[X]=null}swfobject=null})}}();return{registerObject:function(ab,X,aa,Z){if(M.w3&&ab&&X){var Y={};Y.id=ab;Y.swfVersion=X;Y.expressInstall=aa;Y.callbackFn=Z;o[o.length]=Y;w(ab,false)}else{if(Z){Z({success:false,id:ab})}}},getObjectById:function(X){if(M.w3){return z(X)}},embedSWF:function(ab,ah,ae,ag,Y,aa,Z,ad,af,ac){var X={success:false,id:ah};if(M.w3&&!(M.wk&&M.wk<312)&&ab&&ah&&ae&&ag&&Y){w(ah,false);K(function(){ae+="";ag+="";var aj={};if(af&&typeof af===r){for(var al in af){aj[al]=af[al]}}aj.data=ab;aj.width=ae;aj.height=ag;var am={};if(ad&&typeof ad===r){for(var ak in ad){am[ak]=ad[ak]}}if(Z&&typeof Z===r){for(var ai in Z){if(typeof am.flashvars!=D){am.flashvars+="&"+ai+"="+Z[ai]}else{am.flashvars=ai+"="+Z[ai]}}}if(F(Y)){var an=u(aj,am,ah);if(aj.id==ah){w(ah,true)}X.success=true;X.ref=an}else{if(aa&&A()){aj.data=aa;P(aj,am,ah,ac);return}else{w(ah,true)}}if(ac){ac(X)}})}else{if(ac){ac(X)}}},switchOffAutoHideShow:function(){m=false},ua:M,getFlashPlayerVersion:function(){return{major:M.pv[0],minor:M.pv[1],release:M.pv[2]}},hasFlashPlayerVersion:F,createSWF:function(Z,Y,X){if(M.w3){return u(Z,Y,X)}else{return undefined}},showExpressInstall:function(Z,aa,X,Y){if(M.w3&&A()){P(Z,aa,X,Y)}},removeSWF:function(X){if(M.w3){y(X)}},createCSS:function(aa,Z,Y,X){if(M.w3){v(aa,Z,Y,X)}},addDomLoadEvent:K,addLoadEvent:s,getQueryParamValue:function(aa){var Z=j.location.search||j.location.hash;if(Z){if(/\?/.test(Z)){Z=Z.split("?")[1]}if(aa==null){return L(Z)}var Y=Z.split("&");for(var X=0;X<Y.length;X++){if(Y[X].substring(0,Y[X].indexOf("="))==aa){return L(Y[X].substring((Y[X].indexOf("=")+1)))}}}return""},expressInstallCallback:function(){if(a){var X=c(R);if(X&&l){X.parentNode.replaceChild(l,X);if(Q){w(Q,true);if(M.ie&&M.win){l.style.display="block"}}if(E){E(B)}}a=false}}}}();