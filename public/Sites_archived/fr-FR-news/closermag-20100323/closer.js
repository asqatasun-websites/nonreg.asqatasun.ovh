$(document).ready(function() {

	$("img").noContext();

   // Toutes pages - Tous les mini formulaires
   // Gestion du focus / blur
   var form_messages = {
      msg_email_short : 'Votre pseudo',
      msg_email : 'Saisissez votre E-mail',
      msg_email_2 : 'Renseignez votre E-mail',
      msg_bykeywords : 'Par Mots clés',
      msg_starname : 'Nom de la star'
   };
   $('input.input_text').focus(function() {
      var msg = extractClass($(this), 'msg_');
      if (msg != null) {
         if ($(this).val() == form_messages[msg]) {
            $(this).val('');
         }
      }
   });
   $('input.input_text').blur(function(){
      var msg = extractClass($(this), 'msg_');
      if (msg != null) {
         // Le champ supporte la gestion des labels fugitifs
         if ($(this).val() == '') {
            $(this).val(form_messages[msg]);
         }
      }
   });

   // Toutes pages - Menus
   // Gestion des menus
   $('.menu > li').mouseover(function(){
      $(this)
         .addClass('mnuselected')
         .children('ul').css({display:'block'});
   });
   $('.menu > li').mouseout(function(){
      if (! $(this).hasClass('locked') ) {
         $(this).removeClass('mnuselected');
      }
      $(this).children('ul').css({display:'none'});
   });

   // Toutes pages - Gestion des liens favoris / home page
   // Positionne la page en cours comme page d'accueil
   $('#links_main>.link:nth(2) a').click(function(){
      document.body.style.behavior='url(#default#homepage)';
      if (typeof(document.body.setHomePage) == 'function') {
         document.body.setHomePage(location.href);
      }
      else {
         alert('Votre navigateur ne peut pas déclarer la page en cours de visualisation comme page d\'accueil.\nUtilisez les menus et préférences de votre navigateur pour la déclarer.');
      }
   });
   // Ajoute la page en cours dans les favoris
   $('#links_main>.link:nth(3) a').click(function(){
      if (window.external && typeof(window.external.AddFavorite) != 'undefined') {
         var url = location.href; // Contourne la restriction de securite IE7
         window.external.AddFavorite(url, document.title);
      }
      else {
         alert('Votre navigateur ne peut pas ajouter automatiquement la page en cours de visualisation dans vos favoris.\nUtilisez les menus de votre navigateur, ou tapez CTRL+D (si vous utilisez un navigateur compatible Mozilla).');
      }
   });

   // Toutes pages - Formulaires
   // Gestion automatique des boutons de soumission de formulaires
   $('.button.submit a,.okbutton.submit a').click(function(){
      $(this).parents('form').get(0).submit();
   });


   // Page envoi d'une vidéo youtube, vérification de l'url saisie avant submit
   $("#contribvideos_form").submit(function() {
   	if ($("#contribvideos_form_url").val()!="")
   		return true;
   	else{
   		alert('URL incorrecte');
   		return false;
   	}
   });

});

// Fonction utilitaires
function extractClass(node, classPrefix, classIgnored) {
   var classes = node.attr('class').split(' ');
   var pfxLength = classPrefix.length;
   for (i = 0; i < classes.length; ++i) {
      if (classes[i].substr(0, pfxLength) == classPrefix) {
         if (classIgnored == undefined || classes[i] != classIgnored) {
            return classes[i];
         }
      }
   }
   return null;
}