/*
* name: homepage.ui.generic.js
* author: RAJF
* notes: global ui functions and config
*/

/*
* ## INTERNAL VARS ##
*/
var _debug;
var _iconLocation;
var _arrowLocation;
var _params = {};
var _attributes = {};
var _flashFileUrl;
var _cookiename;
var _cookieJar;

var _ie6;
var _extension = ".png";
/*
* ## END INTERNAL VARS ##
*/

/*
* ## CONFIG ##
*/
// Set to false for Live!

// Log to Firebug Console
_debug = false;
_base = "/_looks/alstomV2/"; // BT ADDED
_css = "/_looks/alstomV2/css/nHP/";
_imageLocation = _css+"images/"; // BT MODIFIED
_iconLocation = _imageLocation + "structure/icon/";
_flashLocation = _base+"flash/";// BT MODIFIED
_arrowLocation = _imageLocation + "structure/elements/";
// cookie name
_cookiename = "panelSettings"
/* swf object */
_params.scale = "noscale";
_params.salign = "tl";
_params.swliveconnect = "true";
_params.allowscriptaccess = "always";
/* And Also module */
var _storyArray = new Array; // BT MODIFIED
var _andAlsoNextLabel="next"; // BT ADDED
var _andAlsoPrevLabel="previous";// BT ADDED
/********* BEGIN BT ADDED *************/
_urlFLV = '/_looks/alstomV2/flash/360.flv';
_urlFLVPreview = '';
_urlXMLFile = '';
_backoffice = false;
_urlFlashFile = '';
_widthNHP = '771';
_heightNHP = '243';
var currStory = 0;
var _timer = null;
var _tempo = 4;
var _active = false;

var _thumbtext = 'Watch Video';
var _replaytext = 'Replay';
var _fulltext = '';
var _fulllink = '';


function bt_escape(text) {
	var res = text.replace("'","%27");
	res = res.replace("\"","%22");
	res = res.replace("&","%26");
	return res;
}

function bt_escapeBis(text) {
	var res = bt_escape(text);
	res = res.replace("<","%26lt;");
	res = res.replace(">","%26gt;");
	return res;
}

/********* END BT ADDED *************/

/*
* ## END CONFIG ##
*/

/*
* window onload function
*/
$(function(){
	_cookieJar = $.cookieJar(_cookiename);
	 /********** BEGIN  BT MODIFIED ************/
	if (_urlXMLFile!='' && _urlFlashFile!='') {
		swfobject.embedSWF(_urlFlashFile, "banner", _widthNHP, _heightNHP, "9.0.0", false, {xmlUrl: _urlXMLFile}, _params, {id:"home_banner"});
	}
	var flashvars  = {};
	flashvars.flvUrl = _urlFLV;
	if (_urlFLVPreview!='') {
		flashvars.thumbnail=_urlFLVPreview;
	}
	if (_thumbtext!='')  {
		flashvars.thumbtext=bt_escape(_thumbtext);
	}
	if (_replaytext!='') {
		flashvars.replaytext =bt_escapeBis(_replaytext);
		
	}
	if (_fulltext!='' && _fulllink!='') {
		flashvars.fulltext = bt_escapeBis(_fulltext);
		if (_fulllink.indexOf('http://')==0) {
			flashvars.fulllink = _fulllink;
		}
		else {
			flashvars.fulllink = 'http://'+_fulllink;
		}
	}	
	swfobject.embedSWF(_flashLocation + "player_timecode.swf", "video", "254", "190", "9.0.0", false, flashvars, _params, {id:"video_player"});
	/********** END BT MODIFIED ************/

	_ie6 = false /*@cc_on || @_jscript_version < 5.7 @*/;
	if (_ie6) _extension = ".gif";
	
	// add corners
	/********** BEGIN  BT MODIFIED ************/
	if (!_ie6 && !_backoffice)
	{
		$(".module").not(".whats_new").corner("bottom");
	}
	/********** END BT MODIFIED ************/
	
	// set shareprice row colours
	$(".shares tr:even").addClass("even");
	
	// set relative link functionality
	setLinkRel();
		
	// set identifiers
	$('.oc').each(function(i){
		$(this).addClass("module"+i)
	});
	
	// if oc not have open or close add open
	
	// set classes from cookie
	debug(_cookieJar);
	if(_cookieJar != "{}")
	{
		$.each(_cookieJar.toObject(), function(i, val) {
			debug(i + '##'+val);
			if (val == 'open') {
				$("." + i).removeClass("close");
				$("." + i).addClass("open");
			}
			else{
				$("." + i).removeClass("open");
				$("." + i).addClass("close");
			}
		});	
	}

	// set alert module to always open
	$('.alert').removeClass("close");
	$('.alert').addClass("open");
	
	// set visibility
	$(".oc h2").each(function(i){
		if($(this).parent().hasClass("close"))
		{
			$(this).append('<span><img alt="open" src="' +_iconLocation + 'plus' + _extension + '" /></span>');
			$(this).parent().find(".container").hide();
		}
		else{
			$(this).append('<span><img alt="close" src="' +_iconLocation + 'minus' + _extension + '" /></span>');
			$(this).parent().find(".container").show();
		}
	});
	
	// setup module click functions and icons
	setClicks();
	if (_storyArray.length>1){ // BT ADDED
		setSlideshow();
	}// BT ADDED
	
});

// log to firebug console
function debug(message)
{
	if(_debug) if (window.console) console.log(message);
}

/*
* open windows in external window
*/ 
function setLinkRel()
{
  $('a[rel="external"]').click( function() {
      window.open( $(this).attr('href') );
      return false;
  });
}

/*
* setup modules
*/ 
function setClicks()
{
	
	$(".oc h2 span").click(function(){
		var parent = $(this).parent().parent();

		if(parent.hasClass("close"))
		{
			parent.find(".container").show();
			parent.removeClass("close");
			parent.addClass("open");
			setOpenClose(this,"close");
		}
		else
		{
			parent.find(".container").hide();
			parent.removeClass("open");
			parent.addClass("close");
			setOpenClose(this,"open");
		}

		setCookie();
	});
}

function setOpenClose(element, state)
{
	debug(state + " it");
	if (state  == "close"){
		$(element).find("img").attr("src", _iconLocation + "minus" + _extension);
		$(element).find("img").attr("alt", "Close");
	}
	else{
		$(element).find("img").attr("src", _iconLocation + "plus" + _extension);
		$(element).find("img").attr("alt", "Open");
	}
	
}

/*
* set cookie
*/ 
function setCookie(value)
{
	//_cookieJar.remove();
	$('.oc').each(function(i){
		if($(this).hasClass("close"))
		{
			_cookieJar.set("module"+i, "close");
		}
		else
		{
			_cookieJar.set("module"+i, "open");
		}
	});
}

/******  BEGIN BT ADDED *************/

function bt_showPR(section) {
	/*
	if (section=='corporate') {
		$("#WN_corporate").css('display','block');
		$("#WN_power").css('display','none');
		$("#WN_transport").css('display','none');
	}
	else if (section=='power') {
		$("#WN_corporate").css('display','none');
		$("#WN_power").css('display','block');
		$("#WN_transport").css('display','none');
	}
	else if (section=='transport') {
		$("#WN_corporate").css('display','none');
		$("#WN_power").css('display','none');
		$("#WN_transport").css('display','block');
	}
	*/
}


function previousImage(){
	debug(currStory);
    if(currStory>0){
      currStory--
    } else {
      currStory=_storyArray.length-1
    }    
    updateSlideShowImage();    
}

function nextImage(){
	debug(currStory);
    if(currStory<_storyArray.length-1){
      currStory++
    } else {
      currStory=0
    }
    updateSlideShowImage() 
}

function clearTimer() {
	if (_active && _timer!=null) {		
		clearInterval(_timer);  
	}
}

function initTimer() {
	if (_active) {		
		_timer=setInterval("nextImage()", _tempo*1000);  
	}
}

/******  END BT ADDED *************/

/*
* build slideshow
*/
function setSlideshow()
{
   
  $(".slideshow div div").prepend("<img class='left-arrow' alt='"+_andAlsoPrevLabel+"' src='" + _arrowLocation + "left_arrow.gif' />")//BT MODIFIED
  $(".slideshow div div").append("<img class='right-arrow' alt='"+_andAlsoNextLabel+"' src='" + _arrowLocation + "right_arrow.gif' />")//BT MODIFIED
  
   if ($.browser.msie && $.browser.version < '7.0') {
	   $(".slideshow div:first").prepend("<div class='BT_calque'></div>");
   }
  
  /********** BEGIN BT MODIFIED ************/
  $(".left-arrow").click(previousImage);
  $(".right-arrow").click(nextImage);
  /********** END BT MODIFIED ************/
  
  /********** BEGIN BT ADDED ************/
  $(".slideshow").mouseenter(function() {
	  clearTimer();
  });
  
  $(".slideshow").mouseleave(function() {
	  initTimer();
  });
  
  updateSlideShowImage();
  initTimer();
  /******  END BT ADDED *************/
}

function updateSlideShowImage(){ 
	/********** BEGIN BT MODIFIED ************/
	  var max = 100;
	  var t = _storyArray[currStory].text
	  if (t.length>max) { 
		  t = t.substring(0,max-4);
		  t = t.substring(0,t.lastIndexOf(' '));
		  t+= ' ...';
	  }
	  $(".slideshow a").html(t)
	  
	  /********** END BT MODIFIED ************/
  $(".slideshow a").attr("href",_storyArray[currStory].link)
  $(".slideshow-image").attr("src",_storyArray[currStory].src);
  $(".slideshow-image").attr("alt",_storyArray[currStory].alt);
}

/********** BEGIN BT ADDED ************/
function writeFlash(flashURL, width, height, align)
{
	document.write('<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" ');
	if (width!=null && width!='') {
	   document.write(' width="'+width+'" ');
  }
  if (height!=null && height!='') {
	   document.write(' height="'+height+'" ');
  }
  if (align!=null && align!='') {
	   document.write(' align="'+align+'" ');
  }
	document.write('><param value="'+flashURL+'" name="movie">');
	document.write('<param value="high" name="quality">');
	document.write('<param value="transparent" name="wmode">');
	document.write('<embed type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" quality="high" wmode="transparent" src="'+flashURL+'" ');
	if (width!=null && width!='') {
	   document.write(' width="'+width+'" ');
  }
  if (height!=null && height!='') {
	   document.write(' height="'+height+'" ');
  }	
	document.write('></embed></object>');
}
/********** END BT MODIFIED ************/