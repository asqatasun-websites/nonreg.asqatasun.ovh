﻿// Patch pour le navigateur Safari et Chrome qui s'appuient sur WebKit (qui de base n'est pas reconnu par l'AJAX)
Sys.Browser.WebKit = {};
if( navigator.userAgent.indexOf('WebKit/') > -1 )  {
    Sys.Browser.agent = Sys.Browser.WebKit;
    Sys.Browser.version = parseFloat(navigator.userAgent.match(/WebKit\/(\d+(\.\d+)?)/)[1]);
    Sys.Browser.name = 'WebKit';
}
