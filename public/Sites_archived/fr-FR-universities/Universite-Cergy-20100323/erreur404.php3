<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" lang="fr">
<head>
<title>Site Internet de l&#8217;universit� de Cergy-Pontoise: Page indisponible</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="icon" type="image/png" href="http://www.u-cergy.fr/favicon.png" />
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
<style type="text/css">
.alerte {
color: #000;
font-size:0.8em;
font-family: Arial, Helvetica, sans-serif;
font-weight:700;
height:150px;
}
a {
text-decoration: none;
color:#9A3140;
}
.global {
position:absolute;
top: 50%;
left: 50%;
width: 500px;
height:150px;
margin-left: -250px;
margin-top: -75px;
border: 1px solid #eee;
background: #9A3140 url(http://www.u-cergy.fr/squelettes/images/ucp.jpg) no-repeat 0px 0px;
}
.box {
margin-left: 250px;background: #fff;
padding: 10px;
height:130px;
}
</style>
</head>
<body>
<div class="global">
<div class="box">
<div class="alerte">
La page demand�e n'est pas disponible sur le Site Internet de l&#8217;universit� de Cergy-Pontoise.<br /><br />
Vous pouvez revenir � la <a href="erreur404.php3">page pr�c�dente</a> ou vous rendre sur la <a href="http://www.u-cergy.fr/index.html">
page d'accueil</a>.
</div>
</div>
</div>
</body>
</html>
