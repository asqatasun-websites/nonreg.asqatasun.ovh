
function tab_statusarray(global_id)
{
	return "gl_"+global_id+"_f";
}

function tab_initialize(global_id)
{
	// This section is to test if the gl_+global_id+_f array exists,
	// if not it creates it as a singletone
	var glarrayname = tab_statusarray(global_id);
	try{
		var t = typeof(eval(glarrayname));
	}catch(e)
	{
		var i = 0;
		while (document.getElementById(global_id+"Tab_"+i)!=null)
		{
			i++;
		}
		eval(glarrayname+"= new Array("+i+")");
		for (var j=0;j<i;j++)
		{
			eval(glarrayname+"["+j+"] = 0");
		}
	}
}

function tab_reset_flags(global_id)
{
	for (var i=0;i<eval(tab_statusarray(global_id)+".length");i++)
	{
		if (eval(tab_statusarray(global_id)+"["+i+"]") !=1)
			eval(tab_statusarray(global_id)+"["+i+"] = 0");
	}
}

function tab_on(global_id,num)
{
	tab_initialize(global_id);
	if(eval(tab_statusarray(global_id)+"["+num+"]") == 1)
	{
		tab_reset_glags(global_id);
	}else{
		eval(tab_statusarray(global_id)+"["+num+"] = 2");
		for (var i=0;i<eval(tab_statusarray(global_id)+".length");i++)
		{
			if (i!=num)
			{
				document.getElementById(global_id + "Tab_" + i).setAttribute("class", "tabOff_" + i);
            document.getElementById(global_id + "Tab_" + i).setAttribute("className", "tabOff_" + i);
				document.getElementById(global_id + "Content_" + i).style.display = "none";
			}else{
				document.getElementById(global_id + "Tab_" + num).setAttribute("class", "tabOn_" + num);
				document.getElementById(global_id + "Tab_" + num).setAttribute("className", "tabOn_" + num);
				document.getElementById(global_id + "Content_" + num).style.display = "block";
			}
		}
	}
}

function tab_out(global_id,num)
{
	tab_reset_flags(global_id);
}

function tab_onload_random(global_id)
{
	tab_initialize(global_id);
	var num = Math.floor(Math.random() * eval(tab_statusarray(global_id)+".length"));
	tab_on(global_id,num);
}

/*
 * Service Block
 */
function ouvreOngletService(num_onglet)
{
   // par d�faut, c'est le 1er onglet qui est ouvert, on le referme
   document.getElementById("serviceMiniBlockContent_0").style.display = "none";
   document.getElementById("serviceMiniBlockTab_0").setAttribute("class", "tabOff_0");
   document.getElementById("serviceMiniBlockTab_0").setAttribute("className", "tabOff_0");
   
   // Et on ouvre l'onglet demand�
   document.getElementById("serviceMiniBlockContent_" + num_onglet).style.display = "block";
   document.getElementById("serviceMiniBlockTab_" + num_onglet).setAttribute("class", "tabOn_" + num_onglet);
   document.getElementById("serviceMiniBlockTab_" + num_onglet).setAttribute("className", "tabOn_" + num_onglet);
}