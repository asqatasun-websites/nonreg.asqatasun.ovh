var slide = 0;
var nb_slide = 0;
var duree = 5000;

function JSGetObjById(id) {
	if(document.getElementById)
		var obj = document.getElementById(id);
	else if(document.all)
		var obj = document.all[id];
	else if(document.layers)
		var obj = document.layers[id];
	return obj;
}

function JSDisplay(id) {
	var obj = JSGetObjById(id);
	obj.style.visibility="visible";
	obj.style.display="inline";
}

function JSHide(id) {
	var obj = JSGetObjById(id);
	obj.style.visibility="hidden";
	obj.style.display="none";	
}

function display(id) {
	var i;
	slide = id;
	for(i=0; i<nb_slide; i++) {
		if(i==id) {
			JSDisplay("slide_"+i);
		}
		else {
			JSHide("slide_"+i);
		}
	}
}

/*
function changeSlide() {
	if(slide+1==nb_slide) {
		slide = 0;
	}
	else {
		slide = slide+1;
	}
	display(slide, nb_slide);
	timer();
}

function timer() {
	var timeoutID = window.setTimeout("changeSlide()", duree);
}
*/

function init_profil(nb) {
	nb_slide = nb;
	display(slide, nb_slide);
	/*timer();*/
}