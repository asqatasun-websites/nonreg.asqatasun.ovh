 /****** BEGIN LICENSE BLOCK *****
 * Copyright (c) 2005-2006 Harmen Christophe and contributors. All rights reserved.
 *
 * This script is free software; you can redistribute it and/or
 *   modify under the terms of the Creative Commons - Attribution-ShareAlike 2.0
 * <http://creativecommons.org/licenses/by-sa/2.0/>
 * You are free:
 *     * to copy, distribute, display, and perform the work
 *     * to make derivative works
 *     * to make commercial use of the work
 * 
 * Under the following conditions:
 * _Attribution_. You must attribute the work in the manner specified by the
 *   author or licensor.
 * _Share Alike_. If you alter, transform, or build upon this work, you may
 *   distribute the resulting work only under a license identical to this one.
 *     * For any reuse or distribution, you must make clear to others 
 *      the license terms of this work.
 *     * Any of these conditions can be waived if you get permission from 
 *      the copyright holder.
 * 
 * Your fair use and other rights are in no way affected by the above.
 * 
 * This is a human-readable summary of the Legal Code (the full license). 
 * <http://creativecommons.org/licenses/by-sa/2.0/legalcode>
 ***** END LICENSE BLOCK ******/

function trim(s) {return s.replace(/(^\s+)|(\s+$)/g,"");}

function hasClassName(oNode,className) {
	return (oNode.nodeType==1)?((" "+oNode.className+" ").indexOf(" "+className+" ")!=-1):false;
}

function addClassName(oNode,className) {
	if ((oNode.nodeType==1) && !hasClassName(oNode,className))
		oNode.className = trim(oNode.className+" "+className);
}

function deleteClassName(oNode,className) {
	if (oNode.nodeType==1)
    oNode.className = trim((" "+oNode.className+" ").replace(" "+className+" "," "));
}

function isChildNodeOf(oNode,other) {
	if (oNode.compareDocumentPosition) {
		return (oNode.compareDocumentPosition(other)==10);
	} else if (other.contains) {
		return other.contains(oNode);
	}
	var bIsChildNodeOf = false;
	function _isChildNodeOf(oNode,other) {
		while (other) {
			if (other==oNode) {
				bIsChildNodeOf = true;
				return;
			} else _isChildNodeOf(oNode,other.firstChild);
			other = other.nextSibling;
		}
	}
	_isChildNodeOf(oNode,other.firstChild);
	return bIsChildNodeOf;
}

function addEventLst(EventTarget,type,listener,useCapture) {
	useCapture = typeof(useCapture)=="boolean"?useCapture:false;
	if (EventTarget.addEventListener) {
		EventTarget.addEventListener(type, listener, useCapture);
	} else if ((EventTarget==window) && document.addEventListener) {
		document.addEventListener(type, listener, useCapture);
	} else if (EventTarget.attachEvent) {
		EventTarget["e"+type+listener] = listener;
		EventTarget[type+listener] = function() {EventTarget["e"+type+listener](window.event);}
		EventTarget.attachEvent("on"+type, EventTarget[type+listener]);
	} else {
		EventTarget["on"+type] = listener;
	}
}

function removeEventLst(EventTarget,type,listener,useCapture) {
	useCapture = typeof(useCapture)=="boolean"?useCapture:false;
	if (EventTarget.removeEventListener) {
		EventTarget.removeEventListener(type,listener, useCapture);
	} else if ((EventTarget==window) && document.removeEventListener) {
		document.removeEventListener(type,listener, useCapture);
	} else if (EventTarget.detachEvent) {
		EventTarget.detachEvent("on"+type, EventTarget[type+listener]);
		EventTarget[type+listener]=null;
		EventTarget["e"+type+listener]=null;
	} else {
		EventTarget["on"+type]=null;
	}
}

/*
	domEl() function - painless DOM manipulation
	written by Pawel Knapik  //  pawel.saikko.com
*/

var domEl = function(e,c,a,p,x) {
if(e||c) {
	c=(typeof c=='string'||(typeof c=='object'&&!c.length))?[c]:c;	
	e=(!e&&c.length==1)?document.createTextNode(c[0]):e;	
	var n = (typeof e=='string')?document.createElement(e) : !(e&&e===c[0])?e.cloneNode(false):e.cloneNode(true);	
	if(e.nodeType!=3) {
		c[0]===e?c[0]='':'';
		for(var i=0,j=c.length;i<j;i++) typeof c[i]=='string'?n.appendChild(document.createTextNode(c[i])):n.appendChild(c[i].cloneNode(true));
		if(a) {for(var i=(a.length-1);i>=0;i--) a[i][0]=='class'?n.className=a[i][1]:n.setAttribute(a[i][0],a[i][1]);}
	}
}
	if(!p)return n;
	p=(typeof p=='object'&&!p.length)?[p]:p;
	for(var i=(p.length-1);i>=0;i--) {
		if(x){while(p[i].firstChild)p[i].removeChild(p[i].firstChild);
			if(!e&&!c&&p[i].parentNode)p[i].parentNode.removeChild(p[i]);}
		if(n) p[i].appendChild(n.cloneNode(true));
	}	
}

/*
Copyright (c) 2006 Stuart Colville
http://muffinresearch.co.uk/archives/2006/04/29/getelementsbyclassname-deluxe-edition/

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial 
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
/*  
function getElementsByClassName(strClass, strTag, objContElm) {
  strTag = strTag || "*";
  objContElm = objContElm || document;    
  var objColl = (strTag == '*' && document.all) ? document.all : objContElm.getElementsByTagName(strTag);
  var arr = new Array();                              
  var delim = strClass.indexOf('|') != -1  ? '|' : ' ';   
  var arrClass = strClass.split(delim);    
  for (i = 0, j = objColl.length; i < j; i++) {
    if( objColl[i].tagName == undefined ) continue;   
    var arrObjClass = objColl[i].className.split(' ');   
    if (delim == ' ' && arrClass.length > arrObjClass.length) continue;
    var c = 0;
    comparisonLoop:
    for (k = 0, l = arrObjClass.length; k < l; k++) {
      for (m = 0, n = arrClass.length; m < n; m++) {
        if (arrClass[m] == arrObjClass[k]) c++;
        if (( delim == '|' && c == 1) || (delim == ' ' && c == arrClass.length)) {
          arr.push(objColl[i]); 
          break comparisonLoop;
        }
      }
    }
  }
  return arr; 
}
*/


function getElementsByClassName(className, tag, elm){
	var testClass = new RegExp("(^|\\s|\\|)" + className.replace(/\-/g, "\\-") + "(\\s|$|\\|)");
	var tag = tag || "*";
	var elm = elm || document;
	var elements = (tag == "*" && elm.all)? elm.all : elm.getElementsByTagName(tag);
	var returnElements = [];
	var current;
	var length = elements.length;
	for(var i=0; i<length; i++){
		current = elements[i];
		if(testClass.test(current.className)){
			returnElements.push(current);
		}
	}
	return returnElements;
}

 
// To cover IE 5 Mac lack of the push method
if (! Array.prototype.push) {
    Array.prototype.push = function(value) {
      this[this.length] = value;                                                              
    }
}

var mainMenu_timerID = null;
var secondaryMenu_timerID = null;

function initMainMenu(menuId) 
{
    var nMenu = document.getElementById(menuId);
    var ulList=nMenu.getElementsByTagName('ul');
    var columnsWidth = new Array();
    
    for(var i=1,max=ulList.length;i<max;i++)
    {
        var item = ulList[i].parentNode;
        addClassName(item,"submenu");
        initColumnWidth(item, columnsWidth);
    }
    
    for(var j=1,max=ulList.length;j<max;j++)
    {
        var oNode = ulList[j].parentNode;
        if (hasClassName(oNode,"submenu"))
	    {   
            var nbChild,child;
	        child = oNode.childNodes;
	        nbChild = child.length;
         
            for(var k = 0; k < nbChild; k++)
	        {
		        var curChild = child[k];
	            if (curChild.nodeType == 1 && curChild.tagName && curChild.tagName.toLowerCase() == "ul")
                {
                    curChild.style.width = columnsWidth[j-1];
                    break;
	            }
	        } 
	    } 
    } 

    loadMainMenu(menuId); 
}

function initSecondMenu(menuId) {
    var nMenu = document.getElementById(menuId);
    var ulList=nMenu.getElementsByTagName('ul');
    for(var i=1,max=ulList.length;i<max;i++){
        addClassName(ulList[i].parentNode,"submenu2h");
    }
    loadSecondMenu(menuId);
}

function loadMainMenu(menuId) {
	var nMenu = document.getElementById(menuId);
   if (nMenu.addEventListener) {
		nMenu.addEventListener("mouseover",eventLstShowMainMenu,true);
		nMenu.addEventListener("focus",eventLstShowMainMenu,true);
		nMenu.addEventListener("DOMFocusIn",eventLstShowMainMenu,true);
		nMenu.addEventListener("mouseout",eventLstHideMainMenu,true);
		nMenu.addEventListener("blur",eventLstHideMainMenu,true);
		nMenu.addEventListener("DOMFocusOut",eventLstHideMainMenu,true);

	}
	 else {
		var nA;
		var lessubmenu = getElementsByClassName('submenu', 'li', nMenu);
		for (var i=0,max=lessubmenu.length; i<max; i++) {
			addEventLst(lessubmenu[i],"mouseover",eventLstShowMainMenu);
			addEventLst(lessubmenu[i],"mouseout",eventLstHideMainMenu);
			for (var j=0; nA = lessubmenu[i].getElementsByTagName("a")[j]; j++) {
				addEventLst(nA,"focus",eventLstShowMainMenu);
				addEventLst(nA,"blur",eventLstHideMainMenu);
			}
		}
	}    
    
	addClassName(nMenu,"withjavascript");
}

function loadSecondMenu(menuId) {
	var nMenu = document.getElementById(menuId);
   if (nMenu.addEventListener) {
		nMenu.addEventListener("mouseover",eventLstShowSecondMenu,true);
		nMenu.addEventListener("focus",eventLstShowSecondMenu,true);
		nMenu.addEventListener("DOMFocusIn",eventLstShowSecondMenu,true);
		nMenu.addEventListener("mouseout",eventLstHideSecondMenu,true);
		nMenu.addEventListener("blur",eventLstHideSecondMenu,true);
		nMenu.addEventListener("DOMFocusOut",eventLstHideSecondMenu,true);

	}
	 else {
		var nA;
		var lessubmenu = getElementsByClassName('submenu2h', 'li', nMenu);
		for (var i=0,max=lessubmenu.length; i<max; i++) {
			addEventLst(lessubmenu[i],"mouseover",eventLstShowSecondMenu);
			addEventLst(lessubmenu[i],"mouseout",eventLstHideSecondMenu);
			for (var j=0; nA = lessubmenu[i].getElementsByTagName("a")[j]; j++) {
				addEventLst(nA,"focus",eventLstShowSecondMenu);
				addEventLst(nA,"blur",eventLstHideSecondMenu);
			}
		}
	}    
    
	addClassName(nMenu,"withjavascript");
}

function eventLstShowMainMenu(evt) {
	var oNode;
	if (evt && evt.target) {
		oNode = evt.target;
	} else if (window.event) {
		oNode = window.event.srcElement;
	} else {
		oNode = this;
	}
	if (mainMenu_timerID!=null) {cacherMenus();}
	while (oNode.id!="menu") {
	
	    if (hasClassName(oNode,"submenu"))
		{   
			addClassName(oNode,"encourt");
			break;
		} 
		else 
		{
            addClassName(oNode,"focus");
        }
		oNode = oNode.parentNode;
	}

	// Pour MSIE ou il faut annuler le bouillonnement
	if (window.event &&
		(typeof(window.event.cancelBubble)=="boolean") )
	{
		window.event.cancelBubble = true;
	}
	return false;
}

function eventLstShowSecondMenu(evt) {
	var oNode;
	if (evt && evt.target) {
		oNode = evt.target;
	} else if (window.event) {
		oNode = window.event.srcElement;
	} else {
		oNode = this;
	}
	if (secondaryMenu_timerID!=null) {cacherMenus();}
	while (oNode.id!="menu2h") 
	{
		if (hasClassName(oNode,"submenu2h")) 
		{
			addClassName(oNode,"encourt2h");
		} 
		else 
		{
            addClassName(oNode,"focus2h");
        }
		oNode = oNode.parentNode;
	}

	// Pour MSIE ou il faut annuler le bouillonnement
	if (window.event &&
		(typeof(window.event.cancelBubble)=="boolean") )
	{
		window.event.cancelBubble = true;
	}
	return false;
}

function eventLstHideMainMenu(evt) {
	var oNode, nRelatedTarget;
	if (evt && evt.target) {
		oNode = evt.target;
		nRelatedTarget = evt.relatedTarget;
	} else if (window.event) {
		oNode = window.event.srcElement;
		nRelatedTarget = window.event.toElement;

	} else {
		oNode = this;
	}

	if (nRelatedTarget) {
		var nCacherChildsTheme;
		while (oNode.id!="menu") {
			if ( (hasClassName(oNode,"submenu")) && (isChildNodeOf(nRelatedTarget,oNode)) ) {
				nCacherChildsTheme = oNode;
				break;
			}
			oNode = oNode.parentNode;
		}

		if (nCacherChildsTheme) {
			cacherMenus(nCacherChildsTheme);
        } else {
			mainMenu_timerID = setInterval("cacherMenus()",400);
		}

	} else {
		cacherMenus();
	}

	// Pour MSIE ou il faut annuler le bouillonnement
	if (window.event &&
		(typeof(window.event.cancelBubble)=="boolean") )
	{
		window.event.cancelBubble = true;
	}
	return false;
}

function eventLstHideSecondMenu(evt) {
	var oNode, nRelatedTarget;
	if (evt && evt.target) {
		oNode = evt.target;
		nRelatedTarget = evt.relatedTarget;
	} else if (window.event) {
		oNode = window.event.srcElement;
		nRelatedTarget = window.event.toElement;

	} else {
		oNode = this;
	}

	if (nRelatedTarget) {
		var nCacherChildsTheme;
		while (oNode.id!="menu2h") {
			if ( (hasClassName(oNode,"submenu2h")) && (isChildNodeOf(nRelatedTarget,oNode)) ) {
				nCacherChildsTheme = oNode;
				break;
			}
			oNode = oNode.parentNode;
		}

		if (nCacherChildsTheme) {
			cacherMenus(nCacherChildsTheme);
        } else {
			secondaryMenu_timerID = setInterval("cacherMenus()",400);
		}

	} else {
		cacherMenus();
	}

	// Pour MSIE ou il faut annuler le bouillonnement
	if (window.event &&
		(typeof(window.event.cancelBubble)=="boolean") )
	{
		window.event.cancelBubble = true;
	}
	return false;
}

function cacherMenus(oNode) {
	
	var getElem = getElementsByClassName;
	var delClass = deleteClassName;
	if (isDefinedElement("menu2h")){
	    var nMenu = document.getElementById("menu2h");
	    
	    if (secondaryMenu_timerID!=null) {
		    clearInterval(secondaryMenu_timerID);
		    secondaryMenu_timerID = null;
	    }
	    
		oNode = oNode?oNode:document.getElementById("menu2h");
		var lessubmenu = getElem('submenu2h','li', nMenu);
		var lessubfocus = getElem('focus2h','*', nMenu);
		for (var i=0; lessubmenu[i]; i++) {
			delClass(lessubmenu[i],"encourt2h");
		}
		for (var j=0; lessubfocus[j]; j++) {
			delClass(lessubfocus[j],"focus2h");
		}
	}
	var nMenu = document.getElementById("menu");
	
	if (mainMenu_timerID!=null) {
		clearInterval(mainMenu_timerID);
		mainMenu_timerID = null;
	}
	
	oNode = oNode?oNode:document.getElementById("menu");
	var lessubmenu = getElem('submenu','li', nMenu);
    var lessubfocus = getElem('focus','*', nMenu);
	for (var i=0; lessubmenu[i]; i++) {
		delClass(lessubmenu[i],"encourt");
	}
    for (var j=0; lessubfocus[j]; j++) {
		delClass(lessubfocus[j],"focus");
	}
	
}

//Resize the columns of the menu if they are too small or too wide.
function initColumnWidth(oNode, columnsWidth) 
{    
    if (hasClassName(oNode,"submenu"))
	{   
        var nbChild,child;
        var subMenuSize = oNode.offsetWidth;
        
        if(subMenuSize > 200) 
        {
            if(BrowserDetect.browser == 'Explorer' && BrowserDetect.version == '6')
            {
                subMenuSize = 190;
            }
            else
            {
                subMenuSize = 200;
            }
        }
        else if(subMenuSize <= 170)
        {
            if(BrowserDetect.browser == 'Explorer' && BrowserDetect.version == '6')
            {
                subMenuSize = subMenuSize + 15;
            }
            else
            {
                subMenuSize = subMenuSize + 30;
            }
        }
        else
        {
            if(BrowserDetect.browser == 'Explorer' && BrowserDetect.version == '6') subMenuSize = subMenuSize - 15;
        }
            
        subMenuSize += "px";
	    child = oNode.childNodes;
	    nbChild = child.length;
	    for(var i = 0; i < nbChild; i++)
	    {
		    var curChild = child[i];
	        if (curChild.nodeType == 1 && curChild.tagName && curChild.tagName.toLowerCase() == "ul")
            {    
                columnsWidth[columnsWidth.length] = subMenuSize ;             
                break;
	        }
	    }
	}   
}