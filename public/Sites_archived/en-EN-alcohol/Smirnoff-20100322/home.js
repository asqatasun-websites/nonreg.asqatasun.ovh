(function($) {
    // start anonymous function so we can use jQuery as $
    var momentText = false;
    var $empty = function() {};
    var SD = Smirnoff.SecondaryDialogs;
    
    var overlay = {
        animate: function(styles, duration, callback) {
            overlay.el.animate(styles, duration, callback && typeof callback === 'function' ? callback: $empty);
        },

        el: false,
        loadErrors: function(data, showSecondaryError) {
            // Bridge; TODO - change all referneces to call the below function directly
            Smirnoff.SecondaryDialogs.loadErrors(overlay, data, showSecondaryError);
        },
        
        /**
         * Pull HTML via AJAX and insert it
         * @param {Object} panel
         */
        loadPanel: function(panel) {
            Smirnoff.util.log.debug('loadPanel()');
            
            $.get(panel.path,
            function(data, ts) {
                Smirnoff.util.log.debug('ajax success');
                panel.html = data;
                
                var f = function() {
                    Smirnoff.util.log.debug('inserting html...');
                    overlay.el.html(panel.html);
                    overlay.el.find('.story-text').html(momentText);
                    overlay.show(panel.setup);
                };
                
                /*
                 * Make sure user doesn't see the html removal
                 */
                if (overlay.el.hasClass('active')) {
                    overlay.hide(function(e) {
                        Smirnoff.util.log.debug('removing html...');
                        overlay.el.children().remove().end();
                        f();
                    });                
                }
                else {
                    f();
                }
            });
         
            return false;
        },
        setupEdit: function() {
            $('#edit-story').click(function(e) {
                return overlay.hide(function() {
                    $('.momentsText').focus();
                    overlay.el.removeClass('active');
                });
            });
        },
        /**
         * Attach submit event
         * 
         * @param {Object} url
         * @param {Object} error
         * @param {Object} success
         */
        setupForm: function(url, error, success) {
            Smirnoff.util.log.debug('setupForm()');
            var form = $('.storyForm form');
            form.submit(function(e) {
                Smirnoff.util.log.debug('setupForm().submit callback');
                overlay.el.addClass('loading');
                var values = form.serializeArray();
                $.ajax({
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('ajax', 'true');
                    },
                    data: values,
                    dataType: 'json',
                    error: error,
                    success: function(data) {
                        Smirnoff.util.log.debug('before setupForm ajax success callback');
                        success(data);
                        Smirnoff.util.log.debug('after setupForm ajax success callback');
                    },
                    type: 'POST',
                    url: url
                });
                return false;
            });
        },
        show: function(callback) {
            Smirnoff.SecondaryDialogs.show(overlay, storyRollover, callback);
        },
        hide: function(callback) {
            Smirnoff.SecondaryDialogs.hide(overlay, storyRollover, callback);
        },
        submitError: function(data) {
            // unexpected server error
            overlay.el.removeClass('loading');
            overlay.loadErrors(data, true);
        },
        submitSuccess: function(panel, data) {
            Smirnoff.util.log.debug('submitSuccess()');
            var D = Smirnoff.UI.Dialogs;
            if (D.hasErrors(data) && D.handleRedirect(data)) {return;}
            
            overlay.el.removeClass('loading');
            var method = 'validation' + (data.success ? 'Success': 'Errors');
            panel[method](data);
        }
    };
    Smirnoff.SecondaryDialogs.loginDialog = {
        error: overlay.submitError,
        html: false,
        loadPanel: function() {
            return overlay.loadPanel(SD.loginDialog);
        },
        path: '/dialogs/panels/storyLogin.aspx' + "?rd=" + Math.floor(Math.random() * 100001),
        setup: function() {
            cmCreatePageviewTag(cm_pageid + ": Login Form Loaded", cm_categoryid, cm_search);
            cmCreateConversionEventTag(cm_pageid + ": Login Form", 1);
            overlay.el.find('.register a').click(register.loadPanel);
            overlay.setupForm(LOGIN_URL + "&rd=" + Math.floor(Math.random() * 100001), SD.loginDialog.error, SD.loginDialog.success);
            overlay.setupEdit();
        },
        success: function(data) {
            overlay.submitSuccess(SD.loginDialog, data);
        },
        validationErrors: function(data) {
            overlay.loadErrors(data);
        },
        validationSuccess: function(data) {
            cmCreateConversionEventTag(cm_pageid + ": Login Form", 2);
            Smirnoff.Session.afterLogin(data);
            videoUpload.loadPanel();
        }
    };
    var register = {
        countryChange: function(e) {
           return Smirnoff.SecondaryDialogs.countryChange.call(this, e);
        },
        error: overlay.submitError,
        html: false,
        loadPanel: function() {
            return overlay.loadPanel(register);
        },
        path: '/dialogs/panels/storyRegister.aspx' + "?rd=" + Math.floor(Math.random() * 100001),
        setup: function() {
            Smirnoff.SecondaryDialogs.setupRegisterDialog(overlay, register, SD.loginDialog);
        },
        success: function(data) {
            overlay.submitSuccess(register, data);
        },
        validationErrors: function(data) {
            Smirnoff.util.log.debug('register.validationErrors()');
            overlay.loadErrors(data, true);
        },
        validationSuccess: function(data) {
            Smirnoff.util.log.debug('register.validationSuccess()');
            cmCreateConversionEventTag(cm_pageid + ": Registration Form", 2);
            
            Smirnoff.Session.afterLogin(data);
            videoUpload.loadPanel();
        }
    };
    // It's not really an upload, only adding a reference

    var share = {
        html: false,
        loadPanel: function() {
            return overlay.loadPanel(share);
        },
        path: '/dialogs/panels/storyShare.aspx',
        reset: function() {
            videoUpload.skipped = false;
            imageUpload.skipped = false;
            overlay.el.removeClass('skipped').removeClass('active');
            $(".momentsText").attr('value', $(".momentsText").attr("title"));
        },
        setup: function() {
            // add share this here
            $("#storyOverlay .shareThis").each(function(i) {
                $(this).click(function(e) {
                    shareThis.popup(e);
                    return false;
                });
            });
            $('#storyOverlay div.actions p a').click(function() {
                cmCreatePageElementTag('Download Link',$(this).attr('href'),'Bursary Submission','Bursary');
            });

            overlay.el.find('.close').add('p.info a')
				.click(function(e) {
					e.preventDefault();
					Smirnoff.SecondaryDialogs.hide(overlay, storyRollover, null);
					share.reset();
				});
        }
    };

    var storyRollover = {
        hide: function() {
//            if (!storyRollover.el) { return; }
//            // bound to the '#stories .story-bubble' which was rolled out of
//            var story = $(this),
//            el = storyRollover.el,
//            b = story.data('bottom');
//            storyRollover.el.stop().animate({
//                bottom: 0,
//                opacity: 0
//            },
//            250,
//            function() {
//                el.css('display', 'none');
//            });
        },
        el: false,
        load: function(callback) {
            if (!storyRollover.html) {
                $.get('/dialogs/storyRollover.aspx',
                function(data, ts) {
                    $('.wrap').append(data);
                    storyRollover.el = $('#storyRollover');
                    if (callback && typeof callback === 'function') {
                        callback();
                    }
                });
            }
        },
        rateSuccess: function(data, textStatus) {
            if (data.success) {
                var there = storyRollover.el.find('a.there span.count');
                there.html('(' + data.fives + ')');
                var notthere = storyRollover.el.find('a.notthere span.count');
                notthere.html('(' + data.ones + ')');
                storyRollover.el.addClass('counted');
            }
            if (data.message) {
                storyRollover.el.addClass('messaged').find('span.message').html(data.message);
            }
        },
        show: function() {
            // bound to the '#stories .story-bubble' which was rolled over
            if (!storyRollover.el) {
                return;
            }
            
            var story = $(this),
            el = storyRollover.el.removeClass('messaged').removeClass('counted');
            
            // Copy stuff from homepage story
            var id = story.find('input.momentid').attr('value');
            
            // Copy story text
            el.find('.story').html(story.find('.story-text').html()).end()
            
            // Copy ID
            el.find('.momentid').attr('value', id).end()
            
            // Copy username
            .find('.user a').html(story.find('.story-signature a').html()).attr('href', story.find('.story-signature a').attr('href'));
            
            // Copy image src 
            var src = story.find('input.imgsrc').attr('value');
            src = src === '' ? '/images/storyDefault.jpg': src;
            el.find('.storyimage img').attr('src', src).end().find('span.message').html('');
            
            // Attach events
            el.unbind('click').ratingControl();
            
            story.append(el);
            
            el.stop().css({
                bottom: 0,
                left: 60,
                opacity: 0,
                display: 'block'
            }).animate({
                opacity: 1
            }, function() {
                
            });
        },
        waiting: false
    };
    


    var imageUpload, videoUpload;

    /*
     * Make these accessible globally
     */
    $.extend(Smirnoff.SecondaryDialogs, {
        overlay: overlay,
        login: Smirnoff.SecondaryDialogs.loginDialog,
        register: register,
        share: share,
        storyRollover: storyRollover,
        imageUpload: imageUpload, 
        videoUpload: videoUpload
    });

    $(function() {
        /*
         * Container for animated login/register/story posting overlay
         */
        overlay.el = $('#storyOverlay');

        if (check_login()) { }

        var moments_default_text = $(".momentsText").attr("title");
        $(".momentsText").keyup(function(event) {
            var maxLength = 140;
            $("#char-count").text(maxLength - $(this).attr("value").length);
            if ((maxLength - $(this).attr("value").length) <= 0) {
                $("#char-count").addClass("neg-char-count");
            }
            else {
                $("#char-count").removeClass("neg-char-count");
            }
        }).focus(function() {
            if ($(this).attr("value") === moments_default_text) {
                $(this).attr("value", "");
            }
        }).blur(function() {
            if ($(this).attr("value") === '') {
                $(this).attr('value', moments_default_text);
            }
        }).mouseenter(function(e) {
            storyRollover.hide();
        }).attr('value', moments_default_text);

        /**
         * Injects a <form> tag
          */
        $('#momentsDiv').children().wrapAll('<form class="clear" name="momentForm" action="/dialogs/addMoment.aspx" id="momentForm"></form>');

        /*
         * Load and show the moment dialog onsubmit
         */
        $('#momentForm').bind('submit', function(e) {
            e.preventDefault();
            e.stopPropagation();
            momentText = $('.momentsText').attr('value').replace(/<script.*?<\/script>/g, '');
            
            var terms = $('#termsConditions');
            
			if (terms.length && !terms.is(':checked')) {
				$("#termsError").fadeIn("slow");				
				return false;
			} else {
				$("#termsError").fadeOut("slow");
			}
            
            if (momentText.length > 140 || momentText.length <= 0) {
                return false;
            }
            if (check_login()) {
                videoUpload.loadPanel();
            } else {
                SD.loginDialog.loadPanel();
            }
            
            textAndImage = {
                momentText: momentText,
                momentImage: '',
                momentVideo: '',
                momentVideoTitle: '',
                verificationString: ''
            };
            
			return false;
        });
        $('#moments button').focus(function() {
            $(this).addClass('focus');
        }).blur(function() {
            $(this).removeClass('focus');
        }).hover(function() { $(this).addClass('hover'); }, function() { $(this).removeClass('hover') });;

        storyRollover.load(function() {
            $('#stories .story-bubble').hover(storyRollover.show, storyRollover.hide);
        });


        imageUpload = {
            error: overlay.submitError,
            html: false,
            loadPanel: function() {
                return overlay.loadPanel(imageUpload);
            },
            path: '/dialogs/panels/storyAddImage.aspx',
            setup: function() {
                overlay.el.attr('style', overlay.el.attr('style').replace(/filter.*?;/ig, ''));
                function imageChanged(e) {
                    Smirnoff.util.log.debug('imageChanged()');
                    // Save the value of the file input, since IE removes it after upload (why??)
                    var path = $('#image-file-upload').attr('value');
                    overlay.el.addClass('fileUploading');
                    overlay.el.find('.uploadPreview').animate({
                        borderWidth: '1px',
                        height: '146px'
                    });
                    
                    $.ajaxFileUpload({
                        type: "POST",
                        url: "/dialogs/addMoment_Image.aspx" + "?rd=" + Math.floor(Math.random() * 100001),
                        secureuri: '/resources/blank.html',
                        fileElementId: 'image-file-upload',
                        dataType: 'json',
                        success: function(data, ts) {
                            Smirnoff.util.log.debug('upload success');
                            if (data.imageid) {
                                Smirnoff.util.log.debug('appending image');
                                // Save the image url
                                textAndImage.momentImage = data.imageid;
                                // Insert image thumbnail
                                overlay.el.find('.uploadPreview')
                                    .children('.preview').remove().end()
                                    .append('<img class="preview" src="' + data.imageid + '" />');
                                // Update fake file upload with file path
                                overlay.el.find('#fauxFileText').html(path);
                                // When the image loads...
                                overlay.el.find('.uploadPreview .preview').load(function() {
                                    overlay.el.removeClass('fileUploading');
                                    var img = $(this);

						            var D = Smirnoff.UI.Dialogs,
						                img = overlay.el.find('.preview'),
										newSize = Smirnoff.util.thumbDimensions(img.width(), img.height(), 133, 133);

						            img.css({
						                width: newSize.width,
						                height: newSize.height,
						                visibility: 'visible'
						            });
                                    
                                    img.css('top', Smirnoff.DOM.getVCenterTop(img) + 'px')

                                });
                                overlay.el.find('#image-file-upload').attr('value', '').bind('change', imageChanged);
                            } else {
                                overlay.loadErrors(data);
                            }
                        },
                        error: function() {}
                    });
                    
                }
                overlay.el.find('#image-file-upload').bind('change', imageChanged)
                .focus(function() {
                    $(this).parent().addClass('focus');
                }).blur(function() {
                    $(this).parent().removeClass('focus');
                })
                .hover(function() {
                    $(this).parent().addClass('hover');
                },
                function() {
                    $(this).parent().removeClass('hover')
                });
                overlay.el.find('form').submit(function(e) {
                    if (imageUpload.skipped) {
                        textAndImage.momentImage = '';
                    }
                    overlay.el.addClass('loading');
                    textAndImage.verificationString = $('#verificationString').attr('value');
                    $.ajax({
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('ajax', 'true');
                        },
                        data: textAndImage,
                        dataType: 'json',
                        error: imageUpload.error,
                        success: imageUpload.success,
                        type: 'POST',
                        url: '/dialogs/addMoment.aspx' + "?rd=" + Math.floor(Math.random() * 100001)
                    });
                    return false;
                });
                overlay.el.find('button.skip').click(function(e) {
                    imageUpload.skipped = true;
                    overlay.el.addClass('skipped');
                });
                overlay.setupEdit();
            },
            skipped: false,
            success: function(data) {
                overlay.submitSuccess(imageUpload, data);
            },
            validationErrors: function(data) {
                overlay.loadErrors(data);
            },
            validationSuccess: function(data) {
                var momentBody = textAndImage.momentText;
                var first = $('#stories .story-bubble').eq(0);
                var clone = first.clone().find('#storyRollover').remove().end(); // clone and remove #storyRollover
                clone.find('.story-text').html(momentBody).end()
                    .find('.story-signature a').attr('href','/templates/publicprofile.aspx?id=' + data.userid).html(data.username).end()
                    .find('.userid').attr('value',data.userid).end()
                    .find('.momentid').attr('value',data.contentid).end()
                    .hover(storyRollover.show, storyRollover.hide)
                    .css({height:0,overflow:'hidden'});
                first.before(clone); // insert cloned element
                clone.animate({height:first.height()},500,function() {
                    clone.removeAttr('style');
                    if ($.browser.msie) { clone.css('zoom','1'); }
                });
                // coremetrics tagging for finished moment with image
                share.loadPanel();
            }
        }; // imageUpload

        videoUpload = {
            error: overlay.submitError,
            html: false,
            loadPanel: function() {
                return overlay.loadPanel(videoUpload);
            },
            path: '/dialogs/panels/storyAddVideo.aspx',
            setup: function() {
                overlay.el.removeClass('fileUploading');
                overlay.el.find('form').submit(function(e) {
                    if (!videoUpload.skipped) {
                        textAndImage.momentVideo = $('#videoUrl').attr('value');
                        textAndImage.momentVideoTitle = $('#videoTitle').attr('value');
                    }
                    imageUpload.loadPanel();
                    return false;
                });
                overlay.el.find('button.skip').click(function(e) {
                    videoUpload.skipped = true;
                });
                overlay.setupEdit();
            },
            skipped: false,
            success: function(data) {
                overlay.submitSuccess(videoUpload, data);
            },
            validationErrors: function(data) {
                overlay.loadErrors(data);
            },
            validationSuccess: function(data) {
                // coremetrics tagging for finished video addition
                imageUpload.loadPanel();
            }
        }; // videoUpload
 
       
    });
})(jQuery);
