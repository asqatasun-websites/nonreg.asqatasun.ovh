/**
 * @author klaunay
 */
function initSondage(form_sondage,div_sondage,idsondage)
{
	var form_sondage;
	var i;
	var intHasVoted=LireCookie('sondage' + idsondage);
	if (intHasVoted == idsondage) {
		if(div_sondage)
		{
//		   	var objChart = new FusionCharts("/FusionCharts/FCF_Bar2D.swf", "What", "200", "200");
//			objChart.setDataURL('/includes/ajax/getXmlsondage.asp?idsondage='+idsondage);
//			objChart.render(div_sondage);
			div_sondage.show();
		}
	}
	else{
		if(form_sondage)
		{
			form_sondage.show();

			var DelayedEvent={
				handler:null,
				submit:function(event,form_sondage){
					var radio = form_sondage['reponse'];
					var	j=0;
					var booChecked=false;
					while(j<radio.length && booChecked==false)
					{
						booChecked=$(radio[j]).checked;
						j++;	
					}
					if(booChecked==false)
					{
						alert('Choisissez une r�ponse avant de proc�der au vote');
						Event.stop(event);
					}
					else
					{
						setCookie('sondage'+ idsondage,idsondage,60,'/',null,null);
					}
				}
			}
			DelayedEvent.handler = DelayedEvent.submit.bindAsEventListener(DelayedEvent,form_sondage)
			Event.observe(form_sondage,'submit',DelayedEvent.handler,false);	
		}		
	}
}