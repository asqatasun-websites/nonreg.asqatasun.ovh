function SetPageHierarchy(propVal)
{
	var levelCollection
	var levelHierarchy
	var propItem
	var propValCollection
	var level1 = '';
	var level2 = '';
	var level3 = '';
	var pagecnt = 0;
	var UserGuid = GetFsCookie("UserId");

	if (UserGuid!=""){
		s.prop29=UserGuid;
	}

	s.linkTrackVars='eVar38,eVar39,eVar40,prop15,prop16,prop17,prop29,hier2';
	propValCollection = propVal.split("|")

	for (propItem in propValCollection){
		levelCollection = propValCollection[propItem].split(";");
		
		if (levelCollection[0]){
			if (level1 != levelCollection[0]){
				s.eVar38 = levelCollection[0];
				s.prop15 = levelCollection[0];
			}
				level1 = levelCollection[0];
				levelHierarchy = levelCollection[0];
		}
		else{
			levelHierarchy = "No Level 1";
		}
		
		if (levelCollection[1]){
			if (level2 != levelCollection[1]){
				s.eVar39=levelCollection[1];
				s.prop16=levelCollection[1];
			}
			level2 = levelCollection[1];
			levelHierarchy = levelHierarchy + "/" + levelCollection[1];
		}
		else{
			levelHierarchy = levelHierarchy + "/" + "No Level 2";
		}
		
		if (levelCollection[2]){
			if (level3 != levelCollection[2]){
				s.eVar40=levelCollection[2];
				s.prop17=levelCollection[2];
			}
			level3 = levelCollection[2];
			levelHierarchy = levelHierarchy + "/" + levelCollection[2];
		}
		else{
			levelHierarchy = levelHierarchy + "/" + "No Level 3";
		}
		
		s.hier2=levelHierarchy;
		
		pagecnt = ++pagecnt;
		if (pagecnt > 1){
		s.tl(true,'o','Channel Hierarchy');
		}
		
		s.eVar38 = undefined;
		s.eVar39 = undefined;
		s.eVar40 = undefined;
		s.prop15 = undefined;
		s.prop16 = undefined;
		s.prop17 = undefined;
		s.hier2 = undefined;
		levelHierarchy = '';
	}
 s.linkTrackVars='prop21,prop22,prop23,prop24';
}

function SetSearchHierarchy(propVal,searchKeyWord)
{
	var levelCollection
	var levelHierarchy
	var propItem
	var propValCollection
	var level1 = '';
	var level2 = '';
	var level3 = '';
	var searchcnt = 0;
	var UserGuid = GetFsCookie("UserId");

	if (UserGuid!=""){
		s.prop29=UserGuid;
	}
	
	s.linkTrackVars='prop4,prop26,prop27,prop28,prop29';
	
	s.prop4 = searchKeyWord;
	
	propValCollection = propVal.split("|")

	for (propItem in propValCollection){
		levelCollection = propValCollection[propItem].split(";");
		
		if (levelCollection[0]){
			if (level1 != levelCollection[0]){
				s.prop26 = levelCollection[0];
			}	
			level1 = levelCollection[0];
		}
		
		if (levelCollection[1]){
			if (level2 != levelCollection[1]){
				s.prop27 = levelCollection[1];
		    }
		    level2 = levelCollection[1];
		}
		
		if (levelCollection[2]){
			if (level3 != levelCollection[2]){
				s.prop28 = levelCollection[2];
		    }
		    level3 = levelCollection[2];
		}

		searchcnt = ++searchcnt;
		if (searchcnt > 1){
		s.tl(true,'o','Search Page Referrer Channel Hierarchy');
		}
		
		s.prop26 = undefined;
		s.prop27 = undefined;
		s.prop28 = undefined;
	}
   s.linkTrackVars='prop21,prop22,prop23,prop24';
}

function sendFeed(pageName) {

if (typeof(s_account)!='undefined') {
	if (typeof(s)!='undefined') {

		var tempPageName = pageName;
		var subscription = "RSS Subscription";
		var eventName = "event40";
		var pageNameToLower = pageName.toLowerCase();
		var c_type = "type";
		var c_accenture = "/accenture/";
		var c_aspx = ".aspx?";
	
		var feedLocation = pageNameToLower.indexOf(c_type + "=podcast");
		if (feedLocation > -1) {
			subscription = subscription.replace("RSS", "Podcast");
			eventName = eventName.replace("0","1");
		}
	
		var aspxLocation = tempPageName.indexOf(c_aspx);
		if (aspxLocation > -1) {
			tempPageName = getFeedName(tempPageName, "/");
		}
		else {
			var splitted = tempPageName.split("/");
			var limit = splitted.length - 1;
			for (var ctr = limit; ctr > 0; ctr --){
				var currentValue = splitted[ctr];
					if (currentValue.length > 0) {
					var temp = currentValue;
					if (currentValue.indexOf("?") > -1) {
						temp = getFeedName(currentValue, "?");
						temp = currentValue.replace("?" + temp, '');
					}
				
					if (temp.length > 0) {
						tempPageName = temp;
						break;
					}
				}
				else if (currentValue.indexOf("?") == -1 && currentValue.length > 0){
					var temp = getFeedName(currentValue, "?");
					tempPageName = currentValue.replace("?" + temp, '');
					break;
				}
			}
		}
	
		var data = s.pageName + " | " + tempPageName + " | " + subscription;
		sendOmniture(eventName,data,subscription);
		}
	}
}
		
function getFeedName(url, delimiter) {
	var urlLocation = url.lastIndexOf(delimiter) ;
	if (urlLocation > -1) {
		urlLocation++;
		var urlLocationLength = url.length;
		return url.slice(urlLocation, urlLocationLength); 
	}
	return -1;
}
 
function sendOmniture(eventName,data,description)
{
	var s=s_gi(s_account); 
	var UserGuid = GetFsCookie("UserId");

	if (UserGuid!=""){
		s.prop29=UserGuid;
	}
	
	// Initialization
	s.linkTrackVars='eVar34,prop29,events';
	s.linkTrackEvents=eventName;
	
	// Population
	s.eVar34=data;
	s.events=eventName;
	s.tl(true,'o',description);
}

function formStartEvent(urlpage)
{	

	if (typeof(s)!='undefined')
	{
		url = currentPage
		var cookieVal=GetFsCookie('formStart');
		if (cookieVal!=null && cookieVal!="")	
		{
			if (cookieVal.indexOf(url)==-1)
			{
				TriggerFormStart(cookieVal,url)
			}
		}
		else
		{
			TriggerFormStart(cookieVal,url)
		}	
	}
}

function TriggerFormStart(cookieVal,url)
{

	if (navigator.appName=='Microsoft Internet Explorer') {
	  if ((window.name=='pop')||(window.name=='popupwindow')) {
		if (document.cookie.indexOf("ReferrerURL")!=-1) {
			
		var RawIEPopUpCookie = document.cookie;
		var IEPopUpCookie
    
		IEPopUpCookie=RawIEPopUpCookie.substring(RawIEPopUpCookie.indexOf("ReferrerURL"));
			
		if (IEPopUpCookie.indexOf("?")!=-1 )
		{
		  IEPopUpCookie=IEPopUpCookie.substring(IEPopUpCookie.indexOf("//"), IEPopUpCookie.indexOf("?"));
		  IEPopUpCookie=IEPopUpCookie.substring(2);
		  IEPopUpCookie=IEPopUpCookie.substring(IEPopUpCookie.indexOf("/"));
		  IEPopUpCookie=IEPopUpCookie.substring(1);      
		  formPageReferrer=IEPopUpCookie;
		}
		else
		{
		  IEPopUpCookie=IEPopUpCookie.substring(IEPopUpCookie.indexOf("//"), IEPopUpCookie.indexOf(";"));
		  IEPopUpCookie=IEPopUpCookie.substring(2);
		  IEPopUpCookie=IEPopUpCookie.substring(IEPopUpCookie.indexOf("/"));
		  IEPopUpCookie=IEPopUpCookie.substring(1);      
		  formPageReferrer=IEPopUpCookie;     
		}    
	   }
	 }
   }
   
	var UserGuid = GetFsCookie("UserId");

	if (UserGuid!=""){
		s.prop29=UserGuid;
	}
	
	SetFsCookie('formStart'	,cookieVal,url)

	s.linkTrackVars='eVar15,prop29,events';
	s.linkTrackEvents='event3';
	s.eVar15=formPageReferrer + " | " + url;
	s.events='event3';
	s.tl(true,'o','Form Start'); 

	SetFRCookie('formPageReferrer', formPageReferrer, 30, '/', '', '')
 }


function GetFsCookie(c_name)
{
if (document.cookie.length>0)
  {
  c_start=document.cookie.indexOf(c_name + "=");
  if (c_start!=-1)
    { 
    c_start=c_start + c_name.length + 1; 
    c_end=document.cookie.indexOf(";",c_start);
    if (c_end==-1) c_end=document.cookie.length;
    return unescape(document.cookie.substring(c_start,c_end));
    } 
  }
return "";
}

function SetFsCookie(c_name,oldvalue,newvalue)
{
	//var exdate=new Date();
	//var expiredays=30; 
	//exdate.setDate(exdate.getDate()+expiredays);
	if (oldvalue!=null && oldvalue!="")
	{
		document.cookie=c_name+ "=" + oldvalue + "|" +escape(newvalue);// + ((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
	}
	else
	{
		document.cookie=c_name+ "=" +escape(newvalue);// + ((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
	}
}

function deleteCookie(name) 
{
var cookieVal=GetFsCookie(name);
if (cookieVal!=null)
	document.cookie = name + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';
}

function SetFRCookie( name, value, expires, path, domain, secure )
{
var today = new Date();
today.setTime( today.getTime() );

if ( expires )
{
expires = 30 * 1000 * 60 * 60 * 24;
}
var expires_date = new Date( today.getTime() + (expires) );

document.cookie = name + "=" +( value ) +
( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) +
( ( path ) ? ";path=" + path : "" ) +
( ( domain ) ? ";domain=" + domain : "" ) +
( ( secure ) ? ";secure" : "" );
}

//Create a cookie and set its attributes (client-side)
function SetCookie(name, value, expireYear, expireMonth, expireDay, domain, secure)
{
	var strCookie = name + "=" + escape(value) + "; path=/";
		
	//optional attributes	
	if (expireYear)
	{
		//Date format in paramater should be YYYY-MM-DD
		var expires = new Date(expireYear, expireMonth-1, expireDay);
		strCookie += "; expires=" + expires.toGMTString();
	}
		
	if (domain)
		strCookie += "; domain=" + escape(domain);
		
	if (secure)
		strCookie += "; secure";
	
	document.cookie = unescape(strCookie);
}


//Get Featured Result Data and keyword
function GetFeaturedResult(eventName,data)
{           
	if (eventName="Featured Result")
		{
			var s=s_gi(s_account); 
			var searchKeyword = s.prop4;
			searchKeyword = searchKeyword.slice(0,1).toUpperCase() + searchKeyword.slice(1);
			data = "Featured Result | " + searchKeyword + " | " + data;                     
			s.linkTrackVars='pageName,events';
            s.linkTrackEvents='event20';    
			s.pageName=data;                                                                  
			s.tl(true,'o','Featured Result Link');
        }           
	s.linkTrackVars = "None";
	s.linkTrackEvents = "None";
	s.events = "";               
}

