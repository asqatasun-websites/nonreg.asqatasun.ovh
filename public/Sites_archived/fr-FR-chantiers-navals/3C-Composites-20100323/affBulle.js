//######################################################################################
// INFO BULLE DHTML
//######################################################################################
/*
	<!-- Appel de l'initialisation des infos bulles (laisser dans le BODY) -->
	<!-- initBulle(style) -->
	<script language="JavaScript">initBulle();</script>
	...
	<a href="#" onmouseover="affBulle('Info bulle du lien')" onmouseout="hideBulle()">Lien</a>

stopBulle = setTimeout('hideBulle()',5000);
clearTimeout(stopBulle);
*/
var IB = new Object;
var posX = 0;
var posY = 0;
var xOffset = 10;
var yOffset = 20;

function affBulle(texte) {
	stopBulle = setTimeout('hideBulle()',2500);
	contenu=texte+"&nbsp;";
	var finalPosX=posX-xOffset;
	if (finalPosX<0) finalPosX=0;
	if (document.layers) {
		document.layers["bulle"].document.write(contenu);
		document.layers["bulle"].document.close();
		document.layers["bulle"].top=posY+yOffset;
		document.layers["bulle"].left=finalPosX;
		document.layers["bulle"].visibility="show";}
	if (document.all) {
		var bulle=document.all["bulle"];
		bulle.innerHTML="";
		bulle.innerHTML=contenu;
		bulle.style.top=posY+yOffset;
		bulle.style.left=finalPosX;
		bulle.style.visibility="visible";
	}
	else if (document.getElementById) {
		document.getElementById("bulle").innerHTML=contenu;
		document.getElementById("bulle").style.top=posY+yOffset;
		document.getElementById("bulle").style.left=finalPosX;
		document.getElementById("bulle").style.visibility="visible";
	}
}
function getMousePos(e) {
	if (document.all) {
	posX=event.x+document.body.scrollLeft;
	posY=event.y+document.body.scrollTop;
	}
	else {
	posX=e.pageX;
	posY=e.pageY; 
	}
}
function hideBulle() {
	clearTimeout(stopBulle);
	if (document.layers) {document.layers["bulle"].visibility="hide";}
	if (document.all) {document.all["bulle"].style.visibility="hidden";}
	else if (document.getElementById){document.getElementById("bulle").style.visibility="hidden";}
}

function initBulle() {
	if (document.layers) {
		window.captureEvents(Event.MOUSEMOVE);window.onMouseMove=getMousePos;
		document.write("<layer name='bulle' top=0 left=0 visibility='hide' class='bulle'></layer>");
	}
	if (document.all) {
		document.write("<div id='bulle' style='position:absolute;top:0;left:0;visibility:hidden' class='bulle'></div>");
		document.onmousemove=getMousePos;
	}
	else if (document.getElementById) {
	        document.onmousemove=getMousePos;
	        document.write("<div id='bulle' style='position:absolute;top:0;left:0;visibility:hidden' class='bulle'></div>");
	}

}

//######################################################################################
// FONCTIONS DIVERSES
//######################################################################################

// Ouvre une fenetre popup
function affPopup(url,largeur,hauteur,paramRes,paramScr,paramMen,paramToo,paramDir,paramLoc,paramSta) {
	if (!url) {url = 'about:blank'}
	var haut = (screen.height - hauteur) / 2;
	var gauche = (screen.width - largeur) / 2;
	var params = 'width=' + largeur + ',height=' + hauteur + ',top=' + haut + ',left=' + gauche;
	//alert(screen.width+', '+screen.height+', '+params);
	params += ',resizable='   + (paramRes ? 'yes' : 'no');
	params += ',scrollbars='  + (paramScr ? 'yes' : 'no');
	params += ',menubar='     + (paramMen ? 'yes' : 'no');
	params += ',toolbar='     + (paramToo ? 'yes' : 'no');
	params += ',directories=' + (paramDir ? 'yes' : 'no');
	params += ',location='    + (paramLoc ? 'yes' : 'no');
	params += ',status='      + (paramSta ? 'yes' : 'no');
	popup = window.open(url,'',params);
	if (parseInt(navigator.appVersion) >= 4) { popup.window.focus(); }
}
// Coche ou decoche une serie de cases a cocher
/*
	<a href="javascript:cocheTout('formulaire','cc1',true)">Coche tout</a><br>
	<a href="javascript:cocheTout('formulaire','cc1',false)">Decoche tout</a>
*/
function cocheTout(formulaire,checkboxname,etat){
	var serie = eval("document.forms."+formulaire+"."+checkboxname);
	for (i=0; i<serie.length; i++) {
		if (serie[i].disabled == false) serie[i].checked = etat;
	}
}
// Tabulation automatique
/*
	<b>Enter your 10-digit phone number:</b>
	<form name="sampleform">
	<input type="text" name="first" size=4 onKeyup="autotab(this, document.sampleform.second)" maxlength=3> <input type="text" name="second" size=4 onKeyup="autotab(this, document.sampleform.third)" maxlength=3> <input type="text" name="third" size=5 maxlength=4>
	</form>
*/
function autotab(original,destination) {
	if (original.getAttribute && original.value.length == original.getAttribute("maxlength")) destination.focus();
}
// Fonction Ajouter aux favoris
function ajouterAuxFavoris(url,titre){
	if (document.all) window.external.AddFavorite(url,titre);
}
// Affiche ou masque un element html (id), visible = true / false
function changeVisibility(idElement,bascule,visible) {
	if (visible || (bascule && document.getElementById(idElement).style.visibility == 'hidden')) {
		document.getElementById(idElement).style.visibility = 'visible';
	} else {
		document.getElementById(idElement).style.visibility = 'hidden';
	}
}
function changeDisplay(idElement,bascule,display) {
	if (display || (bascule && document.getElementById(idElement).style.display == 'none')) {
		document.getElementById(idElement).style.display = 'block';
	} else {
		document.getElementById(idElement).style.display = 'none';
	}
}

//######################################################################################
// GESTION DES LISTES
//######################################################################################
/*
	<select name="test2_liste1" size="9" style="width:200px">
		<option value="a1">Article 1</option>
		<option value="a2">Article 2</option>
		<option value="a3">Article 3</option>
		<option value="a4">Article 4</option>
		<option value="a5">Article 5</option>
	</select>
	<br>
	<a href="javascript:liste_ajouter('formulaire','test2',true)" title="Ajouter"><img src="images/plus.png" width="12" height="12" border="0"></a>
	<a href="javascript:liste_enlever('formulaire','test2')" title="Enlever"><img src="images/moins.png" width="12" height="12" border="0"></a>
	&nbsp;&nbsp;
	<a href="javascript:liste_monter('formulaire','test2')" title="Monter"><img src="images/haut.png" width="12" height="12" border="0"></a>
	<a href="javascript:liste_descendre('formulaire','test2')" title="Descendre"><img src="images/bas.png" width="12" height="12" border="0"></a>
	<br>
	<select name="test2_liste2" size="9" style="width:200px">
	</select>
	<input type="hidden" name="test2" size="40">
*/
var txt_aucunElementSel = 'Aucune �l�ment s�lectionn�';
var txt_aucunTexte = 'Aucun texte';

// Recuperer les champs du formulaire
function liste_champs(formulaire,name) {
	position = name.indexOf('&');
	if (position > 0) {
		name1 = name.substr(0,position);
		name2 = name.substr(position+1);
	} else {
		name1 = name;
		name2 = name;
	}
	liste1 = eval('document.'+formulaire+'.'+name1+'_liste1');
	liste2 = eval('document.'+formulaire+'.'+name2+'_liste2');
	champOrdre = eval('document.'+formulaire+'.'+name2);
	//alert(' name1='+name1+'\n name2='+name2);
}
// Deplace un element de la liste liste1 vers la liste liste2
function liste_deplacer(liste1,liste2,supprimer) {
	if (liste1.options.selectedIndex>=0) {
		op = new Option(liste1.options[liste1.options.selectedIndex].text,liste1.options[liste1.options.selectedIndex].value);
		liste2.options[liste2.options.length] = op;
		ex = liste1.options.selectedIndex;
		if (supprimer) {
			liste1.options[liste1.options.selectedIndex] = null;
			liste1.options.selectedIndex = (ex < liste1.options.length) ? ex : liste1.options.length - 1;
		}
	} else {
		alert(txt_aucunElementSel);
	}
}
// Ajouter un element de liste, si supprimer = true, alors on supprime l'element de liste1
function liste_ajouter(formulaire,name,supprimer) {
	liste_champs(formulaire,name);
	liste_deplacer(liste1,liste2,supprimer)
	liste_ordre(liste2,champOrdre);
}
// Enlever un element de liste
function liste_enlever(formulaire,name) {
	liste_champs(formulaire,name);
	liste_deplacer(liste2,liste1,true)
	liste_ordre(liste2,champOrdre);
}
// Monter un element de liste
function liste_monter(formulaire,name) {
	liste_champs(formulaire,name);
	var indice = liste2.selectedIndex;
	if (indice<0) {
		alert(txt_aucunElementSel);
	}
	if (indice>0) {
		liste_echanger(liste2,indice,indice-1,champOrdre);
	}
}
// Descendre un element de liste
function liste_descendre(formulaire,name) {
	liste_champs(formulaire,name);
	var indice = liste2.selectedIndex;
	if (indice<0) {
		alert(txt_aucunElementSel);
	}
	if (indice<liste2.options.length-1) {
		liste_echanger(liste2,indice,indice+1,champOrdre);
	}
}
// Echanger un element de liste
function liste_echanger(liste,i,j,champOrdre) {
	var valeur = liste.options[i].value;
	var texte = liste.options[i].text;
	var couleur = liste.options[i].style.backgroundColor;
	liste.options[i].value = liste.options[j].value;
	liste.options[i].text = liste.options[j].text;
	liste.options[i].style.backgroundColor = liste.options[j].style.backgroundColor;
	liste.options[j].value = valeur;
	liste.options[j].text = texte;
	liste.options[j].style.backgroundColor = couleur;
	liste.selectedIndex = j;
	liste_ordre(liste,champOrdre);
}
// Optenir l'ordre de la liste
function liste_ordre(liste,champOrdre) {
	var ordre = '';
	for(var i=0;i<liste.options.length;i++) {
		if (i>0) {ordre += ',';}
		ordre += liste.options[i].value;		
	}
	champOrdre.value = ordre;
}
// Ajouter un element de liste (avec gestion des couleurs), si supprimer = true, alors on supprime l'element de liste1
function liste_ajouterCouleur(formulaire,name,supprimer) {
	saisie = eval('document.'+formulaire+'.'+name+'_saisie');
	couleurs = eval('document.'+formulaire+'.'+name+'_couleurs');
	liste2 = eval('document.'+formulaire+'.'+name+'_liste2');
	champOrdre = eval('document.'+formulaire+'.'+name);
	if (saisie.value != '') {
		texte = saisie.value.replace(/,/gi,'');
		texte = texte.replace(/\|/gi,'');
		et = texte + ' [' + couleurs.options[couleurs.options.selectedIndex].text + ']';
		ev = texte + '|' + couleurs.value;
		op = new Option(et,ev);
		liste2.options[liste2.options.length] = op;
		op.style.backgroundColor = '#' + couleurs.value;
		if (supprimer) saisie.value = '';
	} else {
		alert(txt_aucunTexte);
	}
	liste_ordre(liste2,champOrdre);
}
// Enlever un element de liste (avec gestion des couleurs)
function liste_enleverCouleur(formulaire,name,champOrdre) {
	saisie = eval('document.'+formulaire+'.'+name+'_saisie');
	couleurs = eval('document.'+formulaire+'.'+name+'_couleurs');
	liste2 = eval('document.'+formulaire+'.'+name+'_liste2');
	champOrdre = eval('document.'+formulaire+'.'+name);
	if (liste2.options.selectedIndex>=0) {
		temp = liste2.options[liste2.options.selectedIndex].value.split('|');
		saisie.value = temp[0];
		for(var i=0;i<couleurs.options.length;i++) {
			if (couleurs.options[i].value == temp[1]) index = i;		
		}
		couleurs.selectedIndex = index;
		document.getElementById(name+'_couleur').style.backgroundColor='#'+temp[1];
		ex = liste2.options.selectedIndex;
		liste2.options[liste2.options.selectedIndex] = null;
		liste2.options.selectedIndex = (ex < liste2.options.length) ? ex : liste2.options.length - 1;
	} else {
		alert(txt_aucunElementSel);
	}
	liste_ordre(liste2,champOrdre);
}
