function sb_pdf(path,lng){
	// Section language
	if(lng!='en' && lng!='fr')
	lng	=	'fr';
	
	// Section language
	pagetitle	=	'Parcsafari';
	closealt	=	'Fermer';
	if(lng=='en'){
		pagetitle	=	'Parcsafari';
		closealt	=	'Close';
	}
	
	// Section tilte HTML
	pagetitle	=	'<div id="title_txt">'+pagetitle+'</div>';
	pagetitle	+=	'<div id="btn_close"><a href="javascript:void(0);" onclick="javascript:Shadowbox.close();"><img src="'+path+'js/shadowbox/src/skin/default/icons/close.gif" alt="'+closealt+'" title="'+closealt+'"/></a></div>';
	
	// Content
	//content		=	'<a href="javascript:void(0);" onclick="javascript:popup_pdf(\''+path+'\',\''+lng+'\');"><img src="'+path+'images/accueil/'+lng+'/20_dollars.jpg" /></a>';
	content		=	'<img src="'+path+'images/accueil/'+lng+'/promo.jpg" />';
	
	Shadowbox.open({
		player: 	'html',
		content: 	content,
		height: 	460,
		width:		266,
		title:		pagetitle,
		options:	{
			top_pos:		20,
			hide_object:	false
		}
	});
}

function popup_pdf(path,lng){
	width	=	920;
	height	=	600;
	
	posleft	=	(screen.width-width)/2;
	postop	=	(screen.height-height)/2;
	
	page_popup_pdf	=	window.open(path+"pdf/accueil/20dollars_"+lng+".pdf", "page_popup_pdf", "width="+width+",height="+height+",toolbar=0,scrollbars=0,statusbar=0,menubar=0,resizable=1,left="+posleft+",top="+postop+"");
	page_popup_pdf.focus();
}