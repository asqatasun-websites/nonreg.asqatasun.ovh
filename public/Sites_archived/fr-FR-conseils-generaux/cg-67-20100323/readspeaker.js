﻿/* The selected text function */

var selectedString = "";

function getSelectedHTML() {
    selectedString = "";
    var rng = undefined;
    if (window.getSelection) {
        selobj = window.getSelection();
        if (!selobj.isCollapsed) {
            if (selobj.getRangeAt) {
                rng = selobj.getRangeAt(0);
            }
            else {
                rng = document.createRange();
                rng.setStart(selobj.anchorNode, selobj.anchorOffset);
                rng.setEnd(selobj.focusNode, selobj.focusOffset);
            }
            if (rng) {
                DOM = rng.cloneContents();
                object = document.createElement('div');
                object.appendChild(DOM.cloneNode(true));
                selectedString = object.innerHTML;
            }
            else {
                selectedString = selobj;
            }
        }
    }
    else if (document.selection) {
        selobj = document.selection;
        rng = selobj.createRange();
        if (rng && rng.htmlText) {
            selectedString = rng.htmlText;
        }
        else if (rng && rng.text) {
            selectedString = rng.text;
        }
    }
    else if (document.getSelection) {
        selectedString = document.getSelection();
    }
    selectedString = cleanSelectedString(selectedString);
}

function copyselected() {
    setTimeout("getSelectedHTML()", 50);
    return true;
}

document.onmouseup = copyselected;
document.onkeyup = copyselected;

/* The expanding function */

function readspeaker(rs_call, nbArt) {
    
    if (selectedString.length > 0) {
        rs_call = rs_call.replace("/cgi-bin/rsent?", "/enterprise/rsent_wrapper.php?");
    }
    savelink = rs_call + "&save=1";
    start_rs_table = "<table style='border:1px solid #aeaeae; font-size: 10px;'><tr><td>";
    rs_embed = "<object type='application/x-shockwave-flash' data='http://media.readspeaker.com/flash/readspeaker20.swf?mp3=" + escape(rs_call) + "&autoplay=1&rskin=bump' style='height:20px; width:250px;'><param name='movie' value='http://media.readspeaker.com/flash/readspeaker20.swf?mp3=" + escape(rs_call) + "&autoplay=1&rskin=bump' /><param name='quality' value='high' /><param name='SCALE' value='exactfit' /><param name='wmode' value='transparent' /><embed wmode='transparent' src='http://media.readspeaker.com/flash/readspeaker20.swf?mp3=" + escape(rs_call) + "&autoplay=1&rskin=bump' quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer' type='application/x-shockwaveflash' scale='exactfit' style='height:20px; width:250px;' /></embed></object>";
    rs_downloadlink = "<br /><a href='" + savelink + "'>Télécharger</a>";
    close_rs = "<br /><a href='#' onclick='close_rs_div("+nbArt+"); return false;'>Fermer</a>";
    end_rs_table = "</td></tr></table>";

    var x;
    if (document.getElementById('rs_div')) {
        x = document.getElementById('rs_div');
     
    }
    else {
       
            x = document.getElementById('rs_div'+nbArt);
    }
     x.innerHTML = start_rs_table + rs_embed + rs_downloadlink + close_rs + end_rs_table;
    
}

function close_rs_div(pos) {
    var x;
    if (document.getElementById('rs_div')) {
        x = document.getElementById('rs_div');
    }
    else {
        x = document.getElementById('rs_div'+pos);

    }
    x.innerHTML = "";
}

/* Selected text cleaning function */

function cleanSelectedString(theString) {
    var comments = theString.match(/<!--/gi);
    var temp = "";
    if (comments != undefined) {
        for (i = 0; i < comments.length; i++) {
            temp = theString.substring(theString.search(/<!--/gi), theString.search(/-->/gi) + 3);
            theString = theString.replace(temp, "");
        }
    }
    var regexp = /(<\/?[A-Z]+[0-9]?)\s?[^>]*>/gi
    theString = theString.replace(regexp, "$1>");
    var regexp2 = /<\/?[A-Z]+[0-9]?\s?>/gi
    theString = theString.replace(regexp2, function(m) { return returnTags(m); });
    var regexp_spaces = /\s+?\s*/gi
    theString = theString.replace(regexp_spaces, " ");
    var regexp_br = /(<br>)+?\s?(<br>\s*)*/gi
    theString = theString.replace(regexp_br, "<br>");
    return theString;
}

function returnTags(thematch) {
    var regtag = /<\/?(h[1-6]|a|area|ul|ol|dl|dd|dt|li|table|td|tr|th|p|hr|br)>/i
    if (regtag.test(thematch)) {
        return thematch;
    }
    else {
        return "";
    }
}
