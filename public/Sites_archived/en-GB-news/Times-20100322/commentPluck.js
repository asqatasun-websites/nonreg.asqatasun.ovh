<!--
var noOfComments = '0';

var oBrowserObject = {
		currentBrowser: function() {
			var sAgent = navigator.userAgent.toLowerCase();
			var curr;	
			curr = (sAgent.indexOf("chrome") != "-1") ? "chrome" : (sAgent.indexOf("firefox") != "-1") ? "firefox" : (sAgent.indexOf("opera") != "-1") ? "Opera" :(sAgent.indexOf("safari") != "-1") ? "Safari" : (sAgent.indexOf("msie") != "-1") ? "IE" : (sAgent.indexOf("netscape") != "-1") ? "netscape": "Unknown";
			return curr;
		}
	};
// -----------------------
// FUNCTION: fSubmitRequestBatch
// DESCRIPTION: A function to send the request batch to Pluck
// ARGUMENTS: daapiUrl is the Pluck begin request url.articleId is the article id of the 
// article being viewed
// -----------------------

function fSubmitRequestBatch(daapiBeginRequestUrl , articleId) {
	
	var requestBatch = new RequestBatch();
	var articleKey = new ArticleKey(articleId);
	if(displayBehaviouralNav){
		var sections = new Array(new Section("All"));  
       var categories = new Array(new Category("All"));  
       var userTiers = pluckUserTiers.split(',');
       
       var contributors = new Array();  
       for(var tierItr = 0 ; tierItr < userTiers.length ; tierItr++ ){
    	   contributors[tierItr] = new UserTier(userTiers[tierItr]); 
       }       
       var activity = new Activity("Commented");  
       var age = eval(mostCommentedDays); // maximum age in days  
       var numItemsToGet = 4;  
               
        // create and send request  
        var discoveryAction = new DiscoverArticlesAction(  
                                         sections,  
                                         categories,  
                                         contributors,  
                                         activity,  
                                         age,  
                                         numItemsToGet);  
                   
       requestBatch.AddToRequest(discoveryAction);     
	}
       requestBatch.AddToRequest(articleKey);           
	requestBatch.BeginRequest(daapiBeginRequestUrl , fRenderNumberOfComments);

}
//-----------------------
//FUNCTION: fDisplayCommentPageNum
//DESCRIPTION: A function to set the page numbers for comments for eg. Displaying 1-10
//ARGUMENTS:pgnum - page number of comments received from Pluck, perpage - number of comments displayed perpage
//
function fDisplayCommentPageNum(pgnum, perpage){
	var start = (eval(pgnum)-1)*perpage+1;
	var end = eval(pgnum)*perpage;
	var comments = document.getElementById('comment-bottom-p').innerHTML ;
	var tmparr = new Array();
	tmparr = comments.split(" ");

	if(eval(tmparr[0])<end)
		end = tmparr[0];
	document.getElementById('comment-display-page-num').innerHTML = ' (Displaying '+start+'-'+end+')';

}

// -----------------------
// FUNCTION: fRenderNumberOfComments
// DESCRIPTION: A function to set the noOfComments
// ARGUMENTS:responseBatch is the response received from Pluck
// 
function fRenderNumberOfComments(responseBatch) {
	
	var isValidLength = false , isMostCommented = false;
	
	if(responseBatch.Responses.length > 0){
		  isValidLength = true;
          var responsePluck = responseBatch.Responses[0];
          if(responsePluck.DiscoverArticlesAction != null){
			  if(responsePluck.DiscoverArticlesAction.DiscoveredArticles.length > 0 )
				  isMostCommented = true;
	          for(var i=0; i< responsePluck.DiscoverArticlesAction.DiscoveredArticles.length; i++){
					    
			       articleHeadlines[i] = responsePluck.DiscoverArticlesAction.DiscoveredArticles[i].PageTitle;	
			       articlePaths[i] = responsePluck.DiscoverArticlesAction.DiscoveredArticles[i].PageUrl;
	                                
	          }
		}
     }

	if(isValidLength){
		var responsePluck;
			if(!isMostCommented)
				 responsePluck = responseBatch.Responses[0];
			else
				 responsePluck = responseBatch.Responses[1];
			
		   if (responsePluck.Article != null) { 
				 noOfComments = responsePluck.Article.Comments.NumberOfComments;	
				 if(noOfComments == '0'){
					 if(pgoffset == '0'){
						 document.getElementById('comment-count').style.display = 'none';
					 }
					 document.getElementById('comment-sort-order').style.display = 'none';
					 document.getElementById('disable-output-widget').style.display = 'none';	
				 }
				 else if ( noOfComments == '1' ){
					 if(pgoffset == '0'){
						 document.getElementById('comment-link').innerHTML = noOfComments + ' Comment';
					 }
					document.getElementById('comment-bottom-p').innerHTML = noOfComments + ' Comment';
					document.getElementById('comment-sort-order-only').style.display = 'none';	
				 }
				 else{
					 if(pgoffset == '0'){
						 document.getElementById('comment-link').innerHTML = noOfComments + ' Comments';
					 }
					document.getElementById('comment-bottom-p').innerHTML = noOfComments + ' Comments';
				 }
			}else{
				if(pgoffset == '0'){
					document.getElementById('comment-count').style.display = 'none';
				}
			   document.getElementById('comment-sort-order').style.display = 'none';	
			   document.getElementById('disable-output-widget').style.display = 'none';	
		    }
		}else{
			if(pgoffset == '0'){
				document.getElementById('comment-count').style.display = 'none';
			}
			document.getElementById('comment-sort-order').style.display = 'none';	
			document.getElementById('disable-output-widget').style.display = 'none';		
		}
		
	}


// -----------------------
// FUNCTION: fGetRequestObject
// DESCRIPTION: A function that gets the request object for different browsers
// ARGUMENTS: 
// RETURN: None
// -----------------------
var bRequest = false;

function fGetRequestObject() {
	try {
		bRequest = new XMLHttpRequest();
	} 
	catch (trymicrosoft){
		try {
			bRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} 
		catch (othermicrosoft) {
			try {
				bRequest = new ActiveXObject("Microsoft.XMLHTTP");			
			} 
			catch (failed) {
				bRequest = false;			 
			}  
		}
	}
	if (!bRequest)
		alert("Error initializing XMLHttpRequest!");
}

//-----------------------
//FUNCTION: fGetRequestObjectLocal
//DESCRIPTION: A function that gets the request object for different browsers
//ARGUMENTS: 
//RETURN: None
//-----------------------
function fGetRequestObjectLocal() {
	try {
		bRequest = new XMLHttpRequest();
	} 
	catch (trymicrosoft){
		try {
			bRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} 
		catch (othermicrosoft) {
			try {
				bRequest = new ActiveXObject("Microsoft.XMLHTTP");			
			} 
			catch (failed) {
				bRequest = false;			 
			}  
		}
	}
	if (!bRequest)
		alert("Error initializing XMLHttpRequest!");
	return bRequest;
}



//-----------------------
//FUNCTION: fValidateIsNotEqual
//DESCRIPTION: A function that checks for is not equal to condition.
//ARGUMENTS: two parameters to be compared
//RETURN: true or false
//-----------------------
function fValidateIsNotEqual(input, value){
	if(input!=value){
		return true;
	}else{
		return false;
	}
}

//-----------------------
//FUNCTION: fValidateIsEqual
//DESCRIPTION: A function that checks for is equal to condition.
//ARGUMENTS: two parameters to be compared
//RETURN: true or false
//-----------------------
function fValidateIsEqual(input, value){
	if(input == value){
		return true;
	}else{
		return false;
	}
}

//-----------------------
//FUNCTION: fCallOverlay
//DESCRIPTION: A function to display overlay
//ARGUMENTS: light - id of the div to hide and fade - id to display the background
//RETURN: None
//
function fCallOverlay(light,fade) {
	
	
	document.getElementById(fade).style.display='block';
	//document.getElementById("login-error").style.display='none';
	document.getElementById(fade).style.height=document.documentElement.offsetHeight+"px";
	
	
	if(oBrowserObject.currentBrowser()== "IE") {
		document.getElementById(fade).style.height=document.body.offsetHeight+"px";
		document.getElementById(fade).style.width=document.body.offsetWidth+"px";
		
	}
	 
	 if(light == 'login-light' && oBrowserObject.currentBrowser()== "IE"){	
		  
             document.getElementById(light).innerHTML= '<iframe id="lframe" src="'+commentsUrlPrefix+'\/genreg\/jsp\/TOL\/commentsLogin.jsp?articleUrl='+articleUrl+'" width="426" height="294" scrolling="no" frameborder=0 marginwidth=0 marginheight=0><\/iframe>' ;
	         document.getElementById("lframe").src = document.getElementById("lframe").src;
	         
        }else if (light == 'login-light' && oBrowserObject.currentBrowser() != "IE")
        {
                document.getElementById(light).innerHTML='<iframe src="'+commentsUrlPrefix+'\/genreg\/jsp\/TOL\/commentsLogin.jsp?articleUrl='+articleUrl+'" style="overflow:hidden;" scrolling="no" class="float-left" width="426" height="294" align="left" frameborder=0 marginwidth=0 marginheight=0><\/iframe>' ;

        }else if (light == 'login-fade')
        {
                document.getElementById(light).innerHTML='';
        }else if(light == 'register-light' && oBrowserObject.currentBrowser()== "IE"){
                document.getElementById(light).innerHTML='<iframe id="rframe" src="'+commentsUrlPrefix+'\/genreg\/jsp\/TOL\/commentsRegistration.jsp?articleUrl='+articleUrl+'" scrolling="no" width="426" height="890" frameborder=0 marginwidth=0 marginheight=0><\/iframe>';;
                document.getElementById("rframe").src = document.getElementById("rframe").src;

        }else if (light == 'register-light' && oBrowserObject.currentBrowser() != "IE")
        {
                document.getElementById(light).innerHTML='<iframe src="'+commentsUrlPrefix+'\/genreg\/jsp\/TOL\/commentsRegistration.jsp?articleUrl='+articleUrl+'" scrolling="no" class="float-left" width="426" height="890" align="left" frameborder=0 marginwidth=0 marginheight=0><\/iframe>';;
        }else if (light == 'register-fade')
        {
                document.getElementById(light).innerHTML='';
        }else if(light == 'participate-light' && oBrowserObject.currentBrowser()== "IE"){
                document.getElementById(light).innerHTML= '<iframe id="pframe" src="'+commentsUrlPrefix+'\/genreg\/jsp\/TOL\/commentsLogin.jsp?articleUrl='+articleUrl+'&action=confirm" scrolling="no" width="426" height="267" frameborder=0 marginwidth=0 marginheight=0><\/iframe>' ;
                document.getElementById("pframe").src = document.getElementById("pframe").src;

        }else if (light == 'participate-light' && oBrowserObject.currentBrowser() != "IE")
        {
                document.getElementById(light).innerHTML= '<iframe src="'+commentsUrlPrefix+'\/genreg\/jsp\/TOL\/commentsLogin.jsp?articleUrl='+articleUrl+'&action=confirm" style="overflow:hidden;" scrolling="no" class="float-left" width="426" height="267" align="left" frameborder=0 marginwidth=0 marginheight=0><\/iframe>' ;
        }else if (light == 'participate-fade')
        {
                document.getElementById(light).innerHTML='';
        }else if (light == 'logout-light' && oBrowserObject.currentBrowser()== "IE"){
                document.getElementById(light).innerHTML= '<iframe id="logoutframe" src="'+commentsUrlPrefix+'\/genreg\/jsp\/TOL\/commentsLogout.jsp?articleUrl='+articleUrl+'" scrolling="no" width="426" height="267" frameborder=0 marginwidth=0 marginheight=0><\/iframe>' ;
		  //alert(document.getElementById("logoutframe").src);
                document.getElementById("logoutframe").src = document.getElementById("logoutframe").src;

        }else if (light == 'logout-light' && oBrowserObject.currentBrowser() != "IE")
        {
                document.getElementById(light).innerHTML= '<iframe src="'+commentsUrlPrefix+'\/genreg\/jsp\/TOL\/commentsLogout.jsp?articleUrl='+articleUrl+'&action=confirm" style="overflow:hidden;" scrolling="no" class="float-left" width="426" height="267" align="left" frameborder=0 marginwidth=0 marginheight=0><\/iframe>' ;
        }else if (light == 'logout-fade'){
        		document.getElementById(light).innerHTML='';
	 }

	document.getElementById(light).style.display='block';
}
//-----------------------
//FUNCTION: fRemoveOverlay
//DESCRIPTION: A function to show/hide the login pop up and page
//ARGUMENTS: light - id of the div to hide and fade - id to display the background
//RETURN: None
//
function fRemoveOverlay(light,fade) {
	document.getElementById(light).style.display='none';
	document.getElementById(fade).style.display='none';
}

//-----------------------
//FUNCTION: fRemoveOverlayReset
//DESCRIPTION: A function to show/hide the regn pop up and page
//ARGUMENTS: light - id of the div to hide and fade - id to display the background
//RETURN: None
//
function fRemoveOverlayReset(light,fade,form) {
	document.getElementById(form).reset();

	if(form == "commentRegistrationForm"){
		fResetRegistrationErrors();
		fReloadCaptcha();
	}
	if(form == "commentLoginForm"){
		fResetLoginErrors();
	}
	document.getElementById(light).style.display='none';
	document.getElementById(fade).style.display='none';
	
}


//-----------------------
//FUNCTION: fCallWidget
//DESCRIPTION: A function to refresh the widget
//ARGUMENTS: 
//RETURN: None
//
function fCallWidget(){

	document.getElementById('widget-enable').style.display='none';	
	document.getElementById('widget-disable').style.display='block';	
}

document.onmousedown=function() {	
	}


//-----------------------
//FUNCTION: getDisplayName
//DESCRIPTION: A function to get display name for the user
//ARGUMENTS: 
//RETURN: None
//--------------------
function getDisplayName(){
	var atCookie = getCookie('at');
	var displayName = '';
	if(atCookie){
		var cookieValArray = atCookie.split('&');
		for(var itr = 0 ; itr < cookieValArray.length ; itr ++ ){
				if(cookieValArray[itr].indexOf('a=') != -1){
					
					displayName = replaceAll(cookieValArray[itr],'a=','');
					
					displayName = replaceAll(displayName,'+',' ');
					break;
				}	
		}
	}
	
	document.getElementById('display-name').innerHTML = displayName;
}


//-----------------------
//FUNCTION: fGetLoginStatus
//DESCRIPTION: A function to get the login status
//ARGUMENTS: 
//RETURN: None
//--------------------
function fGetLoginStatus(){
	var atCookie = getCookie('at');
	var erightsCookie = getCookie('ERIGHTS');
	var strAfterLogin ='';
	
	if( atCookie && erightsCookie){
		var currentDisplayName =  fGetCurrentDisplayName();
		strAfterLogin+='	<div class="padding-left-right-10"><div class="padding-top-10"><\/div><p class="small padding-bottom-5 color-666"><strong>'+ signinText +'<\/strong><strong> '+currentDisplayName+'<\/strong><\/p>';
		strAfterLogin+='	<div class="padding-top-5"><\/div>';
		strAfterLogin+='	<div class="float-left padding-top-2"><a class="link-06c small" href="'+myProfileUrl+'"><strong>'+myProfileText+'<\/strong><\/a><\/div>';
		strAfterLogin+='	<div class="float-left comment-border-right padding-left-6 height-20"><\/div>';
		strAfterLogin+='	<div class="float-left padding-top-2 padding-left-8"><a class="link-06c small" href="javascript:void(0)" onclick = "fCallOverlay(\'logout-light\',\'logout-fade\');"><strong>'+logoutText+'<\/strong><\/a><\/div>';
		strAfterLogin+='  	<div class="clear"><\/div><\/div>';
		
		document.getElementById("login-status").innerHTML=strAfterLogin;
	}
	else if( !atCookie && erightsCookie){
		document.getElementById("login-status").innerHTML=strAtParticipate;
	}
	else{
		document.getElementById("login-status").innerHTML=strBeforeLogin;
	}
}

//-----------------------
//FUNCTION: fGetCurrentDisplayName
//DESCRIPTION: A function to get the display  name
//ARGUMENTS: 
//RETURN: None
//--------------------
function fGetCurrentDisplayName(){
	
	var atCookie = getCookie('at');
	var displayName = '';
	if(atCookie){
		var cookieValArray = atCookie.split('&');
		for(var itr = 0 ; itr < cookieValArray.length ; itr ++ ){
				if(cookieValArray[itr].indexOf('a=') != -1){
					displayName = cookieValArray[itr].replace('a=','').replace('+',' ');
					break;
				}	
		}
	}
	return displayName;
}

//-----------------------
//FUNCTION: fAutoResizeCommentIframe
//DESCRIPTION: A function to resize the comments input widget
//			   depending upon the space occupied by thank you 
//			   error text
//ARGUMENTS: 
//RETURN: None
//--------------------
function fAutoResizeCommentIframe() {
	var iframe = document.getElementById('commentsiframe');
	if (!iframe) { return; }
	
	function addEvent(elem, name, fn) {
		if (elem.addEventListener)
			elem.addEventListener(name, fn, false);
		else if (elem.attachEvent)
			elem.attachEvent('on' + name, fn); 
	}
	function iframeFinder() {
	    var doc = window.frames['commentsiframe'].document
	    	,found = false , form , textarea , container , isPresent = false;
	    if(doc){
	    	form = doc.forms[0];
	    	container = doc.getElementById('CommentsContainer');
	    }
	   if(form){ 
	    	textarea = form.elements['comment.Body']
		    // set background colour for document element (fix IE bug)
		    if (/*@cc_on!@*/false && container && container.currentStyle)
		      doc.documentElement.style.backgroundColor = container.currentStyle.backgroundColor;
		    
		    
			for (var i = 0, elems = form.elements; elem = elems[i]; i++) {
				if (elem.type == 'submit') {
					found = true;
					break;
				}
			}
			
			if (found) {
				var node = elem, top = 0;
				do {
					top += node.offsetTop;
				} while(node = node.offsetParent);
				
				addEvent(elem, 'click', function(e) { setTimeout(function() {
					var frame = document.getElementById('commentsiframe');
					frame.style.height = (top + elem.offsetHeight + (textarea.value !== '' ? 16 : 27)) + 'px';
				},100) });
			}
		}
	}
	addEvent(iframe, 'load', iframeFinder);
}
//-----------------------
//FUNCTION: fGetPluckRequestBatch
//DESCRIPTION: A function get the request batch for Pluck
//-----------------------
function fGetPluckRequestBatch(){
	return (new RequestBatch());
}

//-----------------------
//FUNCTION: fCreateRequestBatch
//DESCRIPTION: A function get the request batch for Pluck
//ARGUMENTS: requestBatch - batch of requests to be sent to pluck 
//			 articleId - Unique id of an article
//-----------------------
function fCreateRequestBatch( requestBatch , articleId ) {
	var articleKey = new ArticleKey(articleId);
	requestBatch.AddToRequest(articleKey);           

}

//-----------------------
//FUNCTION: fSubmitReqNoOfComments
//DESCRIPTION: A function to submit request batch to Pluck
//ARGUMENTS: requestBatch - batch of requests to be sent to pluck 
//			 daapiBeginRequestUrl - pluck daapi url
//-----------------------
function fSubmitReqNoOfComments( requestBatch , daapiBeginRequestUrl ){
	requestBatch.BeginRequest(daapiBeginRequestUrl , fRenderNumberOfCommentsLockUp);
}

function fSubmitMostCommented(daapiBeginRequestUrl){
	
	var requestBatch = new RequestBatch();
	var sections = new Array(new Section("All"));  
       var categories = new Array(new Category("All"));  
       var userTiers = pluckUserTiers.split(',');
       
       var contributors = new Array();  
       for(var tierItr = 0 ; tierItr < userTiers.length ; tierItr++ ){   	   
    	   contributors[tierItr] = new UserTier(userTiers[tierItr]); 
       }           
       var activity = new Activity("Commented");  
       var age = eval(mostCommentedDays); // maximum age in days  
       var numItemsToGet = 4;  
               
        // create and send request  
        var discoveryAction = new DiscoverArticlesAction(  
                                         sections,  
                                         categories,  
                                         contributors,  
                                         activity,  
                                         age,  
                                         numItemsToGet);  
                   
       requestBatch.AddToRequest(discoveryAction);     
    
	requestBatch.BeginRequest(daapiBeginRequestUrl , fRenderMostCommented);	
}

function fRenderMostCommented(responseBatch){
	
	var output = "";
	if(responseBatch.Responses.length > 0){
                        var responsePluck = responseBatch.Responses[0];
			  

                        for(var i=0; i< responsePluck.DiscoverArticlesAction.DiscoveredArticles.length; i++){
				    
		                  articleHeadlines[i] = responsePluck.DiscoverArticlesAction.DiscoveredArticles[i].PageTitle;
	
				    
				    articlePaths[i] = responsePluck.DiscoverArticlesAction.DiscoveredArticles[i].PageUrl;
                                
                        }
 
                        
                        
        }
	
			for(var j=0; j<articleHeadlines.length; j++){
					var headline = articleHeadlines[j];
					if(articleHeadlines[j].length>=45){
						headline = articleHeadlines[j].substring(0,44)+"...";
					}
	   				output  = output  + "<li><a href="+articlePaths[j]+" class=\"link-666\">"+headline +"<\/a><\/li>"; 
			  }
	document.getElementById("mostcommented").innerHTML = output;


}

//-----------------------
//FUNCTION: fRenderNumberOfCommentsLockUp
//DESCRIPTION: A function to set the noOfComments for individual articles
//ARGUMENTS:responseBatch is the response received from Pluck
//
function fRenderNumberOfCommentsLockUp(responseBatch) {
	var responseItr , artItr , spanNo = 1;
	var responseLength = responseBatch.Responses.length;
	
	if(responseLength != 0){
		for( responseItr = 0 ; responseItr < responseLength ; responseItr++ ){
			 var responsePluck = responseBatch.Responses[responseItr];
			 if (responsePluck.Article != null) { 
				 
				 noOfComments = responsePluck.Article.Comments.NumberOfComments;
				 if(noOfComments == '0'){
					 document.getElementById('comment-count-'+spanNo).style.display = 'none';
				 }
				 else if ( noOfComments == '1' ){
					 if(showPipe)
						 document.getElementById('comment-count-'+spanNo).innerHTML = ' | '+ noOfComments + ' Comment';
					 else
						 document.getElementById('comment-count-'+spanNo).innerHTML = noOfComments + ' Comment';
				}
				 else{
					 if(showPipe)
						 document.getElementById('comment-count-'+spanNo).innerHTML = ' | '+ noOfComments + ' Comments';
					 else
						 document.getElementById('comment-count-'+spanNo).innerHTML =  noOfComments + ' Comments';
					
				 }
			}
			 spanNo++;
		}
		}else{
			for( artItr = 1 ; artItr <= noOfArticles ; artItr ++ ){
				document.getElementById('comment-count-'+artItr).style.display = 'none';
			}
		}		
}

//-----------------------
//FUNCTION: replaceAll
//DESCRIPTION: A function to replace all occurences of a given string
//ARGUMENTS: 
//RETURN: None
//--------------------
function replaceAll(Source,stringToFind,stringToReplace){

	  var temp = Source;

	    var index = temp.indexOf(stringToFind);

	        while(index != -1){

	            temp = temp.replace(stringToFind,stringToReplace);

	            index = temp.indexOf(stringToFind);

	        }

	        return temp;

	}
//-->