// Bibliothèque de scripts - Chambre de Commerce et d'Industrie - Le Puy-en-Velay / Yssingeaux

// This is variable for storing callback function
var ae_cb = null;
 
// this is a simple function-shortcut
// to avoid using lengthy document.getElementById
function ae$(a) { return document.getElementById(a); }
 
// This is a main ae_prompt function
// it saves function callback 
// and sets up dialog
function ae_prompt(cb, q, a) {
	ae_cb = cb;
	ae$('aep_prompt').innerHTML = q;
	ae$('aep_text').value = a;
	ae$('aep_ovrl').style.display = ae$('aep_ww').style.display = '';
	ae$('aep_text').focus();
	ae$('aep_text').select();
}
 
// This function is called when user presses OK(m=0) or Cancel(m=1) button
// in the dialog. You should not call this function directly.
function ae_clk(m) {
	// hide dialog layers 
	ae$('aep_ovrl').style.display = ae$('aep_ww').style.display = 'none';
	if (!m)  
		ae_cb(null);  // user pressed cancel, call callback with null
	else
		ae_cb(ae$('aep_text').value); // user pressed OK 
}

function hw2(n)
	{
	if (n != null)
		{
		document.formPaImmo.strId.value=n;
		document.formPaImmo.submit();
		}
	}


var arrImg = new Array();
//****************************************************************
// Affichage des sous-menus 
//****************************************************************

sfHover = function() {
	if (document.getElementById('navigation'))
		{
		var sfEls = document.getElementById('navigation').getElementsByTagName("LI");
		for (var i=0; i<sfEls.length; i++) {
			sfEls[i].onmouseover=function() {
				this.className+=" sfhover";
			}
			sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
			}
		}
		}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);


//****************************************************************
// Affichage du filtre FORMATION INDEX 
//****************************************************************

sfHover = function() {
	if (document.getElementById('indexsearch'))
		{
		var sfEls = document.getElementById('indexsearch').getElementsByTagName("LI");
		for (var i=0; i<sfEls.length; i++) {
			sfEls[i].onmouseover=function() {
				this.className+=" sfhover";
			}
			sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
			}
		}
		}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);


//****************************************************************
// Surbrillance des lignes de tableaux #RESULTATS 
//****************************************************************

sfHover = function() {
	if (document.getElementById("resultats")) {
		var sfEls = document.getElementById("resultats").getElementsByTagName("TR");
		for (var i=0; i<sfEls.length; i++) {
			sfEls[i].onmouseover=function() {
				this.className+=" sfhover";
			}
			sfEls[i].onmouseout=function() {
				this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
			}
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);


//****************************************************************
// Surbrillance des lignes de tableaux #ANNONCES 
//****************************************************************

sfHover = function() {
	if (document.getElementById("annonces")) {
		var sfEls = document.getElementById("annonces").getElementsByTagName("TR");
		for (var i=0; i<sfEls.length; i++) {
			sfEls[i].onmouseover=function() {
				this.className+=" sfhover";
			}
			sfEls[i].onmouseout=function() {
				this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
			}
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);


//****************************************************************
// Couleur de fonds des lignes du tableau #CALENDAR
//****************************************************************

sfHover = function() {
	if (document.getElementById("calendar"))
		{
		var sfEls = document.getElementById("calendar").getElementsByTagName("TR");
		for (var i=0; i<sfEls.length; i++)
			{
			sfEls[i].onmouseover=function()
				{
				this.className+=" sfhover";
				}
			sfEls[i].onmouseout=function()
				{
				this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
				}
			}
		}
	}
if (window.attachEvent) window.attachEvent("onload", sfHover);



//-------------------------------------------------------------------------------
// Fonction de suppression d'une image
//-------------------------------------------------------------------------------
function funcImgDelete(varImg, varImgName, varId)
	{
	varImg.src = "../images/_charte/NoFile2.gif";
	varImg.alt = "";
	varImg.title = "Photo";
	if (varImgName != '')
		{
		var oXmlHttp = null;
		// Mozilla
		if (window.XMLHttpRequest)
			{
			oXmlHttp = new XMLHttpRequest();
			}
		// IE
		else if (window.ActiveXObject)
			{
			oXmlHttp = new ActiveXObject("Microsoft.XmlHttp");
			}
			
		if (oXmlHttp != null)
			{
			oXmlHttp.open("GET","../_inc/XML_delImg.asp?ms="+ new Date().getTime() +"&strId=" + varId + "&strImg=" + varImgName, false);
			oXmlHttp.setRequestHeader('Content-Type','text/html; charset=iso-8859-1')
			oXmlHttp.setRequestHeader("Cache-Control","no-cache");
			oXmlHttp.setRequestHeader('Pragma','no-cache');
			oXmlHttp.send(null);
			if (oXmlHttp.responseText != '')
				alert(oXmlHttp.responseText);
			}
		}
			


	
//	varImg.outerHTML = '<img alt="' + varImg.alt + '" id="' + varImg.id + '" title="Photo" src="../images/_charte/NoFile2.gif" onLoad="funcRedimImg(this,200,200);">';
//	alert(varImg.outerHTML);
	}

//-------------------------------------------------------------------------------
// Fonction de chargement d'une image
//-------------------------------------------------------------------------------
function funcImgChange(varImg, varSrc)
	{
//	var reg=new RegExp('\\', 'g');
	var tempSrc = 'file:///' + varSrc.replace(/\\/gi,'/');

	if (document.getElementById(varImg))
		{
//		document.getElementById(varImg).src = '';
//alert(tempSrc);
		document.getElementById(varImg).src = tempSrc;
//		document.getElementById(varImg).src = 'file:///' + varSrc.replace(reg, '/'); //+ '?' + (new Date()).getTime();		
//		document.getElementById(varImg).src = 'd:/documents and settings/serge/mes images/ebay/VRX928RVD/DSCN6240.jpg';
		}
//	varImg.outerHTML = '<img alt="' + varImg.alt + '" id="' + varImg.id + '" title="Photo" src="' + rewriteLocalURL(varSrc) + '" onLoad="funcRedimImg(this,200,200);">';
//	varImg.src = varSrc;
//	alert('ImgCh//ange : ' +varImg.outerHTML);
	}

//-------------------------------------------------------------------------------
//	Fonction de redimensionnement des images
//-------------------------------------------------------------------------------
function funcRedimImg(varImg, varWidth, varHeight)
	{
	var oldWidth = varImg.width;
	var oldHeight = varImg.height;
	arrImg[varImg.alt] = varImg.width + 'X' + varImg.height;
//	alert(varImg.alt + ' - ' + arrImg[varImg.alt]);
//	alert(varImg.width + ' X ' + varImg.height);
	var newWidth = oldWidth;
	var newHeight = oldHeight;
	if (varImg.width > varWidth)
		{
		newWidth = varWidth;
		newHeight = Math.round((varWidth/oldWidth) * oldHeight);
		varImg.width = newWidth;
		varImg.height = newHeight;
		oldWidth = newWidth;
		oldHeight = newHeight;
		}
	if (newHeight > varHeight)
		{
		newHeight = varHeight;
		newWidth = Math.round((varHeight/oldHeight) * newWidth);
		varImg.width = newWidth;
		varImg.height = newHeight;
		}
	}

//-------------------------------------------------------------------------------
//	Fonction de suppression d'une image en base
//-------------------------------------------------------------------------------
function funcImgDelToDb(varTable, varSelectField, varSelectValue, varFileField, varBlnMsg, varReload)
	{
//	alert('ok');
	var	strMessage = "<h4><center><font color=#FF0000>La suppression de l'image est en cours</font></center></h4>"
					+'<div align="center"><img src="../images/progression.gif"></div><br><br>';
	var strHtmlCode = 	 "<html>"
 						+"<body leftmargin=3 topmargin=3 marginwidth=3 marginheight=3 onBlur='focus();'>"
						+"<div align='left'><font color='#000000' style='font-family:Verdana, sans-serif;font-size:11;'>" + strMessage + "&nbsp;&nbsp;</font></div>"
						+ "</div>"
						+"</body></html>";
	WinPopupImgDel = window.open ("../_inc/funcImgUpload.asp?strAction=delete&strTable=" + varTable + "&strSelectField=" + varSelectField + "&strSelectValue=" + varSelectValue + "&strFileField=" + varFileField, "popupImgDel", "height=800, width=450, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, center=yes, directories=no, status=no, modal=1");
//	WinPopupEnreg.document.write(strHtmlCode);
//	WinPopupEnreg.document.close();

//	WinPopupEnreg.moveTo(Math.round((screen.width-450)/2), Math.round((screen.height-300)/2));


					
//	var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
//	xmlhttp.open("POST","../_inc/funcImgUpload.asp?strAction=delete&strTable="+ varTable +"&strSelectField="+ varSelectField +"&strSelectValue="+ varSelectValue +"&strFileField="+ varFileField, false);
//	xmlhttp.send();
	}

	
//-------------------------------------------------------------------------------
//	Fonction de lancement d'un upload d'image en base
//-------------------------------------------------------------------------------
function funcImgToDb(varPath, varTable, varSelectField, varSelectValue, varFileField, varBlnMsg, varReload)
	{
   // create ADO-stream Object
   var ado_stream = new ActiveXObject("ADODB.Stream");

   // create XML document with default header and primary node
   var xml_dom = new ActiveXObject("MSXML2.DOMDocument");
   xml_dom.loadXML('<?xml version="1.0" ?> <root/>');
   // specify namespaces datatypes
   xml_dom.documentElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com:datatypes");

   // create a new node and set binary content
   var l_node1 = xml_dom.createElement("file1");
   l_node1.dataType = "bin.base64";
	if (varPath != "")
	   	{
		// open stream object and read source file
		ado_stream.Type = 1;  // 1=adTypeBinary 
		ado_stream.Open(); 
		ado_stream.LoadFromFile(varPath);
	
	   // store file content into XML node
	   l_node1.nodeTypedValue = ado_stream.Read(-1); // -1=adReadAll
	   ado_stream.Close();
	   xml_dom.documentElement.appendChild(l_node1);
		// send XML document to SysWeb server
		var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		xmlhttp.open("POST","../_inc/funcImgUpload.asp?strAction=upload&strTable="+ varTable +"&strSelectField="+ varSelectField +"&strSelectValue="+ varSelectValue +"&strFileField=" + varFileField + "&strPath=" + varPath, false);
		xmlhttp.send(xml_dom);
		}
	else
		{
		// send XML document to SysWeb server
		var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		xmlhttp.open("POST","../_inc/funcImgUpload.asp?strAction=delete&strTable="+ varTable +"&strSelectField="+ varSelectField +"&strSelectValue="+ varSelectValue +"&strFileField="+ varFileField, false);
		xmlhttp.send(xml_dom);
		}
	
   // show server message in message-area
	if (varBlnMsg)
		alert(xmlhttp.responseText);

	if (varReload == 1)
		window.location.href = window.location.href;
}

function rewriteLocalURL(url) { 
                var uri = url; 
				var spaceMatch = / /gi; 
		        var slashMatch = /\\/gi;
                /* identify Windows-Scheme */ 
                if ((uri.charAt(1) == ':') && (uri.charAt(2) == '\\'))
					{ 
                    uri = 'file:///' + uri.replace(slashMatch, "/");
					uri = uri.replace(spaceMatch, "%20"); 

//replace(/\\+/, '/'); 
	                } 
                /* identify Linux-Scheme */ 
                else if (uri.charAt(0) == '/') { 
                        uri = 'file:///' + uri; 
                } 
                return uri; 
        } 



//****************************************************************
// Affichage du PANIER / des compléments AGENDA
//****************************************************************
function funcDisplayDiv(varObj)
	{
	if (varObj)
		{
		if (varObj.style.display == "none")
			{
			varObj.style.display = "block";
			}
		else if (varObj.style.display == "block")
			{
			varObj.style.display = "none";
			}
		else
			{
			varObj.style.display = "block";
			}
		}
	}


//****************************************************************
// Fonction d'import des animations Flash 
//****************************************************************

function RunFlash(swf, hauteur, largeur, nom) {
	document.write("<object width=\""+largeur+"\" height=\""+hauteur+"\" id=\""+nom+"\" classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0\">\n");
	document.write("<param name=\"quality\" value=\"high\">\n");
	document.write("<param name=\"wmode\" value=\"transparent\">\n");
	document.write("<param name=\"movie\" value=\""+swf+"\">\n");
	document.write("<embed src=\""+swf+"\" quality=\"high\" wmode=\"transparent\" width=\""+largeur+"\" height=\""+hauteur+"\" name=\""+nom+"\" align=\"middle\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\">\n");
	document.write("</object>\n");
}


function funcAffRSS(varRssId)
	{
	alert(varRssId);
	var oXmlHttp = false;
	// IE
	try
		{
		oXmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
	catch (e)
		{
		try
			{
			oXmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (E)
			{
			oXmlHttp = false;
			}
		}
	
	// Mozilla
	if (!oXmlHttp && typeof XMLHttpRequest != 'undefined')
		{
		try
			{
			oXmlHttp = new XMLHttpRequest();
			}
		catch(e)
			{
			oXmlHttp = false;
			}
		}
		
	if (oXmlHttp) {
		oXmlHttp.open("GET","../_inc/XML_funcRSSDetail.asp?strId=" + varRssId, false);
		oXmlHttp.setRequestHeader('Content-Type','text/html; charset=iso-8859-1')				
		oXmlHttp.send(null);
alert(oXmlHttp.responseText);
		return eval(oXmlHttp.responseText);
		}
	}



//****************************************************************
// Affichage des POPUP  
//****************************************************************

function funcPopup(varUrl, varWidth, varHeight, varResize)
	{
	var ScreenW = screen.availWidth;
	var ScreenH = screen.availHeight;
	var WinPopUp = window.open(varUrl, 'popup', "toolbar=no, menubar=no, scrollbars=yes, resizable=" + varResize + ", location=no, directories=no, status=no, width=" + varWidth + ", height=" + varHeight);
	WinPopUp.moveTo(Math.round((ScreenW-varWidth)/2), Math.round((ScreenH-varHeight)/2));	
	}

function funcPopupPlan(varUrl)
	{
	WinPopUpPlan = window.open (varUrl, "PopUpPlan", "height=560, width=550, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no");
	WinPopUpPlan.focus();
	var ScreenW = screen.availWidth;
	var ScreenH = screen.availHeight;
	WinPopUpPlan.moveTo((ScreenW-600)/2, (ScreenH-600)/2);
	}


//****************************************************************
// Gestion de l'affichage du contenu du title d'un lien
//****************************************************************

var niceTitleDummy;
var varTxt = "";

function initNiceTitle()
	{
	niceTitleDummy = document.createElement("div");
	niceTitleDummy.style.position = "absolute";
	niceTitleDummy.className = "nicetitle";
	niceTitleDummy.style.left = "-1000em";
	niceTitleDummy.style.top = "-1000em";
	document.body.appendChild(niceTitleDummy);
	}

function hideNiceTitle(e)
	{
	if (e)
		{
		if (getEventTarget(e))
			{
			if (getNodeWithTag(getEventTarget(e), "a"))
				{
				var lnk = getNodeWithTag(getEventTarget(e), "a");
				if (varTxt != "")
					{
					lnk.setAttribute("title",varTxt);
					varTxt = "";
					}
				niceTitleDummy.style.left = "-1000em";
				niceTitleDummy.style.top = "-1000em";
				niceTitleDummy.innerHTML = "";
				}
			}
		}
	}

function showNiceTitle(e) {
	if (getNodeWithTag(getEventTarget(e), "a"))
		{	
		var lnk = getNodeWithTag(getEventTarget(e), "a");
		var spanEls = lnk.getElementsByTagName("SPAN");
		for (var i=0; i<spanEls.length; i++)
			{
			spanEls[i].setAttribute("title","");
			}
		if (window.event)
			{
			x = window.event.clientX+document.body.scrollLeft; 
			y = window.event.clientY+document.body.scrollTop;
			}
		else
			{
			x = e.clientX + window.scrollX; 
			y = e.clientY + window.scrollY;
			}
	
		if (varTxt == "")
			{
			varTxt = lnk.getAttribute("title");
			lnk.setAttribute("title","");
			}
	
		niceTitleDummy.style.left = x+7;
		niceTitleDummy.style.top = y+18;
		niceTitleDummy.innerHTML = "<img src='../images/nt_img.gif'><br><div>"+ varTxt +"</div>";
		}
	}

function makeNiceTitles()
	{
	for (var i=0; i<document.links.length; i++)
		{
		var lnk = document.links[i];
		if (lnk.title)
			{
			addEvent(lnk, "mousemove", showNiceTitle);
			addEvent(lnk, "mouseout", hideNiceTitle);
			}
		}
	initNiceTitle();
	}

function addEvent(obj, evType, fn){
	if (obj.attachEvent){
		return obj.attachEvent("on" + evType, fn);
	} else if (obj.addEventListener){
		obj.addEventListener(evType, fn, true);
		return true;
	} else {
		return false;
	}
}

function getEventTarget(event)
{
	if (window.event && window.event.srcElement)
		return window.event.srcElement;
	else if (event && event.target)
		return event.target;
	return null;
}

function getNodeWithTag(el, tag) {
	// toLowerCase is for Gecko bug, supposed to be uppercase
	tag = tag.toLowerCase();
	while ((el != null) && ((el.nodeType != 1) || (el.tagName.toLowerCase() != tag)))
		el = el.parentNode;
	return el;
}

function funcNiceTitlesAndAltOnload()
	{
	makeNiceTitles();
	makeNiceAlts();
	}
//window.onload = makeNiceTitles;


//****************************************************************
// Gestion de l'affichage du contenu du alt d'une image
//****************************************************************

var niceAltDummy;
var varAltTxt = "";

function initNiceAlt()
	{
	niceAltDummy = document.createElement("div");
	niceAltDummy.style.position = "absolute";
	niceAltDummy.className = "nicetitle";
	niceAltDummy.style.left = "-1000em";
	niceAltDummy.style.top = "-1000em";
	document.body.appendChild(niceAltDummy);
	}

function hideNiceAlt(e) {
	if (getEventTarget(e))
		if (getNodeWithTag(getEventTarget(e), "img"))
			{
			var img = getNodeWithTag(getEventTarget(e), "img");
			if (varAltTxt != "")
				{
				img.setAttribute("alt",varAltTxt);
				varAltTxt = "";
				}
			niceAltDummy.style.left = "-1000em";
			niceAltDummy.style.top = "-1000em";
			niceAltDummy.innerHTML = "";
			}
	}

function showNiceAlt(e) {
	if (getEventTarget(e))
		if (getNodeWithTag(getEventTarget(e), "img"))
			{
			var img = getNodeWithTag(getEventTarget(e), "img");
			if (window.event)
				{
				x = window.event.clientX+document.body.scrollLeft; 
				y = window.event.clientY+document.body.scrollTop;
				}
			else
				{
				x = e.clientX + window.scrollX; 
				y = e.clientY + window.scrollY;
				}
		
			if (varAltTxt == "")
				{
				varAltTxt = img.getAttribute("alt");
				img.setAttribute("alt","");
				}
		
			niceAltDummy.style.left = x+7;
			niceAltDummy.style.top = y+18;
			niceAltDummy.innerHTML = "<img src='../images/nt_img.gif'><br><div>"+ varAltTxt +"</div>";
			}
	}

function makeNiceAlts()
	{
	for (var i=0; i<document.images.length; i++)
		{
		var img = document.images[i];
		if (img.alt)
			{
			addEvent(img, "mousemove", showNiceAlt);
			addEvent(img, "mouseout", hideNiceAlt);
			}
		}
	initNiceAlt();
	}

function addEvent(obj, evType, fn){
	if (obj.attachEvent){
		return obj.attachEvent("on" + evType, fn);
	} else if (obj.addEventListener){
		obj.addEventListener(evType, fn, true);
		return true;
	} else {
		return false;
	}
}

function getEventTarget(event)
{
	if (window.event && window.event.srcElement)
		return window.event.srcElement;
	else if (event && event.target)
		return event.target;
	return null;
}

function getNodeWithTag(el, tag) {
	// toLowerCase is for Gecko bug, supposed to be uppercase
	tag = tag.toLowerCase();
	while ((el != null) && ((el.nodeType != 1) || (el.tagName.toLowerCase() != tag)))
		el = el.parentNode;
	return el;
}

//****************************************************************
// Gestion des INPUT d'authentification
//****************************************************************
function funcChgElt(varElt)
	{
	var oElt;
	try
		{
		oElt = document.createElement('<input type="password" name="RmContactPwd" id="RmContactPwd" value="">');
		varElt.insertAdjacentElement("BeforeBegin",oElt);
		varElt.outerHTML = '';
		oElt.focus();
		oElt.select();
		}
	catch(e) {}
	if (!oElt || !oElt.name)
		{
		varElt.type = 'password';				
		varElt.value = '';
		}
	}


window.onload = funcNiceTitlesAndAltOnload; 


