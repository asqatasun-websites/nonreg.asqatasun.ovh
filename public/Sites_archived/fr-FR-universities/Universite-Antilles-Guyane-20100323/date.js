var now = new Date();

var dayName = now.getDayName(false);
var monthName = now.getMonthName(false);
var day = now.getDate();
var year = now.getFullYear();

// Inject the date when the body is loaded in the element of id 'day'
$(function()
{
	$('#day').text(dayName + ' ' + day + ' ' + monthName.toLowerCase() + ' ' + year);
});

// When called, inject the month into the given element
function displayMonth($elt)
{
	var initial = monthName.charAt(0).toUpperCase();
	var month = initial + monthName.substring(1);
	$elt.text(month + ' ' + year);
}