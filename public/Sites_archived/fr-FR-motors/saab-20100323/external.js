if (typeof (com) == 'undefined') var com = new Object ();
if (typeof (com.saab) == 'undefined') com.saab = new Object ();
com.saab.external = new Object ();
com.saab.external.instances = new Object ();

com.saab.external.createInstance = function (name) {
	com.saab.external.instances [name] = new com.saab.external.Instance ();
}

com.saab.external.Instance = function () {
	var _iframe = null;
	var _properties = new Object ();
	
	/**
	*	Draw interface
	*/

	this.draw = function () {
		_iframe = document.createElement ('iframe');
		_iframe.setAttribute ('src', _properties ['src']);
		_iframe.setAttribute ('frameBorder', '0');

    /* JED - I.E. seems to have problems with the scrolling
       parameter being set AFTER the initial element creation.
       Only solution seems to be to set the correct value here
       rather than later on. */
       
    /* For some reason leaving the "usescroller" metadata tag
       in SiteVision blank makes it return 'no'. To get round this
       we use boolean 0/1 to set no/yes for scrolling and any other
       value defaults to auto */
		switch (_properties ['usescroller'] ) {
			case '0': // no
				_iframe.setAttribute ('scrolling', 'no');
				break;
			case '1': // yes
				_iframe.setAttribute ('scrolling', 'yes');
				break;
			default: // auto ( anything else )
				_iframe.setAttribute ('scrolling', 'auto');
				break;
		}

		document.getElementsByTagName ('body') [0].appendChild (_iframe);
		_iframe.style.position = 'absolute';
		_iframe.style.zIndex = '2';
		_iframe.style.border = '0px';
		_iframe.style.margin = '0px';
		_iframe.style.padding = '0px';
		_iframe.style.overflow = 'auto';
		for (var prop in _properties) {
			this.setProperty (prop, _properties [prop], true);
		}
	}

	/**
	*	Kill kill kill
	*/

	this.kill = function () {
		_iframe.parentNode.removeChild (_iframe);
	}

	/**
	*	Properties
	*/

	this.setProperty = function (property, value, forced) {
		
		if( !forced && _properties[property] == value )
			return;
		
		_properties[property] = value;

		if (_iframe != null) {
			switch (property) {
				case 'src':
					_iframe.src = value;
					break;
				case 'x':
					_iframe.style.left = Math.round (value) + 'px';
					break;
				case 'y':
					_iframe.style.top = Math.round (value) + 'px';
					break;
				case 'width':
					_iframe.style.width = Math.ceil (value) + 'px';
					break;
				case 'height':
					_iframe.style.height = Math.ceil (value) + 'px';
					break;
                case 'usescroller':
                    if( value == '0' ) _iframe.style.overflow = 'hidden'; // no
                    else if( value == '1' ) _iframe.style.overflow = 'scroll'; // yes
                    else _iframe.style.overflow = 'auto'; // auto (anything else)
                    break;
				case 'clip':
					if (parseInt (value) > 0) {
						/*
						_iframe.style.top = (Math.round (_properties ['y']) + Math.round (value)) + 'px';
						_iframe.style.height = (Math.round (_properties ['height']) - Math.round (value)) + 'px';
						*/
						//_iframe.style.top = '-' + Math.round (value) + 'px';
						_iframe.style.clip = 'rect(' + Math.floor (value) + 'px auto auto auto)';
						if( navigator.userAgent.indexOf ('Windows') > -1 && navigator.userAgent.indexOf ('Mozilla') > -1) {
							//_iframe.style.display = 'none'; // seems to force the contained flash to restart...
							_iframe.style.visibility = 'hidden';
						}
					} else {
						/*
						_iframe.style.top = Math.round (_properties ['y']) + 'px';
						_iframe.style.height = Math.round (_properties ['height']) + 'px';
						*/
						//_iframe.style.top = '0px';
						_iframe.style.clip = 'rect(auto auto auto auto)';
						_iframe.style.display = 'block';
						_iframe.style.visibility = 'visible';
					}
					break;
			}
		}
	}
	
}