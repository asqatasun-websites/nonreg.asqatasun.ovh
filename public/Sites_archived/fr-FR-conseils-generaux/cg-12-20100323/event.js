// event.js
// ARPEGE 2006 - LB
// Cr�ation de demandes de documents d'Etat Civil � destination
// d'ActesWeb.
// Gestion des evenements

var oldperenom = '' ;
var oldpereprenom = '' ;

function managepere(ctrl)
{
	var oForm = document.getElementById('formDemande');
	
	// recupere l'etat de la checkbox checkpereinconnu
	// Si coch� -> vider contenu champ,disable champs
	// Si non coch� -> enable champs
	
	if (!oForm.checkpereinconnu.checked)
	{
		oForm.NomPere.disabled = false ;
		oForm.PrenomsPere.disabled =  false;
		oForm.NomPere.value = oldperenom ;
		oForm.PrenomsPere.value = oldpereprenom ;
	}
	else
	{
		oForm.NomPere.disabled = true ;
		oForm.PrenomsPere.disabled = true ;
		oldperenom = oForm.NomPere.value ;
		oldpereprenom = oForm.PrenomsPere.value ;
		oForm.NomPere.value = '' ;
		oForm.PrenomsPere.value = '' ;
	}
}


var oldmerenom = '' ;
var oldmereprenom = '' ;
function managemere(ctrl)
{
	var oForm = document.getElementById('formDemande');
	// recumere l'etat de la checkbox checkmereinconnu
	// Si coch� -> vider contenu champ,disable champs
	// Si non coch� -> enable champs
	
	if (!oForm.checkmereinconnu.checked)
	{
		oForm.NomMere.disabled = false ;
		oForm.PrenomsMere.disabled = false ;
		oForm.NomMere.value = oldmerenom ;
		oForm.PrenomsMere.value = oldmereprenom ;
	}
	else
	{
		oForm.NomMere.disabled = true ;
		oForm.PrenomsMere.disabled = true ;
		oldmerenom = oForm.NomMere.value ;
		oldmereprenom = oForm.PrenomsMere.value ;
		oForm.NomMere.value = '' ;
		oForm.PrenomsMere.value = '' ;
	}
}


var oldpereconjointnom = '' ;
var oldpereconjointprenom = '' ;
function managepereconjoint(ctrl)
{
	var oForm = document.getElementById('formDemande');
	// recupere l'etat de la checkbox checkpereinconnu
	// Si coch� -> vider contenu champ,disable champs
	// Si non coch� -> enable champs
	if (!oForm.checkpereconjointinconnu.checked)
	{
		oForm.NomPereConjoint.disabled = false ;
		oForm.PrenomsPereConjoint.disabled = false ;
		oForm.NomPereConjoint.value = oldpereconjointnom ;
		oForm.PrenomsPereConjoint.value = oldpereconjointprenom ;
	}
	else
	{
		oForm.NomPereConjoint.disabled = true ;
		oForm.PrenomsPereConjoint.disabled = true ;
		oldpereconjointnom = oForm.NomPereConjoint.value ;
		oldpereconjointprenom = oForm.PrenomsPereConjoint.value ;
		oForm.NomPereConjoint.value = '' ;
		oForm.PrenomsPereConjoint.value = '' ;
	}
}


var oldmereconjointnom = '' ;
var oldmereconjointprenom = '' ;
function managemereconjoint(ctrl)
{
	var oForm = document.getElementById('formDemande');
	// recumere l'etat de la checkbox checkmereinconnu
	// Si coch� -> vider contenu champ,disable champs
	// Si non coch� -> enable champs
	if (!oForm.checkmereconjointinconnu.checked)
	{
		oForm.NomMereConjoint.disabled = false ;
		oForm.PrenomsMereConjoint.disabled = false ;
		oForm.NomMereConjoint.value = oldmereconjointnom ;
		oForm.PrenomsMereConjoint.value = oldmereconjointprenom ;
	}
	else
	{
		oForm.NomMereConjoint.disabled = true ;
		oForm.PrenomsMereConjoint.disabled = true ;
		oldmereconjointnom = oForm.NomMereConjoint.value ;
		oldmereconjointprenom = oForm.PrenomsMereConjoint.value ;
		oForm.NomMereConjoint.value = '' ;
		oForm.PrenomsMereConjoint.value = '' ;
	}
}