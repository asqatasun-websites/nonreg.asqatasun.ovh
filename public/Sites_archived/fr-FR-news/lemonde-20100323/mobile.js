function detectDevice(user_agent)
{
   if (!mobile_base_url)
   {
      mobile_base_url = 'http://mobile.lemonde.fr'
   }

   var url_mobile = mobile_base_url+document.location.pathname;
   var html_1 = 'Visitez notre site optimis&eacute; pour ';
   var html_2 = ' <a href="'+url_mobile+'">mobile.lemonde.fr</a>';

   // Liste des supports mobiles
   var device = new Array(
                  "Android",
                  "Blackberry",
                  "Blazer",
                  "Handspring",
                  "iPhone",
                  "iPod",
                  "Kyocera",
                  "LG",
                  "Motorola",
                  "Nokia",
                  "Palm",
                  "PlayStation Portable",
                  "Samsung",
                  "Smartphone",
                  "SonyEricsson",
                  "Symbian",
                  "WAP",
                  "Windows CE"
                );

   var max_device = device.length;

   for(i=0 ; i<max_device ; i++)
   {
      if(user_agent.indexOf(device[i]) != -1)
      {
         if (device[i] == "iPhone")
         {
            document.getElementById('annonce_site_mobile').innerHTML = html_1 + 'l\'Iphone' + html_2;
            document.getElementById('annonce_site_mobile').style.display = 'block';
         }
         else if (device[i] == "iPod")
         {
            document.getElementById('annonce_site_mobile').innerHTML = html_1 + 'l\'iPod Touch' + html_2;
            document.getElementById('annonce_site_mobile').style.display = 'block';
         }
         else if (device[i] == "Android")
         {
            document.getElementById('annonce_site_mobile').innerHTML = html_1 + 'Android' + html_2;
            document.getElementById('annonce_site_mobile').style.display = 'block';
         }
         else
         {
            document.location = url_mobile;
         }
         return;
      }
   }
}
