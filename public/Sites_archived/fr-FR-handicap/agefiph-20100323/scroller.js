﻿Element.Events.extend({
	"wheelup": {
		type: Element.Events.mousewheel.type,
		map: function(event){
			event = new Event(event);
			if (event.wheel >= 0) this.fireEvent("wheelup", event)
		}
	},
 	"wheeldown": {
		type: Element.Events.mousewheel.type,
		map: function(event){
			event = new Event(event);
			if (event.wheel <= 0) this.fireEvent("wheeldown", event)
		}
	}
});
//
function initSmScroller() {
	var lsScroller = $$(".smScrollContent");
	if (lsScroller.length == 0) {
		return;
	};
	lsScroller.each(function(scrollElement){
		var barElelement = scrollElement.getNext();
		if (!barElelement || !barElelement.hasClass("smScroller")) {
			return;
		};
		// init variables
		var upBtn = barElelement.getChildren()[0].getFirst();
		var scrollBar = barElelement.getChildren()[1];
		var scrollBtn = scrollBar.getFirst();
		var dnBtn = barElelement.getChildren()[2].getFirst();
		// create slider
		scrollBar.scrollInterval = null;
		scrollBar.scrollContent = scrollElement;
		scrollBar.maxScroll = scrollElement.getSize().scrollSize.y-scrollElement.getSize().size.y;
		if (scrollBar.maxScroll <= 0) {
			barElelement.setOpacity(0);
		}
		scrollBar.slideFx = new Slider(scrollBar, scrollBtn, {
			onChange: function(pos) {
				scrollBar.scrollContent.scrollTo(0, scrollBar.maxScroll*pos/100);
				scrollBar.slideFx.curPos = pos;
			},
			mode: "vertical"
		});
		scrollBar.slideFx.curPos = 0;
		// wheel control
		scrollElement.addEvents({
			"wheelup": function(e) {
				new Event(e).stop();
				//
				var el = $(this);
				el.scrollTo(0, el.getSize().scroll.y-20);
				// 
				var scrollPos = Math.floor(el.getSize().scroll.y/el.scrollBar.maxScroll*100);
				el.scrollBar.slideFx.set(scrollPos);
			},
			"wheeldown": function(e) {
				new Event(e).stop();
				//
				var el = $(this);
				el.scrollTo(0, el.getSize().scroll.y+20);
				// 
				var scrollPos = Math.floor(el.getSize().scroll.y/el.scrollBar.maxScroll*100);
				el.scrollBar.slideFx.set(scrollPos);
			}
		});
		// button control
		upBtn.addEvents({
			"mousedown": function(e) {
				new Event(e).stop();
				var el = $(this);
				clearInterval(el.scrollBar.scrollInterval);
				el.scrollBar.scrollInterval = setInterval(function () {
					fMoveSlider(el.scrollBar.slideFx, -5);
				}, 40);
			},
			"mouseup": function(e) {
				new Event(e).stop();
				var el = $(this);
				clearInterval(el.scrollBar.scrollInterval);
			},
			"click": function(e) {
				new Event(e).stop();
				var el = $(this);
				clearInterval(el.scrollBar.scrollInterval);
			},
			"mouseout": function(e) {
				new Event(e).stop();
				var el = $(this);
				clearInterval(el.scrollBar.scrollInterval);
			}
		});
		upBtn.scrollBar = scrollBar;
		//
		dnBtn.addEvents({
			"mousedown": function(e) {
				new Event(e).stop();
				var el = $(this);
				clearInterval(el.scrollBar.scrollInterval);
				el.scrollBar.scrollInterval = setInterval(function () {
					fMoveSlider(el.scrollBar.slideFx, 5);
				}, 40);
			},
			"mouseup": function(e) {
				new Event(e).stop();
				var el = $(this);
				clearInterval(el.scrollBar.scrollInterval);
			},
			"click": function(e) {
				new Event(e).stop();
				var el = $(this);
				clearInterval(el.scrollBar.scrollInterval);
			},
			"mouseout": function(e) {
				new Event(e).stop();
				var el = $(this);
				clearInterval(el.scrollBar.scrollInterval);
			}
		});
		dnBtn.scrollBar = scrollBar;
		// init scroll content
		scrollElement.scrollTo(0, 0);
		scrollElement.scrollBar = scrollBar;
	});
	//
	function fMoveSlider(slider, dPos){
		if ((slider.curPos == 0) && (dPos<0)) {
			return;
		};
		if ((slider.curPos == 100) && (dPos>0)) {
			return;
		};
		slider.set(slider.curPos+dPos);
	};
}
//
window.addEvent("load", function(){
	//initSmScroller();
});