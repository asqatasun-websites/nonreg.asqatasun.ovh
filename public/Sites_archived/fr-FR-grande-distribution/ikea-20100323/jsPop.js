/*************************************
Generic JavaScript functions for opening popup windows
Created by Lars Henrik Rotnes
Last modified: 2002-10-01
*************************************/

/*
CVS Version : $Id: jsPop.js,v 1.6 2003/02/26 17:37:41 lhr Exp $

*/


/**
*Function to open a pop up window
*
* calls openPopUpWindow and discards the returned value. This is so the function openPopup can
* be used in the href attribute as href="javascript:openPopUp(...)".
* Note that this use is deprecated, but widespread all over the application. See
* the popupLink function below for a better alternative...!
* 
* argUrl   string 	= the location of the file to open in the popup window
* argWidth string 	= the width of the window
* argHeight string = the height of the window
* argTitle string 	= the title of the window
* argScroll int  	= specify if we need scrollbars, 1 = yes, 0 = no 
*/
var is = new Object()
is.ie = (document.all) ? 1:0
is.ns4 = (document.layers) ? 1:0
is.w3c = (document.getElementById && !is.ie) ? 1:0
is.win = (navigator.userAgent.toLowerCase().indexOf("win") > 0) ? 1:0
is.mac = (navigator.userAgent.toLowerCase().indexOf("mac") > 0) ? 1:0

function openPopUp(argUrl, argWidth, argHeight, argTitle, argScroll ) {
	openPopUpWindow(argUrl, argWidth, argHeight, argTitle, argScroll );
}

//function for for stat generation with exception handling
function IRWdcsMultiTrack() {
	try {
		if (window.gDomain===undefined) {
			WTdom_Load=false;
		} else {
			WTdom_Load=true;
		}
		if (window.WTmain_Load === true && window.WTdom_Load === true) {
			dcsMultiTrack.apply(this,arguments);
		}
	} catch (e) {}	
}

//Function added to launch the Artificial Assistant (en_BG, en_US, de_DE)
if(self.name ==''|| self.name==undefined){
self.name='IKEA';
}

//That's the target wished name of the IKEA window to make possible that the
//Assistant opens the links in the proper window. If the window actually has
//another name, please, tell us and we will change the target in the
//Assistant's links!!!!

function popUpAnna(URL,popW,popH) {

var w = 480, h = 340;

if (document.all || document.layers) {

   w = self.screen.availWidth;
   h = self.screen.availHeight;
}

var topPos = (h-popH)-(4*h/100), leftPos = (w-popW)-(1*w/100);

day = new Date();
id = day.getTime();

   eval("page" + id + " =window.open(URL,'popup','resizable=no, toolbar=no, location=no, status=no, scrollbars=no, menubar=no, titlebar=no, width=' + popW + ',height=' + popH + ',left=' + leftPos + ',top=' + topPos + ',screenX=' + leftPos + ',screenY=' + topPos);");
   
   	// 070709 PAMN - Begin
	var path = document.getElementsByName("returnURL");
	
	if(null != path && path.length == 1) {
		path = path[0].value;
		path = path.substr(0, path.lastIndexOf('/')) + '/ask_anna.html';

		IRWdcsMultiTrack('WT.ti','Ask Anna', 'DCS.dcsuri', path);
	}
	// 070709 PAMN - End
}

//**********************************************

/**
*Function to open a pop up window
*
*argUrl   string 	= the location of the file to open in the popup window
*argWidth string 	= the width of the window
*argHeight string = the height of the window
*argTitle string 	= the internal name for the window. This should be a one-word alphabetic name and is used only internally.
*                     IE does not accept regular NLS texts, so use a symbolic name for this parameter!
*argScroll int  	= specify if we need scrollbars, 1 = yes, 0 = no 
*/
var is = new Object()
is.ie = (document.all) ? 1:0
is.ns4 = (document.layers) ? 1:0
is.w3c = (document.getElementById && !is.ie) ? 1:0
is.win = (navigator.userAgent.toLowerCase().indexOf("win") > 0) ? 1:0
is.mac = (navigator.userAgent.toLowerCase().indexOf("mac") > 0) ? 1:0

function openPopUpWindow(argUrl, argWidth, argHeight, argTitle, argScroll ) {
  //for win ie, add extra width to accomodate scrollbar on pop07
 // if(is.ie && !is.mac  && argScroll=="1" && ( argTitle == "pop07" || argTitle == "pop08") ){
  //  argWidth = parseInt(argWidth) + 26;
  //}
    
	if(argScroll=="1" && !is.mac){
		argWidth = parseInt(argWidth) + 26;
	}
	
	if(argScroll=="1" && is.mac){
		argWidth = parseInt(argWidth) + 10;
	}
    
		
	var x = (screen.availWidth - argWidth ) / 2;
	var y = (screen.availHeight - argHeight ) / 2;
  
	var sFeatures = "width=" + argWidth + "," + 
                  "height=" + argHeight + "," + 
									"toolbar=no," + 
                  "status=no," + 
									"scrollbars=" + argScroll + "," + 
									"resizable=yes," + 
                  
                  "left=" + x + ", top="+ y;
	
	var windowOpened = false;
	
	if (window.open) {
		//alert('"'+argTitle+'"');
		newWindow = window.open(argUrl, argTitle, sFeatures, false);
	
		windowOpened = newWindow?true:false;
	
		if (windowOpened && newWindow.focus) {
	
			newWindow.focus();
		}
		
	}
	
	// return false if a window was opened. This was the function can
	// be used in the onclick action handler of an HTML link
	return !windowOpened;
}



/**
* Function to open the target of a link in a new window. Usage example:
* 	<a href="http://..." target="_blank" onclick="return popupLink(this,'Window Title')">Window Title</a>
* This will use Javascript to open a new window. If the window cannot be opened
* (if Javascript is disabled, or the user has a popup blocker installed), the
* link is opened using the standard feature of the web browser. Aka "graceful degradation"...
*
* Parameters:
* 	link	The DOM object reference of the A element that is used to open the link with
*	name	The internal name for the window. This should be a one-word alphabetic name and is used only internally.
*           IE does not accept regular NLS texts, so use a symbolic name for this parameter!
*/
function popupLink(link,name) {

	return openPopUpWindow(link.href, 500, 600, name, 'yes');
}
