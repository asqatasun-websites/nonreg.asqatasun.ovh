function setPrintLinkEntete(langue) {
	if (langue == "FR") { 
		document.write("<div id=\"print\"><a title=\"Imprimer la page sous sa forme optimis&eacute;e\" href=\"javascript:window.print();\">Imprimer</a></div>");
	}
	else {
		document.write("<div id=\"print\"><a title=\"Print the page under its optimized shape\" href=\"javascript:window.print();\">Print</a></div>");
	}
}


function setPrintLinkBas(langue) {
	if (langue == "FR") {
	document.write("<a id=\"imprimer-bas\" title=\"Imprimer la page sous sa forme optimis&eacute;e\" href=\"javascript:window.print();\">Imprimer</a>");
	}
	else {document.write("<a id=\"imprimer-bas\" title=\"Print the page under its optimized shape\" href=\"javascript:window.print();\">Print</a>");
	}
}


function setPrintLinkLienSousNiveau(langue) {
	if (langue == "FR") {
	document.write("<a class=\"lien-souniveau\" href=\"javascript:self.print();\" title=\"Imprimer la page sous sa forme optimis&eacute;e\">Imprimer </a>");
	}
	else {document.write("<a class=\"lien-souniveau\" href=\"javascript:self.print();\" title=\"Print the page under its optimized shape\">Print </a>");
	}
}
