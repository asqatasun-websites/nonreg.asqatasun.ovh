
function ws_includeJavaScript(jsFile)
{
  document.write('<script type="text/javascript" src="'
    + jsFile + '"></script>'); 
}

if (document.URL.substr(0,5).toLowerCase() == 'https') {
		 ws_includeJavaScript('https://www.wysistat.com/statistique.js'); 
}
else {
		 ws_includeJavaScript('http://www.wysistat.com/statistique.js');
}
