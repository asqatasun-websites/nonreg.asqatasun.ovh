/***********************************/
/*<!-- DEFINITION DU MENU UTBM -->*/
/***********************************/

/*** 
This is the menu creation code - place it right after you body tag
Feel free to add this to a stand-alone js file and link it to your page.
**/

//Menu object creation
oCMenu=new makeCM("oCMenu") //Making the menu object. Argument: menuname

oCMenu.frames = 0

//Menu properties   
oCMenu.pxBetween=0
oCMenu.fromLeft=0 
oCMenu.fromTop=80
oCMenu.rows=0 
oCMenu.menuPlacement=0
                                                             
oCMenu.offlineRoot="" 
oCMenu.onlineRoot="" 
oCMenu.resizeCheck=1 
oCMenu.wait=50 
oCMenu.fillImg=""
oCMenu.zIndex=0

//Background bar properties
oCMenu.useBar=1
oCMenu.barWidth=""
oCMenu.barHeight="menu"
oCMenu.barClass="menu_Bar"
oCMenu.barX=0 
oCMenu.barY=0
oCMenu.barBorderX=0
oCMenu.barBorderY=0
oCMenu.barBorderClass=""

//Level properties - ALL properties have to be spesified in level 0
oCMenu.level[0]=new cm_makeLevel()//Add this for each new level
oCMenu.level[0].width=150
oCMenu.level[0].height=23 
oCMenu.level[0].regClass="menu_Level_0"
oCMenu.level[0].overClass="menu_Level_0_over"
oCMenu.level[0].borderX=0
oCMenu.level[0].borderY=0
oCMenu.level[0].borderClass="menu_Level_0_border"
oCMenu.level[0].offsetX=0
oCMenu.level[0].offsetY=1
oCMenu.level[0].rows=0
oCMenu.level[0].arrow=""
oCMenu.level[0].arrowWidth=3
oCMenu.level[0].arrowHeight=6
oCMenu.level[0].align="bottom"

//EXAMPLE SUB LEVEL[1] PROPERTIES - You have to specify the properties you want different from LEVEL[0] - If you want all items to look the same just remove this

oCMenu.level[1]=new cm_makeLevel() //Add this for each new level (adding one to the number)
oCMenu.level[1].width=oCMenu.level[0].width-2
oCMenu.level[1].height=35
oCMenu.level[1].regClass="menu_Level_1"
oCMenu.level[1].overClass="menu_Level_1_over"
oCMenu.level[1].borderX=1
oCMenu.level[1].borderY=1
oCMenu.level[1].align="right" 
oCMenu.level[1].offsetX=-1
oCMenu.level[1].offsetY=-1
oCMenu.level[1].arrow=""
oCMenu.level[1].arrowWidth=3
oCMenu.level[1].arrowHeight=6
oCMenu.level[1].borderClass="menu_Level_1_border"

/***********************************/
/*<!-- /DEFINITION DU MENU UTBM -->*/
/***********************************/

/***********************************/
/*<!-- DEFINITION DU MENU FORMATION ET ADMISSION -->*/
/***********************************/

/*** 
This is the menu creation code - place it right after you body tag
Feel free to add this to a stand-alone js file and link it to your page.
**/

//Menu object creation
oCMenu1=new makeCM("oCMenu1") //Making the menu object. Argument: menuname

oCMenu1.frames = 0

//Menu properties   
oCMenu1.pxBetween=0
oCMenu1.fromLeft=60 
oCMenu1.fromTop=80
oCMenu1.rows=0 
oCMenu1.menuPlacement=0
                                                             
oCMenu1.offlineRoot="" 
oCMenu1.onlineRoot="" 
oCMenu1.resizeCheck=1 
oCMenu1.wait=50 
oCMenu1.fillImg=""
oCMenu1.zIndex=0

//Background bar properties
oCMenu1.useBar=1
oCMenu1.barWidth=""
oCMenu1.barHeight="menu"
oCMenu1.barClass="menu_Bar"
oCMenu1.barX=0 
oCMenu1.barY=0
oCMenu1.barBorderX=0
oCMenu1.barBorderY=0
oCMenu1.barBorderClass=""

//Level properties - ALL properties have to be spesified in level 0
oCMenu1.level[0]=new cm_makeLevel()//Add this for each new level
oCMenu1.level[0].width=150
oCMenu1.level[0].height=23 
oCMenu1.level[0].regClass="menu_Level_0"
oCMenu1.level[0].overClass="menu_Level_0_over"
oCMenu1.level[0].borderX=0
oCMenu1.level[0].borderY=0
oCMenu1.level[0].borderClass="menu_Level_0_border"
oCMenu1.level[0].offsetX=-1
oCMenu1.level[0].offsetY=1
oCMenu1.level[0].rows=0
oCMenu1.level[0].arrow=""
oCMenu1.level[0].arrowWidth=3
oCMenu1.level[0].arrowHeight=6
oCMenu1.level[0].align="bottom"

//EXAMPLE SUB LEVEL[1] PROPERTIES - You have to specify the properties you want different from LEVEL[0] - If you want all items to look the same just remove this

oCMenu1.level[1]=new cm_makeLevel() //Add this for each new level (adding one to the number)
oCMenu1.level[1].width=oCMenu1.level[0].width-2
oCMenu1.level[1].height=35
oCMenu1.level[1].regClass="menu_Level_1"
oCMenu1.level[1].overClass="menu_Level_1_over"
oCMenu1.level[1].borderX=1
oCMenu1.level[1].borderY=1
oCMenu1.level[1].align="right" 
oCMenu1.level[1].offsetX=-1
oCMenu1.level[1].offsetY=-1
oCMenu1.level[1].arrow=""
oCMenu1.level[1].arrowWidth=3
oCMenu1.level[1].arrowHeight=6
oCMenu1.level[1].borderClass="menu_Level_1_border"

/***********************************/
/*<!-- /DEFINITION DU MENU FORMATION ET ADMISSION -->*/
/***********************************/

/***********************************/
/*<!-- DEFINITION DU MENU VIE ETUDIANTE -->*/
/***********************************/

/*** 
This is the menu creation code - place it right after you body tag
Feel free to add this to a stand-alone js file and link it to your page.
**/

//Menu object creation
oCMenu7=new makeCM("oCMenu7") //Making the menu object. Argument: menuname

oCMenu7.frames = 0

//Menu properties   
oCMenu7.pxBetween=0
oCMenu7.fromLeft=198 
oCMenu7.fromTop=80
oCMenu7.rows=0 
oCMenu7.menuPlacement=0
                                                             
oCMenu7.offlineRoot="" 
oCMenu7.onlineRoot="" 
oCMenu7.resizeCheck=1 
oCMenu7.wait=50 
oCMenu7.fillImg=""
oCMenu7.zIndex=0

//Background bar properties
oCMenu7.useBar=1
oCMenu7.barWidth=""
oCMenu7.barHeight="menu"
oCMenu7.barClass="menu_Bar"
oCMenu7.barX=0 
oCMenu7.barY=0
oCMenu7.barBorderX=0
oCMenu7.barBorderY=0
oCMenu7.barBorderClass=""

//Level properties - ALL properties have to be spesified in level 0
oCMenu7.level[0]=new cm_makeLevel()//Add this for each new level
oCMenu7.level[0].width=150
oCMenu7.level[0].height=23 
oCMenu7.level[0].regClass="menu_Level_0"
oCMenu7.level[0].overClass="menu_Level_0_over"
oCMenu7.level[0].borderX=0
oCMenu7.level[0].borderY=0
oCMenu7.level[0].borderClass="menu_Level_0_border"
oCMenu7.level[0].offsetX=-1
oCMenu7.level[0].offsetY=1
oCMenu7.level[0].rows=0
oCMenu7.level[0].arrow=""
oCMenu7.level[0].arrowWidth=3
oCMenu7.level[0].arrowHeight=6
oCMenu7.level[0].align="bottom"

//EXAMPLE SUB LEVEL[1] PROPERTIES - You have to specify the properties you want different from LEVEL[0] - If you want all items to look the same just remove this

oCMenu7.level[1]=new cm_makeLevel() //Add this for each new level (adding one to the number)
oCMenu7.level[1].width=oCMenu7.level[0].width-2
oCMenu7.level[1].height=35
oCMenu7.level[1].regClass="menu_Level_1"
oCMenu7.level[1].overClass="menu_Level_1_over"
oCMenu7.level[1].borderX=1
oCMenu7.level[1].borderY=1
oCMenu7.level[1].align="right" 
oCMenu7.level[1].offsetX=-1
oCMenu7.level[1].offsetY=-1
oCMenu7.level[1].arrow=""
oCMenu7.level[1].arrowWidth=3
oCMenu7.level[1].arrowHeight=6
oCMenu7.level[1].borderClass="menu_Level_1_border"

/***********************************/
/*<!-- /DEFINITION DU MENU VIE ETUDIANTE -->*/
/***********************************/

/***********************************/
/*<!-- DEFINITION DU MENU INTERNATIONAL -->*/
/***********************************/

/*** 
This is the menu creation code - place it right after you body tag
Feel free to add this to a stand-alone js file and link it to your page.
**/

//Menu object creation
oCMenu3=new makeCM("oCMenu3") //Making the menu object. Argument: menuname

oCMenu3.frames = 0

//Menu properties   
oCMenu3.pxBetween=0
oCMenu3.fromLeft=292 
oCMenu3.fromTop=80
oCMenu3.rows=0 
oCMenu3.menuPlacement=0
                                                             
oCMenu3.offlineRoot="" 
oCMenu3.onlineRoot="" 
oCMenu3.resizeCheck=1 
oCMenu3.wait=50 
oCMenu3.fillImg=""
oCMenu3.zIndex=0

//Background bar properties
oCMenu3.useBar=1
oCMenu3.barWidth=""
oCMenu3.barHeight="menu"
oCMenu3.barClass="menu_Bar"
oCMenu3.barX=0 
oCMenu3.barY=0
oCMenu3.barBorderX=0
oCMenu3.barBorderY=0
oCMenu3.barBorderClass=""

//Level properties - ALL properties have to be spesified in level 0
oCMenu3.level[0]=new cm_makeLevel()//Add this for each new level
oCMenu3.level[0].width=150
oCMenu3.level[0].height=23 
oCMenu3.level[0].regClass="menu_Level_0"
oCMenu3.level[0].overClass="menu_Level_0_over"
oCMenu3.level[0].borderX=0
oCMenu3.level[0].borderY=0
oCMenu3.level[0].borderClass="menu_Level_0_border"
oCMenu3.level[0].offsetX=-1
oCMenu3.level[0].offsetY=1
oCMenu3.level[0].rows=0
oCMenu3.level[0].arrow=""
oCMenu3.level[0].arrowWidth=3
oCMenu3.level[0].arrowHeight=6
oCMenu3.level[0].align="bottom"

//EXAMPLE SUB LEVEL[1] PROPERTIES - You have to specify the properties you want different from LEVEL[0] - If you want all items to look the same just remove this

oCMenu3.level[1]=new cm_makeLevel() //Add this for each new level (adding one to the number)
oCMenu3.level[1].width=oCMenu3.level[0].width-2
oCMenu3.level[1].height=35
oCMenu3.level[1].regClass="menu_Level_1"
oCMenu3.level[1].overClass="menu_Level_1_over"
oCMenu3.level[1].borderX=1
oCMenu3.level[1].borderY=1
oCMenu3.level[1].align="right" 
oCMenu3.level[1].offsetX=-1
oCMenu3.level[1].offsetY=-1
oCMenu3.level[1].arrow=""
oCMenu3.level[1].arrowWidth=3
oCMenu3.level[1].arrowHeight=6
oCMenu3.level[1].borderClass="menu_Level_1_border"

/***********************************/
/*<!-- /DEFINITION DU MENU INTERNATIONAL -->*/
/***********************************/

/***********************************/
/*<!-- DEFINITION DU MENU BIBLIOTHEQUE -->*/
/***********************************/

/*** 
This is the menu creation code - place it right after you body tag
Feel free to add this to a stand-alone js file and link it to your page.
**/

//Menu object creation
oCMenu5=new makeCM("oCMenu5") //Making the menu object. Argument: menuname

oCMenu5.frames = 0

//Menu properties   
oCMenu5.pxBetween=0
oCMenu5.fromLeft=379 
oCMenu5.fromTop=80
oCMenu5.rows=0 
oCMenu5.menuPlacement=0
                                                             
oCMenu5.offlineRoot="" 
oCMenu5.onlineRoot="" 
oCMenu5.resizeCheck=1 
oCMenu5.wait=50 
oCMenu5.fillImg=""
oCMenu5.zIndex=0

//Background bar properties
oCMenu5.useBar=1
oCMenu5.barWidth=""
oCMenu5.barHeight="menu"
oCMenu5.barClass="menu_Bar"
oCMenu5.barX=0 
oCMenu5.barY=0
oCMenu5.barBorderX=0
oCMenu5.barBorderY=0
oCMenu5.barBorderClass=""

//Level properties - ALL properties have to be spesified in level 0
oCMenu5.level[0]=new cm_makeLevel()//Add this for each new level
oCMenu5.level[0].width=150
oCMenu5.level[0].height=23 
oCMenu5.level[0].regClass="menu_Level_0"
oCMenu5.level[0].overClass="menu_Level_0_over"
oCMenu5.level[0].borderX=0
oCMenu5.level[0].borderY=0
oCMenu5.level[0].borderClass="menu_Level_0_border"
oCMenu5.level[0].offsetX=-1
oCMenu5.level[0].offsetY=1
oCMenu5.level[0].rows=0
oCMenu5.level[0].arrow=""
oCMenu5.level[0].arrowWidth=3
oCMenu5.level[0].arrowHeight=6
oCMenu5.level[0].align="bottom"

//EXAMPLE SUB LEVEL[1] PROPERTIES - You have to specify the properties you want different from LEVEL[0] - If you want all items to look the same just remove this

oCMenu5.level[1]=new cm_makeLevel() //Add this for each new level (adding one to the number)
oCMenu5.level[1].width=oCMenu5.level[0].width-2
oCMenu5.level[1].height=35
oCMenu5.level[1].regClass="menu_Level_1"
oCMenu5.level[1].overClass="menu_Level_1_over"
oCMenu5.level[1].borderX=1
oCMenu5.level[1].borderY=1
oCMenu5.level[1].align="right" 
oCMenu5.level[1].offsetX=-1
oCMenu5.level[1].offsetY=-1
oCMenu5.level[1].arrow=""
oCMenu5.level[1].arrowWidth=3
oCMenu5.level[1].arrowHeight=6
oCMenu5.level[1].borderClass="menu_Level_1_border"

/***********************************/
/*<!-- /DEFINITION DU MENU BIBLIOTHEQUE -->*/
/***********************************/


/***********************************/
/*<!-- DEFINITION DU MENU RECHERCHE -->*/
/***********************************/

/*** 
This is the menu creation code - place it right after you body tag
Feel free to add this to a stand-alone js file and link it to your page.
**/

//Menu object creation
oCMenu2=new makeCM("oCMenu2") //Making the menu object. Argument: menuname

oCMenu2.frames = 0

//Menu properties   
oCMenu2.pxBetween=0
oCMenu2.fromLeft=457 
oCMenu2.fromTop=80
oCMenu2.rows=0 
oCMenu2.menuPlacement=0
                                                             
oCMenu2.offlineRoot="" 
oCMenu2.onlineRoot="" 
oCMenu2.resizeCheck=1 
oCMenu2.wait=50 
oCMenu2.fillImg=""
oCMenu2.zIndex=0

//Background bar properties
oCMenu2.useBar=1
oCMenu2.barWidth=""
oCMenu2.barHeight="menu"
oCMenu2.barClass="menu_Bar"
oCMenu2.barX=0 
oCMenu2.barY=0
oCMenu2.barBorderX=0
oCMenu2.barBorderY=0
oCMenu2.barBorderClass=""

//Level properties - ALL properties have to be spesified in level 0
oCMenu2.level[0]=new cm_makeLevel()//Add this for each new level
oCMenu2.level[0].width=150
oCMenu2.level[0].height=23 
oCMenu2.level[0].regClass="menu_Level_0"
oCMenu2.level[0].overClass="menu_Level_0_over"
oCMenu2.level[0].borderX=0
oCMenu2.level[0].borderY=0
oCMenu2.level[0].borderClass="menu_Level_0_border"
oCMenu2.level[0].offsetX=-1
oCMenu2.level[0].offsetY=1
oCMenu2.level[0].rows=0
oCMenu2.level[0].arrow=""
oCMenu2.level[0].arrowWidth=3
oCMenu2.level[0].arrowHeight=6
oCMenu2.level[0].align="bottom"

//EXAMPLE SUB LEVEL[1] PROPERTIES - You have to specify the properties you want different from LEVEL[0] - If you want all items to look the same just remove this

oCMenu2.level[1]=new cm_makeLevel() //Add this for each new level (adding one to the number)
oCMenu2.level[1].width=oCMenu2.level[0].width-2
oCMenu2.level[1].height=35
oCMenu2.level[1].regClass="menu_Level_1"
oCMenu2.level[1].overClass="menu_Level_1_over"
oCMenu2.level[1].borderX=1
oCMenu2.level[1].borderY=1
oCMenu2.level[1].align="left" 
oCMenu2.level[1].offsetX=1
oCMenu2.level[1].offsetY=-1
oCMenu2.level[1].arrow=""
oCMenu2.level[1].arrowWidth=3
oCMenu2.level[1].arrowHeight=6
oCMenu2.level[1].borderClass="menu_Level_1_border"

/***********************************/
/*<!-- /DEFINITION DU MENU RECHERCHE -->*/
/***********************************/


/***********************************/
/*<!-- DEFINITION DU MENU FORMATION CONTINUE -->*/
/***********************************/

/*** 
This is the menu creation code - place it right after you body tag
Feel free to add this to a stand-alone js file and link it to your page.
**/

//Menu object creation
oCMenu4=new makeCM("oCMenu4") //Making the menu object. Argument: menuname

oCMenu4.frames = 0

//Menu properties   
oCMenu4.pxBetween=0
oCMenu4.fromLeft=538 
oCMenu4.fromTop=80
oCMenu4.rows=0 
oCMenu4.menuPlacement=0
                                                             
oCMenu4.offlineRoot="" 
oCMenu4.onlineRoot="" 
oCMenu4.resizeCheck=1 
oCMenu4.wait=50 
oCMenu4.fillImg=""
oCMenu4.zIndex=0

//Background bar properties
oCMenu4.useBar=1
oCMenu4.barWidth=""
oCMenu4.barHeight="menu"
oCMenu4.barClass="menu_Bar"
oCMenu4.barX=0 
oCMenu4.barY=0
oCMenu4.barBorderX=0
oCMenu4.barBorderY=0
oCMenu4.barBorderClass=""

//Level properties - ALL properties have to be spesified in level 0
oCMenu4.level[0]=new cm_makeLevel()//Add this for each new level
oCMenu4.level[0].width=150
oCMenu4.level[0].height=23 
oCMenu4.level[0].regClass="menu_Level_0"
oCMenu4.level[0].overClass="menu_Level_0_over"
oCMenu4.level[0].borderX=0
oCMenu4.level[0].borderY=0
oCMenu4.level[0].borderClass="menu_Level_0_border"
oCMenu4.level[0].offsetX=-1
oCMenu4.level[0].offsetY=1
oCMenu4.level[0].rows=0
oCMenu4.level[0].arrow=""
oCMenu4.level[0].arrowWidth=3
oCMenu4.level[0].arrowHeight=6
oCMenu4.level[0].align="bottom"

//EXAMPLE SUB LEVEL[1] PROPERTIES - You have to specify the properties you want different from LEVEL[0] - If you want all items to look the same just remove this

oCMenu4.level[1]=new cm_makeLevel() //Add this for each new level (adding one to the number)
oCMenu4.level[1].width=oCMenu4.level[0].width-2
oCMenu4.level[1].height=35
oCMenu4.level[1].regClass="menu_Level_1"
oCMenu4.level[1].overClass="menu_Level_1_over"
oCMenu4.level[1].borderX=1
oCMenu4.level[1].borderY=1
oCMenu4.level[1].align="left" 
oCMenu4.level[1].offsetX=1
oCMenu4.level[1].offsetY=-1
oCMenu4.level[1].arrow=""
oCMenu4.level[1].arrowWidth=3
oCMenu4.level[1].arrowHeight=6
oCMenu4.level[1].borderClass="menu_Level_1_border"

/***********************************/
/*<!-- /DEFINITION DU MENU FORMATION CONTINUE -->*/
/***********************************/


/***********************************/
/*<!-- DEFINITION DU MENU CONCOURS ET RECRUTEMENT -->*/
/***********************************/

/*** 
This is the menu creation code - place it right after you body tag
Feel free to add this to a stand-alone js file and link it to your page.
**/

//Menu object creation
oCMenu6=new makeCM("oCMenu6") //Making the menu object. Argument: menuname

oCMenu6.frames = 0

//Menu properties   
oCMenu6.pxBetween=0
oCMenu6.fromLeft=655 
oCMenu6.fromTop=80
oCMenu6.rows=0 
oCMenu6.menuPlacement=0
                                                             
oCMenu6.offlineRoot="" 
oCMenu6.onlineRoot="" 
oCMenu6.resizeCheck=1 
oCMenu6.wait=50 
oCMenu6.fillImg=""
oCMenu6.zIndex=0

//Background bar properties
oCMenu6.useBar=1
oCMenu6.barWidth=""
oCMenu6.barHeight="menu"
oCMenu6.barClass="menu_Bar"
oCMenu6.barX=0 
oCMenu6.barY=0
oCMenu6.barBorderX=0
oCMenu6.barBorderY=0
oCMenu6.barBorderClass=""

//Level properties - ALL properties have to be spesified in level 0
oCMenu6.level[0]=new cm_makeLevel()//Add this for each new level
oCMenu6.level[0].width=150
oCMenu6.level[0].height=23 
oCMenu6.level[0].regClass="menu_Level_0"
oCMenu6.level[0].overClass="menu_Level_0_over"
oCMenu6.level[0].borderX=0
oCMenu6.level[0].borderY=0
oCMenu6.level[0].borderClass="menu_Level_0_border"
oCMenu6.level[0].offsetX=-45
oCMenu6.level[0].offsetY=1
oCMenu6.level[0].rows=0
oCMenu6.level[0].arrow=""
oCMenu6.level[0].arrowWidth=3
oCMenu6.level[0].arrowHeight=6
oCMenu6.level[0].align="bottom"

//EXAMPLE SUB LEVEL[1] PROPERTIES - You have to specify the properties you want different from LEVEL[0] - If you want all items to look the same just remove this

oCMenu6.level[1]=new cm_makeLevel() //Add this for each new level (adding one to the number)
oCMenu6.level[1].width=oCMenu6.level[0].width-2
oCMenu6.level[1].height=35
oCMenu6.level[1].regClass="menu_Level_1"
oCMenu6.level[1].overClass="menu_Level_1_over"
oCMenu6.level[1].borderX=1
oCMenu6.level[1].borderY=1
oCMenu6.level[1].align="left" 
oCMenu6.level[1].offsetX=1
oCMenu6.level[1].offsetY=-1
oCMenu6.level[1].arrow=""
oCMenu6.level[1].arrowWidth=3
oCMenu6.level[1].arrowHeight=6
oCMenu6.level[1].borderClass="menu_Level_1_border"

/***********************************/
/*<!-- /DEFINITION DU MENU CONCOURS ET RECRUTEMENT -->*/
/***********************************/

