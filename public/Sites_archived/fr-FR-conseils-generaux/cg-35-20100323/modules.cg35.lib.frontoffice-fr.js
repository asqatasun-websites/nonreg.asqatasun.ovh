var selectedString='';

function getSelectedHTML() {
  var rng=undefined;
  if (window.getSelection) {
    selobj = window.getSelection();
    if (!selobj.isCollapsed) {
      if (selobj.getRangeAt) {
        rng=selobj.getRangeAt(0);
      }
      else {
        rng = document.createRange();
        rng.setStart(selobj.anchorNode,selobj.anchorOffset);
        rng.setEnd(selobj.focusNode,selobj.focusOffset);
      }
      if (rng) {
        DOM = rng.cloneContents();
        object = document.createElement('div');
        object.appendChild(DOM.cloneNode(true));
        selectedString=object.innerHTML;
      }
      else {
        selectedString=selobj;
      }
    }
  }
  else if (document.selection) {
    selobj = document.selection;
    rng = selobj.createRange();
    if (rng && rng.htmlText) {
      selectedString = rng.htmlText;
    }
    else if (rng && rng.text) {
      selectedString = rng.text;
    }
  }
  else if (document.getSelection) {
    selectedString=document.getSelection();
  }
}

function copySelection() {
  setTimeout("getSelectedHTML()",50);
}

document.onmouseup=copySelection;
document.onkeyup=copySelection;

