function switchBlock(name, item, total, smouth)
{
	var elem = null;

	for(var i = 1; i <= total; i++)
	{
		elem = $('#' + name + i);

		if(item != i)
		{
			elem.css({display:"none"});
		}
	}

	elem = $('#' + name + item);

	if(smouth > 0)
	{
		elem.fadeIn(smouth);
	}
	else
	{
		elem.css({display:"block"});
	}
}

/*	CLOSER STYLE	*/

function displayBlockStyle(name, item, total)
{
	switchBlock(name + '_item_', item, total, 0);

	for(var i = 1; i <= total; i++)
	{
		var elem = $('#' + name + '_menu_' + i);
		if(item == i)
		{
			elem.removeClass('off');
			elem.addClass('on');
		}
		else
		{
			elem.removeClass('on');
			elem.addClass('off');
		}
	}
}

/*	CLOSER MEDIA	*/

function switchMediaCloser(toCloseName, toOpenName)
{
	$('#' + toCloseName).css({display:"none"});
	$('#' + toOpenName).css({display:"block"});
}

/*	SLIDER HOME	*/

var sliderFade = 250;
var sliderShow = 0;
var sliderManual = false;
var sliderTimer = false;

function showSliderItem(name, item, total)
{
	if(sliderShow > 0 && sliderFade > 0)
	{
		$('#' + name + '_' + sliderShow).stop(true, true);
	}
	sliderShow = item;

	switchBlock(name + '_', item, total, sliderFade);

	for(var i = 1; i <= total; i++)
	{
		var elem = $('#' + name + '_menu_' + i);
		if(item == i)
		{
			elem.removeClass('off');
			elem.addClass('on');
		}
		else
		{
			elem.removeClass('on');
			elem.addClass('off');
		}
	}
}

function moveSlider(name, mov, total) {
	var newShow = sliderShow + mov;

	if(newShow <= 0) {			newShow = total;	}
	else if(newShow > total) {	newShow = 1;		}

	showSliderItem(name, newShow, total);
}

function cycleSlider(name, sliderCycle, total)
{
	if(!sliderManual)
	{
		sliderRestart = false;
		moveSlider(name, 1, total)
		sliderTimer = setTimeout("cycleSlider('"+ name +"', "+ sliderCycle +", "+ total +")", sliderCycle);
	}
}

function stopAutoSlider()
{
	sliderManual = true;
	clearTimeout(sliderTimer);
	sliderTimer = false;
}

function startAutoSlider(name, sliderCycle, total)
{
	if(sliderManual && sliderTimer == false)
	{
		sliderManual = false;
		sliderTimer = setTimeout("cycleSlider('"+ name +"', "+ sliderCycle +", "+ total +")", sliderCycle);
	}
}

/*********************************** *
 *		BLOC CLOSER MARKET
 *********************************** */

var closerMarketShow = 0;
var closerMarketInAction = false;
var closerMarketTime = 500;

function deployCloserMarket(num, total) {
	$('#closerMarket .miniature').stop(true, true);

	$('#closerMarket .miniature').hide();

	$('#closerMarket .deployed').show();

	$('#closerMarket_'+num).fadeIn(closerMarketTime);
	$('#closerMarket_txt_'+num).fadeIn(closerMarketTime);

	closerMarketShow = num;
}

function closeCloserMarket() {
	$('#closerMarket_'+closerMarketShow).stop(true, true);
	$('#closerMarket_txt_'+closerMarketShow).stop(true, true);
	$('#closerMarket .deployed').stop(true, true);

	$('#closerMarket_'+closerMarketShow).hide();
	$('#closerMarket_txt_'+closerMarketShow).hide();
	$('#closerMarket .deployed').hide();

	$('#closerMarket .miniature').fadeIn(closerMarketTime);

	closerMarketShow = 0;
}

function moveCloserMarket(mov, total) {
	var newShow = closerMarketShow + mov;

	if(newShow <= 0) {			newShow = total;	}
	else if(newShow > total) {	newShow = 1;		}

	$('#closerMarket_'+closerMarketShow).stop(true, true);
	$('#closerMarket_txt_'+closerMarketShow).stop(true, true);

	$('#closerMarket_'+closerMarketShow).hide();
	$('#closerMarket_txt_'+closerMarketShow).hide();

	$('#closerMarket_'+newShow).fadeIn(closerMarketTime);
	$('#closerMarket_txt_'+newShow).fadeIn(closerMarketTime);

	closerMarketShow = newShow;
}

/*	BLOCK VIDEO // ZAP RADIO	*/

var zapManual = false;
var zapCycle = 7000;

function switchZap(toCloseName, toOpenName)
{
	toClose = $('#zone_' + toCloseName);
	toOpen = $('#zone_' + toOpenName);

	toCloseOnglet = $('#zap_item_' + toCloseName);
	toOpenOnglet = $('#zap_item_' + toOpenName);

	toCloseOnglet.removeClass('on');
	toCloseOnglet.addClass('off');

	toOpenOnglet.addClass('on');
	toOpenOnglet.removeClass('off');

	toClose.css({display:"none"});
	toOpen.css({display:"block"});
}

function cycleZap(toCloseName, toOpenName)
{
	if(!zapManual)
	{
		switchZap(toCloseName, toOpenName);
		setTimeout("cycleZap('" + toOpenName + "', '" + toCloseName + "')", zapCycle);
	}
}

function stopAutoZap()
{
	zapManual = true;
}
