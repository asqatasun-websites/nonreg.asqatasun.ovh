/*
 * Copyright 2009 Daimler AG. All rights reserved.
 *
 * Implementation of the eMB Tracking API
 *
 * @project DSCNeu
 * @author Michael Giere (michael.giere@mediaman.de), mediaman GmbH
 * @author Harald Niesche (harald.niesche@sidion.de)
 * @version v1.0
 *
 */
(function() {

var CONST_DEFAULT_SCOPE = "_default_scope_";

var embTrackingManager = {};
window.embTrackingManager = function (scope) {
    if (!scope || scope == "") {
       scope = CONST_DEFAULT_SCOPE;
    }
    if (!embTrackingManager[scope]) {
       embTrackingManager[scope] = new EmbTrackingManager(scope);
    }
    return embTrackingManager[scope];
}

function EmbTrackingManager(scope) {
	this.accountId = "";
	this.pageName = "";
	this.data = {};
	this.scope = scope;
	this.initialized = false;
}


//printf-debugging support
var debugConsole = window.console;

if (!window.console) {
  debugConsole = {
    log:   function() {},
    debug: function() {},
    print: function() {},
    isDummy: true
  };
  
  try {
    window.console = debugConsole;
  } catch (e) {
    // we don't really care
  }
}

EmbTrackingManager.prototype.init = function(accountId, pageName) {
	if (!this.initialized) {
		if (accountId && accountId != "") this.accountId = accountId;
		if (pageName && pageName != "") this.pageName = pageName;
		this.data = {};
		this.initialized = true;
		// log successful call
		debugConsole.log("EmbTrackingManager(" + this.scope + ") -> init: called with accountId=" + accountId + " and pageName=" + pageName);
	} else {
		debugConsole.log("EmbTrackingManager(" + this.scope + ") -> init: skipped -> EmbTrackingManager already initialized");
	}
}

EmbTrackingManager.prototype.isInitialized = function() {
	return this.initialized;
}

EmbTrackingManager.prototype.setAccountId = function(accountId) {
	if (this.initialized) {
		if (accountId && accountId != "") this.accountId = accountId;
		// log successful call
		debugConsole.log("EmbTrackingManager(" + this.scope + ") -> setAccountId: called with accountId=" + accountId);
	} else {
		debugConsole.log("EmbTrackingManager(" + this.scope + ") -> setAccountId: skipped -> EmbTrackingManager not initialized");
	}
}

EmbTrackingManager.prototype.setPageName = function(pageName) {
	if (this.initialized) {
		if (pageName && pageName != "") this.pageName = pageName;
		// log successful call
		debugConsole.log("EmbTrackingManager(" + this.scope + ") -> setPageName: called with pageName=" + pageName);
	} else {
		debugConsole.log("EmbTrackingManager(" + this.scope + ") -> setPageName: skipped -> EmbTrackingManager not initialized");
	}
}

EmbTrackingManager.prototype.addData = function(props) {
	try {
		if (this.initialized) {
			if (!props) props = {};
			for (var key in props) {
				if (key != "pageName") {
					this.data[key] = props[key];
				} else {
					debugConsole.log("EmbTrackingManager(" + this.scope + ") -> addData: Skipped reserved parameter pageName");
				}
			}	
//			// log successful call (debugging)
//			var propsStr = "{";
//			var count = 0;
//			for (var key in props) {
//				if (count > 0) propsStr += ", ";
//			    propsStr += key + ":\"" + props[key] + "\"";
//			    count++;
//			}
//			propsStr += "}";
//			debugConsole.log("EmbTrackingManager(" + this.scope + ") -> addData: called with props=" + propsStr);
		} else {
			debugConsole.log("EmbTrackingManager(" + this.scope + ") -> addData: skipped -> EmbTrackingManager not initialized");
		}
	} catch(e) {
		debugConsole.log("ERROR in EmbTrackingManager(" + this.scope + ") -> addData: " + e);
	}
}


EmbTrackingManager.prototype.sendData = function(props) {
	try {
		if (this.initialized) {
			// get an instance of the data object
			var dataObj = this.getEmptyDataObject();
			
			// set new data
			dataObj.pageName = this.pageName;
			for (var key in this.data) dataObj[key] = this.data[key];
			
			// send data
			var trackingCode = dataObj.t();
			if (trackingCode && trackingCode != "") document.write(trackingCode);
			
//			// log successful call (debugging)
//			var dataStr = "{";
//			var count = 0;
//			for (var key in this.data) {
//				if (count > 0) dataStr += ", ";
//				dataStr += key + ":\"" + this.data[key] + "\"";
//			    count++;
//			}
//			dataStr += "}";			
//			debugConsole.log("EmbTrackingManager(" + this.scope + ") -> sendData: called using data=" + dataStr);
		} else {
			debugConsole.log("EmbTrackingManager(" + this.scope + ") -> sendData: skipped -> EmbTrackingManager not initialized");
		}
	} catch(e) {
		debugConsole.log("ERROR in EmbTrackingManager(" + this.scope + ") -> sendData: " + e);
	}
}

EmbTrackingManager.prototype.sendLinkEvent = function(linkName) {
	try {
		if (this.initialized) {
			var dataObj = this.getDataObject();
			dataObj.linkTrackVars = "prop19,prop26";
			dataObj.linkTrackEvents = "None";
			dataObj.prop19 = linkName;
			dataObj.prop26 = this.pageName;
			dataObj.tl(this, 'o', linkName);
	
			// log successful call (debugging)
			debugConsole.log("EmbTrackingManager(" + this.scope + ") -> sendLinkEvent: called with linkName=" + linkName);
		} else {
			debugConsole.log("EmbTrackingManager(" + this.scope + ") -> sendLinkEvent: skipped -> EmbTrackingManager not initialized");
		}
	} catch(e) {
		debugConsole.log("ERROR in EmbTrackingManager(" + this.scope + ") -> sendLinkEvent: " + e);
	}
}
EmbTrackingManager.prototype.sendLinkEventMS = function(linkName, type, kind) {
	try {
		if (this.initialized) {
			var dataObj = this.getDataObject();
			if(kind == 'e'){
				dataObj.linkTrackVars = "pageName,prop36";
			} else{
				dataObj.linkTrackVars = "pageName,eVar6,prop36";
			}
			dataObj.linkTrackEvents = "None";
			dataObj.prop36 = type;
			if(kind != 'e'){
				dataObj.eVar6 = linkname;
			}
			dataObj.tl(true, kind, linkName);
	
			// log successful call (debugging)
			debugConsole.log("EmbTrackingManager(" + this.scope + ") -> sendLinkEvent: called with linkName=" + linkName);
		} else {
			debugConsole.log("EmbTrackingManager(" + this.scope + ") -> sendLinkEvent: skipped -> EmbTrackingManager not initialized");
		}
	} catch(e) {
		debugConsole.log("ERROR in EmbTrackingManager(" + this.scope + ") -> sendLinkEvent: " + e);
	}
}
EmbTrackingManager.prototype.sendCCFlashEvent = function(flashtag) {
	try {
		if (this.initialized) {
			var dataObj = this.getDataObject();
			dataObj.linkTrackVars = "prop19,prop26";
			dataObj.linkTrackEvents = "None";
			dataObj.prop19 = flashtag;
			dataObj.prop26 = this.pageName;
			dataObj.tl(this, 'o', flashtag);
	
			// log successful call (debugging)
			debugConsole.log("EmbTrackingManager(" + this.scope + ") -> sendCCFlashEvent: called with flashtag=" + flashtag);
		} else {
			debugConsole.log("EmbTrackingManager(" + this.scope + ") -> sendCCFlashEvent: skipped -> EmbTrackingManager not initialized");
		}
	} catch(e) {
		debugConsole.log("ERROR in EmbTrackingManager(" + this.scope + ") -> sendCCFlashEvent: " + e);
	}
}

EmbTrackingManager.prototype.sendEmbFlashEvent = function(flashtag, pageNameSuffix2remove) {
	try {
		if (this.initialized) {
			var dataObj = this.getDataObject();
			var pageNameString = dataObj.pageName;
			if (pageNameSuffix2remove && pageNameSuffix2remove != "") {
				pageNameString = pageNameString.replace(new RegExp(pageNameSuffix2remove + "$"), "");
			}
			dataObj.linkTrackVars = 'prop1';
			dataObj.linkTrackEvents = 'None';
			dataObj.prop1 = 'eMBflash:' + pageNameString + ':' + flashtag;
			dataObj.tl(this, 'o', 'eMBflash:' + pageNameString + ':' + flashtag);
			
			// log successful call (debugging)
			debugConsole.log("EmbTrackingManager(" + this.scope + ") -> sendFlashEvent: called with flashTag=" + flashtag + " and pageNameSuffix=" + pageNameSuffix);
		} else {
			debugConsole.log("EmbTrackingManager(" + this.scope + ") -> sendFlashEvent: skipped -> EmbTrackingManager not initialized");
		}
	} catch(e) {
		debugConsole.log("ERROR in EmbTrackingManager(" + this.scope + ") -> sendFlashEvent: " + e);
	}
}

EmbTrackingManager.prototype.getDataObject = function() {
	var resultDataObj = {};
	try {
		resultDataObj = s_gi(this.accountId);
	} catch(e) {
		debugConsole.log("ERROR in EmbTrackingManager(" + this.scope + ") -> getDataObject: " + e);
	}
	return resultDataObj;
}

EmbTrackingManager.prototype.getEmptyDataObject = function() {
	var resultDataObj = {};
	try {
		resultDataObj = s_gi(this.accountId);
		
		//clear old data, if exist
		for (var i = 0; i < 50; i++) {
			if (resultDataObj["eVar" + i]) resultDataObj["eVar" + i] = ""; 
			if (resultDataObj["event" + i]) resultDataObj["event" + i] = "";
			if (resultDataObj["prop" + i]) resultDataObj["prop" + i] = "";				
		}
	} catch(e) {
		debugConsole.log("ERROR in EmbTrackingManager(" + this.scope + ") -> getEmptyDataObject: " + e);
	}
	return resultDataObj;
}

})();
