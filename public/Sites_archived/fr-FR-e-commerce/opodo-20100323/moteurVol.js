﻿//compte omniture 
s_account="opodofrprod";

s.pageName="Op:FR:MoteurFlgtHP:Search";
s.channel="HOMEPAGE";
s.campaign=s.getQueryParam('CMP');


//decalage jours entre aller et retour
var dpDelay = 3;
var volDelay = 7;
var rechercherVolHotel = false;
var volHotel_ValidateDate = false;
var ageChildrenOk =false;


		
$(document).ready(function() {
  
	CallToDisplay('Form','moteurSearchVol');

	//initialisation des date du moteur,    
	var today = new Date ();
	//vol
	var departDate = new Date();
		departDate.setDate(today.getDate() + 1);
	var returnDate = new Date();
		returnDate.setDate(today.getDate() + volDelay +1);
	  
	var departMonth	= (departDate.getMonth()+1).toString();
	var departDay 	= departDate.getDate().toString();
	
	var returnMonth	=	(returnDate.getMonth()+1).toString();
	var returnDay	=	returnDate.getDate().toString();
	
	
	if(departMonth.length==1)
	  departMonth = "0"+departMonth;
	
	if(departDay.length==1)
		departDay= "0"+departDay;
			   
	if(returnMonth.length==1)
	  returnMonth = "0"+returnMonth;
	
	if(returnDay.length==1)
		returnDay= "0"+returnDay;
			
	var dateDepartText = departDay+"/"+departMonth+"/"+departDate.getFullYear();
	var dateRetournText = returnDay+"/"+returnMonth+"/"+returnDate.getFullYear();
	
	$('#DATE_DVText').val(dateDepartText);
	$('#DATE_RVText').val(dateRetournText);
	
	
	$("#formVol input[@name=D_DATE]").val(departDate.getFullYear()+""+departMonth+""+departDay);
	$("#formVol input[@name=R_DATE]").val(returnDate.getFullYear()+""+returnMonth+""+returnDay);
	
	// fil hours
	var OptionHours = HourOptions();
	$("#formVol select[@name=D_ANYTIME]").append(OptionHours);
	$("#formVol select[@name=R_ANYTIME]").append(OptionHours);
		
	// call  yahoo calendars 
	initDoubleCalendier("DATE_DVText", "cal3Container", "calendar3","DATE_DVText",12,"","updateReturnDate('DATE_DVText','DATE_RVText','formVol')","DD","YYYYMM");
	initDoubleCalendier("DATE_RVText", "cal4Container", "calendar4","DATE_RVText",12, "","updateDepartureDate('DATE_RVText','formVol')","DD","YYYYMM");	

	// n adults n bb
	
	$("#formVol select[@name=NB_ADULTS]").change(function(){
		var bboptions="";
		for (i=0 ;i<=$("#formVol select[@name=NB_ADULTS]").val();i++)
			bboptions +='<option value="'+i+'">'+ i +'</option>';
			$("#formVol select[@name=NB_INFANTS]").html(bboptions);
		
	});
	
	//	Lier nombre d'adultes au nombre de bébés (Migration)
	$("#formVol select[@name=numberOfAdults]").change(function(){
	var bboptions="";
	for (i=0 ;i<=$("#formVol select[@name=numberOfAdults]").val();i++)
		bboptions +='<option value="'+i+'">'+ i +'</option>';
		$("#formVol select[@name=numberOfInfants]").html(bboptions);
	
	});
	
	
	// formulaire vol

	var allerSimple =false;

	$('#allerSimple').click(function(){
	
		if(!allerSimple){
		
			tracking(this,'Op:FR:MoteurFlgt:choose_AllerSimple');
			$("#vol_retour").css("display", "none");
			$("#btVolHotel").remove();
						
			rechercherVolHotel=false;
			//$("#dv_submitVolHotel").css({"position":"absolute","min-width":"455px","width":"28.4375em","top":"21.8em"});	//	Supprimé : élargissement de la zone
			$("#dv_submitVolHotel").css({"position":"absolute","top":"21.8em"});
			
			allerSimple = true;
		}		
	});

	$('#allerRetour').click(function(){
		if(allerSimple) {
			var radio =true;
			$("#vol_retour").css("display", "block");
			
			//$("#dv_submitVolHotel").css({"position":"absolute","min-width":"455px","width":"28.4375em","top":"21.8em","height":"60px"});	//	Supprimé : élargissement de la zone
			$("#dv_submitVolHotel").css({"position":"absolute","top":"21.8em","height":"60px"});
			$('#btVol').after('<span id="btVolHotel" id="btVolHotel"><img src="http://www.opodo.fr/img_opodo/LandingPage/boutonSubmit-left.jpg" alt=""/><input type="submit" id="rechercheVolHotel" name="rechercheVolHotel" value="Recherchez votre vol+hôtel" class="npt_button" /><img src="http://www.opodo.fr/img_opodo/LandingPage/boutonSubmit-right.jpg" alt=""/></span>');
			
			$('#rechercheVolHotel').click(function() {
				rechercheVoletHotel();
			});
	
			allerSimple=false;
		}	
	});
	
	
	$('#rechercheVolHotel').click(function() {
		tracking(this,'Op:FR:Homepage:Search_DPSubmitButton');
		return rechercheVoletHotel();
	});
	
	
	$('#rechercheVol').click(function() {
		tracking(this,'Op:FR:Homepage:Search_FlgtSubmitButton');
		volHotel_ValidateDate=false;		
		$("#formVol").attr('action', 'http://vols.opodo.fr/apps/jsp/planItGoMP/AirAvailabilityServlet');
		if	(submitValidateSearch())
			initParamsAndSubmit();
		else
			return false
		
	});
	
	//	Nouveau moteur migration
	$('#rechercheVol_Migration').click(function() {
		var	reg_date	=	/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/;
		var tab_champs	=	new Array('D', 'R');
		var tab_chdates	=	new Array('Day', 'Month');
		//	On teste la présence des champs de date
		for(i in tab_champs) {
			var lettre		=	tab_champs[i];
			var debut_champ	=	(lettre == 'D') ? 'departure' : 'return';
			var nom_champ	=	'DATE_'+lettre+'VText';
			//	Si les champs n'existent pas, on les crée
			for(j in tab_chdates) {
				if($('#'+debut_champ+tab_chdates[j]).length == 0)
					$('#'+nom_champ).before('<input type="hidden" name="'+debut_champ+tab_chdates[j]+'" id="'+debut_champ+tab_chdates[j]+'">');
			}
			//	On remplit les champs de dates
			//	On commence par récupérer le contenu du champ texte
			var txt_date	=	$('#'+nom_champ).val();
			reg_date.exec(txt_date);
			var jour	=	RegExp.$1;
			var moisa	=	RegExp.$3+RegExp.$2;
			$('input[name='+debut_champ+'Day]').val(jour);
			$('input[name='+debut_champ+'Month]').val(moisa);
		}
		
		tracking(this,'Op:FR:Homepage:Search_FlgtSubmitButton');
		volHotel_ValidateDate	=	false;
		$("#formVol").attr('action', 'http://billetavion.opodo.fr/opodo/flights/search');
		//	return false;
		if(!submitValidateSearch_migration())
			return false;
	});
	
	if( $('#flash_banner').length != 0 ){
		
		//	On teste s'il y a plus d'un élément dans la liste #fb_objects
		if($('#fb_objects li').size() > 1)
			flash_banner_2(500, 5000);
		
		/*var so = new SWFObject("flashpanel-custom.swf", "flashpanel", "455", "240", "7", "#FFFFFF");
		so.addParam("wmode", "transparent");
		so.addParam	("quality", "high");
		so.useExpressInstall('expressinstall.swf');
		so.write("flash_banner");
		*/
	}
	
	$('#multidestination').click(function(){
		 tracking(this,'Op:FR:Homepage:Search_MultiStopFlgtButton');
		 window.location.replace('http://multi-destinations.opodo.fr/opodo/flights/multistopFlights');
		 return false;
                
	});
	
	$('#directFlight').click(function(){
		 tracking(this,'Op:FR:Homepage:Search_DirectFlgtBox');
		                
	});
	$('#MINUS_PLUS_DATE').click(function(){
		 tracking(this,'Op:FR:Homepage:Search_FlexibleFlgtBox');
		                
	});
	
	$("#fb_objects li").click(function() {
		
		var id = $(this).attr("id");
		tracking(this,'Op:FR:Homepage:flash_banner_'+id);		
								
	});		
		
	//autofil
	applyOpodoAutocomplete("#B_LOCATION_IN_Vol,#E_LOCATION_IN_Vol", "flight");
	
	
});


// flashpanel JS
function flash_banner_2(speed, timeout) {
	$('#fb_objects').before('<ul id="fb_prevnext"><li id="fb_prev"><a href="#">&lt;</a></li><li id="fb_next"><a href="#">&gt;</a></li></ul>');
	$('#fb_objects').before('<div id="fb_pages">').cycle({ 
					fx: 'cover',
					speed:		speed, 
					timeout:	timeout,
					pause:		1,
					next:		'#fb_next a',
					prev:		'#fb_prev a',
					pager:		'#fb_pages'
		});
}


// fonction appelée via le boutton recheche vol+hoetl dasn l'anglet vol.
function rechercheVoletHotel(){
		
		rechercherVolHotel=true;
		volHotel_ValidateDate=true;	
			
		$("#formVol").attr('action', 'http://weekend.opodo.fr/apps/jsp/dphotel/DPAvailabilityServlet');
		
		if	(submitValidateSearch())
			initParamsAndSubmit();
		else
			return false
	
}


	
	// Onsubmit Vols
function submitValidateSearch() {
	
	var departureDateTab = $("#DATE_DVText").val().split("/");
	var returnDateTab = $("#DATE_RVText").val().split("/");	
				
	if ($("#formVol input[@name=B_LOCATION_IN]").val() == '') {
			
		html="<p><img class='alert' src='/img_opodo/homepageV2/alert3.gif' align='ATTENTION' />Veuillez remplir les villes de départ et d'arrivée<img id='closeError' class ='closeError' border='0' alt='Fermer' src='/img_opodo/headfoot/icon-close.gif' /></p>";
		$("#errorVols").html(html);
		$("#errorVols").show();
			
		$('.closeError').click(function() {
			$("#errorVols").hide();
		});
		return false;
				
	}
		
		
	if ($("#formVol input[@name=E_LOCATION_IN]").val() == '') {
					
		html="<p><img class='alert' src='/img_opodo/homepageV2/alert3.gif' align='ATTENTION' />Veuillez remplir les villes de départ et d'arrivée<img id='closeError' class ='closeError' border='0' alt='Fermer' src='/img_opodo/headfoot/icon-close.gif' /></p>";
		$("#errorVols").html(html);
		$("#errorVols").show();
		//$("#moteurSearchVol div[@id=errorVols]").show();
				
		$('.closeError').click(function() {
			$("#errorVols").hide();	
		});
		return false;
	}
			
	if ($("#formVol input[@name=E_LOCATION_IN]").val() != '' && $("#formVol input[@name=B_LOCATION_IN]").val().toLowerCase() == $("#formVol input[@name=E_LOCATION_IN]").val().toLowerCase()) {
			
		html="<p><img class='alert' src='/img_opodo/homepageV2/alert3.gif' align='ATTENTION' />Les villes de départ et de retour doivent être différentes<img id='closeError' class ='closeError' border='0' alt='Fermer' src='/img_opodo/headfoot/icon-close.gif' /></p>";
		$("#errorVols").html(html);
		$("#errorVols").show();
			
		$('.closeError').click(function() {
			$("#errorVols").hide();
		});
		return false;
					
				
	}
			
	if ( (parseInt($("#formVol select[@name=NB_ADULTS]").val()) + parseInt($("#formVol select[@name=NB_CHILDREN]").val())) > 9 ) {
			
		html="<p><img class='alert' src='/img_opodo/homepageV2/alert3.gif' align='ATTENTION' />La réservation de billets d'avion est limitée à 9 passagers<br/> au total.<img id='closeError' class ='closeError' border='0' alt='Fermer' src='/img_opodo/headfoot/icon-close.gif' /></p>";
		$("#errorVols").html(html);
		$("#errorVols").show();

		$('.closeError').click(function() {
			$("#errorVols").hide();
		});
		return false;
	}
				
	// erreurs sur les dates		
	if(!validRegexDate($('#DATE_DVText').val())  || !isDateValid($('#DATE_DVText').val())) {
			  
		html="<p><img class='alert' src='/img_opodo/homepageV2/alert3.gif' align='ATTENTION' />-Veuillez entrer une date de départ valide <br/>(format jj/mm/aaaa).<img id='closeError' class ='closeError' border='0' alt='Fermer' src='/img_opodo/headfoot/icon-close.gif' /></p>";
		$("#errorVols").html(html);
		$("#errorVols").show();
		
		$('.closeError').click(function() {
			$("#errorVols").hide();
		});
		return false;
			
		}
		
	// si c'est pas un aller simple on  verifie la date de retour.
	if(!$("#allerSimple").is(":checked")){
		if(!validRegexDate($('#DATE_RVText').val()) || !isDateValid($('#DATE_RVText').val())) {
							
			html="<p><img class='alert' src='/img_opodo/homepageV2/alert3.gif' align='ATTENTION' />-Veuillez entrer une date de retour valide <br/>(format jj/mm/aaaa).<img id='closeError' class ='closeError' border='0' alt='Fermer' src='/img_opodo/headfoot/icon-close.gif' /></p>";
			$("#errorVols").html(html);
			$("#errorVols").show();
			$('.closeError').click(function() {
			$("#errorVols").hide();
			});
		return false;
		}
	}
	

	
	var status = validateDates("errorVols","formVol", departureDateTab, returnDateTab, "D_ANYTIME", "R_ANYTIME");
	if(status==false)
		return false;
	
	
	// PoP répartition par chambre et ages des enfants
	
	if (rechercherVolHotel && !ageChildrenOk && (parseInt($("#formVol select[@name=NB_ADULTS]").val())>2 || parseInt($("#formVol select[@name=NB_CHILDREN]").val())>0)){	
			
			//listner sur les listes déroulante : adult et enfants,  mois, jour...
			displayChildrenAgeFieldsHPDP($("#formVol select[@name=NB_CHILDREN]").val(),'childrenAgeFieldsVol')
			displayNbRoomsFieldsHPDP('dv_repartRoomVol','nbRoomsFieldsVol',$("#formVol select[@name=NB_ADULTS]").val(), $("#formVol select[@name=NB_CHILDREN]").val());
			
			
			$("#formVol select[@name=NB_CHILDREN]").change(function() {
				displayChildrenAgeFieldsHPDP($("#formVol select[@name=NB_CHILDREN]").val(),'childrenAgeFieldsVol')
				displayNbRoomsFieldsHPDP('dv_repartRoomVol','nbRoomsFieldsVol',$("#formVol select[@name=NB_ADULTS]").val(), $("#formVol select[@name=NB_CHILDREN]").val());
			
			});
			
		
			$("#formVol select[@name=NB_ADULTS]").change(function(){			
				displayNbRoomsFieldsHPDP('dv_repartRoomVol','nbRoomsFieldsVol',$("#formVol select[@name=NB_ADULTS]").val(), $("#formVol select[@name=NB_CHILDREN]").val());
			
			});
			
			$("#calqueRepartition").show();
			
			$('#closePoP').click(function() {
				$("#calqueRepartition").hide();
			});
			
			$('#continuer').click(function() {
				initParamsAndSubmit();
			});
			
			ageChildrenOk =true;
			return false;
	}
	
	return true
	
		
}

function submitValidateSearch_migration() {
	
	var departureDateTab = $("#DATE_DVText").val().split("/");
	var returnDateTab = $("#DATE_RVText").val().split("/");	
				
	if ($("#formVol input[@name=departureAirport]").val() == '') {
			
		html="<p><img class='alert' src='/img_opodo/homepageV2/alert3.gif' align='ATTENTION' />Veuillez remplir les villes de départ et d'arrivée<img id='closeError' class ='closeError' border='0' alt='Fermer' src='/img_opodo/headfoot/icon-close.gif' /></p>";
		$("#errorVols").html(html);
		$("#errorVols").show();
			
		$('.closeError').click(function() {
			$("#errorVols").hide();
		});
		return false;
				
	}
		
		
	if ($("#formVol input[@name=arrivalAirport]").val() == '') {
					
		html="<p><img class='alert' src='/img_opodo/homepageV2/alert3.gif' align='ATTENTION' />Veuillez remplir les villes de départ et d'arrivée<img id='closeError' class ='closeError' border='0' alt='Fermer' src='/img_opodo/headfoot/icon-close.gif' /></p>";
		$("#errorVols").html(html);
		$("#errorVols").show();
		//$("#moteurSearchVol div[@id=errorVols]").show();
				
		$('.closeError').click(function() {
			$("#errorVols").hide();	
		});
		return false;
	}
	
	if($('#departureAirportCode').val() == '' || $('#arrivalAirportCode').val() == '') {
		if($('#departureAirportCode').val() == '' && $('#arrivalAirportCode').val() == '')
			var error_message	=	"Nous n'arrivons pas à retrouver vos villes de départ et d'arrivée.<br>Veuillez essayer de remplir les champs à nouveau.";
		else if($('#departureAirportCode').val() == '')
			var error_message	=	"Nous n'arrivons pas à retrouver votre ville de départ.<br>Veuillez essayer de remplir le champ à nouveau.";
		else
			var error_message	=	"Nous n'arrivons pas à retrouver votre ville d'arrivée.<br>Veuillez essayer de remplir le champ à nouveau.";
		
		var html	=	"<p><img class='alert' src='/img_opodo/homepageV2/alert3.gif' align='ATTENTION' />"+error_message+"<img id='closeError' class ='closeError' border='0' alt='Fermer' src='/img_opodo/headfoot/icon-close.gif' /></p>";
		$("#errorVols").html(html);
		$("#errorVols").show();
				
		$('.closeError').click(function() {
			$("#errorVols").hide();	
		});
		return false;
	}
			
	if ($("#formVol input[@name=arrivalAirport]").val() != '' && $("#formVol input[@name=departureAirport]").val().toLowerCase() == $("#formVol input[@name=arrivalAirport]").val().toLowerCase()) {
			
		html="<p><img class='alert' src='/img_opodo/homepageV2/alert3.gif' align='ATTENTION' />Les villes de départ et de retour doivent être différentes<img id='closeError' class ='closeError' border='0' alt='Fermer' src='/img_opodo/headfoot/icon-close.gif' /></p>";
		$("#errorVols").html(html);
		$("#errorVols").show();
			
		$('.closeError').click(function() {
			$("#errorVols").hide();
		});
		return false;
					
				
	}
			
	if ( (parseInt($("#formVol select[@name=numberOfAdults]").val()) + parseInt($("#formVol select[@name=numberOfChildren]").val())) > 9 ) {
			
		html="<p><img class='alert' src='/img_opodo/homepageV2/alert3.gif' align='ATTENTION' />La réservation de billets d'avion est limitée à 9 passagers<br/> au total.<img id='closeError' class ='closeError' border='0' alt='Fermer' src='/img_opodo/headfoot/icon-close.gif' /></p>";
		$("#errorVols").html(html);
		$("#errorVols").show();

		$('.closeError').click(function() {
			$("#errorVols").hide();
		});
		return false;
	}
				
	// erreurs sur les dates		
	if(!validRegexDate($('#DATE_DVText').val())  || !isDateValid($('#DATE_DVText').val())) {
			  
		html="<p><img class='alert' src='/img_opodo/homepageV2/alert3.gif' align='ATTENTION' />-Veuillez entrer une date de départ valide <br/>(format jj/mm/aaaa).<img id='closeError' class ='closeError' border='0' alt='Fermer' src='/img_opodo/headfoot/icon-close.gif' /></p>";
		$("#errorVols").html(html);
		$("#errorVols").show();
		
		$('.closeError').click(function() {
			$("#errorVols").hide();
		});
		return false;
			
		}
		
	// si c'est pas un aller simple on  verifie la date de retour.
	if(!$("#allerSimple").is(":checked")){
		if(!validRegexDate($('#DATE_RVText').val()) || !isDateValid($('#DATE_RVText').val())) {
							
			html="<p><img class='alert' src='/img_opodo/homepageV2/alert3.gif' align='ATTENTION' />-Veuillez entrer une date de retour valide <br/>(format jj/mm/aaaa).<img id='closeError' class ='closeError' border='0' alt='Fermer' src='/img_opodo/headfoot/icon-close.gif' /></p>";
			$("#errorVols").html(html);
			$("#errorVols").show();
			$('.closeError').click(function() {
			$("#errorVols").hide();
			});
		return false;
		}
	}
	

	
	var status = validateDates("errorVols","formVol", departureDateTab, returnDateTab, "D_ANYTIME", "R_ANYTIME");
	if(status==false)
		return false;
	
	
	return true
	
		
}

function initParamsAndSubmit() {

	var departureDateTab = $("#DATE_DVText").val().split("/");
	var returnDateTab = $("#DATE_RVText").val().split("/");	

// Initialisation des parametres et submit
	for (i=1 ;i<=9;i++){
		eval("document.formVol.TRAVELLER_TYPE_"+i+".value=''");
		eval("document.formVol.HAS_INFANT_"+i+".value='false'");
	}
	for (i=1 ;i<=$("#formVol select[@name=NB_ADULTS]").val();i++)
		eval("document.formVol.TRAVELLER_TYPE_"+i+".value='ADT'");
				
	for (i=$("#formVol select[@name=NB_ADULTS]").val()*1+1; i<= $("#formVol select[@name=NB_ADULTS]").val()*1+$("#formVol select[@name=NB_CHILDREN]").val()*1; i++)
		eval("document.formVol.TRAVELLER_TYPE_"+i+".value='CHD'");
	for (i=1 ;i<=$("#formVol select[@name=NB_INFANTS]").val();i++)
		eval("document.formVol.HAS_INFANT_"+i+".value='true'");
			
	// on rempli les champs cachés
		
	if(departureDateTab[1].toString().length==1)
		departureDateTab[1]="0"+departureDateTab[0];
  
	if(departureDateTab[1].toString().length==1)
		departureDateTab[1]="0"+departureDateTab[1];
  
	if(returnDateTab[0].toString().length==1)
		returnDateTab[0]="0"+returnDateTab[0];
	   
	if(returnDateTab[1].toString().length==1)
		returnDateTab[1]="0"+returnDateTab[1];
	
	$("#formVol input[@name=D_DATE]").val(departureDateTab[2]+''+ departureDateTab[1]+''+departureDateTab[0]);
	$("#formVol input[@name=R_DATE]").val(returnDateTab[2]+''+ returnDateTab[1]+''+returnDateTab[0]); 
						
	$('#formVol').submit();


}










