var browser 		= navigator.userAgent.toLowerCase();
var isIE 				= ((browser.indexOf( "msie" ) != -1) && (browser.indexOf( "opera" ) == -1) && (browser.indexOf( "webtv" ) == -1));





// position de la balse UL par rapport au conteneur DIV

/* INTEGRATION FLASH */
function flash(url, alttext, classe){
	if(!isIE){
			document.write('<object type="application/x-shockwave-flash"');
			document.write('  data="'+url+'" class="'+classe+'">');
	}else{
			document.write('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" class="'+classe+'"');
			document.write('  codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0">');
			document.write('  <param name="movie" value="'+url+'" />');
	}
	document.write('<param name="loop" value="true" />');
	document.write('<param name="menu" value="false" />');
  document.write('<param name="wmode" value="transparent" />');
	document.write('<p>'+alttext+'</p>');
	document.write('</object>');
}


/* CALENDRIER */
function validDate(d,m,y){
	// verifie la validite d'une date
 if(isNaN(d) || isNaN(m) || isNaN(y)){
		return false;
	}else{
	  chkDate = new Date(y,m,d);
	  if(d==chkDate.getDate() && m==chkDate.getMonth() && y==chkDate.getFullYear()){
	    return true;
	  }else{
			return false;
		}
	}
}

function calendrier(m,y,p) {
	  var now = new Date();
	  var page = p;
	  
		// pas d'arguments : la date courante est utilisée / un seul argument qui est l'id de page(paramètre m)
		if(p == undefined){
		  	d = now;
		  	if(m == undefined){
		  		// aucun argument => page_id nulle
		  		page = 1;
		  	}
		  	else {
		  		// 1 seul argument => page_id non nulle
		  		page = m;
		  	}
		}else{
		  	var d = new Date();
		  	d.setDate(1);
		  	d.setMonth(m);
				d.setFullYear(y);
				page=p;
		}
		// tableaux des mois/jours en français
		var mois 	= ["Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"];
		var njrs 	= [31,28,31,30,31,30,31,31,30,31,30,31];
		var jr 		= ["L","M","M","J","V","S","D"];
		var j = 1;
		// récupération du conteneur du tableau
		var cal = document.getElementById("calendrier-div");
		if(document.getElementById("calendrier")) cal.removeChild(document.getElementById("calendrier"));

		// création du tableau
		var tbl     = document.createElement("table");
		tbl.setAttribute("id", "calendrier");
		tbl.setAttribute("summary", "Calendrier");
		/*tbl.setAttribute("cellpadding", "0");
		tbl.setAttribute("cellspacing", "0");
		tbl.cellpadding = "0"; // fix bug IE
		tbl.cellspacing = "0"; // fix bug IE*/
		var tblHead = document.createElement("thead");
		var tblBody = document.createElement("tbody");

		for (var l = 0; l < 7; l++) {
			var row = document.createElement("tr");
			// menu navigation (mois précédent/suivant)
			if(l==0){
			  prevMonth = (d.getMonth()==0) ? 11 : d.getMonth()-1;
			  prevYear = (d.getMonth()==0) ? d.getFullYear()-1 : d.getFullYear();
				cell = document.createElement('th');
				a = document.createElement('a');
				a.setAttribute("href", "Javascript:calendrier("+prevMonth+","+prevYear+","+page+")");
				a.appendChild(document.createTextNode("<"));
				cell.appendChild(a);
				row.appendChild(cell);

				cell = document.createElement('th');
				cell.colSpan = "5";
				a = document.createElement('a');
				a.setAttribute("href", "Javascript:calendrier("+d.getMonth()+","+d.getFullYear()+","+page+")");
				a.appendChild(document.createTextNode(mois[d.getMonth()]+" "+d.getFullYear()));
				cell.appendChild(a);
				row.appendChild(cell);

			  nextMonth = (d.getMonth()==11) ? 0 : d.getMonth()+1;
			  nextYear = (d.getMonth()==11) ? d.getFullYear()+1 : d.getFullYear();
				cell = document.createElement('th');
				a = document.createElement('a');
				a.setAttribute("href", "Javascript:calendrier("+nextMonth+","+nextYear+","+page+")");
				a.appendChild(document.createTextNode(">"));
				cell.appendChild(a);
				row.appendChild(cell);

				tblHead.appendChild(row);

			// liste des jours de la semaine
			}else if(l==1){
				for (var i = 0; i < jr.length; i++) {
					cell = document.createElement('th');
					cell.appendChild(document.createTextNode(jr[i]));
					row.appendChild(cell);
				}
				tblHead.appendChild(row);

			// liste des dates
			}else{
				for (var i = 1; i <=jr.length; i++) {
				  dd = new Date(d.getFullYear(),d.getMonth(),j);
				  // date invalide
				  if(!validDate(j,d.getMonth(),d.getFullYear())){
						cell = document.createElement('td');
						cell.appendChild(document.createTextNode(" "));
						row.appendChild(cell);
				  // la date correspond au jour de la semaine (i) de la colonne en cours
				  }else if(dd.getDay()==i || (dd.getDay()==0 && i==7)){
						cell = document.createElement('td');
						a = document.createElement('a');
						var M = "" + (d.getMonth()+1);
					    var MM = "0" + M;
					    MM = MM.substring(MM.length-2, MM.length);
					    var D = "" + j;
					    var DD = "0" + D;
					    DD = DD.substring(DD.length-2, DD.length);
						a.setAttribute("href", "jsp/site/Portal.jsp?page=agenda&date="+DD+"/"+MM+"/"+d.getFullYear()+"&page_id="+page);
						a.appendChild(document.createTextNode(j));
						// date du jour => style différent
						if(now.getFullYear()==dd.getFullYear() && now.getMonth()==dd.getMonth() && now.getDate()==j) cell.setAttribute("id", "today");
						cell.appendChild(a);
						row.appendChild(cell);
						// ajout d'une ligne supplémentaire si le dernier jour du mois n'est pas atteint
						if(l==6 && i==7 && njrs[d.getMonth()]>j) l--;
						j++;
				  // case vide (date invalide ou ne correpond pas au jour de la semaine)
					}else{
						cell = document.createElement('td');
						cell.appendChild(document.createTextNode(" "));
						row.appendChild(cell);
					}
				}
				tblBody.appendChild(row);
			}
		}
		tbl.appendChild(tblHead);
		tbl.appendChild(tblBody);
		cal.appendChild(tbl);
	}

window.onload = function(){
  var t1 = document.getElementById("menu-gauche").offsetHeight;
  var t2 = document.getElementById("menu-droite").offsetHeight;
  var t3 = document.getElementById("contenu").offsetHeight;
  var t = (t1>t2) ? t1 : t2;
  if(t3<t) document.getElementById("contenu").style.height = t+'px';
}

function ancre_redirect(ancre_name){
	var ancre = document.location.href;
	var ancre_pos = ancre.lastIndexOf("#");
	var ancre_length =ancre.length;
	if(ancre_pos>0){
		document.location.href=ancre.substr(0,ancre_pos)+ancre_name;
	}
	else{
		document.location.href=document.location.href+ancre_name;
	}
}

function setMaxLength() {
	var x = document.getElementsByTagName('textarea');
	/* var counter = document.createElement('div'); 
	counter.className = 'counter';*/
	var counter = document.getElementById("countertext");
	for (var i=0;i<x.length;i++) {
		if (x[i].getAttribute('maxlength')) {
			var counterClone = counter.cloneNode(true);
			counterClone.relatedElement = x[i];
			counterClone.innerHTML = '<span>0</span> / '+x[i].getAttribute('maxlength')+" caract&egrave;res maximum";
			x[i].parentNode.insertBefore(counterClone,x[i].nextSibling);
			x[i].relatedElement = counterClone.getElementsByTagName('span')[0];

			x[i].onkeyup = x[i].onchange = checkMaxLength;
			x[i].onkeyup();
		}
	}
}

function checkMaxLength() {
	var maxLength = this.getAttribute('maxlength');
	var currentLength = this.value.length;
	var currentVal = this.value;
	var tbNbLigne =currentVal.split('\n');
	var nbLigne =tbNbLigne.length -1;
	if (nbLigne>0)
	{
		currentLength=currentLength + nbLigne;
	}
	if (currentLength > maxLength)
	{
		this.relatedElement.className = 'red';
	
	}
	else
		this.relatedElement.className = '';
	this.relatedElement.firstChild.nodeValue = currentLength;
	// not innerHTML
}