body {
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 10px;
background-color: #FFFFFF;
color : #5B7CD9;
margin: 0;
padding: 0;
height:100%;
}
p {
	margin: 0 0 5px 0;
	text-align : justify;
	padding-right:2px;	
}
dl, dt, dd, ul, li {
	margin: 0;
	padding: 0;
	list-style-type: none;
}
#conteneur {
	top:0px;
	position: relative;
	width: 1000px;
	left: 50%;
	margin-left: -500px;
	background-image:url( fond_accueil.gif);
	background-repeat:repeat-y;	
	height: auto;
}
/* ENTETE */	
#entete {
	margin-bottom: 0px;
	padding-bottom: 0px;	
	height: 150px;
	background-color: #FFFFFF;
}
/* PARTIE GAUCHE */	
#gauche {
	position: absolute;
	left:0px;
	width: 210px;	
	height: auto !important;
	height: 675px;
	min-height: 675px;
	background-image:url(logo_cg.jpg); 
	/* background-image:url(/fileadmin/templates/images/jaures/fond_menu.jpg); */
	background-position:bottom center;
	background-repeat:no-repeat;	
}

/* PARTIE CENTRALE */
#centre {
	font-size: 12px;	
	margin-left: 218px;
	margin-right: 380px;
	padding-left:2px;
	overflow: visible;	
	height: auto !important;
	height: 600px;
	min-height: 600px;
	background-color: #f6f6f6;
	background-image:url(bas_relief.jpg);
	background-position:bottom left;
	background-repeat:repeat-x;		
}
#centre a {
	color:#5B7CD9;
	font-weight: bold;
}
#centre a:hover {
	color:#80D559; 
	font-weight: bold;
}
#centre h6 {
	text-align : right;	
	padding-right : 5px;
}


#resume {
	position:relative;
}
#resume h1 {
	font-size: 18px;
	color:#5B7CD9; 
	font-weight: bolder;
	background: url(fleche.gif) no-repeat left;
	padding-top: 0px;	
	padding-left: 20px;
	font-family: Arial;
}
#resume h2 {
	font-size: 12px;
	font-weight: bold;
}	
#eadministration {
	position:relative;
	background-color: #F6F6F6;
}
#eadministration h1 {
	padding-left: 20px;
	padding-top: 2px;   
	font-size: 16px;
	font-weight: bolder;
	background: url(http://www.tarn.fr/fileadmin/templates/images/CG/puce_eadmin.gif) no-repeat left;
}	
#eadministration h2 {
	padding-left: 20px;
	font-size: 14px;
	font-weight: bold;
}	
/* PARTIE DROITE */
#droite {
	position: absolute;
	right:0;
	width: 375px;
}
#actus {
	margin-top: 1px;
	position:relative;
	background-color: #f6f6f6;
}
#breves {
	position:relative;
	font-size: 10px;
	margin-top: 7px;
	background-color: #f6f6f6;
	background-image:url(bas_relief.jpg);
	background-position:bottom left;
	background-repeat:no-repeat;	
}
#breves a {
	font-size: 12px;
	text-decoration: none;
	color: #5B7CD9;
 }
#breves a:hover {
     background: #5B7CD9;
     color: #ffffff;

     }
#breves a:active {
     background: #5B7CD9;
     color: #ffffff;
     }     
#infomedias {
	font-size: 10px;
	margin-top: 7px;
	position:relative;
	background-color: #f6f6f6;
	background-image:url(bas_relief.jpg);
	background-position:bottom left;
	background-repeat:no-repeat;	
}
#infomedias a {
     font-size: 12px;
     text-decoration: none;
     color: #5B7CD9;
 }
#infomedias a:hover {
     background: #5B7CD9;
     color: #ffffff;

     }
#infomedias a:active {
     background: #5B7CD9;
     color: #ffffff;
     }     
#zoom {
	background-color: #CCFFCC;
	height: auto !important;
	height: 550px;
	min-height: 550px;	
	margin-left: 2px;
	margin-right: 3px;
}
#zoom h1 {
	font-size: 12px;
	font-weight: bolder;
	background: #CCFFCC url(http://www.tarn.fr/fileadmin/templates/images/CG/puce_saviezvous.gif) no-repeat left;
	padding-left: 25px;
	padding-top: 4px;
	padding-bottom: 0px;
	padding-right: 7px;
	heigth: 20px;
	margin: 2;
}
#zoom h2 {
   font-size: 10px;	
   font-weight: normal;
   padding-left: 25px;
   padding-right: 7px;
   background: #CCFFCC;
   margin: 3;
}

/* PIED DE PAGE */
#pied {
	clear: both; 
	bottom: 0;
	margin: 0;
	padding: 0;
	padding-top: 10px;	
	background: #FFFFFF;
	height:50px;
}
/* MENU GAUCHE */
.menuG a {
     margin:5px;
     height: 20px;
     display: block;
     text-decoration: none;
     color: #5B7CD9;
     vertical-align:middle;
     font-weight: bold;
     font-size: 12px;     
	 }
	 
.menuG a:hover {
     background:#5B7CD9;
     color: #ffffff;
     text-decoration: none;
     }

.menuG a:active {
     background: #5B7CD9;
     color: #ffffff;
     text-decoration: none;
     }

.cg a {
   background: url(carre.gif) no-repeat left;
   padding-left: 10px;
   padding-top: 2px;   
	 }
     
.social a {
   background: url(carre.gif.1) no-repeat left;
   padding-left: 10px;
   padding-top: 2px;   
	 }

.dev a {
   background: url(carre.gif.2) no-repeat left;
   padding-left: 10px;
   padding-top: 2px;   
	 }

.education a {
   background: url(carre.gif.3) no-repeat left;
   padding-left: 10px;
   padding-top: 2px;   
	 }

/* SOUS MENU DROIT */

.menuD a {
     margin:7px;
     height: 20px;
     display: block;
     text-decoration: none;
     color: #fff;
     vertical-align:middle;
     font-size: 12px;
     background: #3366CC;
     text-align:center;
	 }
	 
.menuD a:hover {
     background: #FFFFFF;
     color: #000000;
     text-decoration: none;
     }

.menuD a:active {
     background: #FFFFFF;
     color: #000000;
     text-decoration: none;
     }
.tx-dcdflvplayer-pi1 { 
	margin:0px;
	padding:0px;
}
     
/* MENU PIED DE PAGE */

#pied a {
     margin:7px;
     padding-left:5px;
     padding-right:5px;
     height: 14px;
     display: block;
     text-decoration: none;
     color: #fff;
     vertical-align:middle;
     font-size: 12px;
	 }
	 
#pied a:hover {
     background: #FFFFFF;
     color: #000000;
     text-decoration: none;
     }

#pied a:active {
     background: #FFFFFF;
     color: #000000;
     text-decoration: none;
     }     