Object.extend(document, {
    isDocReady: false,
    isDocLoaded: false,
    ready: function(fn) { Event.observe(document, "doc:ready", fn); },
    load: function(fn) { Event.observe(document, "doc:loaded", fn); }
});
Event.observe(document, "dom:loaded", function() {
    Event.fire(document, "doc:ready");
    document.isDocReady = true;
    if (document.isDocLoaded)
        Event.fire(document, "doc:loaded");
});
Event.observe(window, "load", function() {
    document.isDocLoaded = true;
    if (!document.isDocReady) return;
    Event.fire(document, "doc:loaded");
});


/* Use the two following functions */
document.ready(function(){
    //ok to run some code
});
document.load(function(){
    //ok for DOM manipulation
    Event.observe(document.body, 'mouseup', function(e) {
    	irwButtonPressed = false;
    });
	// Check if the base target should be set to _self since this is needed by IHP2 in the Kiosk
	 if (window.dialogArguments) {
	  var mArgs = window.dialogArguments;
	  if (mArgs.kiosk) { 
	   var vBase = document.createElement('base');
	   vBase.setAttribute('target', '_self'); 
	   document.getElementsByTagName('head')[0].appendChild(vBase);
	  }
	 }
});

function irwInit() {
	if (navigator.platform.toLowerCase().indexOf('mac') != -1) {
		var css = new Element('link', {'href':'/ms/css/macos.css', 'type':'text/css', 'rel':'stylesheet'});
		$$('head')[0].insert(css);
	}
}

var irwButtonPressed = false;

/**
* This function is used by the createButton functions to prevent duplicate IDs on a page
*/
var buttons = new Hash();
function generateButtonID(_id) {
	var newid = _id;
	var counter = 0;
	if (buttons.get(newid) != null) {
		counter = parseInt(buttons.get(newid)) + 1;
		newid = _id + "_" + counter;
	}
	buttons.set(_id, counter);
	return newid;
}	

var buttonTemplate = new Template("<div class=\"buttonContainer\">" + 
																		"<a id=\"#{buttonId}\" class=\"#{buttonClass}\" href=\"#\" onclick=\"return false;\">" +
																			"<div class=\"buttonLeft#{buttonLeftClass}\">&nbsp;</div>" +
																			"<div class=\"buttonCaption#{buttonCaptionClass}\">" + 
																				"<input type=\"button\" value=\"#{buttonCaption}\" />" + 
																			"</div>" + 
																			"<div class=\"buttonRight#{buttonRightClass}\">&nbsp;</div>" + 
																		"</a>" + 
																	"</div>");

var scriptTemplate = new Template("<script type=\"text/javascript\">\n" + 
																		"Event.observe($('#{buttonId}'), 'click', function(e) { if (!this.hasClassName('disabledButton')) {#{buttonFunction};}});" + 
																		"var input = $('#{buttonId}').down('input');" + 
																		"Event.observe(input, 'mousedown' , function(e) { if (!this.hasClassName('pressed')) this.addClassName('pressed'); if (!this.hasClassName('down')) this.addClassName('down'); if (this.hasClassName('downNoMove')) this.removeClassName('downNoMove'); irwButtonPressed = true;	});" +
																		"Event.observe(input, 'mouseup' , function(e) { if (this.hasClassName('down')) this.removeClassName('down'); if (this.hasClassName('pressed')) this.removeClassName('pressed'); if (this.hasClassName('downNoMove')) this.removeClassName('downNoMove'); });" +
																		"Event.observe(input, 'mouseout' , function(e) {	if (this.hasClassName('down')) this.removeClassName('down'); if (this.hasClassName('downNoMove')) this.removeClassName('downNoMove'); var a = this.up('a'); if (a.hasClassName('hover')) a.removeClassName('hover'); });" +
																		"Event.observe(input, 'mouseover' , function(e) { var a = this.up('a'); if (!a.hasClassName('hover')) a.addClassName('hover'); if (this.hasClassName('pressed')) { if (irwButtonPressed) {	this.addClassName('down'); this.addClassName('downNoMove');	} else { this.removeClassName('pressed');	}	}	});" +
																	"</script>");

/**
* Creates new button with caption, class and listener function
* The listener function only executes if the button is enabled,
* that is the button does not contains the class 'disabledButton' 
*
* Each button gets a unique id, like irw_button_1, irw_button_2, etc.
* The function returns the generated ID
*/
function createButton(_caption, _id, _class, _func) {
	//check if there is only three parameters
	if (typeof(_func) == "undefined") {
		//assume that no id is specified
		_func = _class;
		_class = _id;
		_id = "irw_button";
	}
	var newid = generateButtonID(_id);
	var obj = new Object();
	obj.buttonId = newid;
	obj.buttonClass = _class;
	obj.buttonCaption = _caption;
	obj.buttonSize = _caption.length;
	obj.buttonLeftClass = "";
	obj.buttonCaptionClass = "";
	obj.buttonRightClass = "";	
	var reg = new RegExp("([a-zA-Z]+)Button","g");
	var m = null;
	var c = "";
	while (m = reg.exec(_class)) {
		if (m[1] != "disabled") {
			c += " " + m[1];
		}
	}
	if (c.length > 0) {
		var tmp = c.substring(1).split(' ');
		for (var i=0;i<tmp.length;i++) {
			obj.buttonLeftClass += " " + "buttonLeft" + tmp[i].substring(0,1).toUpperCase() + tmp[i].substring(1);
			obj.buttonCaptionClass += " " + "buttonCaption" + tmp[i].substring(0,1).toUpperCase() + tmp[i].substring(1);
			obj.buttonRightClass += " " + "buttonRight" + tmp[i].substring(0,1).toUpperCase() + tmp[i].substring(1);
		}
	}
	document.write(buttonTemplate.evaluate(obj));
	Event.observe($(newid), 'click', function(e) {
		if (!this.hasClassName('disabledButton')) {
			_func(e); 
		}
	});
	var input = $(newid).down('input');
	Event.observe(input, 'mousedown' , function(e) {
		if (!this.hasClassName('pressed')) this.addClassName('pressed');	
		if (!this.hasClassName('down')) this.addClassName('down');
		if (this.hasClassName('downNoMove')) this.removeClassName('downNoMove');
		irwButtonPressed = true;
	});
	Event.observe(input, 'mouseup' , function(e) {
		if (this.hasClassName('down')) this.removeClassName('down');
		if (this.hasClassName('pressed')) this.removeClassName('pressed');			
		if (this.hasClassName('downNoMove')) this.removeClassName('downNoMove');
	});
	Event.observe(input, 'mouseout' , function(e) {
		if (this.hasClassName('down')) this.removeClassName('down');
		if (this.hasClassName('downNoMove')) this.removeClassName('downNoMove');
		var a = this.up('a');
		if (a.hasClassName('hover')) a.removeClassName('hover');
	});
	Event.observe(input, 'mouseover' , function(e) {
		var a = this.up('a');
		if (!a.hasClassName('hover')) a.addClassName('hover');
		if (this.hasClassName('pressed')) {
			if (irwButtonPressed) {
				this.addClassName('down');
				this.addClassName('downNoMove');				
			} else {
				this.removeClassName('pressed');
			}
		}
	});
}

/**
* This function creates a string with the button and a script tag,
* which creates the observer. It is used by commerce for creating content
* dynamically, for example in the "Add to shopping cart" popup window.
* 
* It works in the same way as the previous function, except that the _func
* parameter is a string representation of a existing javascript function
*/
function createButtonLayout(_caption, _id, _class, _func) {
	//check if there is only three parameters
	if (typeof(_func) == "undefined") {
		//assume that no id is specified
		_func = _class;
		_class = _id;
		_id = "irw_button";
	}
	var newid = generateButtonID(_id);
	var newfunc = _func + (_func.indexOf('(') == -1 ? '(e)' : '');
	var obj = new Object();
	obj.buttonId = newid;
	obj.buttonClass = _class;
	obj.buttonCaption = _caption;
	obj.buttonSize = _caption.length;
	obj.buttonLeftClass = "";
	obj.buttonCaptionClass = "";
	obj.buttonRightClass = "";
	obj.buttonFunction = newfunc;	
	var reg = new RegExp("([a-zA-Z]+)Button","g");
	var m = null;
	var c = "";
	while (m = reg.exec(_class)) {
		if (m[1] != "disabled") {
			c += " " + m[1];
		}
	}
	if (c.length > 0) {
		var tmp = c.substring(1).split(' ');
		for (var i=0;i<tmp.length;i++) {
			obj.buttonLeftClass += " " + "buttonLeft" + tmp[i].substring(0,1).toUpperCase() + tmp[i].substring(1);
			obj.buttonCaptionClass += " " + "buttonCaption" + tmp[i].substring(0,1).toUpperCase() + tmp[i].substring(1);
			obj.buttonRightClass += " " + "buttonRight" + tmp[i].substring(0,1).toUpperCase() + tmp[i].substring(1);
		}
	}
	
	return buttonTemplate.evaluate(obj) + scriptTemplate.evaluate(obj);
}



/*  NLP Releated JS  */


function loadNlpProducts(placeHolder, marketCode, products) {
  //  alert(products.length);     
    var langs = marketCode.split("_");
    
    var params = "?type=xml&dataset=normal,prices,allimages,parentCategories";
    var prodTemplate = new Template("<div class=\"product\"><a class=\"image\" href=\"#{url}\"><img src=\"#{image}\" border=\"0\" /><img class=\"newLowerPriceImage\" src=\"/ms/flash/rooms_ideas/mpa2/images/logos/newlowerprice/#{market}/nlp_01.png\" border=\"0\" /></a><div class=\"name\">#{name}</div><div class=\"description\">#{description}</div><div class=\"link\"><a href=\"#{url}\">#{link}</a></div></div>");
    var btnTemplate = new Template("<a class=\"leftBtn\"   href=\"javascript:nlpMoveLeft('#{id}',#{count});\"></a><a href=\"javascript:nlpMoveRight('#{id}',#{count});\" class=\"rightBtn\"></a>");
    
    var addNlpProducts = function(xml) {

	     
        var p = xml.getElementsByTagName('product');
        var html = btnTemplate.evaluate({'id':placeHolder, 'count':products.length}); 
        html += "<div class=\"nlpClipArea\"><div class=\"products\" style=\"width:" + (142*products.length) + "px;\">";
        for (var i = 0; i < p.length; i++) {
            var obj = {};
            var item = p[i].getElementsByTagName('item')[0];
            obj.url = item.getElementsByTagName('URL')[0].childNodes[0].nodeValue;    
            obj.name = item.getElementsByTagName('name')[0].childNodes[0].nodeValue;
            obj.image = item.getElementsByTagName('images')[0].getElementsByTagName('thumb')[0].childNodes[0].childNodes[0].nodeValue;
            var price = item.getElementsByTagName('prices')[0].getElementsByTagName('normal')[0].getElementsByTagName('priceNormal')[0].childNodes[0].nodeValue;
            obj.description = products[i].description.replace('{0}',price);
            obj.link = products[i].link;
            obj.market = marketCode;
            html += prodTemplate.evaluate(obj);
        }
        html += "</div></div>";
            
        $(placeHolder).update(html);
                
    }    

    var requestUrl = "/" + langs[1].toLowerCase() + "/" + langs[0].toLowerCase() + "/catalog/products/";
    products.each(function(product) {
        requestUrl += product.id + ',';        
    });    
    requestUrl = requestUrl.substr(0,requestUrl.length-1) + params;
    
   // alert(requestUrl);
    
    new Ajax.Request(requestUrl, {
    	method: 'get',
    	contentType: 'application/xml',
    	onSuccess: function(response) {
    		addNlpProducts(response.responseXML);		
    	}
    });
            
}

function nlpMoveLeft(placeHolder, count) {
    var pDiv = $(placeHolder).select('.products')[0];
    var m = pDiv.style.width.match("([0-9]+)");
    var w = m != null && m.length > 0 ? m[0] : 0;
    var pw = w/count;
    m = pDiv.style.left.match("([0-9]+)");
    var cl = m != null && m.length > 0 ? m[0] : 0;
    var c = cl/pw;
    c = (c+count-1) % count;
    new Effect.Move(pDiv, { x: -c*pw, y: 0, mode: 'absolute', duration:0.0 });
}


function nlpMoveRight(placeHolder, count) {
    var pDiv = $(placeHolder).select('.products')[0];
    var m = pDiv.style.width.match("([0-9]+)");
    var w = m != null && m.length > 0 ? m[0] : 0;
    var pw = w/count;
    m = pDiv.style.left.match("([0-9]+)");
    var cl = m != null && m.length > 0 ? m[0] : 0;
    var c = cl/pw;
    c = (c+1) % count;
    new Effect.Move(pDiv, { x: -c*pw, y: 0, mode: 'absolute', duration:0.0 });    
}


function nlpMoveRighte(placeHolder, count) {
    var pDiv = $(placeHolder).select('.products')[0];
    var m = pDiv.style.width.match("([0-9]+)");
    var w = m != null && m.length > 0 ? m[0] : 0;
    var pw = w/count;
    m = pDiv.style.left.match("([0-9]+)");
    var cl = m != null && m.length > 0 ? m[0] : 0;
    var c = cl/pw;
    //alert("before"+c);
    var k = (c+1) % count;
    
    if (k == 0 )
    {
	c = c - 1 ;
    }
    else
    {
    c= (c+1) % count;
    }
     var c = (c+1) % count;
    
   // alert("after"+c);
    
    new Effect.Move(pDiv, { x: -c*pw, y: 0, mode: 'absolute', duration:0.0 });    
}







/*  NLP Releated JS ENDS  */








