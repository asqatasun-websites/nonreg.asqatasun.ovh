//Cette fonction renvoie TRUE si le format de la date
//est correctement tap� : DD/MM/YYYY
//Pour cela, j'ai utilis� l'objet Regular Expressions
function isDateFormat(sDate)
{
//Format de la date : DD/MM/YYYY � tester
var sDateFormat = /^\d{2}\/\d{2}\/\d{4}$/
var regex = new RegExp(sDateFormat);
return regex.test(sDate);
}


//Cette fonction renvoie TRUE si la Date est valide
//Elle attend une chaine qui correspond � la date Ex : (01/01/2001)
function isDate(sDate)
{
if(!isDateFormat(sDate)) return false;
var myArray = new Array();
myArray = sDate.split("/")
var iDay = parseInt(myArray[0]);
var iMonth = parseInt(myArray[1]);
var iYear = parseInt(myArray[2]);
//Si la date du mois ne correspond pas
// au max du mois
//Je soustrais de 1 le mois parce que �a commence par 0
if((iDay > getLastDay(iMonth - 1,iYear) || (iDay < 1)) || ((iMonth < 1) || (iMonth > 12)) || (iYear < 1970))
return false;
return true;
}

function MM_displayStatusMsg(msgStr) { //v2.0
  status=msgStr;
  document.MM_returnValue = true;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}  

//---- cache tous les calques ----
function MM_HideLayers() { 
	MM_showHideLayers('videos','','hide','publications','','hide','vivre5','','hide','enseignement','','hide','decouvrir','','hide','vivre','','hide','aide','','hide','enfance','','hide','environnement','','hide','conseil','','hide','conseil1','','hide','conseil2','','hide','conseil3','','hide','conseil4','','hide','conseil5','','hide','decouvrir1','','hide','decouvrir2','','hide','conseil6','','hide');
}  

//---- affichage calque DECOUVRIR ----
function MM_ShowLayerDec() { 
	MM_showHideLayers('videos','','hide','publications','','hide','vivre5','','hide','enseignement','','hide','decouvrir','','show','vivre','','hide','aide','','hide','enfance','','hide','environnement','','hide','conseil','','hide','conseil1','','hide','conseil2','','hide','conseil3','','hide','conseil4','','hide','conseil5','','hide','decouvrir1','','hide','decouvrir2','','hide','conseil6','','hide');
} 
//---- affichage calque DECOUVRIR 1 ----
function MM_ShowLayerDec1() { 
	MM_showHideLayers('videos','','hide','publications','','hide','vivre5','','hide','enseignement','','hide','decouvrir','','show','vivre','','hide','aide','','hide','enfance','','hide','environnement','','hide','conseil','','hide','conseil1','','hide','conseil2','','hide','conseil3','','hide','conseil4','','hide','conseil5','','hide','decouvrir1','','show','decouvrir2','','hide','conseil6','','hide');
} 

//---- affichage calque DECOUVRIR 2 ----
function MM_ShowLayerDec2() { 
	MM_showHideLayers('videos','','hide','publications','','hide','vivre5','','hide','enseignement','','hide','decouvrir','','show','vivre','','hide','aide','','hide','enfance','','hide','environnement','','hide','conseil','','hide','conseil1','','hide','conseil2','','hide','conseil3','','hide','conseil4','','hide','conseil5','','hide','decouvrir1','','hide','decouvrir2','','show','conseil6','','hide');
} 
//---- affichage calque VIVRE ----
function MM_ShowLayerViv() { 
	MM_showHideLayers('videos','','hide','publications','','hide','vivre5','','hide','enseignement','','hide','decouvrir','','hide','vivre','','show','aide','','hide','enfance','','hide','environnement','','hide','conseil','','hide','conseil1','','hide','conseil2','','hide','conseil3','','hide','conseil4','','hide','conseil5','','hide','decouvrir1','','hide','decouvrir2','','hide','conseil6','','hide');
} 
//---- affichage calque VIVRE 1 ----
function MM_ShowLayerViv1() { 
	MM_showHideLayers('videos','','hide','publications','','hide','vivre5','','hide','enseignement','','hide','decouvrir','','hide','vivre','','show','aide','','show','enfance','','hide','environnement','','hide','conseil','','hide','conseil1','','hide','conseil2','','hide','conseil3','','hide','conseil4','','hide','conseil5','','hide','decouvrir1','','hide','decouvrir2','','hide','conseil6','','hide');
} 
//---- affichage calque VIVRE 2 ----
function MM_ShowLayerViv2() { 
	MM_showHideLayers('videos','','hide','publications','','hide','vivre5','','hide','enseignement','','hide','decouvrir','','hide','vivre','','show','aide','','hide','enfance','','show','environnement','','hide','conseil','','hide','conseil1','','hide','conseil2','','hide','conseil3','','hide','conseil4','','hide','conseil5','','hide','decouvrir1','','hide','decouvrir2','','hide','conseil6','','hide');
} 
//---- affichage calque VIVRE 3 ----
function MM_ShowLayerViv3() { 
	MM_showHideLayers('videos','','hide','publications','','hide','vivre5','','hide','enseignement','','hide','decouvrir','','hide','vivre','','show','aide','','hide','enfance','','hide','environnement','','show','conseil','','hide','conseil1','','hide','conseil2','','hide','conseil3','','hide','conseil4','','hide','conseil5','','hide','decouvrir1','','hide','decouvrir2','','hide','conseil6','','hide');
} 
//---- affichage calque VIVRE 4 ----
function MM_ShowLayerViv4() { 
	MM_showHideLayers('videos','','hide','publications','','hide','vivre5','','hide','environnement','','hide','decouvrir','','hide','vivre','','show','aide','','hide','enfance','','hide','enseignement','','show','conseil','','hide','conseil1','','hide','conseil2','','hide','conseil3','','hide','conseil4','','hide','conseil5','','hide','decouvrir1','','hide','decouvrir2','','hide','conseil6','','hide');
} 
//---- affichage calque VIVRE 5 ----
function MM_ShowLayerViv5() { 
	MM_showHideLayers('videos','','hide','publications','','hide','vivre5','','show','environnement','','hide','decouvrir','','hide','vivre','','show','aide','','hide','enfance','','hide','enseignement','','hide','conseil','','hide','conseil1','','hide','conseil2','','hide','conseil3','','hide','conseil4','','hide','conseil5','','hide','decouvrir1','','hide','decouvrir2','','hide','conseil6','','hide');
} 
//---- affichage calque VIVRE 6 ----
function MM_ShowLayerViv6() { 
	MM_showHideLayers('videos','','hide','publications','','show','vivre5','','hide','environnement','','hide','decouvrir','','hide','vivre','','show','aide','','hide','enfance','','hide','enseignement','','hide','conseil','','hide','conseil1','','hide','conseil2','','hide','conseil3','','hide','conseil4','','hide','conseil5','','hide','decouvrir1','','hide','decouvrir2','','hide','conseil6','','hide');
} 
//---- affichage calque VIVRE 6 ----
function MM_ShowLayerViv7() { 
	MM_showHideLayers('videos','','show','publications','','hide','vivre5','','hide','environnement','','hide','decouvrir','','hide','vivre','','show','aide','','hide','enfance','','hide','enseignement','','hide','conseil','','hide','conseil1','','hide','conseil2','','hide','conseil3','','hide','conseil4','','hide','conseil5','','hide','decouvrir1','','hide','decouvrir2','','hide','conseil6','','hide');
} 
//---- affichage calque CONSEIL ----
function MM_ShowLayerCon() { 
	MM_showHideLayers('videos','','hide','publications','','hide','vivre5','','hide','enseignement','','hide','decouvrir','','hide','vivre','','hide','aide','','hide','enfance','','hide','environnement','','hide','conseil','','show','conseil1','','hide','conseil2','','hide','conseil3','','hide','conseil4','','hide','conseil5','','hide','decouvrir1','','hide','decouvrir2','','hide','conseil6','','hide');
} 
//---- affichage calque CONSEIL 1 ----
function MM_ShowLayerCon1() { 
	MM_showHideLayers('videos','','hide','publications','','hide','vivre5','','hide','enseignement','','hide','decouvrir','','hide','vivre','','hide','aide','','hide','enfance','','hide','environnement','','hide','conseil','','show','conseil1','','show','conseil2','','hide','conseil3','','hide','conseil4','','hide','conseil5','','hide','decouvrir1','','hide','decouvrir2','','hide','conseil6','','hide');
} 
//---- affichage calque CONSEIL 2 ----
function MM_ShowLayerCon2() { 
	MM_showHideLayers('videos','','hide','publications','','hide','vivre5','','hide','enseignement','','hide','decouvrir','','hide','vivre','','hide','aide','','hide','enfance','','hide','environnement','','hide','conseil','','show','conseil1','','hide','conseil2','','show','conseil3','','hide','conseil4','','hide','conseil5','','hide','decouvrir1','','hide','decouvrir2','','hide','conseil6','','hide');
} 
//---- affichage calque CONSEIL 3 ----
function MM_ShowLayerCon3() { 
	MM_showHideLayers('videos','','hide','publications','','hide','vivre5','','hide','enseignement','','hide','decouvrir','','hide','vivre','','hide','aide','','hide','enfance','','hide','environnement','','hide','conseil','','show','conseil1','','hide','conseil2','','hide','conseil3','','show','conseil4','','hide','conseil5','','hide','decouvrir1','','hide','decouvrir2','','hide','conseil6','','hide');
} 
//---- affichage calque CONSEIL 4 ----
function MM_ShowLayerCon4() { 
	MM_showHideLayers('videos','','hide','publications','','hide','vivre5','','hide','enseignement','','hide','decouvrir','','hide','vivre','','hide','aide','','hide','enfance','','hide','environnement','','hide','conseil','','show','conseil1','','hide','conseil2','','hide','conseil3','','hide','conseil4','','show','conseil5','','hide','decouvrir1','','hide','decouvrir2','','hide','conseil6','','hide');
} 
//---- affichage calque CONSEIL 5 ----
function MM_ShowLayerCon5() { 
	MM_showHideLayers('videos','','hide','publications','','hide','vivre5','','hide','enseignement','','hide','decouvrir','','hide','vivre','','hide','aide','','hide','enfance','','hide','environnement','','hide','conseil','','show','conseil1','','hide','conseil2','','hide','conseil3','','hide','conseil4','','hide','conseil5','','show','decouvrir1','','hide','decouvrir2','','hide','conseil6','','hide');
} 
//---- affichage calque CONSEIL 6 ----
function MM_ShowLayerCon6() { 
	MM_showHideLayers('videos','','hide','publications','','hide','vivre5','','hide','enseignement','','hide','decouvrir','','hide','vivre','','hide','aide','','hide','enfance','','hide','environnement','','hide','conseil','','show','conseil1','','hide','conseil2','','hide','conseil3','','hide','conseil4','','hide','conseil5','','hide','decouvrir1','','hide','decouvrir2','','hide','conseil6','','show');
} 

function UpdateLocation(list)
{
	url = list.options[list.selectedIndex].value;
	parent.location.href = url;
}

function OuvertureFenetre(list)
{
	url = list.options[list.selectedIndex].value;
	window.open(url,'','scrollbars,resizable,height=600,width=800');
}

function VerifSaisie() 
{
  if (document.ajout.selectlien.selected == "1")
  {
	alert("Veuillez renseigner votre message !");
	return false;
  }
return true;
}   

function VerifSaisieRecherche() 
{
  if (document.id.qu.value == "")
  {
	alert("Attention fonction recherche ! Veuillez renseigner un mot cl� ! ");
	return false;
  }
return true;
}  