/** Code by www.labsmedia.com && skyrock.com */
function catchClickHeat(e)
{
    try
    {
        showClickHeatDebug('Gathering click data...');
        
        if (clickHeatQuota == 0)
        {
            showClickHeatDebug('Click not logged: quota reached');
            
            return true;
        }
        
        if (clickHeatGroup == '')
        {
            showClickHeatDebug('Click not logged: group name empty (clickHeatGroup)');
            
            return true;
        }
        
        if (e == undefined)
        {
            e = window.event;
            c = e.button;
            element = e.srcElement;
        }
        else
        {
            c = e.which;
            element = null;
        }
        
        if (c == 0)
        {
            showClickHeatDebug('Click not logged: no button pressed');
            return true;
        }
        
        if (element != null && element.tagName.toLowerCase() == 'iframe')
        {
            if (element.sourceIndex == clickHeatLastIframe)
            {
                showClickHeatDebug('Click not logged: same iframe (happens when a click on iframe occured opening a popup and popup is closed)');
                
                return true;
            }
            clickHeatLastIframe = element.sourceIndex;
        }
        else
        {
            clickHeatLastIframe = -1;
        }
        
        var x = e.clientX;
        var y = e.clientY;
        if (y > clickHeatClickOffset.from)
        {
            y -= clickHeatClickOffset.offset;
        }
        if (y < 0)
        {
            showClickHeatDebug('Click not logged: out of document (negative value)');
            return true;
        }
        var w = clickHeatDocument.clientWidth != undefined ? clickHeatDocument.clientWidth : window.innerWidth;
        var h = clickHeatDocument.clientHeight != undefined ? clickHeatDocument.clientHeight : window.innerHeight;
        var scrollx = window.pageXOffset == undefined ? clickHeatDocument.scrollLeft : window.pageXOffset;
        var scrolly = window.pageYOffset == undefined ? clickHeatDocument.scrollTop : window.pageYOffset;
        
        if (x > w || y > h)
        {
            showClickHeatDebug('Click not logged: out of document (should be a click on scrollbars)');
            return true;
        }
        
        clickTime = new Date();
        if (clickTime.getTime() - clickHeatTime < 1000)
        {
            showClickHeatDebug('Click not logged: at least 1 second between clicks');
            return true;
        }
        clickHeatTime = clickTime.getTime();
        
        if (clickHeatQuota > 0)
        {
            clickHeatQuota = clickHeatQuota - 1;
        }

        params = 's=' + clickHeatSite + '&g=' + clickHeatGroup + '&x=' + (x + scrollx) + '&y=' + (y + scrolly) + '&w=' + w + '&b=' + clickHeatBrowser + '&c=' + c + '&random=' + (new Date()).getTime();
        showClickHeatDebug('Ready to send click data...');
        var sent = false;
        if (clickHeatServer.substring(0, 4) != 'http')
        {
            var xmlhttp = false;
            
            try
            {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (e)
            {
                try
                {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (oc)
                {
                    xmlhttp = null;
                }
            }
            
            if (!xmlhttp && typeof XMLHttpRequest != undefined)
                xmlhttp = new XMLHttpRequest();
            if (xmlhttp)
            {
                if (clickHeatDebug == true)
                {
                    xmlhttp.onreadystatechange =
                        function()
                        {
                            if (xmlhttp.readyState == 4)
                            {
                                if (xmlhttp.status == 200)
                                {
                                    showClickHeatDebug('Click recorded at ' + clickHeatServer + ' with the following parameters: x=' + (x + scrollx) + ' (' + x + 'px from left+'+scrollx+'px of horizontal scrolling) y='+(y+scrolly)+' ('+y+'px from top+'+scrolly+'px of vertical scrolling) width='+w+' browser='+clickHeatBrowser+' click='+c+' site='+clickHeatSite+' group='+clickHeatGroup+' Server answer: '+xmlhttp.responseText);
                                }
                                else if (xmlhttp.status == 404)
                                {
                                    showClickHeatDebug('click.php was not found at: '+(clickHeatServer!=''?clickHeatServer:'/clickheat/click.php')+' please set clickHeatServer value');
                                }
                                else
                                {
                                    showClickHeatDebug('click.php returned a status code '+xmlhttp.status+' with the following error: '+xmlhttp.responseText);
                                }
                            }
                        }
                }
                
                xmlhttp.open('GET', clickHeatServer + '?' + params, true);
                xmlhttp.setRequestHeader('Connection', 'close');
                xmlhttp.send(null);
                sent = true;
            }
        }
        
        if (sent == false)
        {
            if (clickHeatDebug == true)
            {
                showClickHeatDebug('Click recorded at '+clickHeatServer+' with the following parameters: x='+(x+scrollx)+' ('+x+'px from left+'+scrollx+'px of horizontal scrolling) y='+(y+scrolly)+' ('+y+'px from top+'+scrolly+'px of vertical scrolling) width='+w+' browser='+clickHeatBrowser+' click='+c+' site='+clickHeatSite+' group='+clickHeatGroup+' Server answer: '+'<iframe src="'+clickHeatServer+'?'+params+'" width="400" height="30"></iframe>');
            }
            else
            {
                var clickHeatImg = new Image();
                clickHeatImg.src = clickHeatServer+'?'+params;
            }
        }
    }
    catch(e)
    {
        showClickHeatDebug('An error occurred while processing click (Javascript error): ' + e.message);
    }
    
    return true;
}

var clickHeatServer = '/clickheat/click.php'; // ne marchera pas sur les urls rewrit�es.
var clickHeatLastIframe = -1;
var clickHeatTime = 0;
var clickHeatQuota = -1;
var clickHeatBrowser = '';
var clickHeatDocument = '';
var clickHeatDebug = (window.location.href.search(/debugclickheat/) != -1);
var clickHeatClickOffset = {from: 0, offset: 0};
function initClickHeat()
{
    if (typeof clickHeatGroup == 'undefined' || typeof clickHeatServer == 'undefined' || typeof clickHeatSite == 'undefined'
     || clickHeatGroup == '' || clickHeatServer == '' || clickHeatSite == '')
    {
        showClickHeatDebug('ClickHeat NOT initialised: either clickHeatGroup or clickHeatServer is empty');
        
        return false;
    }
    
    domain = window.location.href.match(/http:\/\/[^/]+\//);
    if (domain != null && clickHeatServer.substring(0, domain[0].length) == domain[0])
    {
        clickHeatServer = clickHeatServer.substring(domain[0].length-1, clickHeatServer.length)
    }
    if (typeof document.onmousedown == 'function')
    {
        currentFunc = document.onmousedown;
        document.onmousedown =
            function(e)
            {
                catchClickHeat(e);
                
                return currentFunc(e);
            }
    }
    else
    {
        document.onmousedown = catchClickHeat;
    }
    
    iFrames = document.getElementsByTagName('iframe');
    for (i = 0 ; i < iFrames.length ; i++)
    {
        if (typeof iFrames[i].onfocus == 'function')
        {
            currentFunc = iFrames[i].onfocus;
            iFrames[i].onfocus =
                function(e)
                {
                    catchClickHeat(e);
                    
                    return currentFunc(e);
                }
        }
        else
        {
            iFrames[i].onfocus = catchClickHeat;
        }
    }
    
    clickHeatDocument = document.documentElement != undefined && document.documentElement.clientHeight != 0 ? document.documentElement : document.body;
    var b = navigator.userAgent != undefined ? navigator.userAgent.toLowerCase().replace(/-/g, '') : '';
    clickHeatBrowser = b.replace(/iceweasel/, 'firefox').replace(/^.*(firefox|kmeleon|safari|msie|opera).*$/, '$1');
    if (b == clickHeatBrowser || clickHeatBrowser == '')
        clickHeatBrowser = 'unknown';
    
    // Gestion des pubs qui font un d�calage
    var p = document.getElementById('habillage_haut');
    if (p && p.clientHeight > 0)
    {
        showClickHeatDebug("Habillage haut displayed");
        clickHeatClickOffset.offset += p.clientHeight;
    }
    p = document.getElementById('habillage_haut2');
    if (p && p.clientHeight > 0)
    {
        showClickHeatDebug("Habillage haut2 displayed");
        clickHeatClickOffset.offset += p.clientHeight;
    }
    
    showClickHeatDebug('ClickHeat initialised with: site='+clickHeatSite+' group='+clickHeatGroup+' server='+clickHeatServer+' quota='+(clickHeatQuota==-1?'unlimited':clickHeatQuota)+' browser='+clickHeatBrowser);
}

function showClickHeatDebug(str)
{
    if (clickHeatDebug == true)
    {
        console.log(str);
    }
}

initClickHeat();
//onload_funcs.push(initClickHeat);
