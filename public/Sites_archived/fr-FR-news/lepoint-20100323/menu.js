var gl_menuBar_f = new Array(0, 0, 0, 0, 0);

function menuBar_on(num)
{
		/*
		** Si le menu sur lequel on a la souris n'est pas celui qui est actif
		*/
	if (gl_menuBar_f[num] != 1) {
			/*
			** Appel a menuBar, qui permet de passer toutes les classes à tabOff, sauf celle qui nous intéresse.
			*/
		menuBar(num);
	}
}

	/*
	** Fonction appellée par menuBar_on, passe tous les menu à off sauf le concerné (num)
	*/
function menuBar(num)
{ 
	gl_menuBar_f = new Array(0, 0, 0, 0, 0);
	gl_menuBar = new Array('actu', 'opinions', 'culture', 'tendances', 'ed-speciales');
		// Le menu actif est passé à 1
	gl_menuBar_f[num] = 1;
	var cpt;
		// On change la classe de tous les menus pour ne plus les afficher
	for (cpt = 0; cpt < 5; ++cpt) {
		document.getElementById("liste-menu-"+gl_menuBar[cpt]).style.display = "none";
		document.getElementById("liste-menu-"+gl_menuBar[cpt]).setAttribute("class", "menu-off");
	}
		// On affiche celui qui nous intéresse en changeant sa classe
	document.getElementById("menu-"+gl_menuBar[num]).setAttribute("class", "menu-on");
	document.getElementById("liste-menu-"+gl_menuBar[num]).style.display = "block";
	document.getElementById("liste-menu-"+gl_menuBar[num]).style.display = "block";
}