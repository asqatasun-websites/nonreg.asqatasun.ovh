//compte omniture 
s_account="opodofrprod";

// calque quartier � prendre en compte en fonction de l'OT
var tabLayer = 	new Array(
				new Array('NOM_OT',new Array('NOM_QUARTIER1','NOM_QUARTIER1','NOM_QUARTIER1','...')), // NE PAS EFFACER !
				new Array('MACAU',new Array('dv_quartier1','dv_quartier2','dv_quartier3')),
				new Array('WASHINGTON',new Array('dv_quartier1','dv_quartier2','dv_quartier3','dv_quartier4','dv_quartier5','dv_quartier6')));

var tabIdImg = 	new Array('pictoVol','pictoHotel','pictoDP','pictoVoiture','pictoSejour','pictoLocationSki');
var tabIdMoteur = 	new Array('moteurSearchVol','moteurSearchHotel','moteurSearchDP','moteurSearchVoiture','moteurSearchSejour','moteurSearchLocation');
var tabIdLien = new Array('aLinkVol','aLinkHotel','aLinkDP','aLinkVoiture','aLinkSejour','aLinkLocation');

var actifPicto = "";


$(document).ready(function() {
		
	// vols
	$('#aLinkVol').click(function() {
			CallToDisplay('Form','moteurSearchVol');
			return false
	});
	
	 $('#aLinkVol').mouseover(function(){
		
		callToImgNew('pictoVol');
		     
    });
	
	 $("#aLinkVol").mouseout(function(){
		callToImgSrc();
      
    })
	
	// hotel
	LoadMoteur('moteurSearchHotel','/fileadmin/engineV2/moteur_hotel.html','aLinkHotel','pictoHotel');
		
	// dp
	LoadMoteur('moteurSearchDP','/fileadmin/engineV2/moteur_dp.html','aLinkDP','pictoDP');
		
	// voitures
	LoadMoteur('moteurSearchVoiture','/fileadmin/engineV2/moteur_voiture.html','aLinkVoiture','pictoVoiture');
		
	// sejour
	
	LoadMoteur('moteurSearchSejour','/fileadmin/engineV2/moteur_sejour.html','aLinkSejour','pictoSejour');	
	
	// location
	LoadMoteur('moteurSearchLocation','/fileadmin/engineV2/moteur_location.html','aLinkLocation','pictoLocationSki');
	
	// france
	//LoadMoteur('moteurSearchFrance','/fileadmin/engineV2/moteur_france.php','aLinkFrance','pictoFrance');
	
});

var racine = 'fileadmin/scripts/recup.php?recup_url=http://vacances.opodo.fr/';

function LoadMoteur(idMoteur,urlMoteur,idbutton,pictoMoteur){

	$('#'+idbutton).click(function() {
		//$('#moteurSearch').addClass('loading');
		CallToDisplay('Form',idMoteur);
		
		$.ajax({ url: urlMoteur,
				dataType: "html",
				cache: false,
				success: function(html){
					$("#"+idMoteur).empty();
					$("#"+idMoteur).append(html);
					//$('#moteurSearch').removeClass('loading');
					if(idMoteur == "moteurSearchFrance"){
						$.getScript('http://www.opodo.fr/js_opodo/homepageV2/scriptMoteurFrance.js', function() {
						formulaires();
						});
					}
						
				}
		});
		

		
		return false
	});
	
	 $("#"+idbutton).mouseover(function(){
		
		callToImgNew(pictoMoteur);
		     
    });
	
	  $("#"+idbutton).mouseout(function(){
		callToImgSrc();
      
    })
	

}

function CallToHidden(id){
 	var n=0;
	if (id != 'Form'){
		for(i=0;i<tabLayer.length;i++){
			if (tabLayer[i][0] == id) n = i;
		}
		if (n > 0){
			for(i=0;i<tabLayer[n][1].length;i++)
				document.getElementById(tabLayer[n][1][i]).style.display = "none";
		} else
			document.getElementById(id).style.display = "none";
	} else {
		for(i=0;i<tabIdMoteur.length;i++) {
			if($('#'+tabIdMoteur[i]))
				document.getElementById(tabIdMoteur[i]).style.display = "none";
		}
	}
}


function CallToDisplay(id,calc){
	CallToHidden(id);	
	document.getElementById(calc).style.display = "block";
	if (id == 'Form'){
	 	var n=0;
		for(i=0;i<tabIdMoteur.length;i++){
			if (tabIdMoteur[i] == calc) n = i;
		}
		actifPicto = tabIdImg[n]; 
		callToImgNew(tabIdImg[n]);
		changeClassLink(tabIdLien[n]);
	}
}



function redirecTo(url){
	document.location.href = url;
}
function callToImgSrc(){
	for(i=0;i<tabIdImg.length;i++){
		if (tabIdImg[i] != actifPicto)
			
			document.getElementById(tabIdImg[i]).src = "/img_opodo/homepageV2/" + tabIdImg[i] + ".jpg";
	}
	return false;
}
function callToImgNew(id){
	callToImgSrc();
	document.getElementById(id).src = "/img_opodo/homepageV2/" + id + "On.jpg";
}

function initialLink(){
	var n = 0;
	for(i=0;i<tabIdImg.length;i++){
		if (tabIdImg[i] == actifPicto) n = i;
	}
	for(i=0;i<tabIdLien.length;i++){
		if (i != n)
			document.getElementById(tabIdLien[i]).style.color = "";
	}
}
function changeClassLink(id){
	initialLink();
	document.getElementById(id).style.color = "#FF6600";
}


//	tracking omniture
/*function tracking(aElement,name){
	//alert("aElement = "+aElement+" name = "+name);
	if(name =="" || name==null)   name = "Unknown Link";
	s_objectID=name;
	var s=s_gi('opodofrprod');
	s.linkTrackVars='prop41';s.prop41=name;
	s.tl(aElement,'o',name);
	s.linkTrackVars='None';
	return true;
}*/