var playVideo = function(swfURL, url){
	var flashvars = {};
	flashvars.url = url;
	flashvars.xitiPage = ""; // tag de page
	flashvars.xitiS2 = ""; // tag de sous-site, si elle n’est pas spécifiée, elle vaudra  1  par défaut 
	var params = {};
	params.menu = "false";
	params.allowScriptAccess = "always";
	params.allowFullScreen = "true";
	params.quality = "high";
	var attributes = {};
	attributes.id = "videoPlayer";
	swfobject.embedSWF(swfURL, "videoPlayer", "480", "387", "9.0.0", "", flashvars, params, attributes);
}

var initVideoPopin = (function () {		
	//Creer les divs 
	var divs = '<div id="mask" style="cursor: pointer;"></div><div id="popin"><div class="popin-contenu"><h1>Vid&eacute;o</h1><div class="popin-contenu-int"><div id="videoPopin"><div style="position: absolute;"><img src="file/sitemodel/interpe/images/commun/loading.gif" alt="Loading..." /></div><div id="videoPlayer"><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></div></div><p><br /><a class="bt-quitter closePopin" href="#">Quitter</a></p><br class="clear" /></div></div></div>';
	
	var oPage = $(".page");
	if (!oPage) return;
	$(".page").after(divs);
	// cacher les div popin	
	$("#popin").hide();  
	$("#mask").hide();
   initPlayVideo();
});

var initPlayVideo = (function () {
 $("a.playVideo").bind("click", function(){
	 playVideo($(this).attr("href"), $(this).attr("rel"));
	 showVideoPopin();
	 //afficher le popin
	 $("#popin").show();
	 $("#popin").css("z-index","10000000");
    $("#popin").css("opacity", "0.1");
    $("#popin").animate({
     opacity: 1
    }, 500 );
	 window.location = "#"; 
	 return false;
 });
});

var showVideoPopin = (function () {
	 if($.browser.msie){
		// virer les selects pour IE
		$("select").css("visibility","hidden");
		$("div#popin select").css("visibility","visible");
	 }
	 if((navigator.userAgent.toLowerCase().indexOf("mac")!=-1)?true:false){
		// on masque les Flashs sur Mac
		$("embed").css("visibility","hidden");
	 }
    //masquer les div contenant du flash
    //if($(".embed").is("div")){$(".embed").css("visibility","hidden");}
	 //hauteur du masque
	 if (typeof(window.innerHeight)=="number") {
  		wHeight=window.innerHeight;
  	}
  	else {
  		if (document.documentElement && document.documentElement.clientHeight && parseInt(document.documentElement.clientHeight) !=0 ) {
  			wHeight = document.documentElement.clientHeight;
  		}
  		else {
  			if (document.body&&document.body.clientHeight){
  				wHeight=document.body.clientHeight;
  			}
  		}
  	}
	 var pageH = $(".page").height();
	 var maskHeight;
	 if(wHeight>pageH){
	 	maskHeight = wHeight;
	 }
	 else{
	 	maskHeight = pageH;
	 }
	 $("#mask").height(maskHeight+"px");
	 $("#mask").show();
    $("#mask").css("opacity", "0.1");
    $("#mask").animate({
     opacity: 0.7
    }, 500 );
});


var initClosePopin = (function () {
   $("#mask").bind("click", function(){
		hideVideoPopin();
		return false;
	});
	$(".closePopin").bind("click", function(){
		hideVideoPopin();
		return false;
	});
	return false;
});

var hideVideoPopin = (function () {	
	$("#popin").hide();
	$("#mask").hide();
	$("#videoPlayer").remove();
	$("#videoPopin").append('<div id="videoPlayer"></div>');
	
	
	if($.browser.msie){
		$("select").css("visibility","visible");
	}
	if((navigator.userAgent.toLowerCase().indexOf("mac")!=-1)?true:false){
		$("embed").css("visibility","visible");		
	}
   //afficher les div contenant du flash
   //if($(".embed").is("div")){$(".embed").css("visibility","visible");}
	return false;
});

$(document2).ready(function () {
   initVideoPopin();
	initClosePopin();
});