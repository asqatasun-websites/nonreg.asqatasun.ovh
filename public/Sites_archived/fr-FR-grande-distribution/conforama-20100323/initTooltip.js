var initToolTip = (function() {
	$(".tooltip").mouseover(function() {      
      tooltip.show(this);
      return false;
   });
	$(".tooltip").mouseout(function() {      
      tooltip.hide(this);
      return false;
   });
});

//Toolip pour popin
var initToolTipForPopin = (function() {
	$(".tooltipPopin").mouseover(function() {      
      tooltip.show(this);
      return false;
   });
	$(".tooltipPopin").mouseout(function() {      
      tooltip.hide(this);
      return false;
   });
});

$(document).ready(function(){
	initToolTip();
});