var doMove = true;
var monstersky=(function(){

	/* timer utils */
	var _last_scroll=0;
	var _current_loop=0;
	var _window_y=0;
	
	/* animation prefs */
	var _speedreduction=10;
	var _delay = 10;
	var _height_monstersky = 770;
	
	return {
		relocate:function() {
			var isIE = (document.all) ? 1 : 0;
			var last_window_y = _window_y;
			if (isIE) {
				_window_y=document.documentElement.scrollTop;
			}
			else {
				_window_y = window.pageYOffset;
			}
			if (last_window_y==_window_y && doMove==true) {
				_current_loop++;
				if (_current_loop>_last_scroll+_delay) {
					var monstersky_y = $("#monstersky").position().top;
					var new_y = ( _window_y + (_speedreduction*monstersky_y) ) / (_speedreduction+1);
					if ( new_y > $("#wrapper").height() - _height_monstersky ) {
						new_y = $("#wrapper").height() - _height_monstersky;
					}
					if ((new_y-monstersky_y)!=0) {
						new_y = new_y.toString()+"px";
						$("#monstersky").css({"top": new_y});
					}
}
			}
			else {
				_last_scroll = _current_loop;
			}
		}
	}
	
})();


$(document).ready(function(){
	if ( document.getElementById("monstersky") != null ) {
		window.setInterval("monstersky.relocate()", 25);
	}
});