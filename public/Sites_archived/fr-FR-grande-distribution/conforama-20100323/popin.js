var initPopin = (function () {   
   // cacher les div popin
	$("div.popin").hide();
	$("#masque").click(function(){
		hidePopin();
	});
   $("#loading").click(function(){
		hidePopin();
	});
	initClosePopin();	
});

var showPopin = (function () {  
   if($.browser.msie){
		// virer les selects pour IE
		$("select").css("visibility","hidden");
		$("div.popin select").css("visibility","visible");
	}
	if((navigator.userAgent.toLowerCase().indexOf("mac")!=-1)?true:false){
		// on masque les Flashs sur Mac (wmode dans les choux)
		$("embed").css("visibility","hidden");
	}
    //masquer les div contenant du flash
    if($(".embed").is("div")){$(".embed").css("visibility","hidden");}
    $("div.popin").show();
	return false;
});

var initPopinH = (function (){
	var pageH = $("#contentAllWebsite").height();
	var popinH = $("div.popin").height()+50;
	if(popinH>pageH){
		$("#masque").height(popinH +"px");
   }
});

var hidePopin = (function () {
	$("#cssColorTemp").attr("href", "/webapp/wcs/stores/servlet/Conforama/css/general/common/color_temp.css");
	$("#cssColorLangTemp").attr("href", "/webapp/wcs/stores/servlet/Conforama/css/general/common/color_temp.css");
    $("#cssPrintTemp").attr("href", "/webapp/wcs/stores/servlet/Conforama/css/general/common/print_temp.css"); // Ajout Bi 7/01/08
	$("div.popin").empty();
	$("#masque").hide();
	$("#loading").hide();
	$("div.popin").hide();
	if($.browser.msie){
		$("select").css("visibility","visible");
	}
	if((navigator.userAgent.toLowerCase().indexOf("mac")!=-1)?true:false){
		$("embed").css("visibility","visible");
		
	}
   //afficher les div contenant du flash
   if($(".embed").is("div")){$(".embed").css("visibility","visible");}
	initPopinDeclanche();
	return false;
});

var initClosePopin = (function () {
	initPopinH();
	$("a.closePopin").click(function(){
		hidePopin();
		if($(this).is('.showRegistrationForm')) {
			showRegistrationForm();
		}
		return false;
	});
	
	$("div#contentPopin.reloadOnClose a.closePopin").click(reloadUnderPopin);
});


// Ajouter par NTO pour la popin please Wait
// voir pour l'int�grer dans un js � part
jQuery.fn.extend({
   findPos : function() {
       obj = jQuery(this).get(0);
       var curleft = obj.offsetLeft || 0;
       var curtop = obj.offsetTop || 0;
       while (obj = obj.offsetParent) {
                curleft += obj.offsetLeft
                curtop += obj.offsetTop
       }
       return {x:curleft,y:curtop};
   }
});


// Ajout� par NTO pour la gestion de la popin pleaseWait
function PleaseWaitPopin () {
	this.mask = $("#mask");
	this.pageHeight = $("#contentAllWebsite").height();
	this.pageWidth = $("#contentAllWebsite").width();
	this.popin = $("#pleaseWaitPopin");
	this.loading = $("#pleaseWaitLoading");
}

PleaseWaitPopin.prototype  = {
	setPopin: function(param) {
		this.popin = $("#"+param);
    },
    setDomContent: function(content) {
    	this.popin.html(content);	
    },
    show: function() {
    	this.initMask();
    	this.popin.show();
		this.initPositions();
    	this.loading.show();
    	this.keepAnimation();
    },
    initMask: function() {
    	var maskHeight = 1024;
    	//revient en haut de page
    	window.location="#header";
    	if(this.pageHeight > maskHeight) {
    		maskHeight = this.pageHeight;		
    	}
		this.mask.height(maskHeight+"px");
		this.mask.show();
    },
    initPositions: function() {
    	var popinPosition = this.popin.findPos();
		var popinWidth = this.popin.width();
		var loadingWidth = this.loading.width();
		// on fixe la propri�t� left (au lieu du pourcentage)
		var popinLeft = (this.pageWidth-popinWidth)/2;
		//this.popin.css("left",popinPosition.x +"px");
		this.popin.css("left",popinLeft +"px");
		var loadingLeft = popinLeft + 204;
		this.loading.css("top",(popinPosition.y + 110)+"px");
		this.loading.css("left",loadingLeft+"px");
    },
    keepAnimation: function() {
        this.loading.css("visibility","visible");
    	$("#pleaseWaitLoadingImg").css("visibility","visible");
    	var image = document.getElementById("pleaseWaitLoadingImg");
    	setTimeout(function() {image.src = image.src;},1);
        
    }
}

function PleaseWaitPopinCustom () {
	this.mask = $("#masque");
	this.pageHeight = $("#contentAllWebsite").height();
	this.popin = $("#pleaseWaitPopinCustom");
	this.loading = $("#pleaseWaitLoading");
}

PleaseWaitPopinCustom.prototype  = {
    setDomContent: function(content) {
    	this.popin.html(content);	
    },
    show: function() {
    	this.initMask();
    	this.popin.show();
		this.initPositions();
    	this.loading.show();
    	this.keepAnimation();
    },
    initMask: function() {
    	//revient en haut de page
    	window.location="#header";
		this.mask.height(this.pageHeight+"px");
		this.mask.show();
    },
    initPositions: function() {
    	var popinPosition = this.popin.findPos();
		var popinWidth = this.popin.width();
		var loadingWidth = this.loading.width();
		var loadingLeft = popinPosition.x + 204;
		// on fixe la propri�t� left (au lieu du pourcentage)
		this.popin.css("left",popinPosition.x +"px");
		this.loading.css("top",(popinPosition.y + 110)+"px");
		this.loading.css("left",loadingLeft+"px");
    },
    keepAnimation: function() {
        this.loading.css("visibility","visible");
    	$("#pleaseWaitLoadingImg").css("visibility","visible");
    	var image = document.getElementById("pleaseWaitLoadingImg");
    	setTimeout(function() {image.src = image.src;},1);
        
    }
}

// Ajout� par NTO pour la gestion de la popin pleaseWait
function PleaseWaitPopin () {
	this.mask = $("#mask");
	this.pageHeight = $("#contentAllWebsite").height();
	this.pageWidth = $("#contentAllWebsite").width();
	this.popin = $("#pleaseWaitPopin");
	this.loading = $("#pleaseWaitLoading");
}

PleaseWaitPopin.prototype  = {
	setPopin: function(param) {
		this.popin = $("#"+param);
    },
    setDomContent: function(content) {
    	this.popin.html(content);	
    },
    show: function() {
    	this.initMask();
    	this.popin.show();
		this.initPositions();
    	this.loading.show();
    	this.keepAnimation();
    },
    initMask: function() {
    	var maskHeight = 1024;
    	//revient en haut de page
    	window.location="#header";
    	if(this.pageHeight > maskHeight) {
    		maskHeight = this.pageHeight;		
    	}
		this.mask.height(maskHeight+"px");
		this.mask.show();
    },
    initPositions: function() {
    	var popinPosition = this.popin.findPos();
		var popinWidth = this.popin.width();
		var loadingWidth = this.loading.width();
		// on fixe la propri�t� left (au lieu du pourcentage)
		var popinLeft = (this.pageWidth-popinWidth)/2;
		//this.popin.css("left",popinPosition.x +"px");
		this.popin.css("left",popinLeft +"px");
		var loadingLeft = popinLeft + 204;
		this.loading.css("top",(popinPosition.y + 110)+"px");
		this.loading.css("left",loadingLeft+"px");
    },
    keepAnimation: function() {
        this.loading.css("visibility","visible");
    	$("#pleaseWaitLoadingImg").css("visibility","visible");
    	var image = document.getElementById("pleaseWaitLoadingImg");
    	setTimeout(function() {image.src = image.src;},1);
        
    }
}

function PreHomePage () {
	this.hiddenPreHomePage = $("#hiddenPreHomePage");
	this.preHomePage = $("#preHomePage");
	this.preHomePageContent = $("#preHomePageContent");
	this.today = new Date();
	this.startDate = null;
	this.endDate = null;
	this.delay = 0;
	this.URL = "";
	this.showCount = 0;
	this.viewCount = 0;
	this.isAlreadyHidden = false;
	this.visible = false;
	this.exists = false;
	this.external = "";
	this.externalEmpty = true;
	this.elementsToRestoreList = null;
	this.defaultHeight = 600;
	this.cookieExpirationDelay = 30;
}

PreHomePage.prototype = {
    setContent: function() {
		var objet = this;
		$.get(this.URL,function(data){objet.hiddenPreHomePage.html(data);});
 		var obj = '<iframe src="'+ this.URL +'" scrolling=no frameBorder=0 style="width: 100%; height: ' + this.defaultHeight + 'px; margin: 0;" />';
		this.preHomePageContent.html(obj);
		this.hiddenPreHomePage.empty();	
		this.hiddenPreHomePage.hide();	
		
    },
    show: function() {
    	if(this.isVisible()) {
    		this.setContent();
	    	this.initSize();
	    	var object = this;
   			this.preHomePage.show();
    		this.setViewCount(object.viewCount+1);
    		setTimeout(function(){object.hide()},object.delay*1000); 
	    } else {
	    	this.restoreElements();
	    }
    },
    hide: function() {
    	if(!this.isAlreadyHidden) {
    		this.preHomePage.hide();
    		this.preHomePageContent.empty();
    		this.isAlreadyHidden = true;
	    	this.restoreElements();
    	}
    },
    initSize: function() {
		var pageHeight = $("#contentAllWebsite").height();
		var pageWidth = $("#contentAllWebsite").width();
    	//revient en haut de page
    	window.location="#header";
		this.preHomePage.height(pageHeight+"px");
		this.preHomePage.width(pageWidth+"px");
    },
    setExists: function(value) {
		this.exists = value;
    },
    setExternalEmpty: function(value) {
		this.externalEmpty = value;
    },
    getViewCount: function() {
    	if($.cookie('viewCount')!=null) {
			this.viewCount = parseInt($.cookie('viewCount'));
		}
    },
    setViewCount: function(value) {
		$.cookie('viewCount', value, { expires: this.cookieExpirationDelay });
    },
    setDelay: function(delay) {
        this.delay = delay;       
    },
    setURL: function(url) {
        this.URL = url;    
    },
    setStartDate: function(year, month, day) {
		this.startDate = new Date(year, month, day);
    },
    setEndDate: function(year, month, day) {
    	// on fixe � jour +1 car un new Date fixe l'information de l'heure
		this.endDate = new Date(year, month, day+1);
    },
    setShowCount: function(value) {
		this.showCount = value;	
    },
    isVisible: function() {
    	var isVisible = false;
    	var isVisibleLog = "";
    	if(this.exists) {
    		this.getViewCount();
    		// Attention le test depend de l'heure actuelle 
    		if(this.today >= this.startDate && this.today < this.endDate) {
    			if(this.showCount > this.viewCount) {
    				if(this.delay > 0) {
	    				if(!this.externalEmpty) {
	    					isVisible = true;
	    				} else {
	    					isVisibleLog = "l'url externe est vide en base";		
	    				}
	    			} else {
	    				isVisibleLog = "le delais d'affichage n'est pas superieur a 0 seconde";	
	    			}
    			} else {
    				isVisibleLog = "la limite d'affichage a l'utilisateur a ete atteinte";
    			}
    		} else {
    			isVisibleLog = "la date du jour n'est pas comprise dans l'intervalle defini parstartDate et endDate";
    		}
    	} else {
			isVisibleLog = "le bean n'existe pas";
		}
		//alert("reason : " + isVisibleLog);
		this.visible = isVisible;
		return isVisible;
    },
    setElementsToRestoreList: function(array) {
    	this.elementsToRestoreList = array;
    }, 
    restoreElements: function() {
    	jQuery.each(this.elementsToRestoreList, function() {
      		$("#" + this).css("visibility","visible");
    	});
    }
}