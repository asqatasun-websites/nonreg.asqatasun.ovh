// Javascript pour la fonction du moteur DP 

// DP departure cities management
function DepartureCity(code, city) {
	this.code = code;
	this.city = city;
}		

var	dpDepartureCities = new Array;
var i = 0;
dpDepartureCities[i++] = new DepartureCity("PAR", "Paris");
dpDepartureCities[i++] = new DepartureCity("AJA", "Ajaccio");
dpDepartureCities[i++] = new DepartureCity("MLH", "B&acirc;le-Mulhouse");
dpDepartureCities[i++] = new DepartureCity("BIA", "Bastia");
dpDepartureCities[i++] = new DepartureCity("BIQ", "Biarritz");
dpDepartureCities[i++] = new DepartureCity("BOD", "Bordeaux");
dpDepartureCities[i++] = new DepartureCity("BES", "Brest");
dpDepartureCities[i++] = new DepartureCity("CFE", "Clemont-Ferrand");
dpDepartureCities[i++] = new DepartureCity("FDF", "Fort-de-France");
dpDepartureCities[i++] = new DepartureCity("LIL", "Lille");
dpDepartureCities[i++] = new DepartureCity("LYS", "Lyon");
dpDepartureCities[i++] = new DepartureCity("MRS", "Marseille");
dpDepartureCities[i++] = new DepartureCity("MPL", "Montpellier");
dpDepartureCities[i++] = new DepartureCity("NTE", "Nantes");
dpDepartureCities[i++] = new DepartureCity("NCE", "Nice");
dpDepartureCities[i++] = new DepartureCity("PTP", "Pointe-&agrave;-Pitre");
dpDepartureCities[i++] = new DepartureCity("RNS", "Rennes");
dpDepartureCities[i++] = new DepartureCity("SXB", "Strasbourg");
dpDepartureCities[i++] = new DepartureCity("TLN", "Toulon");
dpDepartureCities[i++] = new DepartureCity("TLS", "Toulouse");
dpDepartureCities[i++] = new DepartureCity("", "-----------------");
dpDepartureCities[i++] = new DepartureCity("AMS", "Amsterdam");
dpDepartureCities[i++] = new DepartureCity("BCN", "Barcelone");
dpDepartureCities[i++] = new DepartureCity("BRU", "Bruxelles");
dpDepartureCities[i++] = new DepartureCity("CGN", "Cologne");
dpDepartureCities[i++] = new DepartureCity("FRA", "Francfort");
dpDepartureCities[i++] = new DepartureCity("GVA", "Gen&egrave;ve");
dpDepartureCities[i++] = new DepartureCity("LIS", "Lisbonne");
dpDepartureCities[i++] = new DepartureCity("LON", "Londres");
dpDepartureCities[i++] = new DepartureCity("LUX", "Luxembourg");
dpDepartureCities[i++] = new DepartureCity("MAD", "Madrid");
dpDepartureCities[i++] = new DepartureCity("MIL", "Milan");
dpDepartureCities[i++] = new DepartureCity("YMQ", "Montr&eacute;al");
dpDepartureCities[i++] = new DepartureCity("OPO", "Porto");
dpDepartureCities[i++] = new DepartureCity("ROM", "Rome");
dpDepartureCities[i++] = new DepartureCity("VIE", "Vienne ");
dpDepartureCities[i++] = new DepartureCity("ZRH", "Zurich");



function writeDpDepartureOptions(){
	for (var i = 0; i < dpDepartureCities.length; i++)
		document.write('<option value="'+dpDepartureCities[i].code+'">'+dpDepartureCities[i].city+'</option>'+'\n');
}

//Departure and return hour
	function Hour(code, hour) {
      this.code = code;
      this.hour = hour;
   }

	var dpHour = new Array;
	dpHour[0] = new Hour("", "Horaire indiff�rent");
	dpHour[1] = new Hour("MORNING", "Matin (9:00-12:00)");
	dpHour[2] = new Hour("AFTERNOON", "Apr�s-Midi (12:00-18:00)");
	dpHour[3] = new Hour("EVENING", "Soir�e (18:00-00:00)");
	dpHour[4] = new Hour("0000", "00:00");
	dpHour[5] = new Hour("0100", "01:00");
	dpHour[6] = new Hour("0200", "02:00");
	dpHour[7] = new Hour("0300", "03:00");
	dpHour[8] = new Hour("0400", "04:00");
	dpHour[9] = new Hour("0500", "05:00");
	dpHour[10] =new Hour("0600", "06:00");
	dpHour[11] = new Hour("0700", "07:00");
	dpHour[12] = new Hour("0800", "08:00");
	dpHour[13] = new Hour("0900", "09:00");
	dpHour[14] = new Hour("1000", "10:00");
	dpHour[15] = new Hour("1100", "11:00");
	dpHour[16] = new Hour("1200", "12:00");
	dpHour[17] = new Hour("1300", "13:00");
	dpHour[18] = new Hour("1400", "14:00");
	dpHour[19] = new Hour("1500", "15:00");
	dpHour[20] = new Hour("1600", "16:00");
	dpHour[21] =  new Hour("1700", "17:00");
	dpHour[22] = new Hour("1800", "18:00");
	dpHour[23] = new Hour("1900", "19:00");
	dpHour[24] = new Hour("2000", "20:00");
	dpHour[25] = new Hour("2100", "21:00");
	dpHour[26] = new Hour("2200", "22:00");
	dpHour[27] = new Hour("2300", "23:00");

	function writeHourOptions() {
		
		for (var i = 0; i < dpHour.length; i++)
		document.write('<option value="'+	dpHour[i].code+'">'+	dpHour[i].hour+'</option>'+'\n');
	}
	




// Affichage des Jours, Mois et Ann�e et horaire D�part et Retour
var dpDelay = 3;
var all_month = new Array("JAN", "FEV", "MAR", "AVR", "MAI", "JUN", "JUL", "AOU", "SEP", "OCT", "NOV", "DEC");
var all_monthLitteral = new Array("Janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "D�cembre");






function writeHoraire()	{	
	document.write('<option value="">Aucune pr�f�rence</option>');
	document.write('<option value="MORNING"/>Matin (9:00-12:00)');
	document.write('<option value="AFTERNOON"/>Apr�s-Midi (12:00-18:00)');
	document.write('<option value="EVENING"/>Soir�e (18:00-00:00)');
	for(var i=1;i<10;i++)
		document.write('<option value="0'+i+'00"/>0'+i+':00');
	for(var i=10;i<=23;i++)
		document.write('<option value="'+i+'00"/>'+i+':00');
}	


	
function getNumericOptions(beginIndex, endIndex) {
	var numericOptions = '';
	for(var i=beginIndex; i <= endIndex; i++)
		numericOptions += '<option value="'+i+'">'+i+'</option>\n';
	return numericOptions;
}

// afficher age moins de 1 ans (<1)
function getNumericOptionsHotel(beginIndex, endIndex) {
	var numericOptions = '';
	for(var i=beginIndex; i <= endIndex; i++)
		if(i==0)
			numericOptions += '<option value="0"><1</option>';
		else		
		numericOptions += '<option value="'+i+'">'+i+'</option>\n';
	
	return numericOptions;
}

				


function writeNumericOptions(beginIndex, endIndex){
	document.write(getNumericOptions(beginIndex, endIndex));
}


	


function selectPax(ObjMaster,ObjCustom){
	var minCustom = parseInt(ObjCustom.options[0].value);
	var ObjCustomMaxLength = 9 - parseInt(ObjMaster.value);
	var ObjCustomLength = ObjCustom.options.length;
	var ObjCustomSelect = ObjCustom.value;
	for (j = ObjCustomLength-1; j >0; j--)
		ObjCustom.options[j] = null;
	for (j=0;j<=ObjCustomMaxLength-minCustom;j++)
		ObjCustom.options[j] = new Option(j+minCustom,j+minCustom);
	if (parseInt(ObjCustomSelect) <= ObjCustomMaxLength)
		ObjCustom.value = ObjCustomSelect;
}
	
function updateBebe(ObjMaster,ObjCustom){
	var ObjCustomSelect = ObjCustom.value;
	var ObjCustomLength = ObjCustom.options.length;
	var ObjCustomMaxLength = parseInt(ObjMaster.value);
	for (j = ObjCustomLength-1; j >0; j--)
		ObjCustom.options[j] = null;
	for (j=0;j<=ObjCustomMaxLength;j++)
		ObjCustom.options[j] = new Option(j,j);
	if (parseInt(ObjCustomSelect) <= ObjCustomMaxLength)
		ObjCustom.value = ObjCustomSelect;
	else
		ObjCustom.value = ObjCustomMaxLength;
}

//HomePage DP
function displayChildrenAgeFieldsHPDP(nbChildren,idDivChildrenAgeFields) {
	var childrenAgeFields = '';
	if (nbChildren > 0)
    childrenAgeFields += '<p class="titre">Age des enfants lors du voyage :</p>';
 	for (var i=1; i <= nbChildren; i++){
    	childrenAgeFields += '<p><label for="CHILDREN_AGES_'+(i-1)+'">Enfant '+i+' : </label>';
    	childrenAgeFields += '<select name="CHILDREN_AGES" id="CHILDREN_AGES_'+(i-1)+'">'+getNumericOptions(2,16)+'</select></p>';
    }
    document.getElementById(idDivChildrenAgeFields).innerHTML = childrenAgeFields;
	document.getElementById(idDivChildrenAgeFields).style.display = "block";
	if (nbChildren == 0)
		document.getElementById(idDivChildrenAgeFields).style.display = "none";
}


//HomePage Hotel
function displayChildrenAgeFieldsHPHotel(nbroom,nbChildren,nbChildrenSaisies,idDivChildrenAgeFields) {
	var childrenAgeFields = '';
	var childrenAgeLabelId =new Array;;
	var childrenAgeSelectId = new Array;
	var n =1;  // compteur des enfants
	
	if (nbChildren > 0 /*&& $("#titleAgeEnfants").length == 0*/)
		childrenAgeFields += '<p id ="titleAgeEnfants" class="titre">Age des enfants lors du voyage :</p>';
 	
	var mn=1;
	for (var j=1; j <= nbroom; j++){
		
		for (var i=1; i <= nbChildren; i++){
				
			
			childrenAgeFields += '<p><label id="CHILDREN_AGESLABEL-'+n+'"  for="CHILDREN_AGES-'+n+'">Enfant '+n+' : </label>';
			childrenAgeFields += '<select id="CHILDREN_AGESL-'+n+'" name="childAges'+j+'['+(i-1)+']" >'+getNumericOptionsHotel(0,16)+'</select></p>';

			if(n>nbChildrenSaisies){
					
				childrenAgeLabelId[mn]  = "CHILDREN_AGESLABEL-"+n;
				childrenAgeSelectId[mn] =  "CHILDREN_AGESL-"+n;
				mn++;
				
			}
			
			n++;		
				
		}
		
	}
	
    document.getElementById(idDivChildrenAgeFields).innerHTML = childrenAgeFields;
	
	for (var k =1; k <=	agesChildrenArray.length;k++){
		
		$("#CHILDREN_AGESL-"+k).val(agesChildrenArray[k]); 
		
		if($("#"+childrenAgeSelectId).length !=0){
			$("#"+childrenAgeSelectId).val(agesChildrenArray[k]);
		}
	
	}
	
	$("[id^='CHILDREN_AGESL']").change(function() {
			var age = $(this).val();
			var id_ageList =$(this).attr("id");
			var tabm = id_ageList.split('-'); 
			var m =tabm[1]
			agesChildrenArray[m] = age;
				
			var nbrSaisieChildes = parseInt(nbChildrenSaisies);
		
			if (n > nbChildrenSaisies ){
			var l = 1;
			for (var k = (nbrSaisieChildes+1); k <n; k++){
					
					if($("#"+childrenAgeSelectId[l]).length !=0){
						$("#"+childrenAgeSelectId[l]).val(age);
						agesChildrenArray[k] = age;
					}
					l++;
				}
			}
	});
	for (var k =1; k <=	childrenAgeLabelId.length;k++){
		$("#"+childrenAgeLabelId[k]).hide();
		$("#"+childrenAgeSelectId[k]).hide();
	}

	document.getElementById(idDivChildrenAgeFields).style.display = "block";
	if (nbChildren == 0)
		document.getElementById(idDivChildrenAgeFields).style.display = "none";
}


function displayChildrenAgeFields(nbChildren,idDivChildrenAgeFields) {
	var childrenAgeFields = '';
	if (nbChildren > 0)
    childrenAgeFields += '<p class="titre">Age des enfants lors du voyage :</p>';
 
	
	for (var i=1; i <= nbChildren; i++){
    	childrenAgeFields += '<label for="CHILDREN_AGES_'+(i-1)+'">Enfant '+i+' :</label>';
    	childrenAgeFields += '<select name="CHILDREN_AGES" id="CHILDREN_AGES_'+(i-1)+'">'+getNumericOptions(2,16)+'</select>';
    }
    document.getElementById(idDivChildrenAgeFields).innerHTML = childrenAgeFields;
	document.getElementById(idDivChildrenAgeFields).style.display = "block";
	if (nbChildren == 0)
		document.getElementById(idDivChildrenAgeFields).style.display = "none";
}

function displayNbRoomsFields(idDiv,idp,nbAdults, nbChildren) {
	var maxNbRooms = 5;
	var maxNbPeoplePerRoom = 6;
	var maxNbChildrenPerRoom = 4;
	var nbRoomsFields = '';
	for (var nbRooms=1; nbRooms <= Math.min(nbAdults, maxNbRooms); nbRooms++) {
		var nbAdultsPerRoom = Math.ceil(nbAdults / nbRooms);
		var nbChildrenPerRoom = Math.ceil(nbChildren / nbRooms);
		var nbPeoplePerRoom = nbAdultsPerRoom + nbChildrenPerRoom;
		if (nbPeoplePerRoom <= maxNbPeoplePerRoom && nbChildrenPerRoom <= maxNbChildrenPerRoom) {
			var checkFirstField = (nbRoomsFields == '') ? 'checked' : '';
			nbRoomsFields += '\n <label class="radio"><input type="radio" name="NB_ROOMS" '+checkFirstField+' value="'+nbRooms+'" id="NB_ROOMSDP_'+nbRooms+'" /> '+nbRooms+' chambre(s) pour '+nbPeoplePerRoom+' personne(s)</label>';
		}
	}
	if (nbRoomsFields == '')
		nbRoomsFields = 'Aucune proposition pour les nombres d&#146;adultes et d&#146;enfants s�lectionn�s';
		document.getElementById(idp).innerHTML = nbRoomsFields;
		document.getElementById(idDiv).style.display = 'block';
}

// HomePage DP 
function displayNbRoomsFieldsHPDP(idDiv,idp,nbAdults, nbChildren) {
	var maxNbRooms = 5;
	var maxNbPeoplePerRoom = 6;
	var maxNbChildrenPerRoom = 4;
	var nbRoomsFields = '';
	for (var nbRooms=1; nbRooms <= Math.min(nbAdults, maxNbRooms); nbRooms++) {
		var nbAdultsPerRoom = Math.ceil(nbAdults / nbRooms);
		var nbChildrenPerRoom = Math.ceil(nbChildren / nbRooms);
		var nbPeoplePerRoom = nbAdultsPerRoom + nbChildrenPerRoom;
		if (nbPeoplePerRoom <= maxNbPeoplePerRoom && nbChildrenPerRoom <= maxNbChildrenPerRoom) {
			var checkFirstField = (nbRoomsFields == '') ? 'checked' : '';
			nbRoomsFields += '\n <label class="radio"><input type="radio" name="NB_ROOMS" '+checkFirstField+' value="'+nbRooms+'" id="NB_ROOMS_'+nbRooms+'" /> '+nbRooms+' chambre(s) pour '+nbPeoplePerRoom+' personne(s)</label>';
		}
	}
	if (nbRoomsFields == '')
		nbRoomsFields = '<span class="NoProposition">Aucune proposition pour les nombres d&#146;adultes et d&#146;enfants s�lectionn�s</span>';
		document.getElementById(idp).innerHTML = nbRoomsFields;
		document.getElementById(idDiv).style.display = 'block';
}


//HomePage Hotel
function displayNbRoomsFieldsHPHotel(idDiv,idp,nbAdults, nbChildren) {
	var maxNbRooms = 4;
	var maxNbAdultsPerRoom = 4;
	var maxNbChildrenPerRoom = 4;
	var nbRoomsFields = '';
	
		
	for (var nbRooms=1; nbRooms <= Math.min(nbAdults, maxNbRooms); nbRooms++) {
		var nbAdultsPerRoom = Math.ceil(nbAdults / nbRooms);
		var nbChildrenPerRoom = Math.ceil(nbChildren / nbRooms);
		var nbPeoplePerRoom = nbAdultsPerRoom + nbChildrenPerRoom;
				 
		
		if (nbAdultsPerRoom <= maxNbAdultsPerRoom && nbChildrenPerRoom <= maxNbChildrenPerRoom) {
			//var checkFirstField = (nbRoomsFields == '') ? 'checked' : '';
			if(nbRoomsFields == ''){
				
				var checkFirstField='checked';
				$('#numberOfRooms').val(nbRooms);
				$("#numberOfAdults").val(nbAdultsPerRoom);
				$("#numberOfChildren").val(nbChildrenPerRoom);
			
			}else 
				checkFirstField='';
			
			nbRoomsFields += '\n <label class="radio"><input type="radio" name="NB_ROOMS" '+checkFirstField+' value="'+nbRooms+'" id="NB_ROOMS_'+nbRooms+'" /> '+nbRooms+' chambre(s) pour '+nbPeoplePerRoom+' personne(s)</label>';
			
		}
	}
	
	if (nbRoomsFields == '')
		nbRoomsFields = '<span class="NoProposition">Aucune proposition pour les nombres d&#146;adultes et d&#146;enfants s�lectionn�s</span>';
		
	document.getElementById(idp).innerHTML = nbRoomsFields;
	document.getElementById(idDiv).style.display = 'block';
	// listner sur les checkbox nombre de chambres
		
	$("[id^='NB_ROOMS_']").click(function() {
				
			var nbRooms = $(this).val();
			var nbAdults = $("#NB_ADULTS_Hotel").val();
			var nbChildren = $("#NB_CHILDREN_Hotel").val();
		
			var nbAdultsPerRoom = Math.ceil(nbAdults / nbRooms);
			var nbChildrenPerRoom = Math.ceil(nbChildren / nbRooms);
					
			
			$('#numberOfRooms').val(nbRooms);
			$("#numberOfAdults").val(nbAdultsPerRoom);
			$("#numberOfChildren").val(nbChildrenPerRoom);
			
			displayChildrenAgeFieldsHPHotel(nbRooms,nbChildrenPerRoom,nbChildren,'childrenAgeFields');
				
		});
 }


function displayBaby(objBaby){
	if (objBaby.value > 0)
		document.getElementById('babyNote').style.display = "block";	
	else
		document.getElementById('babyNote').style.display = "none";	
}

function doOnload(form) { 
	with (form) {
		updateWeekDay(D_DAY, D_MONTH, W_D_DAY);
		updateWeekDay(R_DAY, R_MONTH, W_R_DAY);
		selectPax(NB_ADULTS, NB_CHILDREN); 
		updateBebe(NB_ADULTS, NB_INFANTS); 
		displayChildrenAgeFields(NB_CHILDREN.value); 
		displayNbRoomsFields(NB_ADULTS.value, NB_CHILDREN.value);
	}
	if (document.getElementById('volhotel')){
		var element = document.getElementById('volhotel');
		element.className = 'activeTab';
	}	
}
