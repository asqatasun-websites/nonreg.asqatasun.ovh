/**
 * Librairie javascript de manipulation du Dom
 *
 * @package       lemonde
 * @author        Chabe (11/03/08)
 * @author        $Author: chabrou $
 * @version       $Revision: 45540 $
 * @date          $Date: 2010-03-11 14:38:18 +0100 (Thu, 11 Mar 2010) $
 * @id            $Id: dom.js 45540 2010-03-11 13:38:18Z chabrou $
 */
if (undefined===window.MIA)
{
   var MIA = {};
}

if (typeof MIA.Dom == 'undefined')
{
   MIA.Dom =
   {
      class_name : "Dom",

      /**
       * @description méhode de callback qui charge le html recu dans l'élément
       * @method chargerInnerHTML
       * @param string element_id id de l'element dont on souhaite changer le innerHTML
       * @param string url url du html que l'on souhaite insérer dans l'élément
       * @return void
       * @private
       */
      demanderChargementFragmentFromURL : function(element, url, callback, argument)
      {

         if (!callback)
         {
            callback = false;
         }

         if (!argument)
         {
            argument = null;
         }

         var request_callback =
         {
            success:MIA.Dom.chargerInnerHTML,
            failure:function() {return false;},
            argument: {element: element, callback: callback, argument: argument, url : url},
            timeout: 600000
         };
         var request = YAHOO.util.Connect.asyncRequest('GET', url, request_callback);
      },
      
      
      

      chargementSynchrone : function (page,  method, data) 
      {
         if (document.all) // bad IE
            var XhrObj = new ActiveXObject ("Microsoft.XMLHTTP");
         else // Mozilla
            var XhrObj = new XMLHttpRequest ();
         
         if (method != null)
         {
            method = method.toUpperCase ();
         }
         else
         {
            method = "GET";
         }	
         
         if (method == "POST")
         {
            XhrObj.open ("POST", page, false);
         }
         else  
         {
            if (data == null)
               XhrObj.open ("GET", page, false);
            else
               XhrObj.open ("GET", page+"?"+data, false);
         }
         
         if (method == "POST") 
         {
            XhrObj.setRequestHeader ('Content-Type','application/x-www-form-urlencoded');
            XhrObj.send (data);
            if (XhrObj.readyState == 4 && XhrObj.status == 200)
            {
               return( XhrObj.responseText);
            }
         }
         else
         {
            XhrObj.send (null);
            if (XhrObj.readyState == 4 && XhrObj.status == 200)
            {
               return( XhrObj.responseText);
            }
         }
      },
      
      
    
      
      
      /**
       * @description méhode de callback qui charge le html recu dans l'élément
       * @method chargerInnerHTML
       * @param {object} o objet transmis par xmlHttpRequest
       * @return void
       * @private
       */
      chargerInnerHTML : function(o)
      {
         if (o.argument.element)
         {
            o.argument.element.innerHTML  = '<!-- fragment:'+o.argument.url+' -->'+o.responseText+'<!-- /fragment:'+o.argument.url+' -->';

            if (o.argument.callback)
            {
               o.argument.callback(o.argument.argument);
            }
         }
      },

    
      positionnerAncre : function()
      {
         if (MIA.Conf.ancre)
         {
            window.location.hash = '#' + MIA.Conf.ancre;
         }
      },

      toggleDiv : function(div_id)
      {
         if (document.getElementById(div_id))
         {
            if (document.getElementById(div_id).style.display == 'block')
            {
               document.getElementById(div_id).style.display = 'none';
            }
            else
            {
               document.getElementById(div_id).style.display = 'block';
            }	
         }
      },


      getElementsByAttribute : function(attribute, attribute_value, elem_tag, pere_elem)
      {
         return (YAHOO.util.Dom.getElementsBy(function(elem) { return (elem.getAttribute(attribute) == attribute_value); }, elem_tag, pere_elem));
      },
      
      includeDynamicJs : function(url) 
      {
         var head = document.getElementsByTagName('head')[0];
         var script = document.createElement('script');
         script.setAttribute('src',
         url);
         script.setAttribute('type', 'text/javascript');
         head.appendChild(script);
      }
   };
}