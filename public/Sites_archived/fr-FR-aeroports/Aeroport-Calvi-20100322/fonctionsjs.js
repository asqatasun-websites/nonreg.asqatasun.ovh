//Permet de derouler les sous-menus du menu - action OnClick, OnMouseover et OnMouseout d'un objet 
function montre(id) 
{
	var d = document.getElementById(id);
	for (var i = 1; i<=10; i++) 
	{
		if (document.getElementById('smenu'+i)) 
		{document.getElementById('smenu'+i).style.display='none';}
	}
	if (d) 
		{d.style.display='block';}
}

//Permet d'ouvrir les fenetres pop-up -- Action dans un <a href="...>
function ouvreFenetre(page, largeur, hauteur) 
{ 
	var fenetre = window.open(page, "", "scrollbars=no,menubar=no,toolbar=no,resizable=no,width="+ largeur + ",height=" + hauteur);
}

//Permet de supprimer la valeur par defaut d'un input -- action: OnFocus d'un input (text ou area)
function focusInput(Obj,val) { 
	if (Obj.value == val)
		Obj.value = '';
}

//R�ecrire la valeur par defaut de l'input s'il est vide -- action: OnBlur d'un input (text ou area)
function blurInput(Obj,val) { 
	if (Obj.value == '')
		Obj.value = val;
}

//Permet d'inserer des tags de mise en page dans une zone de texte input ou textearea
//nom_du_textarea = document."nomformulaire"."nomTextarea"
function AddText(startTag,defaultText,endTag,nom_du_textarea) 
{
	 if (nom_du_textarea.createTextRange) 
	 {
		  var text;
		  nom_du_textarea.focus(nom_du_textarea.caretPos);
		  nom_du_textarea.caretPos = document.selection.createRange().duplicate();
		  if(nom_du_textarea.caretPos.text.length>0)
		  {
			nom_du_textarea.caretPos.text = startTag + nom_du_textarea.caretPos.text + endTag;
		  }
		  else
		  {
			nom_du_textarea.caretPos.text = startTag+defaultText+endTag;
		  }
	 }
	 else 
		nom_du_textarea.value += startTag+defaultText+endTag;
}
/***********************************************************************************************/

var Diaporamas=new Array();

//constructeur du diaporama
function Diaporama(nom, interval){
	this.Nom=nom;
	this.Index=Diaporamas.length;
	this.Images=new Array();
	this.Liens=new Array();
	this.ImgIndex=0;
	this.Interval=interval;
	
	//Ajouter une image avec son lien
	this.Add=function(src,lien){
		var Index=this.Images.length;
		this.Images[Index]=new Image();
		this.Images[Index].src=src;
		this.Liens[Index]=lien;
	}
	
	//Modifier les images
	this.Show=function(){
		Index=this.ImgIndex;
		if(Index < 0)
			Index=this.Images.length - 1;
		else
		{
			if(Index >= this.Images.length - 1)
				Index=0;
			else
				Index++;
		}
		document.images["image"+this.Nom].src=this.Images[Index].src;
		this.ImgIndex=Index;
	}

	//Declencher le diaporama
	this.Play=function(){
		if(this.Images.length>0)
			setInterval("Diaporamas[" + this.Index + "].Show();", this.Interval);
	}
	
	//Faire le lien dans un diaporama
	this.Link=function(){
		Index=this.ImgIndex;
		if(this.Liens[Index]!="")
			var fenetre = window.open(this.Liens[Index],"","scrollbars=no,menubar=no,toolbar=no,resizable=no,width=340,height=400");
	}

	Diaporamas[this.Index]=this;
}
//d�clenche tous les diaporamas onlad()
function loadDiaporamas(){
	for($i=0;$i<Diaporamas.length;$i++)
	{
		Diaporamas[$i].Play();
	}
}
/*******************************************************************************************/
//Fonction pour creer un rollover, Objet = 'name' de l'image Fichier = chemin de l'image a modifier 
function survol(Objet, Fichier) {
   rep="./images/";
   if (!document.images) {}
   document.images[Objet].src = rep + Fichier;
}

/*Script t�l�charg� sur EasyScript (www.easy-script.com)*/
	// ouvre une fenetre sans barre d'etat, ni de barre de scroll

function popup(page) 
{
	window.open(page,"popup","width=440,height=400,toolbar=no,scrollbars=no");	
}
// FONCTION POUR LE POPUP
function OuvrePopup() {
	// V�rifie que le cookie "pop1fois" n'est pas pr�sent
		// Param�trez ici le mode d'affichage du popup
		window.open("popup_actualites.php","pop1fois","width=340, height=400, toolbars=no, scrollbars=no");
}

