/*********************************************************
 * 
 * index.js - v1.0
 * 
 ********************************************************/

/*********************************************************
 * 
 * Appel des fonctions au chargement
 * 
 ********************************************************/
$(function(){
	// Initialisation du carrousel
	initCarrousel();
	
	$(".news_blocks_footer li:last").css({"background":"none"});
	$(".sidebar_left_2cols .last_comments .news_block:last-child").css({"border": "none"});
});

// Redimensionnement de l'habillage lors du changement de format de fenetre
// $(window).resize(function(){initHabillage();});

/*********************************************************
 * initCarrousel
 * Initialise le carrousel
 ********************************************************/
function initCarrousel(){
	/* PNG fix pour IE6 */
	jQuery(".button img").ifixpng();
	
	var $panels = $('.scrollcontainer .panel');
	var $container = $('.scrollcontainer');
	var panelWidth = 153;
	var panelTotalNumber = $panels.length;
	var lastKeyElement = panelTotalNumber-4;
	var vitesse = 400;
	currentElement = 0;
	
	// calculate de la largeur du container pour inclure l'ensemble des panels
	var containerWidth = panelWidth * panelTotalNumber;
	$container.width(containerWidth);
	
	$(".button.right").click(function(){
		var nextElement = currentElement + 4;
		if(nextElement < panelTotalNumber){
			if(nextElement < lastKeyElement){
				currentElement += 4;
				$(".mask").scrollTo($(".scrollcontainer > div").get(currentElement), vitesse, {axis:"x"});
			} else {
				currentElement = lastKeyElement;
				$(".mask").scrollTo($(".scrollcontainer > div").get(currentElement), vitesse, {axis:"x"});
			}
		} else{
			currentElement = 0;
			$(".mask").scrollTo($(".scrollcontainer > div").get(currentElement), vitesse, {axis:"x"});
		}
	});
	
	$(".button.left").click(function(){
		if(currentElement > 0){
			if(currentElement > 4){
				currentElement -= 4;
				$(".mask").scrollTo($(".scrollcontainer > div").get(currentElement), vitesse, {axis:"x"});
			} else{
				currentElement = 0;
				$(".mask").scrollTo($(".scrollcontainer > div").get(currentElement), vitesse, {axis:"x"});
			}
		} else{
			currentElement = lastKeyElement;
			$(".mask").scrollTo($(".scrollcontainer > div").get(lastKeyElement), vitesse, {axis:"x"});
		}
	});	
}

/**
 * Initialisation de l'affichage des habillages événementiels
 * 
 * @name	initHabillage
 * @author	Rémy Vuong <remy@upian.com>
 * @date	2009-05-28
 * @version	1.0
 * @param	void
 * @return	void
 */
function initHabillage() {
	if ($('body').attr('style')==undefined) { 
		return false;
	}
	
	var habillage = ($('body').attr('style').length!=0);
	if (habillage) {
		// Récupération de la valeur du clickTAG
		var clickTAG = $('#ads_top').find('a').attr('href');
		
		// Animation Flash - Top
		var so = new SWFObject(base_url+"/theme/front/img/habillage/toyota/megabann_1004x272.swf", "ads_top", "1004", "272", "8", "#000");
		so.addParam("allowScriptAccess", "always");
		so.addParam("quality", "high");
		so.addParam("scale", "noscale");
		so.addVariable("clickTAG", clickTAG);
		so.useExpressInstall(base_url+'/files/swf/expressinstall.swf');
		so.write("ads_top");
	}
}
