








/***********************************************************************************
*	(c) Ger Versluis 2000 version 5.411 24 December 2001 (updated Jan 31st, 2003 by Dynamic Drive for Opera7)
*	For info write to menus@burmees.nl		          *
*	You may remove all comments for faster loading	          *		
***********************************************************************************/

	var LowBgColor='#FFFFFF';			// Background color when mouse is not over
	var LowSubBgColor='';			// Background color when mouse is not over on subs
	var HighBgColor='';			// Background color when mouse is over
	var HighSubBgColor='';			// Background color when mouse is over on subs
	var FontLowColor='#7191BC';			// Font color when mouse is not over
	var FontSubLowColor='#2331A7';			// Font color subs when mouse is not over
	var FontHighColor='#7191BC';			// Font color when mouse is over
	var FontSubHighColor='#2331A7';			// Font color subs when mouse is over
	var BorderColor='';			// Border color
	var BorderSubColor='#FFFFFF';			// Border color for subs
	var BorderWidth=1;				// Border width
	var BorderBtwnElmnts=1;			// Border between elements 1 or 0
	var FontFamily="Verdana,Arial"	// Font family menu items
	var FontSize=11;				// Font size menu items
	var FontBold=0;				// Bold menu items 1 or 0
	var FontItalic=0;				// Italic menu items 1 or 0
	var MenuTextCentered='left';			// Item text position 'left', 'center' or 'right'
	var MenuCentered='center';			// Menu horizontal position 'left', 'center' or 'right'
	var MenuVerticalCentered='top';		// Menu vertical position 'top', 'middle','bottom' or static
	var ChildOverlap=.2;				// horizontal overlap child/ parent
	var ChildVerticalOverlap=.2;			// vertical overlap child/ parent
	var StartTop=2;				// Menu offset x coordinate
	var StartLeft=0;				// Menu offset y coordinate
	var VerCorrect=0;				// Multiple frames y correction
	var HorCorrect=0;				// Multiple frames x correction
	var LeftPaddng=2;				// Left padding
	var TopPaddng=2;				// Top padding
	var FirstLineHorizontal=1;			// SET TO 1 FOR HORIZONTAL MENU, 0 FOR VERTICAL
	var MenuFramesVertical=1;			// Frames in cols or rows 1 or 0
	var DissapearDelay=200;			// delay before menu folds in
	var TakeOverBgColor=0;			// Menu frame takes over background color subitem frame
	var FirstLineFrame='navig';			// Frame where first level appears
	var SecLineFrame='space';			// Frame where sub levels appear
	var DocTargetFrame='space';			// Frame where target documents appear
	var TargetLoc='MenuPos';				// span id for relative positioning
	var HideTop=0;				// Hide first level when loading new document 1 or 0
	var MenuWrap=1;				// enables/ disables menu wrap 1 or 0
	var RightToLeft=0;				// enables/ disables right to left unfold 1 or 0
	var UnfoldsOnClick=0;			// Level 1 unfolds onclick/ onmouseover
	var WebMasterCheck=0;			// menu tree checking on or off 1 or 0
	var ShowArrow=0;				// Uses arrow gifs when 1
	var KeepHilite=1;				// Keep selected path highligthed
	var Arrws=['tri.gif',5,10,'tridown.gif',10,5,'trileft.gif',5,10];	// Arrow source, width and height

//MenuColors_default=new Array("#FFFFFF","#FFFFFF","#FFFFFF","#FFFFFF","#FFFFFF","#FFFFFF","#FFFFFF","#FFFFFF");
MenuColors=new Array("","#B2DCBE","#D1CCA1","#B4BFC2","#85C795","#99AB60","#C28C8C","#E9E2C8","#F0A606");

function BeforeStart(){return}
function AfterBuild(){return}
function BeforeFirstOpen(){return}
function AfterCloseAll(){ShowSelect();return}


// Menu tree
//	MenuX=new Array(Text to show, Link, background image (optional), number of sub elements, height, width);
//	For rollover images set "Text to show" to:  "rollover:Image1.jpg:Image2.jpg"

			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu1_1 = new Array("Pr�sentation","dispatch.do?sectionId=site/page_d_accueil_10772717277503/le_var_10772717703904/pr_sentation_10775350717960","",0,19,168.0,MenuColors[1]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu1_2 = new Array("Territoires","dispatch.do?sectionId=site/page_d_accueil_10772717277503/le_var_10772717703904/territoires_10778888129680","",0,19,168.0,MenuColors[1]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu1_3 = new Array("D�placements","dispatch.do?sectionId=site/page_d_accueil_10772717277503/le_var_10772717703904/d_placements_10777847215460","",0,19,168.0,MenuColors[1]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu1_4 = new Array("Les Varois dans le monde","dispatch.do?sectionId=site/page_d_accueil_10772717277503/le_var_10772717703904/les_varois_dans_le_monde_1191224486801284","",0,19,168.0,MenuColors[1]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu1_5 = new Array("Espace documentation","dispatch.do?sectionId=site/page_d_accueil_10772717277503/le_var_10772717703904/espace_documentation_1217426871791106","",0,19,168.0,MenuColors[1]);
			
		//nom menu, lien, image s�paration, ?, hauteur de la case, largeur de la case
		
		Menu1 = new Array("LE VAR","dispatch.do?sectionId=site/page_d_accueil_10772717277503/le_var_10772717703904","",5,19,57.0);
		Menu2 = new Array("|","","",0,19,5);
	
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu3_1 = new Array("L'institution","dispatch.do?sectionId=site/page_d_accueil_10772717277503/conseil_g_n_ral_10772717856095/institution_10794284321160","",0,19,247.0,MenuColors[2]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu3_2 = new Array("Le magazine du Conseil g�n�ral","dispatch.do?sectionId=site/page_d_accueil_10772717277503/conseil_g_n_ral_10772717856095/le_magazine_du_conseil_g_n_ral_121328526550072","",0,19,247.0,MenuColors[2]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu3_3 = new Array("Vid�os","dispatch.do?sectionId=site/page_d_accueil_10772717277503/conseil_g_n_ral_10772717856095/vid_os_1196325434022305","",0,19,247.0,MenuColors[2]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu3_4 = new Array("Plan d'engagement 2009","dispatch.do?sectionId=site/page_d_accueil_10772717277503/conseil_g_n_ral_10772717856095/plan_d_engagement______1213890306096167","",0,19,247.0,MenuColors[2]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu3_5 = new Array("Budget","dispatch.do?sectionId=site/page_d_accueil_10772717277503/conseil_g_n_ral_10772717856095/budget_108005658562312","",0,19,247.0,MenuColors[2]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu3_6 = new Array("Rapport de performance ","dispatch.do?sectionId=site/page_d_accueil_10772717277503/conseil_g_n_ral_10772717856095/rapport_de_performance_1213890355994168","",0,19,247.0,MenuColors[2]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu3_7 = new Array("Offres d'emploi","dispatch.do?sectionId=site/page_d_accueil_10772717277503/conseil_g_n_ral_10772717856095/offres_d_emploi_108254785620317","",0,19,247.0,MenuColors[2]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu3_8 = new Array("March�s publics","dispatch.do?sectionId=site/page_d_accueil_10772717277503/conseil_g_n_ral_10772717856095/march_s_publics_107788997250017","",0,19,247.0,MenuColors[2]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu3_9 = new Array("Appels � projets ","dispatch.do?sectionId=site/page_d_accueil_10772717277503/conseil_g_n_ral_10772717856095/appels___projets__1155300860094230","",0,19,247.0,MenuColors[2]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu3_10 = new Array("Partenariats financiers et subventions","dispatch.do?sectionId=site/page_d_accueil_10772717277503/conseil_g_n_ral_10772717856095/associations_112011341082142","",0,19,247.0,MenuColors[2]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu3_11 = new Array("La presse en parle","dispatch.do?sectionId=site/page_d_accueil_10772717277503/conseil_g_n_ral_10772717856095/la_presse_en_parle_121740707546492","",0,19,247.0,MenuColors[2]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu3_12 = new Array("Actualit�s","dispatch.do?sectionId=site/page_d_accueil_10772717277503/conseil_g_n_ral_10772717856095/actualit_s_10772725857810","",0,19,247.0,MenuColors[2]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu3_13 = new Array("CNIL","dispatch.do?sectionId=site/page_d_accueil_10772717277503/conseil_g_n_ral_10772717856095/cnil_116920448958027","",0,19,247.0,MenuColors[2]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu3_14 = new Array("Acc�s r�serv�","dispatch.do?sectionId=site/page_d_accueil_10772717277503/conseil_g_n_ral_10772717856095/acc_s_r_serv__10867903659060","",0,19,247.0,MenuColors[2]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu3_15 = new Array("D�marches Administratives","dispatch.do?sectionId=site/page_d_accueil_10772717277503/conseil_g_n_ral_10772717856095/service_public_local_1225789185121596","",0,19,247.0,MenuColors[2]);
			
		//nom menu, lien, image s�paration, ?, hauteur de la case, largeur de la case
		
		Menu3 = new Array("CONSEIL G�N�RAL","dispatch.do?sectionId=site/page_d_accueil_10772717277503/conseil_g_n_ral_10772717856095","",15,19,127.5);
		Menu4 = new Array("|","","",0,19,5);
	
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu5_1 = new Array("Economie varoise","dispatch.do?sectionId=site/page_d_accueil_10772717277503/d_veloppement_10772717998436/indicateurs__conomiques_10775351064371","",0,19,188.5,MenuColors[3]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu5_2 = new Array("D�veloppement des territoires","dispatch.do?sectionId=site/page_d_accueil_10772717277503/d_veloppement_10772717998436/d_veloppement_des_territoires_121663228189043","",0,19,188.5,MenuColors[3]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu5_3 = new Array("Tourisme","dispatch.do?sectionId=site/page_d_accueil_10772717277503/d_veloppement_10772717998436/tourisme_108006192451031","",0,19,188.5,MenuColors[3]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu5_4 = new Array("Agriculture","dispatch.do?sectionId=site/page_d_accueil_10772717277503/d_veloppement_10772717998436/agriculture_108006198713032","",0,19,188.5,MenuColors[3]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu5_5 = new Array("Habitat","dispatch.do?sectionId=site/page_d_accueil_10772717277503/d_veloppement_10772717998436/habitat_108012280923844","",0,19,188.5,MenuColors[3]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu5_6 = new Array("Ports","dispatch.do?sectionId=site/page_d_accueil_10772717277503/d_veloppement_10772717998436/ports_1217943558010130","",0,19,188.5,MenuColors[3]);
			
		//nom menu, lien, image s�paration, ?, hauteur de la case, largeur de la case
		
		Menu5 = new Array("D�VELOPPEMENT","dispatch.do?sectionId=site/page_d_accueil_10772717277503/d_veloppement_10772717998436","",6,19,123.5);
		Menu6 = new Array("|","","",0,19,5);
	
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu7_1 = new Array("Espaces naturels sensibles","dispatch.do?sectionId=site/page_d_accueil_10772717277503/environnement_10772718142507/espaces_naturels_sensibles_10794525016072","",0,19,234.0,MenuColors[4]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu7_2 = new Array("Protection de l'environnement","dispatch.do?sectionId=site/page_d_accueil_10772717277503/environnement_10772718142507/protection_de_l_environnement_108006440102143","",0,19,234.0,MenuColors[4]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu7_3 = new Array("Itin�raires","dispatch.do?sectionId=site/page_d_accueil_10772717277503/environnement_10772718142507/itin_raires_108006425780539","",0,19,234.0,MenuColors[4]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu7_4 = new Array("Laboratoire d�partemental d'analyses","dispatch.do?sectionId=site/page_d_accueil_10772717277503/environnement_10772718142507/laboratoire_d_partemental_d_analyse_108006434264742","",0,19,234.0,MenuColors[4]);
			
		//nom menu, lien, image s�paration, ?, hauteur de la case, largeur de la case
		
		Menu7 = new Array("ENVIRONNEMENT","dispatch.do?sectionId=site/page_d_accueil_10772717277503/environnement_10772718142507","",4,19,123.5);
		Menu8 = new Array("|","","",0,19,5);
	
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu9_1 = new Array("Solidarit�s et vie sociale","dispatch.do?sectionId=site/page_d_accueil_10772717277503/social_10772718216408/solidarit_s_et_vie_sociale_1257778946312132","",0,19,169.0,MenuColors[5]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu9_2 = new Array("Personnes �g�es","dispatch.do?sectionId=site/page_d_accueil_10772717277503/social_10772718216408/personnes_ag_es_10794555317248","",0,19,169.0,MenuColors[5]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu9_3 = new Array("Handicap","dispatch.do?sectionId=site/page_d_accueil_10772717277503/social_10772718216408/personnes_handicap_s_10795151328960","",0,19,169.0,MenuColors[5]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu9_4 = new Array("Insertion","dispatch.do?sectionId=site/page_d_accueil_10772717277503/social_10772718216408/insertion_108012565676264","",0,19,169.0,MenuColors[5]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu9_5 = new Array("Sant� - Pr�vention","dispatch.do?sectionId=site/page_d_accueil_10772717277503/social_10772718216408/sant____pr_vention_108012568827865","",0,19,169.0,MenuColors[5]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu9_6 = new Array("Enfance - Famille","dispatch.do?sectionId=site/page_d_accueil_10772717277503/social_10772718216408/enfance___famille_108012571077066","",0,19,169.0,MenuColors[5]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu9_7 = new Array("Logement","dispatch.do?sectionId=site/page_d_accueil_10772717277503/social_10772718216408/logement_108012573671767","",0,19,169.0,MenuColors[5]);
			
		//nom menu, lien, image s�paration, ?, hauteur de la case, largeur de la case
		
		Menu9 = new Array("SOCIAL","dispatch.do?sectionId=site/page_d_accueil_10772717277503/social_10772718216408","",7,19,57.0);
		Menu10 = new Array("|","","",0,19,5);
	
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu11_1 = new Array("Agenda","dispatch.do?sectionId=site/page_d_accueil_10772717277503/culture_10772718348439/agenda_12132627577249","",0,19,188.5,MenuColors[6]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu11_2 = new Array("Tourn�es d�partementales 2010","dispatch.do?sectionId=site/page_d_accueil_10772717277503/culture_10772718348439/tourn_es_d_partementales______1267439613402214","",0,19,188.5,MenuColors[6]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu11_3 = new Array("Actions et soutien","dispatch.do?sectionId=site/page_d_accueil_10772717277503/culture_10772718348439/actions_et_soutien_108012577213868","",0,19,188.5,MenuColors[6]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu11_4 = new Array("F�te du livre - Edition 2009","dispatch.do?sectionId=site/page_d_accueil_10772717277503/culture_10772718348439/f_te_du_livre___edition______12459415018751000","",0,19,188.5,MenuColors[6]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu11_5 = new Array("H�tel des arts","dispatch.do?sectionId=site/page_d_accueil_10772717277503/culture_10772718348439/h_tel_des_arts_108012579767569","",0,19,188.5,MenuColors[6]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu11_6 = new Array("M�diath�que","dispatch.do?sectionId=site/page_d_accueil_10772717277503/culture_10772718348439/m_diath_que_108012581890570","",0,19,188.5,MenuColors[6]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu11_7 = new Array("Interviews","dispatch.do?sectionId=site/page_d_accueil_10772717277503/culture_10772718348439/interview_1224678058654558","",0,19,188.5,MenuColors[6]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu11_8 = new Array("Archives","dispatch.do?sectionId=site/page_d_accueil_10772717277503/culture_10772718348439/archives_10796915383820","",0,19,188.5,MenuColors[6]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu11_9 = new Array("Mus�um","dispatch.do?sectionId=site/page_d_accueil_10772717277503/culture_10772718348439/mus_um_1097668857059130","",0,19,188.5,MenuColors[6]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu11_10 = new Array("Mus�es du Var","dispatch.do?sectionId=site/page_d_accueil_10772717277503/culture_10772718348439/mus_es_du_var_115338388885021","",0,19,188.5,MenuColors[6]);
			
		//nom menu, lien, image s�paration, ?, hauteur de la case, largeur de la case
		
		Menu11 = new Array("CULTURE","dispatch.do?sectionId=site/page_d_accueil_10772717277503/culture_10772718348439","",10,19,66.5);
		Menu12 = new Array("|","","",0,19,5);
	
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu13_1 = new Array("Ev�nements","dispatch.do?sectionId=site/page_d_accueil_10772717277503/sports_107727184200010/ev_nements_108012587572772","",0,19,88.0,MenuColors[7]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu13_2 = new Array("Disciplines","dispatch.do?sectionId=site/page_d_accueil_10772717277503/sports_107727184200010/disciplines_108012585586971","",0,19,88.0,MenuColors[7]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu13_3 = new Array("Equipements","dispatch.do?sectionId=site/page_d_accueil_10772717277503/sports_107727184200010/equipements_1221039409284271","",0,19,88.0,MenuColors[7]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu13_4 = new Array("Aides","dispatch.do?sectionId=site/page_d_accueil_10772717277503/sports_107727184200010/aides_1223386272310485","",0,19,88.0,MenuColors[7]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu13_5 = new Array("Contacts","dispatch.do?sectionId=site/page_d_accueil_10772717277503/sports_107727184200010/contacts_1221040957732282","",0,19,88.0,MenuColors[7]);
			
		//nom menu, lien, image s�paration, ?, hauteur de la case, largeur de la case
		
		Menu13 = new Array("SPORTS","dispatch.do?sectionId=site/page_d_accueil_10772717277503/sports_107727184200010","",5,19,57.0);
		Menu14 = new Array("|","","",0,19,5);
	
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu15_1 = new Array("Coll�ges","dispatch.do?sectionId=site/page_d_accueil_10772717277503/jeunesse_107727185585911/coll_ges_108012592516874","",0,19,161.0,MenuColors[8]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu15_2 = new Array("Op�rations Jeunesse","dispatch.do?sectionId=site/page_d_accueil_10772717277503/jeunesse_107727185585911/op_rations_jeunesse_1223389862580488","",0,19,161.0,MenuColors[8]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu15_3 = new Array("Vacances et loisirs","dispatch.do?sectionId=site/page_d_accueil_10772717277503/jeunesse_107727185585911/vacances_et_loisirs_108012603942278","",0,19,161.0,MenuColors[8]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu15_4 = new Array("Les aides","dispatch.do?sectionId=site/page_d_accueil_10772717277503/jeunesse_107727185585911/bourses_1221141077511299","",0,19,161.0,MenuColors[8]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu15_5 = new Array("Les jeunes et l' Europe","dispatch.do?sectionId=site/page_d_accueil_10772717277503/jeunesse_107727185585911/les_jeunes_et_l__europe_108012598765876","",0,19,161.0,MenuColors[8]);
			
			//nom sous menu, lien, image s�paration, ?, hauteur de la case, largeur de la case, couleur de fond de la case
			
			Menu15_6 = new Array("Enseignement sup�rieur","dispatch.do?sectionId=site/page_d_accueil_10772717277503/jeunesse_107727185585911/enseignement_primaire_et_sup_rieur_108012601642977","",0,19,161.0,MenuColors[8]);
			
		//nom menu, lien, image s�paration, ?, hauteur de la case, largeur de la case
		
		Menu15 = new Array("JEUNESSE","dispatch.do?sectionId=site/page_d_accueil_10772717277503/jeunesse_107727185585911","",6,19,76.0);
		Menu16 = new Array("|","","",0,19,5);
	
var NoOffFirstLineMenus=15;			// Number of first level items
