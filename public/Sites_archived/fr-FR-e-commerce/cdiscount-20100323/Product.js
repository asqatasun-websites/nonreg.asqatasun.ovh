// Sets the SRC attribute of an <img id="<id>" /> element.
function ChangeImage(id , imageUrl) {
    if (document.getElementById(id) != null) {
        document.getElementById(id).src = imageUrl;
        document.getElementsByName('ZoomProductImage')[0].src = imageUrl.toString().replace('/d/', '/f/');
    }
}

function SetUniqueRadioButton(nameregex, current)
{
   re = new RegExp(nameregex);
   for(i = 0; i < document.forms[0].elements.length; i++)
   {
      elm = document.forms[0].elements[i]
      if (elm.type == 'radio')
      {
         if (re.test(elm.name))
         {
            elm.checked = false;
         }
      }
   }
   current.checked = true;}

function CaracMax(id, max) {
    if (document.getElementById(id) != null) {
        var myTextBox = document.getElementById(id);
        if (myTextBox.value.length >= max) {
            alert('Pas plus de ' + max + ' caract\350re(s) !!!'); // '\350' allow to display "�" 
            myTextBox.value = myTextBox.value.substr(0, max - 1);
        }
    }
}
