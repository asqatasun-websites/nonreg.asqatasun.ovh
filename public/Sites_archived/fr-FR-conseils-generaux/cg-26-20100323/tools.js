function checkSearchString(oInput) {
	if (oInput.value == "Cherchez ici") {
		oInput.value = "";
	}
}

// dr_scroller with arrow up and down
var dr_scroller_initialised;
var dr_scroller_speed=50;
var dr_scroller_loop, dr_scroller_timer;

function dr_scroller_verifyCompatibleBrowser(){ 
    this.ver=navigator.appVersion;
    this.dom=document.getElementById?1:0 
    this.ie5=(this.ver.indexOf("MSIE 5")>-1 && this.dom)?1:0; 
    this.ie4=(document.all && !this.dom)?1:0; 
    this.ns5=(this.dom && parseInt(this.ver) >= 5) ?1:0; 
    this.ns4=(document.layers && !this.dom)?1:0; 
    this.dr_scroller_bw = (this.ie5 || this.ie4 || this.ns4 || this.ns5)
    return this;
}
dr_scroller_bw = new dr_scroller_verifyCompatibleBrowser();
 
function dr_scroller_build(obj,nest){ 
    nest=(!nest) ? '':'document.'+nest+'.' 
    this.el=dr_scroller_bw.dom?document.getElementById(obj):dr_scroller_bw.ie4?document.all[obj]:dr_scroller_bw.ns4?eval(nest+'document.'+obj):0; 
    this.css=dr_scroller_bw.dom?document.getElementById(obj).style:dr_scroller_bw.ie4?document.all[obj].style:dr_scroller_bw.ns4?eval(nest+'document.'+obj):0; 
    this.scrollHeight=dr_scroller_bw.ns4?this.css.document.height:this.el.offsetHeight 
    this.clipHeight=dr_scroller_bw.ns4?this.css.clip.height:this.el.offsetHeight 
    this.up=dr_scroller_MoveAreaUp;this.down=dr_scroller_MoveAreaDown; 
    this.MoveArea=dr_scroller_MoveArea; this.x; this.y; 
    this.obj = obj + "Object";
    eval(this.obj + "=this");
    return this;
} 
function dr_scroller_MoveArea(x,y){ 
    this.x=x;
    this.y=y;
    this.css.left=this.x;
    this.css.top=this.y;
} 
function dr_scroller_MoveAreaDown(move) {
	if(this.y>-this.scrollHeight+objContainer.clipHeight) {
		this.MoveArea(0,this.y-move);
    		if(dr_scroller_loop) {
    			setTimeout(this.obj+".down("+move+")",dr_scroller_speed);
    		}
	}
}
function dr_scroller_MoveAreaUp(move) {
	if(this.y<0) {
		this.MoveArea(0,this.y-move);
    		if(dr_scroller_loop) {
    			setTimeout(this.obj+".up("+move+")",dr_scroller_speed);
    		}
	}
}
function dr_scroller_PerformScroll(speed){ 
	if(dr_scroller_initialised){ 
		dr_scroller_loop=true; 
		if(speed>0) {
			objScroller.down(speed) ;
		} else {
			objScroller.up(speed) ;
		}
	} 
} 
function dr_scroller_CeaseScroll(){ 
    dr_scroller_loop=false;
    if(dr_scroller_timer) clearTimeout(dr_scroller_timer);
} 
function dr_scroller_Init(){
    objContainer=new dr_scroller_build('divContainer');
    objScroller=new dr_scroller_build('divContent','divContainer');
    objScroller.MoveArea(0,0);
    objContainer.css.visibility='visible';
    dr_scroller_initialised=true; 
}