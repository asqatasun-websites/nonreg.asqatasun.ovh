function OpenRefWin( url ) 
{
	var options = "toolbar=no,location=no,directories=no,status=no," + "menubar=no,scrollbars=yes,resizable=yes,copyhistory=yes," + "width=560,height=300";
	maFenetre = window.open(url,"",options);
}
function ConfirmDownload( url, str ) 
{ 
	if ( ! confirm( str ) ) return;
	location.href = "download.php?&NoHit=1&TheUrl=" + url;
}
function SetFocusSearch( ) {
	window.frmSearch.BotWrd.focus();
	return;
}
function SelectAll( chk, num ) { 
	if ( document.frmDelete.chkDelOpt1.checked == 1 ) { document.frmDelete.chkDelOpt2.checked = 1; document.frmDelete.chkDelOpt3.checked = 1; } 
}
function LoadMLE () 
{
	if ( document.formDetail.cmbModel.value == "default.txt" ) {
		document.formDetail.mleComment.value = "<H5>INFORMATION</H5>\n\n   (saisissez ici le d�tail de votre publication)\n\n<H5>COMPLEMENTS</H5>\n\n    (saisissez ici vos remarques suppl�mentaires)";
	} else {
		document.formDetail.mleComment.value = "<H5>INFORMATION</H5>\n\n    (saisissez ici le d�tail de votre publication)\n\n<UL>\r\n\t<LI>El�ment de liste 1\r\n\t<LI>El�ment de liste 2\r\n\t<LI>El�ment de liste 3\n</UL>\n\n<H5>COMPLEMENTS</H5>\n\n    (saisissez ici vos remarques suppl�mentaires)";
	}
}
function Visualiser() 
{
	var options = "toolbar=no,location=no,directories=no,status=no,"
		+ "menubar=no,scrollbars=yes,resizable=yes,copyhistory=yes,"
		+ "width=500,height=600";
	var corps = "<CENTER><FORM>"
		+ "<INPUT TYPE=BUTTON VALUE=Fermer onClick='self.close();'>"
		+ "</FORM></CENTER>";
	maFenetre = window.open("","D�tail",options);
	maFenetre.document.write( document.formDetail.mleComment.value + corps);
	maFenetre.document.close();
}
function replacestring( searchstring, old_char, new_char ) 
{
  // D�coupage du fichier en fonction du caract�re
  tmp = searchstring.split( old_char );
  searchstring = '';
  for (var i = 0; i < tmp.length; i++) {
    searchstring = searchstring + tmp[i];
    if (i != tmp.length - 1)
      searchstring = searchstring + new_char;
  }
  return(searchstring);
}
function popUp(menuName,on) 
{
	NS4 = (document.layers) ? 1 : 0;
	IE4 = (document.all) ? 1 : 0;
	ver4 = (NS4 || IE4) ? 1 : 0;

	if (on) {
		if (NS4) {
			document.layers[menuName].visibility = "show";
		} else {
			document.all[menuName].style.visibility = "visible";
		}
	} else {
		if (NS4) {
			document.layers[menuName].visibility = "hide";
		} else {
			document.all[menuName].style.visibility = "hidden";
		}
	}
}

function popPrint(myUrl)
{
	var hd = window.open(myUrl, 'popup_print', 'width=656,height=550,top=50,left=50,menubar=yes,scrollbars=yes');
	hd.focus();
}


function Permut (flag,img) {
   if (document.images) {
      if (document.images[img].permloaded) {
         if (flag==1) document.images[img].src = document.images[img].perm.src
         else document.images[img].src = document.images[img].perm.oldsrc
      }
   }
}
function preloadPermut (img,adresse) {
   if (document.images) {
      img.onload = null;
      img.perm = new Image ();
      img.perm.oldsrc = img.src;
      img.perm.src = adresse;
      img.permloaded = true;
   }
}
function changeColor( dst, obj )
{
	document.all[dst].backcolor = obj.value;
}
function open_mini_word(chemin,repertoire,numcalque)
{
	window.open( "./miniword/mini_word.php?strGetNumCal="+numcalque+"&strGetUrlImg="+chemin+"&strGetRepImg="+repertoire,"InterligoWebEditor",toolbar="menubar=no,scrollbars=yes,resizable=yes,width=630,left=32,top=32");
}
function open_mini_word2(numcalque,zone_form,zone_saisie)
{
	window.open( "./miniword/mini_word.php?strGetNumCal="+numcalque+"&form="+zone_form+"&fenetre="+zone_saisie,"InterligoWebEditor",toolbar="scrollbars=yes,resizable=yes,width=584,height=484,left=32,top=32");
}
function open_mini_word_field(chemin,repertoire,calque,form,field)
{
	window.open( "./miniword/mini_word.php?strGetCal="+calque+"&strGetUrlImg="+chemin+"&strGetRepImg="+repertoire+"&strGetForm="+form+"&strGetField="+field,"InterligoWebEditor",toolbar="menubar=no,scrollbars=yes,resizable=yes,width=630,left=32,top=32");
}
function open_editor(editor,title,width,height)
{
	window.open( editor,title,toolbar="menubar=no,scrollbars=no,toolsbar=no,resizable=yes,width="+width+",height="+height+",left=100,top=100");
}
function open_tagscleaner()
{
	window.open( "tags_remove.php?First=1","TagsCleaner",toolbar="menubar=no,scrollbars=yes,resizable=yes,width=600,left=32,top=32");
}
function open_tablebuilder()
{
	window.open( "table_builder.php","TableBuilder",toolbar="menubar=no,scrollbars=yes,resizable=yes,width=680,left=32,top=32");
}
function launchpeg(page)
{
        peg=window.open( page,"peg",'statusbar=1,toolbar=0,menubar=0,scrollbars=0,resizable=no,width=700,height=550');
}

function CaricaFoto(img){
  foto1= new Image();
  foto1.src=(img);
  Controlla(img);
}

function Controlla(img){
  if((foto1.width!=0)&&(foto1.height!=0)){
    viewFoto(img);
  }
  else{
    funzione="Controlla('"+img+"')";
    intervallo=setTimeout(funzione,20);
  }
}

function viewFoto(img){
  largh=foto1.width+20;
  altez=foto1.height+20;
  stringa="width="+largh+",height="+altez;
  finestra=window.open(img,"",stringa);
}

function checkContactForm(formName){
	
	
	switch (formName) {
		
		case "recherche_individu" :
			if ((document.recherche_individu.nom.value == '')
			&& (document.recherche_individu.prenom.value == '')
			&& (document.recherche_individu.org.value == '')
			&& (document.recherche_individu.fon.value == 0)) {
				alert('Veuillez renseigner au moins un champ.');
				return false;
			}
		break;
		
		case "creer_individu" :
			if (document.creer_individu.nom.value == '') {
				alert('Veuillez renseigner le champ \'nom\'');
				document.creer_individu.nom.focus();
				return false;
			}
		break;
		
		case "recherche_organisme" :
			if ((document.recherche_organisme.nom.value == '')
			&& (document.recherche_organisme.tpo_id.value == 0)
			&& (document.recherche_organisme.stt_id.value == 0)
			&& (document.recherche_organisme.bve_id.value == 0)) {
				alert('Veuillez renseigner au moins un champ.');
				return false;
			}
		break;
		
		case "creer_organisme" :
			if (document.creer_organisme.nom.value == '') {
				alert('Veuillez renseigner le champ \'nom\'');
				document.creer_organisme.nom.focus();
				return false;
			}
			if (document.creer_organisme.tpo_id.value == 0) {
				alert('Veuillez renseigner le champ \'type\'');
				document.creer_organisme.tpo_id.focus();
				return false;
			}
		break;
		
		case "modifier_organisme" :
			if (document.modifier_organisme.nom.value == '') {
				alert('Veuillez renseigner le champ \'nom\'');
				document.modifier_organisme.nom.focus();
				return false;
			}
			if (document.modifier_organisme.tpo_id.value == 0) {
				alert('Veuillez renseigner le champ \'type\'');
				document.modifier_organisme.tpo_id.focus();
				return false;
			}
		break;
		
		case "ajouter_bvc" :
			if (document.ajouter_bvc.bve_id.value == 0) {
				alert('Veuillez renseigner le champ \'bassin-versant\'');
				document.ajouter_bvc.bve_id.focus();
				return false;
			}
			if (document.ajouter_bvc.stt_id.value == 0) {
				alert('Veuillez renseigner le champ \'statut de rattachement\'');
				document.ajouter_bvc.stt_id.focus();
				return false;
			}
		break;
		
		case "creer_contact" :
			if (document.creer_contact.fon_id.value == 0) {
				alert('Veuillez renseigner le champ \'fonction de l\'individu\'');
				document.creer_contact.fon_id.focus();
				return false;
			}
			if (document.creer_contact.bve_id.value == 0) {
				alert('Veuillez renseigner le champ \'bassin-versant\'');
				document.creer_contact.bve_id.focus();
				return false;
			}
			if (document.creer_contact.stt_id.value == 0) {
				alert('Veuillez renseigner le champ \'statut de rattachement\'');
				document.creer_contact.stt_id.focus();
				return false;
			}
		break;
		
		case "modifier_contact" :
			if (document.modifier_contact.fon_id.value == 0) {
				alert('Veuillez renseigner le champ \'fonction de l\'individu\'');
				document.modifier_contact.fon_id.focus();
				return false;
			}
		break;
		
		case "creer_ind_ctc" :
			if (document.creer_ind_ctc.ind_nom.value == '') {
				alert('Veuillez renseigner le champ \'nom de l\'individu\'');
				document.creer_ind_ctc.ind_nom.focus();
				return false;
			}
			if (document.creer_ind_ctc.fon_id.value == 0) {
				alert('Veuillez renseigner le champ \'fonction de l\'individu\'');
				document.creer_ind_ctc.fon_id.focus();
				return false;
			}
			if (document.creer_ind_ctc.bve_id.value == 0) {
				alert('Veuillez renseigner le champ \'bassin-versant\'');
				document.creer_ind_ctc.bve_id.focus();
				return false;
			}
			if (document.creer_ind_ctc.stt_id.value == 0) {
				alert('Veuillez renseigner le champ \'statut de rattachement\'');
				document.creer_ind_ctc.stt_id.focus();
				return false;
			}
		break;
		
		case "creer_org_ctc" :
			if (document.creer_org_ctc.org_nom.value == '') {
				alert('Veuillez renseigner le champ \'nom\'');
				document.creer_org_ctc.org_nom.focus();
				return false;
			}
			if (document.creer_org_ctc.tpo_id.value == 0) {
				alert('Veuillez renseigner le champ \'type\'');
				document.creer_org_ctc.tpo_id.focus();
				return false;
			}
			if (document.creer_org_ctc.bve_id.value == 0) {
				alert('Veuillez renseigner le champ \'bassin de rattachement\'');
				document.creer_org_ctc.bve_id.focus();
				return false;
			}
			if (document.creer_org_ctc.stt_id.value == 0) {
				alert('Veuillez renseigner le champ \'statut de rattachement\'');
				document.creer_org_ctc.stt_id.focus();
				return false;
			}
			if (document.creer_org_ctc.fon_id.value == 0) {
				alert('Veuillez renseigner le champ \'fonction de l\'individu\'');
				document.creer_org_ctc.fon_id.focus();
				return false;
			}
		break;
			
		case "creer_agri" :
			if (document.creer_agri.org_nom.value == "") {
				alert('Veuillez renseigner le champ \'nom de l\'exploitation\'');
				document.creer_agri.org_nom.focus();
				return false;
			}
			if (document.creer_agri.ind_nom.value == "") {
				alert('Veuillez renseigner le champ \'nom de l\'exploitant\'');
				document.creer_agri.ind_nom.focus();
				return false;
			}
			if (document.creer_agri.bve_id.value == 0) {
				alert('Veuillez renseigner le champ \'bassin de rattachement\'');
				document.creer_agri.bve_id.focus();
				return false;
			}
		break;
		
		case "nouveau_groupe" :
			if (document.nouveau_groupe.ltc_libelle.value == "") {
				alert('Veuillez renseigner le champ \'Nom du groupe de contacts\'');
				document.nouveau_groupe.ltc_libelle.focus();
				return false;
			}
			if (document.nouveau_groupe.crl_libelle.value == "") {
				alert('Veuillez renseigner le champ \'Nom du crit�re\'');
				document.nouveau_groupe.crl_libelle.focus();
				return false;
			}
			if ((document.nouveau_groupe.tpo_id.value == 0)
			&& (document.nouveau_groupe.bve_id.value == 0)
			&& (document.nouveau_groupe.stt_id.value == 0)
			&& (document.nouveau_groupe.fon_id.value == 0)
			&& (document.nouveau_groupe.org_id.value == 0)
			&& (document.nouveau_groupe.ind_id.value == 0)) {
				alert('Veuillez renseigner au moins un crit�re');
				return false;
			}
		break;
		
		case "nouveau_critere" :
			if (document.nouveau_critere.crl_libelle.value == "") {
				alert('Veuillez renseigner le champ \'Nom du crit�re\'');
				document.nouveau_critere.crl_libelle.focus();
				return false;
			}
			if ((document.nouveau_critere.tpo_id.value == 0)
			&& (document.nouveau_critere.bve_id.value == 0)
			&& (document.nouveau_critere.stt_id.value == 0)
			&& (document.nouveau_critere.fon_id.value == 0)
			&& (document.nouveau_critere.org_id.value == 0)
			&& (document.nouveau_critere.ind_id.value == 0)) {
				alert('Veuillez renseigner au moins un crit�re');
				return false;
			}
		break;
		
		case "modifier_critere" :
			if ((document.modifier_critere.tpo_id.value == 0)
			&& (document.modifier_critere.bve_id.value == 0)
			&& (document.modifier_critere.stt_id.value == 0)
			&& (document.modifier_critere.fon_id.value == 0)
			&& (document.modifier_critere.org_id.value == 0)
			&& (document.modifier_critere.ind_id.value == 0)) {
				alert('Veuillez renseigner au moins un crit�re');
				return false;
			}
		break;
		
		
	}
}




function checkSuiviForm(formName){
	
	switch (formName) {
		
		case "creation_objectif" :
			if (document.creation_objectif.nombre.value == '') {
				alert('Veuillez renseigner le champs \"Objectif\".');
				return false;
			}
			else{
				if (isNaN(document.creation_objectif.nombre.value)){
					alert('Le champs \"Objectif\" doit �tre num�rique.');
				return false;
					}
			}
			break;
		
		
	}
}


function blockEntree() {
	if (window.event.type == "keypress" & window.event.keyCode == 13) {
				return false; 
			}
}

function MM_openBrWindow(theURL,winName,features) {
  window.open(theURL,winName,features);
  return false;
}

function setParentHTML(element1,element2,value1,value2) {
	window.opener.document.getElementById(element1).innerHTML = value1;
	window.opener.document.getElementById(element2).value = value2;
	this.close();
}

function CheckGroupeMailAdress(adress)
{
	if(adress == '') {
		alert('Aucune adresse email renseign�e pour ce groupe de contacts.');
		return false;
	}
}

function CheckPeriod(annee1,annee2)
{
	if (annee1 > annee2) {
		alert('La premi�re ann�e doit �tre inf�rieur � la seconde ann�e.');
		return false;
	} else {
		return true;
	}
}
 function swap_img(imgname,imgpic){
  document.images[imgname].src=imgpic;
 }




/////////////////////////////////////////////////////////////////////////////////
//Fonction qui v�rifie la syntaxe d'une adresse email
//Renvoie 1 si l'adresse n'est pas correct et renvoie 0 sinon
//
//Param�tres en entr�e:
//	jsobj : cha�ne � �tudier correspondant � l'adresse email
/////////////////////////////////////////////////////////////////////////////////
function js_fIsEMail( jsobj )
{
	serie = jsobj.split('@');

	if ( serie.length == 2 )
	{
		virgule_deb = serie[0].indexOf(".");
		virgule_fin = serie[0].lastIndexOf(".");
		if ( virgule_deb != 0 && virgule_fin != serie[0].length-1 )
		{
			virgule_deb = serie[1].indexOf(".");
			virgule_fin = serie[1].lastIndexOf(".");
			lg = serie[1].length
			serie2 = serie[1].split('.')
			if ( virgule_deb != 0 && virgule_fin != lg-1 && serie2.length > 1 )
			{
				return 1;
			}
		}
	}
	return 0;
}

