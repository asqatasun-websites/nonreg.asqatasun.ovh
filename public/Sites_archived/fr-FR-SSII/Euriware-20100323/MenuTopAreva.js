// Gestion du menu principal - Site Intranet AREVA
// Auteur : Pierre POISSONNIER - 10/06/2005


// syntaxe : case = [ 0:"nom", 1:"image_over", 2:"image_out", 3:"href", 4:"fonction javascript", 5:class_case_over, 6:class_case_out, 7:class_ssmenu_over, 8:class_ssmenu_out, 9:[SOUS-MENU] ]
// syntaxe : menu/ssmenu = [ case, case, ... ]
// affichage : ShowMainMenu( [MENU], nombre_de_cellules_par_colonne, IdDivDestination )



// ******************************************************************************************
// FONCTIONS POUR LES MENUS DEROULANTS
// ******************************************************************************************

var NbCellPerColumn = new Array();

var intNbMainCells = new Array();
var IdTimer = new Array();
var GlobalMainMenu = new Array();

var intSerialMenuId = 0;



function ShowMainMenu(arTabMenu, intNbCellCol, IdTargetDiv) {


	strNewDivContent = ""

	// Affichage du niveau 0
	// *********************

	intSerialMenuId++;
	IdTimer[intSerialMenuId] = null;
	NbCellPerColumn[intSerialMenuId] = intNbCellCol;

	var globalOffset = {x: 0, y: 0 };
	var cell;
	var cellHeight;
	var intNbCol;
	var intNbLin;

	var CurrentCell;
	var intCurrentCellIndex;
	var strCellClassOn;
	var strCellClassOff;

	intNbMainCells[intSerialMenuId] = arTabMenu.length;
	GlobalMainMenu[intSerialMenuId] = arTabMenu;

	strNewDivContent = strNewDivContent+("<TABLE BORDER='0' CELLSPACING='0' CELLPADDING='0'>");
	strNewDivContent = strNewDivContent+("<TR>");

	if (arTabMenu[0][0] != "") {
		if(document.getElementById("BORD_ROUGE_TOP_MENU")){
			document.getElementById("BORD_ROUGE_TOP_MENU").style.visibility = "visible";
			document.getElementById("BORD_ROUGE_TOP_MENU").style.display = "block";
			debug2("type: texte");
//			debug2("visibility: "+document.getElementById("BORD_ROUGE_TOP_MENU").style.visibility);
//			debug2("display: "+document.getElementById("BORD_ROUGE_TOP_MENU").style.display);
		}
	}
	else{
		if(document.getElementById("BORD_ROUGE_TOP_MENU")){
			document.getElementById("BORD_ROUGE_TOP_MENU").style.visibility = "hidden";
			document.getElementById("BORD_ROUGE_TOP_MENU").style.display = "none";
			debug2("type: image");
//			debug2("visibility: "+document.getElementById("BORD_ROUGE_TOP_MENU").style.visibility);
//			debug2("display: "+document.getElementById("BORD_ROUGE_TOP_MENU").style.display);
		}
	}

	var intI = 0;
	while (intI < intNbMainCells[intSerialMenuId]) {

		strNewDivContent = strNewDivContent+("<TD ID='V_" + intSerialMenuId + "_MENUCELL_" + intI + "'");

		if (arTabMenu[intI][6] != "") { strNewDivContent = strNewDivContent+(" CLASS='" + arTabMenu[intI][6] + "'"); }
		if (arTabMenu[intI][3] != "") { strNewDivContent = strNewDivContent+(" onMouseDown=\"self.location.href='" + arTabMenu[intI][3] + "';\""); }
		if ((arTabMenu[intI][4] != "") && (arTabMenu[intI][3] == "")) { strNewDivContent = strNewDivContent+(" onMouseDown=\"" + arTabMenu[intI][4] + "\""); }

		strNewDivContent = strNewDivContent+(" onMouseOver='swapMainMenu(" + intI + "," + intSerialMenuId + ");' onMouseOut='setTimer(" + intSerialMenuId + ");'>");


		// Affichage du contenu de la case
		// *******************************
		if (arTabMenu[intI][0] != "") {
			strNewDivContent = strNewDivContent+(arTabMenu[intI][0]);
		}
		if ((arTabMenu[intI][0] == "") && (arTabMenu[intI][1] != "")) {
			strNewDivContent = strNewDivContent+("<IMG ID='V_" + intSerialMenuId + "_MENUIMG_" + intI + "' SRC='" + arTabMenu[intI][1] + "' BORDER='0'>");
		}

		strNewDivContent = strNewDivContent+("</TD>");

		intI++;
	}

	strNewDivContent = strNewDivContent+("</TR>");
	strNewDivContent = strNewDivContent+("</Table>");




	// Affichage du niveau 1
	// *********************
	
	var intI = 0;
	while (intI < intNbMainCells[intSerialMenuId]) {

		if (arTabMenu[intI][9].length > 0) {


			strNewDivContent = strNewDivContent+("<iframe src='javascript:false;' id='IFRM_" + intSerialMenuId + "_SUBMENU_" + intI + "' style='border:none; " +
				"position:absolute; visibility:hidden; z-index:89;' frameborder='0' scrolling='no'></iframe>");


			strNewDivContent = strNewDivContent+("<DIV ID='V_" + intSerialMenuId + "_SUBMENU_" + intI + "' STYLE='top:5px; left:5px; position : absolute; z-index : 90; visibility:hidden;'>");
			strNewDivContent = strNewDivContent+("<table ID='MENUTBL_" + intSerialMenuId + "_SUBMENU_" + intI + "' style='width:110px;' BORDER='0' CELLSPACING='0' onMouseOver='swapMainMenu(" + intI + ", " + intSerialMenuId + ");' onMouseOut='setTimer(" + intSerialMenuId + ");'>");


			// calcul du nombre de colonnes et de lignes du sous-menu
			// ******************************************************
			intNbCol = Math.floor( (arTabMenu[intI][9].length-1) / NbCellPerColumn[intSerialMenuId] ) + 1;
			if (intNbCol > 1) {
				intNbLin = NbCellPerColumn[intSerialMenuId];
			} else {
				intNbLin = arTabMenu[intI][9].length;
			}

			// Cr�ation du sous-menu
			// *********************
			intJ = 0;
			while (intJ < intNbLin) {
				intJ++;
				strNewDivContent = strNewDivContent+("<TR>");

				intK = 0;
				while (intK < intNbCol) {
					intK++;
					intCurrentCellIndex = ((intK-1)*intNbLin + intJ)-1;

					// Affichage de la cellule
					// ***********************
					CurrentCell = arTabMenu[intI][9][intCurrentCellIndex];

					if (CurrentCell) {
						if (CurrentCell[5] != "") {
							strCellClassOn = CurrentCell[5];
						} else {
							strCellClassOn = arTabMenu[intI][7];
						}
						if (CurrentCell[6] != "") {
							strCellClassOff = CurrentCell[6];
						} else {
							strCellClassOff = arTabMenu[intI][8];
						}

						strNewDivContent = strNewDivContent+("<TD CLASS='" + strCellClassOff + "'");
						strNewDivContent = strNewDivContent+(" onMouseOver='this.className=\"" + strCellClassOn + "\";'");
						strNewDivContent = strNewDivContent+(" onMouseOut='this.className=\"" + strCellClassOff + "\";'");
						if (CurrentCell[3] != "") { strNewDivContent = strNewDivContent+(" onMouseDown=\"self.location.href='" + CurrentCell[3] + "';\""); }
						if ((CurrentCell[4] != "") && (CurrentCell[3] == "")) { strNewDivContent = strNewDivContent+(" onMouseDown=\"" + CurrentCell[4] + "\""); }

						strNewDivContent = strNewDivContent+(">");

						if (CurrentCell[0] != "") {
							strNewDivContent = strNewDivContent+(CurrentCell[0]);
						}



						strNewDivContent = strNewDivContent+("</TD>");
					} else {
						strNewDivContent = strNewDivContent+("<TD CLASS='" + arTabMenu[intI][8] + "'>&nbsp;</TD>");
					}

				}

				strNewDivContent = strNewDivContent+("</TR>");
			}

			strNewDivContent = strNewDivContent+("</table>");
			strNewDivContent = strNewDivContent+("</DIV>");
		}
		intI++;
	}


	// Affectation au Div, ou affichage direct
	// ***************************************
	if (IdTargetDiv == '') {
		document.write(strNewDivContent);
	}
	else {
		MyDivMenu = document.getElementById(IdTargetDiv);

		if (MyDivMenu) {
			MyDivMenu.innerHTML = (strNewDivContent);
		}
		else {
			document.write(strNewDivContent);
		}
	}

}



function hideAllDiv(serialMenuId) {
	var intI = 0;
	while (intI < intNbMainCells[serialMenuId]) {

		if (GlobalMainMenu[serialMenuId][intI][6] != "") { document.getElementById("V_" + serialMenuId + "_MENUCELL_" + intI).className = GlobalMainMenu[serialMenuId][intI][6]; }

		if ((GlobalMainMenu[serialMenuId][intI][0] == "") && (GlobalMainMenu[serialMenuId][intI][1] != "") && (GlobalMainMenu[serialMenuId][intI][2] != "")) {
			document.getElementById("V_" + serialMenuId + "_MENUIMG_" + intI).src = GlobalMainMenu[serialMenuId][intI][1];
		}

		if (document.getElementById("V_" + serialMenuId + "_SUBMENU_" + intI)) {
			document.getElementById("V_" + serialMenuId + "_SUBMENU_" + intI).style.visibility = "hidden";
			document.getElementById("IFRM_" + serialMenuId + "_SUBMENU_" + intI).style.visibility = "hidden";
		}
		intI ++;
	}

	stopTimer(serialMenuId);
}




function swapMainMenu(IdMenu, serialMenuId) {
	hideAllDiv(serialMenuId);
	if (document.getElementById("V_" + serialMenuId + "_SUBMENU_" + IdMenu)) {
		document.getElementById("V_" + serialMenuId + "_SUBMENU_" + IdMenu).style.visibility = "visible";
		document.getElementById("IFRM_" + serialMenuId + "_SUBMENU_" + IdMenu).style.visibility = "visible";

		cell = document.all ? document.all["V_" + serialMenuId + "_MENUCELL_" + IdMenu] : document.getElementById("V_" + serialMenuId + "_MENUCELL_" + IdMenu);
		globalOffset = {x: 0, y: 0 };
		cellHeight = cell.offsetHeight;

		while (cell) {
			globalOffset.x += cell.offsetLeft;
			globalOffset.y += cell.offsetTop;
			cell = cell.offsetParent;
		}

		document.getElementById("V_" + serialMenuId + "_SUBMENU_" + IdMenu).style.top = (globalOffset.y + cellHeight);
		document.getElementById("V_" + serialMenuId + "_SUBMENU_" + IdMenu).style.left = (globalOffset.x);


		document.getElementById("IFRM_" + serialMenuId + "_SUBMENU_" + IdMenu).style.top = (globalOffset.y + cellHeight);
		document.getElementById("IFRM_" + serialMenuId + "_SUBMENU_" + IdMenu).style.left = (globalOffset.x);

		document.getElementById("IFRM_" + serialMenuId + "_SUBMENU_" + IdMenu).style.width = document.getElementById("MENUTBL_" + serialMenuId + "_SUBMENU_" + IdMenu).offsetWidth;
		document.getElementById("IFRM_" + serialMenuId + "_SUBMENU_" + IdMenu).style.height = document.getElementById("MENUTBL_" + serialMenuId + "_SUBMENU_" + IdMenu).offsetHeight;

	}

	if ((GlobalMainMenu[serialMenuId][IdMenu][0] == "") && (GlobalMainMenu[serialMenuId][IdMenu][1] != "") && (GlobalMainMenu[serialMenuId][IdMenu][2] != "")) {
		document.getElementById("V_" + serialMenuId + "_MENUIMG_" + IdMenu).src = GlobalMainMenu[serialMenuId][IdMenu][2];
	}

	if ( GlobalMainMenu[serialMenuId][IdMenu][5] != "") {
		document.getElementById("V_" + serialMenuId + "_MENUCELL_" + IdMenu).className = GlobalMainMenu[serialMenuId][IdMenu][5];	
	}
}



function setTimer(serialMenuId) {
	if (IdTimer[serialMenuId] == null) {
		IdTimer[serialMenuId] = setTimeout( "hideAllDiv(" + serialMenuId + ")" , 250);
	}
}

function stopTimer(serialMenuId) {
	if (IdTimer[serialMenuId] != null) {
		clearTimeout(IdTimer[serialMenuId]);
		IdTimer[serialMenuId] = null;
	}
}
/******************************************************/

function debug2(txt){
	if(top.DEBUG){
		top.DEBUG.document.write('<A HREF="/home/vide.htm">R.A.Z</A>&nbsp;&nbsp;');
		top.DEBUG.document.write(txt+'<BR>\n');
		window.defaultStatus='Mode Debug is On';
	}
}
/******************************************************/