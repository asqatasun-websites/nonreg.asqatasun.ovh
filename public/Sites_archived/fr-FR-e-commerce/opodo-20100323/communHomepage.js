    
// functions update dates and hours
function updateReturnDate(DATE_DCARText,DATE_RCARText,idForm){

	//we will update the arrival date if the departure date is bigger.
	// format date.
	var departureDate = $("#"+DATE_DCARText).val();
	var returnDate = $("#"+DATE_RCARText).val();
	
	var departureDateTab = departureDate.split('/'); 
	var returnDateTab =  returnDate.split('/');
	
	var departureYearMonth = departureDateTab[2]+""+departureDateTab[1];
	var departureDay =departureDateTab[0];
	var numericDepartureDate = departureYearMonth*100 + departureDay;
  
	
	var returnYearMonth = 	returnDateTab[2]+""+	returnDateTab[1];
	var returnDay = 	returnDateTab[0];
	var numericReturnDate = returnYearMonth*100 + returnDay;
		
		
			
	if(numericReturnDate > numericDepartureDate)
		return;
   
		$("#"+DATE_RCARText).val(departureDate);
							
	}

	function updateDepartureDate(DATE_RCARText,idForm){

		;					
	}	
	  
	
  //fonction test si la date est valide
function isDateValid(chaineDate) {
  
   // si format est dd/mm/yyyy

   var ladate = (chaineDate).split("/")

  // Si je n'ai pas r�cup�r� trois �l�ments ou bien s'il ne s'agit pas d'entiers, pas la peine non plus d'aller plus loin
   if ((ladate.length != 3) || isNaN(parseInt(ladate[0])) || isNaN(parseInt(ladate[1])) || isNaN(parseInt(ladate[2]))) return false

  // Sinon, c'est maintenant que je cr�e la date correspondante. Attention, les mois sont �talonn�s de 0 � 11
   var unedate = new Date(eval(ladate[2]),eval(ladate[1])-1,eval(ladate[0]))

  // Bug de l'an 2000 oblige, lorsque je r�cup�re l'ann�e, je n'ai pas toujours 4 chiffres selon les navigateurs, je rectifie donc ici le tir.
   var annee = unedate.getYear()
   if ((Math.abs(annee)+"").length < 4) annee = annee + 1900

   //Il ne reste plus qu'� v�rifier si le jour, le mois et l'ann�e obtenus sont les m�mes que ceux saisis par l'utilisateur.
   //return true
   return ((unedate.getDate() == eval(ladate[0])) && (unedate.getMonth() == eval(ladate[1])-1) && (annee == eval(ladate[2])))
}
 
 
function validRegexDate(chaineDate){
	var date_regexp=/^(\d{1,2}\/){2}\d{4}$/
	
	if (!chaineDate.match(date_regexp))
	  return false;
	else
	  return true;
	  
	  //'veuillez entrer une date valide (format jj/mm/aaaa). 
  }

  
	  
//validate date and hours 
function validateDates(divError,idForm, dateDepartTab, dateReturTab, NameDepartureHour, NameReturnHour){
				
	var departureDate = new Date(dateDepartTab[2],dateDepartTab[1]-1,dateDepartTab[0]);
	var returnDate = new Date(dateReturTab[2],dateReturTab[1]-1,dateReturTab[0]);
	

	
	
	var today = new Date();
	today.setHours(0);
	today.setMinutes(0,0,0);
	
		
	if (departureDate < today ){			
		
		html="<p><img class='alert' src='/img_opodo/homepageV2/alert3.gif' align='ATTENTION' />La date de d�part s�lectionn�e est pass�e.<img id='closeError' class ='closeError' border='0' alt='Fermer' src='/img_opodo/headfoot/icon-close.gif' /></p>";
		$("#"+divError).html(html);
		$("#"+divError).show();
		
		$('.closeError').click(function() {
			$("#"+divError).css('display', 'none');
		});
		return false;
			
		
		//html=html+"<span class='errorMessage'>-La date de d�part s�lectionn�e est pass�e.</span><br/>";
	}						
		
	if ( (idForm !='formVol') || ((idForm =='formVol')&& !$("#allerSimple").is(":checked")) ){
		if ( returnDate < today ){					
			
			html="<p><img class='alert' src='/img_opodo/homepageV2/alert3.gif' align='ATTENTION' />La date de retour s�lectionn�e est pass�e.<img id='closeError' class ='closeError' border='0' alt='Fermer' src='/img_opodo/headfoot/icon-close.gif' /></p>";
			$("#"+divError).html(html);
			$("#"+divError).show();
			
			$('.closeError').click(function() {
				$("#"+divError).css('display', 'none');;
			});
			return false;
							
		}
	
	}
	//Votre date de d�part est trop proche. Les r�servations de Vol + H�tel sont possibles au minimum 3 jours avant le d�part.
	
	
	if((idForm =='formVolHotel') || (idForm =='formVol' && volHotel_ValidateDate)){
		today.setDate(today.getDate() + 3);
		today.setHours(0);
		today.setMinutes(0,0,0);
		if (departureDate < today )	{				
						
			html="<p><img class='alert' src='/img_opodo/homepageV2/alert3.gif' align='ATTENTION' />Votre date de d�part est trop proche. Les r�servations de Vol+H�tel sont possibles au minimum 3 jours avant le d�part.<img id='closeError' class ='closeError' border='0' alt='Fermer' src='/img_opodo/headfoot/icon-close.gif' /></p>";
			$("#"+divError).html(html);
			$("#"+divError).show();
		
			$('.closeError').click(function() {
			$("#"+divError).css('display', 'none');;
			});
			return false;			
			
		}
	}
	//From here we take into account the hours.
	var departureHourOK= false; //on ne pr�cise pas l'heure de d�part	
	var returnHourOK= false;	//on ne pr�cise pas l'heure de de retour
	
	if(NameDepartureHour !=''){
		var departureHour = $("#"+idForm+" select[@name="+NameDepartureHour+"]").val();			
	
		if(departureHour !='' && departureHour !='MORNING' && departureHour !='AFTERNOON' && departureHour !='EVENING'){
			departureHour = departureHour.substring(0,2);
			departureDate.setHours(departureHour);
			departureHourOK=true;
			
		}
	}
	
	if(NameReturnHour !=''){
		var returnHour = $("#"+idForm+" select[@name="+NameReturnHour+"]").val();
		
		if(returnHour !='' && returnHour !='MORNING' && returnHour !='AFTERNOON' && returnHour !='EVENING'){
			
			returnHour = returnHour.substring(0,2);
			returnDate.setHours(returnHour);
			returnHourOK =true;
			
		}
	
	}
	
	if ((returnHourOK && departureHourOK) &&((idForm !='formVol') || ((idForm =='formVol')&& !$("#allerSimple").is(":checked"))) ){
		
		if ( returnDate <= departureDate ){					
						
				html="<p><img class='alert' src='/img_opodo/homepageV2/alert3.gif' align='ATTENTION' />La date/heure de retour est identique ou ant�rieure �<br/>la date/heure de d�part.<img id='closeError' class ='closeError' border='0' alt='Fermer' src='/img_opodo/headfoot/icon-close.gif' /></p>";
				$("#"+divError).html(html);
				$("#"+divError).show();
			
				$('.closeError').click(function() {
				$("#"+divError).css('display', 'none');;
				});
				return false;
			}
			return true
			
		}	

}

	 
	
		
	 
	
	
	
	
	
	
	
	
	
	
	
	
	
	