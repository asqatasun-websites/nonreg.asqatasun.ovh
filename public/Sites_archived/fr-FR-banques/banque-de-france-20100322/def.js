 function fill(){
 def(200, 'i1', 0, 31, '/fr/img/menu/institution-off.gif', '1');
  add("Communiqu�s et discours des autorit�s de la banque","/fr/instit/discours/discours.htm");
  add("Histoire, cadre institutionnel et organisation","/fr/instit/histoire/histoire.htm");
  add("En savoir plus sur les activit�s de la Banque","/fr/instit/actu/actu.htm");
  add("Services rendus","/fr/instit/services/services.htm");
  add("Billets et pi�ces","/fr/instit/billets/billets.htm");
  add("Recrutement","http://www.recrutement-banquedefrance.fr/");
  add("Protection du consommateur","/fr/instit/protection_consommateur/protection_consommateur.htm");
  add("Fonds communs de placement de la Banque de France","/fr/instit/fcp/page1.htm");
  add("Comit�s consultatifs","/fr/instit/comites/comites.htm");
  add("Institut bancaire et financier international (IBFI)","/fr/instit/ibfi/ibfi.htm");
  add("Fondation Banque de France pour la recherche","/fondation/fr/index.htm");
  add("Informations pratiques","/fr/instit/info/info.htm");
  add("D�l�gations de pouvoirs et de signature","/fr/instit/delegations/delegations.htm");
  add("Informatique et libert�s","/fr/instit/infoetlib/infoetlib.htm");
  add("Achats et march�s","/fr/instit/achats/achats.htm");
 clo();

 def(200, 'i2', 0, 31, '/fr/img/menu/politique-off.gif', '2');
  add("Politique mon�taire de l'Eurosyst�me","/fr/poli_mone/euro/euro.htm");
  add("R�glementation et mise en oeuvre de la politique mon�taire","/fr/poli_mone/regle_poli/regle_poli.htm");
  add("Statistiques sur les op�rations de politique mon�taire","/fr/poli_mone/mopm/sopm.htm");
  add("Les taux","/fr/statistiques/taux/taux.htm");
  add("Valeurs du Tr&eacute;sor","/fr/poli_mone/adjudic.htm");
  add("Place bancaire et financi�re","/fr/poli_mone/place/place.htm");
 clo();

 def(200, 'i3', 0, 31, '/fr/img/menu/systeme-off.gif', '3');
  add("Moyens et syst�mes de paiement et de titres fran�ais","/fr/sys_mone_fin/caract/caract.htm");
  add("TARGET2","/fr/sys_mone_fin/target2/target2.htm");
   add("TARGET2-Securities","/fr/sys_mone_fin/target2s/target2s_t2s.htm");
  add("SEPA - Espace unique de paiements en euro","/fr/sys_mone_fin/sepa/sepa.htm");
  add("SEPA - site du Comit� national","http://www.sepafrance.fr/",' target="_blank" ');  
  add("Missions de surveillance","/fr/sys_mone_fin/missions/missions.htm");
  add("Consultations publiques","/fr/sys_mone_fin/consultations/consultations.htm");
  add("Documents de r�f�rence et publications","/fr/sys_mone_fin/rapports/rapports.htm");
  add("Statistiques","/fr/sys_mone_fin/stats/stats.htm");
  add("Syst�mes notifi�s","/fr/sys_mone_fin/titres/titres.htm");
  add("Observatoire de la s�curit� des cartes de paiement","http://www.observatoire-cartes.fr/",' target="_blank" class="style2"');
  add("FICAP","/fr/sys_mone_fin/ficap/ficap.htm");
 clo();

 def(200, 'i4', 0, 31, '/fr/img/menu/acp-off.gif', '4');
  //add("R�glementation bancaire et financi�re","/fr/supervi/regle_bafi/regle_bafi.htm");
  //add("Agr�ment","/fr/supervi/agrement/agrement.htm");
  //add("Supervision bancaire","/fr/supervi/supervi_banc/supervi_banc.htm");
  //add("Communiqu�s et mises en garde","/fr/supervi/agrement/compres/compres.htm");  
  //add("Publications","/fr/supervi/supervi_banc/publi/publi.htm");  
  add("Groupe des Superviseurs Bancaires Francophones","/fr/supervi/gsbf/index.htm" );  
 clo();

 def(200, 'i5', 0, 31, '/fr/img/menu/publication-off.gif', '5');
  add("Alerte de publications","/fr/publications/abonnement.htm");  
  add("Archipel","/archipel/index.html" );
  add("Rapport annuel","/fr/publications/rapport/rapport.htm");  
  add("Revue de la stabilit� financi�re","/fr/publications/rsf/rsf_b.htm");
  add("Le Bulletin de la Banque de France","/fr/publications/bulletin/bulletin.htm");
  add("Registre de publication officiel de la Banque de France","/fr/publications/bo/bobdf.htm");
  add("Le Bulletin officiel du CECEI et de la Commission bancaire","/fr/publications/bo/boceccb.htm");
  add("Le rapport annuel de la Commission bancaire","/fr/publications/catalogue/et_4a.htm");
  add("Le catalogue des publications","/fr/publications/catalogue/catalogue.htm");
  add("La lettre de la Recherche","/fr/publications/lettres_recherch/lettres_recherch.htm");
  add("S�minaires et colloques","/fr/publications/seminaires/seminaires.htm");
  add("Documents de travail","/fr/publications/documents_de_travail/documents_de_travail_10.htm");
  add("Focus","/fr/publications/focus/focus.htm");
  add("Questions actuelles","/fr/publications/questions_actuelles/questions_actuelles.htm");
  add("D�bats �conomiques","/fr/publications/debats/debats.htm");  
  add("Documents et d�bats","/fr/publications/doc_debat/doc_debat.htm");    
  add("�tudes de la Direction des Entreprises","/fr/publications/observatoire/observatoire.htm");
  add("�conomistes et chercheurs","/fr/publications/chercheurs/chercheurs.htm");
  add("Autres documents en t�l�chargement","/fr/publications/autres_telechar/autres_telechar.htm");
 clo();
 


  def(200, 'i6', 0, 31, '/fr/img/menu/stat-off.gif', '6');
  add("Calendrier","/fr/statistiques/calendrier/calendrier.htm");  
  add("Taux","/fr/statistiques/taux/taux.htm");   
  add("Monnaie","/fr/statistiques/monnaie/monnaie.htm");   
  add("Titres, Cr�dit et D�p�ts","/fr/statistiques/titres/titres.htm"); 
  add("Activit� bancaire et financi�re","/fr/statistiques/activite/activite.htm"); 
  add("Economie et Conjoncture","/fr/statistiques/economie/economie.htm"); 
  add("Base de donn�es","/fr/statistiques/base/base.htm"); 
  add("Espace D�clarants","/fr/statistiques/declarants/declarants.htm");       
 clo();


/*
 def(200, 'i6', 0, 31, '/fr/img/menu/stat-off.gif', '6');
  add("Alerte de publications","/fr/stat_conjoncture/abonnement.htm");  
  add("ECEIS","/fr/stat_conjoncture/eceis/eceis.html");
  add("Bonnes pratiques","/fr/stat_conjoncture/deontologie/deontologie.htm");
  add("Dates de diffusion","/fr/stat_conjoncture/calendriers/calendriers_1209.htm");
  add("Chiffres cl�s de la Zone euro","/fr/stat_conjoncture/zoneeuro/zoneeuro.htm");
  add("SDDS (Special Data Dissemination Standard)","/fr/stat_conjoncture/sdds/sdds.htm");
  add("Les taux","/fr/stat_conjoncture/taux/taux.htm");
  add("Statistiques mon�taires et bancaires","/fr/stat_conjoncture/stat_mone/stat_mone.htm");
  add("Balance des paiements et activit� financi�re internationale","/fr/stat_conjoncture/balance/balance.htm");
  add("Monnaie fiduciaire, moyens et syst�mes de paiement","/fr/sys_mone_fin/stats/stats.htm");
  add("Comptes financiers et endettement","/fr/stat_conjoncture/comptefi/comptefi.htm");
  add("Enqu�tes de conjoncture","/fr/stat_conjoncture/conjonc/conjonc.htm");
  add("Centralisations financi�res territoriales","/fr/stat_conjoncture/central_fin/central_fin.htm");
  add("Statistiques d'entreprises","/fr/stat_conjoncture/statent/statent.htm");
  add("R�glementation","/fr/stat_conjoncture/regle/regle.htm");
  add("S�ries chronologiques","/fr/stat_conjoncture/series/series.htm");
  add("Statistiques de l'eurosyst�me","/fr/stat_conjoncture/Dynamin/dynamin.htm");
  add("Liens avec les rubriques statistiques des sites des banques centrales du SEBC","/fr/stat_conjoncture/liens/liens.htm");
 clo();
*/

 def(137, 'i7', 0, 31, '/fr/img/menu/eurosysteme-off.gif', '7');
  add("Europe","/fr/eurosys/europe/europe.htm");
  add("Questions internationales","/fr/eurosys/questions/questions.htm");
  add("Comit� europ�en des centrales de bilans","/fr/eurosys/comite-europeen-centrales-bilans/comite-europeen-centrales-bilans.htm");
  add("Zone Franc","/fr/eurosys/zonefr/zonefr.htm");
  add("Institut bancaire et financier international","/fr/instit/ibfi/ibfi.htm");
  add("Transferts des migrants","/fr/eurosys/transferts/transferts.htm");
 clo();
}