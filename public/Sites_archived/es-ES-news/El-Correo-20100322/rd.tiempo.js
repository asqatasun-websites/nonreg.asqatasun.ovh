var A_Corunna = new Array('Betanzos', 'Ferrol', 'La Coru�a', 'Noia', 'Padr�n', 'Santiago');
var Alava = new Array('Amurrio', 'Labastida', 'Laudio', 'Oi�n', 'Salvatierra', 'Vitoria - Gasteiz');
var Albacete = new Array('Albacete', 'Almansa', 'Hell�n');
var Alicante = new Array('Alcoi', 'Alicante', 'Altea', 'Benidorm', 'Calpe', 'Denia', 'Elda', 'Elx', 'J�vea', 'La Vila Joiosa', 'Orihuela', 'Santa Pola', 'Torrevieja');
var Almeria = new Array('Adra', 'Almer�a', 'Cabo Gata', 'Moj�car', 'Roquetas', 'Vera');
var Asturias = new Array('Avil�s', 'Cangas de Narcea', 'Gij�n', 'Llanes', 'Luarca', 'Oviedo', 'Picos de Europa', 'Ribadesella');
var Avila = new Array('�vila');
var Badajoz = new Array('Almendralejo', 'Badajoz', 'Don Benito', 'Jerez de los C.', 'Merida', 'Villanueva', 'Zafra');
var Baleares = new Array('Alcudia', 'Andratx', 'Art�', 'Calvi�', 'Ciutadella', 'Dei�', 'Felanitx', 'Ibiza', 'Inca', 'Llucmajor', 'Mallorca', 'Manacor', 'Ma�', 'Pollen�a', 'San Antonio', 'Santa Eulalia', 'Soller');
var Barcelona = new Array('Barcelona', 'Calella', 'Granollers', 'Igualada', 'Matar�', 'Sabadell', 'Sitges', 'Terrassa', 'Vic', 'Vilafranca', 'Vilanova');
var Bizkaia = new Array('Balmaseda', 'Bermeo', 'Bilbao', 'Durango', 'Ermua', 'Gernika-Lumo', 'G�e�es', 'Lekeitio', 'Mung�a', 'Ondarroa', 'Ordu�a', 'Ortuella', 'Uribe-Kosta', 'Zornotza');
var Burgos = new Array('Aranda', 'Burgos', 'Miranda');
var Caceres = new Array('C�ceres', 'Plasencia', 'Trujillo');
var Cadiz = new Array('Algeciras', 'C�diz', 'Chiclana', 'Grazalema', 'Jerez', 'La Linea', 'San Fernando', 'Sanl�car', 'Tarifa');
var Cantabria = new Array('Arredondo', 'Castro', 'Comillas', 'Laredo', 'Noja', 'Potes', 'Puente Viesgo', 'Reinosa', 'San Vicente de la Barquera', 'Santander', 'Santillana', 'Suances', 'Torrelavega');
var Castellon = new Array('Benicassim', 'Castell�n', 'Oropesa', 'Pe�iscola', 'Vinaros');
var Ceuta = new Array('Ceuta');
var Ciudad_Real = new Array('Alcaza S.J.', 'Almagro', 'Ciudad Real', 'Valdepe�as');
var Cordoba = new Array('C�rdoba', 'Lucena', 'Montilla', 'Palma del R�o');
var Cuenca = new Array('Cuenca', 'Taranc�n');
var Gipuzkoa = new Array('Andoain', 'Arrasate', 'Azpeitia', 'Beasain', 'Bergara', 'Donostia', 'Eibar', 'Hondarribia', 'Ir�n', 'Lasarte', 'Legazpi', 'Oiartzun', 'O�ati', 'Ordizia', 'Tolosa', 'Zarautz', 'Zumaia', 'Zum�rraga');
var Girona = new Array('Blanes', 'Cadaqu�s', 'Figueres', 'Girona', 'Lloret', 'Palam�s', 'Puigcerd�', 'Sant Feliu de Guixols');
var Granada = new Array('Almu��car', 'Baza', 'Granada', 'Guadix', 'Motril', 'Nevada', 'Sierra');
var Guadalajara = new Array('Guadalajara');
var Huelva = new Array('Ayamonte', 'Huelva', 'Utera');
var Huesca = new Array('Barbastro', 'Benasque', 'Bielsa', 'Fraga', 'Huesca', 'Jaca', 'Monz�n', 'Sabi��nigo', 'Sallent');
var Jaen = new Array('And�jar', 'Baeza', 'Cazorla', 'Ja�n', 'Linares', '�beda');
var La_Rioja = new Array('Alfaro', 'Arnedo', 'Calahorra', 'Ezcaray', 'Haro', 'Logro�o', 'N�jera', 'Sto. Domingo');
var Las_Palmas = new Array('Fuerteventura', 'Lanzarote', 'Las Palmas');
var Leon = new Array('Astorga', 'Le�n', 'Ponferrada');
var Lleida = new Array('Lleida', 'Seu d�Urgell', 'Tarreg�');
var Lugo = new Array('Lugo', 'Monforte de Lemos', 'Ribadeo');
var Madrid = new Array('Alcal�', 'Aranjuez', 'Brunete', 'Buitrago', 'Collado Villalba', 'Colmenar', 'El Escorial', 'Getafe', 'Madrid', 'Majadahonda', 'M�stoles', 'Navacerrada', 'Parla', 'Pozuelo', 'R. Chavela', 'Torrej�n');
var Malaga = new Array('Antequera', 'Benalm�dena', 'Estepona', 'M�laga', 'Marbella', 'Mijas', 'Nerja', 'Ronda', 'Torremolinos');
var Melilla = new Array('Melilla');
var Murcia = new Array('Aguilas', 'Caravaca', 'Cartagena', 'Jumilla', 'La Manga', 'Lorca', 'Los Alc�zares', 'Mazarr�n', 'Mula', 'Murcia', 'San Javier', 'Yecla');
var Navarra = new Array('Aoiz', 'Estella', 'Isaba', 'Leiza', 'Lesaka', 'Pamplona', 'Roncesvalles', 'Sang�esa', 'Tafalla', 'Tudela');
var Ourense = new Array('Ourense', 'Ver�n');
var Palencia = new Array('Palencia');
var Pontevedra = new Array('Baiona', 'Cambados', 'O Grove', 'Pontevedra', 'Tui', 'Vigo', 'Vilagarc�a de Arousa');
var Salamanca = new Array('B�jar', 'Ciudad Rodrigo', 'Salamanca');
var Santa_Cruz_de_Tenerife = new Array('Gomera', 'Hierro', 'La Palma', 'Santa Cruz', 'Tenerife Centro', 'Tenerife Sur');
var Segovia = new Array('Cuellar', 'Segovia');
var Sevilla = new Array('Alcal� de Guadaira', 'Carmona', 'Dos Hermanas', '�cija', 'Sevilla', 'Utrera');
var Soria = new Array('Burgo de Osma', 'Soria');
var Tarragona = new Array('Cambrils', 'Reus', 'Salou', 'Tarragona', 'Tortosa');
var Teruel = new Array('Albarrac�n', 'Alca�iz', 'Calamocha', 'Teruel');
var Toledo = new Array('Talavera de la Reina', 'Toledo');
var Valencia = new Array('Alzira', 'Cullera', 'Gand�a', 'Ontinyent', 'Sagunt', 'Valencia', 'Xativa');
var Valladolid = new Array('Medina del Campo', 'Valladolid');
var Zamora = new Array('Benavente', 'Toro', 'Zamora');
var Zaragoza = new Array('Calatayud', 'Caspe', 'Tarazona', 'Zaragoza');


function poblar() {
	var seleccionado = window.document.getElementById("provincia").options[window.document.getElementById("provincia").selectedIndex].value;
	if (seleccionado != "") {
		eval('poblaciones = ' + seleccionado);
		window.document.getElementById("localidad").length = poblaciones.length + 1;
		option = new Option("Seleccione Localidad","");
		window.document.getElementById("localidad").options[0] = option;
		for (i = 0; i < poblaciones.length; i++) {
			eval('option' + i + '= new Option(\"' + poblaciones[i] + '\", \"' + poblaciones[i] + '\")');
			eval('window.document.getElementById("localidad").options[i + 1]=option' + i);	
		}
		window.document.getElementById("localidad").selectedIndex = 0;
	}
	else {
		window.document.getElementById("localidad").length = 1;
		option = new Option("Seleccione Localidad","");
		window.document.getElementById("localidad").options[0] = option; 
	}
		
}

function poblar2() {
	var seleccionado = window.document.getElementById("provincia2").options[window.document.getElementById("provincia2").selectedIndex].value;
	if (seleccionado != "") {
		eval('poblaciones = ' + seleccionado);
		window.document.getElementById("localidad2").length = poblaciones.length + 1;
		option = new Option("Seleccione Localidad","");
		window.document.getElementById("localidad2").options[0] = option;
		for (i = 0; i < poblaciones.length; i++) {
			eval('option' + i + '= new Option(\"' + poblaciones[i] + '\", \"' + poblaciones[i] + '\")');
			eval('window.document.getElementById("localidad2").options[i + 1]=option' + i);	
		}
		window.document.getElementById("localidad2").selectedIndex = 0;
	}
	else {
		window.document.getElementById("localidad2").length = 1;
		option = new Option("Seleccione Localidad","");
		window.document.getElementById("localidad2").options[0] = option; 
	}
		
}

function setCookie_t(name, value, expires, path, domain, secure) {
	var curCookie = name + "=" + escape(value) + ((expires) ? "; expires=" + expires : "") + ((path) ? "; path=" + path : "") + ((domain) ? "; domain=" + domain : "") + ((secure) ? "; secure" : "");
	document.cookie = curCookie;
}
    	
function getCookie_t(name) {
	var dc = document.cookie;
	var prefix = name + "=";
	var begin = dc.indexOf("; " + prefix);
	if (begin == -1) {
		begin = dc.indexOf(prefix);
	if (begin != 0) return null;
	} else
		begin += 2;
	var end = document.cookie.indexOf(";", begin);
	if (end == -1)
		end = dc.length;
	return unescape(dc.substring(begin + prefix.length, end));
}

function withoutCutes(cadena) {
	
	cadena = cadena.replace(/ /g, '');
	cadena = cadena.replace(/�/gi, 'a');
	cadena = cadena.replace(/�/gi, 'e');
	cadena = cadena.replace(/�/gi, 'i');
	cadena = cadena.replace(/�/gi, 'o');
	cadena = cadena.replace(/�/gi, 'u');
	cadena = cadena.replace(/�/gi, 'u');
	cadena = cadena.replace(/�/gi, 'nn');
	cadena = cadena.replace(/-/gi, '');
	
	return cadena;

}


function actualizaTiempo(ciudad, tipo) {
	
	if (ciudad) {
		ciudad = withoutCutes(ciudad.toLowerCase());
		var http = createRequestObject();
		http.open("get", "/includes/manuales/tiempo/include_" + ciudad + ".xml?r=" + Math.random());
		http.onreadystatechange = function () {
			if(http.readyState == 4){
				if (http.responseXML) {
					cambiaTiempo(http.responseXML, tipo);
						
				}
			}	
		};
		http.send(null);
	}	
	else {
		window.alert("Por favor, seleccione una localidad.");
		return;		
	}
}

function cambiaTiempo(response, tipo) {
	if (response) {
		var localidad = response.getElementsByTagName('localidad').item(0).firstChild.data; 
		var mapa = response.getElementsByTagName('mapa').item(0).firstChild.data;
		
		// Esto es porque el Explorer y el Firefox interpretan distinto el XML
		
		if (response.getElementsByTagName('previsiones').item(0).childNodes.length == 13) {
			
			var tiempo_hoy = response.getElementsByTagName('previsiones').item(0).childNodes[7].childNodes[3].firstChild.data;
			var temperaturamax_hoy = response.getElementsByTagName('previsiones').item(0).childNodes[7].childNodes[5].firstChild.data;
			var temperaturamin_hoy = response.getElementsByTagName('previsiones').item(0).childNodes[7].childNodes[7].firstChild.data;
			
			var dia_manana = response.getElementsByTagName('previsiones').item(0).childNodes[9].childNodes[1].firstChild.data;
			var tiempo_manana = response.getElementsByTagName('previsiones').item(0).childNodes[9].childNodes[3].firstChild.data;
			var temperaturamax_manana = response.getElementsByTagName('previsiones').item(0).childNodes[9].childNodes[5].firstChild.data;
			var temperaturamin_manana = response.getElementsByTagName('previsiones').item(0).childNodes[9].childNodes[7].firstChild.data;
			
			var dia_pasado = response.getElementsByTagName('previsiones').item(0).childNodes[11].childNodes[1].firstChild.data;
			var tiempo_pasado = response.getElementsByTagName('previsiones').item(0).childNodes[11].childNodes[3].firstChild.data;
			var temperaturamax_pasado = response.getElementsByTagName('previsiones').item(0).childNodes[11].childNodes[5].firstChild.data;
			var temperaturamin_pasado = response.getElementsByTagName('previsiones').item(0).childNodes[11].childNodes[7].firstChild.data;
		}
		else {
			var tiempo_hoy = response.getElementsByTagName('previsiones').item(0).childNodes[3].childNodes[1].firstChild.data;
			var temperaturamax_hoy = response.getElementsByTagName('previsiones').item(0).childNodes[3].childNodes[2].firstChild.data;
			var temperaturamin_hoy = response.getElementsByTagName('previsiones').item(0).childNodes[3].childNodes[3].firstChild.data;
			var dia_manana = response.getElementsByTagName('previsiones').item(0).childNodes[4].childNodes[0].firstChild.data;
			var tiempo_manana = response.getElementsByTagName('previsiones').item(0).childNodes[4].childNodes[1].firstChild.data;
			var temperaturamax_manana = response.getElementsByTagName('previsiones').item(0).childNodes[4].childNodes[2].firstChild.data;
			var temperaturamin_manana = response.getElementsByTagName('previsiones').item(0).childNodes[4].childNodes[3].firstChild.data;
			var dia_pasado = response.getElementsByTagName('previsiones').item(0).childNodes[5].childNodes[0].firstChild.data;
			var tiempo_pasado = response.getElementsByTagName('previsiones').item(0).childNodes[5].childNodes[1].firstChild.data;
			var temperaturamax_pasado = response.getElementsByTagName('previsiones').item(0).childNodes[5].childNodes[2].firstChild.data;
			var temperaturamin_pasado = response.getElementsByTagName('previsiones').item(0).childNodes[5].childNodes[3].firstChild.data;
		}
		
		if (tipo ==  0) {
			var contenido = '<table border="0" cellspacing="0" cellpadding="0">';
			contenido += '          <tr>';
			contenido += '            <td rowspan="3" scope="col" class="imagentiempo"><img alt="" width="106" height="81" src="' + mapa + '"></td>';
			contenido += '            <th scope="col">';
			contenido += '              <strong>Hoy</strong>';
			contenido += '            </th>';
			contenido += '            <th scope="col">';
			contenido += '              <strong>' + dia_manana + '</strong>';
			contenido += '            </th>';
			contenido += '            <th scope="col">';
			contenido += '              <strong>' + dia_pasado + '</strong>';
			contenido += '            </th>';
			contenido += '            <td rowspan="3" scope="col" class="enlacetiempo">';
			contenido += '              <p>';
			contenido += '                <a href="#" onclick="cambiarLocalidad()">Cambiar<br>';
			contenido += '                  localidad</a>';
			contenido += '              </p>';
			contenido += '              <div class="separa"></div>';
			contenido += '              <p>';
			contenido += '                <a href="#" onclick="irMeteo(\'' + localidad + '\')">Ver<br>';
			contenido += '                    previsi&oacute;n</a>';
			contenido += '              </p>';
			contenido += '            </td>';
			contenido += '          </tr>';
			contenido += '          <tr>';
			contenido += '            <td><img src="/img/meteo/' + tiempo_hoy + '.gif" alt="" width="29" height="22"></td>';
			contenido += '            <td><img src="/img/meteo/' + tiempo_manana + '.gif" alt="" width="29" height="22"></td>';
			contenido += '            <td><img src="/img/meteo/' + tiempo_pasado + '.gif" alt="" width="29" height="22"></td>';
			contenido += '          </tr>';
			contenido += '          <tr>';
			contenido += '            <td class="temperatura">' + temperaturamax_hoy + '&ordm; / ' + temperaturamin_hoy + '&ordm;</td>';
			contenido += '            <td class="temperatura">' + temperaturamax_manana + '&ordm; / ' + temperaturamin_manana + '&ordm;</td>';
			contenido += '            <td class="temperatura">' + temperaturamax_pasado + '&ordm; / ' + temperaturamin_pasado + '&ordm;</td>';
			contenido += '          </tr>';
			contenido += '        </table>';
			
			window.document.getElementById('ciudad').innerHTML = 'en ' + localidad; 
		}
		else {
			var contenido = '<table border="0" cellspacing="0" cellpadding="0">';
			contenido += '    <tr>';
			contenido += '      <td rowspan="3" scope="col" class="imagentiempo"><img alt="" width="106" height="81" id="mapa" src="' + mapa + '"></td>';
			contenido += '      <th scope="col">';
			contenido += '        <strong>Hoy</strong>';
			contenido += '      </th>';
			contenido += '      <th scope="col">';
			contenido += '        <strong>' + dia_manana + '</strong>';
			contenido += '      </th>';
			contenido += '     <th scope="col">';
			contenido += '        <strong>' + dia_pasado + '</strong>';
			contenido += '      </th>';
			contenido += '    </tr>';
			contenido += '    <tr>';
			contenido += '      <td><img src="/img/meteo/' + tiempo_hoy + '.gif" alt="" width="29" height="22"></td>';
			contenido += '      <td><img src="/img/meteo/' + tiempo_manana + '.gif" alt="" width="29" height="22"></td>';
			contenido += '      <td><img src="/img/meteo/' + tiempo_pasado + '.gif" alt="" width="29" height="22"></td>';
			contenido += '    </tr>';
			contenido += '    <tr>';
			contenido += '        <td class="temperatura">' + temperaturamax_hoy + '&ordm; / ' + temperaturamin_hoy + '&ordm;</td>';
			contenido += '        <td class="temperatura">' + temperaturamax_manana + '&ordm; / ' + temperaturamin_manana + '&ordm;</td>';
			contenido += '        <td class="temperatura">' + temperaturamax_pasado + '&ordm; / ' + temperaturamin_pasado + '&ordm;</td>';
			contenido += '    </tr>';
			contenido += '  </table>';
		}
		
		window.document.getElementById('eltiempo-datos').innerHTML = contenido;
		
		window.document.getElementById('eltiempo-datos').style.display = 'block';
		window.document.getElementById('eltiempo-seleccion').style.display = 'none'	
		
		setCookie_t('ciudad', localidad, 'Tue, 31-Dec-2010 00:00:00 GMT');
	}
	
}


function cambiarLocalidad() {
	window.document.getElementById('eltiempo-datos').style.display = 'none';
	window.document.getElementById('eltiempo-seleccion').style.display = 'block'		
}

function irMeteo(ciudad) {
	
	ciudad = getCookie_t('ciudad') ? getCookie_t('ciudad') : ciudad;
	
	if (ciudad) {
		var dir = document.location.href;
		campos = dir.split("/");
		campos2 = campos[2].split(".");
		window.location = 'http://canalmeteo.' + campos2[1] + '.' + campos2[2]+ "/ciudad.php?ciudad=" + escape(ciudad);	
	}
}

function tieneCookie(tipo) {
	var ciudad = getCookie_t('ciudad');
	if (ciudad) {
		actualizaTiempo(ciudad, tipo);
	}	
}




