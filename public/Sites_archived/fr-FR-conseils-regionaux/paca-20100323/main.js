function windowOnload(f) {
        var prev=window.onload;
	window.onload=function(){ if(prev)prev(); f(); }
}


function getElementsByClass(searchClass,node,tag) {
  var classElements = new Array();
  if (node == null)
    node = document;
  if (tag == null)
    tag = '*';
  var els = node.getElementsByTagName(tag);
  var elsLen = els.length;
  var pattern = new RegExp("(^|\\s)"+searchClass+"(\\s|$)");
  for (i = 0, j = 0; i < elsLen; i++) {
    if (pattern.test(els[i].className) ) {
      classElements[j] = els[i];
      j++;
    }
  }
  return classElements;
}

function cacherNotesDeBasdDePage() {
	var notesDeBasDePage = getElementsByClass('sectionframe21') ;
	for (i = 0; i < notesDeBasDePage.length; i++){
		/*
		notesDeBasDePage[i].className = 'sectionframe21 noshow';
		notesDeBasDePage[i].style.visibility='hidden' ;
		*/
		notesDeBasDePage[i].style.display='none' ;
	};
}

function voirNbp(id){
	
	cacherNotesDeBasdDePage() ;
	var noteDeBasDePage = document.getElementById('content' +id);
	
	/*
	noteDeBasDePage.className = 'sectionframe21 show';
	noteDeBasDePage.style.visibility='visible' ;
	*/
	noteDeBasDePage.style.display='' ;
	location.href='#acontent'+id;
}

function voirNbpParType(letype){
	var notesDeBasDePage = getElementsByClass('nbp-' + letype) ;
	for (i = 0; i < notesDeBasDePage.length; i++){
		if (notesDeBasDePage[i].style.display == 'none'){
			notesDeBasDePage[i].style.display='' ;
			location.href= '#a'+notesDeBasDePage[i].id;
		}else{
			cacherNotesDeBasdDePage() ;
		}
	}
}

/* 
Remplace les ancres #acontentXXXX par javascript:voirNbp("XXXX");
*/
function convertNbpAnchorsToJS(){
        var touslesliens=document.getElementsByTagName("a");
        for(var n=0;n<touslesliens.length;n++){
                re = /acontent([0-9]+)$/;
                var found = touslesliens[n].href.match(re);
                if( found ){
                        touslesliens[n].href = 'javascript:voirNbp(\'' + found[1]  + '\');' ;
                }

	}
}


windowOnload(cacherNotesDeBasdDePage);
windowOnload(convertNbpAnchorsToJS);
