  var divlist = []
  var donthide = 0
  var thebody = document.getElementsByTagName('body').item(0);

  function search_element(id, list) {
    for(var i=0; list[i] && list[i].id != id; i++);
    return list[i];
  }

  function erase_menu(depth) {
      for(var i = 0; i < divlist.length; i++) {
        if(divlist[i].depth > depth)
          xHide(divlist[i])
      }
  }

  function clear_all_timeout() {
    for(var i = 0; i < divlist.length; i++) {
      if(divlist[i].timer)
        clearTimeout(divlist[i].timer)
    }
  }

  function go_url(event) {
    if(!event) {
      event = window.event
      object = event.srcElement
    }
    else
      object = event.target

    if(!object.tagName)
      object = object.parentNode

    document.location = object.href

  }

  function show_submenu(element, menu ,nav, event) {

    if(xIE4Up) {
      event = window.event
      if(!element) {
        element = event.srcElement
        menu = element.mymenu
      }
      else {
        element.depth = 0
      }
    } else {
      if(element && element.type) {
        event = element
        element = event.target
        menu = element.mymenu
      }
      else {
        element.depth = 0
      }
    }

    if(!element.tagName && element.parentNode) {
      donthide = 1
      erase_menu(element.depth)
      clear_all_timeout()
      return
    }


    if(!menu)
      return

    donthide = 1
    erase_menu(element.depth)
    clear_all_timeout()

    if(element.submenudiv) {
      xMoveTo(element.submenudiv, xPageX(element)+xWidth(element), xPageY(element));
      xShow(element.submenudiv);

      return
    }

    if(element.timeout)
        clearTimeout(element.timeout)

    if(!(menuitem  = search_element(element.id, menu)))
      return

    var submenu=menuitem.children

    if(submenu) {
        undiv = document.createElement('div')

        undiv.depth = element.depth + 1

        divlist[divlist.length] = undiv
        element.submenudiv = undiv

        thebody.appendChild(undiv)

        for(var i=0; submenu[i]; i++) {
            mona = document.createElement('a');
            undiv.appendChild(mona);
            mona.id = submenu[i].id;

            mona.innerHTML = submenu[i].title
            mona.href = submenu[i].url;
            mona.className = submenu[i].style;
            mona.depth = undiv.depth;
            mona.style.width = 160

            mona.mymenu = submenu;

            mona.onmouseover = show_submenu;
            mona.onmouseout = hide_submenu;
            mona.onclick = go_url;
        }

        undiv.style.position = 'absolute';
        undiv.style.width= 160;
        xMoveTo(undiv, xPageX(element)+xWidth(element), xPageY(element));
        xShow(undiv);
    }
  }

  function hide_all(element) {
    if(!donthide) {
      for(var i = 0; i < divlist.length; i++)
        xHide(divlist[i])
    }
  }

  function hide_submenu(event) {
    if(!event)
      event = window.event;

    if(xIE4Up)
      element = event.srcElement;

    else {
      element = event.target;
      toElement = event.relatedTarget
    }

    if(!element.tagName && element.parentNode)
      element = element.parentNode

    if(element.submenudiv) {
      donthide = 0
      element.submenudiv.timer = setTimeout('hide_all(element)',1500)
    }
  }
