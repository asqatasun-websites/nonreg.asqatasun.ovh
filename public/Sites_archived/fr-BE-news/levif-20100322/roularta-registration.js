Registration = (function(aRegistrationCode,aSource,aUrl,someSettings){
	var registrationCode = aRegistrationCode;
	var source = aSource;
	var profile = null;
        var url = aUrl;
	var settings = {
		cookiePrefix: 'userProfile_'
	};
	$.extend(settings,someSettings);
	
	function getRegistrationCode(){
		return registrationCode;
	}
	function getRegiCookie(){
		return GetCookie (settings.cookiePrefix + getRegistrationCode());
	}
	function isLoggedOn(){
		return getRegiCookie() != null;
	}
	function getUsername(){
		var username = (getRegiCookie().split(':'))[1];
		if(username.indexOf('|') != -1) {
			username = (username.split('|'))[0];
		}
		return username;
	}
	function getSecureCode(){
		return (getRegiCookie().split(':'))[0];
		
	}
	function getProfile(){
		if(isLoggedOn() && profile==null && RegistrationWSSoapHttpPort_getUserBySecurecode){
			profile = RegistrationWSSoapHttpPort_getUserBySecurecode(getRegistrationCode(),getSecureCode());
		}
		return profile;
	}
	function getUser(){
		var user = null;
		var profile = getProfile();
		if(profile!=null){
			user = profile.user;
		}
		return user;
	}
	function gotoRegistration(url,page,source){
		var regUrl = url + '/' + page + '?source=' + source + '&siteURL=' + escape(escape(document.location.href));
		location.href = regUrl;
	}
	function login(){
		gotoRegistration(url,'services/login.jsp',source);
	}
	function logoff(){
		gotoRegistration(url,'services/logout.jsp',source);
	}
	function register(){
		gotoRegistration(url,'services/enregistrement/nouveau.jsp',source);
	}
	function change(){
		gotoRegistration(url,'services/enregistrement/modification.jsp',source);
	}
	
	this.isLoggedOn = isLoggedOn;
	this.getUsername = getUsername;
	this.getUser = getUser;
	this.gotoRegistration = gotoRegistration;
	this.login = login;
	this.logoff = logoff;
	this.register = register;
	this.change = change;
	
});
