$E.load(

function() {

		var slide = document.getElementById('scrollZone');
		if(!slide) {return;}
		var itemsDisplayed = 2;
		var items = slide.getElementsByTagName('li');
		if(!items.length || items.length < itemsDisplayed) {return;}
		slide.className = 'js';
			
		if(slide.offsetWidth <= 370) {
			
			new slideCycle({
				context: slide,
				next: 'next',
				prev: 'prev',
				items: items,
				itemsDisplayed: itemsDisplayed,
				move: 188,
				offScreen: -10000
			});
		}
	}

);