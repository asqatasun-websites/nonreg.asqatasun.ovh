function nom_autorise(Code,Message)
{
	re = /([a-z0-9_.]*)/i;
	found = Code.value.match(re);
	if (found!=Code.value+","+Code.value)
	{
		alert(Message);
		return (false);
	}
	return (true);
}


function ValidURL(Code,Message)
{
	re = /([a-z0-9_\/:.]*)/i;
	found = Code.value.match(re);
	if (found!=Code.value+","+Code.value)
	{
		alert(Message);
		return (false);
	}
	return (true);
}

function Nombre_Entier_Valide(Code,Message,val_min,val_max)
{
	re = /[0-9]*/i;
	found = Code.value.match(re);
	if (found!=Code.value)
	{
		alert(Message);
		return (false);	
	}

	v=parseInt(Code.value,10);
	if (isNaN(v))
	{
		alert(Message);
		return (false);	
	}
	if ((val_min != -1) && (v<val_min))
	{
		alert(Message);
		return (false);	
	}	

	if ((val_max != -1) && (v>val_max))
	{
		alert(Message);
		return (false);	
	}	


	return (true);
}



function applySubmit(agentName){
	if( document.form.isContext.value==0  )
	{
		// SearchAdmin ou Search
		document.form.Agent.value=agentName;
	}
	document.form.isContext.value=0;

	return true;
}
function showContext(agentName){
	document.form.isContext.value=1;
	// ShowContextAdmin ou ShowContext
	document.form.Agent.value=agentName;
	return true;
}
function showSearch(agentName){
	document.form.isContext.value=0;
	// SearchAdmin ou Search
	document.form.Agent.value=agentName;	
	return true;
}
