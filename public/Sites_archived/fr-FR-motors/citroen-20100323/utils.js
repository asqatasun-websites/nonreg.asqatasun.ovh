<!-- To open an html popup -->
function popup(adr, name, width, height, is_scrollbar) {
	if (typeof(name) == "undefined") {
		name = "popup";
	}
	if (typeof(width) == "undefined") {
		width = screen.width;
	}
	if (typeof(height) == "undefined") {
		height = screen.height;
	}
	if (typeof(is_scrollbar) == "undefined") {
		is_scrollbar = "false";	
	}
	var largeur=screen.availWidth;
    var hauteur=screen.availHeight;
    var demiL=(eval(largeur-width)/2);
    var demiH=(eval(hauteur-height)/2);
    name = window.open(adr,name,'scrollbars='+is_scrollbar+',screenX=0,screenY=0,status=0,location=0, menubar=0,toolbar=0,resizable=0,width='+width+',height='+height+',top='+demiH+',left='+demiL);
}

<!-- Google Analytics tag -->
function toGA(value) {
	try {
		pageTracker._trackPageview(value);
	} catch(err) {}
}

<!-- Redirection to HTML version -->
function redirect() {
	window.location.replace("http://url/de/redirection/");
}

<!-- Place the focus on the swf object -->
function setFlashFocus() {
	if (document.getElementById('content')) {
		document.getElementById('content').focus();
	}
}
