var MTVNI = {};

MTVNI.Global = (function(){

	var _hatTotal = 0;

/* Public Properties and Methods */
   return {
		/* ****** random hats  ****** */
	   hatTrick : function(hatNumber, subset) {
			// var total = 66; // now set in MtvniOnScenic.xml, then in local xml configs per region. DA
			//var total = 0;

			if (localHatTotal && localHatTotal>0){_hatTotal = localHatTotal};
			if (_hatTotal != 0){
				var bodyTag = document.getElementsByTagName('body')[0];
				var classList = bodyTag.className;
				if (classList.indexOf("hat") > -1) {
					var tempClasses = new Array();
					var classes = classList.split(" ");
					var j = 0;
					for(i = 0; i < classes.length; i++) {
						if(classes[i].substr(0,3) != "hat") tempClasses[j++] = classes[i];
					}
					classList = tempClasses.join(" ");
				}
				if (!hatNumber) hatNumber = Math.floor(_hatTotal*Math.random());
				var newHat = " hat" + hatNumber;

			// allow for static overrides from ISIS in HatOverride
			// if no manual override in ISIS - go with random hats
			if(typeof(staticOverrideFlag) == "undefined"){	
					bodyTag.className = classList + newHat + "";
				}
			else {
				if(staticOverrideFlag == "on"){
					var overrideClass = staticOverrideClass;
					bodyTag.className = classList + " " + overrideClass + "";
					}
				}
			}
		} /* ****** end of random hats  ****** */

	}
})();



/*	
Script: dropdownmenu.js
	Fx.DropdownMenu.
		
Dependencies:
	<mootools.js>
Author:
	Andr? Fiedler, <http://visualdrugs.net>

License:
	MIT-style license.

Class: Fx.PopMenu
	The DropdownMenu function creates a group of elements that have the behaviour of an dropdown menu.
	
Arguments:
	element - a list-element the effect will be applied to.
	
Example:
	(start code)
	<ul id="dropdownMenu">
		<li>
			<a href="#">Menu 1</a>
			<ul>
				<li><a href="#">SubMenu 1</a></li>
				<li><a href="#">SubMenu 2</a></li>
				<li><a href="#">SubMenu 3</a></li>	
			</ul>
		</li>
		<li><a href="#">Menu 2</a></li>
		<li><a href="#">Menu 3</a></li>	
	</ul>
	
	<script type="text/javascript">
	
		Window.onDomReady(function() {new DropdownMenu($('dropdownMenu'))});
		
	</script>
	(end)
*/
/* 
modified - 11/26/2008 - DA
fixed flickering of menus as pages load. 
fixed 3rd level nav hidden in IE6
instead of show()/hide(), hide nav sub-menus in css:
then add/remove a class on mouseover/mouseout to toggle display to block
	#topmenu ul { display: none;}
	#topmenu ul.showSubMenu { display: block;}

modified may 28 2009 to work w mootools 1.2
and added delay/fade to justify using a script in the first place...
NOTE = if no tween / no delay, need to use mousenter/mouseleave for IE and mouseover/mouseout for all others - especially opera 
*/
var DropdownMenu = new Class({
		initialize: function(element) 
		{
			$A($(element).childNodes).each(function(el)
			{
				if(el.nodeName.toLowerCase() == 'li')
				{
					$A($(el).childNodes).each(function(el2)
					{
						if(el2.nodeName.toLowerCase() == 'ul')
						{
							//el.addEvent('mouseover', function() //changing this mootools 1.2
							/*
							el.addEvent('mouseenter',function() {
								new Fx.Morph(el2, {duration:10, onComplete: function(){
									el2.addClass("showSubMenu");
									}}).start({'opacity':1}); 
								//return false; // removing this w mootools 1.2
							});
							*/
							el.addEvent('mouseenter',function() {
							//	el2.set("opacity",1); //ie is not too happy if we tween opacity in...
								el2.addClass("showSubMenu");
								//return false; // removing this w mootools 1.2
							});

							//el.addEvent('mouseout', function() //changing this mootools 1.2
						/*	el.addEvent('mouseleave', function() {
						        new Fx.Morph(el2, {duration:250, onComplete: function() {
						        	//.removeClass("showSubMenu");
						        	el2.removeClass("showSubMenu");
						        }}).start({'opacity':.3}); 
						     */   

						        el.addEvent('mouseleave', function() {
							      //  new Fx.Tween(el2, {duration:250, onComplete: function() {
							      //  	//.removeClass("showSubMenu");
							      //  	el2.removeClass("showSubMenu");
							      //  }}).start('opacity',.3); 

								el2.removeClass("showSubMenu");
							});

							new DropdownMenu(el2);
						}
					});
				}
			});
			return this;
		}
	});