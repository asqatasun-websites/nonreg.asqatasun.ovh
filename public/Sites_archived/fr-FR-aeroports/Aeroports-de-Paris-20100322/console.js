// Client-side Javascript used for dragging edit console and opening 
// and closing menus. Should be placed in external .js file for easier
// code maintenance and enhanced performance 

var dragOn=0
var dragDiv=null;
var dragX=300,dragY=300;
var zMax=0;
var dragInit=0;

//document.top_menu.width = "200";
//alert(eval(document.top_menu.width));
//alert(document.top_menu.offSetLeft);
//alert(document.body.clientHeight);
//alert(document.body.clientWidth)

/*
var screenWidth = document.body.clientWidth;
var coordinateX = Math.round(screenWidth * 0.1)
alert(coordinateX);
*/

/*
function getXYcoord ( nvn ) {
    var elm = document.images[nvn];
//alert ( ""+nvn +"  " +elm );
    var rd = { x:0 ,y:0 };
    do { rd.x += parseInt( elm.offsetLeft );
         rd.y += parseInt( elm.offsetTop );
         elm = elm.offsetParent;
    } while ( elm );
    return rd
};

if (document.layers) {
    alert( "X-koordinaten er: " +document.images["top_menu"].x +"\n"
          +"Y-koordinaten er: " +document.images["top_menu"].y );
  } else {
    var coord = getXYcoord ( "top_menu" );
    alert( "X-koordinaten er: " +coord.x +"\n"
          +"Y-koordinaten er: " +coord.y );
  }
*/

/*
var ns4 = (document.layers) ? true : false;
var ie4 = (document.all) ? true : false;

function getXYcoord ( elm ) {
    if ( ns4 ) return elm;
    var rd = { x:0 ,y:0 };
    do { rd.x += parseInt( elm.offsetLeft );
        rd.y += parseInt( elm.offsetTop );
        elm = elm.offsetParent;
    } while ( elm );
    return rd
}; //end getXYcoord ( string ) -> object{x,y}

function findBilled( imgNavn ) {
    return getXYcoord( document.images[imgNavn] );
}; //end findBilled( string )

//test af find koordinater:
var  coords = findBilled("top_menu");
alert( "top_menu er placeret på x,y : " +coords.x + "," +coords.y );
//endtest.

function placer( divnavn, posx, posy ) {
    var obj = (ns4)? document.layers[divnavn] : ( (ie4)? document.all[divnavn] : document.getElementById(divnavn) ); alert( "obj er " +obj.name );
    var styleObj = (ns4)? obj : obj.style;
    if (ns4) {
        styleObj.left = posx;
        styleObj.top  = posy;
    } else {
      styleObj.posLeft = posx;
      styleObj.posTop  = posy;
    };
    styleObj.zIndex = 5;
    styleObj.visibility = "visible";
};

*/


function initDrag() {
	if (document.layers) 
	   document.captureEvents(Event.MOUSEMOVE|Event.MOUSEDOWN|Event.MOUSEUP);
	document.onmousemove=dragf;
	document.onmousedown=dragf;
	document.onmouseup=dragf;
	dragDiv=null;
	dragInit=1;
	if (document.getElementsByTagName) 
	   zMax=document.getElementsByTagName("DIV").length;
	else if (document.all) zMax=document.body.all.tags("DIV").length;
	else if (document.layers) zMax=document.layers.length;
}

function dragf(arg) {
	ev=arg?arg:event;
	if (dragDiv && ev.type=="mousedown") {
		dragOn=1;
		dragX=(ev.pageX?ev.pageX:ev.clientX)-parseInt(dragDiv.style.left);
		dragY=(ev.pageY?ev.pageY:ev.clientY)-parseInt(dragDiv.style.top);
		dragDiv.style.zIndex=zMax++; // remove this line to preserve z-indexes
		return false;
	}
	if (ev.type=="mouseup") {
		dragOn=0;
	}
	if (dragDiv && ev.type=="mousemove" && dragOn) {
		dragDiv.style.left=(ev.pageX?ev.pageX:ev.clientX)-dragX;
		dragDiv.style.top=(ev.pageY?ev.pageY:ev.clientY)-dragY;
		return false;
	}
	if (ev.type=="mouseout") {
		if (!dragOn) dragDiv=null;
	}
}

function drag(div) {
	if (!dragInit) initDrag();
	if (!dragOn) {
		dragDiv=document.getElementById?document.getElementById(div): 
		document.all?document.all[div]:document.layers?document.layers[div]:null;
		if (document.layers) dragDiv.style=dragDiv;
		dragDiv.onmouseout=dragf;
	}
}

function OpenCloseDiv(divName){
	if (divName.style.display == "none") {
		divName.style.display="block";
		txtConsole.innerHTML = "Masquer la console";
	}
	else {
		divName.style.display="none";
		txtConsole.innerHTML = "Afficher la console";
	}
}
