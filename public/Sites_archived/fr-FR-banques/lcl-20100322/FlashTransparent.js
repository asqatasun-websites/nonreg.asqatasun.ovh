__FLASH_CONTAINER__ = "FlashInterstitialDiv";

function setFlashInterstialSize()
{
if((document.getElementById(__FLASH_CONTAINER__))&&(__FlashdivWidth__)&&(__FlashdivHeight__))
	{
		var __Flashdiv__ = document.getElementById("FlashInterstitialDiv");
		__Flashdiv__.style.height= __FlashdivHeight__+'px';
		__Flashdiv__.style.width= __FlashdivWidth__+'px';
	}
}


function center_flash(onload){
if (document.getElementById(__FLASH_CONTAINER__))
			center_layer('par_1024.swf',5,0,10,onload);
}

function center_layer(name,cap,x,y,v){
var divFlashContainer = document.getElementById(__FLASH_CONTAINER__);

var docwidth;
var docheight;

//opera Netscape 6 Netscape 4x Mozilla 
if (window.innerWidth || window.innerHeight){ 
docwidth = window.innerWidth; 
docheight = window.innerHeight; 
} 
//IE Mozilla 
if (document.body.clientWidth || document.body.clientHeight){ 
docwidth = document.body.clientWidth; 
docheight = document.body.clientHeight; 
}
if (getStyle(divFlashContainer, "width")=='auto'){
intLeftPos=docwidth/2;
}
else{
intLeftPos=docwidth/2 - parseInt(getStyle(divFlashContainer, "width"))/2;
}
if (getStyle(divFlashContainer, "height")=='auto'){
intTopPos=docheight/2;
}
else{
intTopPos=docheight/2 - parseInt(getStyle(divFlashContainer, "height"))/2;
}


if(x==null) x=0;
if(y==null) y=0;

setStyle(__FLASH_CONTAINER__,"left", intLeftPos+x + "px");
setStyle(__FLASH_CONTAINER__,"top", intTopPos+y + "px");

//dlib_layers("FlashInterstitialDiv", VISIBLE, true);
  if(v)
  {
    cookie = getCookieCap(name);
    var now = new Date();
    isIE = (document.all);
    var date = new Date(now.getYear() + (isIE ? 1 : 1901), now.getMonth(), now.getDate());
    if(cookie)
    {
      if (cap > cookie || cap <= 0)
      {     
        divFlashContainer.style.visibility="visible"; 
      	setCookieCap(name, Number(cookie) + 1, date, '/');
      }   	         
    }
    else
    {
      divFlashContainer.style.visibility="visible";          
      setCookieCap(name, "1", date, "/");
    }
   }
}

function setCookieCap(name, value, expires, path, domain, secure)
{
document.cookie= name + "=" + escape(value) +
((expires) ? "; expires=" + expires.toGMTString() : "") +
((path) ? "; path=" + path : "") +
((domain) ? "; domain=" + domain : "") +
((secure) ? "; secure" : "");

}

function getCookieCap(name)
{
var dc = document.cookie;
var prefix = name + "=";
var begin = dc.indexOf("; " + prefix);
if (begin == -1)
{
begin = dc.indexOf(prefix);
if (begin != 0) return null;
}
else
{
begin += 2;
}
var end = document.cookie.indexOf(";", begin);
if (end == -1)
{
end = dc.length;
}
return unescape(dc.substring(begin + prefix.length, end));
}


function getStyle(el, style) {
   if(!document.getElementById) return;
   
     var value = el.style[toCamelCase(style)];
   
    if(!value)
        if(document.defaultView)
            value = document.defaultView.
                 getComputedStyle(el, "").getPropertyValue(style);
       
        else if(el.currentStyle)
            value = el.currentStyle[toCamelCase(style)];
     
     return value;
}

function setStyle(objId, style, value) {
    document.getElementById(objId).style[style] = value;
}

function toCamelCase( sInput ) {
    var oStringList = sInput.split('-');
    if(oStringList.length == 1)   
        return oStringList[0];
    var ret = sInput.indexOf("-") == 0 ?
       oStringList[0].charAt(0).toUpperCase() + oStringList[0].substring(1) : oStringList[0];
    for(var i = 1, len = oStringList.length; i < len; i++){
        var s = oStringList[i];
        ret += s.charAt(0).toUpperCase() + s.substring(1)
    }
    return ret;
}

function increaseWidth(addToWidth, whichDiv){
    var theDiv = document.getElementById(whichDiv);
    var currWidth = parseInt(getStyle(theDiv, "width"));
    var newWidth = currWidth + parseInt(addToWidth);
    setStyle(whichDiv, "width", newWidth + "px");
}