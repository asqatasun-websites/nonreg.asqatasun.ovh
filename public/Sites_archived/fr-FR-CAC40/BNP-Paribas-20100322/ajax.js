function makeRequest(url) {
      	var xhr = false;	

        if (window.XMLHttpRequest) { // Mozilla, Safari,...
            xhr = new XMLHttpRequest();
            if (xhr.overrideMimeType) {
                xhr.overrideMimeType('text/xml');
                // Voir la note ci-dessous � propos de cette ligne
            }
        }
        else if (window.ActiveXObject) { // IE
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (e) {
                try {
                    xhr = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {}
            }
        }

        if (!xhr) {
            alert('Abandon :( Impossible de cr�er une instance XMLHTTP');
            return false;
        }


		xhr.onreadystatechange = function() { alertContents(xhr); };
        xhr.open('GET', url, true);
		xhr.send(null);
		setTimeout(abortRequest, 5000);

		
// on annule la requete apr�s 5 secondes
function abortRequest() {
		var content = document.getElementById('ShareContent');
		//si la requete n'est pas termin�e
		if (xhr.readyState != 4) {
			//on affiche le message d'erreur
			content.innerHTML = "Cours BNP Paribas : <a href='http://invest.bnpparibas.com/fr/action/cotation.asp' class='Under'>cliquez ici</a>";  
			//on crash la requete
			xhr.abort();
		}
} 	
	
    }

function alertContents(xhr) {
		var content = document.getElementById('ShareContent');
		

		if(xhr.readyState == 1){
			//content.innerHTML = '<img src="loading.gif">';
		}


		if (xhr.readyState == 4) {
            if (xhr.status == 200) {
               content.innerHTML = xhr.responseText;
            } 
			else if (xhr.status == 404) {
				content.innerHTML = "Cours BNP Paribas : <a href='http://invest.bnpparibas.com/fr/action/cotation.asp' class='Under'>cliquez ici</a>";
			}
			else if (xhr.status == 500) {
				content.innerHTML = "Cours BNP Paribas : <a href='http://invest.bnpparibas.com/fr/action/cotation.asp' class='Under'>cliquez ici</a>";
			}
			else {
				content.innerHTML = "Cours BNP Paribas : <a href='http://invest.bnpparibas.com/fr/action/cotation.asp' class='Under'>cliquez ici</a>";
			}
        }
}