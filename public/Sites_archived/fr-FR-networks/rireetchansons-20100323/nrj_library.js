/*
 *	 _   _  ____      _     __
 *	| \ | ||  _ \    | |   / _| _ __
 *	|  \| || |_) |_  | |  | |_ | '__|
 *	| |\  ||  _ <| |_| |_ |  _|| |
 *	|_| \_||_| \_\\___/(_)|_|  |_|
 *
 *
 *  FUNKY TOOLS
 *  ------------------------------------
 *
 *  @author: NRJ Dev Team
 *  @date: 8 septembre 2009
 *  @revision: 10
 *  @version: 1.0
 *
 */

var Nrj_Library = {
	
	superloader: url_base + '/images/ajax_loader.gif',
	nextstep: '',
	
	// Déroulé Webradios
	init: function() { 
			
		if(!Nrj_Library.getCookie('deroule_wr_j'))
		{
			var domni = window.location.hostname.replace( /^[^.]*/i, "" );
			new Effect.BlindDown(document.getElementById('bloc_top1'));
			Nrj_Library.setCookie(domni + 'deroule_wr_j', true, 4);
			window.setTimeout("Nrj_Library.initClose();", 6000);
		}
	
	},
	
	initClose: function() {
		new Effect.BlindUp(document.getElementById('bloc_top1'));
	},

	setCookie: function(nom,valeur,jours) 
	{
        var expDate = new Date()
        expDate.setTime(expDate.getTime() + (jours * 24 * 3600 * 1000))
        document.cookie = nom + "=" + escape(valeur)
            + ";expires=" + expDate.toGMTString()
    },
    
    getCookie: function(nom) {
       deb = document.cookie.indexOf(nom + "=")
       if (deb >= 0) {
           deb += nom.length + 1
           fin = document.cookie.indexOf(";",deb)
           if (fin < 0) fin = document.cookie.length
           return unescape(document.cookie.substring(deb,fin))
           }
       return ""
    },
        
	onDOMReady2: function( callback )
	{
		document.addEventListener("DOMContentLoaded", callback, false);
	},
	
	onDOMReady: function(handler){
		if (document.addEventListener) {
			if (navigator.userAgent.indexOf('AppleWebKit/') > -1 || window.opera){
			var timer = window.setInterval(function() {
			if (/loaded|complete/.test(document.readyState)){
				window.clearInterval(timer);
				handler();
			 }
	 }, 30);
	 }else document.addEventListener('DOMContentLoaded', handler, false);
	 }else{
	 var tempNode = document.createElement('document:ready');
	 (function(){
	 try {
	 if(document.readyState != 'complete')
	 return setTimeout(arguments.callee, 30);
	 tempNode.doScroll('left');
	 tempNode = null;
	 handler();
	 }catch (e){
	 setTimeout(arguments.callee, 30);
	 }
	 })()
	 }
	 },

	// Use ! in divId to activate NO INDICATOR
	Ajax: function(url, divId, params, options, method)
	{
		if(options == null)  { options = 'application/x-www-form-urlencoded'; }

	    if(divId.match("!") )
	    {
	    	var adds = 1;
	    	divId = divId.replace("!", '');
	    }
	    else
	    {
	    	var adds = 0;
	    }
	    
	    // NO INDICATOR WHERE ! IN DIVID
	    if( adds == 1)
	    {
	    	// nothing
	    }
	    else
	    {
	    	document.getElementById(divId).innerHTML = "<img src='" + this.superloader + "'>";
	    }
	    
		do_ajax = new Ajax.Request(url, {method: 'post', encoding: 'UTF-8', contentType: options, parameters: params, onComplete: function(request){

			document.getElementById(divId).innerHTML = request.responseText;
			ajax_exec(divId);
			
			}
		});
	},


	// Use ! in divId to activate NO INDICATOR
	Get: function(url, divId, params, options)
	{
		if(options == null)  { options = 'application/x-www-form-urlencoded'; }

	    if(divId.match("!") )
	    {
	    	var adds = 1;
	    	divId = divId.replace("!", '');
	    }
	    else
	    {
	    	var adds = 0;
	    }
	    
	    // NO INDICATOR WHERE ! IN DIVID
	    if( adds == 1)
	    {
	    	// nothing
	    }
	    else
	    {
	    	document.getElementById(divId).innerHTML = "<img src='" + this.superloader + "'>";
	    }
	    
		do_ajax = new Ajax.Request(url, {method: 'get', encoding: 'UTF-8', contentType: options, parameters: params, onComplete: function(request){

			document.getElementById(divId).innerHTML = request.responseText;
			ajax_exec(divId);
			
			}
		});
	},
	
	// Ajax plus Callback
	Execute: function(url, params, callback)
	{
		do_ajax = new Ajax.Request(url, {method: 'post', encoding: 'UTF-8', parameters: params, onComplete: callback});
	},
	
	PopinClose: function()
	{
		document.getElementById('popin-login').style.display = 'none';
		document.getElementById('masque').style.display = 'none';
	},
	
	PopinOpen: function()
	{
		document.getElementById('popin-login').style.display = '';
		document.getElementById('masque').style.display = '';
		
		document.getElementById('step1').style.display = '';
		document.getElementById('step2').style.display = 'none';
	},
	
	Okay: function(reponse)
	{
		if(reponse == 1)
		{
			document.getElementById('step1').style.display = 'none';
			document.getElementById('step2').style.display = '';
			if(Nrj_Library.next == 'validate') document.getElementById('test-forming').submit();
		}
		else
		{
			alert("Désolé, nous ne reconnaissons pas ces identifiants, veuillez réessayer");
		}
	},
	
	CheckState: function(next)
	{
		do_ajax = new Ajax.Request(url_base + "/connexion/connected", {method: 'post', encoding: 'UTF-8', parameters: {test:1}, onComplete: function(request){ if(request.responseText == 1) { alert('Vous êtes déjà connecté'); } else { Nrj_Library.next=next; Nrj_Library.PopinOpen(); }  } });
	},
	
	Login: function(login, password)
	{
		$('indicators').innerHTML = "<img src='" + this.superloader + "'>";
		do_ajax = new Ajax.Request(url_base + "/connexion/identification", {method: 'post', encoding: 'UTF-8', parameters: {ajax: 1, username: login, password: password}, onComplete: function(request){ $('indicators').innerHTML = ''; Nrj_Library.Okay(request.responseText); } });
	}
		
}

// Init déroulé web radio
Nrj_Library.onDOMReady(Nrj_Library.init);