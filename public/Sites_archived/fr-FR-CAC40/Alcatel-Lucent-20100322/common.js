/* header rollovers */

var imageBaseURL = "images/";


/* more button functions */

var morecount = 0;

function showmore(which) {
	getObj("showmore" + which).style.display = "none";
	getObj("content" + which).style.display = "block";
	getObj("less" + which).style.display = "block";	
}

function showless(which) {
	getObj("content" + which).style.display = "none";
	getObj("less" + which).style.display = "none";
	getObj("showmore" + which).style.display = "block";
}

/*
function makeMore(data) {	
            var theContent = "<div id='content" + morecount + "' class='nodisplay'>" + data + "</div>";
            var theMore = "<div id='showmore" + morecount + "' class='brdt martp3 padt6'><ul class='list font10pt'><li class='plusbtm'><b><a href='javascript:showmore(" + morecount + ");' onselect='showmore(" + morecount + ");' class='nound' title='More Lorem'>More</a></b></li></ul></div>";
            var theLess = "<div id='less" + morecount + "' class='nodisplay brdt martp3 padt6'><ul class='list font10pt'><li class='minusbtm'><b><a href='javascript:showless(" + morecount + ");' onselect='showless(" + morecount + ");' class='nound'>Less</a></b></li></ul></div>";
			document.write(theContent, theMore, theLess);			
            morecount++;
}

*/
function makeMore(data, brdt, brdb) {	
            var theContent = "<div id='content" + morecount + "' class='nodisplay'>" + data + "</div>";
            var brdtop = "<div class='martp5 marb5 " + brdt + "'></div>";
			var theMore = "<div id='showmore" + morecount + "' class='padb6'><ul class='list font10pt'><li class='plusbtm'><b><a href='javascript:showmore(" + morecount + ");' onselect='showmore(" + morecount + ");' class='nound' title='More Lorem'>More</a></b></li></ul></div>";
            var theLess = "<div id='less" + morecount + "' class='nodisplay padb6'><ul class='list font10pt'><li class='minusbtm'><b><a href='javascript:showless(" + morecount + ");' onselect='showless(" + morecount + ");' class='nound'>Less</a></b></li></ul></div>";
			var brdbot = "<div class='martp5 marb5 " + brdb + "'></div>";
			document.write(theContent, brdtop, theMore, theLess, brdbot);			
            morecount++;
}



function makeMoreURLList(data, brdt, brdb) {		
	    alert("makeMoreURLList = " + data);
            var theContent = "<div id='content" + morecount + "' class='nodisplay'>" + data + "</div>";
            var brdtop = "<div class='martp5 marb5 " + brdt + "'></div>";
			var theMore = "<div id='showmore" + morecount + "' class='padb6'><ul class='list font10pt'><li class='plusbtm'><b><a href='javascript:showmore(" + morecount + ");' onselect='showmore(" + morecount + ");' class='nound' title='More Lorem'>More</a></b></li></ul></div>";
            var theLess = "<div id='less" + morecount + "' class='nodisplay padb6'><ul class='list font10pt'><li class='minusbtm'><b><a href='javascript:showless(" + morecount + ");' onselect='showless(" + morecount + ");' class='nound'>Less</a></b></li></ul></div>";
			var brdbot = "<div class='martp5 marb5 " + brdb + "'></div>";
			document.write(theContent, brdtop, theMore, theLess, brdbot);			
            morecount++;
}


/* expand collapse button functions */

function makeResults(vmore, vless, data) {	
    var theMore = "<div id='showmore" + morecount + "' class=''>" + vmore + "</div>";
	var theLess = "<div id='less" + morecount + "' class='nodisplay'>" + vless + "</div>";
	var theContent = "<div id='content" + morecount + "' class='nodisplay'>" + data + "</div>";
	document.write(theMore, theLess, theContent);			
    morecount++;
}

var expandClickOn = false;
var closebtnOn = false
var countClicks = 0;

function expandAll(){
	countClicks = morecount;
	for (i = 0; i < morecount; i++)
		{
		showmore(i);							
		}
	getObj("expand").style.display = "none";
	getObj("collapse").style.display = "block";
}

function collapseAll(){
	countClicks = 0;
	for (i = 0; i < morecount; i++)
		{
		showless(i);							
		}
	getObj("expand").style.display = "block";
	getObj("collapse").style.display = "none";
}

/* Functions to close all button after it is extended and the subsections are minimized */


function trackExpand (clickedState){
	expandClickOn = clickedState;
	//countClicks = morecount;													
}

function addClicks (){
	countClicks++;
	if (countClicks >= morecount) {
		expandAll();
	}																		
}	

function subClicks (){							
	countClicks--;
	if (countClicks <= 0) {
		collapseAll();
	}							
}
						

/* end expand all functions */

/* font resize functions */

function setActiveStyleSheet(title) {
   var i, a, main;
   for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
     if(a.getAttribute("rel").indexOf("style") != -1
        && a.getAttribute("title")) {
       a.disabled = true;
       if(a.getAttribute("title") == title) a.disabled = false;
     }
   }
}

function getActiveStyleSheet() {
var i, a;
 for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
  if(a.getAttribute("rel").indexOf("style") != -1
  && a.getAttribute("title")
  && !a.disabled) return a.getAttribute("title");
  }
  return null;
}

/* begin search criteria functions */

						 						 
/* this function controls whether the initial comma delimited string is concatenate */
function writeCopy(schHolder, slength, lID, morebold){
	if (morebold) {
		var bOn = "<b>";
		var bOff = "</b>";
	} else {
		var bOn = "";
		var bOff = "";
	}
	var countme = 0;
	var position = 0;
	//var temp = myText;
	
	for (var i = 0 ; i < schHolder.length; i++) { 
        	if((schHolder.substring(i,i+1) == delimiter)) {
			countme++							
		}														 						  
	 }
	 
	if(countme >= slength){							
		for (var j = 0 ; j < slength; j++) { 
		 	 position = schHolder.indexOf(delimiter, position + 1);
		}
		var searchText = schHolder.substring(0, position);	
		searchText = (searchText + ellipsis);
		var lexpand = "<div class='padt4'><ul class='list font10pt'><li class='plusbtm'><a href='javascript:expandCopy(" + '"searchcopy"' + ");' class='nound'>" + bOn + "More" + bOff + "</a></li></ul></div>";	
	} else {
		//alert("more than 5");
		var searchText = searchCriteria;
		var lexpand = " ";
	}								
	var fullSearch = getObj(lID);
	fullSearch.innerHTML = searchText;					
	document.write(lexpand);
}

/* expandCopy and minimizeCopy show and hide the string only after writeCopy is called and the number of commas is longer than the limit set by commaNum */
function expandCopy(lID){
	var searchText = searchCriteria;
	var fullSearch = getObj(lID);
	fullSearch.innerHTML = searchText;
	turnExpand();
}		

function minimizeCopy(slength, lID){
	var fullSearch = getObj(lID);
	var searchText = " ";
	var position = 0;
	for (var j = 0 ; j < slength; j++) { 
		 	 position = searchCriteria.indexOf(delimiter, position + 1);
		}								
	searchText = searchCriteria.substring(0, position);							
	fullSearch.innerHTML = (searchText + ellipsis);
	turnContract();
}		

/*supporting functions for hide and display of more and less buttons*/
function turnExpand(){
 	getObj("expandSearch").style.display = "none";
	getObj("contractSearch").style.display = "block";
}

function turnContract(){
 	getObj("expandSearch").style.display = "block";
	getObj("contractSearch").style.display = "none";
}		


/* end search criteria functions */

/* common functions */

function getObj(theId) {
	if (document.getElementById) {
		theObj = document.getElementById(theId); 
	} else if (document.all) { 
		theObj = document.all[theId];
	}
	return theObj;
}

/*Function for displaying tabs*/

var tabPageName;

function showTab(which)
{
	total = document.getElementById( "numTabs" ).value;
	for (var i = 1; i <=total; i++)	
	{
		if (i == which)		
		{
			if ( document.getElementById( "tabName" + i ) ) {
				document.getElementById( "tabData" + i ).style.display = "";
				document.getElementById( "tabName" + i ).className = "wpsPortletSelectedTab";
				document.getElementById( "tabLink" + i ).className = "font10pt bold nound";
			}
			if (document.location.href.indexOf("whichTab=") < 0)
			{
				document.location.href = "#tabAnchor" + i;
			}
		}
		else
		{
			if ( document.getElementById( "tabName" + i ) ) {
				document.getElementById( "tabData" + i ).style.display = "none";
				document.getElementById( "tabName" + i ).className = "wpsPortletTab";
				document.getElementById( "tabLink" + i ).className = "font10pt white nound";
			}
		}
	}

	if ((typeof s.pageName != "undefined") && tabPageName == null) {
		tabPageName = s.pageName;
	}
	if (tabPageName != null) {
		if (which > 1) {
			s.pageName = tabPageName + " -tab" + which;
		} else {
			s.pageName = tabPageName;
		}
		s.prop31 = s.pageName;
		s.linkTrackVars="prop31";
//		alert("sending omniture custom link");
		s.tl(document.getElementById("tabName" + which),'o',s.prop31);
//		s.tl(true,'o',s.prop31);
//		var s_code=s.t();
//		if(s_code)document.write(s_code);
	}
}

/*Function for creating Locale cookie*/
function createCookie(name, value) { 	
	var date = new Date(); 
	date.setTime(date.getTime()+(365*24*60*60*1000)); 
	var expires = "; expires="+date.toGMTString();	
	document.cookie = name+"="+value+expires+";path=/;";	
}

function createSessionCookie(name, value) { 	
	document.cookie = name+"="+value+";path=/;";	
}

function deleteCookie (cookieName)
{
  var cookieDate = new Date ();  // current date & time
  cookieDate.setTime ( cookieDate.getTime() - 1 );
  document.cookie = cookieName += "=; expires=" + cookieDate.toGMTString();
}


function submitForm(alpha)
{
  document.getElementById("alpha").value=alpha;
  document.getElementById("alpha_tab").submit();
}

function clearForm()
{
	if (!document.getElementsByTagName) return false;

	var all_inputs = document.getElementById('ecmContent').getElementsByTagName('input');
	for (var i=0; i < all_inputs.length; i++)
	{
		var current_input = all_inputs[i];

		if (current_input.getAttribute('type') == 'text')
		{
			current_input.value = '';
		}
		else if (current_input.getAttribute('type') == 'checkbox' || current_input.getAttribute('type') == 'radio')
		{
			current_input.checked = ""; 
		}
	}

	var all_textareas = document.getElementById('ecmContent').getElementsByTagName('textarea');
	for (var i=0; i < all_textareas.length; i++)
	{
		var current_textarea = all_textareas[i];
		current_textarea.value = '';
	}

	var all_selects = document.getElementById('ecmContent').getElementsByTagName('select');
	for (var i=0; i < all_selects.length; i++)
	{
		var current_select = all_selects[i];
		current_select.selectedIndex = 0;
	}
}

function clearFormById(formId)
{
	if (!document.getElementsByTagName) return false;

	var all_inputs = document.getElementById(formId).getElementsByTagName('input');
	
	for (var i=0; i < all_inputs.length; i++)
	{
		var current_input = all_inputs[i];

		if (current_input.getAttribute('type') == 'text')
		{
			current_input.value = '';
		}
		else if (current_input.getAttribute('type') == 'checkbox' || current_input.getAttribute('type') == 'radio')
		{
			current_input.checked = ""; 
		}
	}

	var all_textareas = document.getElementById(formId).getElementsByTagName('textarea');
	for (var i=0; i < all_textareas.length; i++)
	{
		var current_textarea = all_textareas[i];
		current_textarea.value = '';
	}

	var all_selects = document.getElementById(formId).getElementsByTagName('select');
	for (var i=0; i < all_selects.length; i++)
	{
		var current_select = all_selects[i];
		current_select.selectedIndex = 0;
	}
}


function openWin(url) {
	newwindow = window.open(url,'name','height=500,width=500,scrollbars=1,resizable=1');
	newwindow.focus();
}


