/**
 * Permet d'ajouter un item au panier (appel XHR)
*/
function addItem(typeP, typeF, codeF, langueF)
{
	//alert('type = ' + typeP + ', objet = ' + typeF + ', code = ' + codeF + ', langue = '+langueF);
	var url = '/servlet/com.kosmos.panier.processus.TraitementPanier';
  
	var myAjax = new Ajax.Request(url,
		{
			method: 'post',
			asynchronous: true,
			parameters: {
				ACTION: 'AJOUTER',
				TYPE_PANIER: typeP,
				TYPE_FICHE: typeF,
				CODE_FICHE: codeF,
				LANGUE_FICHE: langueF },
			onUninitialized: function (xhr)
			{ // Cr�ation de l'objet XHR
				//alert('onUninitialized');
			},
			onLoading: function (xhr)
			{ // Apr�s appel m�thode open
				//alert('onLoading');
			},
			onLoaded: function (xhr)
			{ // Requ�te envoy�e
				//alert('onLoaded');
			},
			onInteractive: function (xhr)
			{ // R�ponse en cours de r�ception
				//alert('onInteractive');
			},
			on200: function (xhr)
			{ // R�ponse HTTP "OK"
				//alert('on200');
			},
			onSuccess: function (xhr)
			{ // R�ponse HTTP == 2xx
				//alert('onSuccess');
			},
			on404: function (xhr)
			{ // R�ponse HTTP "OK"
				//alert('on404');
			},
			onFailure: function (xhr)
			{ // R�ponse HTTP != 2xx
				//alert('onFailure');
			},
			onException: function (xhr, exception)
			{
				alert(exception);
			},
			onComplete: function (xhr)
			{ // Requ�te totalement termin�e
				if (xhr.status == 200)
				{
					eval(xhr.responseText);
				} else {
					//alert(xhr.status);
				}
			}
		}
	)
}

/**
 * Permet de supprimer un item du panier (appel XHR)
 */
function deleteItem(typeP, idFP)
{
	var url = '/servlet/com.kosmos.panier.processus.TraitementPanier';
	var myAjax = new Ajax.Request(url,
		{
			method: 'post',
			asynchronous: true,
			parameters: {
				ACTION: 'SUPPRIMER',
				TYPE_PANIER: typeP,
				ID_FICHEPANIER: idFP},
			onUninitialized: function (xhr)
			{ // Cr�ation de l'objet XHR
				//alert('onUninitialized');
			},
			onLoading: function (xhr)
			{ // Apr�s appel m�thode open
				//alert('onLoading');
			},
			onLoaded: function (xhr)
			{ // Requ�te envoy�e
				//alert('onLoaded');
			},
			onInteractive: function (xhr)
			{ // R�ponse en cours de r�ception
				//alert('onInteractive');
			},
			on200: function (xhr)
			{ // R�ponse HTTP "OK"
				//alert('on200');
			},
			onSuccess: function (xhr)
			{ // R�ponse HTTP == 2xx
				//alert('onSuccess');
			},
			on404: function (xhr)
			{ // R�ponse HTTP "OK"
				//alert('on404');
			},
			onFailure: function (xhr)
			{ // R�ponse HTTP != 2xx
				//alert('onFailure');
			},
			onException: function (xhr, exception)
			{
				alert(exception);
			},
			onComplete: function (xhr)
			{ // Requ�te totalement termin�e
				if (xhr.status == 200)
				{
					eval(xhr.responseText);
				} else {
					//alert(xhr.status);
				}
			}
		}
	)
}

/**
 * Ajouter un item � la liste
 */
function updateAjouter(typePanier, idFichePanier, titreFichePanier, urlFiche, nbItems, isQuantified)
{
	// on supprime les erreurs existantes
	cleanErreur(typePanier);
	 /*
	// on r�cup�re la liste
	 var ul = document.getElementById('liste_fiches_panier_'+typePanier);
	
	// Si la liste �tait vide on supprime le msg 'Aucune fiche dans le panier'
	if (ul.getElementsByTagName('li').length == 0)
	{
		displayMsgListeVide(typePanier, false);
	}

	
	// on construit le nouveau li et on l'ajoute � la liste
	var li = Builder.node('li', {id:idFichePanier}, '');	
	var a = Builder.node('a', {href:''}, titreFichePanier);
	var img = Builder.node('img', {src:'/kosmos/panier/styles/img/suppression.png', class:'action_panier', onclick:"deleteItem( '"+typePanier+"' , '"+ idFichePanier +"')"} );
	li.appendChild(a);
	if (isQuantified)
	{
		var span_qte = Builder.node('span', {id:idFichePanier+'_qte'}, '(1)');
		li.appendChild(span_qte);
	}
	li.appendChild(img);
	ul.appendChild(li);

	new Effect.Highlight(idFichePanier, {endcolor:'#EEEEEE'});
	
	*/

	// on met � jour la taille du panier
	updateTaillePanier(typePanier, nbItems);
}

/**
 * Mettre � jour la quantit� pour un item existant (augmenter ou diminuer)
 */
function updateQuantite(typePanier, idFichePanier, quantite, nbItems)
{
	/********* VUE ENCADRE */

	// on supprime les erreurs existantes
	cleanErreur(typePanier);

	// on met � jour la taille du panier
	updateTaillePanier(typePanier, nbItems);

	// on met a jour la quantit� dans l'encadr�
	var spanQte = document.getElementById(idFichePanier+'_qte');
	if (spanQte)
	{
		spanQte.innerHTML = ' ('+ quantite +')';	
	}

	/********* VUE DETAIL */
	/*
	// on met a jour la quantit� dans la vue d�taill�e
	var spanQteDetail = document.getElementById(idFichePanier+'_qte_detail');
	if (spanQteDetail)
	{
		spanQteDetail.innerHTML = quantite;
	}*/
}

/**
 * Supprimer un item de la liste
 */
function updateSupprimer(typePanier, idFichePanier, nbItems)
{
	/********* VUE ENCADRE */

	// on supprime les erreurs existantes
	cleanErreur(typePanier);

	// on r�cup�re la liste
	var ul = document.getElementById('liste_fiches_panier_'+typePanier);
	if (ul)
	{
		// on supprime la fiche de la liste
		var li = document.getElementById(idFichePanier);
		if (li)
		{
			ul.removeChild(li);
		}
		// si on a supprim� le dernier �l�ment on r�affiche le message comme quoi le panier est vide
		if (ul.getElementsByTagName('li').length == 0)
		{
			displayMsgListeVide(typePanier, true);
		}
	}

	// on met � jour la taille du panier
	updateTaillePanier(typePanier, nbItems);

	/********* VUE DETAIL */

	// on r�cup�re la liste
	var ulDetail = document.getElementById('liste_detail_panier_'+typePanier);
	if (ulDetail)
	{
		// on supprime la fiche de la liste
		var liDetail = document.getElementById(idFichePanier+'detail');
		if (liDetail)
		{
			ulDetail.removeChild(liDetail);
		}
		// si on a supprim� le dernier �l�ment on r�affiche le message comme quoi le panier est vide
		if (ulDetail.getElementsByTagName('li').length == 0)
		{
			var pDetail = document.getElementById('liste_vide_detail_'+typePanier);
			if (pDetail)
			{
				pDetail.style.display = 'block';
			}
		}
	}
}

/**
 * Met � jour la taille du panier
 */
function updateTaillePanier(typePanier, nbItems)
{
	var span = document.getElementById('taille_panier_'+typePanier);
	if (span)
	{
		span.innerHTML = '('+ nbItems +')';
	}
}

/**
 * Affiche ou masque le message "Liste vide"
 */
function displayMsgListeVide(typePanier, bDisplay)
{
	var p = document.getElementById('liste_vide_'+typePanier);
	if (p)
	{
		p.style.display = (bDisplay ? 'block' : 'none');
	}
}

/**
 * G�rer les messages d'erreur
 */
function updateErreur(codeErreur, msgErreur, typePanier)
{
	// On r�cup�re la div
	var div = document.getElementById('panier_'+typePanier);
	if (div)
	{
		// on supprime les erreurs existantes
		cleanErreur(typePanier);

		// on construit le nouveau p et on l'ajoute � la div
		var p = Builder.node('p', {id:'erreur_panier_'+typePanier, style:'color:red;'}, msgErreur);
		div.appendChild(p);
	}
}

/**
 * Si il y a un message d'erreur on le supprime
 * (par d�faut un <p id="erreur_panier_"+typePanier> dans un <div id='panier_'+typePanier>)
 * ATTENTION : on ne supprime que le message d'erreur de son type de panier
 * TODO : voir comment am�liorer ca
 */
function cleanErreur(typePanier)
{
	var div = document.getElementById('panier_'+typePanier);
	var p = document.getElementById('erreur_panier_'+typePanier);
	if (div && p)
	{
		div.removeChild(p);
	}
}
