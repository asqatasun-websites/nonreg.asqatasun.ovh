// JavaScript Document

var actualCalendar;
var actualCalendarTimeOutId;

//this function initializes a double calendar



function initDoubleCalendier(idImage, idDiv, idCalendar, idDate,  nbrMonthsToShow,calendarXYPosition, executeAfterSelectDate, dayMask, monthYearMask) {
  
  calendar = new YAHOO.widget.CalendarGroup(idCalendar,idDiv,{pages:2} ); //close:true
  //calendar = new YAHOO.widget.Calendar(idCalendar,idDiv);  //Just in case you want to test a single calendar ;)

  // Calendier basic properties. Due a bug in this release, the "pages" property MUST be the first property !!!!!!
  // Due a bug, if we specify the number of pages, after we cannot use the "selected" property. So, we don't use the "pages" property :<
  //calendar.cfg.setProperty("pages",2);  //two page calendar.
  //we use the title as the legend:
  calendar.cfg.setProperty("title","<img src='/img_opodo/calendar/vacances.gif' alt='Vacances scolaires' align='bottom' /> Vacances scolaires <span><img src='/img_opodo/calendar/feries.gif' alt='Jours f&eacute;ri&eacute;s' align='bottom' /> Jours f&eacute;ri&eacute;s </span>");
  //calendar.cfg.setProperty("close",true); //we can close the calendar
  calendar.cfg.setProperty("LOCALE_WEEKDAYS", "medium");
  calendar.cfg.setProperty("START_WEEKDAY", 1); //the first day is monday
  calendar.cfg.setProperty("iframe", true);

  //French dates:
  calendar.cfg.setProperty("MONTHS_SHORT",   ["Jan", "F\u00e9v", "Mar", "Avr", "Mai", "Jui", "Jul", "Ao\u00fb", "Sep", "Oct", "Nov", "D\u00e9c"]);
  calendar.cfg.setProperty("MONTHS_LONG",    ["Janvier", "F\u00e9vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao\u00fbt", "Septembre", "Octobre", "Novembre", "D\u00e9cembre"]);
  calendar.cfg.setProperty("WEEKDAYS_1CHAR", ["D", "L", "M", "M", "J", "V", "S"]);
  calendar.cfg.setProperty("WEEKDAYS_SHORT", ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"]);
  calendar.cfg.setProperty("WEEKDAYS_MEDIUM",["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"]);
  calendar.cfg.setProperty("WEEKDAYS_LONG",  ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"]);

  /* NOTE : USE SOMETHING LIKE THAT PENALIZES A LOT THE PERFORMANCE OF THE CALENDAR.
            INSTED, WE CREATE A CLASS FOR EVERY HOLIDAY !!!!!!
  
  //Vacances d'hiver 2007
  calendar.addRenderer("2/10/2007-3/12/2007", calendar.renderCellStyleHighlight2);

  //Important days:
  calendar.addRenderer("1/1", calendar.renderCellStyleHighlight3);

  //Weekends in different colour. It's a BIT better to use the specific classes: w0 and w6
  calendar.addWeekdayRenderer(7, calendar.renderCellStyleHighlight1);  //saturdays
  calendar.addWeekdayRenderer(1, calendar.renderCellStyleHighlight1);  //sundays
  */

  //First selectable day in Calendar:
  var now = new Date();
  var month = now.getMonth() + 1;
  var day = now.getDate();
  var year = now.getFullYear();
  var stringNow =  month + "/" + day + "/" + year;
  calendar.cfg.setProperty("mindate", stringNow);

  // we cross out all the dates from the current month thar are smaller than "today"
  var stringFirstDay =  month + "/01/" + year;
  calendar.addRenderer(stringFirstDay+"-"+stringNow, calendar.renderBodyCellRestricted);

  //Last selectable day in Calendar:
  var lastDate = new Date();
  lastDate.setMonth(lastDate.getMonth()+nbrMonthsToShow);
  var lastMonth = lastDate.getMonth()+1;
  var lastDay = lastDate.getDate();
  var lastYear = lastDate.getFullYear();
  var stringLastDate = lastMonth + "/" + lastDay + "/" + lastYear;
  
  calendar.cfg.setProperty("maxdate", stringLastDate);  
  
  //calendar.render(); //we will render the calendar when it's opened :) Otherwise the performance is really bad!!!

  // tmp variable to pass the 3 id where the result will be "writen" + the extra parameters to place the calendar in the right position
  var selectIds= [idDate, idImage, idDiv, calendarXYPosition,executeAfterSelectDate, dayMask, monthYearMask]
 //var selectIds= [idDay, idMonthYear, idWeekDay, idImage, idDiv, calendarXYPosition, executeAfterSelectDate, executeAfterSelectMonth, dayMask, monthYearMask]

  // Listener to show the calendar when idImage is clicked
  YAHOO.util.Event.addListener(idImage, "click",openCalendar, {cal: calendar, selIds: selectIds } );
  YAHOO.util.Event.addListener(idImage, "focus",openCalendar, {cal: calendar, selIds: selectIds } );

  // we pass 2 objects to the callback funtion. The first one is the calendar (because we need to close it) and the second
  // object is an array with the select's id names where the calendar will write the selected day.
  calendar.selectEvent.subscribe(handleSelect, {cal: calendar, selIds: selectIds } );
  
  //the calendar will be closed when the cursor is not over the image or thde calendar div.
  YAHOO.util.Event.addListener(idImage, "mouseout", closeCalendarByTimeOut, {cal: calendar });
  YAHOO.util.Event.addListener(idImage, "mouseover", cancelCalendarTimeOut, {cal: calendar });
  YAHOO.util.Event.addListener(idDiv, "mouseout", closeCalendarByTimeOut, {cal: calendar });
  YAHOO.util.Event.addListener(idDiv, "mouseover", cancelCalendarTimeOut, {cal: calendar });

}


function cancelCalendarTimeOut(type, obj){
	
	//just in case htere is more than one calendar in the same page and you go from one calendar image to onother one.
	if (actualCalendar != null && actualCalendar != obj.cal )
		actualCalendar.hide();
		
	clearTimeout(actualCalendarTimeOutId);
}


function closeCalendarByTimeOut(type, obj){
	actualCalendar = obj.cal;
	actualCalendarTimeOutId = setTimeout('actualCalendar.hide();',2000);
}


function openCalendar(type, obj){
  
  // we show in the calendar the selected day from the select list.
  var selectDateId = obj.selIds[0];
  //var selectMonthYearId = obj.selIds[1];
  
  var idImage = obj.selIds[1];
  var idDiv = obj.selIds[2]; 
  var calendarXYPosition = obj.selIds[3];

  var dateSelected = YAHOO.util.Dom.get(selectDateId).value;
    
   
  // verifier format date
   var datesSplit;
  var formatdateOk = validRegexDate(dateSelected)
 
	var valideOrDate;
	if(!validRegexDate(dateSelected) || !isDateValid(dateSelected)){
    var today = new Date();
	  dateSelected = today.getDate()+"/"+(today.getMonth()+1)+"/"+today.getFullYear();
  }
 
 
	datesSplit = dateSelected.split('/');
  
   
	var todayDate= new Date ();
	
	obj.cal.setMonth(todayDate.getMonth());
	obj.cal.setYear(todayDate.getFullYear()); 
  
	//obj.cal.setMonth(datesSplit[1]-1);
	//obj.cal.setYear(datesSplit[2]);
  
	dateSelected=datesSplit[1]+"/"+datesSplit[0]+"/"+datesSplit[2];
  
	//we mark the rigth day:
	obj.cal.cfg.setProperty("selected", dateSelected);    
  
  //we define where the calendar will be placed:
	changeCalendarPosition(selectDateId, idDiv, calendarXYPosition);

  //Here is where we render the calendar!!!!
  
	obj.cal.render();
	obj.cal.show();
}

// That manage the select:
// VERY IMPORTANT:
// The format of the values from the select must be: "DD" for the day select and "MMM YYYY" for the month select
function handleSelect(type,args,obj){
  
    
  var selectDayId = obj.selIds[0];
  var selectDateId = obj.selIds[0];
  var dates = args[0];
  var date = dates[0];
	
  var year = date[0];
  var month = date[1];
  var day = date[2];
	
	
  //format dd/mm/yyyy?
  
  
  
  if(month.toString().length==1)
        month = "0"+month;
        
  if(day.toString().length==1)
         day= "0"+day;
  
    
  var	 txtDate = day + "/"+ month + "/" + year;
  
  YAHOO.util.Dom.get(selectDateId).value = txtDate;
  
  obj.cal.hide();
  
  //We execute the onchange form the select. Usually will refrech the other date pair.
 
  var executeAfterSelectDate = ""+obj.selIds[4];
  var dummy = setTimeout(executeAfterSelectDate,10); 
  
}


function changeCalendarPosition(idReference, idDiv, calendarXYPosition){	
  //we will place the calendar close to the image:  
  var imagePosition = getPosition( YAHOO.util.Dom.get(idReference) );
  
  document.getElementById(idDiv).style.position = 'absolute';
  document.getElementById(idDiv).style.top = imagePosition.y+25+"px"; //we have to add "px" for Firefox!!!!!
  document.getElementById(idDiv).style.left = imagePosition.x+5+"px"; 

  if ("LEFT_UP" == calendarXYPosition){
  	document.getElementById(idDiv).style.top = imagePosition.y-240+"px";
		document.getElementById(idDiv).style.left = imagePosition.x-150+"px";
	}
	else if ("LEFT" == calendarXYPosition){
  	document.getElementById(idDiv).style.left = imagePosition.x-150+"px";
  }
  else if (typeof(calendarXYPosition) != String && calendarXYPosition != "" ){
	  if(calendarXYPosition.xPosition != 0 )
	  	document.getElementById(idDiv).style.left = imagePosition.x + calendarXYPosition.xPosition + "px";
	  
	  if(calendarXYPosition.yPosition != 0 )
	  	document.getElementById(idDiv).style.top = imagePosition.y + calendarXYPosition.yPosition + "px";
	}
  
  /* NOTE: this version of you (0.12) is buggy; CalendarGroup since end up
           attaching the iframe shim to the individual Calendars and not to the
           CalendarGroup container.
  */

  
}

function getPosition(e){
	var left = 0;
	var top  = 0;

	while (e.offsetParent){
		left += e.offsetLeft;
		top  += e.offsetTop;
		e     = e.offsetParent;
	}

	left += e.offsetLeft;
	top  += e.offsetTop;

	return {x:left, y:top};
}

function calendarPosition(xPosition, yPosition){
	this.xPosition = xPosition;
	this.yPosition = yPosition;
}
