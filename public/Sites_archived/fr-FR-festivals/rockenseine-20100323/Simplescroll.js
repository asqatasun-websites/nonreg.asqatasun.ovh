/////////////////////////////////////////////////////////////////////////////////
// class Simplescroll v0.1 - GC
/////////////////////////////////////////////////////////////////////////////////

function Simplescroll(divId,w,h,s){
	this.div 	= document.getElementById(divId);
	this.w		= w;
	this.h		= h;
	this.Y		= 0;
	this.speed  = s;
	this.int;
	
	this.div.innerHTML = "<div id='"+divId+"'><div id='"+divId+"_content'>"+this.div.innerHTML+"</div></div>";
	this.divContent = document.getElementById(divId+"_content");
	
	
	this.div.style.position =  "relative"; 
	this.div.style.height	= h + "px";
	this.div.style.overflow	= "hidden";
	//this.div.style.background = "#DDDDDD";
		
	this.divContent.style.position = "absolute";
	//this.divContent.style.background = "#eeeeee";
	
	this.minY = this.h - this.divContent.offsetHeight;
	
	
	this.Up = function(){
		var localThis = this;
		this.int = setInterval(function(){localThis.move("up")},10);
	}
	
	this.Down = function(){
		var localThis = this;
		this.int = setInterval(function(){localThis.move("down")},10);
	}
	
	this.move = function(type){
		switch(type){
			case "down" : 	if(this.Y < 0){
								this.Y += this.speed;
								this.ScrollY(this.Y);
							}
			break;
			case "up" : 	if(this.Y > this.minY){
								this.Y -= this.speed;
								this.ScrollY(this.Y);
							}
			break;
		}
	}
	
	
	this.ScrollY = function(y){			
			this.Crop(-y,this.w,this.h-y,0);
			this.divContent.style.top = y + "px";
	}
	
	
	this.Crop = function(t,r,b,l){
			//alert(t + " : " + r + " : " + b + " : " + l);
			if (document.layers){
				this.divContent.style.clip.top 		= t + "px";			
				this.divContent.style.clip.right 	= r + "px";
				this.divContent.style.clip.bottom 	= b + "px";
				this.divContent.style.clip.left 	= l + "px";
			}else{
				
				this.divContent.style.clip 			= 'rect(' + t + 'px,' + r + 'px,' + b + 'px,'+ l +'px)';
			}
			
	}
	
	this.Stop = function(){
		clearInterval(this.int);
	}
	
	this.ScrollY(0);
	
}