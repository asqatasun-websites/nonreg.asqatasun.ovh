﻿function Accueil_Fin()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "logoCA"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",200,"BOTTOM", 150, "PlaceAt");
ACTOR.Show();
ACTOR.Play("GestureUser");
ACTOR.Speak("N'hésitez pas à faire[Play(Argue_01)] appel à moi. A bientôt ! ");
ACTOR.Hide();
}

function Accueil_01()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "logoCA"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",200,"BOTTOM", 150, "PlaceAt");
ACTOR.Show();
ACTOR.Play("Explain_02");
ACTOR.Speak("Bonjour ! Je suis Cathy. Je vais [Play(GestureUser)]vous aider à trouver l'information dont vous avez besoin.");
ACTOR.SetBookmark("Accueil_02");
}
function Accueil_02()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "logoCA"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",200,"BOTTOM", 150, "PlaceAt");
ACTOR.Show();
ACTOR.Play("Think");
ACTOR.Speak("Souhaitez-vous [Play(Speak_01)]<a href='asfunction:OnDialogID,Produits'><u><font color='#5555FF'>trouver le produit qu'il vous faut</font></u></a>, [Play(Speak_02)]<a href='asfunction:OnDialogID,Demarches'><u><font color='#5555FF'>être aidé dans vos démarches</font></u></a> ? [Play(Speak_03)]ou effectuer vos <a href='asfunction:OnDialogID,EnLigne'><u><font color='#5555FF'>demandes en ligne</font></u></a>.");
}

function Produits()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "logoCA"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",200,"BOTTOM", 150, "PlaceAt");
ACTOR.Show();
ACTOR.Play("GestureUser");
ACTOR.Speak("Vous cherchez <a href='asfunction:OnDialogID,Moments'><u><font color='#5555FF'>[Play(Argue_01)]un produit qui colle à votre situation ou à un projet</font></u></a>, [Play(Argue_02)]vous cherchez <a href='asfunction:OnDialogID,Comparateurs'><u><font color='#5555FF'>nos comparateurs, nos simulateurs</font></u></a>[Play(Explain_01)] ou <a href='asfunction:OnDialogID,Tarifs'><u><font color='#5555FF'>nos tarifs</font></u></a> ?");
}

function Demarches()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "logoCA"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",200,"BOTTOM", 150, "PlaceAt");
ACTOR.Show();
ACTOR.Play("GestureUser");
ACTOR.Speak("Souhaitez-vous [Play(Argue_01)]<a href='asfunction:OnDialogID,OuvrirCompte'><u><font color='#5555FF'>ouvrir un compte</font></u></a>, [Play(Argue_02)]<a href='asfunction:OnDialogID,Contacter'><u><font color='#5555FF'>contacter un conseiller</font></u></a>, déclarer le [Play(Explain_02)]<a href='asfunction:OnDialogID,Perte_Vol'><u><font color='#5555FF'>vol/perte d'une carte</font></u></a> ou <a href='asfunction:OnDialogID,Sinistres_Accueil'><u><font color='#5555FF'>déclarer [Play(Explain_01)]un sinistre</font></u></a> ?");
}

function EnLigne()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "logoCA"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",200,"BOTTOM", 150, "PlaceAt");;
ACTOR.Show();
ACTOR.Play("GestureUser");
ACTOR.Speak("Cherchez-vous un[Play(Explain_01)] <a href='asfunction:OnDialogID,Econso'><u><font color='#5555FF'>crédit consommation</font></u></a>, [Play(Explain_02)]un <a href='asfunction:OnDialogID,Eimmo'><u><font color='#5555FF'>prêt immobilier</font></u></a> ou vous voulez <a href='asfunction:OnDialogID,Securite'><u><font color='#5555FF'>[Play(Argue_02)]suivre vos comptes\nen direct</font></u></a> ?");
}

function Moments()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "acces_compte_dept"
ACTOR.AlignToHTMLObject("acces_compte_dept","MIDDLE",0,"BOTTOM",350, "PlaceAt");
ACTOR.Show();
ACTOR.Play("Explain_02");
ACTOR.Speak("En couple ou proche de la retraite, à chacun ses besoins,...");
ACTOR.Speak("[Play(GestureRight)]...parcourez cette ligne de vie [Play(Argue_01)]et choisissez ce qui vous ressemble.");
ACTOR.Play("Explain_01");
ACTOR.Speak("Au revoir !");
ACTOR.Hide();
}

function Comparateurs()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "logoCA"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",200,"BOTTOM", 150, "PlaceAt");
ACTOR.Show();
ACTOR.Play("Think");
ACTOR.Speak("Vous chercher une assurance Santé sans questionnaire médical ?");
ACTOR.Play("Argue_01");
ACTOR.Speak("Une assurance auto 0 km ou une assurance Habitation [Play(Argue_02)]qui récompense 3 années sans accidents ni impayés ?");
ACTOR.Play("GestureUser");
ACTOR.Speak("Vous allez trouver chaussure à votre pied. ");
ACTOR.Play("Suggest");
ACTOR.Speak("Comparez les solutions d'épargne[Play(Explain_02)] en choisissant un projet ou une durée de placement...");
ACTOR.Speak("... [Play(Argue_01)]et enfin trouver la carte bancaire qui [Play(Speak_01)]colle à vos besoins ou déplacements. ");
ACTOR.Play("Explain_01");
ACTOR.Speak("A très vite !");
ACTOR.Hide();
}

function Tarifs()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "logoCA"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",200,"BOTTOM", 150, "PlaceAt");
ACTOR.Show();
ACTOR.Play("Suggest");
ACTOR.Speak("Les tarifs sont disponibles sur le site de votre caisse,...");
ACTOR.Speak("[Play(Explain_01)]... j'ouvre pour vous la carte de France pour choisir votre caisse.");
ACTOR.Play("Argue_02");
ACTOR.Speak("A très vite !");
ACTOR.Hide();
ACTOR.SetBookmark("CarteFrance");
}

function OuvrirCompte()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "logoCA"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",200,"BOTTOM", 150, "PlaceAt");
ACTOR.Show();
ACTOR.Play("Explain_02");
ACTOR.Speak("Ouvrir un compte au Crédit Agricole, c'est disposer [Play(Speak_01)]à la fois d'une caisse de proximité et d'une[Play(Explain_01)] banque à distance...");
ACTOR.Speak("... accessible par un serveur vocal interactif, [Play(Argue_01)]Internet, la télévision interactive et le téléphone mobile.");
ACTOR.Play("GestureUser");
ACTOR.Speak("<a href='asfunction:OnDialogID,Trouver_Agence'><u><font color='#5555FF'>Trouvez une caisse proche de chez vous </font></u></a> [Play(Suggest)]et remplissez le formulaire de rendez-vous. [Play(Argue_01)]Voir <a href='asfunction:OnDialogID,Pieces'><u><font color='#5555FF'>la liste des pièces</font></u></a> à fournir");
}

function Perte_Vol()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "faq_sos"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",200,"BOTTOM", 150, "PlaceAt");
ACTOR.Show();
ACTOR.Play("Explain_02");
ACTOR.Speak("Pour faire opposition, [Play(GestureUser)]munissez-vous de votre numéro de carte [Play(Speak_02)]et de la date d'expiration,...");
ACTOR.Play("Explain_01");
ACTOR.Speak("... afin de les donnez au centre national de sécurité, [Play(Speak_01)]joignable 24h/24.");
ACTOR.Play("Suggest");
ACTOR.Speak("Dans tous les cas, confirmez l'opposition [Play(Speak_01)]par lettre recommandée avec AR au Crédit Agricole.");
ACTOR.Play("Explain_01");
ACTOR.Speak("A très vite !");
ACTOR.Hide();
ACTOR.SetBookmark("Url_perte_vol");
}

function Sinistres_Accueil()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "logoCA"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",200,"BOTTOM", 150, "PlaceAt");
ACTOR.Show();
ACTOR.Play("Suggest");
ACTOR.Speak("Ne vous affolez pas, [Play(Speak_01)]il s'agit d'agir vite.");
ACTOR.Speak("Est-ce à propos d'un [Play(Think)]<a href='asfunction:OnDialogID,Sinistre'><u><font color='#5555FF'>sinistre auto ou habitation</font></u></a>,[Play(Speak_01)]de <a href='asfunction:OnDialogID,Assistance'><u><font color='#5555FF'>l'assistance en France ou à l'étranger </font></u></a> ou [Play(Argue_01)]<a href='asfunction:OnDialogID,Litige'><u><font color='#5555FF'>d'un litige</font></u></a> ?");
}

function Securite()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "logoCA"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",200,"BOTTOM", 150, "PlaceAt");
ACTOR.Show();
ACTOR.Play("Speak_02");
ACTOR.Speak("Pour naviguer en toute sécurité, [Play(GestureUser)]vérifier que votre antivirus est à jour et votre pare-feu [Play(Speak_01)]est bien configuré.");
ACTOR.Play("Explain_01");
ACTOR.Speak("A bientôt !");
ACTOR.Hide();
ACTOR.SetBookmark("Securite");
}

function Econso()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "permis_offre"
ACTOR.AlignToHTMLObject("permis","RIGHT",50,"MIDDLE", 0, "PlaceAt");
ACTOR.Show();
ACTOR.Play("Think");
ACTOR.Speak("Voiture, projet, travaux ?");
ACTOR.Play("GestureUser");
ACTOR.Speak("Choisissez la solution de crédit qui s'adapte à votre besoin.");
ACTOR.Play("Speak_02");
ACTOR.Speak("Ensuite, pour une demande en ligne ou pour en savoir plus,...");
ACTOR.Speak("[Play(GestureUser)]... indiquez votre code postal afin d'accéder à l'offre relative à votre caisse régionale.");
//Placement par rapport à un identifiant de la page qui s'appelle "logoCA"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",200,"BOTTOM", 150, "PlaceAt");
ACTOR.Play("Speak_01");
ACTOR.Speak("Financiez votre achat [Play(Explain_02)]comme vous le sentez en choisissant de moduler vos mensualités...");
ACTOR.Speak("...[Play(Suggest)] ou d'allonger la durée de vos remboursements. ");
ACTOR.Play("Speak_01");
ACTOR.Speak("Je [Play(Explain_01)]vous laisse. Au revoir ! ");
ACTOR.Hide();
}

function Pieces()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "logoCA"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",200,"BOTTOM", 280, "PlaceAt");
ACTOR.Show();
ACTOR.Play("GestureRight");
ACTOR.Speak("Voilà la liste des pièces à prévoir pour ouvrir un[Play(GestureUser)] compte. Ensuite, <a href='asfunction:OnDialogID,Trouver_Agence'><u><font color='#5555FF'>trouvez votre agence</font></u></a>.");
}

function Sinistre()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "faq_assurances"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",-180,"BOTTOM", 420, "PlaceAt");
ACTOR.Show();
ACTOR.Play("Speak_02");
ACTOR.Speak("Pour déclarer un sinistre[Play(GestureLeft)], appelez la plateforme téléphonique. ");
ACTOR.Play("Speak_01");
//ACTOR.Speak("Après avoir déclaré le sinistre par téléphone,[Play(Explain_02)] doublez votre déclaration d'un courrier[Play(Suggest)], si possible sous 2 jours.");
//ACTOR.Play("GestureUser");
ACTOR.Speak("Plus vous nous contactez rapidement, [Play(Explain_01)]plus nous serons à même de vous aider et de vous assister. ");
ACTOR.Play("Argue_01");
ACTOR.Speak("Au revoir ! ");
ACTOR.Hide();
ACTOR.SetBookmark("Url_sinistre");
}

function Litige()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "faq_assurances"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",-180,"MIDDLE", 450, "PlaceAt");
ACTOR.Show();
ACTOR.Play("GestureUser");
ACTOR.Speak("Vous êtes souscripteur d'un contrat Pleins [Play(Argue_01)] droits et vous rencontrez un litige ? ");
ACTOR.Play("Suggest");
ACTOR.Speak("Avant d'entreprendre des démarches personnelles[Play(Explain_01)] prenez conseil auprès de [Play(LookLeft)]votre conseiller juridique à ce numéro. ");
ACTOR.Play("Explain_01");
ACTOR.Speak("Je vous retrouve plus tard !");
ACTOR.Hide();
ACTOR.SetBookmark("Url_litige");
}

function Assistance()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "logoCA"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",250,"BOTTOM", 150, "PlaceAt");
ACTOR.Show();
ACTOR.Play("Think");
ACTOR.Speak("Si vous avez perdu vos papiers au cours d'un voyage,...");
ACTOR.Speak("[Play(Suggest)]... il faut déclarer la perte/vol auprès des autorités locales,...");
ACTOR.Speak("[Play(Speak_01)]... puis demander au consulat un document d'identité.");
ACTOR.Play("Greet");
ACTOR.Speak("S'il s'agit de la perte ou du vol de vos moyens de paiement, [Play(GestureUser)]téléphonez impérativement en France pour faire opposition.");
ACTOR.Play("Explain_02");
ACTOR.Speak("N'ayez crainte, [Play(Speak_03)]le Crédit Agricole est avec vous.[Play(Explain_01)]\nA bientôt ! ");
ACTOR.Hide();
ACTOR.SetBookmark("Url_assistance");
}

function Sinistres_Appel()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "logoCA"
ACTOR.AlignToHTMLObject("logoCA","RIGHT",250,"BOTTOM", 150, "PlaceAt");
ACTOR.Show();
ACTOR.Play("Suggest");
ACTOR.Speak("Pas de panique !");
ACTOR.Play("Think");
ACTOR.Speak("Est-ce à propos d'un [Play(Speak_01)]<a href='asfunction:OnDialogID,Perte_Vol'><u><font color='#5555FF'>vol ou perte d'une carte</font></u></a> ?[Play(Speak_02)] D'un <a href='asfunction:OnDialogID,Sinistre'><u><font color='#5555FF'>sinistre auto ou habitation</font></u></a>,[Play(Speak_03)] de <a href='asfunction:OnDialogID,Assistance'><u><font color='#5555FF'>l'assistance en France ou à l'étranger</font></u></a>[Play(Explain_02)] ou <a href='asfunction:OnDialogID,Litige'><u><font color='#5555FF'>d'un litige</font></u></a> ? ");
}

function Eimmo()
{
ACTOR.Stop();
//Placement par rapport à un identifiant de la page qui s'appelle "code_dept"
ACTOR.AlignToHTMLObject("code_dept","LEFT",100,"MIDDLE", 0, "PlaceAt");
ACTOR.Show();
ACTOR.Play("Think");
ACTOR.Speak("Vous avez envie de concrétiser un projet immobilier ?");
ACTOR.Play("GestureUser");
ACTOR.Speak("Vous êtes au bon endroit.");
ACTOR.Play("Speak_02");
ACTOR.Speak("Que ce soit pour effectuer votre demande en ligne ou pour en savoir plus,...");
ACTOR.Speak("[Play(GestureLeft)]... indiquez votre code postal afin d'accéder à l'offre relative à votre caisse régionale. ");
ACTOR.Play("Speak_01");
ACTOR.Speak("Les prêts Habitat du Crédit Agricole sont très souples.");
ACTOR.Speak("Vous pouvez [Play(Explain_02)]par exemple augmentez ou diminuez jusqu'à 30% vos mensualités.");
ACTOR.Play("Suggest");
ACTOR.Speak("C'est pas beau ? ");
ACTOR.Play("Speak_01");
ACTOR.Speak("Je [Play(Explain_01)]vous laisse. Au revoir ! ");
ACTOR.Hide();
}

