	var typeUser;

	//Contr�le du champ pattern
	function controle_pattern(value) {
		if (value == "" || value == I18n.message('txt.defaultPattern')) {
			alert(I18n.message('txt.patternRechercheVideOuDefaut'));
			return false;
		} else {
			return true;
		}
	}
	
	//Contr�le du user
	function controle_user() {
		if (typeUser == 0) {
			//visiteur/membre
			alert(I18n.message('txt.actionNonClient'));
			return false;
		} else {
			return true;
		}
	}	
	
	//Contr�le et sousmission du formulaire simple
	function submit_recherche_simple(){
		if (controle_pattern(document.cours_simple_form.pattern.value)) {
			document.cours_simple_form.submit();
		}		
	}
		
	//Contr�le des champs et determination de l'action � effectuer
	function submit_recherche_avance(option) {
		var form = document.cours_avance_form;
		var pattern = form.pattern.value;
		switch (option) {
			case 1 : 
				//Achat
				if (controle_user() && controle_pattern(pattern) )
				{
					form.act.value = "achat";
					form.submit()
				}
				break;
			case 2 : 
				//Vente
				if (controle_user() && controle_pattern(pattern) )
				{
					form.act.value = "vente";
					form.submit()
				}
				break;
			case 3 : 
				//Fiche valeur
				if (controle_pattern(pattern) )
				{
					form.act.value = "cours";
					form.submit()
				}
				break;
			case 4 : 
				//Graphique
				if (controle_pattern(pattern) )
				{
					form.act.value = "graph";
					form.submit()
				}
				break;
			case 5 : 
				//Actualit�
				if (controle_pattern(pattern) )
				{
					form.action=I18n.message('url.listeNews');
					form.submit()
				}
				break;
			case 6 : 
				//Indice
				if (form.indice.value != '') {
					form.action = I18n.message('url.composition');
					form.submit();
				}
				break;
			case 7 : 
				//key press
				if (controle_pattern(pattern) )
				{
					form.act.value = "cours";
					return true;
				} else {
					return false;
				}
				break;
		}
	}

	var indicesByPlace = {
		FRA:[{lib:'CAC 40',val:'XPARFR0003500008'},
			{lib:'SBF 80',val:'XPARFR0003999473'},
			{lib:'SBF 120',val:'XPARFR0003999481'},
			{lib:'SBF 250',val:'XPARFR0003999499'},
			{lib:'SBF INDICE DU MARCHE LIBRE',val:'XPARFR0003999994'},
			{lib:'SBF FCI INDICE DE PRIX',val:'XPARFR0003999572'},
			{lib:'CAC MID100',val:'XPARQS0010989117'},
			{lib:'CAC SMALL',val:'XPARQS0010989125'},
			{lib:'CAC MID & SMALL',val:'XPARQS0010989133'},
			{lib:'CAC NEXT20',val:'XPARQS0010989109'},
			{lib:'CAC IT20',val:'XPARQS0010989091'}],
		USA:[{lib:'DOW JONES INDUSTRIAL',val:'XNYSUS2605661048'},
			{lib:'S&P 500 COMPOSITE STOCK PRICE',val:'XCMEUS78378X1072'},
			{lib:'NASDAQ COMPOSITE',val:'XNASUS9900751036'},
			{lib:'NASDAQ 100',val:'XNASUS6289081050'}],
		EUR:[{lib:'EURONEXT 100',val:'XPARFR0003502079'},
			{lib:'NEXT 150 INDEX',val:'XPARFR0003502087'},
			{lib:'FTSE 100',val:'NCOTGB0001383545'},
			{lib:'DAX 30 PERFORMANCE',val:'XDWZDE0008469008'},
			{lib:'IBEX 35',val:'XMADES0SI0000005'},
			{lib:'SWISS MARKET INDEX (SMI)',val:'NCOTCH0009980894'},
			{lib:'AMSTERDAM EXCHANGES-INDEX',val:'XAMSNL0000000107'},
			{lib:'FTSE EUROFIRST',val:'XAMSNL0000255438'}],
		CAN:[{lib:'S&P / TSX COMPOSITE INDEX',val:'XTSECA0000017016'}],
		ASE:[{lib:'NIKKEI 225 STOCK AVERAGE',val:'XTKSGB0066396796'},
			{lib:'HANG SENG',val:'XHKGHK0000004322'},
			{lib:'STRAITS TIMES INDEX',val:'XSESXC0009653640'},
			{lib:'TWN TAIWAN-WEIGHTED INDEX',val:'XTAIXC0009694149'}]
	};

	//Mise � jour de la liste des indices en fonction du pays
	function updateIndice(pays) {
	
		var indiceSelect = document.cours_avance_form.indice;
		var indices = indicesByPlace[pays];
		
		indiceSelect.options.length = 1;
		indiceSelect.selectedIndex = 0;

		if (indices) {
			indiceSelect.options.length = indices.length + 1;
			for (index=0;index<indices.length;index++) {
				indiceSelect.options[index + 1]= new Option(indices[index].lib,indices[index].val);
	 		}
	 	}
	}
