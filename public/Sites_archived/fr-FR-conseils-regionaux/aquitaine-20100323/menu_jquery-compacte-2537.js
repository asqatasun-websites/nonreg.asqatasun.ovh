/* JavaScriptCompressor 0.8 [www.devpro.it], thanks to Dean Edwards for idea [dean.edwards.name] */
var localMenu={init:function(){localMenu.hideAll();$(
"#menu a"
).click(localMenu.doClick);},hideAll:function (){$(
"#menu > li ul"
).hide();},showNode:function (id){try {var tab=id.split(
"-"
);if (!tab[2]){
$(
"#menu > li ul"
).each(function(){var $this=$(this);if ($this.attr(
"id"
)==id){$(
'#n-'
+tab[1]).show();}
else{$this.hide();}});}
else{
$(
"#menu > li ul"
).each(function(){var $this=$(this);if ($this.attr(
"id"
)==id||$this.attr(
"id"
).slice(0,2)==id.slice(0,2)){$(
'#n-'
+tab[1]).show();if ($this.attr(
"id"
)==id){$(
'#'
+id).show();$this.prev().css(
"fontWeight"
,
"bold"
);}
else{$this.hide();}}
else{$this.hide();}});}}
catch (exc){
}},doClick:function (){var $this=$(this);if ($this.attr(
'href'
).match(
/#$/)){
var $ul=$this.parents(
"li"
).children(
"ul"
);if ($ul){localMenu.showNode($ul.attr(
"id"
));}}}};
var externalMenu={init:function(){
$(
"div.bloc_lien_direct h3"
).click(function(evt){externalMenu.setActive(this);});externalMenu.hideInactives();},setActive:function(obj){var $this=$(obj).parent();if ($this.hasClass(
"bloc_lien_direct_active"
)){return;}
$(
"div.bloc_lien_direct_active"
).removeClass(
"bloc_lien_direct_active"
);$this.addClass(
"bloc_lien_direct_active"
);externalMenu.hideInactives();$this.find(
"ul"
).show();},hideInactives:function(){$(
"div.bloc_lien_direct"
).not(
"div.bloc_lien_direct_active"
).find(
"ul"
).hide();}};var presidentMenu={init:function(){
var current_path=window.location.href;var $ul=$(
"#president_home ul"
);var is_active=(current_path==
'/'
);if (! is_active){$(
"a"
,$ul).each(function(){
if (current_path==this.href){is_active=true;return false;}});}
if (! is_active){$ul.hide();}
$(
'#president_home h3'
).click(function(evt){$ul.toggle();});},open:function(){$(
"#president_home ul"
).show();}};var presseMenu={init:function(){var $ul=$(
"#presse_home ul"
);$ul.hide();$(
'#presse_home h3'
).click(function(evt){$ul.toggle();});},open:function(){$(
"#presse_home ul"
).show();}};
function initPopExtLinks(){var _isExtLink=$.fn.ExtLink.defaults.isExtLink;var myIsExtLink=function(a,options){if (a.href.indexOf(
"regie.aquitaine.fr"
) !=-1){return false;}
return _isExtLink(a,options);};var extopts={sameDomainOnly:true,isExtLink:myIsExtLink};var pageSize=$.getPageSize();pageSize.height=Math.max(600,Math.round(pageSize.height * 0.66));pageSize.width=Math.max(800,Math.round(pageSize.width * 0.66));pageSize.top=Math.round((window.screen.availHeight-pageSize.height) / 2);pageSize.left=Math.round((window.screen.availWidth-pageSize.width) / 2);$(document).ExtLink(extopts).Popup(pageSize);}
$(document).ready(function(){localMenu.init();externalMenu.init();presidentMenu.init();presseMenu.init();initPopExtLinks();});