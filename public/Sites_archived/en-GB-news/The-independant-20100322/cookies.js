/**
 * Read's a cookie
 */
function readINMCookie(pCookieName) {
	var cookieName = pCookieName + '='
	var cookies = document.cookie;
	var pos = cookies.indexOf(cookieName);
	if (pos != -1) {
		var start = pos + cookieName.length;
		var end = cookies.indexOf(';', start);
		if (end == -1) {
			end = cookies.length;
		}
		return unescape(cookies.substring(start, end));
	}
}

/**
 * Set a cookie
 */
function setINMCookie(pName, pValue) {
	var expireDate = new Date();
	expireDate.setTime(expireDate.getTime() + 1000 * 60 * 60 * 24 * 365)

	document.cookie = pName + '=' + escape(pValue) + ";expires=" + expireDate + ";path=/";
}

/**
 * Reads one individual Identity cookie
 */
function readIdentityCookie(id) {

	var idCookie = readINMCookie('idCache');
	if (idCookie == null) {
		return null;
	}
	var idCacheCookies = idCookie.split(';');
	for (var i = 0; i < idCacheCookies.length; i++) {
		var idCookie = idCacheCookies[i];
		if (idCookie.indexOf(id) != -1) {
			return idCookie;
		}
	}
}

/**
 * An array of Identity  cookie id's
 */
function readIdentityCookieIds() {
	var idCookie = readINMCookie('idCache');
	if (idCookie == null) {
		return null;
	}
	var idCacheCookies = idCookie.split(';');
	var result = new Array(idCacheCookies.length);
	for (var i = 0; i < idCacheCookies.length; i++) {
		var hepp = idCacheCookies[i].split(':')
		result[i] = hepp[0];
	}
	return result;
}

/**
 * Set's a Identity cookie
 */
function setIdentityCookie(field, value) {

	var values = '';
	var pollCookieIds = readIdentityCookieIds();
	if (pollCookieIds != null) {
		for (var i = 0; i < pollCookieIds.length; i++) {
			values += readIdentityCookie(pollCookieIds[i]) + ';';
		}
	}
	var thetime = new Date();
	var year = thetime.getYear() + 1900;
	var month = thetime.getMonth() + 1;
	var day = thetime.getDate();
	var nhours = thetime.getHours();
	var nmins = thetime.getMinutes();
	var nsecn = thetime.getSeconds();

	var curDate = year + '-' + month + '-' + day + '-' + nhours + '-' + nmins + '-' + nsecn


	values += 'id-' + field + ':' + curDate + ':' + value;
	setINMCookie('idCache', values);
}

function clearIdentityCookie()
{
	setINMCookie('idCache', '');
}

function saveIdentity(toggleId, func)
{
	var saveIdentity = document.getElementById(toggleId);

	if (saveIdentity.checked) {
		clearIdentityCookie();

		var name_from = document.getElementById(func + "name");
		var email_from = document.getElementById(func + "email");
		var city_from = document.getElementById(func + "city");
		var country_from = document.getElementById(func + "country");

		setIdentityCookie("name", name_from.value);
		setIdentityCookie("email", email_from.value);
		setIdentityCookie("city", city_from.value);
		setIdentityCookie("country", country_from.value);
	}

	return true;
}

function setIdentity(func) {
	var name_from = document.getElementById(func + "name");
	var email_from = document.getElementById(func + "email");
	var city_from = document.getElementById(func + "city");
	var country_from = document.getElementById(func + "country");

	var str_name_from = readIdentityCookie("name");
	var str_email_from = readIdentityCookie("email");
	var str_city_from = readIdentityCookie("city");
	var str_country_from = readIdentityCookie("country");

	if (name_from != null && str_name_from != null && str_name_from != 'undefined') {
		var name = str_name_from.split(':')
		name_from.value = name[2];
	}
	if (str_email_from != null && str_email_from != 'undefined') {
		var mail = str_email_from.split(':')
		email_from.value = mail[2];
	}
	if (city_from != null && str_city_from != null && str_city_from != 'undefined') {
		var city = str_city_from.split(':')
		city_from.value = city[2];
	}
	if (country_from != null && str_country_from != null && str_country_from != 'undefined') {
		var country = str_country_from.split(':')
		country_from.value = country[2];
	}
}



