	alk=0;
	blk=0;
	clk=0;
	dlk=0;
	function createMailto(sourceForm, targetField, urlType) {
		var to      = sourceForm.to.value;
		var cc      = sourceForm.cc.value;
		var bcc     = sourceForm.bcc.value;
		var subject = sourceForm.subject.value;
		var body    = sourceForm.body.value;
		var urltext = '';
		if (to != '') {
			urltext += to;
		}
		else {
			alert('Vous devez remplir le champ To');
			sourceForm.to.focus();
			return(1);
		}

		if (cc != '') {
			urltext = addDelimiter(urltext);
			urltext += 'CC=' + cc;
		}

		if (bcc != '') {
			urltext = addDelimiter(urltext);
			urltext += 'BCC=' + bcc;
		}

		if (subject != '') {
			urltext = addDelimiter(urltext);
			urltext += 'Subject=' + escape(subject);
		}

		if (body != '') {
			urltext = addDelimiter(urltext);
			urltext += 'Body=' + escape(body);
		}

		if (urlType == 'url') {
			urltext = 'mailto:' + urltext;
		}
		else {
			urltext = '<A HREF=\'mailto:' + urltext + '\'>linkText</A>';
		}

		targetField.value = urltext;
		targetField.focus();
		targetField.select();
		return(1);
	}

	function addDelimiter(inputString) {
		var inString = inputString;
		if (inString.indexOf('?') == -1) {
			inString += '?';
		}
		else {
			inString += '&';
		}
		return inString;
	}

	function testMailto(loc) {
		var doc = loc;
		if (doc.indexOf('HREF=') != -1) {
			var doc = doc.substring(doc.indexOf('HREF=')+6, doc.indexOf('>')-1);
		}
		window.location = doc;
	}

	function viewMailto(linktext) {
		alert('URL:\n\n' + linktext);
	}

	function zoom()
	{
		if (top!=this) {
			open(location.href,'Zoom','directories=no,menubar=yes,scrollbars=yes,resizable=yes,location=no,toolbar=no');
		}
		else {
			close();
		}
	}

	function mapauto(strID_DOC,strID_LANG)
	{
		var strTreeName;
		if (top.TOC!=null){ 
			strTreeName=top.TOC.GetTreeName();
			var strLink=escape('P='+ strID_DOC + '&L=' + strID_LANG ); 
			location='/scripts/file/publigen/tree/templates/map.asp?TREE=' + strTreeName + '&LEVEL=all&REFERER=' + strLink;
		}
	}
	
	function Chargement(site,id_doc,langue) {
		if (top.LE_HAUT)
		{ 
			top.frames['LE_HAUT'].cols='*,13';
			top.frames['DROIT'].location.href="/scripts/"+site+"/PubliGen/InsiteEditing/adm/InsiteEditing.asp?P="+id_doc+"&L="+langue;
		}
	}

	var tmp;

	function openWin(name,src,lngWidth,lngHeight,bScroll) {
		var xCoord;
		var yCoord;
		var strAgent=navigator.userAgent;
		if(tmp)tmp.close();
		if (strAgent.indexOf('Mozilla/3')==-1 && strAgent.indexOf('MSIE 3')==-1) {
			xCoord=(screen.width/2 - (lngWidth/2));
			yCoord=(screen.height/2 - (lngHeight/2));
		}
		tmp=open(src,name,'top='+ yCoord +',left='+ xCoord +',directories=no,menubar=no,scrollbars='+bScroll+',resizable=no,location=no,toolbar=no,width='+lngWidth+',height='+lngHeight);
		if (strAgent.indexOf('MSIE 3')==-1) tmp.focus();
	}

	var arrICONE_OFF 	= new Array(3);
	var arrICONE_ON 	= new Array(3);
	arrICONE_OFF[0] = new Image();
	arrICONE_OFF[0].src="<PUBLIGEN:IMAGES_PATH/>print.gif";arrICONE_ON[0] = new Image();
	arrICONE_ON[0].src="<PUBLIGEN:IMAGES_PATH/>print_on.gif";arrICONE_OFF[1] = new Image();
	arrICONE_OFF[1].src="<PUBLIGEN:IMAGES_PATH/>send.gif";arrICONE_ON[1] = new Image();
	arrICONE_ON[1].src="<PUBLIGEN:IMAGES_PATH/>send_on.gif";arrICONE_OFF[2] = new Image();
	arrICONE_OFF[2].src="<PUBLIGEN:IMAGES_PATH/>map.gif";arrICONE_ON[2] = new Image();
	arrICONE_ON[2].src="<PUBLIGEN:IMAGES_PATH/>map_on.gif";
	
	function rollIcone(id,status) {
	if(status=='on'){
		document.all['IconeLink'][id].src = arrICONE_ON[id].src;}
	else{
		document.all['IconeLink'][id].src = arrICONE_OFF[id].src;}
	}