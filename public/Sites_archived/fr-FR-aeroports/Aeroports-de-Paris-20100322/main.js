
function setsize(h) { 
	document.getElementById('iframe').style.height=h+'px'; 
} 
function openprintpopup() {
	window.open(document.URL+'?print=true');
}

function setActiveStyleSheet(title) {  var i, a, main;  for(i=0; (a = document.getElementsByTagName("style")[i]); i++) {    if(a.getAttribute("title")) {      a.disabled = true;      if(a.getAttribute("title") == title) a.disabled = false;    }  }}

function getActiveStyleSheet() {
  var i, a;
  for(i=0; (a = document.getElementsByTagName("style")[i]); i++) {
    if(a.getAttribute("title") && !a.disabled) return a.getAttribute("title");
  }
  return null;
}

function toggleDisplayDiv(divId){
	var div = document.getElementById(divId);
	
	if (div != null)
	{
		if (div.style.display == "none") 
		{
			div.style.display = "block";
		}
		else 
		{
			div.style.display = "none";
		}
	}
}


function getQueryString(param)
{
	var valeur = "";
	var queryString = document.location.search; //chaine querystring entiere
	var i = 0;
	i = queryString.indexOf(param+"="); //on cherche notre parametre dans la chaine querystring
	if(i > 0) //si on le trouve on recupere sa valeur
	{
		i += (param.length+1);
		iFin = queryString.indexOf("&",i);
		if(iFin >= 0)
			valeur = queryString.substring(i,iFin);
		else
			valeur = queryString.substring(i);
	}
	return valeur;
}

function setVisibility(id, isVisible)
{
	var obj = document.getElementById(id);
	
	if (obj != null)
	{
		if(isVisible)
		{	
			obj.style.display = "block";
			//obj.style.visibility="visible";
			//obj.style.position = "relative";
		}
		else
		{		
			obj.style.display = "none";
			//obj.style.visibility="hidden";
			//obj.style.position = "absolute";
		}
	}
}

function setPrintVisibility(id, isVisibleOnPrint)
{
	var printMode = getQueryString("print");
	if(printMode.toUpperCase() == "TRUE" || printMode == 1)
		setVisibility(id, isVisibleOnPrint);
}

function Search(searchUrl)
{
	var word = document.getElementById('search');
	window.location.href = searchUrl + word.value; 
}