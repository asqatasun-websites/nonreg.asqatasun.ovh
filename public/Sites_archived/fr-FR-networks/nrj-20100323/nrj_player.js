/*
 * Cherie FM, Fr�quences
 * Tous droits r�serv�s (c) Copyright 2008, NRJ GROUP
 */

// Variables propres au Live CFM
var live_height = 610;
var live_width = 950;

// Variables propre aux WR CFM


function openRadio(url, type, webradio_width, webradio_height)
{
	if(type == 'webradio')
	{
		return popUp(url,webradio_width,webradio_height,0);
	}
	else
	{
		return popUp(url,live_width,live_height,0);
	}
}


function popUp(url,largeur,hauteur,scrolling)
{
  window.open(url,'popup','width=' + largeur + ',height=' + hauteur + ',left='+(Math.round((screen.width-largeur)/2))+',top='+(Math.round((screen.height-hauteur)/2))+',scrollbars='+scrolling);
}