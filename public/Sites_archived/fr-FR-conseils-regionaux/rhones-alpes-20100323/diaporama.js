/**
 * @author jeremy@eolas.fr
 * Modification par aurelien@eolas.fr
 * Permet de g�rer le diaporama avec des images de largeurs diff�rentes
 */

YAHOO.namespace("YAHOO.diaporama");

var diaSizeBloc = 0;
var diaNbLeftHiddenBloc = 0;
var diaNbRightHiddenBloc = 0;
var oDiaContainer = null;
var oDiaListing = null;
var aDiaBloc = null;
var oImgForward = null;
var oImgReward = null;
var srcNone = '';
var srcRight = '';
var srcLeft = '';
var idInterval = null;

 YAHOO.diaporama.load = function() {
	
	try {
		
		if (srcNone == '') {
			// initialisations des objects
			oDiaContainer = document.getElementById('diaporama_container');
			oDiaListing = document.getElementById('diaporama_listing');
			oImgForward = document.getElementById('imgForward');
			oImgReward = document.getElementById('imgReward');
			aDiaBloc = YAHOO.util.Dom.getElementsByClassName('bloc', 'div', oDiaListing);

			// Mise a jour action
			var oAForward = document.getElementById('aForward');
			var oAReward = document.getElementById('aReward');
			YAHOO.util.Event.addListener(oAForward, 'mouseover', YAHOO.diaporama.smoothForward);
			YAHOO.util.Event.addListener(oAForward, 'mouseout', YAHOO.diaporama.smoothStop);
			YAHOO.util.Event.addListener(oAForward, 'click', YAHOO.diaporama.diaForward);
			YAHOO.util.Event.addListener(oAReward, 'mouseover', YAHOO.diaporama.smoothReward);
			YAHOO.util.Event.addListener(oAReward, 'mouseout', YAHOO.diaporama.smoothStop);
			YAHOO.util.Event.addListener(oAReward, 'click', YAHOO.diaporama.diaReward);
			
			// d�finition des sources pour les images
			srcNone = oImgForward.src;
			srcRight = srcNone.replace('none', 'next');
			srcLeft = srcNone.replace('none', 'previous');
			
			YAHOO.diaporama.constructDiaporama();
		}
	} catch (ex) {
		alert('load:' + ex);
	}
}


 YAHOO.diaporama.resize = function() {
	if (srcNone == '') YAHOO.diaporama.loadDiaporama();
	else YAHOO.diaporama.constructDiaporama();
}

 YAHOO.diaporama.constructDiaporama = function() {
	// Initialisation (pour le resize)
	oImgForward.src = srcNone;
	YAHOO.util.Dom.setStyle(oImgForward, 'cursor', 'default');
	oImgReward.src = srcNone;
	YAHOO.util.Dom.setStyle(oImgReward, 'cursor', 'default');
	diaNbLeftHiddenBloc = 0;
	diaNbRightHiddenBloc = 0;

	if (aDiaBloc) {	
		cx = oDiaContainer.offsetWidth;
		diaNbBloc = aDiaBloc.length;
		diaSizeBloc = (aDiaBloc[0].offsetWidth) + 12;//default : 12
	  
		// Gestion Hauteur des blocs
		blcHeightNorm = oDiaContainer.offsetHeight;
		blcHeightMax = blcHeightNorm;
		
		// R�cup�ration de la hauteur maximum des blocs
	  for (i=0;aDiaBloc[i];i++) if (aDiaBloc[i].offsetHeight > blcHeightMax) blcHeightMax = aDiaBloc[i].offsetHeight;
	  
	  try {
		  // D�finition des hauteurs des blocs
		  for (i=0;aDiaBloc[i];i++) {
		  		
          aElm = YAHOO.util.Dom.getElementsByClassName('palette', 'div', aDiaBloc[i])
		  		useHeight = (aElm.length > 0 ? aElm[0].height : 0);
		  	    
				/*oStrongs = aDiaBloc[i].getElementsByTagName('strong');
				if (oStrongs.length > 0) useHeight += oStrongs[0].offsetHeight
                */
				oUls = aDiaBloc[i].getElementsByTagName('ul');
				if (oUls.length > 0) useHeight += oUls[0].offsetHeight
				
				margin = (blcHeightMax - useHeight - 10) / 2;
				if (margin >= 0) {
					aElm[0].style.marginTop = margin + 'px';
					aElm[0].style.marginBottom = margin + 'px';
				}
				aDiaBloc[i].style.height = (blcHeightMax - 10) + 'px';
			}
		} catch (ex) {
		  alert(ex)
		}
	  
		oDiaContainer.style.height = (blcHeightMax) + 'px';
	  	oDiaContainer.style.overflow = 'hidden';
		oDiaContainer.scrollLeft = 0;
	
		// D�finition de la largeur du listing
		nb = 0 ;
		for (i=0;aDiaBloc[i];i++) {
			nb = nb+aDiaBloc[i].offsetWidth ;
		}
		
		oDiaListing.style.width = (nb + 400)  + 'px';
		//oDiaListing.style.width = (diaNbBloc * (diaSizeBloc) + 150)  + 'px';
		

		diaNbRightHiddenBloc = diaNbBloc -1 ;
		
		YAHOO.diaporama.diaScrollMaj();
	}
}

YAHOO.diaporama.diaScrollMaj = function() {
	if (diaNbLeftHiddenBloc > 0) {
	  oImgReward.src = srcLeft;
		oImgReward.style.cursor = 'pointer';
	} else {
		oImgReward.src = srcNone;
		oImgReward.style.cursor = 'default';
	}
	
	if (diaNbRightHiddenBloc > 0) {
  	oImgForward.src = srcRight;
		oImgForward.style.cursor = 'pointer';
	} else {
		oImgForward.src = srcNone;
		oImgForward.style.cursor = 'default';
	}
}

YAHOO.diaporama.smoothForward = function() {
	idInterval = setInterval(YAHOO.diaporama.diaForward, 600);
}

YAHOO.diaporama.smoothReward = function () {
	idInterval = setInterval(YAHOO.diaporama.diaReward, 600);
}

YAHOO.diaporama.smoothStop = function () {
	clearInterval(idInterval);
}


YAHOO.diaporama.diaForward = function () {
  if (diaNbRightHiddenBloc > 0 ) {
    diaNbRightHiddenBloc--;
    diaNbLeftHiddenBloc++;
    oDiaContainer.scrollLeft += aDiaBloc[diaNbLeftHiddenBloc-1].offsetWidth + 6;
  } else {
  	clearInterval(idInterval);
  }
  YAHOO.diaporama.diaScrollMaj()
}

YAHOO.diaporama.diaReward = function () {
  if (diaNbLeftHiddenBloc > 0) {
    diaNbLeftHiddenBloc--;
    diaNbRightHiddenBloc++;
    oDiaContainer.scrollLeft -= aDiaBloc[diaNbLeftHiddenBloc].offsetWidth + 6;
  } else {
  	clearInterval(idInterval);
  }
  YAHOO.diaporama.diaScrollMaj();
}
