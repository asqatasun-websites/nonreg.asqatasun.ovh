# robots.txt
# @url: http://www.developpement-durable.gouv.fr
# @generator: SPIP 2.0.9
# @template: squelettes-dist/robots.txt.html

User-agent: *
Disallow: /local/
Disallow: /ecrire/
Disallow: /plugins/
Disallow: /prive/
Disallow: /squelettes-dist/
Disallow: /squelettes/


Sitemap: http://www.developpement-durable.gouv.fr/sitemap.xml
