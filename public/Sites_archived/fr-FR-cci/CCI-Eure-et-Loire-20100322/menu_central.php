<HTML>

<HEAD>
<TITLE>menu_central</TITLE>
<meta name="keywords" content="commerce ext�rieur, export, exportations, formalit�s export, conventions d'affaire, Journ�es pays, International, formation, subvention, �conomie, �conomique, environnement, ISO 14000, ISO 9000, management environnemental, industrie, r�glementation industrielle, installation class�e, transcommerce, commerce, commer�ants prestataires de services, industriels, concours, cuisine, restauration, euro, monnaie europ�enne, kit boutique euro, entreprendre, cr�ation d'entreprise, reprise d'entreprise, fichiers, centre de formalit� des entreprises, stages, passeport en France, Club des cr�ateurs, aides, CFE, enseignement sup�rieur, �cole de commerce, emploi, march� �conomique, �tudes, alternance, cartographie, d�partement, SIG, �quipement commercial, documentation �conomique, information �conomique, Eure-et-Loir, Centre, Chartres, Nogent le Rotrou, Dreux, Ch�teaudun, organisme �conomique, chambre, technologie, innovation, fili�re automobile, constructeurs automobiles, qualit�, propri�t� industrielle, carnet ATA, Chambre, CCI, Chambre de Commerce et d'Industrie, 28, immatriculation, projet, conseil, financement, entreprise, entrepreneur, activit� �conomique, commer�ants non s�dentaires, chefs d'entreprises, tourisme, UCIA, Comit� Technique Local, Foires, sous-traitance, La lettre de la CCI, Communication �conomique, zone euro, Am�nagement du territoire, �tudes �conomiques, d�veloppement local, CDEC, Syst�me d'Information G�ographique, R�seau EGC, Etudes commerciales, Marketing, Gestion, BTS Commerce International, BTS Force de vente, BTS Assistant de Direction, Bac Pro Commerce, Contrat de qualification, Formation en alternance, Campus de Chartres, visas export, pollution, gestion des d�chets, gestion de l'eau, importation, certificats d'origine, imprim�s douaniers, hygi�ne alimentaire, zone urbaine sensible, NTIC, Intelligence �conomique, ESCEMESGC, cit� des entrepreneurs">
<meta name="description" content="La mission de la CCI est de d�velopper l'activit� des 11 000 commer�ants, industriels et prestataires de services d'Eure-et-Loir et � l'implantation de nouvelles entreprises dans le d�partement. Vous trouvez sur le site une pr�sentation de l'�conomie du d�partement et de la R�gion Centre, de l'annuaire des �lus de la Chambre et toute une gamme des services aux entreprises : aides � la cr�ation - reprise d'entreprise, formation professionnelle et initiale, d�veloppement international, �tudes de march�, services � l'industrie (environnement, qualit�, technologie-innovation, sous-traitance, NTIC), indices �conomiques du d�partement, revue de presse locale, agenda des manifestations, ? Vous avez �galement la possibilit� d'�tre conseill� et de commander en ligne des �tudes �conomiques de la CCI.">
<meta name="robots" content="all">
<meta name="revisit" content="30 days">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">

<script language="JavaScript">
<!--
function chargeUrl(framea,urla,frameb,urlb){
	top.frames[framea].location.href=urla
 	top.frames[frameb].location.href=urlb}

//-->
</script>
</HEAD>
<BODY BGCOLOR=#FFFFFF leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" link="#0066ff" vlink="#0066ff" alink="#0066ff">
<table width="96%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr align="center"> 
    <td height="402"> 
      <table width="50%" border="0" height="50">
        <tr> 
          <td height="153" colspan="3"> 
            <p align="center"><img src="menu_central.jpg" width="493" height="302" usemap="#MapMap" border="0"></p>
            <p align="center">&nbsp;</p>
          </td>
          <td width="351" height="474" rowspan="4" valign="top"> 
            <table width="75%" border="0" height="483">
              <tr> 
                <td height="140" rowspan="2"> 
                  <div align="left"> <a href="http://www.seminaires-chartres.com" title="Chartres Cit� des Entrepreneurs" target="_blank"> 
                    <img src="logo&#32;cite&#32;des&#32;entrepreneurs&#32;OK&#32;petit3.jpg" width="160" border="0"></a> 
                  </div>
                </td>
                <td align="center" height="85"> <a href="http://www.eureetloir.cci.fr/P%E9tition_RN154_salari%E9s.pdf" target="_blank"><font face="Verdana, Arial, Helvetica, sans-serif"><b><font color="FB0544" size="3" face="Verdana">RN 
                  154 T&eacute;l&eacute;chargez la p&eacute;tition</font></b></font></a></td>
              </tr>
              <tr> 
                <td align="center"><font color="#FE9603" size="3" face="Verdana"><b><a href="http://www.eureetloir.cci.fr/PreDiagnostic.htm" target="_blank"><font color="#FE9603" size="2">500 
                  pr&eacute; diagnostics financ&eacute;s par l'&eacute;tat &agrave; 
                  90%</font></a></b></font></td>
              </tr>
              <tr> 
                <td height="106" align="center"><a href="http://www.auto-entrepreneur.cci.fr" target="_blank"><img src="Logo&#32;auto-entrepreneur.jpg" width="150" height="113" border="0"></a> 
                </td>
                <td height="106" align="center"><a href="http://blog.auto-entrepreneur.cci.fr/" target="_blank"><img src="Logo&#32;blog&#32;auto-entrepreneur.jpg" width="128" height="70" border="0"></a></td>
              </tr>
              <tr> 
                <td height="54" width="35%" align=center valign="bottom"><a href="http://www.eureetloir.cci.fr/les%20entreprises%20face%20%E0%20la%20crise.php" target="_blank" ><img src="logo&#32;Entreprises&#32;face&#32;a&#32;la&#32;crise.jpg" width="140" height="56" border="0"></a></td>
                <td height="54" width="65%" align="center" valign="bottom"><a href="http://www.eureetloir.cci.fr/chambersign.php"><img src="chambersign.gif" width="120" height="42" name="chambersign" alt="ChamberSign" border="0"></a></td>
              </tr>
              <tr> 
                <td height="186" width="35%" align=center valign="bottom"> <a href="http://www.eureetloir.cci.fr/Plan%20de%20Mandature%20Page1%E04.pdf"> 
                  <img src="plan&#32;de&#32;mandature&#32;new2.jpg" width="110" height="156" border="0"></a> 
                </td>
                <td height="186" width="65%" align="center" valign="bottom"> 
                  <div align="center"><a href="http://www.eureetloir.cci.fr/annuaire%20ucia%20EN%20ENTIER%20site%20internet.pdf" target="_blank"><img src="couv&#32;annuaire&#32;ucia&#32;petit.jpg" width="181" height="156" border="0"></a><font size="3" face="Arial, Helvetica, sans-serif"> 
                    </font></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td height="24" colspan="3"> 
            <table width="100%" border="0">
              <tr> 
                <td> 
                  <div align="center"><font size="1" face="Arial, Helvetica, sans-serif"><a href="http://www.campuscci.fr" target="_blank"><font size="3" color="#0053A4" face="Verdana"><b>www.campuscci.fr</b></font></a></font> 
                  </div>
                </td>
                <td> 
                  <div align="center"><font size="1" face="Arial, Helvetica, sans-serif"><a href="http://www.sensinno.fr/" target="_blank"><font size="3" color="#0053A4" face="Verdana"><b>www.sensinno.fr</b></font></a></font> 
                  </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td colspan="2" height="137"> 
            <p align="center">&nbsp;</p>
            
            <p>&nbsp;</p>
          </td>
          <td width="165" height="137"> 
            <p align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="3"><b><font color="#990000" size="1">PANDEMIE 
              GRIPPALE :</font><font color="#FF0000" size="1"> <br>
              </font><font size="1"><a href="http://www.eureetloir.cci.fr/Grippe.htm"><font color="#990000">Plan 
              de continuit&eacute; d'activit&eacute;</font></a></font></b></font> 
              <font size="1"><br>
              <b><a href="http://www.eureetloir.cci.fr/Grippe.htm"><font color="#990000" face="Verdana">&agrave; 
              t&eacute;l&eacute;charger</font><font color="#990000"> </font></a></b></font></p>
          </td>
        </tr>
        <tr> 
          <td width="169" height="2"> 
            <p align="center">&nbsp;</p>
          </td>
          <td width="160" height="2">&nbsp;</td>
          <td width="165" height="2">&nbsp;</td>
        </tr>
      </table>
      <map name="MapMap"> 
        <area shape="rect" coords="39,98,90,114" href="javascript:chargeUrl('gauche','menu.php?Ouvre=1','bas','cci.htm');">
        <area shape="rect" coords="1,144,86,160" href="javascript:chargeUrl('gauche','menu.php?Ouvre=14','bas','cci.htm');">
        <area shape="rect" coords="36,189,98,206" href="javascript:chargeUrl('gauche','menu.php?Ouvre=23','bas','cci.htm');">
        <area shape="rect" coords="42,229,122,246" href="javascript:chargeUrl('gauche','menu.php?Ouvre=26','bas','cci.htm');">
        <area shape="rect" coords="94,261,160,277" href="javascript:chargeUrl('gauche','menu.php?Ouvre=37','bas','cci.htm');">
        <area shape="rect" coords="305,3,390,21" href="javascript:chargeUrl('gauche','menu.php?Ouvre=40','bas','cci.htm');">
        <area shape="rect" coords="354,32,418,50" href="javascript:chargeUrl('gauche','menu.php?Ouvre=50','bas','cci.htm');">
        <area shape="rect" coords="379,71,440,88" href="javascript:chargeUrl('gauche','menu.php?Ouvre=58','bas','cci.htm');">
        <area shape="rect" coords="392,116,486,136" href="javascript:chargeUrl('gauche','menu.php?Ouvre=64','bas','cci.htm');">
        <area shape="rect" coords="388,170,461,186" href="javascript:chargeUrl('gauche','menu.php?Ouvre=76','bas','cci.htm');">
        <area shape="rect" coords="377,214,486,231" href="javascript:chargeUrl('gauche','menu.php?Ouvre=95','bas','cci.htm');">
        <area shape="rect" coords="342,253,433,272" href="http://international.proforum.fr/">
      </map>
      <map name="Map"> 
        <area shape="rect" coords="39,98,90,114" href="javascript:chargeUrl('gauche','menu.php?Ouvre=1','bas','cci.htm');">
        <area shape="rect" coords="1,144,86,160" href="javascript:chargeUrl('gauche','menu.php?Ouvre=12','bas','cci.htm');">
        <area shape="rect" coords="36,189,98,206" href="javascript:chargeUrl('gauche','menu.php?Ouvre=20','bas','cci.htm');">
        <area shape="rect" coords="42,230,122,247" href="javascript:chargeUrl('gauche','menu.php?Ouvre=23','bas','cci.htm');">
        <area shape="rect" coords="94,261,160,277" href="javascript:chargeUrl('gauche','menu.php?Ouvre=32','bas','cci.htm');">
        <area shape="rect" coords="307,3,392,21" href="javascript:chargeUrl('gauche','menu.php?Ouvre=35','bas','cci.htm');">
        <area shape="rect" coords="354,33,418,51" href="javascript:chargeUrl('gauche','menu.php?Ouvre=45','bas','cci.htm');">
        <area shape="rect" coords="382,71,443,88" href="javascript:chargeUrl('gauche','menu.php?Ouvre=52','bas','cci.htm');">
        <area shape="rect" coords="397,116,491,136" href="javascript:chargeUrl('gauche','menu.php?Ouvre=59','bas','cci.htm');">
        <area shape="rect" coords="394,169,467,185" href="javascript:chargeUrl('gauche','menu.php?Ouvre=70','bas','cci.htm');">
        <area shape="rect" coords="377,215,412,231" href="javascript:chargeUrl('gauche','menu.php?Ouvre=79','bas','cci.htm');">
        <area shape="rect" coords="346,252,437,271" href="javascript:chargeUrl('gauche','menu.php?Ouvre=88','bas','cci.htm');">
      </map>
      <marquee><blink><font face="Arial, Helvetica, sans-serif" size="2"><b>
      Lundi 22 mars 2010. <b><i><font face="Arial, Helvetica, sans-serif" size="4" color="#FF9900">Bienvenue sur le site de la CCI d'Eure-et-Loir</font></i></b>      </b></font></blink></marquee></td>
  </tr>
  <tr align="center">
    <td height="402">&nbsp;</td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      
    <td width="50%" align="center"><font face="Arial, Helvetica, sans-serif" size="1"><b>REALISATION 
      : <a href="http://www.newtimes.fr" target="_blank">NEWTIMES 2000</a> &#169;</b></font></td>
      
    <td align="center"><font face="Arial, Helvetica, sans-serif" size="1"></font></td>
    </tr>
  </table>
</BODY>
</HTML>