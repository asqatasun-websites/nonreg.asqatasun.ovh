var ie = (document.all)? true:false;

function popupAffiche(arg)
{
	// on sauvegarde l'image dans ses dimensions reelles
	imageObj.src = document.getElementById("nomImg"+arg).value;

	var popup = "<div id = \"popup\">";
	popup += "<div id = \"fermeture\" onClick = \"document.getElementById('page').removeChild(this.offsetParent)\"><img src = \"images/fermeture.jpg\" width = 20 height = 20></div>";
	popup += "<div id = \"titre\">"+document.getElementById("infoTitre"+arg).innerHTML+"</div>";
	popup += "<div onClick = \"aggrandir();\" id = \"image\">"+document.getElementById("infoImg"+arg).innerHTML+"</div>";
	popup += "<div id = \"format\">"+document.getElementById("F"+document.getElementById("infoFormat"+arg).value).value+"</div>";
	popup += "<div id = \"cote\">"+document.getElementById("infoCote"+arg).innerHTML+"</div>";
	popup += "<br><br><div id = \"nom_edition\">"+document.getElementById("infoNom_edition"+arg).value+" - </div>";
	popup += "<div id = \"lieu_edition\">"+document.getElementById("infoLieu_edition"+arg).value+" - </div>";
	popup += "<div id = \"date_edition\">"+document.getElementById("infoDate_edition"+arg).value+"</div>";
	if (document.getElementById("S"+document.getElementById("infoSerie"+arg).innerHTML) != null)
		popup += "<div id = \"serie\">"+document.getElementById("S"+document.getElementById("infoSerie"+arg).innerHTML).value+"</div>";
	popup += "<div id = \"observations\">"+document.getElementById("infoObservations"+arg).value+"</div>";
	popup += "</div>";
	document.getElementById('page').innerHTML += popup;
	document.getElementById("popup").onmousedown = beginDrag;
}

function popupActualite(arg)
{
	var popup = "<div id = \"popup\" style = \"min-width: 200px; max-width: 600px;\">";
	popup += "<div id = \"fermeture\" onClick = \"document.getElementById('page').removeChild(this.offsetParent)\"><img src = \"images/fermeture.jpg\" width = 20 height = 20></div>";
	popup += "<div id = \"image\"><img src = '"+document.getElementsByName("nomImg")[arg].value+"' /></div>";
	popup += "<div id = \"titre\"><h2>"+document.getElementsByName("titre")[arg].innerHTML+"</h2</div>";
	popup += "<div id = \"txtCourt\">"+document.getElementsByName("txtCourt")[arg].innerHTML+"</div>";
	popup += "<div id = \"txtLong\">"+document.getElementsByName("txtlong")[arg].value+"</div>";
	popup += "<ul><li><a href = \""+document.getElementsByClassName("lien")[arg].innerHTML+"\" id = \"lien\">"+document.getElementsByClassName("lien")[arg].innerHTML+"</a></li>";
	if (document.getElementsByName("partenaire")[arg].value == 1)
		popup += "<li id = \"partenaire\">Partenaire P&ograve;rta d'&Ograve;c</li>";
	popup += "<li>"+document.getElementsByName("logoDialecte")[arg].innerHTML+"</li></ul>";
	popup += "</div>";
	document.getElementById('page').innerHTML += popup;
	document.getElementById("popup").onmousedown = beginDrag;
}

var imageObj = new Image();
function aggrandir()
{
	if (document.getElementById('popup').offsetWidth < imageObj.width)
		document.getElementById('popup').style.width = imageObj.width;
	document.getElementById('image').innerHTML = "<img src = '"+imageObj.src+"' />";
}

function doDrag(e)
{
	var e1 = document.getElementById("popup");
	if (ie)
	{
		var difX=event.clientX-window.lastX;
		var difY=event.clientY-window.lastY;
	}
	else
	{
		var difX=e.clientX-window.lastX;
		var difY=e.clientY-window.lastY;
	}
	
	var newX1 = parseInt(e1.offsetLeft)+difX;
	var newY1 = parseInt(e1.offsetTop)+difY;

	e1.style.left=newX1+"px";
	e1.style.top=newY1+"px";

	if(ie)
	{
		window.lastX=event.clientX;
		window.lastY=event.clientY;
	}
	else
	{
		window.lastX=e.clientX;
		window.lastY=e.clientY;
	}
}

function beginDrag(e)
{
	if(ie)
	{
		window.lastX=event.clientX;
		window.lastY=event.clientY;
	}
	else
	{
		window.lastX=e.clientX;
		window.lastY=e.clientY;
	}

	document.onmousemove=doDrag;
	document.onmouseup=endDrag;
}

function endDrag(e)
{
	document.onmousemove=null;
}