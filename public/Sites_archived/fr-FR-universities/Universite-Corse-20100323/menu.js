var vertical = false;
var centrer_menu = false;

//var largeur_menu = new Array(108,100,117,109,125,176,116);
var largeur_menu = new Array(116,100,117,109,125,176,116);
var largeur_sous_menu = new Array(250,250,250,250,250,250,250);

var hauteur_menu = 25;
var largeur_auto_ssmenu = true;
var espace_entre_menus = 1;
var top_menu = 137; // Position ssmenu par rapport au top
var top_ssmenu = top_menu + 28;
var left_menu = 0;
var left_ssmenu = largeur_menu+2;
var delai = 650; // en milliseconde
var marge_en_haut_de_page = top_menu + 40;
var marge_a_gauche_de_la_page = largeur_menu + 10;
var suivre_le_scroll=false;
var cacher_les_select=false;

var nbmenu = 0; //Auto-calcul�
var timeout; //ne pas toucher, c'est pour d�clarer la variable
var agt = navigator.userAgent.toLowerCase();
var isMac = (agt.indexOf('mac') != -1);
var isOpera = (agt.indexOf('opera') != -1);
var IEver = parseInt(agt.substring(agt.indexOf('msie ') + 5));
var isIE = ((agt.indexOf('msie')!=-1 && !isOpera && (agt.indexOf('webtv')==-1)) && !isMac);
var isIE5win = (isIE && IEver >= 5);
var isIE5mac = ((agt.indexOf('msie') != -1) && isMac);
var isSafari = (agt.indexOf('safari') != -1);

//pour enlever les "px" pour faire des calculs...
var reg = new RegExp("px", "g");

function preChargement(){
	if (document.getElementById("conteneurmenu")){ document.getElementById("conteneurmenu").style.visibility="hidden"; }
}

function Chargement() {
	nbmenu = 0;
	while (document.getElementById("menu"+(nbmenu+1)))
		nbmenu++;
	
	trimespaces();
	
	positionne();
	CacherMenus();
	
	if(isSafari) { document.getElementById('conteneurmenu').style.fontSize='10px'; }
	if(document.getElementById("conteneurmenu")) {document.getElementById("conteneurmenu").style.visibility='';}
}
window.onresize = Chargement;


function positionne() {
	var largeur_fenetre;
	if (document.documentElement && document.documentElement.clientWidth) {
		largeur_fenetre = document.documentElement.clientWidth;
	} else if (document.body && document.body.clientWidth) {
		largeur_fenetre = document.body.clientWidth;
	} else if (window.innerWidth) {
		largeur_fenetre = window.innerWidth;
	}

	cumul = 0;
	for(i=1;i<=nbmenu;i++) {
		if (document.getElementById("ssmenu"+i)){//undefined
			with(document.getElementById("ssmenu"+i).style) {
							
				lefta = (( largeur_fenetre - 1002 ) / 2 ) + 41;
				
				if(!isIE){lefta=lefta-9;}
				if(lefta<0){lefta=241;}else{lefta=lefta}
				
				top = 155 + "px";
				
				//left=(((i-1)*espace_entre_menus)+cumul+lefta)+"px";
				left=(cumul+lefta)+"px";

				if (!suivre_le_scroll || isIE || isIE5mac)
				position="absolute";
				else position="fixed";
				
				if (isIE || isOpera || isIE5mac || !largeur_auto_ssmenu) {
					if (isFinite(largeur_sous_menu))
						width = largeur_sous_menu+(largeur_sous_menu!="auto"?"px":"");
					else
						width = largeur_sous_menu[i-1]+(largeur_sous_menu[i-1]!="auto"?"px":"");
				} else { width = "auto"; }

				margin="0";
				zIndex="3";
			}
		}
		if ((!vertical && isFinite(largeur_menu)) || (vertical && isFinite(hauteur_menu))) {
			//cumul += (!vertical?largeur_menu:hauteur_menu);
			cumul += 169;
			
		}
		else {
			cumul += (!vertical?largeur_menu[i-1]:hauteur_menu[i-1]);
		}
	}
}


function MontrerMenu(strMenu) {
	AnnulerCacher();
	CacherMenus();
	if (document.getElementById(strMenu))//undefined
		with (document.getElementById(strMenu).style)
			visibility="visible";
	SelectVisible("hidden",document.getElementsByTagName('select'));
}

function CacherDelai() {
	timeout = setTimeout('CacherMenus()',delai);
}
function AnnulerCacher() {
	if (timeout) { clearTimeout(timeout); }
}
function CacherMenus() {
	for(i=1;i<=nbmenu;i++) {
		if (document.getElementById("ssmenu"+i))//undefined
			with(document.getElementById("ssmenu"+i).style)
				visibility="hidden";
	}
	SelectVisible("visible",document.getElementsByTagName('select'));
}

function trimespaces() {
	//Contourne un bug d'IE5/win... il ne capte pas bien les css pour les <li>, donc on les vire !
	if(isIE5win) {
		for(i=1;i<=nbmenu;i++) {
			if (document.getElementById("ssmenu"+i))//undefined
				with(document.getElementById("ssmenu"+i))
					innerHTML = innerHTML.replace(/<LI>|<\/LI>/g,"");
		}
	}
}

function SelectVisible(v,elem) {
	if (cacher_les_select && (isIE||isIE5win))
		for (var i=0;i<elem.length;i++) elem[i].style.visibility=v;
}
