Array.prototype.max = function() {
var max = this[0];
var len = this.length;
for (var i = 1; i < len; i++) if (this[i] > max) max = this[i];
return max;
}
Array.prototype.min = function() {
var min = this[0];
var len = this.length;
for (var i = 1; i < len; i++) if (this[i] < min) min = this[i];
return min;
}


$(document).ready(function() {

// ====================
// = Tri par code APE =
// ====================


	var codeape = new Array();	
	for (var i=0; i <= 88; i++) {
		codeape[i] = new Array();
	};
	
	codeape[1][88]="Action sociale sans hébergement";
	codeape[2][82]="Activités administratives et autres activités de soutien aux entreprises";
	codeape[3][66]="Activités auxiliaires de services financiers et d'assurance";
	codeape[4][90]="Activités créatives, artistiques et de spectacle";""
	codeape[5][71]="Activités d'architecture et d'ingénierie ; activités de contrôle et analyses techniques";
	codeape[6][77]="Activités de location et location-bail";
	codeape[7][53]="Activités de poste et de courrier";
	codeape[8][79]="Activités des agences de voyage, voyagistes, services de réservation et activités connexes";
	codeape[9][97]="Activités des ménages en tant qu'employeurs de personnel domestique";
	codeape[10][94]="Activités des organisations associatives";
	codeape[11][99]="Activités des organisations et organismes extraterritoriaux";
	codeape[12][64]="Activités des services financiers, hors assurance et caisses de retraite";
	codeape[13][70]="Activités des sièges sociaux ; conseil de gestion";
	codeape[14][68]="Activités immobilières";
	codeape[15][98]="Activités indifférenciées des ménages en tant que producteurs de biens et services pour usage propre";
	codeape[16][69]="Activités juridiques et comptables";
	codeape[17][78]="Activités liées à l'emploi";
	codeape[18][86]="Activités pour la santé humaine";
	codeape[19][93]="Activités sportives, récréatives et de loisirs";
	codeape[20][75]="Activités vétérinaires";
	codeape[21][84]="Administration publique et défense ; sécurité sociale obligatoire";
	codeape[22][65]="Assurance";
	codeape[23][74]="Autres activités spécialisées, scientifiques et techniques";
	codeape[24][8]="Autres industries extractives";
	codeape[25][32]="Autres industries manufacturières";
	codeape[26][96]="Autres services personnels";
	codeape[27][91]="Bibliothèques, archives, musées et autres activités culturelles";
	codeape[28][36]="Captage, traitement et distribution d'eau";
	codeape[29][19]="Cokéfaction et raffinage";
	codeape[30][37]="Collecte et traitement des eaux usées";
	codeape[31][38]="Collecte, traitement et élimination des déchets  récupération";
	codeape[32][47]="Commerce de détail, à l'exception des automobiles et des motocycles";
	codeape[33][46]="Commerce de gros, à l'exception des automobiles et des motocycles";
	codeape[34][45]="Commerce et réparation d'automobiles et de motocycles";
	codeape[35][41]="Construction de bâtiments";
	codeape[36][1]="Culture et production animale, chasse et services annexes";
	codeape[37][39]="Dépollution et autres services de gestion des déchets";
	codeape[38][58]="Édition";
	codeape[39][80]="Enquêtes et sécurité";
	codeape[40][85]="Enseignement";
	codeape[41][52]="Entreposage et services auxiliaires des transports";
	codeape[42][6]="Extraction d'hydrocarbures";
	codeape[43][5]="Extraction de houille et de lignite";
	codeape[44][7]="Extraction de minerais métalliques";
	codeape[45][30]="Fabrication d'autres matériels de transport";
	codeape[46][23]="Fabrication d'autres produits minéraux non métalliques";
	codeape[47][27]="Fabrication d'équipements électriques";
	codeape[48][11]="Fabrication de boissons";
	codeape[49][28]="Fabrication de machines et équipements n.c.a.";
	codeape[50][31]="Fabrication de meubles";
	codeape[51][12]="Fabrication de produits à base de tabac";
	codeape[52][22]="Fabrication de produits en caoutchouc et en plastique";
	codeape[53][26]="Fabrication de produits informatiques, électroniques et optiques";
	codeape[54][25]="Fabrication de produits métalliques, à l'exception des machines et des équipements";
	codeape[55][13]="Fabrication de textiles";
	codeape[56][42]="Génie civil";
	codeape[57][87]="Hébergement médico-social et social";
	codeape[58][55]="Hébergement";
	codeape[59][18]="Imprimerie et reproduction d'enregistrements";
	codeape[60][29]="Industrie automobile";
	codeape[61][20]="Industrie chimique";
	codeape[62][14]="Industrie de l'habillement";
	codeape[63][15]="Industrie du cuir et de la chaussure";
	codeape[64][17]="Industrie du papier et du carton";
	codeape[65][21]="Industrie pharmaceutique";
	codeape[66][10]="Industries alimentaires";
	codeape[67][24]="Métallurgie";
	codeape[68][92]="Organisation de jeux de hasard et d'argent";
	codeape[69][3]="Pêche et aquaculture";
	codeape[70][59]="Production de films cinématographiques, de vidéo et de programmes de télévision ; enregistrement sonore et édition musicale";
	codeape[71][35]="Production et distribution d'électricité, de gaz, de vapeur et d'air conditionné";
	codeape[72][60]="Programmation et diffusion";
	codeape[73][62]="Programmation, conseil et autres activités informatiques";
	codeape[74][73]="Publicité et études de marché";
	codeape[75][72]="Recherche-développement scientifique";
	codeape[76][95]="Réparation d'ordinateurs et de biens personnels et domestiques";
	codeape[77][33]="Réparation et installation de machines et d'équipements";
	codeape[78][56]="Restauration";
	codeape[79][63]="Services d'information";
	codeape[80][9]="Services de soutien aux industries extractives";
	codeape[81][81]="Services relatifs aux bâtiments et aménagement paysager";
	codeape[82][2]="Sylviculture et exploitation forestière";
	codeape[83][61]="Télécommunications";
	codeape[84][51]="Transports aériens";
	codeape[85][50]="Transports par eau";
	codeape[86][49]="Transports terrestres et transport par conduites";
	codeape[87][16]="Travail du bois et fabrication d'articles en bois et en liège, à l'exception des meubles ; fabrication d'articles en vannerie et sparterie";
	codeape[88][43]="Travaux de construction spécialisés";
	
	var content = new Array();
	var compteur=0;
	$(".test #ads .ads_entry_summary h2").each(function(){
		var titreNum = $(this).children("span").html().substring(0,2);
		var titre = $(this).html();
		content[compteur] = new Array();
		content[compteur][titreNum] = titre;
		compteur++;
	});
	
	var display = "";
	var lastTitreNum;
	for (var i=0; i < codeape.length; i++) {
		for (var j=0; j < codeape[i].length; j++) {
			if(codeape[i][j]){
				for (var k=0; k < content.length; k++) {
					for (var l=0; l < content[k].length; l++) {
						if(content[k][l] && j==l){
							if(lastTitreNum!=l){
								display += "<div class='blocape'><h2><span>"+codeape[i][j]+"</span></h2>";
							}
							display += "<p>"+content[k][l]+"</p>";

							lastTitreNum = l;
							if(lastTitreNum!=l){
								display += "</div>";
							}							
						}
					};
				};
			}
		};
	};

	$(".test #ads #adsline1").wrap("<div id='tri_alpha'></div>");
	
	$(".test #ads #tri_alpha").after("<div id='tri_ape'>"+display+"</div>");

	// Cacher les listes d'entreprises
	$(".blocape p").hide();
	
	$(".blocape h2").click(function (){
		$(this).parent().children("p").toggle();
	}).css("cursor","pointer");
	
	
	// Changer le mode d'affichage
	$("#tri_alpha").hide();

	var tri_etat1 = true;
	var tri_etat2= false;
	var tri_libelle1 = "Trier par entreprise";
	var tri_libelle2 = "Trier par code APE";	
	
	$(".test #ads_presentation").after("<a href='#' id='change'>"+ tri_libelle1 + "</a>");	
	
	$("#change").click(function () {
		if (tri_etat1) {
			$("#tri_alpha").show();
			$("#tri_ape").hide();
			$("#change").text(tri_libelle2);
			tri_etat1 = false;
			tri_etat2 = true;
			return false;
		}else{
			$("#tri_alpha").hide();
			$("#tri_ape").show();
			$("#change").text(tri_libelle1);
			tri_etat1 = true;
			tri_etat2 = false;
			return false;
		}
		
	});
	
	
	
	
	$("#media_downloads h1").map(function() {
			$('.breadcrumb').after(this);
	});
	
	$("#media_downloads span p").map(function() {
			$('.boxproduct').before(this);
	});
	
	
// ================
// = Banner Flash =
// ================
	
	// $('#flash').flash({
	//     src: '/images/theme/anim.swf',
	//     width: 730,
	//     height: 190,
	// 	wmode: 'transparent'
	// });
	
// ======================
// = Menu en accordéon  =
// ======================

	if($("#nested")) {
			
			$("#nested .second_level").hide();
			$("#nested .third_level").hide();
			$("#nested .fourth_level").hide();
			$("#nested .first_level>li>a>span").click(function() {
				if(this.className.indexOf("clicked") != -1) {
					$(this).parent().next().slideUp(200);
					$(this).removeClass("clicked");
				}
				else {
					$("#nested .first_level>li>a>span").removeClass();
					$(this).addClass("clicked");
					$("#nested .second_level:visible").slideUp(200);
					$(this).parent().next().slideDown(500);
				}
				return false;
			});
			$(".active .second_level").show();
			$(".active .second_level>.active>.third_level").show();
			$(".active .second_level>.active>.third_level>.active>.fourth_level").show();
			
			
		}

// ===============
// = Font resize =
// ===============

var url = document.location.href;

var myTools = '<ul><li><a class="printer" onclick="window.print();return false;" title="Imprimer cette page" href="#">Imprimer cette page</a></li><li><a class="email_go" id="slick-toggle" title="Transmettre" href="mailto:?body='+url+'">Transmettre</a></li><li><a class="decreaseFont" href="#" id="decreaseFont" title="T+">Caractères plus petits</a></li><li><a class="resetFont" href="#" id="resetFont" title="T0">Caractères standards</a></li><li><a class="increaseFont" href="#" id="increaseFont" title="T-">Caractères plus grands</a></li></ul>';

// var myTools2 = '<a class="printer" onclick="window.print();return false;" title="Imprimer cette page" href="#"><img src="/images/icons/printer.png"/></a><a class="email_go" id="slick-toggle" title="Transmettre" href="mailto:?body='+url+'"><img src="/images/icons/email_go.png"/></a><a class="decreaseFont" href="#" id="decreaseFont" title="T+"><img src="/images/icons/tools/font-smaller.gif"/></a><a class="resetFont" href="#" id="resetFont" title="T0"><img src="/images/icons/tools/font-standard.gif"/></a><a class="increaseFont" href="#" id="increaseFont" title="T-"><img src="/images/icons/tools/font-larger.gif"/></a>';		
var myTools2 = '<a class="printer" onclick="window.print();return false;" title="Imprimer cette page" href="#"><img src="/images/theme/picto-print.gif"/></a><a style="margin-left:10px" class="email_go" id="slick-toggle" title="Transmettre" href="mailto:?body='+url+'"><img src="/images/theme/picto-send.gif"/></a><a style="margin-left:10px" class="textsize resetFont" href="#" id="resetFont" title="Zoom -"><img src="/images/theme/picto-smaller.gif"/></a><a style="margin-left:10px" class="textsize increaseFont" href="#" id="increaseFont" title="Zoom +"><img src="/images/theme/picto-bigger.gif"/></a>';		

$("#rightbar").prepend(myTools2);

$(function(){
		$('a.textsize').click(function(){
			var ourText = $('p');
			var currFontSize = ourText.css('fontSize');
			var finalNum = parseFloat(currFontSize, 10);
			var stringEnding = currFontSize.slice(-2);
			if(this.id == 'increaseFont') {
				finalNum *= 1.2;
			}
			else if (this.id == 'resetFont'){
				finalNum /=1.2;
			}
			ourText.css('fontSize', finalNum + stringEnding);
		});
	});


// ========
// = Quiz =
// ========

	var nbReponses = 0;
	
	// Parsing des valeurs du formulaire lors de l'envoi
	$('#poll_form').submit(function(){
       	//on récupère les valeurs cochés égales à "oui"
		$('#poll_form input[type=radio]:checked').each(function(n,element){
			if($(element).val()=="Oui"){
				nbReponses++;
			}
       });
		$.cookie("resultats", nbReponses);
		// alert(nbReponses);
     });
	
	
	// Affichage des résultats
	var results = $.cookie("resultats");
	// alert('Vous avez répondu '+results+' fois "oui"');
	$("#reponsesQuiz").replaceWith('<h3>Vous avez répondu '+results+' fois "oui"</h3>');
	
	$("#reponse0-3").hide();
	$("#reponse3-5").hide();
	$("#reponse6-u").hide();
	
	if(results<=2){
		// alert("moins de 3");
		$("#reponse0-3").show();		
	}else if(results>=3 && results<=5 ){
		// alert("entre 3 et 5");
		$("#reponse3-5").show();				
	}else if(results>=6){
		// alert("plus de 5");
		$("#reponse6-u").show();		
	}






// ======================================
// = Divers manipulations de l'affichage =
// ======================================


$(".gallery a").fancybox();

$('.espace_entreprise a span').click(function(){
	window.location = 'http://www.alencon.cci.fr/espace-clubs/';
});

$("annonce").click(function () {
      $(this).replaceWith("Enseigne");
 });


$(".mon_compte label[for='newsletter']").map(function() {
		$(this).replaceWith('<label for="newsletter">Inscription au journal l\'Eclair en PDF<span class="obligatory"></span> :</label>');
});

$("#formproductversion").hide();
$("p.prix").show(function() {
	$("#formproductversion").show();
});

$('a.LinkOut').click(function(){
this.target = "_blank";
});

$('#rightbar').append($('#contact'));

$('#rightbar').append($('.widget'));





$('#empty_cart').after('<p><a href="/catalogue-2/" class="buttons retour">Retour au catalogue</p>');

$('.panier a').addClass('buttons').addClass('add_basket');



$('a[title="Supprimer"]').addClass('supprimer');
$('input[name="updatecartsubmit"]').addClass('updatecartsubmit').addClass('buttons');
$('input[name="deletecartsubmit"]').addClass('deletecartsubmit').addClass('negative').addClass('buttons');
$('input[name="validcarsubmit"]').addClass('valid').addClass('positive').addClass('buttons');
$('#adresseselectionvalidationform input[type="submit"]').addClass('valid').addClass('buttons').addClass('positive');
$('#loginform input[type="submit"]').attr("value", "Valider").addClass('valid').addClass('buttons').addClass('positive');
$('#loginboxform input[type="submit"]').attr("value", "Valider").addClass('valid').addClass('buttons').addClass('positive');

$('#account_authentification_subscription a').addClass('user_add').addClass('buttons').addClass('positive');

$('#subscribeform input[type="submit"]').attr("value", "Valider").addClass('valid').addClass('buttons').addClass('positive');
$('input[type="text"]').addClass('textfield');
$('input[type="password"]').addClass('textfield');
$('.address a').addClass('buttons').addClass('modifaddr');
$('#cart_detail a[href="viewcart"]').addClass('buttons').addClass('modif_basket');
$('#form').addClass('uniForm');
$('form[name="ordervalidationform"] input[type="submit"]').addClass('buttons').addClass('valid').addClass('positive');
$('#form .submit input[type="submit"]').addClass('buttons').addClass('valid').addClass('positive');
$('#cybermut_form input[type="submit"]').addClass('buttons').addClass('payment').addClass('positive');
$('a:contains("paiement par chèque")').addClass('buttons').addClass('payment').addClass('positive');

$('a:contains("Choisir un autre moyen de paiement")').addClass('buttons').addClass('negative').addClass('deletecartsubmit');
// $('#payment_actions a:last-child').addClass('buttons').addClass('validcarsubmit');
$('#cart_footer a[title="Imprimer cette page"]').addClass('buttons').addClass('print');
$('a[title="Retour"]').addClass('buttons').addClass('retour');
$('a#btn_retour').addClass('buttons').addClass('retour');
$('a[title="Revenir à la page précédante"]').addClass('buttons').addClass('retour');
$('a[title="Aperçu"]').addClass('buttons').addClass('apercu');
$('.pager a:contains("précédent")').addClass('buttons').addClass('prev');
$('.pager a:contains("suivant")').addClass('buttons').addClass('next');
$('.pager a.prev').addClass('buttons');
$('.pager a.next').addClass('buttons');
$('.boxcart .viewcart a').addClass('buttons').addClass('viewcart');
$('a:contains("Ajouter une adresse")').addClass('buttons').addClass('add').addClass('positive');
$('a[title="Editer"]').addClass('buttons').addClass('edit');


// ========================
// = Navigation catalogue =
// ========================

$("#nested .first_level .catalogue").hide();
$("#nested .first_level .catalogue.active").show().each(function(){
	// $("#nested .first_level").prepend($(this));
	// $("#rightbar").append($(this));
	$(this).wrap("<ul id='catalNav'></ul>")
	$("#rightbar").append($("#catalNav"));
});


// if($(".deux_colonnes .textAndImages")){
// 	var hauteurs = new Array();
// 	$(".deux_colonnes .textAndImages").each(function(){
// 		hauteurs.push($(this).height());
// 	});
// 
// 	$(".deux_colonnes .textAndImages").each(function(){
// 		$(this).height(hauteurs.max());
// 	});
// }


// // Permet d'attendre que les images soient chargées avant de tester la hauteur de #conteneur
// while(!$(window).load()){
//  // break;
// }
// 
// $(".deux_colonnes").css({overflow:'hidden'}).each(function(){
// 	$(this).find(".textAndImages").height($(this).height()-20);
// });



});