			// This is to set security trusting between main page and iframe.
			// for mor information please see : http://msdn2.microsoft.com/en-us/library/ms533028.aspx

			/***********************************************
			* IFrame SSI script II- Dynamic Drive DHTML code library (http://www.dynamicdrive.com)
			* Visit DynamicDrive.com for hundreds of original DHTML scripts
			* This notice must stay intact for legal use
			***********************************************/

			//Input the IDs of the IFRAMES you wish to dynamically resize to match its content height:
			//Separate each ID with a comma. Examples: ["myframe1", "myframe2"] or ["myframe"] or [] for none:
			var iframeids=["myiframe"]

			//Should script hide iframe from browsers that don't support this script (non IE5+/NS6+ browsers. Recommended):
			var iframehide="yes"
			
				function resizeCaller()
				{
					var RegEx = new RegExp("http://");
					var unRegEx = new RegExp("http://www.peugeot.fr");
					var testeur = new RegExp("http://[a-z0-9]+\.([^/]+)/");
					document.domain = testeur.exec(window.location)[1];
					var newRegEx = new RegExp(testeur.exec(window.location)[1]);
					if (RegEx.test($(iframeids[0]).src) == false || unRegEx.test($(iframeids[0]).src) == true || newRegEx.test(window.location))
					{
						var dyniframe=new Array()
						for (i=0; i<iframeids.length; i++){
							if (document.getElementById)
							resizeIframe(iframeids[i])
							//reveal iframe for lower end browsers? (see var above):
							if ((document.all || document.getElementById) && iframehide=="no"){
								var tempobj=document.all? document.all[iframeids[i]] : document.getElementById(iframeids[i])
								tempobj.style.display="block"
							}
						}
					}
					else
					{
						$(iframeids[0]).setStyle("display","block");
					}
				}

				function resizeIframe(frameid)
				{
					var frame = document.getElementById(frameid)
					if (!frame || window.opera)
						return ;
					
					frame.style.display = "block";

					var doc = frame.contentWindow || frame.contentDocument;
					if (doc.document)
						doc = doc.document;
					var height = doc.body.offsetHeight;
					if (doc.body.scrollHeight > height)
						height = doc.body.scrollHeight;
					frame.height = height + "px";

					if (frame.addEventListener)
						frame.addEventListener("load", readjustIframe, false)
					else if (frame.attachEvent)
					{
						frame.detachEvent("onload", readjustIframe) // Bug fix line
						frame.attachEvent("onload", readjustIframe)
					}
				}

				function readjustIframe(loadevt) {
					var crossevt=(window.event)? event : loadevt
					var iframeroot=(crossevt.currentTarget)? crossevt.currentTarget : crossevt.srcElement
					if (iframeroot)
						resizeIframe(iframeroot.id);
				}

				function loadintoIframe(iframeid, url){
					if (document.getElementById)
						document.getElementById(iframeid).src=url
				}
