function vidUpdateContent(data) {
    $('#enImageWrapper').empty().append(data);
    vidOnLoad();
}

function vidOnLoad() {
    $("a.video").click(function(evt) {
            //alert(this.href);
            evt.preventDefault(); evt.stopPropagation();
            var m = this.href.match(/[?&]vid=(\d+)/);
            $.get('spip.php', {page:'inc-video', vid:m[1]}, vidUpdateContent);
            return false;
        });

}

$(document).ready(vidOnLoad);
