  /*
  * Cache tous les divs ayant le m�me pr�fixe
  */
  function closeAllMenu(bMontrerPhoto) {
	 var menuPrincipal = document.getElementById('menu_principal');
     if (menuPrincipal) {
    	 if (bMontrerPhoto == null || bMontrerPhoto == true) {
    		 var menuPrincipalItem1 = document.getElementById('menu_principal_1');
    		 menuPrincipalItem1.style.backgroundImage = 'url(images/accueil/vous_etes_1.png)';
    		 var menuPrincipalItem2 = document.getElementById('menu_principal_2');
    		 menuPrincipalItem2.style.backgroundImage = 'url(images/accueil/vous_etes_2.png)';
    	 }
    	 // prendre tous les fils du menu principal pour les rendre invisible
    	 var filsMenuPrincipal = menuPrincipal.getElementsByTagName("*");
    	 for(var i=0; i<filsMenuPrincipal.length; i++){
    		 if (filsMenuPrincipal[i].className == 'menu_open') {
    			 filsMenuPrincipal[i].className = '';
    		 }
    		 if (filsMenuPrincipal[i].className == 'element_menu_cache') {
    			 filsMenuPrincipal[i].style.display = 'none';
    		 }
    	 }
     }
  }
  
  
  /*
   * montrer le div qui est pass� en argument
   */
  function montrerActu(idElementACacher) {
	  // onferme toute les actus ouvertes
	  closeAllMenu(false);
	  var menuPrincipalItem1 = document.getElementById('menu_principal_1');
	  menuPrincipalItem1.style.backgroundImage = '';
	  var menuPrincipalItem2 = document.getElementById('menu_principal_2');
	  menuPrincipalItem2.style.backgroundImage = '';

	  
	  // prendre tous les fils du menu principal pour les rendre visible
	  var menuPrincipal = document.getElementById('menu_principal_' + idElementACacher );
	  menuPrincipal.className = "menu_open";
	  if (menuPrincipal) {
	 	 var filsMenuPrincipal = menuPrincipal.getElementsByTagName("*");
	 	 for(var i=0; i<filsMenuPrincipal.length; i++){
	 		 if (filsMenuPrincipal[i].className == 'element_menu_cache') {
	 			 filsMenuPrincipal[i].style.display = 'block';
	 		 }
	 	 }
 	 }
  }
