// Initialisation de la home
function initHome() 
{
	// Menu
	menu_init('menu-2');
	
	// Moteur de recherche
	RechercheRub('articles');
	
	// Bloc des articles � la une
	//~ affALaUne(0, 5, 5, false);
	affALaUne(0, false, false);
	
	// Bloc des articles pr�f�r�s
	affPref('pref0');
	zapNewsInit('prefBloc0');
	zapNewsInit('prefBloc1');
	zapNewsInit('prefBloc2');
}

// Initialisation de la home
function initHome2() 
{
	// Menu
	menu_init('menu-2');
	
	// Moteur de recherche
	RechercheRub('articles');
	
	// Bloc des articles � la une
	//~ affALaUne(0, 5, 5, false);
	//~ affALaUne(0, false, false);
	
	// Bloc des articles pr�f�r�s
	affPref('pref0');
	zapNewsInit('prefBloc0');
	zapNewsInit('prefBloc1');
	zapNewsInit('prefBloc2');
}

// Initialisation des pages li�es au CMS
function initCMS() 
{
	// Menu
	menu_init('menu-2');
	
	// Bloc des articles pr�f�r�s
	affPref('pref1')
}

// Gestion du bloc "A la une"
//~ function affALaUne(div,old,total,clear)
//~ var cpt = 0;
var old;
var timer;
function affALaUne(div, hideOld, waitMore)
{
	var uneCursor = document.getElementById('uneCursor');
	
	switch (div)
	{
		case 0:
			uneCursor.style.marginTop = '-4px';
		break;
		case 1:
			uneCursor.style.marginTop = '43px';
		break;
		case 2:
			uneCursor.style.marginTop = '90px';
		break;
		case 3:
			uneCursor.style.marginTop = '137px';
		break;
	}
	
	if (hideOld == true){
		document.getElementById('fam' + old).style.display = "block";
		new Effect.Fade('fam' + old, {duration: 0.4, fps:25});
		//new Effect.Fade('titre' + old, {duration: 0.5, fps:25, from: 0.8, to: 0});
		Element.hide ('titre' + old);
		
		document.getElementById('famPetit' + old).style.display = "block";
	}
	
	Element.hide ('titre' + div);
	new Effect.Appear('fam' + div, {duration: 0.4, fps:25});
	setTimeout ("new Effect.Appear('titre" + div +"', {duration: 0.3, fps:25, from: 0, to: 0.8});", 400);
	
	old = div;
	
	div ++;
	
	if (div == 4){
		div = 0;
	}
	
	window.clearTimeout(timer);
	
	if (waitMore == true){
		timer = setTimeout('affALaUne('+ div +', true, false)', 10000);
	}else{
		timer = setTimeout('affALaUne('+ div +', true, false)', 5000);
	}
	
	// encien system
	
	//~ document.getElementById('famPetit' + div).style.display = "none";
	
	//~ if(clear == null)
	//~ {
		//~ if(div == 3)
			//~ aff = 0;
		//~ else
			//~ aff = div+1;
		
		//~ window.clearTimeout(timer);
		//~ for (var i=0; i<total; i++)
		//~ {
			//~ document.getElementById('fam' + i).style.display = "none";
			//~ document.getElementById('famPetit' + i).style.display = "block";
		//~ }
		
		//~ document.getElementById('famPetit' + div).style.display = "none";
		
		//~ timer = setTimeout('affALaUne('+ aff +', '+ div +', 5, false)',10000);
	//~ }
	//~ else
	//~ {
		//~ cpt++;
		//~ if(cpt == 4) cpt=0;
		//~ timer = setTimeout('affALaUne('+ cpt +', '+ div +', 5, false)',5000);
	//~ }
}

// Gestion du bloc "Pr�f�r�s"
function affPref(div)
{
	if (document.getElementById('pref0'))
	{
		for(var i=0; i<3; i++)
		{
			if(div == "pref" + i)
			{
				document.getElementById('pref' + i).className = 'prefT1';
				document.getElementById('prefBloc' + i).style.display = 'block';
				document.getElementById('scroll' + i).style.display = 'block';
			}
			else
			{
				document.getElementById('pref' + i).className = 'prefT2';
				document.getElementById('prefBloc' + i).style.display = 'none';
				document.getElementById('scroll' + i).style.display = 'none';
			}
		}
	}
}


// Gestion du scroll des blocs "Pr�f�r�s"
function scrollPref (div,sens)
{
	var item = document.getElementById(div);
	
	// SENS
	if(sens == "droite")
	{
		var h = item.scrollLeft - (item.style.width.replace(/[^0-9]/gi, '')/1);
		if (item.scrollWidth == h) item.scrollLeft = 0;
		
		item.scrollLeft-=2;
	}
	else
	{
		var h = item.scrollLeft + (item.style.width.replace(/[^0-9]/gi, '')/1);
		
		if (item.scrollWidth == h) item.scrollLeft = 0;
		
		item.scrollLeft+=2;
	}
	timeout = setTimeout('scrollPref("' + div + '","' + sens + '");', 1);
}

function stopScrollPref ()
{
	//~ clearTimeout(timeout);
	return;
}

// --------
// Register
// --------

function getPageSize(){
	
	var xScroll, yScroll;
	
	if (window.innerHeight && window.scrollMaxY) {	
		xScroll = document.body.scrollWidth;
		yScroll = window.innerHeight + window.scrollMaxY;
	} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
		xScroll = document.body.scrollWidth;
		yScroll = document.body.scrollHeight;
	} else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
		xScroll = document.body.offsetWidth;
		yScroll = document.body.offsetHeight;
	}
	
	var windowWidth, windowHeight;
	if (self.innerHeight) {	// all except Explorer
		windowWidth = self.innerWidth;
		windowHeight = self.innerHeight;
	} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
		windowWidth = document.documentElement.clientWidth;
		windowHeight = document.documentElement.clientHeight;
	} else if (document.body) { // other Explorers
		windowWidth = document.body.clientWidth;
		windowHeight = document.body.clientHeight;
	}	
	
	// for small pages with total height less then height of the viewport
	if(yScroll < windowHeight){
		pageHeight = windowHeight;
	} else { 
		pageHeight = yScroll;
	}

	// for small pages with total width less then width of the viewport
	if(xScroll < windowWidth){	
		pageWidth = windowWidth;
	} else {
		pageWidth = xScroll;
	}


	arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight) 
	return arrayPageSize;
}

function showRegister()
{
	var cache = document.getElementById('cache');
	var register = document.getElementById('register');
	
	var arrayPageSize = getPageSize();
	
	document.documentElement.style.overflow = 'hidden';
	cache.style.display = 'block';
	register.style.display = 'block';
	resizeRegister();
	register.getElementsByTagName('INPUT')[0].focus();
	
	cache.style.height = arrayPageSize[1] + 'px';
	cache.style.width = '100%';
}

function hideRegister()
{
	var cache = document.getElementById('cache');
	var register = document.getElementById('register');
	
	register.style.display = 'none';
	cache.style.display = 'none';
	document.documentElement.style.overflow = 'auto';
}

function resizeRegister()
{
	var register = document.getElementById('register');
	var w = document.documentElement.clientWidth;
	var h = document.documentElement.clientHeight;
	
	if (register.style.display == 'block')
	{
		register.style.left = ((w/2)-(register.clientWidth/2))+'px';
		register.style.top = ((h/2)-(register.clientHeight/2))+'px';
	}
}

window.onresize = resizeRegister;



// ================================
//	SCPE
// ================================

function zapNewsInit (div){
	var item = document.getElementById (div);
	
	var nbItem = item.getElementsByTagName ('div')[0].getElementsByTagName ('div').length;
	
		
		if(item.scrollLeft == (nbItem - 4) * 115)
		{
			document.getElementById('arrow_droite_'+div).src = 'http://www.entrevue.fr/images/arrow_droite.gif';
		}
		else
		{
			document.getElementById('arrow_droite_'+div).src = 'http://www.entrevue.fr/images/homeDroit-pref-prefBloc-fd.gif';
		}
		
		if(item.scrollLeft == 0)
		{
			document.getElementById('arrow_gauche_'+div).src = 'http://www.entrevue.fr/images/arrow_gauche.gif';
		}
		else
		{
			document.getElementById('arrow_gauche_'+div).src = 'http://www.entrevue.fr/images/homeDroit-pref-prefBloc-fg.gif';
		}
}

var scrollCount = 0;
var scrollSpeed;

function zapNews (div, fact){
	var item = document.getElementById (div);
	
	var nbItem = item.getElementsByTagName ('div')[0].getElementsByTagName ('div').length;
	
	if (fact < 0 && item.scrollLeft > 0 || fact > 0 && item.scrollLeft < (nbItem - 4) * 115){
		scrollSpeed = (120 - scrollCount) / 5;
		
		var inc = Math.floor (scrollSpeed)
		
		item.scrollLeft += inc * fact;
		
		scrollCount += inc;
		
		if (scrollCount < 115){
			timeout = setTimeout ('zapNews ("' + div + '",' + fact + ');', 30);
		}else{
			scrollCount = 0;
		}
		
		zapNewsInit(div);
	}
}

function deleteElementByID (id){
	if (document.getElementById(id)){
		document.getElementById(id).parentNode.removeChild(document.getElementById(id));
	}
}

function SendDatas3(page, mode, vars)
{

	var req;
	
	if (window.ActiveXObject)
	{
		req = new ActiveXObject("Microsoft.XMLHTTP");
		if (req)
			req.open(mode, page, true);
	}
	else
	{
		req = new XMLHttpRequest();
		try { req.open(mode, page, true); }
		catch(e) { alert("Problem Communicating with Server\n"+e); }
	}
	
	if (mode == 'POST')
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

	req.send(vars);
	
	return req;
}

function new_newsLetter (){
	var email = document.form_newsletter.user_email.value;

	var obj = SendDatas3('http://www.entrevue.fr/oren/new_newsletter.php', 'POST', 'email='+email);
	
	obj.onreadystatechange = function()
	{
		if (obj.readyState == 4)
		{
			deleteElementByID ('newsletter_message');
			
			var newMessage = document.createElement('div');
			newMessage.innerHTML = obj.responseText;
			newMessage.setAttribute ("id", "newsletter_message");
			newMessage.setAttribute ("class", "message");
			document.getElementById('newsletter_message_retour').appendChild (newMessage);
			
			document.getElementById('newsletter_message').className = 'message';
			
			Element.hide(newMessage);
			new Effect.BlindDown(newMessage, {duration:.5});
		}
	}
}



function newsLetter ()
{
	if(window.XMLHttpRequest) // FIREFOX
		xhr_object_newsLetter = new XMLHttpRequest();
	else if(window.ActiveXObject) // IE
		xhr_object_newsLetter = new ActiveXObject("Microsoft.XMLHTTP");
	
	//~ if(document.form_newsletter.newsletter.checked == false)
	//~ {
		//~ var newsletter = 0;
	//~ }
	//~ else
	//~ {
		var newsletter = 1;
	//~ }
	if(document.form_newsletter.sponsor.checked == false)
	{
		var sponsor = 0;
	}
	else
	{
		var sponsor = 1;
	}
		
	var email = document.form_newsletter.email_newsletter.value;
	var site 	= document.form_newsletter.site.value;
	
	document.form_newsletter.email_newsletter.value = '';
	
	xhr_object_newsLetter.open('get', 'http://www.entrevue.fr/oren/block_newsletter.php?email=' + email+'&newsletter='+newsletter+'&sponsor='+sponsor+'&site='+site, true);
	xhr_object_newsLetter.onreadystatechange = function()
	{
		if(xhr_object_newsLetter.readyState == 4)
    	{
        	if(xhr_object_newsLetter.status == 200)
			{
				deleteElementByID ('newsletter_message');
				
				var newMessage = document.createElement('div');
				newMessage.innerHTML = xhr_object_newsLetter.responseText;
				newMessage.setAttribute ("id", "newsletter_message");
				newMessage.setAttribute ("class", "newsletter_message");
				document.getElementById('newsletter_message_retour').appendChild (newMessage);
				
				document.getElementById('newsletter_message').className = 'newsletter_message';
				
				Element.hide(newMessage);
				new Effect.BlindDown(newMessage, {duration:.5});
				
			}

    	}
	};
	xhr_object_newsLetter.send(null);
}

function newsLetterSimple (n)
{
	if(window.XMLHttpRequest) // FIREFOX
		xhr_object_newsLetter = new XMLHttpRequest();
	else if(window.ActiveXObject) // IE
		xhr_object_newsLetter = new ActiveXObject("Microsoft.XMLHTTP");
		
	var site 	= document['form_newsletter_simple'+n]['site'+n].value;
	var email = document['form_newsletter_simple'+n]['email_newsletter'+n].value;

	xhr_object_newsLetter.open('get', 'http://www.entrevue.fr/oren/block_newsletter.php?email=' + email+'&newsletter=1&sponsor=0&site='+site, true);
	xhr_object_newsLetter.onreadystatechange = function()
	{
		if(xhr_object_newsLetter.readyState == 4)
    	{
        	if(xhr_object_newsLetter.status == 200)
			{
				deleteElementByID ('newsletter_message_simple');
				
				var newMessage = document.createElement('div');
				newMessage.innerHTML = xhr_object_newsLetter.responseText;
				newMessage.setAttribute ("id", "newsletter_message_simple");
				newMessage.setAttribute ("class", "newsletter_message");
				document.getElementById('newsletter_message_retour_simple'+n).appendChild (newMessage);
				
				document.getElementById('newsletter_message_simple').className = 'newsletter_message_simple';
				
				Element.hide(newMessage);
				new Effect.BlindDown(newMessage, {duration:.5});
				
			}

    	}
	};
	xhr_object_newsLetter.send(null);
}

function getXhr()
{
	var xhr = null; 
	if(window.XMLHttpRequest)
	{
		xhr = new XMLHttpRequest(); 
	}
	else if(window.ActiveXObject)
	{ 
		try {
			xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	else 
	{
		 alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
		 xhr = false; 
	} 
	return xhr;
}

function sendFriend ()
{
	var XhrObj = getXhr();

	var email  = document.form_friend.email.value;
	var auteur = document.form_friend.auteur.value;
	var titre  = document.form_friend.titre.value;
	var url	   = document.form_friend.url.value;
	
	XhrObj.open('get', 'http://www.entrevue.fr/sendfriend.php?email=' + email+'&titre='+titre+'&url='+url+'&auteur='+auteur, true);
	XhrObj.onreadystatechange = function()
	{
		if (XhrObj.readyState == 4 && XhrObj.status == 200)
		{
			deleteElementByID ('ajax_message');
			
			var newMessage = document.createElement('div');
			newMessage.innerHTML = XhrObj.responseText;
			newMessage.setAttribute ("id", "ajax_message");
			newMessage.setAttribute ("class", "message");
			document.getElementById('ami_message_retour').appendChild (newMessage);
			
			document.getElementById('ajax_message').className = 'message';
			
			Element.hide(newMessage);
			new Effect.BlindDown(newMessage, {duration:.5});
		}
	};
	XhrObj.send(null);
}

// ================================
//	MOTEUR DE RECHRECHE
// ================================

function RechercheRub(rubrique)
{
	document.getElementById('lien_articles').style.fontWeight = 'normal';
	document.getElementById('lien_videos').style.fontWeight = 'normal';
	document.getElementById('lien_images').style.fontWeight ='normal';
	
	switch(rubrique)
	{
		case 'articles':
			document.getElementById('lien_articles').style.fontWeight = 'bold';
			document.getElementById('type_recherche').value = 1;
		break;
		
		case 'videos':
			document.getElementById('lien_videos').style.fontWeight = 'bold';
			document.getElementById('type_recherche').value = 2;
		break;

		case 'images':
			document.getElementById('lien_images').style.fontWeight ='bold';
			document.getElementById('type_recherche').value = 3;
		break;
	}
}



// VOTE

var voted = 0;

function setScore (score, hover){
	if (voted == 0){
		if (hover){
			for (n = 1; n < 6; n ++) {
				if (n <= score){
					document.getElementById('score_' + n).src = 'http://www.entrevue.fr/images/star-survol.gif';
				}else{
					document.getElementById('score_' + n).src = 'http://www.entrevue.fr/images/star-off.gif';
				}
			}
		}else{
			for (n = 1; n < 6; n ++) {
				if (n <= score){
					document.getElementById('score_' + n).src = 'http://www.entrevue.fr/images/star.gif';
				}else{
					document.getElementById('score_' + n).src = 'http://www.entrevue.fr/images/star-off.gif';
				}
			}
		}
	}
}

function vote (path, data, score){
	setScore (score, 0);
	
	new Ajax.Request(path, {
		method: 'get',
		parameters: data
	});
	
	document.location.reload();
}





// Function DateExpiration
// fixe la date d'expiration
function DateExpiration (DateExp) {

      var aujourdhui = new Date(0);
      var aujourdhuidate = aujourdhui.getTime();
      if (aujourdhuidate > 0)
            DateExp.setTime (DateExp.getTime() - aujourdhuidate);

}

// Function FixeCookie
// pour cr�er ou modifier un cookie
function FixeCookie (nom,valeur,expire,path,domaine,securise) {

      document.cookie = nom + "=" + escape (valeur) + ((expire) ? "; expires=" + expire.toGMTString() : "") + ((path) ? "; path=" + path : "") + ((domaine) ? "; domain=" + domaine : "") + ((securise) ? "; secure" : "");

}

// Function SupprCookie
// Supprime un Cookie
function SupprCookie (nom,path,domaine) {

      if (GetValeurCookie(nom)) {
            document.cookie = nom + "=" + ((path) ? "; path=" + path : "") + ((domaine) ? "; domain=" + domaine : "") + "; expires=Thu, 01-Jan-70 00:00:01 GMT";
      }

}

// Function ValeurCookie
// Utiliser par GetValeurCookie
function ValeurCookie (Pos) {

      var endstr = document.cookie.indexOf (";", Pos);
      if (endstr == -1)
            endstr = document.cookie.length;
      return unescape(document.cookie.substring (Pos, endstr));

}

// Function GetValeurCookie
// pour r�cup�rer la valeur d'un cookie
function GetValeurCookie (nom) {

      var cookielength = document.cookie.length;
      var arg = nom + "=";
      var arglength = arg.length;
      var i = 0;
      while (i < cookielength) {
      var j = i + arglength;
      if (document.cookie.substring(i, j) == arg)
            return ValeurCookie (j);
            i = document.cookie.indexOf(" ", i) + 1;
            if (i == 0) break;
      }
      return null;

}

SupprCookie ('__utmz', '/', '.entrevue.fr');
SupprCookie ('__utma', '/', '.entrevue.fr');
SupprCookie ('__utmc', '/', '.entrevue.fr');



// ================================
//	LECTURE DOSSIER
// ================================

var DossierTimer;

function LireDossier(link_dossier, page, nb_page) 
{
	if(page < (nb_page-1))
	{
		window.location.href = link_dossier+page+'-1.html#dossier';
	}
	else
	{
		StopDossier(link_dossier, page);
	}
}

function StopDossier(link_dossier, page) 
{
	window.location.href = link_dossier+page+'.html#dossier';
	
	window.clearTimeout(DossierTimer);
}