var window_handle='';

    function openTab(page)
    {	    
		    window_handle = window.open(page,'');
		    window_handle.focus(); // focus sur popup
    }

    function openPopUp(page, name)
    {	    
		    window_handle = window.open(page, name);
		    window_handle.focus(); // focus sur popup
    }

	function openPopUp(page,name,width,height,scrollbars,resizable,status)
	{	    
		window_handle = window.open(page, name, "scrollbars=" + scrollbars + ",status=" + status +",resizable=" + resizable +",width=" + width + ",height=" + height);
		window_handle.focus(); // focus sur popup
	}
	
	function openPopUp(page,name,width,height,scrollbars,resizable,status,menubar)
	{	    
		var windowOption = "";
		
		windowOption = "scrollbars=" + scrollbars 
						+ ", status=" + status 
						+ ", resizable=" + resizable 
						+ ", menubar=" + menubar 
						+ ", width=" + width 
						+ ", height=" + height
						+ "";
		window_handle = window.open(page, name, windowOption);
		window_handle.focus(); // focus sur popup
	}
	
    function openPopUp(page,name,width,height,scroll,resize)
    {	    
		    var windowOption = "width=" + width 
						    + ", height=" + height
						    + ", scrollbars=" + scroll 
						    + ", resizable=" + resize 
						    + ",status=1";

		    window_handle = window.open(page, name, windowOption);
		    window_handle.focus(); // focus sur popup
    }

	function openPopUp(page,name,width,height,left,top,scrollbars,resizable,status,menubar)
	{	    
	    if((width == "") || (width <= 0))
        {
            width = 800;
        }    
        if((height == "") || (height <= 0))
        {
            height = 600;
        }   
        if(left == "")
        {
            left = 0;
        } 
        if(top == "")
        {
            top = 0;
        } 

		var windowOption = "";
		windowOption = "width=" + width 
						+ ", height=" + height
						+ ", left=" + left
						+ ", top=" + top
						+ ", scrollbars=" + scrollbars 
						+ ", status=" + status 
						+ ", resizable=" + resizable 
						+ ", menubar=" + menubar 
						+ "";

		window_handle = window.open(page, name, windowOption);
		window_handle.focus(); // focus sur popup
   	}

	function openMenuPopUp(page, name)
    {	    
		    window_handle = window.open(page, name, "width=800,height=600,modal,toolbar=false,location=true,directories=false,status=no,menubar=no,scrollbars=no,resizable=no,modal=yes");
		    window_handle.focus(); // focus sur popup
    }
