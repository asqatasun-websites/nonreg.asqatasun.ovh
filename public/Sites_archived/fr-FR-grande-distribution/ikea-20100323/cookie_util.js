/**************************************************************************** 
	The following functions are used to get, set and delete cookies
	Note: cookies are found (using getCookie) from subdomains (i.e. 
	*.domain.com) and subpaths (i.e. /path/to/calling/path/*).
	When deleting cookies, the path and domain must be the same as when set.
	
	Created by Klas Skogmar 2004-01-16
*****************************************************************************/

/**
	name - the name of the cookie. Only one cookie with the same name can exist.
	value - the value of the cookie
	path - the path (and subpaths) that the cookie should be valid for
	domain - the domain and subdomains that the cookie should be valid for
	expires - the time when the cookie will expire
*/
function setCookie(name, value, expires, path, domain, secure) {
	if(!expires) { expires = new Date(); expires.setDate(expires.getDate()+365); }
	document.cookie = name + "=" + escape(value) +
      "; expires=" + expires.toGMTString() +
      ((path) ? "; path=" + path : "") +
      ((domain) ? "; domain=" + domain : "") +
      ((secure) ? "; secure" : "");
}
function deleteCookie(name, path, domain) {
  if (getCookie(name)) {
    document.cookie = name + "=" + 
    ((path) ? "; path=" + path : "") +
    ((domain) ? "; domain=" + domain : "") +
    "; expires=Thu, 01-Jan-70 00:00:01 GMT";
  }
}
function getCookie(name) {
	var cookie_start = document.cookie.indexOf(name + "=");
	if(cookie_start == -1) return null;
	var cookie_end = document.cookie.indexOf("; ", cookie_start);
	if(cookie_end == -1) cookie_end = document.cookie.length;
	return unescape(document.cookie.substring(cookie_start + name.length + 1, cookie_end));
}

function getCookiesEnabled(message) {
	var cookieEnabled = (navigator.cookieEnabled)?true:false

	if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled){ 
	document.cookie = "dummy"
	cookieEnabled = (document.cookie.indexOf("dummy")!= -1)?true:false
	}
	
	if (!cookieEnabled) { 
		alert(message)
		return false
	}
	return true
}

 function decode(utftext) {
    var string = "";
    var i = 0;
    var c = c1 = c2 = 0;

    while ( i < utftext.length ) {

        c = utftext.charCodeAt(i);

        if (c < 128) {
            string += String.fromCharCode(c);
            i++;
        }
        else if((c > 191) && (c < 224)) {
            c2 = utftext.charCodeAt(i+1);
            string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
            i += 2;
        }
        else {
            c2 = utftext.charCodeAt(i+1);
            c3 = utftext.charCodeAt(i+2);
            string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }
    }
    return string;
 }
  
  
function getDecodedCookie(cName){
	
	var cString = getCookie(cName);
	
	if ((cString==null)) {
      return null;
	}
	
	return decode(cString);
}  
    