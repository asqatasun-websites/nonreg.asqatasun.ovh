function initCaract(){
   if(document.getElementById("layerArticles")){
      document.getElementById("layerArticles").className = "displayNone";
		document.getElementById("showArticles").className = "linkInActif";
      var showArt = document.getElementById("showArticles");
      addEvent(showArt, 'click', displayArt, false);
      showArt.href = "javascript:return false;";
   }
}

function displayArt(){
	if(document.getElementById("layerArticles").className == 'displayNone'){
		document.getElementById("layerArticles").className = 'displayArt';
		document.getElementById("showArticles").className = 'linkActif';
	}
	else{
			document.getElementById("layerArticles").className = 'displayNone';
			document.getElementById("showArticles").className = 'linkInActif';
	}
	return(false);
}

//initialisation au chargement de la page
addLoadListener(initCaract);