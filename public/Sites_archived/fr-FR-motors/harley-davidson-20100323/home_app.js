/**
 * @author Stan Carrico, WHITTMANHART Interactive
 * July, 2009
 * chagelog :: 	07/26 - updated get promos function //SC
 * 				10/27 - removed getPromos(), flash now reads page source //.dm
 *				2010/01/08 - created INT version
 */
 
 
 
home.app = {

	get : function (el) {
		if (YAHOO.env.ua.ie > 0) {
				var origText = document.getElementById(el).innerHTML;
				var exp = /(\w+[a-zA-Z]+=[^'"><\s]+)/mg;
				
				try{
					result = origText.replace(exp,function(e){
						var newStr = e.split('=');
						//console.log(newStr);
						var strAttribute = newStr[0] + '="' + newStr[1] + '"';
						return strAttribute;
					});
					var exp2 = /(<\w*[^\s])/mig;
					result2 = result.replace(exp2,function(e){
						var le = e.toLowerCase();
						return le;						
					});
					
					var exp3 = /(<\/\w+>)/mig;
					result3 = result2.replace(exp3,function(e){
						var le = e.toLowerCase();
						return le;						
					});
					
					 var exp4 = /<img[^<]+?>/mig;
					 result4 = result3.replace( exp4, "$&</img>" );										
				}
				catch(e){
					//console.log(e);
				}
				return result4;
		}
		else if(YAHOO.env.ua.opera > 0){
			//lower case tags for opera
					var origTextb = document.getElementById(el).innerHTML;
					
					//lower case open tags
					var expb = /(<\w*[^\s])/mig;
					resultb = origTextb.replace(expb,function(e){
						var le = e.toLowerCase();
						return le;						
					});
					
					//lower case close tags
					var exp2b = /(<\/\w+>)/mig;
					result2b = resultb.replace(exp2b,function(e){
						var le = e.toLowerCase();
						return le;						
					});
					var exp3b = /<img[^<]+?>/mig;
					result3b = result2b.replace( exp3b, "$&</img>" );
				
					return result3b;
			
		}
		else {
			
			var origTextc = document.getElementById(el).innerHTML;
			var exp4c = /<img[^<]+?>/mig;
			resultc = origTextc.replace( exp4c, "$&</img>" );
			return resultc;
		}
	},
	
	getBody : function (){
		return docBody;		
	},
	
	/**
	 * This method fires links from events passed in from flash. It receies an index value that 
	 * corrresponds to the position of the link array created on page load.
	 * 
	 * @method getLink
	 * @param {String} index value in link array on page load
	 * @param {String} view is what is visible in flash when link is clicked
	 * @param {String} xPos is X position of click (within flash area)
	 * @param {String} yPos is Y position of flash (within flash area)
	 */
	getLink : function (index,view,xPos,yPos){
		
		var name = linkArray[index].name;
		var href = linkArray[index].href;
		var onclick = linkArray[index].onclick;
		var target = linkArray[index].target;
		
		script = /javascript/;
		tgt = /_blank/;
		
		switch(script.test(href)){
			case true:
				onclick();
			break;
			
			case false:
				switch(tgt.test(target)){
					case true:
						window.open(href);
					break;
					case false:
						document.location = href;
					break;
				}
			break;
		}
		home.app.track(name,view,xPos,yPos,"4","4");
	},
	/**
	 * This method tracks both calls to the link function and clicks to the promo area in the main page.
	 *
	 * @method track
	 * @param {String} name attribute of anchor
	 * @param {String} view visible in flash or promo id clicked
	 * @param {String} xPos is X coordinate of flash click
	 * @param {String} yPos is Y coordinate of flash click
	 * @param {String} clickW is width of clicked area (defaults to 4 from link function)
	 * @param {String} clickH is height of clicked area (defaults to 4 from link function)
	 */
	track : function (name,view,xPos,yPos,clickW,clickH){
		//hardcode click region size
		
		flashX = YUD.getX('home_flash_app');
		flashY = YUD.getY('home_flash_app');
		
		clickX = String(Number(xPos) + Number(flashX));
		clickY = String(Number(yPos) + Number(flashY));
		
		//concat flash view with link name attribute
		strView = view + ":" + name;
		
		_hbLink(strView, '', clickX + "," + clickY + "," + clickW + "," + clickH);

	}
	
};

