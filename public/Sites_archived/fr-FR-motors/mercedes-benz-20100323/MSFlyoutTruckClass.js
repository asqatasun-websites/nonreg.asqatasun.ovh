/**
 * The MSFlyoutTruck Class handles the Flyout Menus on the website.
 * It offers timeout values that can be different for each
 * flyout type. This class depends on the daimler_basic.js!
 * 
 * @author Stefan.Bechtold(at)namics.com
 * @version 1.0
 * @requires daimler_basic.js
 * @requires navigation.js
 */

// Class MSFlyoutTruck, Constructor
function MSFlyoutTruck(/*String*/ id, /*int*/ type, currentActive) {
	this.id = id;
	this.type = type;
	this.active = false;
	this.currentActive = currentActive;
	
	// Get Layer for this Flyout
	if (id) {
		this.flyout = getLayer(id);
	}

	// Check and extend global MSFlyoutTruck list if necessary
	if (!MSFlyoutTruck.flyouts[type]) {
		MSFlyoutTruck.flyouts[type] = new Array();
	}

	// Add flyout to global MSFlyoutTruck object
	MSFlyoutTruck.flyouts[type][id] = this;
	
	// Add event listener for this flyout
	if (this.flyout && type==MSFlyoutTruck.TYPE_CORENAV1) {
		addEvent(this.flyout, "mouseover", function() { MSFlyoutTruck.flyouts[type][id].activate() }, true);
		addEvent(this.flyout, "mouseout", function() { MSFlyoutTruck.flyouts[type][id].deactivate() }, true);
	}
	else if (this.flyout && type==MSFlyoutTruck.TYPE_CORENAV2){
		addEvent(this.flyout, "mouseover", function() { MSFlyoutTruck.flyouts[type][id].activateSecond() }, true);
		addEvent(this.flyout, "mouseout", function() { MSFlyoutTruck.flyouts[type][id].deactivateSecond() }, true);
	}
}

// Static Flyout Types
MSFlyoutTruck.TYPE_CORENAV1 = 1;
MSFlyoutTruck.TYPE_CORENAV2 = 2;

// Static Timeout Settings
MSFlyoutTruck.ACTIVATION_TIMEOUT = new Array();
MSFlyoutTruck.ACTIVATION_TIMEOUT[MSFlyoutTruck.TYPE_CORENAV1] = 200;
MSFlyoutTruck.ACTIVATION_TIMEOUT[MSFlyoutTruck.TYPE_CORENAV2] = 200;

MSFlyoutTruck.DEACTIVATION_TIMEOUT = new Array();
MSFlyoutTruck.DEACTIVATION_TIMEOUT[MSFlyoutTruck.TYPE_CORENAV1] = 200;
MSFlyoutTruck.DEACTIVATION_TIMEOUT[MSFlyoutTruck.TYPE_CORENAV2] = 200;

// Static CSS Settings
MSFlyoutTruck.CSS_CLASS_ACTIVE = "ms-active";
MSFlyoutTruck.CSS_CLASS_HOVER = "ms-fly-hover";

// Static Vars
MSFlyoutTruck.flyouts = new Array();
MSFlyoutTruck.openFlyout = new Array();
MSFlyoutTruck.changeTimeout = new Array();
MSFlyoutTruck.closeTimeout = new Array();

MSFlyoutTruck.secondIsOn = -1;

// Static Methods
MSFlyoutTruck.change = function(/*String*/ id, /*int*/ type) {
	// Close open MSFlyoutTruck Menu
	if (MSFlyoutTruck.openFlyout[type]) {
		MSFlyoutTruck.openFlyout[type].handleDeactivate();
	}
	
	// Open new MSFlyoutTruck Menu
	if (MSFlyoutTruck.flyouts[type][id]) {
		MSFlyoutTruck.flyouts[type][id].handleActivate();
	}
}

MSFlyoutTruck.closeAll = function(/*int*/ type) {
	// Close all open MSFlyoutTruck Menus
	for (var flyout in MSFlyoutTruck.flyouts[type]) {
		if (MSFlyoutTruck.flyouts[type][flyout].active) {
			MSFlyoutTruck.flyouts[type][flyout].handleDeactivate();
		}
	}
	
	// Reset openFlyout Reference
	MSFlyoutTruck.openFlyout[type] = undefined;
}

MSFlyoutTruck.closeAllSecond = function(/*int*/ type) {
	// Close all open MSFlyoutTruck Menus
	for (var flyout in MSFlyoutTruck.flyouts[type]) {
		document.getElementById(flyout).style.display = "none";
	}
}

MSFlyoutTruck.pushClass = function(object, className) {
	var /*String*/ objClasses = getClassName(object);
	if (objClasses.indexOf(className) == -1) {
		objClasses += " " + className;
	}
	setClass(object, objClasses);
}

MSFlyoutTruck.popClass = function(object, className) {
	var /*String*/ objClasses = getClassName(object);
	var /*int*/ posClassName = objClasses.indexOf(className);
	if (posClassName != -1) {
		if (posClassName + className.length < objClasses.length) {
			objClasses = objClasses.substring(0, posClassName) + objClasses.substring(posClassName + className.length);
		} else {
			objClasses = objClasses.substring(0, posClassName);
		}
	}
	if(MSFlyoutTruck.secondIsOn < 0){
		setClass(object, objClasses);
	}
}



// Methods
MSFlyoutTruck.prototype.activate = function() {
	// Clear Closing Timeout
	MSFlyoutTruck.secondIsOn = -1;
	
	clearTimeout(MSFlyoutTruck.changeTimeout[this.type]);
	clearTimeout(MSFlyoutTruck.closeTimeout[this.type]);
	
	if (MSFlyoutTruck.openFlyout[this.type] == undefined) {
		// If no MSFlyoutTruck Menu is open, strictly open the current one
		MSFlyoutTruck.openSecondLevel(this.id);
		this.handleActivate();
	} else if (MSFlyoutTruck.openFlyout[this.type] != this) {
		MSFlyoutTruck.closeTimeout[this.type] = setTimeout("MSFlyoutTruck.openSecondLevel(\"" + this.id + "\")", MSFlyoutTruck.DEACTIVATION_TIMEOUT[this.type]);
		MSFlyoutTruck.changeTimeout[this.type] = setTimeout("MSFlyoutTruck.change(\"" + this.id + "\", \"" + this.type + "\")", MSFlyoutTruck.ACTIVATION_TIMEOUT[this.type]);
	}
}

MSFlyoutTruck.openSecondLevel = function(elemId){
	MSFlyoutTruck.closeAllSecond(MSFlyoutTruck.TYPE_CORENAV2);
	var mom = document.getElementById(elemId);
	var kids = mom.getElementsByTagName("div");
	for(var i = 0; i < kids.length; i++){
		kids[i].style.display = "block";
	}
}

MSFlyoutTruck.prototype.activateSecond = function() {
	MSFlyoutTruck.secondIsOn = 1;
}

MSFlyoutTruck.prototype.deactivateSecond = function() {
	MSFlyoutTruck.secondIsOn = -1;
	//document.getElementById(this.id).parentNode, MSFlyoutTruck.CSS_CLASS_HOVER);
	//showCorrectTruckNavi();
}

MSFlyoutTruck.prototype.handleActivate = function() {
	// Activate MSFlyoutTruck
	this.active = true;
	MSFlyoutTruck.pushClass(this.flyout, MSFlyoutTruck.CSS_CLASS_HOVER);
	
	// Set openFlyout Reference
	MSFlyoutTruck.openFlyout[this.type] = this;
}

MSFlyoutTruck.prototype.deactivate = function() {
	// Set Closing Timeout
	MSFlyoutTruck.closeTimeout[this.type] = setTimeout("MSFlyoutTruck.closeAll(\"" + this.type + "\")", MSFlyoutTruck.DEACTIVATION_TIMEOUT[this.type]);
	MSFlyoutTruck.changeTimeout[this.type] = setTimeout("MSFlyoutTruck.changeSecondLevel()", MSFlyoutTruck.DEACTIVATION_TIMEOUT[this.type]);
}

MSFlyoutTruck.changeSecondLevel = function(){
	MSFlyoutTruck.closeAllSecond(MSFlyoutTruck.TYPE_CORENAV2);
	var count = 0;
	for (var flyout in MSFlyoutTruck.flyouts[MSFlyoutTruck.TYPE_CORENAV2]) {
		if (MSFlyoutTruck.flyouts[MSFlyoutTruck.TYPE_CORENAV2][flyout].currentActive) {
			document.getElementById(flyout).style.display = "block";
			count++;
		}
	}
	if(count < 1){
		showCorrectTruckNavi();
	}
}

MSFlyoutTruck.prototype.handleDeactivate = function() {
	// Deactivate MSFlyoutTruck
	this.active = false;
	MSFlyoutTruck.popClass(this.flyout, MSFlyoutTruck.CSS_CLASS_HOVER);
}
