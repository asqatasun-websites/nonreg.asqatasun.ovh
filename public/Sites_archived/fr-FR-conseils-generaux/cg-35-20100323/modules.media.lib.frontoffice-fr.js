var ie = (document.defaultCharset && document.getElementById && !window.home);

if (ie){
	document.write ("<style id='hideObject'> object{display:none;} </style>");
}

objectSwap = function(){
	if (!document.getElementsByTagName){
		return;
	}
	var stripQueue = [];
	var objects = document.getElementsByTagName('object');
	for (var i=0; i<objects.length; i++){
		var o = objects[i];
		var h = o.outerHTML;
		var params = "";
		var hasFlash = true;
		for (var j = 0; j<o.childNodes.length; j++) {
			var p = o.childNodes[j];
			if (p.tagName == "PARAM"){
				if (p.name == "flashVersion"){
					hasFlash = detectFlash(p.value);
					if (!hasFlash){
						o.id = (o.id == "") ? ("stripFlash"+i) : o.id;
						stripQueue.push(o.id);
						break;
					}
				}
				params += p.outerHTML;
			}
		}
		if (!hasFlash){
			continue;
		}
		if (!ie){
			continue;
		}
		if (o.className.toLowerCase().indexOf ("noswap") != -1){
			continue;
		}
		var tag = h.split(">")[0] + ">";
		var newObject = tag + params + o.innerHTML + " </OBJECT>";
		o.outerHTML = newObject;
	}
	if (stripQueue.length) {
		stripFlash(stripQueue)
	}
	if (ie){
		document.getElementById("hideObject").disabled = true;
	}
}

detectFlash = function(version){
	if(navigator.plugins && navigator.plugins.length){
		var plugin = navigator.plugins["Shockwave Flash"];
		if (plugin == undefined){
			return false;
		}
		var ver = navigator.plugins["Shockwave Flash"].description.split(" ")[2];
		return (Number(ver) >= Number(version))
	} else if (ie && typeof (ActiveXObject) == "function"){
		try{
			var flash = new ActiveXObject("ShockwaveFlash.ShockwaveFlash." + version);
			return true;
		}
		catch(e){
			return false;
		}
	}
	return true;
}

stripFlash = function (stripQueue){
	if (!document.createElement){
		return;
	}
	for (var i=0; i<stripQueue.length; i++){
		var o = document.getElementById(stripQueue[i]);
		var newHTML = o.innerHTML;
		newHTML = newHTML.replace(/<!--\s/g, "");
		newHTML = newHTML.replace(/\s-->/g, "");
		newHTML = newHTML.replace(/<embed/gi, "<span");
		var d = document.createElement("div");
		d.innerHTML = newHTML;
		d.className = o.className;
		d.id = o.id;
		o.parentNode.replaceChild(d, o);
	}
}

try
{
    $(document).ready(function(){objectSwap();});
}
catch(e)
{
    var tempFunc = window.onload;
    window.onload = function(){
    	if (typeof (tempFunc) == "function"){
    		try{
    			tempFunc();
    		} catch(e){}
    	}
    	objectSwap();
    }
}

$(document).ready(function() {
    try
    {
    	zoom.setup();
    	$('img[@src$=.png],div').ifixpng(); 
   	}
   	catch (e)
   	{
   	}
});


var zoom = {
	oldbg: null,
	topoffset: 25,
	leftoffset: 0,
	minheight: 50,
	minwidth: 50,
	securitybelt: 60,
	containerpadding: 20,
	opening: false,
	setup: function () {
		$("img.image").bind("click", function() { zoom.initialize(this); });
	    $("#media-zoom-overlay").bind("click", function() { zoom.hide(); });
	},
	computeTop: function (height) {		
		return (($(window).innerHeight() - height - this.topoffset - this.containerpadding) / 2);
	},	
	computeLeft: function (width) {
		return (($(window).innerWidth() - width - this.leftoffset) / 2);
	},	
	getZoomUrl: function (cmpref) {
		return "/index.php?module=media&action=Display&cmpref=" + cmpref + "&width=" + ($(window).innerWidth() - this.securitybelt) + "&height=" + ($(window).innerHeight() - this.securitybelt);
	},
	initialize: function (current, force) {
		if (!this.opening && (force || !$(current).parents().is("a")) && $(current).attr("id") && $(current).attr("src").match(/formatted/))
		{
		    var idMatch = $(current).attr("id").match(/(\d+)/);
		    if (parseInt(idMatch[1]) > 0)
		    {
		    	this.opening = true;		    	
		        if (!$("#media-zoom-overlay").length)
                {
                	$(new Image()).attr("src", "/icons/admin/loading.gif");                	
                    $('<div id="media-zoom-overlay"><span id="media-zoom-overlay-label">Cliquez pour fermer</span></div>').appendTo("body").css("opacity", 0).hide().bind("click", function() { zoom.hide(); });
                }                
                if ($.browser.msie)
		        {
		        	$(new Image()).load(function () { zoom.openForDummies(this); }).attr("src", this.getZoomUrl(parseInt(idMatch[1])));
		        }
		        else
		        {
		        	$(new Image()).load(function () { zoom.open(this); }).attr("src", this.getZoomUrl(parseInt(idMatch[1])));
		        }
		        this.oldbg = $("#media-zoom-overlay").css("background-color");
		        $("#media-zoom-overlay-label").hide();
		        $("#media-zoom-overlay").css("background", "url(/icons/admin/loading.gif) center no-repeat");
		        if (!$.browser.msie)
		        {
			        $("#media-zoom-overlay").animate(
	                    {
	                        top: this.computeTop(this.minheight)  + "px",
	                        left: this.computeLeft(this.minwidth)  + "px",
	                        width: this.minwidth + (this.containerpadding * 2) + "px",
	                        height: this.minheight + (this.containerpadding * 2) + "px",
	                        opacity: 1
	                    },
	                    {
	                        duration: 200,
	                        easing: "easeInOutExpo"
	                    }
	                );
	            }	            
                $("#media-zoom-overlay-image").animate(
                    {
                        opacity: 0
                    },
                    {
                        duration: 200,
                        complete: function () { zoom.destroy()},
	                        easing: "easeInOutExpo"
                    }
                );
		    }
		}
	},
	openForDummies: function (current) {
		if ($("#media-zoom-overlay").length && current.width && current.height && this.oldbg)
		{
			this.open(current);
		}
		else
		{
			window.setTimeout(function () { zoom.openForDummies(current);}, 100);
		}	
	},
	open: function (current) {
		$("#media-zoom-overlay")
			.css("background", this.oldbg)
			.animate(
    	    {
                top: this.computeTop(current.height)  + "px",
                left: this.computeLeft(current.width)  + "px",
                width: current.width + (this.containerpadding * 2) + "px",
                height: current.height + (this.containerpadding * 2) + "px",
                opacity: 1
            },
            {
                duration: 500,
                complete: function () { zoom.show(current)},
	                        easing: "easeInOutExpo"
            }
        );
	},
	show: function (current) {
		$("#media-zoom-overlay").css("background", this.oldbg);
	    if (!$("#media-zoom-overlay-image").length)
        {
            $('<img id="media-zoom-overlay-image" />').appendTo("body").bind("click", function() { zoom.hide(); });
        }
        $("#media-zoom-overlay-image")
            .attr("src", current.src)
            .css("opacity", 0)
            .css("top", this.computeTop(current.height) + this.containerpadding + "px")
            .css("left", this.computeLeft(current.width)  + this.containerpadding + "px")
            .animate(
                {
                    opacity: 1
                },
                {
                    duration: 300,
	                        easing: "easeInOutExpo"
                }
            );
         $("#media-zoom-overlay-label").show();
         this.opening = false;
	},
	hide: function (current) {
		$("#media-zoom-overlay-label").hide();
		$("#media-zoom-overlay-image")
            .animate(
                {
                    opacity: 0
                },
                {
                    duration: 200,
                    complete: function () { zoom.destroy()},
	                        easing: "easeInOutExpo"
                }
            );
	    $("#media-zoom-overlay").animate(
	       {
                top: this.computeTop(this.minheight)  + "px",
                left: this.computeLeft(this.minwidth)  + "px",
                width: this.minwidth + (this.containerpadding * 2) + "px",
                height: this.minheight + (this.containerpadding * 2) + "px",
                opacity: 0
            },
            {
                duration: 200,
                complete: function () { zoom.close()},
	                        easing: "easeInOutExpo"
            }
        );
	},
	close: function () {
	    $("#media-zoom-overlay").hide();
	},
	destroy: function () {
	    $("#media-zoom-overlay-image").remove();
	}
};