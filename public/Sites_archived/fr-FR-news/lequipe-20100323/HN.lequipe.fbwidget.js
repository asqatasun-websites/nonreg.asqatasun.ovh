if(!window.HN)var HN=window.HN={};
var userAgent=navigator.userAgent.toLowerCase();
HN.browser={
version:(userAgent.match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/)||[])[1],
safari:/webkit/.test(userAgent),
opera:/opera/.test(userAgent),
msie:/msie/.test(userAgent)&&!/opera/.test(userAgent),
mozilla:/mozilla/.test(userAgent)&&!/(compatible|webkit)/.test(userAgent)
};
HN.lequipe={};
HN.lequipe.locals={};
HN.lequipe.locals.base_url="/facebook/";
HN.lequipe.CM=new HN.CookieManager();
HN.lequipe.fbwidget={
minWidth:1000,
marginsWidth:21,
contentBordersWidth:1,
maskUnfoldedWidth:0,
contentUnfoldedWidth:0,
maskFoldedWidth:180,
contentFoldedLeft:-1000,
contentFoldedWidth:1179,
contentUnfoldedLeft:0,
resizeInterval:null,
outWidth:null,
connectedOnce:false,
inputStatusFocusedOnce:false,
liveFeedShowedOnce:false,
profileLoaded:false,
lastUpdatedFriendStatusLoaded:false,
browserBoxShowed:false,
optionsBoxShowed:false,
statusBoxShowed:false,
friendsBoxShowed:false,
liveFeedShowed:false,
dockFolded:true,
dockShowed:true,
onConnected:function(user_id){
if(!HN.lequipe.fbwidget.connectedOnce){
HN.lequipe.fbwidget.connectedOnce=true;
FB.Facebook.get_sessionState().waitUntilReady(function(){
$("#fb-widget").show();
HN.lequipe.fbwidget.loadProfile(function(){HN.lequipe.fbwidget.TryUnfoldDock();});
HN.lequipe.fbwidget.getLastUpdatedFriendStatus(function(){HN.lequipe.fbwidget.TryUnfoldDock();});
$("#fb-widget .fb-logo").click(function(){
if(HN.lequipe.fbwidget.optionsBoxShowed)
HN.lequipe.fbwidget.hideOptions();
else
HN.lequipe.fbwidget.showOptions();
});
$("#fb-widget .fb-options-box-close").click(function(){HN.lequipe.fbwidget.hideOptions();});
$("#fb-widget .fb-close-dock-link").click(function(){
HN.lequipe.fbwidget.hideDock();
if($("#fb_etat"))
{
$("#fb_etat").html("<a href=\"javascript: HN.lequipe.fbwidget.showDock();\"><img src=\"/img_v5/FBConnect_light_large_short.gif\" title='Ouvrir le dock facebook / lequipe.fr' /></a>");
}
});
$("#fb-widget .fb-disconnect-link").click(function(){FB.Connect.logoutAndRedirect(window.location.href);});
$("#fb-widget .fb-profile-pic").click(function(){window.open("http://www.facebook.com","facebook");});
$("#fb-widget .fb-status-btn").click(function(){HN.lequipe.fbwidget.setStatus();});
$("#fb-widget .fb-status-update-box .fb_inputsubmit").click(function(){HN.lequipe.fbwidget.hideStatusBox();});
$("#fb-widget .fb-status-input").focus(function(){
if(!HN.lequipe.fbwidget.inputStatusFocusedOnce){
HN.lequipe.fbwidget.inputStatusFocusedOnce=true;
this.value="";
}
});
$("#fb-widget .fb-friends").click(function(){
if(HN.lequipe.fbwidget.friendsBoxShowed)
HN.lequipe.fbwidget.hideFriends();
else
HN.lequipe.fbwidget.showFriends();
});
$("#fb-widget .fb-friends-box-close").click(function(){HN.lequipe.fbwidget.hideFriends();});
$("#fb-widget .fb-lequipe-friends").click(function(){
if(HN.lequipe.fbwidget.liveFeedShowed)
HN.lequipe.fbwidget.hideLiveFeed();
else
HN.lequipe.fbwidget.showLiveFeed();
});
$("#fb-widget .fb-lequipe-friends-box-close").click(function(){HN.lequipe.fbwidget.hideLiveFeed();});
$("#fb-widget .fb-become-fan").click(function(){window.open("http://www.new.facebook.com/pages/LEquipefr/76283417065","devenirFan");});
if($("#fb_etat"))
{
$("#fb_etat").html("<a href=\"javascript: FB.Connect.logoutAndRedirect(window.location.href);\"><img src=\"/img_v5/FBConnect_light_large_short.gif\" title='Se d\u00e9connecter' /></a>");
}
});
}
},
onNotConnected:function(){
FB.ensureInit(function(){
FB.Connect.get_status().waitUntilReady(function(status){
if(status==FB.ConnectState.userNotLoggedIn){
if($("#fb_etat"))
{
$("#fb_etat").html("<a href=\"#\" onclick=\"HN.lequipe.fbwidget.connect(); return false;\"><img src=\"/img_v5/FBConnect_light_large_short.gif\" title='Se connecter avec son compte facebook' /></a>");
}
}
if(status==FB.ConnectState.appNotAuthorized){
if($("#fb_etat"))
{
$("#fb_etat").html("<a href=\"#\" onclick=\"HN.lequipe.fbwidget.connect(); return false;\"><img src=\"/img_v5/FBConnect_light_large_short.gif\" title='Se connecter avec son compte facebook' /></a>");
}
}
});
});
},
connect:function(){
if(HN.browser.opera||(HN.browser.msie&&HN.browser.version<7)){
HN.lequipe.fbwidget.showBrowserBox();
}
else{
FB.Connect.requireSession()
}
},
showBrowserBox:function(){
$("#fb-wrapper .fb-browser-box").css({
left:Math.round(($(window).width()-$("#fb-wrapper .fb-browser-box").width())/2),
bottom:Math.round(($(window).height()-$("#fb-wrapper .fb-browser-box").height())/2)
});
if(!HN.lequipe.fbwidget.browserBoxShowed){
$("#fb-wrapper .fb-browser-box").show();
HN.lequipe.fbwidget.browserBoxShowed=true;
}
},
hideBrowserBox:function(){
$("#fb-wrapper .fb-browser-box").hide();
HN.lequipe.fbwidget.browserBoxShowed=false;
},
showOptions:function(){
$("#fb-widget .fb-options-box").css({display:"block"});
HN.lequipe.fbwidget.hideFriends();
HN.lequipe.fbwidget.hideLiveFeed();
HN.lequipe.fbwidget.optionsBoxShowed=true;
},
hideOptions:function(){
$("#fb-widget .fb-options-box").css({display:"none"});
HN.lequipe.fbwidget.optionsBoxShowed=false;
},
showStatusBox:function(){
$("#fb-widget .fb-status-update-box").css({
left:Math.round(($(window).width()-$("#fb-widget .fb-status-update-box").width())/2),
bottom:Math.round(($(window).height()-$("#fb-widget .fb-status-update-box").height())/2)
});
if(!HN.lequipe.fbwidget.statusBoxShowed){
$("#fb-widget .fb-status-update-box").show();
HN.lequipe.fbwidget.statusBoxShowed=true;
}
},
hideStatusBox:function(){
$("#fb-widget .fb-status-update-box").hide();
HN.lequipe.fbwidget.statusBoxShowed=false;
},
friendsBoxIntervalCheck:null,
checkFriendsBoxSize:function(check){
if(check){
var box_out=$("#fb-widget .fb-friends-box-out");
var box_in=$("#fb-widget .fb-friends-box-in");
HN.lequipe.fbwidget.friendsBoxIntervalCheck=setInterval(function(){
var final_height;
var box_in_height=box_in.height();
var max_height=Math.round(($(window).height()-(25+15+4))*0.80);
if(box_in_height>max_height)
final_height=max_height;
else
final_height=box_in_height;
if(box_out.height()!=final_height)
box_out.height(final_height);
},13);
}
else{
clearInterval(HN.lequipe.fbwidget.friendsBoxIntervalCheck);
}
},
showFriends:function(){
$("#fb-widget .fb-friends-box").css({display:"block"});
$("#fb-widget .fb-friends-box-in").load(HN.lequipe.locals.base_url+"scripts/process.php?friends=1&t="+Math.floor((new Date()).getTime()/1000),"",function(){FB.XFBML.Host.parseDomTree();});
HN.lequipe.fbwidget.hideLiveFeed();
HN.lequipe.fbwidget.friendsBoxShowed=true;
HN.lequipe.fbwidget.checkFriendsBoxSize(true);
},
hideFriends:function(){
$("#fb-widget .fb-friends-box").css({display:"none"})
HN.lequipe.fbwidget.friendsBoxShowed=false;
HN.lequipe.fbwidget.checkFriendsBoxSize(false);
},
liveFeedIntervalCheck:null,
checkLiveFeedSize:function(check){
if(check){
var box_out=$("#fb-widget .fb-lequipe-friends-box-out");
var box_in=$("#fb-widget .fb-lequipe-friends-box-in");
HN.lequipe.fbwidget.liveFeedIntervalCheck=setInterval(function(){
var final_height;
var box_in_height=box_in.height();
var max_height=Math.round(($(window).height()-(25+15+4))*0.80);
if(box_in_height>max_height)
final_height=max_height;
else
final_height=box_in_height;
if(box_out.height()!=final_height)
box_out.height(final_height);
},13);
}
else{
clearInterval(HN.lequipe.fbwidget.liveFeedIntervalCheck);
}
},
showLiveFeed:function(){
if(!HN.lequipe.fbwidget.liveFeedShowedOnce){
$("#fb-widget .fb-lequipe-friends-box-in").load(HN.lequipe.locals.base_url+"scripts/remote.php",function(){
FB.XFBML.Host.parseDomTree();
$("#fb-widget .fb-lequipe-friends-box-in").css({height:"auto"});
});
HN.lequipe.fbwidget.liveFeedShowedOnce=true;
}
$("#fb-widget .fb-lequipe-friends-box").css({display:"block"});
HN.lequipe.fbwidget.hideFriends();
HN.lequipe.fbwidget.liveFeedShowed=true;
HN.lequipe.fbwidget.checkLiveFeedSize(true);
},
hideLiveFeed:function(){
$("#fb-widget .fb-lequipe-friends-box").css({display:"none"})
HN.lequipe.fbwidget.liveFeedShowed=false;
HN.lequipe.fbwidget.checkLiveFeedSize(false);
},
facebook_prompt_permission:function(permission,callbackFunc){
FB.Facebook.apiClient.users_hasAppPermission(permission,
function(result){
if(result==0){
FB.Connect.showPermissionDialog(permission,callbackFunc);
}else{
callbackFunc(true);
}
});
},
loadProfile:function(){
var callback_func=null;
if(arguments.length>0&&typeof arguments[0]=="function")
callback_func=arguments[0];
$.ajax({
data:"profile=1&t="+Math.floor((new Date()).getTime()/1000),
dataType:"json",
error:function(XMLHttpRequest,textStatus,errorThrown){
},
success:function(data,textStatus){
$("#fb-widget .fb-status-input").val(data["status"]);
if(data["pic_square"]!=""){
var pic=document.createElement("img");
pic.src=data["pic_square"];
$("#fb-widget .fb-profile-pic").append(pic);
}
if(data["is_fan"]=="0"){
$("#fb-widget .fb-become-fan-title").css({"background-image":"url("+HN.lequipe.locals.base_url+"images/text-become-fan-EFR.png)"});
}
HN.lequipe.fbwidget.profileLoaded=true;
if(callback_func)callback_func();
},
type:"GET",
url:HN.lequipe.locals.base_url+"scripts/process.php"
});
},
setStatus:function(){
HN.lequipe.fbwidget.facebook_prompt_permission('status_update',function(accepted){
if(accepted){
var user_status=$("#fb-widget .fb-status-input").val();
$("#fb-widget .fb-status-update-state").load(HN.lequipe.locals.base_url+"scripts/process.php?status=1&&t="+Math.floor((new Date()).getTime()/1000),{txt:user_status},function(){
HN.lequipe.fbwidget.showStatusBox();
});
}
});
},
getLastUpdatedFriendStatus:function(){
var callback_func=null;
if(arguments.length>0&&typeof arguments[0]=="function")
callback_func=arguments[0];
$.ajax({
data:"last_updated_friend_status=1&t="+Math.floor((new Date()).getTime()/1000),
dataType:"json",
error:function(XMLHttpRequest,textStatus,errorThrown){
},
success:function(data,textStatus){
if(data["uid"].length!=0)
$("#fb-widget .fb-friends-pic").html("<fb:profile-pic uid=\""+data["uid"]+"\" width=\"24\" height=\"24\" linked=\"false\" facebook-logo=\"false\"></fb:profile-pic>");
var message=data["message"];
if(message.length==0){
$("#fb-widget .fb-friends").css({width:"110px"});
$("#fb-widget .fb-friends-sep").hide();
$("#fb-widget .fb-friends-status").hide();
}
else{
$("#fb-widget .fb-friends").css({width:"auto"});
if(message.length>25){
message=message.substr(0,23)+"...";
}
$("#fb-widget .fb-friends-sep").show();
$("#fb-widget .fb-friends-status").show().html(message);
}
HN.lequipe.fbwidget.lastUpdatedFriendStatusLoaded=true;
if(callback_func)callback_func();
FB.XFBML.Host.parseDomTree();
},
type:"GET",
url:HN.lequipe.locals.base_url+"scripts/process.php"
});
},
TryUnfoldDock:function(){
if(HN.lequipe.fbwidget.profileLoaded&&HN.lequipe.fbwidget.lastUpdatedFriendStatusLoaded){
var dockWasFolded=HN.lequipe.CM.read("fb-dock-folded");
if(!dockWasFolded||dockWasFolded=="0")
HN.lequipe.fbwidget.unFoldDock();
}
},
showDock:function(){
$("#fb-widget").show();
if($("#fb_etat"))
{
$("#fb_etat").html("<a href=\"#\" onclick=\"FB.Connect.logoutAndRedirect(window.location.href);\"><img src=\"/img_v5/FBConnect_light_large_short.gif\" title='Se d\u00e9connecter' /></a>");
}
},
hideDock:function(){
$("#fb-widget").hide();
},
foldDock:function(){
HN.lequipe.fbwidget.hideFriends();
HN.lequipe.fbwidget.hideLiveFeed();
$("#fb-widget .fb-bar-content").animate({
width:HN.lequipe.fbwidget.contentFoldedWidth+"px",
left:HN.lequipe.fbwidget.contentFoldedLeft+"px"
},{
duration:500,
complete:function(){
HN.lequipe.fbwidget.dockFolded=true;
$("#fb-widget .fb-bar-mask").width(HN.lequipe.fbwidget.maskFoldedWidth);
$("#fb-widget .fb-dbl-arrow").removeClass("fb-dbl-arrow-left");
$("#fb-widget .fb-dbl-arrow").addClass("fb-dbl-arrow-right");
var date=new Date;
date.setMonth(date.getMonth()+1);
HN.lequipe.CM.write("fb-dock-folded","1",date);
}
});
},
unFoldDock:function(){
$("#fb-widget .fb-bar-mask").width(HN.lequipe.fbwidget.maskUnfoldedWidth);
$("#fb-widget .fb-bar-content").animate({
width:HN.lequipe.fbwidget.contentUnfoldedWidth+"px",
left:HN.lequipe.fbwidget.contentUnfoldedLeft+"px"
},{
duration:500,
complete:function(){
HN.lequipe.fbwidget.dockFolded=false;
$("#fb-widget .fb-dbl-arrow").removeClass("fb-dbl-arrow-right");
$("#fb-widget .fb-dbl-arrow").addClass("fb-dbl-arrow-left");
var date=new Date;
date.setMonth(date.getMonth()+1);
HN.lequipe.CM.write("fb-dock-folded","0",date);
}
});
}
};
$(function(){
var timestamp=Math.floor((new Date()).getTime()/1000);
FB_RequireFeatures(["XFBML"],function(){
FB.Facebook.init(
"90f417b3796ccc719edc79345ef7c13d",
HN.lequipe.locals.base_url+"xd_receiver.htm",{
ifUserConnected:HN.lequipe.fbwidget.onConnected,
ifUserNotConnected:HN.lequipe.fbwidget.onNotConnected
});
});
var imageSrcList=["dbl-arrow-left.png","dbl-arrow-right.png","loading.gif"];
for(var i=0,max=imageSrcList.length;i<max;i++){
var img=new Image();
img.src=HN.lequipe.locals.base_url+"images/"+imageSrcList[i];
}
var wv=HN.lequipe.fbwidget;
$("#fb-widget .fb-dbl-arrow").click(function(){
if(wv.dockFolded)
wv.unFoldDock();
else
wv.foldDock();
});
$("#fb-wrapper .fb-browser-box .fb_inputsubmit").click(function(){wv.hideBrowserBox();});
if(HN.browser.mozilla){
$(window).bind("mouseleave",function(e){
wv.resizeInterval=setInterval(function(){
var w=$(window).width();
if(w!=wv.outWidth)$(window).resize();
},100);
});
$(window).bind("mouseenter",function(e){
clearInterval(wv.resizeInterval);
});
}
$(window).resize(function(){
wv.outWidth=$(window).width();
var width=($(window).width()-wv.marginsWidth);
if(width<wv.minWidth)
width=wv.minWidth;
wv.maskUnfoldedWidth=width;
wv.contentUnfoldedWidth=width-wv.contentBordersWidth;
if(!wv.dockFolded){
$("#fb-widget .fb-bar-mask").width(wv.maskUnfoldedWidth+"px");
$("#fb-widget .fb-bar-content").width(wv.contentUnfoldedWidth+"px");
}
if(wv.statusBoxShowed){
wv.showStatusBox();
}
if(wv.browserBoxShowed){
wv.showBrowserBox();
}
});
$(window).resize();
});