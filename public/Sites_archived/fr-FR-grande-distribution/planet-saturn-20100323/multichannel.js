// Namespace object for multichannel functions
var multiChannel = new Object();
multiChannel.url = new Object();
// show Layer
multiChannel.showLayer = function(elementId){
	$(elementId).show();
};

// hide Layer
multiChannel.hideLayer = function(elementId){
	$(elementId).hide();
};

//***************
// Layer Handling
//***************
multiChannel.layer = new Object();
multiChannel.layerParameters;
multiChannel.layer.callback;

multiChannel.openLayer = function(layerId, parameters, callback, layerProvider){
	multiChannel.layerParameters = parameters;
	var layerUrl = multiChannel.context.layer;
	if (layerProvider != null) {
		layerUrl = layerProvider.providerUrl;
	}

	if ((layerId != null && layerId.length > 0)){
		if (!$defined(callback)) {
			if (layerId == 'flyer') {
				callback = function() {
					multiChannel.flyer.show(multiChannel.context.outletId);
				};
			}
		}

		if (!$defined(multiChannel.layer[layerId])) {
			multiChannel.layer[layerId] = new Object();
		}
		multiChannel.layer[layerId].callback = callback;

		if (!$defined(multiChannel.layer[layerId].content)) {
			var uri = new URI(layerUrl);
			uri.setData({layer:layerId}, true);

			if (parameters){ 
				uri.setData(parameters, true);
			}

			//Safari Cache Bugfix
			uri.setData({r:$random(1, 99999999)},true);	
			uri.set('query', uri.get('query').cleanQueryString());

			var caller = new URI();
			uri.set('scheme', caller.get('scheme'));
			uri.set('port', caller.get('port'));					

			multiChannel.addJavaScript('jsLayer', uri.toString());
		} else {
			multiChannel.renderLayer(layerId);
		}
	}
};

multiChannel.closeLayer = function(){
	var body = $(document.body);
	var overlay = $('overlay');
	var layer = $('layer-container');
	var iframe = $('layer-container-iframe');

	if (body != undefined && overlay != undefined)
		body.removeChild(overlay);
		
	if (body != undefined && layer != undefined)
		body.removeChild(layer);
		
	if (body != undefined && iframe != undefined)
		body.removeChild(iframe);
};

multiChannel.closeAndRemoveLayer = function(layerId){
	if ($defined(multiChannel.layer[layerId])) {
		multiChannel.layer[layerId].content = undefined;
	}
	multiChannel.closeLayer();
};

multiChannel.closeAndReopenLayer = function(layerId, options){
	if ($defined(multiChannel.layer[layerId])) {
		multiChannel.layer[layerId].content = undefined;
	}
	multiChannel.openLayer(layerId, options);
};

multiChannel.renderLayer = function(layerId){
	multiChannel.closeLayer();
	
	var content = multiChannel.layer[layerId].content;
	
	var body = $(document.body);
	
	// overlay container (modal)
	var overlay = new Element('div', {'id': 'overlay'});
	body.appendChild(overlay);

	// layer container (center)
	var container = new Element('div', {'id': 'layer-container'});
	body.appendChild(container);

	// layer container (shadow)
	var layer = new Element('div', {'id': 'layer'});
	layer.innerHTML = content;
	if (multiChannel.layerParameters != null && multiChannel.layerParameters['layerClass']){
		layer.className = multiChannel.layerParameters['layerClass'];
	}
	if (multiChannel.layerParameters != null && multiChannel.layerParameters['layerWidth']){
		layer.style.width = multiChannel.layerParameters['layerWidth'];
	}
	container.appendChild(layer);

	// layer container iframe (center)
	var container2 = new Element('div', {'id': 'layer-container-iframe'});
	body.appendChild(container2);

	// iframe fix for IE6
	var iframe = new Element('iframe', {'id': 'layer-iframe'});
	iframe.frameborder = '0px';
	iframe.src = '';
	container2.appendChild(iframe);
	
	// Update iframe
	multiChannel.fitLayer();
	
	// Scroll to top
	self.scrollTo(0, 0);
	
	// Init faq layer (if needed)
	if (layerId == 'faq'){
		var anchor;
		if (multiChannel.layerParameters != null){
			anchor = multiChannel.layerParameters['anchor'];
		}
		faq.init(anchor);
	}

	if ($defined(multiChannel.layer[layerId].callback)) {
		multiChannel.layer[layerId].callback();
		multiChannel.layer[layerId].callback = undefined;
	}
};

multiChannel.fitLayer = function(){
	var layer = $('layer');
	var iframe = $('layer-iframe');
	iframe.style.width = layer.offsetWidth + 'px';
	iframe.style.height = layer.offsetHeight + 'px';
};

multiChannel.getQueryString = function() {
	var uri = new URI();
	if ($chk(multiChannel.krypto)) {
		var queryString = uri.get('query');
		queryString += $chk(queryString) ? ('&' + multiChannel.krypto) : multiChannel.krypto;
		uri.set('query', queryString);
	}
	return uri.toString();
};

// ********************
// get an URL-parameter
// ********************
multiChannel.getParameter = function(name) {
	var uri = new URI();
	
	if ($chk(multiChannel.krypto)) {
		var queryString = uri.get('query');
		queryString += $chk(queryString) ? ('&' + multiChannel.krypto) : multiChannel.krypto;
		uri.set('query', queryString);
	}
	return uri.getData(name);
};

// ********************
// set an URL-parameter
// ********************
multiChannel.setParameter = function(name, value) {
	var parameters = new Object();
	parameters[name] = value;
	multiChannel.setParameters(parameters);
};

multiChannel.setParameters = function(parameters) {
	var uri = new URI();
	uri.setData(parameters, true);
	uri.set('query', uri.get('query').cleanQueryString());
	uri.go();
};

multiChannel.setNavigationLeft = function(id) {
	if(!$defined(id)) {
		// Product area
		var leftNav = $('navigationLeft');
		var leftNavAnchors = leftNav.getElements('a');
		var id = 'left_' + multiChannel.getParameter('categoryId');
	}
	var selectedElement = $(id);
	
	if ($defined(selectedElement)) {
		selectedElement.addClass('selected');

		selectedElement.getParents('ul').each(function(item) {
			item.addClass('selected');

			if ($defined(item.getPrevious('a'))) {
				item.getPrevious('a').addClass('selected');
			}
		});

		selectedElement.getParents('li').each(function(item) {
			item.addClass('selected');
		});

		if ($defined(selectedElement.getNext('ul'))) {
			selectedElement.getNext('ul').addClass('selected');
		}

		var element = $('navigationLeft').getFirst('ul').getFirst('li.selected');

		if ($defined(element)) {
			multiChannel.setNavigationSub('top_' + element.getFirst('a').id.replace(/[^0-9]*([0-9]*)/, '$1'));
			
			if ($defined(element.getFirst('ul.selected'))) {

				while ($defined(element.getFirst('ul.selected')) && $defined(element.getFirst('ul.selected').getLast('li'))) {
					element = element.getFirst('ul.selected').getLast('li');
				}

				if ($defined(element.getFirst('a'))) {
					element.getFirst('a').addClass('last');
				}
			}
		}

	}
};

multiChannel.processSlider = function() {

	$$('div .sliderToggle').each(function(toggleElement) {
		var contentElement = toggleElement.getNext('div .sliderContent');

		var togglerElement = new Element('div', {
			'class': 'boxCollapse'
		});
		var targetElement = new Element('div', {
			'class': 'sliderTarget',
			'styles': {
				'height': contentElement.offsetHeight
			}
		});

		toggleElement.grab(togglerElement);
		targetElement.wraps(contentElement);

		var fx = new Fx.Slide(
			targetElement,
			{
				duration: contentElement.offsetHeight * 5,
				transition: Fx.Transitions.Bounce.easeOut,
				onStart: function() {
					if(togglerElement.hasClass('boxExpand')){
						targetElement.setStyles({'visibility': 'visible'});
					}
					togglerElement.toggleClass('boxCollapse');
					togglerElement.toggleClass('boxExpand');
				},
				onComplete: function() {
					if(togglerElement.hasClass('boxExpand')){
						targetElement.setStyles({'visibility': 'hidden'});
					}
				}
			});

		//Fix Fx.Slide-bug not setting height initially
		targetElement.parentNode.setStyle('height', contentElement.offsetHeight);

		//Fix IE6 misbehaviour
		if (Browser.Engine.trident && Browser.Engine.version == 4) {
			contentElement.setStyle('top', 0);
		}

		togglerElement.addEvent('click', function(e) {
			e = new Event(e);
			fx.toggle();
			e.stop();
		});
	});
};

/**
 * executes a store change by a store drop down box
 */
multiChannel.changeStore = function (storeId) {

	if (storeId != -1) {
		var redirectUrl =  multiChannel.getParameter('redirectURL');  
		var uri = redirectUrl == null ? new URI() : new URI(redirectUrl);
		uri.setData({'storeId': storeId}, true);
		uri.go();
	}
};

multiChannel.changeStoreAndReturn = function (storeId, param) {
	if (storeId != -1) {
		var uri = new URI(multiChannel.getQueryString());
		var p = 'all';
		if(param) p = param;
		uri.setData({'storeId': storeId}, true);
		uri.setData({'display': p}, true);
		uri.go();
	}
};

multiChannel.setNavigation = function(id) {
	$('navigationList').getChildren('li').each(function(element) {
		if (element.id != undefined) {
			element.removeClass(element.id + 'Selected');
			element.addClass(element.id);
			if (element.id == id) {
				element.removeClass(element.id);
				element.addClass(element.id + 'Selected');
			}
		}
	});
};

multiChannel.setNavigationSub = function(id) {
	if ($(id) != null) {
		$(id).addClass('selected');
	}
};

multiChannel.restoreNavigation = function(event, id) {
	event = event || window.event || false;
	var element = event.relatedTarget || event.toElement || false;

	if ($(element) != $('navigation') &&  !$('navigation').hasChild($(element)) && ( !$defined($('topnavlayer_' + id))  || $(element) != $('topnavlayer_' + id) && !$('topnavlayer_' + id).hasChild($(element)))) {
		multiChannel.resetTopnav();
		multiChannel.setNavigation(multiChannel.navigationId);
	}
}

multiChannel.setNavigationInit = function(id) {
	if ($defined($(id))){
		if ($(id).getParents('div.sub').length > 0) {
			multiChannel.navigationId = $(id).getParents('div.sub')[0].getParent('div').id;
			multiChannel.setNavigationSub(id);
		} else {
			multiChannel.navigationId = id;
		}
		
		//$('navigation').onmouseout = multiChannel.restoreNavigation(event, id);
		multiChannel.setNavigation(multiChannel.navigationId);
		
	
		if (multiChannelTemplate.campaignSkin[multiChannel.navigationId] != null) {
			var campaignSkin = multiChannelTemplate.campaignSkin[multiChannel.navigationId].campaignSkin;
	
			$$('link[title=theme]').each(function(item) {
				item.href = item.href.replace(multiChannelTemplate.defaultCampaignSkin, campaignSkin);
			});
		}	
		if(id=='products'){
			$('searchOptionproducts').selected=true;
		}else if(id=='mediapedia'){
			$('searchOptionmediapedia').selected=true;
		}else if(id=='download'){
			$('searchOptiondownload').selected=true;
		}else {
			$('searchOptionallcategories').selected=true;
		}
	}
};

//topnav
multiChannel.topnavLayerInit = function(){
		window.addEvent('domready', function(){
			if($defined($('topnavlayer-container'))) {
				$('topnavlayer-container').getChildren().each(function(item){
					item.onmouseout = function(event) {
						event = event || window.event || false;
						var element = event.relatedTarget || event.toElement || false;
						if (element != null) multiChannel.closeTopnavLayer(element, item.get('id'));
					}
				});

				$$('#navigation ul li').each(function(item){
					item.addEvent('mouseover', function(){
						multiChannel.resetTopnav();
						if(!multiChannel.bigStartLayer.sliding){
							var navId = item.get('id');
							if($defined($('topnavlayer_' + navId))) {

								var layer = $('topnavlayer_' + navId);
								if(layer.hasClass('layer-small')){
									multiChannel.setTopnavArrow(navId, 'small');
								} else {
									multiChannel.setTopnavArrow(navId);
								}
								layer.show().addClass('active');
								multiChannel.fitTopnavLayer(navId);
								multiChannel.setNavigation(navId);
								$('navigationTopnavLink').focus(); 
							}
						}
					});
				});
			}			
		});
};

multiChannel.resetTopnav = function(){
	$('topnavlayer-container').getChildren().each(function(item){
		item.hide().removeClass('active');
	});
	multiChannel.removeTopnavIframe();
};
	
multiChannel.getXYPosition = function(element){
   if (!element) {
	  return {"x":0,"y":0};
   }
   var xy = {"x" : element.offsetLeft, "y" : element.offsetTop}
   var part = multiChannel.getXYPosition(element.offsetParent);
   for (var key in part) {
	  xy[key] += part[key];
   }
   return xy;
};
	
multiChannel.setTopnavArrow = function(navId, mode){
	if( $defined($('topnavlayer-arrow_' + navId)) && $defined($(navId)) ){
		var x = multiChannel.getXYPosition($('header')).x;
		// x = x + Abstand Left Nav + Abstand Pfeil  - Abstand div <-> Pfeil
		if(mode && mode == 'small'){
			x = x + 222 + 739 - 20;
		} else {
			x = x + 116 + 739 - 20;
		}
		var x2 = multiChannel.getXYPosition($(navId)).x;
		$('topnavlayer-arrow_' + navId).setStyle('backgroundPosition', '-' + (x-x2) + 'px 0px');
	}
};

multiChannel.fitTopnavLayer = function(navId){
	var layer = $('topnavlayer_' + navId);
	if($defined($(layer))){
		var addY = multiChannel.getXYPosition($('header')).y;
		var oldY = 116;
		var sum  = addY + oldY;
		$('topnavlayer-container').setStyle('top', sum);
		
		var layer_content = $('topnavlayer-content_' + navId);

		if($defined($(layer_content))){
			var iframe = $('topnavlayer-iframe');
			iframe.style.width = layer_content.offsetWidth + 'px';
			iframe.style.height = layer_content.offsetHeight + 'px';
		}
		var iframe_container = $('topnavlayer-container-iframe');
		iframe_container.setStyle('top', sum + 16);
		iframe_container.show();
	}
};
	
multiChannel.removeTopnavIframe = function(){
	var iframe = $('topnavlayer-iframe');
	iframe.style.width = 0;
	iframe.style.height = 0;
	$('topnavlayer-container-iframe').hide();
};
	
multiChannel.closeTopnavLayer = function(element, id){
	
	if(!$(id).hasChild($(element))){
		$(id).hide().removeClass('active');
		multiChannel.removeTopnavIframe();
		multiChannel.setNavigation(multiChannel.navigationId);
	}
	
};

multiChannel.allOutlets =  new Hash();
// Get all outlets
multiChannel.loadOutlets = function(tableId) {
		var tableEl = $(tableId);
		var childElements = tableEl.getElements('a');
		for (var i=0; i<childElements.length; i++) {
			var outletLinkEl=childElements[i];
			// Key: Outletname, Value: Url
			multiChannel.allOutlets.set(outletLinkEl.innerHTML, outletLinkEl.href);
		}
};

multiChannel.showOutlets = function(firstToken, tableId) {
	if($defined(tableId)) {
		var outlets = new Hash();
		var tableEl = $(tableId);
		// Collect all possible outlets
		outlets = multiChannel.allOutlets.filter(function(value, key){
			return key.test(firstToken) || firstToken=='all';
		});

		// Remove the content of our outlet table
		tableEl.deleteRow(0);
		tableEl.insertRow(-1);
		var counter = 0;
		var newCell;
		outlets.each(function(value, key){
			// Insert 6 outlets per column
			if(counter == 0 || counter % 6 == 0) {
				newCell = tableEl.rows[0].insertCell(-1);
				newCell.className = "tableCell";
			}
			counter++;
			var raquoSpan = new Element('span', {
				'html': '&raquo; ',
				'class' : 'storeprefix'
			});
			var uri = new URI(value);
			var storeId = uri.getData('storeId');
			var aLink = new Element('a', {
				'href': value,
				'onclick': 'multiChannel.changeStore(' + storeId + '); return false;',
				'html': key,
				'class' : 'outletlink'
			});
			var divEl = new Element('div', {
				'class': 'outletRow'
			});
			divEl.grab(raquoSpan);
			divEl.grab(aLink);
			$(newCell).grab(divEl);
		});

	}
};

// Add javascript to our head element
multiChannel.addJavaScript = function(elementId, src) {
	var headElement = document.getElementsByTagName("head")[0];
	var scriptElement = document.createElement('script');
	scriptElement = new Element('script', {
												'id':elementId,
												'type':'text/javascript',
												'src':src
		});
	scriptElement.type = 'text/javascript';
	scriptElement.src = src;
	headElement.appendChild(scriptElement);
};

/**************************************************
MultiChannel External SSO Functions
**************************************************/

multiChannel.sso = new Object();

// TEST: This method should be implemented by our partner
multiChannel.myCallBack = function() {
	alert('this is my callback function');
};

multiChannel.sso.extractCookieName = function(cookie) {
	var pos = cookie.indexOf("=");
	if (pos > -1) {
		return cookie.substr(0, pos);
	} else {
		return cookie;
	}
};

// This method performs a user login and rewrites the cookies by an callback function.
multiChannel.sso.logonUser = function(logonId, logonPassword, callbackFunction, params) {
	var url = multiChannel.context.ssoLogonUser + '&logonId=' + logonId + '&logonPassword=' + logonPassword;

	// Rewrite cookies as callback
	multiChannel.sso.setMCCallbackFunction(multiChannel.user.writeCookies);
	// Set our partners callbackFunction. This function will be called after we got our response from backend.
	multiChannel.sso.setCallbackFunction(callbackFunction);
	// Add to header
	multiChannel.addJavaScript('ssoResponseScript', url);
};

// User registration
multiChannel.sso.registerUser = function(logonId, logonPassword, logonPasswordVerify, firstName, lastName, captchaInput, callbackFunction, params) {
	var url = multiChannel.context.ssoRegisterUser + '&logonId=' + logonId
		+ '&logonPassword=' + logonPassword
		+ '&logonPasswordVerify=' + logonPasswordVerify
		+ '&firstName=' + firstName
		+ '&lastName=' + lastName
		+ '&captchaInput=' + captchaInput;
	params.each(function(item, index) {
		if(item)
			url += '&' + index + '=' + item;
	});
	// Rewrite cookies as callback
	multiChannel.sso.setMCCallbackFunction(multiChannel.user.writeCookies);
	// Set our callbackFunction. This function will be called after we got our response from backend.
	multiChannel.sso.setCallbackFunction(callbackFunction);
	// Add to header
	multiChannel.addJavaScript('ssoResponseScript', url);
};

// Resets the password and informs the customer by email
multiChannel.sso.forgotPassword = function(logonId, callbackFunction, params) {
	var url = multiChannel.context.ssoForgotPassword + '&logonId=' + logonId;
	// Set our callbackFunction. This function will be called after we got our response from backend.
	multiChannel.sso.setCallbackFunction(callbackFunction);
	// Add to header
	multiChannel.addJavaScript('ssoResponseScript', url);
};

// This function loads all user infos into our json object. Precondition: User must be logged in.
multiChannel.sso.getUserInfos = function(callbackFunction, params) {
	var url = multiChannel.context.ssoUserInfos;
	// Set our callbackFunction. This function will be called after we got our response from backend.
	multiChannel.sso.setCallbackFunction(callbackFunction);
	// Add to header
	multiChannel.addJavaScript('ssoResponseScript', url);
};

// This functions refreshed the basket of our current user
multiChannel.sso.refreshBasket = function(provider, count, basketURL, callbackFunction, params) {
	var url = multiChannel.context.ssoRefreshBasket + '&provider=' + provider + '&count=' + count + '&basketURL=' + escape(basketURL);
	// Update our basket
	multiChannel.sso.setMCCallbackFunction(multiChannel.basket.updateBasket);
	// Set our callbackFunction. This function will be called after we got our response from backend.
	multiChannel.sso.setCallbackFunction(callbackFunction);
	// Add to header
	multiChannel.addJavaScript('ssoResponseScript', url);
};

// Accept the terms and conditions permanently
multiChannel.sso.updateTACState = function(state, callbackFunction, params) {
	var url = multiChannel.context.ssoUpdateTACState + '&state=' + state;
	// Set our callbackFunction. This function will be called after we got our response from backend.
	multiChannel.sso.setCallbackFunction(callbackFunction);
	// Add to header
	multiChannel.addJavaScript('ssoResponseScript', url);
};

// Logout user
multiChannel.sso.logoutUser = function(callbackFunction, params) {
	var url = multiChannel.context.ssoLogoutUser;
	// Rewrite cookies as callback
	multiChannel.sso.setMCCallbackFunction(multiChannel.user.writeCookies);
	// Set our callbackFunction. This function will be called after we got our response from backend.
	multiChannel.sso.setCallbackFunction(callbackFunction);
	// Add to header
	multiChannel.addJavaScript('ssoResponseScript', url);
};

multiChannel.sso.periodicalUserRefreshFunction = function() {
	var headElement = document.getElementsByTagName("head")[0];
	var scriptElement = headElement.getElementById("userJS");
	if(scriptElement) {
		var userJSSrc = scriptElement.get("src");
		scriptElement.destroy();
		multiChannel.addJavaScript("userJS", userJSSrc);
	}
};

// Refresh sso informations every 25 minutes
multiChannel.sso.periodicalUserRefresh = multiChannel.sso.periodicalUserRefreshFunction.periodical(1500000);

/**************************************************
MultiChannel SSO functions (internal use only)
**************************************************/

multiChannel.sso.jsonObject = {};

// Method will be called by our sso response to provide the results to our partner
multiChannel.sso.setSSOJSONObject = function(jsonObject) {
	multiChannel.sso.jsonObject = jsonObject;
	// remove created script of our sso response
	var ssoScriptEl = $(document.head).getElementById('ssoResponseScript');
	if($defined(ssoScriptEl)) {
		ssoScriptEl.destroy();
	}

	// Call our own callback if defined (updateBasket() or writeCookies())
	if(multiChannel.sso.mcCallbackFunction && multiChannel.sso.requestSuccessful()) {
		multiChannel.sso.mcCallbackFunction();
	}

	// Call the callback function of our partner
	if(multiChannel.sso.callbackFunction) {
		multiChannel.sso.callbackFunction();
	} else {
		alert('Partner: No callback Function defined!');
	}

};

// Sets an internal callback to update cookies or basket
multiChannel.sso.setMCCallbackFunction = function(callback) {
	multiChannel.sso.mcCallbackFunction = callback;
};

// Sets the callback function of our partner. Will be used by our sso functions.
multiChannel.sso.setCallbackFunction = function(callback) {
	multiChannel.sso.callbackFunction = callback;
};

/**************************************************
MultiChannel External SSO functions (response)
**************************************************/

// Service method for our partner
multiChannel.sso.getJSON = function() {
	return multiChannel.sso.jsonObject;
};

// Service method for our partner
multiChannel.sso.getStatusCode = function() {
	if(multiChannel.sso.jsonObject && multiChannel.sso.jsonObject.state && multiChannel.sso.jsonObject.state.code)
		return multiChannel.sso.jsonObject.state.code;
	return 'SSO_NO_REQUEST';
};

// Service method for our partner
multiChannel.sso.requestSuccessful = function() {
	if(multiChannel.sso.jsonObject && multiChannel.sso.jsonObject.state && multiChannel.sso.jsonObject.state.code)
		return multiChannel.sso.jsonObject.state.code == 'SSO_SUCCESS';
	return false;
};

// Service method for our partner
multiChannel.sso.getUserToken = function() {
	if(multiChannel.sso.jsonObject && multiChannel.sso.jsonObject.userToken)
		return multiChannel.sso.jsonObject.userToken;
	return false;
};

// Service method for our partner
multiChannel.sso.getJSONUserInfos = function() {
	if(multiChannel.sso.jsonObject && multiChannel.sso.jsonObject.userinformationlist) {
		return multiChannel.sso.jsonObject.userinformationlist[0]
	} else {
		return null;
	}
};

// Service method for our partner
multiChannel.sso.getErrorText = function() {
	if(multiChannel.sso.jsonObject && multiChannel.sso.jsonObject.state && multiChannel.sso.jsonObject.state.text) {
		return multiChannel.sso.jsonObject.state.text;
	} else {
		return 'No error';
	}
};

// Service method for our partner
multiChannel.sso.getSSOToken = function() {
	var ssoToken;
	if(multiChannel.sso.jsonObject && multiChannel.sso.jsonObject.userToken && multiChannel.sso.jsonObject.userToken[1]) {
		ssoToken = multiChannel.sso.jsonObject.userToken[1];
	} 
	return ssoToken;
};

// Service method for our partner
multiChannel.sso.getRegisterType = function() {
	var regType;
	if(multiChannel.sso.jsonObject && multiChannel.sso.jsonObject.userToken && multiChannel.sso.jsonObject.userToken[0]) {
		regType = multiChannel.sso.jsonObject.userToken[0];
	} 
	return regType;
};
/**************************************************
END SSO Functions
**************************************************/

multiChannel.removeLastOptionalElement = function(element) {
	var lastElement = element.getLast('div.optional');

	if (lastElement != null) {

		if (lastElement.getPrevious().hasClass('separator')) {
			lastElement.getPrevious().dispose();
		}
		lastElement.dispose();
	}
};

multiChannel.getInnerHeight = function(element) {
	var result = 0;
	lastElement = element.getLast('div');

	if (lastElement != null) {
		result = lastElement.offsetTop + lastElement.offsetHeight;
	}
	return result;
}

multiChannel.getOptionalElementCount = function(element) {
	var result = 0;

	element.getChildren('div.optional').each(function(optionalElement) {

		if (optionalElement.isDisplayed()) {
			result++;
		}
	});
	return result;
}

multiChannel.fitSite = function() {
	var contentMiddle = $('contentMiddle');
	var contentLeft = $('contentLeft');
	var contentRight = $('contentRight');

	if (contentMiddle != null && contentLeft != null && contentRight != null) {
		var middleHeight = multiChannel.getInnerHeight(contentMiddle);

		if (contentLeft.getLast('div').hasClass('separator')) {
			contentLeft.getLast('div').dispose();
		}
		contentLeft.getLast('div').setStyle('margin-bottom', 0);
		contentRight.getLast('div').setStyle('margin-bottom', 0);

		var leftHeight = multiChannel.getInnerHeight(contentLeft);
		var leftOptionalCount = multiChannel.getOptionalElementCount(contentLeft);
		var rightHeight = multiChannel.getInnerHeight(contentRight);
		var rightOptionalCount = multiChannel.getOptionalElementCount(contentRight);


		while (leftHeight > middleHeight || rightHeight > middleHeight) {

			if (leftHeight > rightHeight && leftOptionalCount > 0) {
				multiChannel.removeLastOptionalElement(contentLeft);
				leftHeight = multiChannel.getInnerHeight(contentLeft);
				leftOptionalCount--;
				contentLeft.getLast('div').setStyle('margin-bottom', 0);
			} else if (rightHeight > leftHeight && rightOptionalCount > 0) {
				multiChannel.removeLastOptionalElement(contentRight);
				rightHeight = multiChannel.getInnerHeight(contentRight);
				rightOptionalCount--;
				contentRight.getLast('div').setStyle('margin-bottom', 0);
			} else if (leftHeight > rightHeight && leftOptionalCount > 0 && rightOptionalCount > 0) {
				multiChannel.removeLastOptionalElement(contentLeft);
				leftHeight = multiChannel.getInnerHeight(contentLeft);
				leftOptionalCount--;
				contentLeft.getLast('div').setStyle('margin-bottom', 0);
				multiChannel.removeLastOptionalElement(contentRight);
				rightHeight = multiChannel.getInnerHeight(contentRight);
				rightOptionalCount--;
				contentRight.getLast('div').setStyle('margin-bottom', 0);
			} else {
				break;
			}
		}
	}
};

multiChannel.submitenter = function(e, callBack) {
	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
	else return true;

	if (keycode == 13) {
		// f.e. submit()
		callBack();
		return false;
	}
	else
		 return true;
};

//*********************************
//** Userdata handling functions **
//*********************************

multiChannel.setPhone = function(phoneEl, prefixEl, numberEl) {
	var prefix = prefixEl.get('value');
	var number = numberEl.get('value');
	if(phoneEl && prefix && number) {
		phoneEl.setProperty('value', prefix + '-' + number);
	}
};

multiChannel.setBirthday = function(birthdayEl, dayEl, monthEl, yearEl) {
	var day = dayEl.get('value');
	var month = monthEl.get('value');
	var year = yearEl.get('value');
	if(birthdayEl && day && month && year) {
		birthdayEl.setProperty('value', day + '.' + month + '.' + year);
	}
};

//*********************
//** Validator **
//*********************
multiChannel.validator = new Object();

// Simple logging with firebug or alert function
multiChannel.validator.log = function(value) {
  if (window.console) { console.log(value); } else {alert(value);}
};

multiChannel.validator.FormValidator = new Class({
  Implements: [Options, Events],

  options: {
    requiredSpan: new Element('span', { 'class': 'red'}),
    requiredSpanText: '*',
    onFormValidate: function(isValid, form){},
    onElementValidate: function(isValid, field){}
  },

  initialize: function(form, formFieldoptions, errorBox, globalErrorMessage, globalErrorsMessage){
    this.formFieldoptions = formFieldoptions;
    try {
      if(errorBox){
        this.errorBox = $(errorBox);
      }
      else{
        this.errorBox = $('errorBox');
      }
      if(globalErrorMessage) {
      	this.globalErrorMessage = globalErrorMessage;
      }
      if(globalErrorsMessage) {
      	this.globalErrorsMessage = globalErrorsMessage;
      }
      this.form = $(form);
      this.watchFields();
    }
    catch(e){
      multiChannel.validator.log(e);
    }
  },

  watchFields: function(){
    try{
      for(fieldId in this.formFieldoptions){
        var el = $(fieldId);
        if(this.formFieldoptions[fieldId].required){
          this.initRequired(fieldId);
        }
      };
    }
    catch(e){
      multiChannel.validator.log(e);
    }
  },

  initRequired: function(fieldId){
    var tmpId = fieldId;
    if(this.formFieldoptions[fieldId].destination){
      tmpId = this.formFieldoptions[fieldId].destination;
    }
    if($('t_' + tmpId)){
    var descrElem = $('t_' + tmpId);
      var tmpEL = this.options.requiredSpan.clone();
      tmpEL.set('html', this.options.requiredSpanText);
      tmpEL.injectInside(descrElem);
    }
  },

  validate: function(force) {
    this.clearGlobalError();
    this.clearLocalErrors();
    var result = true;
    var count = 0;
    for(fieldId in this.formFieldoptions){
      var el = $(fieldId);
      if(!this.validateField(el, force, true)){
        result = false;
				count++;
        if(!force)
          break;
      }else if(el.hasClass('error')){
				el.removeClass('error');
				el.removeClass('error_text');
			}
    };
    /*if(!result && this.globalErrorMessage){
			var message;
			if(count > 1)
				message = this.globalErrorsMessage.replace('{0}', count);
			else
				message = this.globalErrorMessage;
      //this.addGlobalError(message);
    }*/
    return result;
  },

  validateField: function(field, force, full){
    //alert(field.id + '|' + force + '|' + full)
    if(this.paused) return true;
    field = $(field);
    var result = false;

    result = this.formFieldoptions[field.id].validators.some(function(validatorObj){
      var depends = false;
      if(this.formFieldoptions[field.id].dependsSrc){
        var tmpButtons = $('input[name='+this.formFieldoptions[field.id].dependsSrc+']', this.form.id);
        if(tmpButtons && tmpButtons.length>0) {
          for (var i = 0; i < tmpButtons.length; i++) {
            if(tmpButtons[i].value == this.formFieldoptions[field.id].dependsVal && tmpButtons[i].checked){
              depends = true;
            }
          }
        }

        tmpButtons = $(this.formFieldoptions[field.id].dependsSrc, this.form.id);

        if (tmpButtons.value == this.formFieldoptions[field.id].dependsVal){
         depends = true;
        }

        if(tmpButtons && tmpButtons.length > 0) {
          for (var i = 0; i < tmpButtons.length; i++) {
            if(tmpButtons[i].selected && tmpButtons[i].value == this.formFieldoptions[field.id].dependsVal){
              depends = true;
            }
          }
        }


      }

      var tmpResult = true;
      field.removeClass('error_text');
      if((field.get('value') != '' || full) && !this.formFieldoptions[field.id].dependsField){
        var tmpValidator = this.validators[validatorObj.name];

        if(depends || !this.formFieldoptions[field.id].dependsSrc){
          tmpResult = tmpValidator.test(field, this.formFieldoptions[field.id]);

          if(!tmpResult || validatorObj.name == 'Valid'){
            if(force){
            	//this.addLocalError(validatorObj.msg + "<br />", field);
							this.addGlobalError(validatorObj.msg + "<br />", field);
            }
          }
        }

       //control payment data with depends field
       } else if ((field.get('value') != '' || full) && this.formFieldoptions[field.id].dependsField){
         element = field.get('value');
         options = $(this.formFieldoptions[field.id].dependsField).get('value');
         srcval = $(this.formFieldoptions[field.id].dependsSrc).get('value');
         dpval = this.formFieldoptions[field.id].dependsVal;

         if (srcval == dpval){

           if($(this.formFieldoptions[field.id].fieldpair1)){
             fieldpairvalue = $(this.formFieldoptions[field.id].fieldpair1).get('value');
             if ((element == '' && fieldpairvalue != '')){
               //this.addLocalError(validatorObj.msg2 + "<br />", field);
							 this.addGlobalError(validatorObj.msg2 + "<br />", field);
              return tmpResult;
           }
         }

         if(element.length == 0 && options.length == 0){
           //this.addLocalError(validatorObj.msg2 + "<br />", field);
					 this.addGlobalError(validatorObj.msg2 + "<br />", field);
           return tmpResult;
         }

        }

       }

      return !tmpResult;

    }, this);

    if(!full) {
      this.validate(false);
    }
    return !result;
  },

	addGlobalError: function(message, theField){
	    if (this.errorBox.hasClass('globalErrorBoxHideout')){
	      this.errorBox.removeClass('globalErrorBoxHideout');
	      message = message;
	    }
			
	    var tmpEL = new Element('li');
	    tmpEL.set('html', message);
	    tmpEL.addClass('error_text');
			
			var error;
	    if (this.errorBox.getElementById(this.errorBox.id + '_messages')){
	    	error = this.errorBox.getElementById(this.errorBox.id + '_messages');
	    }
			
	    if(error){
		    tmpEL.injectInside(error);
		    if(theField){
					theField.addClass('error_text');
					theField.addClass('error');
		    }
	    }
	},

  // Injects a new table row with an error message
	addLocalError: function(message, theField){
		// Inject error message before error
		var parent = theField.getParent('tr');
		// Get the number of cells
		var childCells = parent.getChildren();
		var rowClassname = parent.className;
		var cellClassname = childCells[0].className;
		var colSpan = 0;
		childCells.each(function(childCell, index) {
			if(childCell.get('colspan')) {
				colSpan += childCell.get('colspan');
			}
		});
		// Create new row
		var errorRow = new Element('tr');
		errorRow.addClass('error_text');
		// Assing row style to our new row
		if(rowClassname)
			errorRow.addClass(rowClassname);
		// Create cell
		var errorCell = new Element('td');
		// Assing cell style to our new cell
		if(cellClassname)
			errorCell.addClass(cellClassname);
		errorCell.set('colspan', colSpan);
		//errorCell.innerHTML = message;
		errorCell.set('html', message);
		// Inject cell into row
		errorCell.inject(errorRow);
		// Inject row before error row
		errorRow.inject(parent, 'before');
		theField.addClass('error_text');
		if(theField.match('select')) {
			var followingSelectBoxes = theField.getAllNext('select');
			followingSelectBoxes.each(function(selectBox, index) {
					selectBox.addClass('error_text');
			});
			var previousSelectBoxes = theField.getAllPrevious('select');
			previousSelectBoxes.each(function(selectBox, index) {
					selectBox.addClass('error_text');
			});
		}
	},

	clearGlobalError: function(){
		if(!this.errorBox.hasClass('globalErrorBoxHideout'))
			this.errorBox.addClass('globalErrorBoxHideout')
		if(this.errorBox.getElementById(this.errorBox.id + '_messages'))
			this.errorBox.getElementById(this.errorBox.id + '_messages').set('html', '');
	},

	clearLocalErrors: function(){
			if(this.form) {
				var tbody = this.form.getChildren('tbody');
				var errorRows = tbody[0].getChildren('tr');
				errorRows.each(function(errorRow, index){
					if(errorRow.hasClass('error_text'))
						errorRow.destroy();
				});
			}
  },

  stop: function(){
    this.paused = true;
  },

  start: function(){
    this.paused = false;
  },


  validators: {
    IsEmpty: {
      test: function(element, options) {
        if(element.type == "select-one"||element.type == "select")
          return (element.selectedIndex >= 0 && element.options[element.selectedIndex].value != "");
        else
		  return !((element.get('value') == null) || (element.get('value').trim().length == 0));
      }
    },

    IsChecked: {
      test: function(element) {
        return (element.checked);
      }
    },
    
	IsOneChecked: {
		test: function(element, options) {
			if(element.checked || options.dependingCheckbox.checked)
				return true;
		}
    },

    IsNumber: {
      test: function (element) {
        if(!((element.get('value') == null) || (element.get('value').length == 0)))
          return (/^[0-9]+$/.test(element.get('value')));
          return true;
      }
    },
    
    IsNoNumber: {
    	test: function (element) {
    	  if(!((element.get('value') == null) || (element.get('value').length == 0)))
	          return (/^[^0-9]+$/.test(element.get('value')));
	          return true;
    	}
    },

    IsValidPhoneNumber: {
      test: function (element) {
        return /^[0-9\-\/ ]*$/.test(element.get('value'));
      }
    },

    IsValidPhoneAreaCode: {
      test: function (element) {
      	if(!((element.get('value') == null) || (element.get('value').length == 0)))
        	return /^0[0-9]*$/.test(element.get('value'));
        return true;
      }
    },

    IsValidEmail: {
      test: function (element) {
      	if(!((element.get('value') == null) || (element.get('value').length == 0)))
	        return (/^([a-zA-Z0-9-_]+(?:\.[a-zA-Z0-9-_]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?)$/.test(element.get('value')));
        return true;
      }
    },

    IsEqualField1: {
      test: function (element, options) {
        return element.get('value').trim() == options.field1.get('value').trim();
      }
    },
    
    IsNotEqualField1: {
      test: function (element, options) {
      	var result = element.get('value').trim() != options.field1.get('value').trim();
        return result;
      }
    },

    IsDependsField1: {
      // Checks if both fields are empty
      test: function (element, options) {
        if (!((element.get('value') == null || (element.get('value').length == 0)) && (options.field1.get('value') == null || (options.field1.get('value').length == 0)))){
          return true;
        }
      }
    },

    FieldPairs: {
      // if  exactly one field (of two) is not empty
      test: function (element, options) {
        if (!(element.get('value').length == 0 && options.fieldpair1.get('value').length > 0) || element.get('value').length > 0 && options.fieldpair1.get('value').length == 0){
          return true;
        }
      }
    },


    IsValidLength: {
      test: function (element, options) {
        if(!((element.get('value') == null) || (element.get('value').length == 0)))
          return element.get('value').length >= options.minLength;
        return true;
      }
    },

    IsValidMaxLength: {
      test: function (element, options) {
        if(!((element.get('value') == null) || (element.get('value').length == 0)))
          return element.get('value').length <= options.maxLength;
        return true;
      }
    },

    // File type validator (use with eg. filetypes:['txt','doc']  )

    IsValidUnsolicitedFileType: {
      test: function (element, options) {
        var result = true;
        if(!((element.get('value') == null) || (element.get('value').length == 0))){
          var filename=element.get('value').toLowerCase();
          var filetype = filename.substring(filename.lastIndexOf('.')+1);
          result = options.filetypes.contains(filetype);
        }
        return result;
      }
    },

    // Date validators for separate fields for day, month and year

    IsValidSepDate: {
      test: function (element, options) {
        var result = false;
        var day = options.dayField.get('value');
        var month = options.monthField.get('value');
        var year = options.yearField.get('value');

        if(year.length == 4 && month.length <= 2 && day.length <= 2){
          var date = new Date(year, month - 1, day);
          result = ((day == date.getDate()) && ((month - 1) == date.getMonth()) && (year == date.getFullYear()));
        }
        return result;
      }
    },

    IsSepDateLowerThen: {
      test: function (element, options) {
        var day = options.dayField.get('value');
        var month = options.monthField.get('value');
        var year = options.yearField.get('value');
        var date = new Date(year, month - 1, day);
        return date.getTime() >= options.minDate.getTime();
      }
    },

    IsSepDateGreaterThen: {
      test: function (element, options) {
        var day = options.dayField.get('value');
        var month = options.monthField.get('value');
        var year = options.yearField.get('value');
        var date = new Date(year, month - 1, day);
        return date.getTime() <= options.maxDate.getTime();
      }
    },

    SpecialChars: {
      test: function(element) {
        return (/^[-a-zA-Z0-9������� .`/]*$/.test(element.get('value')));
      }
    }
  }
});
//*********************
//** END Validator **
//*********************

/**************************************************
START Internal user functions
**************************************************/
// Login/Logoff and redirect to current page
multiChannel.logon = function(redirect) {
	if(Cookie.read('MC_USERTYPE') == 'R') {
		multiChannel.sso.logoutUser(multiChannel.evaluateLogout);
	}
	else {
	    var rParameter = multiChannel.getParameter('redirectURL');
		if(rParameter){	
			window.location.href = multiChannel.context.getLogonURL(rParameter, '', redirect);
		}else{
			window.location.href = multiChannel.context.getLogonURL(document.URL, '', redirect);
		}
	}
};


// START Login handling
multiChannel.login = function() {
	if(multiChannelLogon.validator.validate(true)) {
		var email = $('logon_email').get('value');
		var password = $('logon_password').get('value');
		multiChannel.sso.logonUser(email, password, multiChannel.evaluateLogin);
		return true;
	} else {
		return false;
	}
};

// Callback after login failed/succeeds
multiChannel.evaluateLogin = function() {
	if(multiChannel.sso.requestSuccessful()) {
		var redirectUrl;
		if(multiChannel.getParameter('redirectURL')) {
			var rUrl = multiChannel.getParameter('redirectURL');
			var anchorIndex = rUrl.indexOf('#');
			if(anchorIndex != -1) {
				rUrl = rUrl.substring(0, anchorIndex);
			}
			redirectUrl =  new URI(rUrl);
			if(redirectUrl.get('host') != multiChannel.context.domain) {
				var userTokenArray = multiChannel.sso.getUserToken();
				if(userTokenArray) {
					var parameters = new Object();						
					parameters['userType'] = userTokenArray[0];
					parameters['ssoToken'] = userTokenArray[1];
					redirectUrl.setData(parameters, true);
				}
			}
		} else if(multiChannel.getParameter('URL') != null) {
			redirectUrl =  new URI(multiChannel.getParameter('URL'));
		} else {
			redirectUrl= new URI(multiChannel.context.personaldata);
		}
		redirectUrl.go();
	} else {
		multiChannelLogon.validator.addGlobalError(multiChannelLogon.backendError, $('logon_email'));
		//multiChannelLogon.validator.addLocalError(multiChannelLogon.backendError, $('logon_email'));
		return false;
	}
};
// END Login handling

// START Logout handling
// Callback after logout
multiChannel.evaluateLogout = function() {
	if(multiChannel.sso.requestSuccessful()) {
		var redirectUrl;
		var rUrl = document.URL;
		var anchorIndex = rUrl.indexOf('#');
		if(anchorIndex != -1) {
			rUrl = rUrl.substring(0, anchorIndex);
		}
		redirectUrl =  new URI(rUrl);
		if(redirectUrl.get('host') != multiChannel.context.domain) {
			var userTokenArray = multiChannel.sso.getUserToken();
			if(userTokenArray) {
				var parameters = new Object();						
				parameters['userType'] = userTokenArray[0];
				parameters['ssoToken'] = userTokenArray[1];
				redirectUrl.setData(parameters, true);
			}
		}
		redirectUrl.go();	
	}
};
// END Logout handling

// Jump to registration
multiChannel.jumpToRegistration = function(){ 
    var urlParameter = multiChannel.getParameter('URL'); 
    var tpParameter = multiChannel.getParameter('tpOrigin');
    var rParameter = multiChannel.getParameter('redirectURL');
    var redirect = multiChannel.getParameter('redirect');

    if(tpParameter && rParameter) {
            var url = multiChannel.context.register;
            url += '&redirectURL=' + escape(rParameter);
            url += '&tpOrigin=' + tpParameter;
            window.location.href = url;
    } else if(urlParameter) {
            window.location.href = multiChannel.context.getRegisterURL( multiChannel.getParameter('redirectURL'), multiChannel.getParameter('URL').split('?')[0], redirect); 
    } else {     		
            window.location.href = multiChannel.context.getRegisterURL( multiChannel.getParameter('redirectURL'), '', redirect); 
    } 
 
}; 

// START Registration handling
// Registration by our sso request
multiChannel.register = function() {
	if(multiChannelRegister.validator.validate(true)) {
		var logonId = $('register_email').get('value');
		var logonPassword = $('register_password').get('value');
		var logonPasswordVerify = $('register_password').get('value');
		var firstname = $('register_firstname').get('value');
		var lastname = $('register_lastname').get('value');
		var nickname = $('register_nickname').get('value');
		var persontitle = $('register_title').get('value');
		var businesstitle = $('register_businesstitle').get('value');
		var captchaInput = multiChannel.context.captchaEnabled? captchaInput = $('register_captcha_input').get('value'): '';
		var rememberme = $('register_remember').get('checked');
		/*var newsletter = $('register_newsletter').get('checked');
		var params = new Hash({'displayName' : nickname, 'personTitle' : persontitle, 'rememberMe' : rememberme, 'subscribeNewsletter' : newsletter});*/
		var params = new Hash({'displayName' : nickname, 'personTitle' : persontitle, 'rememberMe' : rememberme, 'businessTitle' : businesstitle});
		multiChannel.sso.registerUser(logonId, logonPassword, logonPasswordVerify, firstname, lastname, captchaInput, multiChannel.evaluateRegistration, params);
	} else {
		// Load a new capture
		multiChannel.loadNewCaptcha($('captchaImage'), $('register_captcha_input'));
	}
};
// Callback after registration failed/succeeds
multiChannel.evaluateRegistration = function() {
	if(multiChannel.sso.requestSuccessful()) {
		var redirectUrl;
		if(multiChannel.getParameter('redirectURL')) {
			redirectUrl =  multiChannel.getParameter('redirectURL');
		} else if(multiChannel.getParameter('URL')) {
			redirectUrl =  multiChannel.getWebpath() + multiChannel.getParameter('URL');
		} else {
			redirectUrl= multiChannel.context.masterdata;
		}
		if(multiChannel.getParameter('tpOrigin')) {
			var userTokenArray = multiChannel.sso.getUserToken();
			if(userTokenArray) {
				redirectUrl += '&userType='+ userTokenArray[0];
				redirectUrl += '&ssoToken='+userTokenArray[1];
			}
		}
		
		window.location = redirectUrl;
	} else {
		if(multiChannel.sso.getStatusCode() == 'SSO_ERROR_SYSTEM') {
			multiChannelRegister.validator.addGlobalError(multiChannel.global.errorMessage1, $('logon_email'));
		} else if(multiChannel.sso.getStatusCode() == 'SSO_ERROR_MISSING_PARAM') {
			multiChannelRegister.validator.addGlobalError(multiChannelRegister.backendErrorEmptyEmail, $('register_email'));
			//multiChannelRegister.validator.addLocalError(multiChannelRegister.backendErrorEmptyEmail, $('register_email'));
		} else if(multiChannel.sso.getStatusCode() == 'SSO_ERROR_ALREADY_REGISTERED') {
			multiChannelRegister.validator.addGlobalError(multiChannelRegister.backendErrorExistingEmail, $('register_email'));
			//multiChannelRegister.validator.addLocalError(multiChannelRegister.backendErrorExistingEmail, $('register_email'));
		} else if(multiChannel.sso.getStatusCode() == 'SSO_ERROR_NICKNAME_EXISTS') {
			multiChannelRegister.validator.addGlobalError(multiChannelRegister.backendErrorExistingNickname, $('register_nickname'));
			//multiChannelRegister.validator.addLocalError(multiChannelRegister.backendErrorExistingNickname, $('register_nickname'));
		} else if(multiChannel.sso.getStatusCode() == 'SSO_AUTHENTICATION_MINIMUMLENGTH_PASSWORD') {
			multiChannelRegister.validator.addGlobalError(multiChannelRegister.backendErrorMinimumLengthPassword, $('register_password'));
			//multiChannelRegister.validator.addLocalError(multiChannelRegister.backendErrorMinimumLengthPassword, $('register_password'));
		} else if(multiChannel.sso.getStatusCode() == 'SSO_AUTHENTICATION_MAXCONSECUTIVECHAR_PASSWORD') {
			multiChannelRegister.validator.addGlobalError(multiChannelRegister.backendErrorMaxConsecutiveCharPassword, $('register_password'));
			//multiChannelRegister.validator.addLocalError(multiChannelRegister.backendErrorMaxConsecutiveCharPassword, $('register_password'));
		} else if(multiChannel.sso.getStatusCode() == 'SSO_AUTHENTICATION_MAXINTANCECHAR_PASSWORD') {
			multiChannelRegister.validator.addGlobalError(multiChannelRegister.backendErrorMaxInstanceCharPassword, $('register_password'));
			//multiChannelRegister.validator.addLocalError(multiChannelRegister.backendErrorMaxInstanceCharPassword, $('register_password'));
		} else if(multiChannel.sso.getStatusCode() == 'SSO_AUTHENTICATION_MINIMUMLETTERS_PASSWORD') {
			multiChannelRegister.validator.addGlobalError(multiChannelRegister.backendErrorMinimumLettersPassword, $('register_password'));
			//multiChannelRegister.validator.addLocalError(multiChannelRegister.backendErrorMinimumLettersPassword, $('register_password'));
		} else if(multiChannel.sso.getStatusCode() == 'SSO_AUTHENTICATION_MINIMUMDIGITS_PASSWORD') {
			multiChannelRegister.validator.addGlobalError(multiChannelRegister.backendErrorMinimumDigitsPassword, $('register_password'));
			//multiChannelRegister.validator.addLocalError(multiChannelRegister.backendErrorMinimumDigitsPassword, $('register_password'));
		} else if(multiChannel.sso.getStatusCode() == 'SSO_AUTHENTICATION_USERIDMATCH_PASSWORD') {
			multiChannelRegister.validator.addGlobalError(multiChannelRegister.backendErrorUserIdMatchPassword, $('register_password'));
			//multiChannelRegister.validator.addLocalError(multiChannelRegister.backendErrorUserIdMatchPassword, $('register_password'));
		} else if(multiChannel.sso.getStatusCode() == 'SSO_AUTHENTICATION_REUSEOLD_PASSWORD') {
			multiChannelRegister.validator.addGlobalError(multiChannelRegister.backendErrorReuseOldPassword, $('register_password'));
			//multiChannelRegister.validator.addLocalError(multiChannelRegister.backendErrorReuseOldPassword, $('register_password'));
		} else if(multiChannel.sso.getStatusCode() == 'SSO_AUTHENTICATION_INVALID_CAPTCHA') {
			multiChannelRegister.validator.addGlobalError(multiChannelRegister.backendErrorInvalidCaptcha, $('register_captcha_input'));
			//multiChannelRegister.validator.addLocalError(multiChannelRegister.backendErrorInvalidCaptcha, $('register_captcha_input'));
		}
		// Load a new capture
		multiChannel.loadNewCaptcha($('captchaImage'), $('register_captcha_input'));
	}
};
// END Registration handling

multiChannel.loadNewCaptcha = function(imageElement, inputElement) {
	var uri = new URI(imageElement.src);
	uri.setData({'refresh': $random(100000, 999999)}, true);
	imageElement.src = uri.toString();
	inputElement.value = '';
}

// START Wishlist
multiChannel.wishlist = new Object();

// Select all checkboxes
multiChannel.wishlist.selectAll = function(element) {
	var isChecked = element.checked;
	
	multiChannel.wishlist.checkAll(document.wishlist_form.select_all, isChecked);
	if (document.wishlist_form.select_catentry) {
		multiChannel.wishlist.checkAll(document.wishlist_form.select_catentry, isChecked);
	}
};

// Check all fields
multiChannel.wishlist.checkAll = function(field, isChecked) {
	if (field.length) {
		for (i = 0; i < field.length; i++) {
			field[i].checked = isChecked;
		}
	} else {
		field.checked = isChecked;
	}
};

// Perform an action for all checked wishlist items
multiChannel.wishlist.updateAll = function(field) {
	var actionType = field.value;
	
	if (actionType == 'delete') {
		var allCatentries = document.wishlist_form.select_catentry;
		
		if (allCatentries) {
			var url = new URI(multiChannel.context.wishlistdelete);
			var parameters = new Object();
			var checkedFound = false;
			
			parameters['URL'] = multiChannel.context.wishlist;
			url.setData(parameters, true);
			url = url.toString();
			
			if (allCatentries.length) {
				for (var i = 0; i < allCatentries.length; i++) {
					if (allCatentries[i].checked) {
						var catentryId = allCatentries[i].getAttribute('value');
						
						if (checkedFound) {
							url = url + '&catEntryId=' + catentryId;
						} else {
							url = url + catentryId;
							checkedFound = true;
						}
					}
				}
			} else {
				var catentryId = allCatentries.getAttribute('value');
				
				url = url + catentryId;
				checkedFound = true;
			}
			
			if (checkedFound) {
				window.location.href = url;
			}
		}
	}
};

multiChannel.addToWishList = function(catEntryId) {

	if (!$defined(catEntryId) || isNaN(catEntryId)) {
		return false;
	}

	if (Cookie.read('MC_USERTYPE') == 'R') {
		var uri = new URI(multiChannel.context.wishlistadd);

		if (multiChannel.getQueryString().indexOf('https') != -1) {
			uri = new URI(multiChannel.context.wishlistaddSSL);
		}

		var parameters = {
			'catEntryId': catEntryId,
			'URL': 'MultiChannelAjaxResponse'
		};
		uri.setData(parameters, true);
		var onsuccess = function() {
			multiChannel.openLayer('addedtowishlist', { 'catEntryId': catEntryId });
		};

		var request = new Request({
			method: 'post',
			url: uri.toString(),		
			onSuccess: onsuccess
		});
		request.send();
	} else {
		var uri = new URI();
		uri.setData({ 'wishlistEntry': catEntryId, 'l': 'addedtowishlist' }, true);
		var wishlistURI = new URI(multiChannel.context.wishlistadd);
		wishlistURI.setData({ 'catEntryId': catEntryId, 'URL': uri.toString() }, true);
		multiChannel.openLayer('loginforwishlist', { 'redirectUrl': wishlistURI.toString() });		
	}
};

multiChannel.wishlistLogin = function(redirectUrl){
	window.location.href = multiChannel.context.getLogonURL(redirectUrl, '', true);
};

// END Wishlist

/**************************************************
END Internal user functions
**************************************************/

/**************************************************
START basket functions
**************************************************/

multiChannel.basket = new Object();
// Updates the basket
multiChannel.basket.updateBasket = function() {
	if(multiChannel.sso.getJSON() && multiChannel.sso.getJSON().basket) {
		var basketUpdate = multiChannel.sso.getJSON().basket;
		multiChannel.basket.addBasketEntry(basketUpdate[0], basketUpdate[1], basketUpdate[2]);
	}
};

multiChannel.basket.addBasketEntry = function(id, count, basketURL) {
	if(id && count && basketURL) {
		var shoppingCartContent = $('shoppingCartContent');
		if(shoppingCartContent) {
      // Possible elements are predefined (24-7 or CEWE)
      var basketEntry = shoppingCartContent.getElementById(id);
      if(basketEntry) {
        if(count > 0) {
          // Show basket entry
          if(basketEntry && basketEntry.hasClass('hideContent')) {
            basketEntry.removeClass('hideContent');
          }
          // Update basket url
          var anchorEl = basketEntry.getElement('a');
          anchorEl.set('href', basketURL);
        } else {
          
          // Remove the link from the basket url
          var anchorEl = basketEntry.getElement('a');
          anchorEl.set('href', '#');
        }
        // Set the right count
        var countSpan = anchorEl.getElements('span')[1];
        countSpan.set('text', '(' + count + ')');
      }
      // Reformat all active entries and set total count
      multiChannel.basket.formatBasketEntries();
    }
  }
};

// Adds the right styles to our basket entries
multiChannel.basket.formatBasketEntries = function() {
		// Return if shopping cart is not displayed
		if ($('shoppingCartBox').getStyle('display') == 'none')
			return;
			
		// Add right css styles to basketEntries
		var shoppingCartContent = $('shoppingCartContent');
		var basketRows = shoppingCartContent.getElements('td');
		var counter = 0;
		var totalCount = 0;
		basketRows.each(function(basketRow, index){
			if(!basketRow.hasClass('hideContent')) {
				if(counter == 0) {
				 basketRow.removeProperties('class');
				 basketRow.addClass('tableBorderFirst');
				} else {
				 basketRow.removeProperties('class');
				 basketRow.addClass('tableBorder');
				}
				var countText = basketRow.getElements('span')[1].get('text');
				var count = countText.replace('(', '').replace(')', '');
				if(count && count.length > 0) totalCount += parseInt(count);
				counter++;
			}
		});
		var basketTotalCount = $('shoppingCartCounterText');
		if(!totalCount) totalCount = 0;
		basketTotalCount.set('text', totalCount);
		multiChannel.basket.processBasketSlider();
};

multiChannel.basket.processBasketSlider = function() {
		var toggleElement = $('shoppingCartBox');
		// Check if shopping cart is in use
		if (toggleElement.getStyle('display') == 'none')
			return;
		
		var contentElement = toggleElement.getNext('div');

		// Effect exists. Remove it first!
		if(contentElement.get('id') != 'shoppingCartContent') {
			contentElement = $('shoppingCartContent').clone(true, true);
			// Remove effect style
			contentElement.erase('style');
			var targetEl = $('shoppingCartContent').getParent();
			var effectEl = targetEl.getParent();
			targetEl.dispose();
			effectEl.dispose();
			var toggler = $('shoppingCartToggler');
			toggler.dispose();
			contentElement.inject(toggleElement, 'after');
		}
		var togglerElement = new Element('div', {
			'id': 'shoppingCartToggler',
			'class': 'boxCollapse'
		});
		toggleElement.grab(togglerElement);
		
		var targetElement = new Element('div', {
			'class': 'sliderTarget',
			'styles': {
				'height': contentElement.offsetHeight
			}
		});

		targetElement.wraps(contentElement);

		var fx = new Fx.Slide(
			contentElement,
			{
				duration: contentElement.offsetHeight * 5,
				transition: Fx.Transitions.Bounce.easeOut,
				onStart: function() {
					togglerElement.toggleClass('boxCollapse');
					togglerElement.toggleClass('boxExpand');
				},
				onComplete: function() {
					if(togglerElement.hasClass('boxExpand')){
						targetElement.setStyles({'display': 'none'});
						targetElement.setStyles({'visibility': 'hidden'});
					}
				}
				
			});
			
		//Fix Fx.Slide-bug not setting height initially
		targetElement.setStyle('height', contentElement.offsetHeight);

		//Fix IE6 misbehaviour
		if (Browser.Engine.trident && Browser.Engine.version == 4) {
			contentElement.setStyle('top', 0);
		}
		
		togglerElement.removeEvent('click');
		togglerElement.addEvent('click', function(e) {
			e = new Event(e);
			if(targetElement.getStyle('display') == 'none'){
				targetElement.setStyles({'display': ''});
				targetElement.setStyles({'visibility': 'visible'});
			}
			fx.toggle();
			e.stop();
		});
		
/*
		$('shoppingCartLink').removeEvent('click');
		$('shoppingCartLink').addEvent('click', function(e) {
			e = new Event(e);
			if(targetElement.getStyle('display') == 'none'){
				targetElement.setStyles({'display': ''});
				targetElement.setStyles({'visibility': 'visible'});
			}
			fx.toggle();
			e.stop();
		});
*/
	
		// Close this slider
		fx.toggle();
};

multiChannel.minibasket = new Object();
multiChannel.minibasket.addToBasket = function(catEntryId, quantity, wishlist){
	
	var q = 1;
	var url = multiChannel.context.addtobasket;
	if(multiChannel.getQueryString().indexOf('https') != -1) url = multiChannel.context.addtobasketSSL;
	
	if(quantity && quantity != null && !isNaN(quantity) && quantity > 1) q = quantity;
	
	var eId = '';
	var entryId = multiChannel.getParameter('catEntryId');
	if(catEntryId  && !isNaN(catEntryId)) eId = catEntryId;
	else if(entryId != null && !isNaN(entryId)) eId = entryId;
	else return false;
	
	var uri = new URI(url);

	var uri2 = new URI('MultiChannelAjaxViewBasket');
	var parameters2 = {
		storeId : uri.getData('storeId')
	}
	uri2.setData(parameters2, true);
	
	var state = "in";
	if($defined($('miniBasketState'))) state = $('miniBasketState').get('value');
	
	var parameters = {
		URL:  uri2.toString(),
		quantity : q,
		merge: '*',
		catEntryId: eId,
		isWishList: (wishlist ? 'true' : 'false'),
		state: state
	}
	
	options = false;
	if(wishlist){
		options = {
			'onsuccess': multiChannel.minibasket.removeItemFromWishlist(catEntryId)
		}
	}

	uri.setData(parameters, true);
	multiChannel.minibasket.request(uri, options);	
};

multiChannel.minibasket.removeItemFromWishlist = function(catEntryId){
	$('catentry_'+catEntryId).style.display = 'none';
	$('pagedInterestListCount').value = ($('pagedInterestListCount').value - 1);
	if($('pagedInterestListCount').value < 1){
		$('pagedInterestList').style.display = 'none';
		$('pagedInterestListEmpty').style.display = '';
	}	
}

multiChannel.minibasket.updateSSOEntry = function(){
	var items = 0;
	var aDiv = $('shoppingBasketContent').getElements('div');
	
	for(i=0; i<aDiv.length; i++){
		if(aDiv[i].className == 'shoppingBasketItemContainer'){
			items++;
		}
	}

	var url = multiChannel.context.updatebasket;
	var uri = new URI(url);

	var uri2 = new URI('MultiChannelDisplayBasket');
	var parameters2 = {
		storeId : uri.getData('storeId')
	}
	uri2.setData(parameters2, true);
	multiChannel.sso.refreshBasket(
		'onlineshop',
		items,
		uri2.toString(),
		function() {}
		);
}


multiChannel.minibasket.updateBasket = function(orderItemId, quantity){
	if(quantity && quantity > 0){
		var q = quantity;
		
		var url = multiChannel.context.updatebasket;
		var uri = new URI(url);
		var oId = '';
		if(orderItemId  && !isNaN(orderItemId)) oId = orderItemId;
		else return false;
		
		var uri3 = new URI('MultiChannelAjaxViewBasket');
		var parameters3 = {
			storeId : uri.getData('storeId')
		}
		uri3.setData(parameters3, true);
		
		var uri2 = new URI('MultiChannelDisplayBasket');
		var parameters2 = {
			URL: uri3.toString(),
			storeId : uri.getData('storeId')
		}
		uri2.setData(parameters2, true);
		
		var state = "in";
		if($defined($('miniBasketState'))) state = $('miniBasketState').get('value');
		
		var parameters = {
			URL:  uri2.toString(),
			quantity : q,
			storeId : uri.getData('storeId'),
			orderItemId: oId,
			state: state
		}
		uri.setData(parameters, true);
		multiChannel.minibasket.request(uri);
	}
};

multiChannel.minibasket.deleteFromBasket = function(orderItemId){
	
	var url = multiChannel.context.deletefrombasket;
	var uri = new URI(url);
	
	var oId = '';
	if(orderItemId  && !isNaN(orderItemId)) oId = orderItemId;
	else return false;
	
	var uri3 = new URI('MultiChannelAjaxViewBasket');
	var parameters3 = {
		storeId : uri.getData('storeId')
	}
	uri3.setData(parameters3, true);
	
	var uri2 = new URI('MultiChannelDisplayBasket');
	var parameters2 = {
		URL: uri3.toString(),
		storeId : uri.getData('storeId')
	}
	uri2.setData(parameters2, true);
	
	var state = "in";
	if($defined($('miniBasketState'))) state = $('miniBasketState').get('value');

	var parameters = {
		URL:  uri2.toString(),
		storeId : uri.getData('storeId'),
		orderItemId: oId,
		state: state
	}
	uri.setData(parameters, true);
		
	multiChannel.minibasket.request(uri);
};

multiChannel.minibasket.request = function(uri, options){
	var onsuccess = function(response){};
	if(options){
		if(options.onsuccess) onsuccess = options.onsuccess;
	}
	
	new Request.HTML({
		method: 'post',
		url: uri.toString(),
		update: $('shoppingBasketContent'),
		onRequest: function(){},
		onSuccess: onsuccess
	}).send();
};

/**************************************************
END basket functions
**************************************************/


multiChannel.counterUrl = 'MultiChannelCounterInc';

multiChannel.processCounter = function() {
	var counterId = '';
	$('content').getElements('div[class*=counterId]').each(function(element) {
		counterId += element.className.replace(/^.*counterId([0-9]*).*$/, '$1') + ',';
	});
	if (counterId.length > 0) {
		counterId = counterId.substring(0, counterId.length - 1);
		var counterRequest = new Request({method: 'get', url: multiChannel.counterUrl});
		counterRequest.send('counterId=' + counterId);
	}
};

multiChannel.processTeaser = function() {
	$('content').getElements('div[class*=counterId]').each(function(element) {
		if(element.get('title')) multiChannel.addToMetaTag("WT.ad",element.get('title'));
	});
};

multiChannel.showPrintView = function() {
	$('contentMiddle').addClass('print');
};

multiChannel.addPaginatorInfo = function(id, url) {
	var manufacturerIdValue = multiChannel.getParameter('manufacturerId');
	var hitsValue = multiChannel.getParameter('hits');
	var sortValue = multiChannel.getParameter('sort');
	var posValue = multiChannel.getParameter('pos');
	var displayValue = multiChannel.getParameter('display');
	
	$(id).getElements('a[href^=' + url + ']').each(function(aElement) {
		var href = new URI(aElement.href);
		var data = href.get("data");
		var pos = data['pos']? data['pos']: posValue;
		var disp = data['display']? data['display']: displayValue;
		href.setData({manufacturerId: manufacturerIdValue, hits: hitsValue, sort: sortValue, pos: pos, display: displayValue}, true);
		aElement.href = href.toURI();
	});
};

multiChannel.addPaginatorInfoCatalogEntry = function(url, position) {
	var manufacturerIdValue = multiChannel.getParameter('manufacturerId');
	var sortValue = multiChannel.getParameter('sort');
	var displayValue = multiChannel.getParameter('display');

	$('catalogEntryList').getChildren('div.catalogEntry').each(function(catalogEntryElement) {

		catalogEntryElement.getElements('a[href^=' + url + ']').each(function(aElement) {
			var href = new URI(aElement.href);
			var data = href.get("data");
			var disp = data['display']? data['display']: displayValue;
			href.setData({manufacturerId: manufacturerIdValue, sort: sortValue, pos: position, display: displayValue}, true);
			aElement.href = href.toURI();
		});
		position++;
	});
};

multiChannel.addPaginatorInfoInfield = function(url, display) {
	var manufacturerIdValue = multiChannel.getParameter('manufacturerId');
	var sortValue = multiChannel.getParameter('sort');
	var displayValue = display;

	$$('#contentMiddle .categoryManifold').each(function(categoryManifold) {
		categoryManifold.getElements('a[href^=' + url + ']').each(function(aElement) {
			var href = new URI(aElement.href);
			var data = href.get("data");
			var disp = data['display']? data['display']: displayValue;
			href.setData({manufacturerId: manufacturerIdValue, sort: sortValue, display: displayValue}, true);
			aElement.href = href.toURI();
		});
	});
};

multiChannel.changeCompProduct = function(catId, eId) {
	if(catId && catId.length > 0 && eId && eId.length > 0){
		var categoryId = catId;
		if(multiChannel.getParameter('categoryId')) {
			categoryId =  multiChannel.getParameter('categoryId');
		}
		var cId = 'comp_' + eId;
		if( $defined( $(cId) )  ){
			if( $(cId).checked == true  ){
				multiChannel.updateCompProducts(categoryId, eId);
			} else {
				multiChannel.updateCompProducts(categoryId, eId, {mode:'delete'});
			}

		}		
	}
};


multiChannel.updateCompProducts = function(catId, entries, options) {
	if(catId && catId.length > 0){
		var req = new Request.JSON({
			method: 'post',
			url: 'MultiChannelUpdateCompProducts',
			onSuccess: function(request){
				if(options && options.fn) options.fn(request);
			}
		});
		var sendStr = "entryIds=" + entries + "&catId=" + catId;
		if(options && options.mode) sendStr += "&mode=" + options.mode;
		req.send(sendStr);
	}
};

multiChannel.selectCompProduct = function(entryId){
	if( $defined( $('comp_' + entryId) )) {
		$('comp_' + entryId).checked = true;
	}
};

multiChannel.removeAllCompProducts = function(catId) {
	multiChannel.removeCompProduct(catId);
};

multiChannel.removeCompProduct = function(catId, entryId) {
	var eId = '';
	var options = new Object();
	options.mode = 'replace';
	options.fn = multiChannel.closeAndRemoveLayer('comparison');
	if(entryId && entryId.length > 0) {
		eId = entryId;
		if( $defined( $('comp_' + entryId) )) $('comp_' + entryId).checked = false;
		options.mode = 'delete';
		options.fn = function(request){
			if(request){
				for(i=0;i<request.entries.length;i++){
					if(request.entries[i] == catId){
						multiChannel.closeAndReopenLayer('comparison', {categoryId:catId});
						break;
					}
				};
				
			}
		}
	} else {
		$$('#catalogEntryList .catalogEntry .comparable').each(function(item){
			item.checked = false;
		});
	}
	multiChannel.updateCompProducts(catId, eId, options);
};

//***************
// Events Handling 
//***************
multiChannel.event_detail_id = null;

multiChannel.expandEvent = function(id){
	if (multiChannel.event_detail_id != null){
		multiChannel.setEventVisible(multiChannel.event_detail_id, false)
	}
	multiChannel.setEventVisible(id, true);
	multiChannel.event_detail_id = id;
};

multiChannel.setEventVisible = function (id, visible){
	var event_detail = $('event_detail_' + id);
	var event_summary = $('event_summary_' + id);
	if (visible){
		event_detail.style.visibility = 'visible';
		event_detail.style.display = 'block';
		event_summary.style.visibility = 'hidden';
		event_summary.style.display = 'none';
	} else {
		event_detail.style.visibility = 'hidden';
		event_detail.style.display = 'none';
		event_summary.style.visibility = 'visible';
		event_summary.style.display = 'block';
	}
};

// ******************************************************************
// helper-method to add contents to a webtrends-meta-tag contents
// ******************************************************************
multiChannel.addToMetaTag=function (lvName, content) {
  var metaElement = null;
  var metaElements = $$('meta[name='+lvName+']');
  var ct = null;
  var se =';';
  
  
  var contentRightElement = $('contentRight');
  
	if (!$defined(contentRightElement)||(contentRightElement.getStyle('display')!="none")) {
	
		metaElement = metaElements == null ? null : metaElements[0];

		if (metaElement ==null) {
			metaElement = new Element('meta', {'name':lvName,'content':''});
			$$('head')[0].appendChild(metaElement);
			se='';
		}
		ct =(content!=null)? content.toLowerCase():content;
		
		if (ct!=null && ct != '') {  
			ct = ct.replace(/&/g, "+");
			ct = ct.replace(/=/g, ":");
			ct = ct.replace(/;/g, ":");
			if(ct.length>64) {
				ct = ct.substr(0, 64);
			}
		}

		var ct_tmp= metaElement.get('content')==null?'':metaElement.get('content');
		metaElement.set('content', ct_tmp+se+ct);
	}
}

// ******************************************************************
// helper-method to change imageMap area tags
// ******************************************************************
function changeAreaHrefsOfImageMap(mapName, wtac) {
  var mapElement = document.getElementsByName(mapName);
  var areaElements = mapElement[0].areas;
  var areaLength = mapElement[0].areas.length;
  for(var i=0;i<areaLength;i++){
    if(areaElements[i].href.length>0){  	  
      if(areaElements[i].href.indexOf("?") !=-1){
        areaElements[i].href=areaElements[i].href+'&'+wtac;
      }else{
        areaElements[i].href=areaElements[i].href+'?'+wtac;
      }
    }
  }
}

// ******************************************************************
// MyAccount MasterData stuff
// ******************************************************************

multiChannel.masterData = new Object();

multiChannel.masterData.removeShippingAddress = function(index) {
	var table = $('shipping' + index + '_table');
	table.setStyle('display', 'none');
	$('shipping' + index + '_ignore').set('value', 'true');
};

multiChannel.masterData.setAge = function(target, ageYear, ageMonth, ageDay) {
	var dateString = "" + ageYear.get('value') + ageMonth.get('value') + ageDay.get('value');
	if (dateString.match("^[A-Za-z]+$")) {
		target.set('value', '');
	} else {
		target.set('value', dateString);
	}
};

multiChannel.masterData.setFields = function() {
	multiChannel.masterData.setAge($('age'), $('masterdata_birthday_year'), $('masterdata_birthday_month'), $('masterdata_birthday_day'));
	var rememberMe = $('rememberMe').checked;
	if(rememberMe) {
	  $('demographicField4').set('value', '1');
	}
};

multiChannel.masterData.clearErrorClassesBelow = function(node) {
	if (node.hasClass('error')) {
		node.removeClass('error');
	}
	var elements = node.getChildren();
	elements.each(multiChannel.masterData.clearErrorClassesBelow);
};

multiChannel.masterData.clearErrorClasses = function() {
	multiChannel.masterData.clearErrorClassesBelow($('RegistrationUpdateForm'));
	multiChannel.fitSite();
};

multiChannel.masterData.fieldErrorTable = new Hash({
	'age': function(value) {
		$('masterdata_birthday_day').addClass('error');
		$('masterdata_birthday_month').addClass('error');
		$('masterdata_birthday_year').addClass('error');
	}
});

multiChannel.masterData.displayFieldErrors = function(errors) {
	$each(errors, function(value, key) {
		if (multiChannel.masterData.fieldErrorTable.has(key)) {
			(multiChannel.masterData.fieldErrorTable.get(key))(value);
		} else {
			var field = $(key);
			if (field != null) {
				field.addClass('error');
			}
		}
	});
};

multiChannel.masterData.displayErrorBox = function(errors) {
	var list = $('errorBox_messages');
	list.empty();
	$each(errors, function(value) {
		var elem = new Element('li', { text: value });
		elem.inject(list);
	});
	$('errorBox').removeClass('globalErrorBoxHideout');
	multiChannel.fitSite();
};

multiChannel.masterData.removeExistingShippingAddressResponse = function(response, index) {
	if (response != null && response.success == "false") {
		displayErrorBox(response.errors);
	} else {
		multiChannel.masterData.removeShippingAddress(index);	
	}
};

multiChannel.masterData.removeExistingShippingAddress = function(index) {
	var element = $('shipping' + index + '_id');
	if (element != null) {
		var id = element.get('value');
		if (id != null) {
			var request = new Request({
				url: 'MultiChannelMAMasterDataRemoveAddress',
				onSuccess: function(response) {
					multiChannel.masterData.removeExistingShippingAddressResponse(response, index);
				}
			});
			var storeId = multiChannel.getParameter('storeId');
			request.send({
				data: 'URL=MultiChannelMAMasterDataRemoveAddress&storeId=' + storeId + '&addressId=' + id
			});
		}
	}
};

multiChannel.masterData.replaceShippingAddressIds = function(element) {
	var elements = element.getChildren();
	elements.each(multiChannel.masterData.replaceShippingAddressIds);
	var addressCount = multiChannel.masterData.shippingAddressCount;
	var id = element.get('id');
	if (id != null) {
		element.set('id', id.substitute({ IDX: addressCount }));
		if (id.test('_remove$')) {
			element.addEvent('click', function() {
				multiChannel.masterData.removeShippingAddress(addressCount);
				return false;
			});
		}
	}
	var name = element.get('name');
	if (name != null) {
		element.set('name', name.substitute({ IDX: addressCount }));
	}
	if (elements.length == 0) {
		var text = element.get('text');
		if (text != null) {
			element.set('text', text.substitute({ IDX: addressCount }));
		}
	}
};

multiChannel.masterData.addShippingAddress = function() {
	var elem = $('masterdata_extra_shipping');
	var cloned = elem.clone(true, true);
	cloned.set('id', 'shipping' + multiChannel.masterData.shippingAddressCount + '_table');
	multiChannel.masterData.replaceShippingAddressIds(cloned);
	cloned.setStyle('display', 'block');
	cloned.injectBefore('masterdata_submit_button');
	multiChannel.masterData.shippingAddressCount++;
	multiChannel.fitSite();
};

multiChannel.masterData.updateIds = function(data) {
	if (data != null) {
		$each(data, function(value, key) {
			var elem = $(key);
			if (elem != null) {
				elem.set('value', value);
			}
		});
	}
};

multiChannel.masterData.handleResponse = function(rsp) {
	var response = JSON.decode(rsp);
	multiChannel.masterData.clearErrorClasses();
	if (response != null && response.success == "true") {
		$('errorBox').addClass('globalErrorBoxHideout');
		multiChannel.openLayer('personaldatasaved');
		multiChannel.masterData.updateIds(response.data);
	} else if (response != null && response.errors != null) {
		multiChannel.masterData.displayFieldErrors(response.errors);
		multiChannel.masterData.displayErrorBox(response.errors);
	} else {
		errors = new Hash({ 'error': 'An application error has occurred.' });
		multiChannel.masterData.displayErrorBox(errors);
	}
	multiChannel.fitSite();
};

multiChannel.masterData.addressFieldMapping = {
	'shipping1_firstname': 'firstName',
	'shipping1_lastname': 'lastName',
	'shipping1_street': 'address1',
	'shipping1_zip': 'zipCode',
	'shipping1_city': 'city',
	'shipping1_address2': 'address2'
};

multiChannel.masterData.handleSameAddressCheckbox = function() {
	var elem = $('masterdata_same_address');
	if (elem != null) {
		var mapping = multiChannel.masterData.addressFieldMapping;
		if (elem.checked) {
			for (id in mapping) {
				var source = $(mapping[id]);
				var target = $(id);
				target.setProperty('readonly', 'readonly');
				target.set('value', source.get('value'));
				target.addClass('inp-disabled');
			}
		} else {
			for (id in mapping) {
				var target = $(id);
				target.removeProperty('readonly');
				target.removeClass('inp-disabled');
			}
		}
	}
};

multiChannel.masterData.updateData = function(event) {
	multiChannel.masterData.setFields();
	
	var sameAddress = $('masterdata_same_address');
	if (sameAddress != null && sameAddress.checked) {
		var mapping = multiChannel.masterData.addressFieldMapping;
		for (id in mapping) {
			var source = $(mapping[id]);
			var target = $(id);
			target.set('value', source.get('value'));
		}
	}
	var form = $('RegistrationUpdateForm');
	form.set('send', { onComplete: multiChannel.masterData.handleResponse });
	form.send();
	return false;
};

// ******************************************************************
// Contact form
// ******************************************************************
multiChannel.contact = new Object();

multiChannel.contact.init = function() {
	var contactType = document.getElementsByName('contact_type');
	for (var i = 0; i < contactType.length; i++){
		var parent = $(contactType[i].parentNode);
		if (parent.isDisplayed()){
			contactType[i].checked = true;
			break;
		}
	}
}

multiChannel.contact.preselect = function(id) {
	if ($defined($(id))) {
		$(id).checked = true;
	}
}

multiChannel.contact.send = function() {
	var view = $('view');	

	var form = new Element('form');
	form.method = 'post';
	form.action = multiChannel.context.contact;
	form.target = 'contact_sender';
	form.enctype = 'multipart/form-data';
	view.appendChild(form);	
	
	multiChannel.contact.values.each(function(value, key){
		multiChannel.contact.createInputHidden(form, key, value);
	});

	form.submit();
};

multiChannel.contact.submit = function() {
	var addressPart = $('contact_part_address');
	var addressPartVisible = addressPart.style.display == 'inline';
	var messagePart = $('contact_part_message');
	var storePart = $('contact_part_store');
	var successPart = $('contact_part_success');
	
	if (addressPartVisible && multiChannel.contact.validateAddress()){
		multiChannel.contact.showMessagePart(addressPart, messagePart, storePart);
	} else if (!addressPartVisible && multiChannel.contact.validateMessageAndStore(storePart)) {
		multiChannel.contact.showSuccessPart(successPart, messagePart, storePart);
		multiChannel.contact.send();
	}
}

multiChannel.contact.showMessagePart = function(addressPart, messagePart, storePart){
	var type = multiChannel.contact.getRadioButtonValue(document.getElementsByName('contact_type'));

	// Hide address part
	addressPart.style.display = 'none';
	
	// Change title
	var title = $('contact_title');
	title.innerHTML = multiChannel.contact.titles[type];
	
	// Enable store selection (if needed)
	if (multiChannel.contact.storeSelections[type]){
		$('contact_store_entity_id').getChildren('option')[0].dispose();
		storePart.style.display = 'inline';
	}
	
	// Enable message part
	messagePart.style.display = 'inline';
	
	// Update iframe
	multiChannel.fitLayer();	
};

multiChannel.contact.showSuccessPart = function(successPart, messagePart, storePart){
	// Hide store part
	storePart.style.display = 'none';

	// Hide message part
	messagePart.style.display = 'none';
	
	// Hide submit part
	var submitPart = $('contact_submit_part');
	submitPart.style.display = 'none';
	
	// Change title
	var title = $('contact_title');
	title.style.display = 'none';
	
	var successTitle = $('contact_success_title');
	successTitle.style.display = 'block';
	
	// Enable success part
	successPart.style.display = 'inline';
	
	// Enable redirect to home button
	$('contact_redirect_home').style.display = 'block';
	
	// Update iframe
	multiChannel.fitLayer();	
};

multiChannel.contact.validateAddress = function() {
	var validator = new multiChannel.validator.FormValidator('contact_table', multiChannel.contact.fieldOptions, 'errorBoxContact', multiChannel.global.errorMessage1, multiChannel.global.errorMessageN);
	if (validator.validate(true)){
		multiChannel.contact.values = new Hash();
		multiChannel.contact.values['title'] = multiChannel.contact.getRadioButtonValue(document.getElementsByName('contact_title'));	
		multiChannel.contact.values['firstName'] = $('contact_firstname').value;
		multiChannel.contact.values['lastName'] = $('contact_lastname').value;
		multiChannel.contact.values['phoneAreaCode'] = $('contact_phone_area_code').value;
		multiChannel.contact.values['phoneNumber'] = $('contact_phone_number').value;
		multiChannel.contact.values['email'] = $('contact_email').value;
		multiChannel.contact.values['type'] = multiChannel.contact.getRadioButtonValue(document.getElementsByName('contact_type'));	
		return true;
	}
	return false;
};

multiChannel.contact.validateMessageAndStore = function(storePart) {
	var validator = new multiChannel.validator.FormValidator('contact_message_table', multiChannel.contact.messageFieldOptions, 'errorBoxContactMessage', multiChannel.global.errorMessage1, multiChannel.global.errorMessageN);
	if (validator.validate(true)){
		multiChannel.contact.values['message'] = $('contact_message').value;
		if (storePart.style.display == 'inline') {
			multiChannel.contact.values['storeEntityId'] = multiChannel.contact.getSelectValue($('contact_store_entity_id'));
		} else {
			multiChannel.contact.values['storeEntityId'] = '';
		}
		return true;
	}

	return false;
};

multiChannel.contact.getRadioButtonValue = function(radioButton) {
	for (var i = 0; i < radioButton.length; i++){
		if (radioButton[i].checked)
			return radioButton[i].value;
	}
	return null;
};

multiChannel.contact.getSelectValue = function(select) {
	for (var i = 0; i < select.options.length; i++){
		if (select.options[i].selected)
			return select.options[i].value;
	}
	return null;
};

multiChannel.contact.createInputHidden = function(form, key, value){
	var hidden = new Element('input');
	hidden.type = 'hidden';
	hidden.name = key;
	hidden.value = value;
	
	form.appendChild(hidden);
};

multiChannel.initCategoryPage = function(notAvailableOnline, notAvailableLocal) {

	if (notAvailableOnline) {
		$('categoryOnlineErrorBox').show();
	}

	if (notAvailableLocal) {
		$('categoryMarketErrorBox').show();
	}

	if ($defined($('brandFilter'))) {
		var manufacturerId = multiChannel.getParameter('manufacturerId');

		if ($defined(manufacturerId)) {
			$('manufacturerId').value = manufacturerId;
		}
	} else if ($defined($('catalogFilter'))) {
		var display = multiChannel.getParameter('display');

		if ($defined(multiChannel.context.outletId)) {

			switch (display) {
			case 'online':
				$('onlineMarketFilter').disabled = true;
				$('localMarketFilter').checked = false;
				break;
			case 'local':
				$('localMarketFilter').disabled = true;
				$('onlineMarketFilter').checked = false;
				break;
			}

			if (notAvailableOnline) {
				$('onlineMarketFilter').checked = false;
				$('onlineMarketFilter').disabled = true;
				$('localMarketFilter').checked = true;
				$('localMarketFilter').disabled = true;
			} else if (notAvailableLocal) {
				$('onlineMarketFilter').checked = true;
				$('onlineMarketFilter').disabled = true;
				$('localMarketFilter').checked = false;
				$('localMarketFilter').disabled = true;
			}
			$('storeSelect').value = multiChannel.context.storeId;
		} else {
			$('onlineMarketFilter').disabled = true;
			$('localMarketFilter').checked = false;
		}
	}
};

multiChannel.setStoreFilter = function() {
	var storeId = $('storeSelect').value;
	
	if (storeId != -1) {
		var display = 'all';

		if ($('onlineMarketFilter').checked && !$('localMarketFilter').checked) {
			display = 'online';
		} else if (!$('onlineMarketFilter').checked && $('localMarketFilter').checked) {
			display = 'local';
		}
		multiChannel.setParameters({'storeId': storeId, 'display': display});
	}
};

multiChannel.selectCompareProducts = function(catalogEntryIds) {
	$$('#catalogEntryList .catalogEntry .comparable').each(function(item) {
		item.checked = false;
	});

	for (var i = 0; i < catalogEntryIds.length; i++) {
		checkboxElement = $('comp_' + catalogEntryIds[i]);

		if ($defined(checkboxElement)) {
			checkboxElement.checked = true;
		}
	}
};

multiChannel.showProductDetailTab = function(tab) {

	if ($defined($(tab))) {

		$$('#infoTabNav li').each(function(item) {
			item.removeClass('selected');
		});

		$$('#infoTabContent .catalogEntryTab').each(function(item) {
			item.addClass('hideout');
		});
		$(tab).addClass('selected');
		$(tab.replace(/ref_/, 'tab_')).removeClass('hideout');
	}
};

multiChannel.correctCategoryId = function() {
	var categoryId = multiChannel.getParameter('categoryId');

	$('catalogEntryList').getChildren('div[id^=catentry]').each(function(divElement) {

		divElement.getElements('a').each(function(aElement) {

			if (aElement.href != new URI().toString() + '#') {
				var uri = new URI(aElement.href);

				if ($defined(uri.getData('categoryId')) && uri.getData('categoryId') != categoryId) {
					uri.setData({ 'categoryId': categoryId }, true);
					aElement.href = uri.toString();
				} else if ($defined(uri.getData('redirectURL'))) {
					var redirectUri = new URI(uri.getData('redirectURL'));

					if ($defined(redirectUri.getData('categoryId')) && redirectUri.getData('categoryId') != categoryId) {
						redirectUri.setData({ 'categoryId': categoryId }, true);
						uri.setData({ 'redirectURL': redirectUri.toString() }, true);
						aElement.href = uri.toString();
					}
				}
			}
		});
	});
};

multiChannel.search = new Object();

multiChannel.search.checkSubmitSearchQuery = function(formnode) {
	if($('searchQuery').value.length > 0){
		formnode.submit();
	}else{
		multiChannel.openLayer('missingsearchquery');			
	}
};

multiChannel.search.checkSearchQuery = function() {
	var result = false;
	if($('searchQuery').value.length > 0){
		result = true;
	}else{
		multiChannel.openLayer('missingsearchquery');
	}
	return result;
};

multiChannel.search.searchSubmitForm = function() {
	if($('searchQuery').value.length > 2){
		document.searchForm.submit();
	}
};

multiChannel.bigStartLayer = new Object();

multiChannel.bigStartLayer.sliding = false;

multiChannel.bigStartLayer.slideOut = function(){	
	var openLayerDiv = $('bigStartLayerOpenLayer');
	var sld=new Fx.Slide('layer_content',
		{
			duration: 1000,
			transition: Fx.Transitions.Bounce.easeOut,
			onStart: function() { 
				multiChannel.bigStartLayer.sliding = true;
			},
			onComplete: function() {
				openLayerDiv.setStyles({'display': 'block'});
				multiChannel.bigStartLayer.sliding = false;
			}
		});
		$('layer_content').parentNode.setStyle('height', $('layer_content').offsetHeight);
		sld.slideOut();
		multiChannel.bigStartLayer.moveUp();
};

multiChannel.bigStartLayer.moveUp = function() {
	var container = $('big_flash_layer_container');
	if (container != null) {
		var top = container.getStyle('top').toInt();
		var offset = $('layer_content').offsetHeight.toInt();
		var difference = top - offset;
		var tween = new Fx.Tween(container, 
			{
				duration: 1000,
				transition: Fx.Transitions.Bounce.easeOut,
				onStart: function() {},
				onComplete: function() {}
			}
		);
		tween.start('top', top + 'px', difference + 'px');
	}
};

multiChannel.bigStartLayer.slideIn = function(){	
	var openLayerDiv = $('bigStartLayerOpenLayer');
	var sld=new Fx.Slide('layer_content',
		{
			duration: 1000,
			transition: Fx.Transitions.Bounce.easeOut,
			onStart: function() {
				openLayerDiv.setStyles({'display': 'none'});
				multiChannel.bigStartLayer.sliding = true;
			},
			onComplete: function() {
				multiChannel.bigStartLayer.sliding = false;
			}
		}
	);
	sld.slideIn();
	multiChannel.bigStartLayer.moveDown();
};
	
multiChannel.bigStartLayer.moveDown = function() {
	var container = $('big_flash_layer_container');
	if (container != null) {
		var top = container.getStyle('top').toInt();
		var offset = $('layer_content').offsetHeight.toInt();
		var sum = top + offset;
		var tween = new Fx.Tween(container, 
			{
				duration: 1000,
				transition: Fx.Transitions.Bounce.easeOut,
				onStart: function() {},
				onComplete: function() {}
			}
		);
		tween.start('top', top + 'px', sum + 'px');
	}
};

multiChannel.bigStartLayer.checkAndSetCookie = function(){	
	var mcscookie = Cookie.read('mcsstatusbsl');
	if(mcscookie == null){
		Cookie.write('mcsstatusbsl', 'bsl=0');
		window.addEvent('domready', multiChannel.bigStartLayer.adjustLayer);
	}else{		
		var openLayerDiv = $('bigStartLayerOpenLayer');
		var sld=new Fx.Slide('layer_content',
			{
				duration: 1,
				onStart: function() {
				},
				onComplete: function() {
					openLayerDiv.setStyles({'display': 'block'});
				}
			}		
		);		
		$('layer_content').parentNode.setStyle('height', $('layer_content').offsetHeight);
		sld.slideOut();
	}
};

multiChannel.bigStartLayer.adjustLayer = function() {
	var container = $('big_flash_layer_container');
	if (container != null) {
		var top = container.getStyle('top').toInt();
		var offset = $('layer_content').offsetHeight.toInt();
		var sum = top + offset;
		container.setStyle('top', sum + 'px');
	}
};

multiChannel.bigFlashLayer = new Object();

multiChannel.bigFlashLayer.checkAndSetCookie = function(){	
	var mcscookie = Cookie.read('mcsstatusbfl');
	if (mcscookie == null) {
		$('big_flash_layer_container').show();
		Cookie.write('mcsstatusbfl', 'bfl=0');
	}
};

/**************************************************
account functions
**************************************************/
multiChannel.account = new Object();

multiChannel.account = new Object();

multiChannel.account.formatLinkEntries = function(logoutDisplayed, logoutPosition, logoutText){
	if ($defined($('links'))) {
		if (logoutDisplayed) {
			var a = new Element('a');
			a.onclick = function(){
				multiChannel.logon(); return false
			}
			a.appendText(logoutText);
			a.href = '';
			
			var logout = new Element('li');
			logout.appendChild(a);
			
			var links = $('links');
			var children = links.getElements('li');
			var length = children.length;
			if (logoutPosition < length){
				logout.inject(children[logoutPosition], 'before');
			} else {
				links.appendChild(logout);
			}
		}
		
		var links = $('links');
		var children = links.getElements('li');
		children[0].addClass('first');
		children[children.length - 1].addClass('last');
	}
};


multiChannel.productzoom = new Object();

multiChannel.productzoom.show = function(partNumber) {
	if ($defined(partNumber)) {
		var uri = new URI(multiChannel.context.getScene7ProductZoomURL(partNumber));
		s7loadViewer(uri, 755, 411, uri.getData('instanceName'), '#FFFFFF', 'opaque', 'productzoomcontent');
	}
};