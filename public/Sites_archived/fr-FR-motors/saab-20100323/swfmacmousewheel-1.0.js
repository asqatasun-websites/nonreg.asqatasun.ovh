function SWFMacMouseWheel( swfObject ) {
	this.so = swfObject;
	this.init();
}

SWFMacMouseWheel.enabled = false;

SWFMacMouseWheel.prototype = {
	init: function() {
		SWFMacMouseWheel.instance = this;
		if (window.addEventListener) {
	        window.addEventListener('DOMMouseScroll', SWFMacMouseWheel.instance.wheel, false);
		}
		window.onmousewheel = document.onmousewheel = SWFMacMouseWheel.instance.wheel;
	},
	
	handle: function( delta ) {
		if( document[ this.so.getAttribute('id') ].externalMouseEvent ) 
			document[ this.so.getAttribute('id') ].externalMouseEvent( delta );
	},

	wheel: function(event) {
		if (!event) // IE
        	event = window.event;
	
        var delta = 0;
        if (event.wheelDelta) { // IE/Opera.
			delta = event.wheelDelta/120;
			if (window.opera) delta = -delta;
        } else if (event.detail) { // Mozilla.
            delta = -event.detail/3;
        }
        if( /AppleWebKit/.test(navigator.userAgent) ) {
        	delta /= 3;	
        }
        /** 
         * If delta is nonzero, handle it.
         * Basically, delta is now positive if wheel was scrolled up,
         * and negative, if wheel was scrolled down.
         */
		if( delta )
               SWFMacMouseWheel.instance.handle( delta );
        /** 
         * Prevent default actions caused by mouse wheel if
         * it's already being controlled by the script (i.e. 
         * it's enabled).
         */
        if( SWFMacMouseWheel.enabled ) { 
			if( event.preventDefault )
				event.preventDefault();
			event.returnValue = false;
		}
	}
};

/**
 * Pretty much makes sure we may still use the main 
 * browser scroller when the mouse is not over something scrollable.
 */
SWFMacMouseWheel.enable = function() {
	SWFMacMouseWheel.enabled = true;
}

SWFMacMouseWheel.disable = function() {
	SWFMacMouseWheel.enabled = false;
}
