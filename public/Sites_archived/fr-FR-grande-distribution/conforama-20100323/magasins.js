// ****
// Retourne le magasin principal de l'utilisateur
//
// Si l'utilisateur n'a pas encore choisi de magasins, pas de cookie.
// Lecture du cookie "MAG". Si cookie absent, ou cookies inactifs, retourne null.
//
// @return l'ID du magasin principal de l'utilisateur (null si pas de magasins).
//
// ****
function getMag1() {
	var mag1 = null;
	var magasins = getMagasins();
	if(magasins != null) {
		mag1 = magasins.split("|")[0];
	}
	return mag1;
}

// ****
// Retourne le magasin secondaire de l'utilisateur
//
// Si l'utilisateur n'a pas encore choisi de magasins, pas de cookie.
// Lecture du cookie "MAG". Si cookie absent, ou cookies inactifs, retourne null.
//
// @return l'ID du magasin secondaire de l'utilisateur (null si pas de magasins).
//
// ****
function getMag2() {
	var mag2 = null;
	var magasins = getMagasins();
	if(magasins != null) {
		mag2 = magasins.split("|")[1];
	}
	return mag2;
}

// ****
//
// Lecture des magasins sélectionnés
//
// si l'utilisateur a sélectionné des magasins : IDmag1|IDmag2
// sinon (cookie absent, cookies désactivés) : null
// @return liste des IDs des magasins (magasin principal, magasin secondaire)
//
// ****
function getMagasins() {
	return getCookie("MAG");
}