var szNormal = 90, szSmall  = 70, szFull  = 150;
var coin="";
var drawall_x,drawall_y,drawall_w,drawall_h;
var drawall_x1,drawall_y1,drawall_w1,drawall_h1;
var precision=2 // = precision pour le replacement : 1 faible Ne pas dépasser 5 pour éviter d'avoir des boucles trop longues.
var mvtencours;
var tab_item_sur_scene = new Array();
var IUSER_MAXELEMS = 5;	
var espacement = 10;
var binterval;
var interval_check;
var repositionnement;

var last_expanded_box = "";
/* images à precharger pour la home */
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------- FONCTIONS DE POSITIONNEMENT ------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/**************************************
* Jonas Raoni Soares Silva
* http://www.joninhas.ath.cx
**************************************/
hitTest = function(o, l){
    function getOffset(o){
        for(var r = {l: o.offsetLeft, t: o.offsetTop, r: o.offsetWidth, b: o.offsetHeight};
            o = o.offsetParent; r.l += o.offsetLeft, r.t += o.offsetTop);
        return r.r += r.l, r.b += r.t, r;
    }
    var a = arguments, j = a.length;
    j > 2 && (o = {offsetLeft: o, offsetTop: l, offsetWidth: j == 5 ? a[2] : 0,
    offsetHeight: j == 5 ? a[3] : 0, offsetParent: null}, l = a[j - 1]);
    for(var b, s, r = [], a = getOffset(o), j = isNaN(l.length), i = (j ? l = [l] : l).length; i;
        b = getOffset(l[--i]), (a.l == b.l || (a.l > b.l ? a.l <= b.r : b.l <= a.r))
        && (a.t == b.t || (a.t > b.t ? a.t <= b.b : b.t <= a.b)) && (r[r.length] = l[i]));
    return j ? !!r.length : r;
};

//+ Jonas Raoni Soares Silva
//@ http://jsfromhell.com/math/is-point-in-poly [rev. #0]

function isPointInPoly(poly, pt){
    for(var c = false, i = -1, l = poly.length, j = l - 1; ++i < l; j = i)
        ((poly[i].y <= pt.y && pt.y < poly[j].y) || (poly[j].y <= pt.y && pt.y < poly[i].y))
        && (pt.x < (poly[j].x - poly[i].x) * (pt.y - poly[i].y) / (poly[j].y - poly[i].y) + poly[i].x)
        && (c = !c);
    return c;
}

var getUnders = function(oElem,aBoites){
	return hitTest(oElem,aBoites).remove(oElem);
}
var isAbove = function(oElem1, oElem2){
	if (oElem1 != oElem2 && hitTest(oElem1,[oElem2]).length > 0 && oElem1.style.zIndex > oElem2.style.zIndex){
		return true;
	}else{
		return false;
	}
}


function desactive_items() {
	
	var tab_items = $$('item');
		tab_items.each(function(item, index) {
			item.removeEvents();
		}
	);
}
 
function setCoord(drawall) {
	drawall_x = drawall.offsetLeft +drawall.offsetParent.offsetParent.offsetLeft;
	drawall_y = 90;
	drawall_w = 530;
	drawall_h = 280;
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------- FONCTIONS D'INITIALISATION -------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/


function ferme_box(element) {
	if (element=='visu_large') $('anim_visu_large').innerHTML = "";
	
	/* cas autocompleter */
	if ($('theAutoCompleter'))
		$('theAutoCompleter').remove()
	
	//080122 Alexis : L'effacement de la boite est ameliore : Elle est cachee, puis effacee et enfin placee hors de l'ecran en cas
	//d'erreur d'effacement.
	
	$(element).effect('opacity', {
		duration : 200,
		onComplete : function(){
			$(element).setStyles({ 'left':-(drawall_x), 'top':-(drawall_y)});
			iframor.workspace(tab_item_sur_scene,this.element);
			tab_item_sur_scene.remove(this.element);
			/*iframor.unset(this.element);*/
			$(element).style.display = 'none';
		}
	}).start(0)
	
	
	setposXCookie(getEndString('/',b_outils[outils_id.indexOf(element)]),"x");
	setposYCookie(getEndString('/',b_outils[outils_id.indexOf(element)]),"y");

	//switch_icone(element)
	var elm_id = element.replace('_element', '');
	switchImage(elm_id, 'on');
}


function set_box(box,x,y,z) {
	if($(box)){
	//	alert(box)
		//create layerDiv
		iframor.create($(box));
		x = Number(x);
		y = Number(y);
		if (x >= 0 && y >= 0) {
			$(box).setStyles({'opacity': '1','position': 'absolute','left':(drawall_x+x),'top':(drawall_y+y),'display' : 'block'});
			if (z){
				$(box).style.zIndex = z;
			}
			iframor.setInit($(box));
			tab_item_sur_scene.push($(box));
			
			
			/**
			 * Chargement ajax de la page contenant le squelette de la boite
			 */
			//console.log('setting boxe');
			
			loadBoxContent(box);
			//(function(){loadBoxContent(box);}.delay(10))
			
		} else {
			//$(box).setStyles({ 'left':-400, 'top':-400});
			
			$(box).className = 'element';
		}
		
	}
}
function print_tab(myTab) {
	var liste_tab='';
	for (t=0;t<myTab.length;t++) liste_tab+=myTab[t].id+'\n';
}

function cache_select(elm) {
	if (!window.ie6) return;
	var liste_select= document.getElementsByTagName("select");
	var start = 2;
	
	if (elm) { 
		liste_select = elm.getElementsByTagName("select");
		start = 0;
	}
		
	for (ls = start ; ls < liste_select.length ; ls++) {
		//liste_select[ls].style.display='none';
		liste_select[ls].style.visibility='hidden';
	}
}

function show_select() {
	if (!window.ie6) return;
	var liste_select=document.getElementsByTagName("select");
	for (ls = 2; ls < liste_select.length; ls++) {
		//liste_select[ls].style.display = 'block';
		liste_select[ls].style.visibility='visible';
	}
}


function check_cookies() {
	if (!Cookie.get(b_outils[outils_id.indexOf('highlight_element')])) {
		savaData();
		set_box('highlight_element',0,0);
		setposYCookie('b_hightlights.html',1);
		setposXCookie('b_hightlights.html',1);
		
		set_box('curiculum-vitae_element',0,200);
		setposYCookie('b_news.html',200);
		setposXCookie('b_news.html',1);
	}
}

/*window.addEvent('domready',*/
var aBaseLeft = [];
function proto_init(){
	//demon sizing
	windowSettings();
	
	//temporaire
	//document.getElement('.bloc_ft_image').addEvent('click', resetCookie)
	cache_select($('production_element'));
	
	//on instancie class de generation de layer 
	iframor = new Iframor();
	//recuperation cookie
	if (getBoxesAndIndexes()){
		var cook = getBoxesAndIndexes();
		var zIndexes = cook.split('#')[0].split('&')
		var aBoxes = cook.split('#')[1].split('@');
		//tri tableau outils_id en fonction du cookie
		var resultsArr = [];
		var resultsArr2 = [];
		for (var i=0; i < outils_id.length; i++) {
			var item = outils_id[i];
			if (aBoxes.contains(item)){
				resultsArr[aBoxes.indexOf(item)] = b_outils[i];
				resultsArr2[aBoxes.indexOf(item)] = outils_id[i];
			}
		};
		b_outils = resultsArr.merge(b_outils);
		outils_id = resultsArr2.merge(outils_id);
	}
	savaData();
	
	var tabs = [	$('press_releases_element'),
				 	$('calendar_element'),
					$('cards_element'),
					$('production_element'),
					$('mymag_element'),
					$('jobs_element'),
					$('stocks_element'),
					$('glossary_element'),
					$('televideo_element'),
					$('docs_center_element'),
					$('bookmarks_element')
	]
	
	//22/10/07 Alexis : Rattachement des pages html � l'id de l'�l�ment qui le contiendra dans la navigation de la home page
	//R�cup�ration de tous les �l�ments de menu
	var tab_ids_boites = $$('div.element');
	tab_ids_boites = $A(tab_ids_boites);
	
	var loader;
	//Insertion des chargeurs de boite dans chaque div � afficher
	
	tab_ids_boites.each(function(div_boite, i) {
		//alert('pouer')
		loader = new Element('div', {'id':'loader_'+div_boite.id, 'class':'loader_boite'});
		img_loader = new Element('img', {'src':image_chargement});
		loader.setStyle('width', div_boite.offsetWidth);
		loader.adopt(img_loader);
		div_boite.loader = loader;
		div_boite.getFirst().adopt(loader);
	});
	if (navJS){
		var icones = $$('#items li');// items = Ensembles des icones du menus des boxes
		icones.setOpacity(1);
	}
	var elements = $$('#elements .element');
	
	var drawall = $('drawall');
	drawCoord = $('drawall').getCoordinates();
	setCoord(drawall);
	//magikEffect = new magikMask();
	
	// Pour chaque icone, on applique les fonctions suivantes
	elements.each(function(item,i){
		item.icon = $(item.id.replace('_element', '')) ? $(item.id.replace('_element', '')) : false;
		item.elem = item;
		if (item.elem && !item.id.match(/\balaune_element\b/)){	
		/*	
			Garbage.collect(item.elem).addEvents({
				'emptydrop': function() {
					//item.tips.end();
					item.elem.setStyles({'opacity':1, 'cursor': 'pointer'});
				}
			});
		*/
			item.elem.drag = new Drag.Move(item.elem, {
				'handle': item.elem.getFirst(),
				// F6 DEBUG 'handle' : item.elem,
				container: 'drawall',
				snap:5,
				'excludedElement' : 'alaune_element',
				'onComplete': function(elem){
					dragging = false;
					//071112 Alexis : on indique � la page d'accueil qu'on a rel�ch� la boite
					boite_saisie = false;
					
					var pos = [elem.offsetLeft, elem.offsetTop ];
					var zone_depot = $('drawall');
					
					// F6 DEBUG setZIndex(tab_item_sur_scene, elem);
					
					setposXCookie(getEndString('/', b_outils[outils_id.indexOf(elem.id)]),parseInt(elem.style.left) - parseInt(zone_depot.getLeft()));
					setposYCookie(getEndString('/', b_outils[outils_id.indexOf(elem.id)]),parseInt(elem.style.top) - parseInt(zone_depot.getTop()));
					
					iframor.workspace(tab_item_sur_scene,elem);
				},

				'onStart': function(elem){
					iframor.unset(elem);
					// F6 DEBUG check_cookies();
					elem.setStyles({'display':'block','opacity': 1, 'position': 'absolute', 'cursor': 'move','z-index':1000});
				}
			}); // this returns the dragged element
		}
		
		if (item.icon){
			item.icon.addEvent('click', function(e) {
				new Event(e).stop();
				openBox(item.id);
			});
		}
		

	});
//	var no_cookies_box = true;
	
	// F6 DEBUG set_box('element_conteneur',-1,-1);
	//alert(!!getBoxesAndIndexes())
	if (!getBoxesAndIndexes()) {
		outils_id.each(function(item,i){
			initParamCookie(tools[item]);
			var outilPosition = outils_coordinates[item];
			if (!outilPosition) return;
			set_box(item,outilPosition.x,outilPosition.y);
			setposXCookie(getEndString('/', b_outils[i]),outilPosition.x);
			setposYCookie(getEndString('/', b_outils[i]),outilPosition.y);
			var item_id = item.substr(0,item.indexOf("_element"));
			if(item_id!='curiculum-vitae' && item_id!='highlight' && item_id!='alaune') {
			/*	var image_item = $('image_'+item_id).src;
				$('image_'+item_id).src = image_item.substr(0,image_item.indexOf(".png"))+"_off.png";
				$('image_'+item_id).off = 1;*/
				switchImage(item_id, 'off');
			}
		
		});
		// F6 DEBUG setZIndex(tab_item_sur_scene, $(outils_id[i]));
		
	}else{
		for (outil=0;outil<b_outils.length;outil++) {
			var boxCook = {
				'x' : getValueCookie(getEndString('/', b_outils[outil]),1),
				'y' : getValueCookie(getEndString('/', b_outils[outil]),2)
			}
			if (boxCook.x>=0) {
				var zInd = 0;
				var zInd = aBoxes.indexOf(outils_id[outil]) != '-1' ? zIndexes[aBoxes.indexOf(outils_id[outil])] : 0;
				
				//on set la box en fonction de son id et de sa position
				set_box(outils_id[outil],Number(boxCook.x),Number(boxCook.y), zInd);
				//si outil sur la page et loadé on gère la barre d'outils
				if (outils_id[outil]!='highlight_element' && outils_id[outil]!='curiculum-vitae_element' && outils_id[outil]!='alaune_element') {
					var item_id = outils_id[outil].substr(0,outils_id[outil].indexOf("_element"));
					switchImage(item_id, 'off');
				}

			}  else {
				//on set la box ailleurs si pas de cookie
				set_box(outils_id[outil],-1,-1);
			}
			
		}
	}
	
	/* Definition du tableau des offsetLeft de base */
	var els = $('elements').getElements(".element");
	els.each( function(el, i){
		aBaseLeft.push(el.getLeft());
	});
}
//);

var openBox = function(sId){
	if (!$$('object#items')[0]){
		ouvrir_box($(sId));
	}else{
		ouvrir_box(sId, 1);
	}
	
}

var ouvrir_box = function(item, mode){
	if (mode == 1){
		var itemId = item;
		var element = $(item + '_element')
	}else{
		var itemId = item.id.replace('_element','');
		var element = item.elem;
	}
	//console.log("proto::ouvrir_box", item, itemId, element);
	
	if (mode == 1 || !$('image_'+itemId).off) {
	
		switchImage(itemId, 'off');
		tab_item_sur_scene.push(element);
		element.setStyle('display','block');
		var position = {
			'x' : ((drawCoord.width / 2)  - parseInt(element.offsetWidth) / 2) + drawCoord.left,
			'y' : 260
		}
		element.setStyles({ 'left':position.x, 'top':position.y});
		element.effect('opacity').start(1);
		
		//tester position elements
		var otherItems = $$('#elements .element').remove(element);
		//instance effet deplacement
		fxMove  = new Fx.Styles(element, {duration:500,onComplete : function(){testCol();}})
		var testCol = function(){
			var coord = element.getCoordinates();
			element.style.zIndex = 10000;
			var points = [
			    {x: coord.left -10, y: coord.top -10},
			    {x: coord.right +10, y: coord.top -10},
				{x: coord.right +10, y: coord.bottom +10},
			    {x: coord.left -10, y: coord.bottom +10}

			]
			var lastCoords = false;
			otherItems.each(function(elm){
				var c = elm.getCoordinates();
				if (isPointInPoly(points, {x: c.left, y: c.top}) && isPointInPoly(points, {x: c.right, y: c.bottom})){
					lastCoords = c;
				}else{
				}
			})
			if (lastCoords){
				fxMove.start({
					'left' : lastCoords.left + 25,
					'top' : lastCoords.top + 40
				})
			}else{
				//enregistrement dans cookie
				setposXCookie(getEndString('/', b_outils[outils_id.indexOf(element.id)]), coord.left - drawall_x);
				setposYCookie(getEndString('/', b_outils[outils_id.indexOf(element.id)]), coord.top - drawall_y);

				iframor.workspace(tab_item_sur_scene,element);
				//magikEffect.set({'x' : coord.left, 'y' : coord.top }, element);
			}
		}
	
		//test collision
		testCol();
		
		var itemToLoad = mode == 1 ? item+'_element' : item.id;
		loadBoxContent(itemToLoad);
	}
}

var magikMask = new Class({
	initialize : function(){
		//creation de l'effet
		this.elems = $('elements');
		if (window.ie6){
			this.elems.mask = new Element('div', {'class' : 'magikMaskIE'}).setOpacity(0);
			this.heightToGo = 500;
			this.widthToGo = 700;
		}else if (window.ie){
			this.elems.mask = new Element('img', {'class' : 'magikMask', 'src' : '/images/elm_fond2.png'}).setOpacity(0);
			this.heightToGo = 500;
			this.widthToGo = 700;
		}else{
			this.elems.mask = new Element('img', {'class' : 'magikMask', 'src' : '/images/elm_fond.png'}).setOpacity(0);
			this.heightToGo = 500;
			this.widthToGo = 800;
		}
		this.elems.adopt(this.elems.mask);
		fxMagik = new Fx.Styles(this.elems.mask,{duration:250,fps:25,transition: Fx.Transitions.Cubic.easeOut});
		
		
		
	},
	set: function(oPosition, item){
		var masque = this.elems.mask;
		var heightToGo = this.heightToGo;
		var widthToGo = this.widthToGo;
		
		var itemHeight = parseInt(item.offsetHeight);
		var itemWidth = parseInt(item.offsetWidth);
		masque.setStyles({
			'width': itemWidth * 2,
			'height': itemHeight * 2,
			'left':oPosition.x - itemWidth + (itemWidth /2),
			'top':oPosition.y - itemHeight + (itemHeight /2),
			'z-index':item.style.zIndex - 1
		})
		fxMagik.start({
			'opacity' :0.3
		}).chain(function(){
		    fxMagik.start({
			    'height': heightToGo,
				'top' : ((parseInt(masque.style.top)*2) + parseInt(masque.offsetHeight) - heightToGo)/2,
				'width' : widthToGo,
				'left' : ((parseInt(masque.style.left)*2) + parseInt(masque.offsetWidth) - widthToGo)/2,
				'opacity' : 0
			})
		}).chain(function(){
			masque.setStyles({'width':widthToGo, 'height':heightToGo})
		});
	
	}
})

var closeBox = function(sId){
	ferme_box(sId + '_element');
}

var setZIndex = function(aBoxes, oElem){
	aBoxes.remove(oElem);
	aBoxes.push(oElem);
	var aIndex = [];
	aBoxes.each(function(el, i){
		i++;
		el.setStyle('z-index', i*10);
		if (el.layerDiv)
			el.layerDiv.style.zIndex = (i*10)+1;
		aIndex.push(i*10);
	})
	setZIndexCookie(aBoxes,aIndex);
}
function getFlashObject (pAnim)
{
	if (navigator.appName.indexOf("Microsoft") != -1) return window[pAnim]; else return document[pAnim];
}
flashLance = false;
var switchImage = function(sId, sStatus){
	var existObjectItems = ''+$$('object#items')[0]; // hack pour IE pour recupérer le undefined sous forme de texte au lieu d'objet undefined
	if(existObjectItems != 'undefined'){
		//console.log("flash -- "+sId+" -- "+sStatus);
		var timer = (function(){
			if (flashLance){
				sStatus = sStatus == 'on' ? 'off' : 'on';
				getFlashObject("items").setIconStatus(sId,sStatus);
				$clear(timer)	
			}
		}).periodical(30)
	}else if ($(sId)){
		//console.log("normal -- "+sId+" -- "+sStatus);
		var img = $(sId);
		if (sId.match(/\balaune\b/) || sId.match(/\bcuriculum\b/) || sId.match(/\bhighlight\b/)) return;
		switch (sStatus) {
		    case 'on': //Mode Switch On
				if ($('image_'+sId).src.match(/_off\b/)){
					$('image_'+sId).src = $('image_'+sId).src.replace('_off','');
					$('image_'+sId).off = 0;
				}
			break;
		    case 'off': //Mode Switch Off

				$('image_'+sId).src = $('image_'+sId).src.replace('.png','_off.png')
				$('image_'+sId).off = 1;
			break;
		    default:
			return false;
		}
	}
}


var Iframor = new Class({
	initialize : function(){
		this.dropZone = $('elements');
		this.init = true;
	},
	create : function(el){
	/*	if (el && el.layerDiv) return; */
		var layerDiv=new Element('div', {'class':'layerDivElement'});
		this.dropZone.adopt(layerDiv);
		el.layerDiv = layerDiv;
		el.layerMode = false;
	},
	setInit : function(el){
		this.workspace(tab_item_sur_scene,el, true);
	},
	set : function(el){
		if (el && !el.layerDiv) this.create(el);
		var _self = this;
		this.coord = el.getCoordinates();
		this.coord.height = this.coord.height - 45;
		this.coord.top = this.coord.top + 45;
		this.coord.left = (el.style.left).toInt();
		el.layerDiv.setStyles(this.coord);
		el.layerDiv.style.zIndex = parseInt(el.style.zIndex) + 1;
		el.layerMode = true;
		el.layerDiv.addEvent('click', function(e){
			new Event(e).stop();
			_self.unset(el);
			_self.workspace(tab_item_sur_scene,el);
		})
	},
	unset : function(el){
		if (el && el.layerDiv){
			el.layerDiv.style.left = -1000 + 'px';
			el.layerDiv.style.top = -1000 + 'px';
			el.layerDiv.removeEvents();
		}
		el.layerMode = false;
	},
	workspace : function(arr,oElm, initMode){
		var _self = this;
		var inColision = getUnders(oElm,arr);
		show_select($('production_element'));
		inColision.each(function(item){
			if (item.id.match(/\bproduction_element\b/)){
				cache_select($('production_element'));
			}
			_self.set(item);
		})
		if (initMode){
			oElm.lastInColision = arr;
		}
		var above = false;
		if (oElm.lastInColision){
			oElm.lastInColision.each(function(item){
				if (!inColision.contains(item)){
					above = true;
					var index = 0;
					while (above && arr.length > index){
						if (isAbove(arr[index], item)){
							above = false;
						}
						index++;
					}
					// F6 DEBUG console.log(item , above)
					if (above){
						_self.unset(item)
					}
				}
				
			})
		}
		if (!initMode) setZIndex(tab_item_sur_scene, oElm);
		oElm.lastInColision = inColision;
	}
})

function createNav_right(responseXML){
	var items = responseXML.getElementsByTagName('item');
    if (items) {
		var zoneMenu = $$('#items .nav_outils')[0];
		for (var i=0; i < items.length; i++) {
			var item = items[i];
			var li = new Element('li');
			var a = new Element('a');
			var img = new Element('img');
			li.id = item.attributes.getNamedItem('id').nodeValue;
			a.title = item.attributes.getNamedItem('name').nodeValue;
			a.href = '#';
			img.id = 'image_' + li.id;
			img.src = item.getElementsByTagName('imageOn')[0].attributes.getNamedItem('src').nodeValue;
			zoneMenu.adopt(li.adopt(a.adopt(img)));			
		};
	}
}
function onFlashReady(targetId){
	// ICI ON DISPATCH L'EVENEMENT POUR L'INITIALISATION DU MENU
	flashLance = true;
//	switchImage(sId, sStatus, 1);
}



var bkgEvt = function(){
	var sUrl = bkgImage;
	var setBkg = function(oImg){
		document.body.style.backgroundImage = 'url(' + oImg.src + ')';
		document.body.style.backgroundRepeat = 'no-repeat';
		document.body.style.backgroundPosition = 'center top';

		/*var oImgEl = new Element('div', {'class':'imgEvent'}).setOpacity(0);
		oImgEl.injectTop(document.body);
		oImgEl.adopt(oImg);
		
		if (window.ie){
			var width = window.getWidth();
		}else{
			var width = window.offsetWidth;
		}
		oImgEl.setStyles({
			'width': width,
			'height': window.getHeight()
		})
		var fx = new Fx.Style(oImgEl, 'opacity', {fps:25, duration: 250}).start(1);*/
		
	}
	
	var date = new Date();
	sUrl += '?nocache=' + date.getTime();
	
	new Asset.image(sUrl, {
		onload: function(){
			if (this){
				setBkg(this);
			}
		}
	});
}


var windowSettings = function(){
	var draw = $('drawall');
	var elms = $('elements');
	var initSizeWindow = $(window).getSize().size;
	var previousCase = 0;
	var currentCase = previousCase;
	var prevLeft = 0;
	var els = $('elements').getElements(".element");
	elms.setStyle('left', 0);
	window.onresize = function(){
		
		var whatIwant = (initSizeWindow.x - 997) / 2;
		if( whatIwant < 0 )
			whatIwant = 0;
		
		if( $(window).getSize().size.x > 997 )
			var diff = whatIwant - Math.abs(parseInt(($(window).getSize().size.x - 997) / 2));
		else
			var diff = whatIwant;
		
		elms.setStyle('left', 0);
		els.each(function(elm, i){
			if (diff > whatIwant){
				elm.setStyle('left', aBaseLeft[i])
			}else{
				elm.setStyle('left', aBaseLeft[i] - diff)
			}
		});
		

		setCoord(draw);
		drawCoord = $('drawall').getCoordinates();
		
	}
	
}



/* ************************************************************************************
	EXTENSION CUSTOM DRAG.BASE + MOOTOOLS
************************************************************************************** */

Drag.Base = null;



Drag.Base = new Class({

	options: {
		handle: false,
		unit: 'px',
		onStart: Class.empty,
		onBeforeStart: Class.empty,
		onComplete: Class.empty,
		onSnap: Class.empty,
		onDrag: Class.empty,
		limit: false,
		modifiers: {x: 'left', y: 'top'},
		grid: false,
		snap: 6
	},

	initialize: function(el, options){
		this.setOptions(options);
		this.element = $(el);
		this.handle = $(this.options.handle) || this.element;
		this.excludedElement = $(this.options.excludedElement);
		this.mouse = {'now': {}, 'pos': {}};
		this.value = {'start': {}, 'now': {}};
		this.coord = {'excludedElement':false}
		this.bound = {
			'start': this.start.bindWithEvent(this),
			'check': this.check.bindWithEvent(this),
			'drag': this.drag.bindWithEvent(this),
			'stop': this.stop.bind(this)
		};
		this.attach();
		if (this.options.initialize) this.options.initialize.call(this);
	},

	attach: function(){
		this.handle.addEvent('mousedown', this.bound.start);
		return this;
	},

	detach: function(){
		this.handle.removeEvent('mousedown', this.bound.start);
		return this;
	},

	start: function(event){
		this.fireEvent('onBeforeStart', this.element);
		this.mouse.start = event.page;
		var limit = this.options.limit;

		if (this.excludedElement) this.coord.excludedElement = this.excludedElement.getCoordinates();
		//console.log(this.coord.excludedElement);
		this.limit = {'x': [], 'y': []};
		for (var z in this.options.modifiers){
			if (!this.options.modifiers[z]) continue;
			this.value.now[z] = this.element.getStyle(this.options.modifiers[z]).toInt();
			this.mouse.pos[z] = event.page[z] - this.value.now[z];
			if (limit && limit[z]){
				for (var i = 0; i < 2; i++){
					if ($chk(limit[z][i])) this.limit[z][i] = ($type(limit[z][i]) == 'function') ? limit[z][i]() : limit[z][i];
				}
			}
		}
		if ($type(this.options.grid) == 'number') this.options.grid = {'x': this.options.grid, 'y': this.options.grid};
		document.addListener('mousemove', this.bound.check);
		document.addListener('mouseup', this.bound.stop);
		//this.fireEvent('onStart', this.element);
		event.stop();
	},

	check: function(event){
		var distance = Math.round(Math.sqrt(Math.pow(event.page.x - this.mouse.start.x, 2) + Math.pow(event.page.y - this.mouse.start.y, 2)));
		if (distance > this.options.snap){
			document.removeListener('mousemove', this.bound.check);
			document.addListener('mousemove', this.bound.drag);
			this.drag(event);
			this.fireEvent('onSnap', this.element);
		}
		event.stop();
	},

	drag: function(event){
		this.out = false;
		this.mouse.now = event.page;
		var coords = '';
		
		for (var z in this.options.modifiers){
			if (!this.options.modifiers[z]) continue;
			this.value.now[z] = this.mouse.now[z] - this.mouse.pos[z];
			if (this.limit[z]){
				coords+=" now."+z+" = "+this.value.now[z]+" limit."+z+" = "+this.limit[z][1];
				if ($chk(this.limit[z][1]) && (this.value.now[z] > this.limit[z][1])){
					this.value.now[z] = this.limit[z][1];
					this.out = true;
				} else if ($chk(this.limit[z][0]) && (this.value.now[z] < this.limit[z][0])){
					this.value.now[z] = this.limit[z][0];
					this.out = true;
				}
				if (this.value.now['x'] < this.coord.excludedElement.right && this.value.now['y'] < this.coord.excludedElement.bottom){
					this.value.now['x'] = this.coord.excludedElement.right;
					this.value.now['y'] = this.coord.excludedElement.bottom;
					this.out = true;
				}
				
				coords+=" NEW "+z+" = "+this.value.now[z];
			}
			if (this.options.grid[z]) this.value.now[z] -= (this.value.now[z] % this.options.grid[z]);
			this.element.setStyle(this.options.modifiers[z], this.value.now[z] + this.options.unit);
		}
		
		//alert("coords : "+coords);
		
		this.fireEvent('onDrag', this.element);
		event.stop();
	},

	stop: function(){
		document.removeListener('mousemove', this.bound.check);
		document.removeListener('mousemove', this.bound.drag);
		document.removeListener('mouseup', this.bound.stop);
		this.fireEvent('onComplete', this.element);
	}

});

Drag.Base.implement(new Events, new Options);

/*
Class: Element
	Custom class to allow all of its methods to be used with any DOM element via the dollar function <$>.
*/

Element.extend({

	/*
	Property: makeResizable
		Makes an element resizable (by dragging) with the supplied options.

	Arguments:
		options - see <Drag.Base> for acceptable options.
	*/

	makeResizable: function(options){
		return new Drag.Base(this, $merge({modifiers: {x: 'width', y: 'height'}}, options));
	}

});

/*
Script: Drag.Move.js
	Contains <Drag.Move>, <Element.makeDraggable>

License:
	MIT-style license.
*/

/*
Class: Drag.Move
	Extends <Drag.Base>, has additional functionality for dragging an element, support snapping and droppables.
	Drag.move supports either position absolute or relative. If no position is found, absolute will be set.
	Inherits methods, properties, options and events from <Drag.Base>.

Note:
	Drag.Move requires an XHTML doctype.

Arguments:
	el - the $(element) to apply the drag to.
	options - optional. see Options below.

Options:
	all the drag.Base options, plus:
	container - an element, will fill automatically limiting options based on the $(element) size and position. defaults to false (no limiting)
	droppables - an array of elements you can drop your draggable to.
	overflown - an array of nested scrolling containers, see Element::getPosition
*/

Drag.Move = Drag.Base.extend({

	options: {
		droppables: [],
		container: false,
		overflown: []
	},

	initialize: function(el, options){
		this.setOptions(options);
		this.element = $(el);
		this.droppables = $$(this.options.droppables);
		this.container = $(this.options.container);
		this.position = {'element': this.element.getStyle('position'), 'container': false};
		if (this.container) this.position.container = this.container.getStyle('position');
		if (!['relative', 'absolute', 'fixed'].contains(this.position.element)) this.position.element = 'absolute';
		var top = this.element.getStyle('top').toInt();
		var left = this.element.getStyle('left').toInt();
		
		if (this.position.element == 'absolute' && !['relative', 'absolute', 'fixed'].contains(this.position.container)){
			top = $chk(top) ? top : this.element.getTop(this.options.overflown);
			left = $chk(left) ? left : this.element.getLeft(this.options.overflown);
		} else {
			top = $chk(top) ? top : 0;
			left = $chk(left) ? left : 0;
		}
		this.element.setStyles({'top': top, 'left': left, 'position': this.position.element});
		this.parent(this.element);
	},

	start: function(event){
		this.fireEvent('onStart', this.element);
		cache_select($('production_element'));
		
		this.overed = null;
		if (this.container){
			var cont = this.container.getCoordinates();

			var el = this.element.getCoordinates();
			if (this.position.element == 'absolute' && !['relative', 'fixed', 'absolute'].contains(this.position.container)){
				//console.log(drawall_x, drawCoord);
				
				//alert("cont.right = "+cont.right+" - el.width = "+el.width+" cont.right - el.width = "+(cont.right - el.width));
				//alert("cont.bottom = "+cont.bottom+" - el.height = "+el.height+" cont.bottom - el.height = "+(cont.bottom - el.height));
				this.options.limit = {
					//'x': [cont.left, cont.right - el.width],
					'x': [drawall_x,cont.width + drawall_x - el.width],
					'y': [cont.top, cont.bottom - el.height]
				};
			} else {
				this.options.limit = {
					'y': [0, cont.height - el.height],
					'x': [0, cont.width - el.width]
				};
			}
		}
		this.parent(event);
	},

	drag: function(event){
		/*
		this.out = false;
		this.mouse.now = event.page;
		for (var z in this.options.modifiers){
			if (!this.options.modifiers[z]) continue;
			this.value.now[z] = this.mouse.now[z] - this.mouse.pos[z];
			if (this.limit[z]){
				if ($chk(this.limit[z][1]) && (this.value.now[z] > this.limit[z][1])){
					this.value.now[z] = this.limit[z][1];
					this.out = true;
				} else if ($chk(this.limit[z][0]) && (this.value.now[z] < this.limit[z][0])){
					this.value.now[z] = this.limit[z][0];
					this.out = true;
				}
			}
			if (this.options.grid[z]) this.value.now[z] -= (this.value.now[z] % this.options.grid[z]);
			this.element.setStyle(this.options.modifiers[z], this.value.now[z] + this.options.unit);
		}
		this.fireEvent('onDrag', this.element);
		event.stop();*/
		
		this.parent(event);
		
		var overed = this.out ? false : this.droppables.filter(this.checkAgainst, this).getLast();
		
		//alert("overed = "+overed);
		
		if (this.overed != overed){
			if (this.overed) {
				this.overed.fireEvent('leave', [this.element, this]);
			}
			
			this.overed = overed ? overed.fireEvent('over', [this.element, this]) : null;
		}
		return this;
	},

	checkAgainst: function(el){
		el = el.getCoordinates(this.options.overflown);
		var now = this.mouse.now;
		
		//return (now.x > el.left && now.x < el.right && now.y < el.bottom && now.y > el.top);
		return (this.mouse.now.x && this.mouse.now.y && this.mouse.now.x > el.left && this.mouse.now.x < el.right && this.mouse.now.y < el.bottom && this.mouse.now.y > el.top);
	},

	stop: function(){
		if (this.overed && !this.out) this.overed.fireEvent('drop', [this.element, this]);
		else this.element.fireEvent('emptydrop', this);
		this.parent();
		return this;
	}

});

/*
Class: Element
	Custom class to allow all of its methods to be used with any DOM element via the dollar function <$>.
*/

Element.extend({

	/*
	Property: makeDraggable
		Makes an element draggable with the supplied options.

	Arguments:
		options - see <Drag.Move> and <Drag.Base> for acceptable options.
	*/

	makeDraggable: function(options){
		return new Drag.Move(this, options);
	}

});