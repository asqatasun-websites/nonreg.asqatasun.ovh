function xsltTransform(xmlPath,xslPath,parameter)
{
xml=loadXMLDoc(xmlPath);
xsl=loadXSLDoc(xslPath);

var param = parameter.split(";");
var count= param.length;

if (window.ActiveXObject)
  {
    var template = new ActiveXObject("MSXML2.XSLTemplate");
    template.stylesheet = xsl.documentElement;
    var xsltProcessor = template.createProcessor();
    xsltProcessor.input=xml;
	for (i=0; i<=count-1;i++) 
	{
		var paramKeyValue = param[i].split(",");	
		xsltProcessor.addParameter(paramKeyValue[0],paramKeyValue[1]);
	}    
    xsltProcessor.transform();
    var result=xsltProcessor.output; 
	
    return result; 
 }
 
  else if (document.implementation && document.implementation.createDocument)
  {
	  	
	xsltProcessor=new XSLTProcessor();
    xsltProcessor.importStylesheet(xsl);
	for (i=0; i<=count-1;i++) 
	{
		var paramKeyValue = param[i].split(",");	
		xsltProcessor.setParameter(null,paramKeyValue[0],paramKeyValue[1]);
	} 	
	var resultDocument = xsltProcessor.transformToFragment(xml,document);

	return resultDocument
   	
  }
}


function loadXSLDoc(fname)
{

  var xmlDoc;
  
  if (window.ActiveXObject)
  {
		xmlDoc=new ActiveXObject("MSXML2.FreeThreadedDomDocument");
  }
 
  else if (document.implementation && document.implementation.createDocument)
  {  
		xmlDoc=document.implementation.createDocument("","",null);
  }
  else
  {
		alert('Your browser cannot handle this script');
  }

 if(xmlDoc&&typeof xmlDoc.load!='undefined')
    {
		xmlDoc.async=false;
		xmlDoc.load(fname);
		return xmlDoc;
	}
  
 else
   {
		var request=new XMLHttpRequest();
		request.open("GET",fname,false);
		request.send("");
		return request.responseXML;
	}
	
}


var xmlhttp;
function loadXMLDoc(url)
{
	xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp=new XMLHttpRequest();
	}
	else if (window.ActiveXObject)
	{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	if (xmlhttp!=null)
	{
		xmlhttp.open("GET",url,false);
		xmlhttp.send("");
		return xmlhttp.responseXML;
	}

}



