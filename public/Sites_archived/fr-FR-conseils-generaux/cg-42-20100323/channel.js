document.write("<SCRIPT language='JavaScript' src='js/tabpane.js'      type='text/javascript'></SCRIPT>");
document.write("<SCRIPT language='JavaScript' src='js/tree.js'         type='text/javascript'></SCRIPT>");

var isIE = document.all?true:false;
var isNN4 = document.layers?true:false;
var isNN7 = (document.all && document.getElementById)?true:false;
var isOPERA = (navigator.userAgent.indexOf("Opera") != -1 && document.getElementById)?true:false;
var isSafari = (navigator.userAgent.indexOf("Safari") != -1);

// ---------------------------------------
// Form Action Functions |||||||||||||||||
// ---------------------------------------

function getFormElementPos(form, name){

  // Get all elements
  elms = form.elements;
 
  // Find out the (first) current element
  for (var i = 0 ; i < elms.length ; i++){
    if (elms[i].name == name){
      return i;
    }
  }
  return -1;
}

function updateOptionMenu(index, tgtOptions, group){
  for (m = tgtOptions.length-1; m > 0; m--) {
    tgtOptions[m]=null
  }
  for (i = 0; i < group[index].length; i++){
    tgtOptions[i]=new Option(group[index][i].text, group[index][i].value)
  }
  tgtOptions[0].selected=true
}

function setField(field, value) {
  if (field != null) {
    field.value = value;
  }
}

function clearField(e1, e2, e3, e4) {
    if (e1 != null) {
      e1.value = "";
      if (e1.selectedIndex)
        e1.selectedIndex = 0;
    }
    if (e2 != null) {
      e2.value = "";
      if (e2.selectedIndex)
        e2.selectedIndex = 0;
    }
    if (e3 != null) {
      e3.value = "";
      if (e3.selectedIndex)
        e3.selectedIndex = 0;
    }
    if (e4 != null) {
      e4.value = "";
      if (e4.selectedIndex)
        e4.selectedIndex = 0;
    }
}

function blankField(e1, e2, e3, e4) {
    if (e1 != null) {
      e1.value = " ";
    }
    if (e2 != null) {
      e2.value = " ";
    }
    if (e3 != null) {
      e3.value = " ";
    }
    if (e4 != null) {
      e4.value = " ";
    }
}


function uncheckElement(form, i1, i2, i3) {
  if (i1 != null && i1 >= 0) {
    form.elements[i1].checked = false;
  }
  if (i2 != null && i2 >= 0) {
    form.elements[i2].checked = false;
  }
  if (i3 != null && i3 >= 0) {
    form.elements[i3].checked = false;
  }
}

function uncheckAll(form, name, checkbox) {
  if (name != null) {
    var elements = form.elements[name];
    for(i = 0; i < elements.length; i++) {
      if (elements[i] != checkbox) {
        elements[i].checked = false;
      }
    }
  }
}

function checkAll(form, name, checkbox) {
  checkAllwithId(form, name, checkbox, null, null);
}


function checkAllwithId(form, name, checkbox, id) {
  // Get all elements
  elms = form.elements;
 
  // Find out the (first) current check
  for (var i = 0 ; i < elms.length ; i++){
    if (elms[i].name != name)
      continue;
    
    if (id != null && elms[i].id.indexOf(id) < 0)
      continue;
      
    checkIt = !elms[i].checked;
    break;
  }

  // Check all in the toggle state
  for (var i = 0 ; i < elms.length ; i++){
    if (elms[i].name != name)
      continue;
    if (id != null && elms[i].id.indexOf(id) < 0)
      continue;
    elms[i].checked = checkIt;
  }
  checkbox.checked = true;
}

function selectAll(form, name){
 selectAllwithId(form, name,null);
}

function selectAllwithId(form, name, id) {

  // Get all elements
  elms = form.elements;
 
  // Find out the (first) current value
  for (var i = 0 ; i < elms.length ; i++){
    if (elms[i].name != name)
      continue;
    
    if (id != null && elms[i].id.indexOf(id) < 0)
      continue;
      
    selectIt = elms[i].selectedIndex;
    break;
  }

  // Check all in the toggle state
  for (var i = 0 ; i < elms.length ; i++){
    if (elms[i].name != name)
      continue;
    if (id != null && elms[i].id.indexOf(id) < 0)
      continue;
    elms[i].selectedIndex = selectIt;
  }
}

function getSelectedElements(form,name,sep){
  elms  = form.elements;
  value = "";
  cpt   = 0;
  for (var i = 0 ; i < elms.length ; i++){
    if (elms[i].name != name)
      continue;
    if (! elms[i].checked)
      continue;
      
    if (cpt > 0)
      value += sep;
    
    value += elms[i].value;
    cpt++;
  }
  return value;
}

// DEPRECATED should be removed
function checkAndSubmitForm(form, field1, field2, error1, error2) {
   
  if (field1 && field1.value == ""){
    alert(error1);
    return;
  }
  
  if (field2 && field2.value == ""){
    alert(error2);
    return;
  }
  
  if (form.onsubmit)
    form.onsubmit();
  form.submit();
}

// ---------------------------------------
// Functions for select element (<select><option>)
// ---------------------------------------

  
  // Select all element in the given select form input
  function selectAllInSelectElement(elSel)
  {
    for (var i = 0; i < elSel.length; i++) {
      if (elSel.options[i] != null) {
        elSel.options[i].selected = true;
      }
    }
  }
  
  // Add new option to the given select form element, from the input element
  function addInputToSelectElement(elInput, elSel)
  {
    var elOptNew = document.createElement('option');
    elOptNew.text = elInput.value;
    elOptNew.value = elInput.value;
    
    for (var i = 0; i < elSel.length; i++) {
      if (elSel.options[i] != null && elSel.options[i].value == elOptNew.value) {
        return; // do not add anything if value already there
      }
    }  
    try {
      elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
    }
    catch(ex) {
      elSel.add(elOptNew); // IE only
    }
  }
  
  // Remove the selected option of the given select form element
  function removeSelectedOptionFromSelectElement(elSel)
  {
    for (var i = elSel.length - 1; i >= 0; i--) {
      if (elSel.options[i] != null && elSel.options[i].selected) {
        elSel.remove(i);
      }
    }
  }

// ---------------------------------------
// Used ? sould be removed |||||||||||||||
// ---------------------------------------


function showLayer(id, action) {
  if (isIE) {
    eval("document.all." + id + ".style.visibility='" + action + "'");
  } else if (isNN4) {
    eval("document." + id + ".style.visibility='" + action + "'");
  } else {
    eval("document.getElementById('" + id + "').style.visibility='" + action + "'");
  }
}

function toggleEltVisibility(elt, cond) {
  if (cond) {
    document.getElementById(elt).style.visibility = "visible";
  } else {
    document.getElementById(elt).style.visibility = "hidden";
  }
}

function toggleEltDisplay(id) {
  elt = document.getElementById(id);
  if (elt.style.display == "none") {
    elt.style.display = "block";
  } else {
    elt.style.display = "none";
  }
}

// ---------------------------------------
// ImgBrowser Functions ||||||||||||||||||
// ---------------------------------------
function insertSelectedPub(id, label, targetInput, targetLabel, imgUrl) {
  document.editForm.elements[ targetInput ].value = id;
  document.editForm.elements[ targetLabel ].value = label;
}

function insertSelectedImg(id, label, targetInput, targetLabel, imgUrl) {
  document.editForm.elements[ targetInput ].value = imgUrl;
}

// ---------------------------------------
// Vertical Menu Functions |||||||||||||||
// ---------------------------------------

var expandMenu = false;

function vMenuOver(obj, iconOpen, iconClosed) {
  expandMenu = true;
  icon = obj.className=="open" ? iconClosed : iconOpen;
  obj.src = icon;
}
function vMenuOut(obj, iconOpen, iconClosed) {
  expandMenu = false;
  icon = obj.className=="open" ? iconOpen : iconClosed;
  obj.src = icon;
}

// Expand menu only if the mouse is over an expand icon
function vMenuLink(obj, iconOpen, iconClosed) {
  if (expandMenu == true) {
    toggleMenu(obj, iconOpen, iconClosed);
    return false;
  }
}

// Toggle the menu visibility and change the expand icon
function toggleMenu(obj, iconOpen, iconClosed) {
  if (obj.parentNode.lastChild.className=="open") {
    obj.parentNode.lastChild.className = obj.firstChild.className = "close";
    obj.firstChild.src = iconOpen;
  } else {
    obj.parentNode.lastChild.className = obj.firstChild.className = "open";
    obj.firstChild.src = iconClosed;
  }
}

// ---------------------------------------
// Move Form Functions |||||||||||||||||||
// ---------------------------------------

// Move up/down or delete selected option of a list of select
function moveFormOption(form, pos, op, first, last) {
  array = form.elements;

  // Move Up
  if (op == "up") {
    if (pos > first) { 
      tmp = array[pos].selectedIndex;
      array[pos].selectedIndex = array[pos - 1].selectedIndex;
      array[pos - 1].selectedIndex = tmp;
    } else {
      tmp = array[first].selectedIndex;
      for(i = first; i < last; i++) {
        array[i].selectedIndex = array[i + 1].selectedIndex;
      }
      array[last].selectedIndex = tmp;
    }
  } 
  // Move Down
  else if (op == "down") {
    if (pos < last) {
      tmp = array[pos].selectedIndex;
      array[pos].selectedIndex = array[pos + 1].selectedIndex;
      array[pos + 1].selectedIndex = tmp;
    } else {
      tmp = array[last].selectedIndex;
      for(i = last; i >= first; i--) {
        array[i].selectedIndex = array[i - 1].selectedIndex;
      }
      array[first].selectedIndex = tmp;
    }
  }
  // Remove Element
  else if (op == "remove") {
    for(i = pos; i < last; i++) {
      array[i].selectedIndex = array[i + 1].selectedIndex;
    }
    array[last].selectedIndex = 0;

  }
}

// Move up/down or delete value of list of input
function moveFormElement(form, pos, op, first, last) {
  array = form.elements;

  // Move Up
  if (op == "up") {
    if (pos > first) { 
      tmp = array[pos].value;
      array[pos].value = array[pos - 1].value;
      array[pos - 1].value = tmp;
    } else {
      tmp = array[first].value;
      for(i = first; i < last; i++) {
        array[i].value = array[i + 1].value;
      }
      array[last].value = tmp;
    }
  } 
  // Move Down
  else if (op == "down") {
    if (pos < last) {
      tmp = array[pos].value;
      array[pos].value = array[pos + 1].value;
      array[pos + 1].value = tmp;
    } else {
      tmp = array[last].value;
      for(i = last; i >= first; i--) {
        array[i].value = array[i - 1].value;
      }
      array[first].value = tmp;
    }
  }
  // Remove Element
  else if (op == "remove") {
    for(i = pos; i < last; i++) {
      array[i].value = array[i + 1].value;
    }
    array[last].value = "";

  }
}

// Move up/down or delete value of list of couple of input
function move2FormElement(form, pos, op, first, last) {
  array = form.elements;

  // Move Up
  if (op == "up") {
    if (pos > (first + 1)) { 
      tmp = array[pos].value;
      array[pos].value = array[pos - 2].value;
      array[pos - 2].value = tmp;

      tmp = array[pos - 1].value;
      array[pos - 1].value = array[pos - 3].value;
      array[pos - 3].value = tmp;
    } else {
      tmp1 = array[first].value;
      tmp2 = array[first + 1].value;
      for(i = first ; i < last; i++) {
        array[i].value = array[i + 2].value;
      }
      array[last - 1].value = tmp1;
      array[last].value = tmp2;
    }
  } 
  // Move Down
  else if (op == "down") {
    if (pos < last) {
      tmp = array[pos].value;
      array[pos].value = array[pos + 2].value;
      array[pos + 2].value = tmp;

      tmp = array[pos - 1].value;
      array[pos - 1].value = array[pos + 1].value;
      array[pos + 1].value = tmp;
    } else {
      tmp1 = array[last].value;
      tmp2 = array[last - 1].value;
      for(i = last; i >= first; i--) {
        array[i].value = array[i - 2].value;
      }
      array[first + 1].value = tmp1;
      array[first].value = tmp2;
    }
  }
  // Remove Element
  else if (op == "remove") {
    for(i = pos - 1; i < last; i++) {
      array[i].value = array[i + 2].value;
    }
    array[last - 1].value = "";
    array[last].value = "";

  }
}

// ---------------------------------------
// Change URL Functions ||||||||||||||||||
// ---------------------------------------

function getUrlWithUpdatedParam(url,param,value){
  var targeturl = url.toString();
      
  re1 = new RegExp("([^\?]*\\?.*)("+param+"=[^&]*&?)(.*)","i");
  re2 = new RegExp("([^\?]*\\?)(.*)","i");
  re3 = new RegExp("([^\?]*)","i");
  
  if (targeturl.search(re1) != -1){
    if (value)
      targeturl = targeturl.replace(re1,"$1"+param+"="+value+"&$3");
    else
      targeturl = targeturl.replace(re1,"$1"+"$3");
  }
  else if (targeturl.search(re2) != -1){ 
    if (value)
      targeturl = targeturl.replace(re2,"$1"+param+"="+value+"&$2");
    else
      targeturl = targeturl.replace(re2,"$1"+"$2");
  }
  else { 
    if (value)
      targeturl = targeturl.replace(re3,"$1?"+param+"="+value);
    else
      targeturl = targeturl.replace(re3,"$1");
  }
  
  return targeturl;
}

// ---------------------------------------
// Prompt and Confirm Functions ||||||||||
// ---------------------------------------

function popupWindow(url, title, w, h, status, resizable, scrollbars, reuse, winOpener){
  Popup.popupWindow(url, title, w, h, status, resizable, scrollbars, reuse, winOpener);
}

function promptAction(msg , url, param, defvalue){
  value = top.prompt(msg,defvalue);
  if (value) {
    document.location = getUrlWithUpdatedParam(url, param, value);
  }
}

function confirmAction(msg, url) {
  if (top.confirm(msg)) {
    document.location = url;
  }
}

/*
* This function will not return until (at least)
* the specified number of milliseconds have passed.
* It does a busy-wait loop.
*/
function pause(numberMillis) {
    var now = new Date();
    var exitTime = now.getTime() + numberMillis;
    while (true) {
        now = new Date();
        if (now.getTime() > exitTime)
            return;
    }
}

// ---------------------------------------
// DOM, CrossBrowser and eventHandling |||
// ---------------------------------------

// Create a equivalent to the IE event object for Gecko and such // TO BE COMPLETED
if (window.captureEvents && !isOPERA) {

 window.event = new Object() ;

 // 1. MOUSEMOVE
 window.captureEvents( Event.MOUSEMOVE ) ;
 function setMouseMoveEvent(e) {
   event.clientX = e.pageX-document.body.scrollLeft ;
   event.clientY = e.pageY-document.body.scrollTop ;
 }
 window.onmousemove = setMouseMoveEvent ;

 // 2. MOUSEDOWN AND MOUSEUP
 window.captureEvents( Event.MOUSEDOWN | Event.MOUSEUP ) ;
 function setMouseClickEvent(e) {
  setMouseMoveEvent(e) ;
  var b = e.which ;
  if ( b == 1 ) { // left
   event.button = 1 ;
  } else if ( b == 3 ) { // right
   event.button = 2 ;
  } else if ( b == 2 ) { // middle
   event.button = 4 ;
  } else {
   event.button = b ;
  }
 }
 window.onmousedown = setMouseClickEvent ;
 window.onmouseup = setMouseClickEvent ;

 // 3. KEYDOWN AND KEYUP
 window.captureEvents( Event.KEYDOWN | Event.KEYUP ) ;
 function setKeyEvent(e) {
  var k = e.which ;
  event.keyCode = k ;
  var p = ( e.type == 'keydown' ) ;
  if ( k == 16 ) {
   event.shiftKey = p ;
  } else if ( k == 17 ) {
   event.ctrlKey = p ;
  } else if ( k == 18 ) {
   event.altKey = p ;
  }
 }
 window.onkeydown = setKeyEvent ;
 window.onkeyup = setKeyEvent ;

}

if (!document.getElementById) {
 document.getElementById = function (sId) {
  if (document.all) return document.all[sId] ;
  if (document.layers) return document.layers[sId] ;
 }
}
function getElement(idStr) { ///// Got to be removed |||||||||||||||||
 if (document.getElementById) return document.getElementById(idStr) ;
 if (document.all) return document.all[idStr] ;
 if (document.layers) return document.layers[idStr] ;
}
function getElementStyle(idStr) {
 if (document.layers) return getElement(idStr) ;
 else return getElement(idStr).style ;
}


// ---------------------------------------
// Toggle Combo Functions ||||||||||||||||
// ---------------------------------------

var oldComboDivId = null;

function toggleCombo(e, comboDivId) {
  if (oldComboDivId != null && oldComboDivId != comboDivId) {
    toggleCombo(event, oldComboDivId);
  }
  var comboDiv = getElement(comboDivId);
  if (comboDiv == null) {
    alert("Invalid div ID: "+ comboDivId);
    return;
  }
  var comboIFrame = getElement(comboDivId + '_iFrame');
  if (comboDiv.style.display == "none") {
    // Retrieve mouse position
    var posx = 0;
    var posy = 0;
    if (!e) var e = window.event;
    if (e.pageX || e.pageY) {
      posx = e.pageX;
      posy = e.pageY;
    } else if (e.clientX || e.clientY) {
      posx = e.clientX + document.body.scrollLeft;
      posy = e.clientY + document.body.scrollTop;
    }
    // Move comboDiv into body and show it
    var bodyNode = document.getElementsByTagName('body')[0];
    bodyNode.appendChild(comboDiv);//, bodyNode.firstChild);
    comboDiv.style.top = posy+5+'px';
    comboDiv.style.left = posx+5+'px';
    comboDiv.style.display = "block";
    // Do the same with the iframe
    if (comboIFrame && !isOPERA && !isSafari) {
      var bodyNode = document.getElementsByTagName('body')[0];
      bodyNode.insertBefore(comboIFrame, comboDiv);
      comboIFrame.style.width = comboDiv.offsetWidth+'px';
      comboIFrame.style.height = comboDiv.offsetHeight+'px';
      comboIFrame.style.top = comboDiv.style.top;
      comboIFrame.style.left = comboDiv.style.left;
      comboIFrame.style.zIndex = comboDiv.style.zIndex - 1;
      comboIFrame.style.display = "block";
    }
    oldComboDivId = comboDivId;
  } else {
    comboDiv.style.display = "none";
    if (comboIFrame) {
      comboIFrame.style.display = "none";
    }
    oldComboDivId = null;
  }
}

function toggleLangCombo(event) {
  toggleCombo(event, 'langCombo');
}

function toggleWorkspaceCombo(event) {
  toggleCombo(event, 'workspaceCombo');
}

function toggleEditCombo(event, id) {
  toggleCombo(event, id);
}

// ---------------------------------------
// Caddy Functions ||||||||||||||||||||
// ---------------------------------------

function swapImgSrcAndDescription(img, secondSrc, secondDescription) {
  if (!img.secondSrc) {
    img.secondSrc = secondSrc;
    img.secondTitle = secondDescription + ".";
    img.secondAlt = secondDescription;
	  // preload second image
	  if (document.images) {
	    secondImgPreload = new Image();
	    secondImgPreload.src = img.secondSrc; 
	  }
  }
  // Swap Values
  var oldSrc = img.src;
  var oldTitle = img.title;
  var oldAlt = img.alt;
  img.src = img.secondSrc;
  img.title = img.secondTitle;
  img.alt = img.secondAlt;
  img.secondSrc = oldSrc;
  img.secondTitle = oldTitle;
  img.secondAlt = oldAlt;
}

// ---------------------------------------
// Progress Functions ||||||||||||||||||||
// ---------------------------------------


function progressInsert(msg) {
  document.write("<div id='progress' style=\"position:absolute;display:none;top:200;left:200;border: 1px black solid;padding:3px 3px 3px 3px;background-color:#FFFFFF;font-family: sans-serif;font-size: 80%\"><NOBR><img src='images/jalios/icons/wait.gif' align='texttop'><br>" + msg + "</NOBR></div>");
  document.onmousemove = getMousePosition;
}

function progressShow() {
  document.all["progress"].style.top = mouseY + 10;
  document.all["progress"].style.left = mouseX + 10;
  document.all["progress"].visibility= "visible";
  document.all["progress"].style.display = "block";
}

function progressHide() {
  document.all["progress"].style.display = "none";
}

// ---------------------------------------
// Mouse Functions |||||||||||||||||||||||
// ---------------------------------------


function getMousePosition(e) {
  if (!isIE) {
    mouseX = e.pageX;
    mouseY = e.pageY;
  }
  if (isIE) {
    mouseX = event.clientX;
    mouseY = event.clientY;
  }
  return true;
}

// ---------------------------------------
// On Load Functions |||||||||||||||||||||
// ---------------------------------------


function doOnLoad(){
  setupAllTabs();
}

function getJsonRPC() {
  if (!document.JCMS_jsonrpc && document.JCMS_AJAXenabled == true) {
    document.JCMS_jsonrpc = new JSONRpcClient(document.JCMS_contextPath + "/JSON-RPC");
  }
  return document.JCMS_jsonrpc;
}


// Initialization hook up
Event.observe(window, 'load', function() { doOnLoad(); });

