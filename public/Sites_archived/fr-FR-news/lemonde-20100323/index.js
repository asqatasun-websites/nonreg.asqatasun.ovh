
function retournerHtmlOreilleNonAbo()
{
   //appel du getter pour forcer l acces au cookie info_user_web
   var html_oreille = '';

   // lors d un premier affichage de la une, ce test repond false
   // on affiche alors une oreille_gold_vide, celle ci sera mise � jour apres l appel AJAX de maj des cookie perso
   if (MIA.ServicesAbonnes.testerExistenceInfo('info_user'))
   {
      var civilite = MIA.ServicesAbonnes.getInfoInUserCookie()['info_user']['civilite'];
      var nom = MIA.ServicesAbonnes.getInfoInUserCookie()['info_user']['nom'];
      var prenom = MIA.ServicesAbonnes.getInfoInUserCookie()['info_user']['prenom'];
      var existe_email = MIA.ServicesAbonnes.getInfoInUserCookie()['info_user']['email_lemonde'];
      var login = MIA.ServicesAbonnes.getInfoInUserCookie()['info_user']['login'];
      var email_faux = MIA.ServicesAbonnes.getInfoInUserCookie()['info_user']['email_faux'];

      var nb_objets_classeur = 0;
      if (MIA.ServicesAbonnes.testerExistenceInfo('classeur'))
         nb_objets_classeur = MIA.ServicesAbonnes.getInfoInUserCookie()['classeur']['nb_items'];

      var nb_credits_archive = 0;
      if (MIA.ServicesAbonnes.testerExistenceInfo('archives'))
         nb_credits_archive = MIA.ServicesAbonnes.getInfoInUserCookie()['archives']['credit'];

      var liste_villes_meteo = [];
      if (MIA.ServicesAbonnes.testerExistenceInfo('meteo'))
         liste_villes_meteo = MIA.ServicesAbonnes.getInfoInUserCookie()['meteo'];

      if (email_faux == '1')
      {
         html_oreille += '<a href="http://www.lemonde.fr/web/ep/email_modifier/1,27-0,1-0,0.html"><span style="color:red;">Attention votre adresse e-mail n\'est pas valide.<br>Cliquez ici pour la corriger.</span></a>';
      }
      else
      {
         html_oreille += '<div class="login">';
         html_oreille += 'Bienvenue ' + '<a href="http://www.lemonde.fr/web/ep/newsletters/1,27-0,1-0,0.html">' + civilite + '&nbsp;' + prenom + ' ' + nom + '</a>';
         html_oreille += '</div>';

         html_oreille += '<div class="weather">';

         var ligne_meteo = '';
         var temperature = '';
         var image_meteo = '';
         var desc_temps = '';
         var existe_meteo = 0;
         var nb_villes = liste_villes_meteo.length;
         for (ct = 0 ; ct < nb_villes ; ct++)
         {
            var nom_ville = liste_villes_meteo[ct]['nom_ville'];
            var id_ville = liste_villes_meteo[ct]['id_ville'];

            temperature = liste_villes_meteo[ct]['temperature'];
            image_meteo = liste_villes_meteo[ct]['icone'];
            desc_temps = liste_villes_meteo[ct]['description'];

            var url_meteo = 'http://www.lemonde.fr/web/meteo/0,31-0,42-XXXX,0.html';
            url_meteo = url_meteo.replace(/XXXX/gi, id_ville);

            if (image_meteo)
            {
               ligne_meteo = '   <img src="http://medias.lemonde.fr/mmpub/img/icn/met/pt/'+ image_meteo + '.gif" width="16" height="16" border="0" hspace="3" alt="'+ desc_temps +'" title="'+ desc_temps +'"></img>';
            }
            else
            {
               ligne_meteo = '';
            }

            if (nom_ville != "")
            {
               existe_meteo = 1;
               if (temperature)
               {
                  temperature = temperature +' &#186;C';
                  html_oreille += '   <a href="' + url_meteo + '">' + ligne_meteo + '&nbsp;' + nom_ville + '&nbsp;' + temperature + '</a>';
               }
               else
               {
                  html_oreille += '   <a href="' + url_meteo + '">' + ligne_meteo + '&nbsp;' + nom_ville + '</a>';
               }
            }
         }

         if (existe_meteo == 0)
         {
            html_oreille += '              <a href="http://www.lemonde.fr/web/ep/meteo/1,27-0,1-0,0.html">';
            html_oreille += '                 Personnalisez votre m&#233;t&#233;o</a>';
         }
         html_oreille += '   <div class="clear"></div>';
         html_oreille += '</div>';

         html_oreille += '<div class="quickLinks">';
         html_oreille += '   <div class="link"><a href="http://www.lemonde.fr/deconnexion/">Quitter</a></div>';
         html_oreille += '   <div class="link"><a href="http://www.lemonde.fr/abojournal/?xtor=AL-32280085&247SEM">Abonnez-vous au journal</a></div>';
         html_oreille += '   <div class="clear"></div>';
         html_oreille += '</div>';
      }
   }
   return html_oreille;
}

function mettreAjourOreillePersoNonAbo()
{
   if ($('#oreille_perso_non_abo'))
   {
      // ajout des classes CSS
      $('#oreille_perso_non_abo').removeClass();
      $('#oreille_perso_non_abo').addClass("links");
      $('#oreille_perso_non_abo').addClass("free");

      //ajout du code HTML
      var html_oreille = retournerHtmlOreilleNonAbo();
      $('#oreille_perso_non_abo').html(html_oreille);
   }
}


if (MIA.ServicesAbonnes.testerExistenceCookiePerso())
{
   document.write('<div id="oreille_perso_non_abo" class="links free">');
   document.write(retournerHtmlOreilleNonAbo());
   document.write('</div>');
}
else
{
   document.write('<!-- LOGIN -->');
   document.write('<div id="oreille_perso_non_abo" class="links default">');
   document.write('  <div class="login"><a href="#" onClick="$(\'#floatingBg\').css(\'opacity\', \'0.5\'); $(\'#loginBoxContainer\').animate({\'opacity\': \'show\'}, 800); $(\'#floatingBg\').fadeIn(800, function() {}); $(\'#login\').focus(); return false;"><img src="http://www.lemonde.fr/medias/www/1.2.216/img/bt/login_new.gif" alt="" border="0"></a></div>');
   document.write('  <div class="clear"></div>');
   document.write('  <div><a href="http://www.lemonde.fr/abojournal/?xtor=AL-32280085&247SEM" rel="nofollow"><img src="http://www.lemonde.fr/medias/www/1.2.216/img/icn/newspaper.jpg" alt="" border="0">Abonnez-vous au journal<br>Le Monde : 17&euro;/mois</a></div>');
   document.write('</div>');

   //register pour maj apres maj cookie
   MIA.ServicesAbonnes.ajouterFonctionMaj(mettreAjourOreillePersoNonAbo);
}
//
