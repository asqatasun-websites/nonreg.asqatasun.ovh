// Carga de SWFObject (libreria de inclusion de objetos flash en html)
if (!SWFObject) {
if(typeof deconcept=="undefined"){var deconcept=new Object();}if(typeof deconcept.util=="undefined"){deconcept.util=new Object();}if(typeof deconcept.SWFObjectUtil=="undefined"){deconcept.SWFObjectUtil=new Object();}deconcept.SWFObject=function(_1,id,w,h,_5,c,_7,_8,_9,_a){if(!document.getElementById){return;}this.DETECT_KEY=_a?_a:"detectflash";this.skipDetect=deconcept.util.getRequestParameter(this.DETECT_KEY);this.params=new Object();this.variables=new Object();this.attributes=new Array();if(_1){this.setAttribute("swf",_1);}if(id){this.setAttribute("id",id);}if(w){this.setAttribute("width",w);}if(h){this.setAttribute("height",h);}if(_5){this.setAttribute("version",new deconcept.PlayerVersion(_5.toString().split(".")));}this.installedVer=deconcept.SWFObjectUtil.getPlayerVersion();if(!window.opera&&document.all&&this.installedVer.major>7){deconcept.SWFObject.doPrepUnload=true;}if(c){this.addParam("bgcolor",c);}var q=_7?_7:"high";this.addParam("quality",q);this.setAttribute("useExpressInstall",false);this.setAttribute("doExpressInstall",false);var _c=(_8)?_8:window.location;this.setAttribute("xiRedirectUrl",_c);this.setAttribute("redirectUrl","");if(_9){this.setAttribute("redirectUrl",_9);}};deconcept.SWFObject.prototype={useExpressInstall:function(_d){this.xiSWFPath=!_d?"expressinstall.swf":_d;this.setAttribute("useExpressInstall",true);},setAttribute:function(_e,_f){this.attributes[_e]=_f;},getAttribute:function(_10){return this.attributes[_10];},addParam:function(_11,_12){this.params[_11]=_12;},getParams:function(){return this.params;},addVariable:function(_13,_14){this.variables[_13]=_14;},getVariable:function(_15){return this.variables[_15];},getVariables:function(){return this.variables;},getVariablePairs:function(){var _16=new Array();var key;var _18=this.getVariables();for(key in _18){_16[_16.length]=key+"="+_18[key];}return _16;},getSWFHTML:function(){var _19="";if(navigator.plugins&&navigator.mimeTypes&&navigator.mimeTypes.length){if(this.getAttribute("doExpressInstall")){this.addVariable("MMplayerType","PlugIn");this.setAttribute("swf",this.xiSWFPath);}_19="<embed type=\"application/x-shockwave-flash\" src=\""+this.getAttribute("swf")+"\" width=\""+this.getAttribute("width")+"\" height=\""+this.getAttribute("height")+"\" style=\""+this.getAttribute("style")+"\"";_19+=" id=\""+this.getAttribute("id")+"\" name=\""+this.getAttribute("id")+"\" ";var _1a=this.getParams();for(var key in _1a){_19+=[key]+"=\""+_1a[key]+"\" ";}var _1c=this.getVariablePairs().join("&");if(_1c.length>0){_19+="flashvars=\""+_1c+"\"";}_19+="/>";}else{if(this.getAttribute("doExpressInstall")){this.addVariable("MMplayerType","ActiveX");this.setAttribute("swf",this.xiSWFPath);}_19="<object id=\""+this.getAttribute("id")+"\" classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" width=\""+this.getAttribute("width")+"\" height=\""+this.getAttribute("height")+"\" style=\""+this.getAttribute("style")+"\">";_19+="<param name=\"movie\" value=\""+this.getAttribute("swf")+"\" />";var _1d=this.getParams();for(var key in _1d){_19+="<param name=\""+key+"\" value=\""+_1d[key]+"\" />";}var _1f=this.getVariablePairs().join("&");if(_1f.length>0){_19+="<param name=\"flashvars\" value=\""+_1f+"\" />";}_19+="</object>";}return _19;},write:function(_20){if(this.getAttribute("useExpressInstall")){var _21=new deconcept.PlayerVersion([6,0,65]);if(this.installedVer.versionIsValid(_21)&&!this.installedVer.versionIsValid(this.getAttribute("version"))){this.setAttribute("doExpressInstall",true);this.addVariable("MMredirectURL",escape(this.getAttribute("xiRedirectUrl")));document.title=document.title.slice(0,47)+" - Flash Player Installation";this.addVariable("MMdoctitle",document.title);}}if(this.skipDetect||this.getAttribute("doExpressInstall")||this.installedVer.versionIsValid(this.getAttribute("version"))){var n=(typeof _20=="string")?document.getElementById(_20):_20;n.innerHTML=this.getSWFHTML();return true;}else{if(this.getAttribute("redirectUrl")!=""){document.location.replace(this.getAttribute("redirectUrl"));}}return false;}};deconcept.SWFObjectUtil.getPlayerVersion=function(){var _23=new deconcept.PlayerVersion([0,0,0]);if(navigator.plugins&&navigator.mimeTypes.length){var x=navigator.plugins["Shockwave Flash"];if(x&&x.description){_23=new deconcept.PlayerVersion(x.description.replace(/([a-zA-Z]|\s)+/,"").replace(/(\s+r|\s+b[0-9]+)/,".").split("."));}}else{if(navigator.userAgent&&navigator.userAgent.indexOf("Windows CE")>=0){var axo=1;var _26=3;while(axo){try{_26++;axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash."+_26);_23=new deconcept.PlayerVersion([_26,0,0]);}catch(e){axo=null;}}}else{try{var axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");}catch(e){try{var axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");_23=new deconcept.PlayerVersion([6,0,21]);axo.AllowScriptAccess="always";}catch(e){if(_23.major==6){return _23;}}try{axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash");}catch(e){}}if(axo!=null){_23=new deconcept.PlayerVersion(axo.GetVariable("$version").split(" ")[1].split(","));}}}return _23;};deconcept.PlayerVersion=function(_29){this.major=_29[0]!=null?parseInt(_29[0]):0;this.minor=_29[1]!=null?parseInt(_29[1]):0;this.rev=_29[2]!=null?parseInt(_29[2]):0;};deconcept.PlayerVersion.prototype.versionIsValid=function(fv){if(this.major<fv.major){return false;}if(this.major>fv.major){return true;}if(this.minor<fv.minor){return false;}if(this.minor>fv.minor){return true;}if(this.rev<fv.rev){return false;}return true;};deconcept.util={getRequestParameter:function(_2b){var q=document.location.search||document.location.hash;if(_2b==null){return q;}if(q){var _2d=q.substring(1).split("&");for(var i=0;i<_2d.length;i++){if(_2d[i].substring(0,_2d[i].indexOf("="))==_2b){return _2d[i].substring((_2d[i].indexOf("=")+1));}}}return "";}};deconcept.SWFObjectUtil.cleanupSWFs=function(){var _2f=document.getElementsByTagName("OBJECT");for(var i=_2f.length-1;i>=0;i--){_2f[i].style.display="none";for(var x in _2f[i]){if(typeof _2f[i][x]=="function"){_2f[i][x]=function(){};}}}};if(deconcept.SWFObject.doPrepUnload){if(!deconcept.unloadSet){deconcept.SWFObjectUtil.prepUnload=function(){__flash_unloadHandler=function(){};__flash_savedUnloadHandler=function(){};window.attachEvent("onunload",deconcept.SWFObjectUtil.cleanupSWFs);};window.attachEvent("onbeforeunload",deconcept.SWFObjectUtil.prepUnload);deconcept.unloadSet=true;}}if(!document.getElementById&&document.all){document.getElementById=function(id){return document.all[id];};}var getQueryParamValue=deconcept.util.getRequestParameter;var FlashObject=deconcept.SWFObject;var SWFObject=deconcept.SWFObject;
}

var iCont = 0;
var iWidth1 = 0;
var iHeight1 = 0;
var sDiv1 = "";
var sFile1 = "";
var sPreview1 = "";
var floating1 = false;
var fullscreen = false;
var sZonas = "";
function getKeyWords(title) {
	title = title.toLowerCase();
	title = title.replace(/[����]+/g, "a");
	title = title.replace(/[����]+/g, "e");
	title = title.replace(/[����]+/g, "i");
	title = title.replace(/[����]+/g, "o");
	title = title.replace(/[����]+/g, "u");
	title = title.replace(/[�]+/g, "c");
	title = title.replace(/\b/g, "_");
	title = title.replace(/\W/g, "");
	var words = title.split("_");
	var keywords = "";
	for ( var i = 0; i < words.length; i++) {
		if (3 < words[i].length)
			keywords += "," + words[i];
	}
	if (keywords != "")
		keywords = keywords.substring(1);

	return keywords;
}
//Player antiguo
function showPlayer(sPlayer, sTags, sZonas, bStretch) {
	var fo = new SWFObject(sPlayer, "movie_player", iWidth1, iHeight1, 8,
			"#000000");
	fo.addParam("allowfullscreen", fullscreen1);
	fo.addParam("wmode", "transparent");
	fo.addVariable("image", sPreview1);
	fo.addVariable("width", iWidth1);
	if (typeof (bStretch) != 'undefined')
		fo.addVariable("overstretch", bStretch);
	else
		fo.addVariable("overstretch", "true");
	fo.addVariable("height", iHeight1);
	fo.addVariable("file", sFile1);
	if (floating1)
		fo.addVariable("displayheight", iHeight1);
	fo.addVariable("sZonas", sZonas);
	fo.addVariable("p1U", sZonas);
	fo.addVariable("sTags", sTags);
	fo.write(sDiv1);
}

function loadMethodeVideo(div, w, h, sFile, sPreview, fullscreen, floating,
		sPortal, sZonas, sTitle, sCategory, bStretch) {
	
	/*var opciones = {
		categoriaPubli: sCategory,
		titulo: sTitle,
		imgPrevia: sPreview,
		site: sPortal
	};
	
	PlayerWebTV.cargaVideo(div, sFile, w, h, opciones);*/
	loadMultimedia(div, w, h, sFile, sPreview, sPortal, sTitle, sCategory);
}

function loadMultimedia(div, w, h, sFile, sPreview, sPortal, sTitle, sCategory) {	
	var opciones = {
		categoriaPubli: sCategory,
		titulo: sTitle,
		imgPrevia: sPreview,
		site: sPortal
	};
	
	PlayerWebTV.cargaVideo(div, sFile, w, h, opciones);
}

/**
  Obtiene el OAS Site Page correcto en funci�n del valor de la variable que tiene esta pagina
*/
function getOAS_sitepage(sOAS_sitepage) {

	var campos = sOAS_sitepage.split('/');		
	var campos2 = campos[0].split('.');
	
	switch (campos2[1]) {
		case 'elcomercio-sa':
			var periodico = 'elcomerciodigital';
			break;
		case 'diario-elcorreo':
			var periodico = 'elcorreodigital';
			break;
		case 'la-verdad':
			var periodico = 'laverdad';
			break;
		case 'lavozdecadiz':
			var periodico = 'lavozdigital';
			break;
		default:
			var periodico = campos2[1];
			break;	
	}
	
	//var ediciones = new Array('alava', 'vizcaya', 'aviles', 'gijon', 'oviedo', 'almeria', 'granada', 'jaen', 'alicante', 'valencia', 'albacete', 'murcia', 'cadiz', 'jerez', 'castellon');		
	var separador = '/';	
	return 'vocento.' + periodico + separador + campos[1];

}
	

/**
 * Constructor
 */
function PlayerWebTV() {
	this.initted = false;
	this.portal = null;
	this.mosca  = null;
	this.rutaConfig  = null;
}

// ************************************************************************
// ******************  CONSTANTES *****************************************
//************************************************************************
PlayerWebTV.MIN_ANCHO_PLAYER = 180;
PlayerWebTV.MIN_ALTO_PLAYER = 100;
PlayerWebTV.BASE_CONFIG  = "/includes/manuales/swf/playerWebTV/";
PlayerWebTV.VERSION_FLASH  = 8;

/**
 * CTES identificadoras de los portales
 */
PlayerWebTV.IDS_PORTALES = {
	ABC: "abc",
	ABC_SEVILLA: "abcdesevilla",
	NORTE_CASTILLA: "nortecastilla",
	LA_VOZ: "lavozdigital",
	DIARIO_MONTANES: "eldiariomontanes",
	DIARIO_SUR: "diariosur",
	DIARIO_VASCO: "diariovasco",
	EL_COMERCIO: "elcomerciodigital",
	EL_CORREO: "elcorreo",
	EL_CORREODIGITAL: "elcorreodigital",
	HOY: "hoy",
	IDEAL: "ideal",
	LA_RIOJA: "larioja",
	LA_VERDAD: "laverdad",
	LAS_PROVINCIAS: "lasprovincias",
	SUR_INGLES:	"sureng"
};

/**
 * Contador para retrasar la carga de multiples videos a la vez en IExplorer
 */
PlayerWebTV.ieCounter = 1;

/**
 * entorno en el que nos encontramos
 */
PlayerWebTV.entorno = getEntorno();

//TODO: faltan mas configuraciones de portales
/**
 * Informacion de los portales
 */
PlayerWebTV.INFO_PORTALES = {};
PlayerWebTV.INFO_PORTALES[PlayerWebTV.IDS_PORTALES.ABC] = {
	"resizer": "abc.es",
	"servidores": ["abc.es", "http://212.81.129.25:8280", "http://212.81.129.25", "http://preproduccionabc.abc.es"],
	"dominio": PlayerWebTV.entorno,
	"baseConfig": "/swf/playerWebTV/"
};
PlayerWebTV.INFO_PORTALES[PlayerWebTV.IDS_PORTALES.ABC_SEVILLA] = {
	"resizer": "abc.es",
	"servidores": ["abcdesevilla.es", "http://212.81.129.25:8281", "http://212.81.129.25", "http://preproduccionsevilla.abc.es"],
	"dominio": PlayerWebTV.entorno,
	"baseConfig": "/swf/playerWebTV/"
};
PlayerWebTV.INFO_PORTALES[PlayerWebTV.IDS_PORTALES.NORTE_CASTILLA] = {
	"resizer": "nortecastilla.es",
	"servidores": [PlayerWebTV.entorno + ".nortecastilla.es"],
	"dominio": "http://" + PlayerWebTV.entorno + ".nortecastilla.es"
};
PlayerWebTV.INFO_PORTALES[PlayerWebTV.IDS_PORTALES.LA_VOZ] = {
	"resizer": "lavozdigital.es",
	"servidores": [PlayerWebTV.entorno + ".lavozdigital.es"],
	"dominio": "http://" + PlayerWebTV.entorno + ".lavozdigital.es"
};
PlayerWebTV.INFO_PORTALES[PlayerWebTV.IDS_PORTALES.DIARIO_MONTANES] = {
	"resizer": "eldiariomontanes.es",
	"servidores": [PlayerWebTV.entorno + ".eldiariomontanes.es"],
	"dominio": "http://" + PlayerWebTV.entorno + ".eldiariomontanes.es"
};
PlayerWebTV.INFO_PORTALES[PlayerWebTV.IDS_PORTALES.DIARIO_SUR] = {
	"resizer": "diariosur.es",
	"servidores": [PlayerWebTV.entorno + ".diariosur.es"],
	"dominio": "http://" + PlayerWebTV.entorno + ".diariosur.es"
};
PlayerWebTV.INFO_PORTALES[PlayerWebTV.IDS_PORTALES.SUR_INGLES] = {
		"resizer": "surinenglish.com",
		"servidores": [PlayerWebTV.entorno + ".surinenglish.com"],
		"dominio": "http://" + PlayerWebTV.entorno + ".surinenglish.com"
};
PlayerWebTV.INFO_PORTALES[PlayerWebTV.IDS_PORTALES.DIARIO_VASCO] = {
	"resizer": "diariovasco.com",
	"servidores": [PlayerWebTV.entorno + ".diariovasco.com"],
	"dominio": "http://" + PlayerWebTV.entorno + ".diariovasco.com"
};
PlayerWebTV.INFO_PORTALES[PlayerWebTV.IDS_PORTALES.EL_COMERCIO] = {
	"resizer": "elcomerciodigital.com",
	"servidores": [PlayerWebTV.entorno + ".elcomerciodigital.com"],
	"dominio": "http://" + PlayerWebTV.entorno + ".elcomerciodigital.com"
};
PlayerWebTV.INFO_PORTALES[PlayerWebTV.IDS_PORTALES.EL_CORREO] = {
	"resizer": "elcorreo.com",
	"servidores": [PlayerWebTV.entorno + ".elcorreo.com"],
	"dominio": "http://" + PlayerWebTV.entorno + ".elcorreo.com"
};
PlayerWebTV.INFO_PORTALES[PlayerWebTV.IDS_PORTALES.EL_CORREODIGITAL] = {
	"resizer": "elcorreo.com",
	"servidores": [PlayerWebTV.entorno + ".elcorreo.com"],
	"dominio": "http://" + PlayerWebTV.entorno + ".elcorreo.com"
};
PlayerWebTV.INFO_PORTALES[PlayerWebTV.IDS_PORTALES.HOY] = {
	"resizer": "hoy.es",
	"servidores": [PlayerWebTV.entorno + ".hoy.es"],
	"dominio": "http://" + PlayerWebTV.entorno + ".hoy.es"
};
PlayerWebTV.INFO_PORTALES[PlayerWebTV.IDS_PORTALES.IDEAL] = {
	"resizer": "ideal.es",
	"servidores": [PlayerWebTV.entorno + ".ideal.es"],
	"dominio": "http://" + PlayerWebTV.entorno + ".ideal.es"
};
PlayerWebTV.INFO_PORTALES[PlayerWebTV.IDS_PORTALES.LA_RIOJA] = {
	"resizer": "larioja.com",
	"servidores": [PlayerWebTV.entorno + ".larioja.com"],
	"dominio": "http://" + PlayerWebTV.entorno + ".larioja.com"
};
PlayerWebTV.INFO_PORTALES[PlayerWebTV.IDS_PORTALES.LA_VERDAD] = {
	"resizer": "laverdad.es",
	"servidores": [PlayerWebTV.entorno + ".laverdad.es"],
	"dominio": "http://" + PlayerWebTV.entorno + ".laverdad.es"
};
PlayerWebTV.INFO_PORTALES[PlayerWebTV.IDS_PORTALES.LAS_PROVINCIAS] = {
	"resizer": "lasprovincias.es",
	"servidores": [PlayerWebTV.entorno + ".lasprovincias.es"],
	"dominio": "http://" + PlayerWebTV.entorno + ".lasprovincias.es"
};

/**
 * Cach� de controladores para el player
 */
PlayerWebTV.cache = {};

//************************************************************************
//******************  METODOS ESTATICOS **********************************
//************************************************************************

/**
 * Obtiene la extension del fichero multimedia
 */
PlayerWebTV.getExtension = function(video) {
	var result = video;
	
	while (result.indexOf('/') != -1) {
		result = result.substring(result.indexOf('/')+1, result.length);
	}
	
	if (result.indexOf('.') != -1) {
		result = result.substring(result.indexOf('.')+1, result.length);
	}
	
	return result;
};

/**
 * Obtiene el entorno en el que nos encontramos
 */
function getEntorno() {
	var result = "www";

	if 		(location.href.indexOf('http://www.abc.es') != -1) result = 'http://www.abc.es';
	else if (location.href.indexOf('http://www.abcdesevilla.es') != -1) result = 'http://www.abcdesevilla.es';
	else if (location.href.indexOf('http://212.81.129.25:8280') != -1) result = 'http://212.81.129.25:8280';
	else if (location.href.indexOf('http://212.81.129.25:8281') != -1) result = 'http://212.81.129.25:8281';
	else if (location.href.indexOf('http://sevilla-origin.abc.es') != -1) result = 'http://www.abcdesevilla.es';
	else if (location.href.indexOf('http://www-origin.abc.es/') != -1) result = 'http://www.abc.es';
	else if (location.href.indexOf('http://n1abc10.abc.es') !=-1) result = 'http://www.abc.es';
	else if (location.href.indexOf('http://n2abc10.abc.es') !=-1) result = 'http://www.abc.es';
	else if (location.href.indexOf('http://n3abc10.abc.es') !=-1) result = 'http://www.abc.es';
	else if (location.href.indexOf('http://n4abc10.abc.es') !=-1) result = 'http://www.abc.es';
	else if (location.href.indexOf('http://preproduccionabc.abc.es') !=-1) result = 'http://www.abc.es';
	else if (location.href.indexOf('http://preproduccionsevilla.abc.es') !=-1) result = 'http://www.abcdesevilla.es';	
	else {		
		try {
			result = location.href.split('/')[2].split('.')[0];
			if ( (result != 'www') && (result != 'desarrollo') && (result != 'rediseno') && (result != 'redisenodev') ) {
				result = 'www';
			}
		} catch (e) {
		}		
	}
	
	return result;
}

/**
 * Comprueba si es ABC Desarrollo
 */
function isDesarrolloABC() {
	var result = false;

	if ( (location.href.indexOf('http://212.81.129.25:8280') != -1)  ||
		 (location.href.indexOf('http://212.81.129.25:8281') != -1) ) {
			result = true;
	}
	
	return result;
}

/**
 * Comprueba si es ABC en cualquier entorno
 */
function isABC() {
	var result = false;

	if ( (location.href.indexOf('http://www.abc.es') != -1)  ||
		 (location.href.indexOf('http://www.abcdesevilla.es') != -1)  ||
		 (location.href.indexOf('http://212.81.129.25:8280') != -1)  ||
		 (location.href.indexOf('http://212.81.129.25:8281') != -1) ||
		 (location.href.indexOf('http://sevilla-origin.abc.es') != -1)  ||
		 (location.href.indexOf('http://www-origin.abc.es/') != -1)  ||
		 (location.href.indexOf('http://n1abc10.abc.es') != -1)  ||
		 (location.href.indexOf('http://n2abc10.abc.es') != -1)  ||
		 (location.href.indexOf('http://n3abc10.abc.es') != -1)  ||
		 (location.href.indexOf('http://n4abc10.abc.es') != -1) ) {
			result = true;
	}
	
	return result;
}

/**
 * Obtiene el color de fondo
 */
function getColorFondo() {
	var colorFondo = '#000000';

	if (isABC()) colorFondo = '#f1f1f1';
	
	return colorFondo;
}

/**
 * Muestra el player cargandolo en la cache de portales
 */
PlayerWebTV.cargaVideo = function(div, sFile, w, h, opciones) {
	var funcion = PlayerWebTV.cargaVideo; 
	if (arguments.length < 5) {
		// Si no nos pasan un id de capa, creamos 1 en la posicion actual y la usamos para escribir el video
		// (CUIDADO!  Si la pagina ya esta cargada, siempre hay que indicar un id, sino borraria la pagina actual)
		var uid = this.getUID("video");
		document.write('<div id="' + uid + '"></div>');
		
		var args = new Array(5);
		args[0] = uid;
		for (var i = 0; i < arguments.length; i++) {
			args[i + 1] = arguments[i];
		}
		
		funcion.call(this, args[0], args[1], args[2], args[3], args[4]);
		return;
	}
	var player = null;
	var config = null;
	if (!opciones) {
		opciones = {};
	}
	
	if (opciones) {
		config = opciones.config || opciones.site;
	}
	if (config) {
		config = config.toLowerCase();
	}
	if (config == this.IDS_PORTALES.ABC) {
		var exp = /(sevilla)|(212\.81\.129\.25:8281)/gi;
		if (exp.test(location.host)) {
			config = this.IDS_PORTALES.ABC_SEVILLA;
		}
	} else if (config == "abcsevilla") {
		config = this.IDS_PORTALES.ABC_SEVILLA;
	} else if (config == "sur") {
		config = this.IDS_PORTALES.DIARIO_SUR;
	} else if (config == "diavas") {
		config = this.IDS_PORTALES.DIARIO_VASCO;
	} else if (config == "comerc") {
		config = this.IDS_PORTALES.EL_COMERCIO;
	} else if (config == "correo") {
		config = this.IDS_PORTALES.EL_CORREO;
	} else if (config == "diamon") {
		config = this.IDS_PORTALES.DIARIO_MONTANES;
	} else if (config == "idealm") {
		config = this.IDS_PORTALES.IDEAL;
	} else if (config == "rioja") {
		config = this.IDS_PORTALES.LA_RIOJA;
	} else if (config == "proval") {
		config = this.IDS_PORTALES.LAS_PROVINCIAS;
	} else if (config == "vermur") {
		config = this.IDS_PORTALES.LA_VERDAD;
	} else if (config == "vozcad" || config == "Lavoz" || config == "lavoz") {
		config = this.IDS_PORTALES.LA_VOZ;
	} else if (config == "norcas") {
		config = this.IDS_PORTALES.NORTE_CASTILLA;
	}
	
	if (!config) {
		player = new PlayerWebTV();
	} else {
		opciones.site = config;
		opciones.config = config;
		if (!PlayerWebTV.cache[config]) {
			PlayerWebTV.cache[config] = new PlayerWebTV();
		}
		player = PlayerWebTV.cache[config];
	}
	
	player.showVideo(div, sFile, w, h, opciones);
};

/**
 * Funcion utilidad para eliminar eventos
 */
PlayerWebTV.nada = function() {};


/**
 * Obtiene el portal del que cargar su configuracion
 * 
 * Si se indica como parametro, se devuelve ese, en otro caso, se calcula de la URL
 */
PlayerWebTV.getPortal = function(opciones) {
	var result = null;
	
	if (opciones && opciones.site) {
		result = opciones.site;
	} else {
		// Identificaci�n por URL
		var servidor = location.host;

		for (var portal in this.INFO_PORTALES) {
			var servidores = this.INFO_PORTALES[portal].servidores;
			
			for (var i = 0; i < servidores.length; i++) {
				var candidato = servidores[i];
				if (servidor.indexOf(candidato) != -1) {
					result = portal;
					break;
				}
			}
		}
	}
	
	return result;
};





/**
 * Elimina caracteres extra�os del titulo
 */
PlayerWebTV.getCleanTitle = function(title) {
	title = title.toLowerCase();
	title = title.replace(/[����]+/g, "a");
	title = title.replace(/[����]+/g, "e");
	title = title.replace(/[����]+/g, "i");
	title = title.replace(/[����]+/g, "o");
	title = title.replace(/[����]+/g, "u");
	title = title.replace(/[�]+/g, "c");
	title = title.replace(/�/g, "n");
	title = title.replace(/[^ \w]/g, "");
	return title;
};


/**
 * Extrae las palabras clave del titulo
 */
PlayerWebTV.getKeyWords = function(title) {
	var keywords = "";
	if (title) {
		title = title.toLowerCase();
		title = title.replace(/[����]+/g, "a");
		title = title.replace(/[����]+/g, "e");
		title = title.replace(/[����]+/g, "i");
		title = title.replace(/[����]+/g, "o");
		title = title.replace(/[����]+/g, "u");
		title = title.replace(/[�]+/g, "c");
		title = title.replace(/[�]+/g, "n");
		title = title.replace(/\b/g, "_");
		title = title.replace(/\W/g, "");
		var words = title.split("_");
		var keywords = "";
		for ( var i = 0; i < words.length; i++) {
			if (3 < words[i].length)
				keywords += "," + words[i];
		}
		if (keywords != "") {
			keywords = keywords.substring(1);
		}
	}
	return keywords;
}

/**
 * Elimina caracteres extra�os de una URL
 */
PlayerWebTV.cleanURL = function(url) {
	if (url) {
		return url.replace(/[^\w- ]/, "");
	} else {
		return url;
	}
}

/**
 * Genera un identificador unico (se usan 2 numeros aleatorios, ya que varias llamadas consecutivas pueden dar los
 * mismos nanosegundos.
 * @param prefijo Cadena que se contanera al principio del identificador
 * @return prefijo_numAleat_numAleat
 */
PlayerWebTV.getUID = function(prefijo) {
	var result = [new Date().getTime(), Math.ceil(Math.random() * 1000)];
	if (prefijo) {
		result.splice(0, 0, prefijo);
	}
	return result.join("_");
}

//************************************************************************
//******************  METODOS DE CLASE  **********************************
//************************************************************************
PlayerWebTV.prototype = {
	/**
	 * Inicializa el player cn la configuraci�n pasada como parametro
	 */
	init: function(opciones, video) {
		var rutaConfig = null;
		
		// Intentamos calcular el portal desde el que se carga la libreria
		var portal = this.portal;
		if (!this.portal) {
			portal = this.portal = this.getPortal(opciones);
		}
		
		var config = null;
		
		if (opciones) {
			config = opciones.config || portal;
		}
		
		if (!this.rutaConfig) {
			var extension = PlayerWebTV.getExtension(video);
			var rutaConfig;
			if (extension == 'mp3') {
				rutaConfig = "config-por-defecto-audio.xml";
				if (config) {
					rutaConfig = "config-" + config + "-audio.xml";
				}
			}
			else {
				rutaConfig = "config-por-defecto.xml";
				if (config) {
					rutaConfig = "config-" + config + ".xml";
				}
			}			
			
			this.rutaConfig = this.getURLBase("config/" + rutaConfig);
		}
		
		if (!this.mosca) {
			// Mosca
			this.mosca = {
				url: "",
				ancho: -1,
				alto: -1,
				img: null
			};
		}
	
		if (config) {
			if (!this.mosca.img) {
				this.mosca.img = new Image();
				var urlMosca = this.getURLBase("imagenes/" + config + "/mosca.png");
				this.mosca.img.src = this.mosca.url = urlMosca;
				var moscaImg = this.mosca.img;
				
				var _this = this;
				if (moscaImg.complete) {
					_this.cargarConfigMosca(moscaImg);
				} else {
					if (!moscaImg.onload) {
						moscaImg.onload = function() {
							_this.cargarConfigMosca(moscaImg);
						}
						moscaImg.onabort = moscaImg.onerror = function() {
							_this.initted = true;
						}
					}
				}
			}
		} else {
			this.initted = true;
		}
		
		return this.initted;
	},
	
	cargarConfigMosca : function(logo) {
		this.mosca.ancho = parseInt(logo.width, 10);
		this.mosca.alto = parseInt(logo.height, 10);
		
		this.initted = true;
	},
	
	calculaMosca: function(ancho, alto) {
		try {
			var config = this.mosca;
			var mosca = "mosca.png";
			var iAncho = parseInt(ancho, 10);
			var iAlto = parseInt(alto, 10);
			
			var altoMosca = config.alto;
			var anchoMosca = config.ancho;
			
			if ((iAncho * 0.5) < anchoMosca || (iAlto * 0.3) < altoMosca) {
				mosca = "mosca-small.png";
			}
			
			if (iAncho < this.MIN_ANCHO_PLAYER || iAlto < this.MIN_ALTO_PLAYER) {
				mosca = "";
			}
			
			if (mosca) {
				return config.url.substring(0, config.url.lastIndexOf("/") + 1) + mosca;
			} else {
				return "";
			}
		} catch (e) {
			return "";
		}
	},
	/*
	utmac	UA-6773806-3
	utmcc	__utma=65922302.2025355597012597800.1252482418.1253875116.1255016623.18;+__utmz=65922302.1253699542.12.2.utmcsr=212.81.129.25:8281|utmccn=(referral)|utmcmd=referral|utmcct=/index.asp;
	utmcs	UTF-8
	utmdt	ABC WEB TV
	utme	5(video*Video servido*abctv._gente.en-abctv.Indios y Vaqueros: Alondra Bentley)
	utmfl	10.0 r32
	utmhid	1273621659
	utmhn	www.abctv.es
	utmje	0
	utmn	343073796
	utmp	/
	utmr	-
	utmsc	32-bit
	utmsr	1280x800
	utmt	event
	utmul	es-es
	utmwv	4.3as
	 */
	setValoresGoogleAnalytics : function(fo, opciones) {
		var categorias = this.getCategoriasGoogleAnalytics(opciones);
		var result = [];
		result.push("channel::" + categorias[0]);
		result.push("subChannel::" + categorias[1]);
		fo.addVariable("googleAnalytics", result.join(","));
	},
		
	getCategoriasGoogleAnalytics : function(opciones) {
		var categoria = "";
		var subcategoria = "";
		
		try {
			if (this.esDominioConocido()) {
				var url = location.pathname;
				var partes = url.split("/");
				
				// eliminamos la 1� pq siempre llega vacia
				partes.splice(0, 1);
				
				if (partes[partes.length - 1] == "") {
					// comprobamos si la ultima partes es vacia
					partes.splice(partes.length - 1, 1);
				}
				
				if (partes.length > 0) {
					if (/^\d{8}/.test(partes[0])) {
						// la 1� componente es 1 fecha, luego es un detalle de noticia
						// la eliminamos
						partes.splice(0, 1);
					}
				}
				if (partes.length > 0) {
					if (partes[partes.length - 1].indexOf(".") != -1) {
						if (partes[partes.length - 1].indexOf(".asp") != -1) {
							// si acaba en asp es una portada de ABC, la preparamos para intentar sacar luego la subcategoria
							var paginaTemp = partes[partes.length - 1];
							partes[partes.length - 1] = paginaTemp.replace(/\.asp/gi, "");
						} else {
							// es una p�gina html, asp...
							// lo eliminamos
							partes.splice(partes.length - 1, 1);
						}
					}
				}
				
				if (partes.length == 0) {
					categoria = "portada";
				} else if (partes.length == 1) {
					if(this.cleanURL(partes[0])=="alava" || this.cleanURL(partes[0])=="almeria" || this.cleanURL(partes[0])=="jaen" || this.cleanURL(partes[0])=="albacete" || this.cleanURL(partes[0])=="alicante" || this.cleanURL(partes[0])=="jerez")
						categoria = "portada";
					else
						categoria = this.cleanURL(partes[0]);
				} else {
					// tenemos mas de 2 porciones de url, las cogemos como categoria y subcategoria
					categoria = this.cleanURL(partes[0]);
					if(/^\d{8}/.test(partes[1])){
						subcategoria = this.cleanURL(partes[2]);
					}
					else{
						subcategoria = this.cleanURL(partes[1]);
						if (this.cleanURL(partes[2]))
							subcategoria+= "/" +this.cleanURL(partes[2]);
					}
				}
			} else {
				var url = location.href;
				var partesURL = url.split("/");
				var pagina = partesURL[partesURL.length - 1];
				if (/\.(.)+$/.test(pagina)) {
					subcategoria = partesURL.splice(partesURL.length - 1, 1);
					categoria = partesURL.join("/");
				} else {
					categoria = url;
				}
			}
		} catch (e) {
		}
		
		return [categoria, subcategoria];
	},
	
	esDominioConocido: function() {
        var dominio = location.hostname;
        for (var portal in this.INFO_PORTALES) {
        	var servidores = this.INFO_PORTALES[portal].servidores;
        	for (var i = 0; i < servidores.length; i++) {
        		var candidato = servidores[i];
        		if (candidato == dominio) {
        			return true;
                }
           }
        }
         return false;
	},

	// zoneid:126,CAT_gente,SCATen-abctv,PORabctv,indios-vaqueros-alondra-bentley-54441
	/**
	 * Las zonas se cargaran de la configuracion, asi como el portal o site
	 */
	/*setValoresAdvernet: function(fo, opciones) {
		var result = [];
		
		var tags = null;
		if (opciones) {
			tags = opciones.tags || this.getKeyWords(opciones.titulo) || "";
		}
		if (tags) {
			tags = tags.replace(/[, ]/g, "-");
			result.push("tags::" + tags);
		}
		var categoria = undefined;
		var subcategoria = undefined;
		if (opciones) {
			categoria = opciones.categoriaPubli || undefined;
			subcategoria = opciones.subcategoriaPubli || undefined;
		}
		result.push("channel::" + categoria);
		result.push("subChannel::" + subcategoria);
		
		fo.addVariable("advernet", result.join(","));
	},*/
	
	_showVideo : function(id, video, w, h, opciones) {
		var funcion = this._showVideo;
		
		if (arguments.length < 5) {
			// Si no nos pasan un id de capa, creamos 1 en la posicion actual y la usamos para escribir el video
			// (CUIDADO!  Si la pagina ya esta cargada, siempre hay que indicar un id, sino borraria la pagina actual)
			var uid = this.getUID("video");
			document.write('<div id="' + uid + '"></div>');
			
			var args = new Array(5);
			args[0] = uid;
			for (var i = 0; i < arguments.length; i++) {
				args[i + 1] = arguments[i];
			}
			
			funcion.call(this, args[0], args[1], args[2], args[3], args[4]);
			return;
		}
		
		
		if (!opciones) {
			opciones = {};
		}
		
		// Esperamos que se cargue la imagen de mosca para poder calcular cual de las dos mostrar
		// si la peque�a o la normal o ninguna
		if (!this.init(opciones, video)) {
			var _this = this;
			setTimeout(function() {
				funcion.call(_this, id, video, w, h, opciones);
			}, 500);
			return;
		}
		
		// con esto conseguimos ids unicos
		var idPlayer = this.getUID("movie_player");
	
		var fo = new SWFObject(this.getURLBase("FLVPlayer.swf"), idPlayer, w, h, this.VERSION_FLASH, getColorFondo());
	
		// transparencia
		fo.addParam("wmode", "transparent");
		
		fo.addParam("swLiveConnect", "true");
		fo.addParam("allowScriptAccess", "always");
	
		// ruta del fichero de configuracion
		if (this.rutaConfig) {
			fo.addVariable("config", this.rutaConfig);
		}
		
		// URL del video
		if (video.indexOf('youtube.com') != -1) {
			var idYoutube = video.substring(video.indexOf('=')+1, video.length);
			fo.addVariable("file", idYoutube);
			fo.addVariable("cdn", "youtube");
		}
		else {
			fo.addVariable("file", video);
		}
		
		//comprueba si es live
		if ( (video.indexOf('rtmp://') != -1) || (video.indexOf('rtmpt://') != -1) ) {
			fo.addVariable("isLive", "true");
		}
		
		// alto y ancho
		fo.addVariable("width", parseInt(w, 10));
		fo.addVariable("height", parseInt(h, 10));
		
		// google analytics
		this.setValoresGoogleAnalytics(fo, opciones);
		
		// advernet
		//this.setValoresAdvernet(fo, opciones);
		
		//DART
		if (typeof (OAS_sitepage) != 'undefined'){
			fo.addVariable("advertisement","OAS_sitepage::" + getOAS_sitepage(OAS_sitepage));
		}else{
			fo.addVariable("advertisement","OAS_sitepage::");
		}
		
		if (opciones) {
			if (!opciones.titulo) {
				opciones.titulo = "Desconocido";
			}
			opciones.titulo = this.getCleanTitle(opciones.titulo);
			
			// pantalla completa
			if (opciones.fullscreen !== false) {
				fo.addParam("allowfullscreen", "true");
			}
			
			// imagen de previa
			if (opciones.imgPrevia) {
				var imagen = this.getURLResizer(opciones.imgPrevia, w, h);
				imagen = imagen.replace(/&/g,"%26");
				fo.addVariable("image", imagen);
			}			
			if (opciones.autoplay) {
				fo.addVariable("autostart", "true");
			}
			
			// mosca
			var mosca = this.calculaMosca(w, h);
			if (mosca) {
				fo.addVariable("logo", "image::" + mosca);
			}
			
			// ajustar
			if (opciones.ajustar === true) {
				fo.addVariable("overstretch", "overstretch");
			} else if (opciones.ajustar === false) {
				fo.addVariable("overstretch", "maintain");
			} else if (opciones.ajustar) {
				fo.addVariable("overstretch", opciones.ajustar);
			} else {
				fo.addVariable("overstretch", "maintain");
			}
			
			// titulo del video
			if (opciones.titulo) {
				fo.addVariable("title", opciones.titulo);
			}
		} else {
			// Si no nos llega configuracion
			fo.addParam("allowfullscreen", "true");
			fo.addVariable("autostart", "false");
			fo.addVariable("overstretch", "maintain");
		}
	
		if (navigator && navigator.appName && navigator.appName == "Microsoft Internet Explorer") {
			setTimeout(function() {
				var contenedor = document.getElementById(id);
				if (contenedor) {
					contenedor.innerHTML = '<div style="text-align: center; margin:0 auto"></div>';
					fo.write(contenedor.firstChild);
				}
				PlayerWebTV.ieCounter--;
			}, 500 * PlayerWebTV.ieCounter++);
		} else {
			var contenedor = document.getElementById(id);
			if (contenedor) {
				contenedor.innerHTML = '<div style="text-align: center; margin:0 auto"></div>';
				fo.write(contenedor.firstChild);
			}
		}
	},
	
	_showAudio : function(id, video, w, h, opciones) {
		var funcion = this._showAudio;
		
		if (arguments.length < 5) {
			// Si no nos pasan un id de capa, creamos 1 en la posicion actual y la usamos para escribir el video
			// (CUIDADO!  Si la pagina ya esta cargada, siempre hay que indicar un id, sino borraria la pagina actual)
			var uid = this.getUID("video");
			document.write('<div id="' + uid + '"></div>');
			
			var args = new Array(5);
			args[0] = uid;
			for (var i = 0; i < arguments.length; i++) {
				args[i + 1] = arguments[i];
			}
			
			funcion.call(this, args[0], args[1], args[2], args[3], args[4]);
			return;
		}
		
		
		if (!opciones) {
			opciones = {};
		}
		
		// Esperamos que se cargue la imagen de mosca para poder calcular cual de las dos mostrar
		// si la peque�a o la normal o ninguna
		if (!this.init(opciones, video)) {
			var _this = this;
			setTimeout(function() {
				funcion.call(_this, id, video, w, h, opciones);
			}, 500);
			return;
		}
		
		// con esto conseguimos ids unicos
		var idPlayer = this.getUID("movie_player");
	
		var fo = new SWFObject(this.getURLBase("FLVPlayer.swf"), idPlayer, w, h,this.VERSION_FLASH, getColorFondo());
	
		// transparencia
		fo.addParam("wmode", "transparent");
		
		fo.addParam("swLiveConnect", "true");
		fo.addParam("allowScriptAccess", "always");
	
		// ruta del fichero de configuracion
		if (this.rutaConfig) {
			fo.addVariable("config", this.rutaConfig);
		}
		
		// URL del audio
		fo.addVariable("file", video);
		
		// alto y ancho
		fo.addVariable("width", parseInt(w, 10));
		fo.addVariable("height", parseInt(h, 10));
		
		// google analytics
		//this.setValoresGoogleAnalytics(fo, opciones);
		
		// advernet
		//this.setValoresAdvernet(fo, opciones);
		
		if (opciones) {
			if (!opciones.titulo) {
				opciones.titulo = "Desconocido";
			}
			opciones.titulo = this.getCleanTitle(opciones.titulo);
			
			// pantalla completa
			if (opciones.fullscreen !== false) {
				fo.addParam("allowfullscreen", "true");
			}
			
			// imagen de previa
			if (opciones.imgPrevia) {
				//fo.addVariable("image", this.getURLResizer(opciones.imgPrevia, w, h));
				if (w <= 253) var ancho =  253;
				else if (w > 253 && w <= 300) var ancho =  300;
				else if (w > 300 && w <= 384) var ancho =  384;
				else if (w > 384 && w <= 390) var ancho =  390;
				else if (w > 390 && w <= 665) var ancho =  665;
				var imagen = this.getURLResizer(opciones.imgPrevia, w, h, 'audio-onda_' + ancho + '.png');
				imagen = imagen.replace(/&/g,"%26");
				fo.addVariable("image", imagen);
			}
			
			if (opciones.autoplay) {
				fo.addVariable("autostart", "true");
			}
			
			// mosca
			//var mosca = this.calculaMosca(w, h);
			//if (mosca) {
			//	fo.addVariable("logo", "image::" + mosca);
			//}
			
			// ajustar
			if (opciones.ajustar === true) {
				fo.addVariable("overstretch", "overstretch");
			} else if (opciones.ajustar === false) {
				fo.addVariable("overstretch", "maintain");
			} else if (opciones.ajustar) {
				fo.addVariable("overstretch", opciones.ajustar);
			} else {
				fo.addVariable("overstretch", "maintain");
			}
			
			// titulo del video
			if (opciones.titulo) {
				fo.addVariable("title", opciones.titulo);
			}
		} else {
			// Si no nos llega configuracion
			fo.addParam("allowfullscreen", "true");
			fo.addVariable("autostart", "false");
			fo.addVariable("overstretch", "maintain");
		}
	
		if (navigator && navigator.appName && navigator.appName == "Microsoft Internet Explorer") {
			setTimeout(function() {
				var contenedor = document.getElementById(id);
				if (contenedor) {
					contenedor.innerHTML = '<div style="text-align: center;"></div>';
					fo.write(contenedor.firstChild);
				}
				PlayerWebTV.ieCounter--;
			}, 500 * PlayerWebTV.ieCounter++);
		} else {
			var contenedor = document.getElementById(id);
			if (contenedor) {
				contenedor.innerHTML = '<div style="text-align: center;"></div>';
				fo.write(contenedor.firstChild);
			}
		}
	},	
	
	showVideo: function(id, video, w, h, opciones) {
		var extension = PlayerWebTV.getExtension(video);
		
		var funcion = this.showVideo;
		if (arguments.length < 5) {
			// Si no nos pasan un id de capa, creamos 1 en la posicion actual y la usamos para escribir el video
			// (CUIDADO!  Si la pagina ya esta cargada, siempre hay que indicar un id, sino borraria la pagina actual)
			var uid = this.getUID("video");
			document.write('<div id="' + uid + '"></div>');
			
			var args = new Array(5);
			args[0] = uid;
			for (var i = 0; i < arguments.length; i++) {
				args[i + 1] = arguments[i];
			}
			
			funcion.call(this, args[0], args[1], args[2], args[3], args[4]);
			return;
		}
		if (!opciones) {
			opciones = {};
		}
		
		var _this = this;
		if (!this.init(opciones, video)) {
			setTimeout(function() {
				funcion.call(_this, id, video, w, h, opciones);
			}, 500);
			return;
		}
		
		var contenedor = document.getElementById(id);
		
		if (contenedor) {
			if (opciones && opciones.imgPrevia) {
				if (extension == 'mp3') {
					var marcaAgua = 'audio-play.png';
				}
				else {
					var marcaAgua = 'play.png';
				}
				
				var urlPreviaRedimensionada = this.getURLResizer(opciones.imgPrevia, w, h, marcaAgua);
				this.pintarImagen(id, video, w, h, opciones, urlPreviaRedimensionada);
			} else {
				this.pintarImagen(id, video, w, h, opciones, null);
			}
		}
	},
	
	pintarImagen: function(id, video, w, h, opciones, urlImgPrevia, imgPrevia) {
		var extension = PlayerWebTV.getExtension(video);
		
		var _this = this;
		var funcion = this.pintarImagen;
		
		if (urlImgPrevia && !imgPrevia) {
			var imgPrevia = new Image();
			imgPrevia.src = urlImgPrevia;
			if (imgPrevia.complete) {
				funcion.call(_this, id, video, w, h, opciones, urlImgPrevia, imgPrevia);
			} else {
				imgPrevia.onload = function() {
					funcion.call(_this, id, video, w, h, opciones, urlImgPrevia, imgPrevia);
				};
				imgPrevia.onerror = imgPrevia.onabort = function() {
					funcion.call(_this, id, video, w, h, opciones, urlImgPrevia, new Image());
				};
			}
		} else if (!urlImgPrevia || (urlImgPrevia && imgPrevia)) {
			if (!urlImgPrevia) {
				imgPrevia = new Image();
			}
			
			var contenedor = document.getElementById(id);
			var alto = parseInt(h, 10);
			var ancho = parseInt(w, 10);
			var tamIcono = [64, 61];
			
			opciones.autoplay = true;
			
			var accion = function() {
				if (extension == 'mp3') {
					_this._showAudio(id, video, w, h, opciones);
					this.onclick = _this.nada;
				}
				else {
					_this._showVideo(id, video, w, h, opciones);
					this.onclick = _this.nada;
				}
			};
			
			var estrategia = "MarcaAguaEncajar";
			switch (estrategia) {
				case "SinMarcaAguaPosicionado":
					// Version con play posicionado (problemas con IE6)
					this.getHTMLImagenPosicion(ancho, alto, tamIcono, imgPrevia, contenedor, accion, extension);
				break;
				case "MarcaAguaAncho100AltoProporcional":
					// Version con ancho 100% y alto proporcional
					this.getHTMLImagenMarcaAguaAltoProporcional(ancho, alto, tamIcono, imgPrevia, contenedor, accion, extension);
				break;
				case "MarcaAguaEncajar":
				default:
					// Version con marca de agua e imagen centrada horizontal y verticalmente
					this.getHTMLImagenMarcaAgua(ancho, alto, tamIcono, imgPrevia, contenedor, accion, extension);
				break;
			}
		}
	},
	
	// Version con play posicionado (problemas con IE6)
	getHTMLImagenPosicion: function(ancho, alto, tamIcono, imgPrevia, contenedor, accion, extension) {
		if (extension == 'mp3') {
			var textoAlt = 'Pulse para escuchar el audio';
			var imagenPlay = 'imagenes/audio-play.png';
		}
		else {
			var textoAlt = 'Pulse para ver el video';
			var imagenPlay = 'imagenes/play.png';
		}
		
		var styleIcono = "margin-left: " + (ancho/2 - tamIcono[0]/2) + "px; top: " + (alto/2 - tamIcono[1]/2) + "px;";
		var html = '<div style="text-align: center; margin:0 auto"><div style="position: relative; margin:0 auto;cursor: pointer; background-color: ' + getColorFondo() + '; width: ' + ancho + 'px; height: ' + alto + 'px;">';
		html += '<img src="' + this.getURLBase(imagenPlay) + '" alt="' + textoAlt + '" title="' + textoAlt + '" style="margin:0 auto; text-align:center; border: 0; position: absolute; z-index: 101; ' + styleIcono + '" />';
		if (imgPrevia && (imgPrevia.width + imgPrevia.height) > 0) {
			var style = "";
			var proporcionPlayer = ancho / alto;
			var proporcionImagen = imgPrevia.width / imgPrevia.height;
			
			var altoFinal = imgPrevia.height;
			var anchoFinal = imgPrevia.width;
			
			if (proporcionPlayer > proporcionImagen) {
				anchoFinal = anchoFinal * alto / altoFinal;
				altoFinal = alto;
			} else {
				altoFinal = altoFinal * ancho / anchoFinal;
				anchoFinal = ancho;
			}
			// centramos la imagen
			var coords = [(ancho - anchoFinal) / 2, (alto - altoFinal) / 2];
			var style = "height: " + altoFinal + "px; width: " + anchoFinal + "px; position: absolute; z-index: 100; top: " + coords[1] + "px; left: " + coords[0] + "px;";
			
			html += '<img src="' + imgPrevia.src + '" alt="' + textoAlt + '" title="' + textoAlt + '" style="border: 0; margin:0 auto; text-align:center;' + style + '" />';
		} else {
			html += '<img src="' + this.getURLBase(imagenPlay) + '" alt="' + textoAlt + '" title="' + textoAlt + '" style="margin:0 auto; text-align:center; border: 0; margin-top: ' + ((alto - tamIcono[1]) / 2) + 'px;" />';
		}
		html += '</div></div>';
		
		contenedor.innerHTML = html;
		contenedor.firstChild.firstChild.onclick = accion;
	},
	
	// Version con marca de agua e imagen centrada horizontal y verticalmente
	getHTMLImagenMarcaAgua: function(ancho, alto, tamIcono, imgPrevia, contenedor, accion, extension) {
		if (extension == 'mp3') {
			var textoAlt = 'Pulse para escuchar el audio';
			var imagenPlay = 'imagenes/audio-play.png';
		}
		else {
			var textoAlt = 'Pulse para ver el video';
			var imagenPlay = 'imagenes/play.png';
		}
		
		var html = '<div style="text-align: center; margin:0 auto"><div style="margin:0 auto;cursor: pointer; background-color: ' + getColorFondo() + '; width: ' + ancho + 'px; height: ' + alto + 'px;">';
		if (imgPrevia && (imgPrevia.width + imgPrevia.height) > 0) {
			var style = "";
			var proporcionPlayer = ancho / alto;
			var proporcionImagen = imgPrevia.width / imgPrevia.height;
			
			var altoFinal = imgPrevia.height;
			var anchoFinal = imgPrevia.width;
			
			if (proporcionPlayer > proporcionImagen) {
				anchoFinal = anchoFinal * alto / altoFinal;
				altoFinal = alto;
			} else {
				altoFinal = altoFinal * ancho / anchoFinal;
				anchoFinal = ancho;
			}
			// centramos la imagen
			var coords = [(ancho - anchoFinal) / 2, (alto - altoFinal) / 2];
			var style = "height: " + altoFinal + "px; width: " + anchoFinal + "px; margin-top: " + coords[1] + "px;";
			
			html += '<img src="' + imgPrevia.src + '" alt="' + textoAlt + '" title="' + textoAlt + '" style="margin:0 auto; text-align:center; border: 0; ' + style + '" />';
		} else {
			html += '<img src="' + this.getURLBase(imagenPlay) + '" alt="' + textoAlt + '" title="' + textoAlt + '" style="margin:0 auto; text-align:center; border: 0; margin-top: ' + ((alto - tamIcono[1]) / 2) + 'px;" />';
		}
		html += '</div></div>';
		
		contenedor.innerHTML = html;
		contenedor.firstChild.firstChild.onclick = accion;
	},
	
	// Version con ancho 100% y alto proporcional
	getHTMLImagenMarcaAguaAltoProporcional: function(ancho, alto, tamIcono, imgPrevia, contenedor, accion, extension) {
		if (extension == 'mp3') {
			var textoAlt = 'Pulse para escuchar el audio';
			var imagenPlay = 'imagenes/audio-play.png';
		}
		else {
			var textoAlt = 'Pulse para ver el video';
			var imagenPlay = 'imagenes/play.png';
		}
		
		// Version con marca de agua e imagen ocupa todo el ancho y alto proporcional
		var html = '<div style="text-align: center; margin:0 auto"><div style="margin:0 auto; background-color: ' + getColorFondo() + '; cursor: pointer; width: ' + ancho + 'px;">';
		if (imgPrevia && (imgPrevia.width + imgPrevia.height) > 0) {
			html += '<img src="' + imgPrevia.src + '" alt="' + textoAlt + '" title="' + textoAlt + '" style="margin:0 auto; text-align:center; border: 0 " />';
		} else {
			html += '<img src="' + this.getURLBase(imagenPlay) + '" alt="' + textoAlt + '" title="' + textoAlt + '" style="margin:0 auto; text-align:center; border: 0; margin-top: ' + ((alto - tamIcono[1]) / 2) + 'px;" />';
		}
		html += '</div></div>';
		
		contenedor.innerHTML = html;
		contenedor.firstChild.firstChild.onclick = accion;
	},
	
	/**
	 * Obtiene la URL completa para un recurso, seg�n el portal especificado
	 */
	getURLBase: function(url) {
		if (this.portal && this.INFO_PORTALES[this.portal]) {
			if (this.INFO_PORTALES[this.portal].baseConfig) {
				// si el portal tiene un baseConfig especifico, lo uso
				return this.INFO_PORTALES[this.portal].dominio + this.INFO_PORTALES[this.portal].baseConfig + url;
			} else {
				return this.INFO_PORTALES[this.portal].dominio + this.BASE_CONFIG + url;
			}
		} else {
			return this.BASE_CONFIG + url;
		}
	},
	
	/**
	 * Combierte URL's relativas al servidor en URL's absolutas.  Utilizado para el resizer
	 */
	getFullURL: function(src) {
		if (src.indexOf("//") == -1 && !(src.charAt(0) == "/")) {
			// la ruta es relativa a la pagina
			// src = "nivel2/imagen.png"
			var ruta = location.href.substring(0, location.href.lastIndexOf("/") + 1);
			src = ruta + src;
		} else if (src.charAt(0) == "/") {
			// la ruta de la imagen es relativa al servidor, concatenamos el servidor actual
			// src = "/carpeta/imagen.png"
			src = location.protocol + "//" + location.host + src;
		}
		return src;
	},
	
	/**
	 * Devuelve la URL para pedir al resizer la nueva imagen
	 */
	getURLResizer: function(src, ancho, alto, marcaAgua) {
		var result = this.getFullURL(src);
		
		if (this.portal) {
			var medio = "";
			var medio = this.INFO_PORTALES[this.portal].resizer;
			
			if (!medio) {
				medio = this.portal + ".es";
			}
			
			if (!isDesarrolloABC()) {
				if (marcaAgua) {
					result = "http://foto-cache." + medio + "/resizer/resizer.php?imagen=" + result + "&nuevoancho=" + ancho + "&nuevoalto=" + alto + "&copyright=false&encrypt=false&marcaagua=marcasagua/" + marcaAgua + "&posmarca=C&transparencia=85";
				} else {
					result = "http://foto-cache." + medio + "/resizer/resizer.php?imagen=" + result + "&nuevoancho=" + ancho + "&nuevoalto=" + alto + "&copyright=conCopyright&encrypt=false";
				}
			}
		}
		
		return result;
	}
};

/**
 * Incluimos las propiedades estaticas en el prototipo
 */
(function() {
	var prototipo = PlayerWebTV.prototype;
	for (var prop in PlayerWebTV) {
		if (prop != "prototype") {
			prototipo[prop] = PlayerWebTV[prop];
		}
	}
})();
