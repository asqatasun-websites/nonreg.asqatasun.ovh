function MiaClass() { }

MiaClass.prototype.construct = function() {};

MiaClass.inherits = function(def)
{
	var classDef = function()
   {
		if (arguments[0] !== MiaClass) { this.construct.apply(this, arguments); }
	};
	var proto = new this(MiaClass);
	var superClass = this.prototype;
	var statics = new Array();
	for (var n in def)
   {
		var item = def[n];
		if ('function' === typeof item)
      { 
         item.$ = superClass;
      }
		proto[n] = item;
		if (n == 'statics')
			statics = item;
	}
	for (var i = 0; i < statics.length; ++i) 
   {
		classDef[statics[i]] = def[statics[i]];
	}
	classDef.prototype = proto;
	classDef.inherits = this.inherits;
	return classDef;
};

if (!Array.prototype.map)
{
  Array.prototype.map = function(fun /*, thisp*/)
  {
    var len = this.length >>> 0;
    if (typeof fun != "function")
      throw new TypeError();

    var res = new Array(len);
    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in this)
        res[i] = fun.call(thisp, this[i], i, this);
    }

    return res;
  };
}

if (!Array.prototype.filter)
{
  Array.prototype.filter = function(fun /*, thisp*/)
  {
    var len = this.length >>> 0;
    if (typeof fun != "function")
      throw new TypeError();

    var res = new Array();
    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in this)
      {
        var val = this[i]; // in case fun mutates this
        if (fun.call(thisp, val, i, this))
          res.push(val);
      }
    }

    return res;
  };
}

Array.prototype.notIn = function (array) {
	var result = new Array();
	this.forEach(function (i) {
		if (array.indexOf(i) == -1)
			result.push(i);
	});
	return result;
}

if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

if (!Array.prototype.forEach)
{
  Array.prototype.forEach = function(fun /*, thisp*/)
  {
    var len = this.length >>> 0;
    if (typeof fun != "function")
      throw new TypeError();

    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in this)
        fun.call(thisp, this[i], i, this);
    }
  };
}

if (!Array.prototype.shuffle) 
{
   Array.prototype.shuffle = function() 
   {
      for(var j, x, i = this.length; i; j = parseInt(Math.random() * i), x = this[--i], this[i] = this[j], this[j] = x);
      return this;
   }
};

if (undefined===window.MIA)
{
   var MIA = {};
}
MIA.Debug = 
{
   log : function(str, level)
   {
      //ne marche qu en dev
      var location = new String(window.location);
      if (location.indexOf('lemonde-dev') > 0)
         alert(str);
   }
};