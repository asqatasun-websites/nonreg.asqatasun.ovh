/**
 * This file includes JavaScript functions for the navigation elements.
 */
var /*HashMap*/ ms_corenav_elementsLoaded = new Object();
var /*HashMap*/ ms_corenav_elementsSelected = new Object();
var /*AJAXConnector*/ ajaxConnector = new AJAXConnector( );

function /*void*/ ms_corenav_handleRequest(status, data, param, type, statusmsg) {
	if(status == AJAXConnector.SUCCID_LOAD && type == AJAXConnector.RESPONSE_TEXT) {
		ms_corenav_printFlyout(param, data);
	}
}

function /*void*/ ms_corenav_loadFlyoutData(/*String*/ elementId, /*String*/ handle) {
	// Return, if no handle was given
	if (!handle || handle == "") {
		return;
	}
	
	// Process Ajax, if element has not been loaded yet
	if (ms_corenav_elementsLoaded[elementId] == undefined) {
		// Send Ajax request
		ajaxConnector.registerDataHandler(ms_corenav_handleRequest);
		ajaxConnector.setMaxRequestTime(10000);
		ajaxConnector.sendRequest(handle + ".ajax.component.corenavigation2.corenavigation2_Single.corenavigation2.html", "", AJAXConnector.REQUEST_GET, elementId);
	}
}

function /*void*/ ms_corenav_printFlyout(/*String*/ elementId, /*String*/ data) {
	// Print to Layer
	if (getLayer(elementId) != undefined) {
		// Replace elementId spacer
		data = data.replace(/{elementId}/g, elementId);
	
		// Paste response to layer
		getLayer(elementId).innerHTML = data;
		
		// Set counter for this elementId
		var /*int*/ counter = getLayer(elementId).getElementsByTagName("ul")[0].childNodes.length
		ms_corenav_elementsLoaded[elementId] = counter;

		// Init first list entry
		var /*List*/ links = getLayer(elementId).getElementsByTagName("li");
		if (links[0].firstChild) {
			ms_corenav_changeFlyout(links[0].firstChild, elementId, 0);
			ms_corenav_resetFlyout(links[0].firstChild, elementId, 0);
		}
	}
}

function ms_corenav_resetFlyout(/*XMLNode*/ callerObj, /*String*/ elementId, /*int*/ item) {
	// Get current marked node
	var /*XMLNode*/ current = ms_corenav_elementsSelected[elementId];
	
	// Reset the current class for IE
	if (document.all) {
		current.className = "ms-hover";
	}
}

function ms_corenav_changeFlyout(/*XMLNode*/ callerObj, /*String*/ elementId, /*int*/ item) {
	// Get current marked node
	var /*XMLNode*/ current = ms_corenav_elementsSelected[elementId];
	
	// IllegalArgumentException
	if (item < 0) {
		return;
	}

	// Check if current element has changed
	if (current == callerObj) {
		return;
	}

	// Reset previous marked element
	if (current != undefined) {
		if (current.className) {
			current.className = '';
		} else {
			current.removeAttribute("class");
		}
	}
	
	// Set current marked element
	if (callerObj.className) {
		callerObj.className = 'ms-hover';
	} else {
		callerObj.setAttribute("class", "ms-hover");
	}
	ms_corenav_elementsSelected[elementId] = callerObj;
	
	// Set all display:none
	for (var /*int*/ i = 0; i < ms_corenav_elementsLoaded[elementId]; i++) {
		getLayer(elementId + "-r" + i).style.display = "none";
		getLayer(elementId + "-b" + i).style.display = "none";
	}
	
	// Set specified number display:block
	getLayer(elementId + "-r" + item).style.display = "block";
	getLayer(elementId + "-b" + item).style.display = "block";
}

var /*Boolean*/ ms_setIFrameHeight_stopper = false;
function ms_setIFrameHeight(/*String*/flyoutId) {
	// Only IE6
	if (!window.attachEvent || window.opera || window.XMLHttpRequest) return;
	// Get IFrame and FlyOut element
	if (flyoutId == '') return;
	// Only CORE NAV Flyout Type 2
	
	var flyout = document.getElementById(flyoutId);
	var /*XMLNode*/ iframe = flyout.getElementsByTagName("IFRAME")[0];
	if (!iframe) return;
	var /*XMLNode*/ list = flyout.getElementsByTagName("UL")[0];
	if (!list) return;
	var /*NodeList*/ lists = flyout.getElementsByTagName("UL");
	if (!lists) return;
	var /*int*/ height = 0;
	for (var i = 0; i < lists.length; i++) {
		height += lists.item(i).offsetHeight;
	}
			
	flyout.style.display = "";
	if (!ms_setIFrameHeight_stopper) {
		// Sometimes IE needs some time to render the hidden
		// elements, so we give it 25 additional milliseconds.
		ms_setIFrameHeight_stopper = true;
		setTimeout("ms_setIFrameHeight('" + flyoutId + "')", 25);
		return;
	} else if (ms_setIFrameHeight_stopper) {
		ms_setIFrameHeight_stopper = false;
	}
	
	// Set height on IFrame. In some cases the height of the iframe must be
	// larger than that of the list because the whole flyout is larger.
	if(iframe.parentNode.className == 'ms-navi-main-fly-v2-1'){
		iframe.style.height = (height + 18) + "px";
	}
	else if(iframe.parentNode.className == 'ms-navi-main-fly-v1-1'){
		iframe.style.height = 390 + "px";
	}
	else if(iframe.parentNode.parentNode.className && iframe.parentNode.parentNode.className.indexOf('ms-navi-main-fly-v3') != -1){
		iframe.style.height = 395 + "px";
	}
	else{
		iframe.style.height = height + "px";
	}
}

//functions for the CRM part of the MetaNav
var /*boolean*/ ms_updateMetaNav = false;
function getCorrectMetaNav(salStructure){
	ms_updateMetaNav = true;
	
	var elem = document.getElementById("cs-salutation");
	if (elem) {
		var salutation = document.getElementById("cs-salutation").innerHTML;
		var greeting = buildGreetingString(salStructure);
		
		elem.innerHTML = salutation + greeting;
	}
	
	updateMetaNavigation();
}

function updateMetaNavigation() {
	if (!ms_updateMetaNav) {
		return;
	}
	
	var userProfile = embGetProfileManager();
	var isLoggedIn = userProfile.isLoggedIn();
	var isSoftLoggedIn = userProfile.isSoftLoggedIn();

	// Meta Navigation
	if(isLoggedIn){
		var elem;
		elem = document.getElementById("loggedoutul");
		if (elem) {
			elem.style.display='none';
		}
		elem = document.getElementById("loggedinul");
		if (elem) {
			elem.style.display='block';
		}
	}
	else {
		var elem;
		elem = document.getElementById("loggedinul");
		if (elem) {
			elem.style.display='none';
		}
		elem = document.getElementById("loggedoutul");
		if (elem) {
			elem.style.display='block';
		}
	}
}

//get the correct form of the CoreNav3
var /*boolean*/ ms_updateNavigation3 = false;
var /*boolean*/ ms_correctNav3ShowDF = false;
var /*boolean*/ ms_correctNav3ShowDMBP = false;
function getCorrectNav3(showdf, showdmbp){
	ms_updateNavigation3 = true;
	ms_correctNav3ShowDF = showdf;
	ms_correctNav3ShowDMBP = showdmbp;
	updateNavigation3();
}

// update navigation 3
function updateNavigation3() {
	if (!ms_updateNavigation3) {
		return;
	}
	
	var userProfile = embGetProfileManager();
	var isLoggedIn = userProfile.isLoggedIn();
	var isRegistered = !userProfile.isUnregistered();
	var hasFavoriteSet = userProfile.getFavoriteBmCode() && userProfile.getFavoriteBmCode().length > 0;
	var hasDealerSet = userProfile.getDealerName1() && userProfile.getDealerName1().length > 0;
	
	// Core Navigation 3
	if(isRegistered){
		var dealer = userProfile.getDealerName1() + "<br>";
		dealer = dealer + userProfile.getDealerName2() + "<br>";
		dealer = dealer + userProfile.getDealerStreet() + "<br>";
		dealer = dealer + userProfile.getDealerZIP() + " ";
		dealer = dealer + userProfile.getDealerCity();
		var dynPicture = userProfile.getFavoriteImgSmallUrl();
		var dynFavorite = userProfile.getFavoriteName();
		document.getElementById("showifnotloggedin").style.display='none';
		document.getElementById("showifnotloggedin").parentNode.style.display='none';
		if(ms_correctNav3ShowDF && hasFavoriteSet){
			var doomedElem = document.getElementById("crm-favorite-static");
			if (doomedElem) {
				doomedElem.parentNode.removeChild(doomedElem);
			}
			if (document.getElementById("loggedinpic").firstChild) {
				document.getElementById("loggedinpic").firstChild.removeAttribute("src");
				document.getElementById("loggedinpic").firstChild.setAttribute("src", dynPicture);
			} else {
				document.getElementById("loggedinpic").innerHTML = "<img src='" + dynPicture + "' />"
			}
			document.getElementById("loggedinfav").innerHTML = dynFavorite;
		} else {
			var doomedElem = document.getElementById("crm-favorite-dynamic");
			if (doomedElem) {
				doomedElem.parentNode.removeChild(doomedElem);
			}
		}
		if(ms_correctNav3ShowDMBP && hasDealerSet){
			var doomedElem = document.getElementById("crm-mbpartner-static");
			if (doomedElem) {
				doomedElem.parentNode.removeChild(doomedElem);
			}
			document.getElementById("loggedindealer").innerHTML = dealer;
		} else {
			var doomedElem = document.getElementById("crm-mbpartner-dynamic");
			if (doomedElem) {
				doomedElem.parentNode.removeChild(doomedElem);
			}
		}
		document.getElementById("showifloggedin").style.display='block';
	}
	else{
		document.getElementById("showifloggedin").style.display='none';
		document.getElementById("showifloggedin").parentNode.style.display='none';
		document.getElementById("showifnotloggedin").style.display='block';
	}
}

function buildGreetingString(greetingPattern) {
	
	var userProfile = embGetProfileManager();
	var greeting = greetingPattern.toString();
	
	greeting = greeting.replace(/%a/, userProfile.getSalutation());
	
	if (greeting.indexOf("%t") != -1) {
		var title = userProfile.getTitle();
		if (title && title.length > 0) {
			greeting = greeting.replace(/%t/, title);
		} else {
			greeting = greeting.replace(/%t/, "");
		}
	}
	
	greeting = greeting.replace(/%f/, userProfile.getFirstName());
	
	if (greeting.indexOf("%m") != -1) {
		var secondFirstName = userProfile.getSecondFirstName();
		if (secondFirstName && secondFirstName.length > 0) {
			greeting = greeting.replace(/%m/, secondFirstName);
		} else {
			greeting = greeting.replace(/%m/, "");
		}
	}
	
	greeting = greeting.replace(/%l/, userProfile.getLastName());
	
	if (greeting.indexOf("%s") != -1) {
		var secondLastName = userProfile.getSecondLastName();
		if (secondLastName && secondLastName.length > 0) {
			greeting = greeting.replace(/%s/, secondLastName);
		} else {
			greeting = greeting.replace(/%s/, "");
		}
	}
	
	return greeting;
}
function setHighlightNavi(elemID,elemLabel) {
	if (location.href.indexOf(elemLabel) != -1) {			
		//document.getElementById(elemID).setAttribute("class", "ms-active");
		var urlpart = window.location.href.substr(window.location.href.indexOf("/content/"),window.location.href.length);
		//var jsPattern =/content\/.*?\/.*?\/.*?\/.*?\/.*?\/.*?\/.*?\/.*?\/(.*?)/gi;
		//var test=jsPattern.exec(urlpart);		
		var urlArray=urlpart.split("/");
		var index=9;// index of Navigation  		
			if(elemLabel == urlArray[index]){
				var obj = document.getElementById(elemID);
				obj.className = 'ms-active';
				//document.getElementById(elemID).setAttribute('class', 'ms-active');
				}
		}
	}
