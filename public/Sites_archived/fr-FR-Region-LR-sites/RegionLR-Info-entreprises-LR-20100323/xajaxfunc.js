//
// Created on: <20-Jul-2004 10:54:01 fh>
//
// SOFTWARE NAME: eZ publish
// SOFTWARE RELEASE: 3.10.0
// BUILD VERSION: 20254
// COPYRIGHT NOTICE: Copyright (C) 1999-2007 eZ systems AS
// SOFTWARE LICENSE: GNU General Public License v2.0
// NOTICE: >
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of version 2.0  of the GNU General
//   Public License as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of version 2.0 of the GNU General
//   Public License along with this program; if not, write to the Free
//   Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//   MA 02110-1301, USA.
//
//
            
/*! \file xajaxfunc.js
*/


function addOption(selectId, val, txt) {
	var objOption = new Option(txt, val);
	document.getElementById(selectId).options.add(objOption);
}

function updateMultiselectionHidden(ldValue) {
	var multiSelect = document.getElementById(ldValue);
	var hiddenField = document.getElementById(ldValue + "_hidden");
	
	var optionsSelectiones = "";
	for(i = 0; i < multiSelect.options.length; i++){
		if(multiSelect.options[i].selected) {
			optionsSelectiones = optionsSelectiones + ";" + multiSelect.options[i].value;
		}
	}

	hiddenField.value = optionsSelectiones.substr(1, optionsSelectiones.length - 1);
}
