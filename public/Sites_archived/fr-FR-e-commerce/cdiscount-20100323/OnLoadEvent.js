var onLoadFunction = new Array();

/* Function that have to be run on window.load Event */
function OnLoadFunctions(fct) {
    onLoadFunction[onLoadFunction.length] = fct;
}

/* WindowsOnLoad Function */
function WindowOnLoad() {
    var i;
    for(i=0; i<onLoadFunction.length; i++) 
    {
        onLoadFunction[i]();
    }
}

/* Define window.onload Event */
window.onload= WindowOnLoad;

