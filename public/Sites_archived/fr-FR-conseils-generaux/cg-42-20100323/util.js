
// ----------------------------------------------------
//  UTILITY FUNCTION
// ----------------------------------------------------

var Popup = {
  popupWindow: function(url, title, w, h, status, resizable, scrollbars, reuse, winOpener){
  
	  if (!status) status="no";
	  if (!w) w=320;
	  if (!h) h=260;
	 
	  if (resizable == undefined) { resizable = "resizable=yes"; }
	  else { resizable = "resizable=" + (resizable ? "yes" : "no"); }
	 
	  if (scrollbars == undefined) { scrollbars = "scrollbars=yes"; }
	  else { scrollbars = "scrollbars=" + (scrollbars ? "yes" : "no"); }
	 
	  if (reuse == undefined) { 
	    reuse = true; 
	  }
	 
	  if (!navigator.jalios) {
	    navigator.jalios = new Object();
	  }
	 
	  if (winOpener == undefined) {
	    winOpener = window;
	  }
	 
	  if (reuse && navigator.jalios.popupWindow && navigator.jalios.popupWindow.close) {
	    navigator.jalios.popupWindow.close();
	  }
	  
	  if (!winOpener.opener) {
	    title = '_blank';
	  }
	  
	  try {
	    navigator.jalios.popupWindow = winOpener.open(url, title, 'status=' + status + ',width=' + w+ ',height=' + h + ',menubar=no,'+ resizable + ',' + scrollbars);
	    if (!navigator.jalios.popupWindow){
	      alert(I18N.glp('warn.popup.blocker'));
	    }
	  }
	  catch(ex){
	     alert(I18N.glp('warn.popup.blocker'));
	  }
	  
	  // Set the focus if opener have the focus
	  if (winOpener.focus && navigator.jalios.popupWindow){
	    navigator.jalios.popupWindow.focus();
	  }
  }
}


// ----------------------------------------------------
//  CONVENIENT
// ----------------------------------------------------

document.getElementsBySelector = function(selector) {
  return $$(selector);
}

