/* ------------------------------------------------------------------------------ BLOC DEFILANT */

function moveLayer(bloc,Sens){
	if(document.getElementById){
		Objet = document.getElementById(bloc);
	}
	else{
		Objet = document.all[bloc];
	}
	if(parseInt(Objet.offsetTop) + (3*Sens) > 0){
		Objet.style.top = "0px";
	}
	else{
		Objet.style.top = (parseInt(Objet.offsetTop) + (3*Sens)) + "px";
	}
	Timer = window.setTimeout("moveLayer('"+bloc+"',"+Sens+");", 50);
}
/*------------------------------------------------------------------------------------------ défilement bloc gauche */
var stepref = -0.25;		// Nbre de pixels de déplacement;
var step = stepref;
var delay = 15;
var pos;
var tid;

function getImagePosition (img) {

  if (document.layers)
  {
    return {x: img.x, y: img.y };
  }
  else {
    var coords = {x: 0, y: 0 };
    while (img) {
      coords.x += img.offsetLeft;
      coords.y += img.offsetTop;
      img = img.offsetParent;
    }
    return coords;
  }
}

function scrollContent () {
  pos += step;
  var contentHeight;
  if (document.layers) {
  	window.document.scrollBox.document.scrollContent.visibility = 'show';
    window.document.scrollBox.document.scrollContent.top = pos;
    contentHeight = window.document.scrollBox.document.scrollContent.document.height;
    boxHeight = window.document.scrollBox.clip.height;
  }
  else if (document.all) {
  	document.all.scrollContent.style.visibility = 'visible';
    document.all.scrollContent.style.top = pos + 'px';
    contentHeight = document.all.scrollContent.offsetHeight;
    boxHeight = document.all.scrollBox.offsetHeight;
  }
  else if (document.getElementById) {;
  	document.getElementById('scrollContent').style.visibility = 'visible';
    document.getElementById('scrollContent').style.top = pos + 'px';
    contentHeight = document.getElementById('scrollContent').offsetHeight;
    boxHeight = document.getElementById('scrollBox').offsetHeight;
  }
  if (pos < -440) 
    pos = 100;
}

function init_ticker () {
  var imgCoords = getImagePosition(document.placeHolder);
  if (document.layers) {
    document.scrollBox.left = imgCoords.x;
    document.scrollBox.top = imgCoords.y;
    document.scrollBox.clip.width = 145;
    document.scrollBox.clip.height = 100;
    document.scrollBox.visibility = 'show';
  }
  else if (document.all) {
    //document.all.scrollBox.style.left = imgCoords.x + 'px';
   // document.all.scrollBox.style.top = imgCoords.y + 'px';
    document.all.scrollBox.style.visibility = 'visible';
  }
  else if (document.getElementById) {
    //document.getElementById('scrollBox').style.left = imgCoords.x + 'px';
   // document.getElementById('scrollBox').style.top = imgCoords.y + 'px';
    document.getElementById('scrollBox').style.visibility = 'visible';
  }
  pos = 82;
  tid = setInterval('scrollContent()', delay);
}

$(document).ready(function(){
	init_ticker();
});


