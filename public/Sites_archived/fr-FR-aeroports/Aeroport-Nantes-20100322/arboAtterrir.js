var IE = document.all;
var NN = !document.all&&document.getElementById;

var timerLevel = null;
var jsTimerLevel = "";

// affich� les sous-menus
function showLevel(idRacineArbo, objId, forceDisplay, idItemActif, displayChild)
{
	keepLevel();
	
	//forceDisplay = true;

//    if (idItemActif != "")
//	{
//		document.getElementById(idItemActif).firstChild.className = " hover";
//	}
		
	if(document.getElementById)
	{		
		if (objId != "")
		{		
			var el = document.getElementById(objId);
			
			if (el != null) 
			{
			    var elParent = IE ? document.getElementById(objId).parentElement : document.getElementById(objId).parentNode;

			    if(elParent.tagName != "UL")
			    {
				    elParent = IE ? elParent.parentElement : elParent.parentNode;
			    }

			    if(forceDisplay)
			    {
				    if(elParent.id != idRacineArbo)
				    {
					    showLevel(idRacineArbo, elParent.id, true, "", false);
				    }
			    }
    			
			    var ar = elParent.getElementsByTagName("li");
    			
                // masquer toutes les arbo de niveau 2
			    for(var i=0; i<ar.length; i++)
			    {
			        //alert(i + " " + ar[i].firstChild + " / " + ar[i].firstChild.id + " / " + ar[i].firstChild.tagName);
			        if (firstNonTextChild(ar[i])!=null )
			        {
			            while ( getClass(firstNonTextChild(ar[i])).indexOf("hover",0)>-1 )
			            {
			                setClass(firstNonTextChild(ar[i]), getClass(firstNonTextChild(ar[i])).replace("hover",""));
			            }
        			    
			            if (lastNonTextChild(ar[i])!= null && lastNonTextChild(ar[i]).tagName == "UL" )
				            lastNonTextChild(ar[i]).style.display = "none";
				    }
			    }
    		
			    el.style.display = "block";
    			
			    if(el.parentNode.tagName == "LI" && getClass(firstNonTextChild(el.parentNode)).indexOf("hover",0) < 0 )
			    {
                    setClass(firstNonTextChild(el.parentNode), getClass( firstNonTextChild(el.parentNode)) + " hover");
                }
            }
		}

//		if (displayChild)
//		{
//			
//			if (idItemActif != "")
//			{

//				var el = document.getElementById(idItemActif);

//				for (var i=0; i<el.children.length; i++)
//				{
//					el.children[i].style.display = "block";
//				}
//				
//				if(elChild.tagName != "li")
//				{
//					elChild = IE ? elChild.childElement : elChild.childNode;
//				}
//				
//				var ar = elChild.getElementsByTagName("li");
//			
//				if(el.style.display != "block")
//				{	
//					for (var i=0; i<ar.length; i++)
//					{
//						ar[i].style.display = "none";
//					}
//				
//					el.style.display = "block";
//				}
//				else
//				{
//					el.style.display = "none";
//				}
//			}
//			
//		}
	}
}

function showLevel3(elem)
{
    if  ( elem != null )
    {
        if ( elem.parentNode.parentNode.parentNode.tagName == "LI" )
        {
            elem = firstNonTextChild(elem.parentNode.parentNode.parentNode);
            while ( elem!=null && elem.tagName != "SPAN" )
            {
                elem = elem.nextSibling;
            }
        }
        else
        {
            if ( elem.parentNode.tagName == "LI" )
            {
                elem = firstNonTextChild(elem.parentNode);
                while ( elem!=null && elem.tagName != "SPAN" )
                {
                    elem = elem.nextSibling;
                }
            }
        }
        
        if ( elem != null )
        {
            var next = elem.nextSibling;
            
            while ( next != null && next.tagName != "UL" )
            {
                next = next.nextSibling;
            }
              
            if (next != null )
            {    
                //fermer toutes les arbo de niveau 3 avant d'ouvrir une autre
                var listeUL = next.parentNode.parentNode.getElementsByTagName("UL");
                for(var c=0 ; c<listeUL.length ; c++)
                {
                    if (listeUL[c].style.display !="none")
                    {
                        listeUL[c].style.display ="none"
                        if (listeUL[c].previousSibling.tagName == "SPAN" )
                        {
                            listeUL[c].previousSibling.innerHTML = "[+]";
                            listeUL[c].previousSibling.parentNode.className = "arbo_page_contenuCache";
                        }
                    }
                }
                        
                if (next.style.display !="block" )
                {
                    next.style.display ="block"
                    elem.innerHTML = "[-]";
                    
                    elem.parentNode.className = "arbo_page_contenuVisible";
                }
                else
                {
                    next.style.display ="none"
                    elem.innerHTML = "[+]";
                    
                    elem.parentNode.className = "arbo_page_contenuCache";
                }
            }
        }
    }
}

// demarre le compteur qui cache les sous-menus
function startHideLevel(objId)
{	
	if (objId != '')
		jsTimerLevel = "executeHideLevel('" + objId + "'); "; 
	
	if (jsTimerLevel != "")
		timerLevel = setTimeout(jsTimerLevel, 500);

}

// cache les sous-menus
function executeHideLevel(objId)
{
	if(document.getElementById)
	{	
		if (objId != "")
		{
			var el = document.getElementById(objId);
			if ( el != null )
			{
			    var elParent = null ;
    			elParent = IE ? el.parentElement : el.parentNode;
			    
			    if ( elParent != null )
			    {
			        if ( elParent.tagName != "LI" )
			        {
			            // element de premier niveau sans enfants ( Accueil )
			            while ( getClass(firstNonTextChild(el)).indexOf("hover",0)>-1 )
			            {
			                setClass(firstNonTextChild(el), getClass(firstNonTextChild(el)).replace("hover",""));
			                //alert(getClass(firstNonTextChild(el)));
			            }
			        }
			        else
			        {
//			            if(elParent.tagName != "UL")
//			            {
//				            elParent = IE ? elParent.parentElement : elParent.parentNode;
//			            }			
//			            var ar = elParent.getElementsByTagName("ul");
			            
//			            // masquer toutes les arbo de niveau 2
//			            for (var i=0; i<ar.length; i++)
//			            {
//			                ar[i].parentNode.firstChild.className = ar[i].parentNode.firstChild.className.replace(" hover","");
//				            ar[i].style.display = "none";
//			            }

			            el.style.display = 'none';
			            
			            while ( getClass(firstNonTextChild(elParent)).indexOf("hover",0)>-1 )
			            {
			                setClass(firstNonTextChild(elParent), getClass(firstNonTextChild(elParent)).replace("hover",""));
			            }
			        }
		        }
			}
		}
	}
}

// cache les sous-menus s'ils ont ete ouverts
function hideLevel()
{		

	if (jsTimerLevel != "")
		eval(jsTimerLevel);

}

// interrompt le compteur qui cache les sous-menus
function keepLevel()
{
	if (timerLevel != null)
	{
		clearTimeout(timerLevel);
		timerLevel = null;
	}
}

function menuGeneralOn(numero, couleur)
{
//	document.getElementById("entite_ANA_ARRIVE_COMM").style.backgroundImage="url(../../Projet-ressources/images/fond_menu_" + couleur + ".jpg)";
//	document.getElementById("arboItem_" + numero).className = couleur + "_on";
//	document.getElementById("arboItemDroite_" + numero).className = "arbo_ANAV2_DEP_COMM_DROITE_" + couleur + "_on";
//	if (document.getElementById("arboItemSeparateur_" + numero) != null)
//	{
//		document.getElementById("arboItemSeparateur_" + numero).className = "arbo_MENU_GENERAL_SEPARATEUR_" + couleur + "_on";
//	}

    keepLevel();
    
    // Si niveau 1
    
    var el = document.getElementById("arboHautItem_" + numero);
    if ( el != null )
    {
        var elParent = el.parentNode;
    	if ( elParent!=null )	
    	{
    	    if ( elParent.parentNode.tagName != "LI" )
    	    {
				// passage d'un niveau 1 � un niveau 1 sans niveau 2
				var ar = elParent.getElementsByTagName("li");
				//alert(ar);
				// masquer toutes les arbo de niveau 1
				for(var i=0; i<ar.length; i++)
				{
					if (firstNonTextChild(ar[i])!=null )
					{
						while ( getClass(firstNonTextChild(ar[i])).indexOf("hover",0)>-1 )
						{
							setClass(firstNonTextChild(ar[i]), getClass(firstNonTextChild(ar[i])).replace("hover",""));
						}
					}
				}
                // Reprise de Showlevel
                // masquer toutes les arbo de niveau 2
                var ar = elParent.getElementsByTagName("ul");
	            for (var i=0; i<ar.length; i++)
	            {
	                while ( getClass(firstNonTextChild(ar[i].parentNode)).indexOf("hover",0)>-1 )
			        {
	                    setClass(firstNonTextChild(ar[i].parentNode), getClass(firstNonTextChild(ar[i].parentNode)).replace("hover",""));
	                }
		            ar[i].style.display = "none";
	            }

                if ( getClass(firstNonTextChild(el)).indexOf("hover",0) < 0 )
                {
                    setClass(firstNonTextChild(el), getClass(firstNonTextChild(el)) + " hover");
                }
            }
        }
    }

}
function menuGeneralOff(numero, couleur, rubrique)
{
//	document.getElementById("entite_ANA_ARRIVE_COMM").style.backgroundImage="url(../../Projet-ressources/images/fond_menu_" + rubrique + ".jpg)";
//	document.getElementById("arboItem_" + numero).className = couleur + "_out";
//	document.getElementById("arboItemDroite_" + numero).className = "arbo_ANAV2_DEP_COMM_DROITE_" + couleur + "_out";
//	if (document.getElementById("arboItemSeparateur_" + numero) != null)
//	{
//		document.getElementById("arboItemSeparateur_" + numero).className = "arbo_MENU_GENERAL_SEPARATEUR_" + couleur + "_out";
//	}

    var el = document.getElementById("arboHautItem_" + numero);
    //executeHideLevel(element.parentNode);
    
    if (el && el.parentNode && el.parentNode.parentNode)
    {
        if ( el.parentNode.parentNode.tagName != "DIV" )
        {
            startHideLevel(el.parentNode.id);
        }
        else
        {
            startHideLevel(el.id);
        }
    }
}

/*
function menuANA_ARRIVE_INSTOn(numero, premier, dernier)
{
	document.getElementById("arboItem_" + numero).className = "arbo_ANA_ARRIVE_INST_ITEM_on";
	document.getElementById("arboPuce_" + numero).className = "arbo_ANA_ARRIVE_INST_PUCE_on";
	if(!premier)
	{
		document.getElementById("arboGauche_" + numero).className = "arbo_ANA_ARRIVE_INST_GAUCHE_on";
	}
	else
	{
		document.getElementById("ANA_ARRIVE_INST_ar_gauche").className = "ANA_ARRIVE_INST_ar_gauche_on";
	}
	if(!dernier)
	{
		document.getElementById("arboDroite_" + numero).className = "arbo_ANA_ARRIVE_INST_DROITE_on";
	}
	else
	{
		document.getElementById("ANA_ARRIVE_INST_ar_droit").className = "ANA_ARRIVE_INST_ar_droit_on";
	}
}

function menuANA_ARRIVE_INSTOff(numero, premier, dernier)
{
	document.getElementById("arboItem_" + numero).className = "arbo_ANA_ARRIVE_INST_ITEM_out";
	document.getElementById("arboPuce_" + numero).className = "arbo_ANA_ARRIVE_INST_PUCE_out";
	if(!premier)
	{
		document.getElementById("arboGauche_" + numero).className = "arbo_ANA_ARRIVE_INST_GAUCHE_out";
	}
	else
	{
		document.getElementById("ANA_ARRIVE_INST_ar_gauche").className = "ANA_ARRIVE_INST_ar_gauche_out";
	}
	if(!dernier)
	{
		document.getElementById("arboDroite_" + numero).className = "arbo_ANA_ARRIVE_INST_DROITE_out";
	}
	else
	{
		document.getElementById("ANA_ARRIVE_INST_ar_droit").className = "ANA_ARRIVE_INST_ar_droit_out";
	}
}
function menuANA_ARRIVE_CIBLOn(numero, premier, dernier)
{
	document.getElementById("arboItem_" + numero).className = "arbo_ANA_ARRIVE_CIBL_ITEM_on";
	document.getElementById("arboPuce_" + numero).className = "arbo_ANA_ARRIVE_CIBL_PUCE_on";
	if(!premier)
	{
		document.getElementById("arboGauche_" + numero).className = "arbo_ANA_ARRIVE_CIBL_GAUCHE_on";
	}
	else
	{
		document.getElementById("ANA_ARRIVE_CIBL_ar_gauche").className = "ANA_ARRIVE_CIBL_ar_gauche_on";
	}
	if(!dernier)
	{
		document.getElementById("arboDroite_" + numero).className = "arbo_ANA_ARRIVE_CIBL_DROITE_on";
	}
	else
	{
		document.getElementById("ANA_ARRIVE_CIBL_ar_droit").className = "ANA_ARRIVE_CIBL_ar_droit_on";
	}
}

function menuANA_ARRIVE_CIBLOff(numero, premier, dernier)
{
	document.getElementById("arboItem_" + numero).className = "arbo_ANA_ARRIVE_CIBL_ITEM_out";
	document.getElementById("arboPuce_" + numero).className = "arbo_ANA_ARRIVE_CIBL_PUCE_out";
	if(!premier)
	{
		document.getElementById("arboGauche_" + numero).className = "arbo_ANA_ARRIVE_CIBL_GAUCHE_out";
	}
	else
	{
		document.getElementById("ANA_ARRIVE_CIBL_ar_gauche").className = "ANA_ARRIVE_CIBL_ar_gauche_out";
	}
	if(!dernier)
	{
		document.getElementById("arboDroite_" + numero).className = "arbo_ANA_ARRIVE_CIBL_DROITE_out";
	}
	else
	{
		document.getElementById("ANA_ARRIVE_CIBL_ar_droit").className = "ANA_ARRIVE_CIBL_ar_droit_out";
	}
}
function menuANA_ARRIVE_TRANOn(numero, premier, dernier)
{
	document.getElementById("arboItem_" + numero).className = "arbo_ANA_ARRIVE_TRAN_ITEM_on";
	document.getElementById("arboPuce_" + numero).className = "arbo_ANA_ARRIVE_TRAN_PUCE_on";
	if(!premier)
	{
		document.getElementById("arboGauche_" + numero).className = "arbo_ANA_ARRIVE_TRAN_GAUCHE_on";
	}
	else
	{
		document.getElementById("ANA_ARRIVE_TRAN_ar_gauche").className = "ANA_ARRIVE_TRAN_ar_gauche_on";
	}
	if(!dernier)
	{
		document.getElementById("arboDroite_" + numero).className = "arbo_ANA_ARRIVE_TRAN_DROITE_on";
	}
	else
	{
		document.getElementById("ANA_ARRIVE_TRAN_ar_droit").className = "ANA_ARRIVE_TRAN_ar_droit_on";
	}
}

function menuANA_ARRIVE_TRANOff(numero, premier, dernier)
{
	document.getElementById("arboItem_" + numero).className = "arbo_ANA_ARRIVE_TRAN_ITEM_out";
	document.getElementById("arboPuce_" + numero).className = "arbo_ANA_ARRIVE_TRAN_PUCE_out";
	if(!premier)
	{
		document.getElementById("arboGauche_" + numero).className = "arbo_ANA_ARRIVE_TRAN_GAUCHE_out";
	}
	else
	{
		document.getElementById("ANA_ARRIVE_TRAN_ar_gauche").className = "ANA_ARRIVE_TRAN_ar_gauche_out";
	}
	if(!dernier)
	{
		document.getElementById("arboDroite_" + numero).className = "arbo_ANA_ARRIVE_TRAN_DROITE_out";
	}
	else
	{
		document.getElementById("ANA_ARRIVE_TRAN_ar_droit").className = "ANA_ARRIVE_TRAN_ar_droit_out";
	}
}
*/

function getClass(object)
{
    var className = "";
    
    if ( object==null )
    {
        //alert("erreur object null");
    }
    else
    {
        //alert(object.id);
        try
        {
            //IE
            var className= object.className
        }
        catch(ex)
        {
            //FF 
            className = object.getAttribute('class');
            
            if ( className==null )
            {
                className = "";
            }
        }
    }
    return className
}

function setClass(object,className)
{
    if ( object==null )
    {
        //alert("erreur object null");
    }
    else
    {
        //alert(object.id);
        try
        {
            //IE
            object.className = className;
        }
        catch(ex)
        {
            //FF
            object.setAttribute('class',className);
        }
    }
}

var ie = /MSIE/.test(navigator.userAgent);
var moz = !ie && navigator.product == "Gecko";

if (moz) 
{
//     delete HTMLElement.prototype.children;
//     HTMLElement.prototype.__defineGetter__("children", function() {
//           var arr = new Array(), i = 0, l = this.childNodes.length;
//           for ( i = 0; i < l; i++ ) {
//               if ( this.childNodes[ i ].nodeType == 1 ) {
//                    arr.push( this.childNodes[ i ] );
//               }
//           }
//      return arr;
//      });

      delete HTMLElement.prototype.firstChild;
      HTMLElement.prototype.__defineGetter__("firstChild", function() {
            var node = this.childNodes[ 0 ];
            while (node.nodeType == 3) node = node.nextSibling;
                 return node;
            });
       
     delete HTMLElement.prototype.lastChild;  
     HTMLElement.prototype.__defineGetter__("lastChild", function() {
         var node = this.childNodes[ this.childNodes.length - 1 ];
         while (node.nodeType == 3) node = node.previousSibling;
               return node;
      });
} 

function firstNonTextChild(parent) 
{
    if (parent!=null)
    {
        var node = parent.firstChild;
        while ( node!= null && node.nodeType == 3) {
            node = node.nextSibling;
        }
        
        if (node==null)
        {
            //alert("firstNonTextChild : node null pour " + parent.id);
        }
        return node;
    }
    else
    {
        return null;
    }
    
} 
function lastNonTextChild(parent) 
{
    if (parent!=null)
    {
        var node = parent.lastChild;
        while (node!= null && node.nodeType == 3) {
            node = node.previousSibling;
        }
        
        if (node==null)
        {
            //alert("lastNonTextChild : node null pour " + parent.id);
        }
        return node;
    }
    else
    {
        return null;
    }
    
}

function setMenuActif(menuId)
{
	var menu = document.getElementById(menuId);
	if (menu)
	{
		menu.className += " actif";
	}
}
