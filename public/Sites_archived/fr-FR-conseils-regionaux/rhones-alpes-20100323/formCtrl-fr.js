formCtrl.schemes = [["isNotNull","Le champ \"%s\" doit être renseigné."],
						["isDate","Le champ \"%s\" n'est pas une date valide.\nFormat : jj/mm/aaaa."],
						["isEmail","Le champ \"%s\" n'est pas un email valide."],
						["isInt","Le champ \"%s\" n'est pas un entier valide."],
						["isFloat","Le champ \"%s\" n'est pas un réel valide."]
					];
