function checkKeyPressed(evt, func, params)
{
  evt = (evt) ? evt : (window.event) ? event : null;
  if (evt)
  {
    var charCode = (evt.charCode) ? evt.charCode :
                   ((evt.keyCode) ? evt.keyCode :
                   ((evt.which) ? evt.which : 0));
	var args = arguments;
	args.shift();
	args.shift();
    if (charCode == 13) return func(args);
  }    
}


function ouvreDansOpener(href)
{
	if(window.opener)
	{
		window.opener.focus();
		window.opener.location = href;
		return false;
	}
	else return true;
}