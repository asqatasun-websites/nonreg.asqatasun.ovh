/**
 * Librairie javascript de manipulation de chaines de caractere
 *
 * @package       lemonde
 * @author        Chabe (11/03/08)
 * @author        $Author: chabrou $
 * @version       $Revision: 28997 $
 * @date          $Date: 2008-03-12 20:53:32 +0100 (mer., 12 mars 2008) $
 * @id            $Id: dom.js 28997 2008-03-12 19:53:32Z chabrou $
 */
if (undefined===window.MIA)
{
   var MIA = {};
}
MIA.StringHelper =
{
   class_name : "StringHelper",

   toUrlFriendly : function(strChaine)
   {  
      strChaine = strChaine.toLowerCase();
      
      strChaine = MIA.StringHelper.supressAccents(strChaine);
      
      var tmpChaine = '';
      var tmp = '';
      var intLen = strChaine.length;
      for (var i = 0; i < intLen; i++)
      {
      	var asciiCode = strChaine.charCodeAt(i);
      	if(asciiCode < 48 || asciiCode > 122 || (asciiCode > 57 && asciiCode < 97))
      		tmp = '-';
      	else
            tmp = strChaine.charAt(i);
      	tmpChaine += tmp;
      }
      tmpChaine = tmpChaine.replace(/\-+/g,"-");
      
      return tmpChaine;
   },
   
   supressAccents : function(strChaine)
   {
      strChaine = strChaine.replace(/\xE0/g,"a");
      strChaine = strChaine.replace(/\xE1/g,"a");
      strChaine = strChaine.replace(/\xE2/g,"a");
      strChaine = strChaine.replace(/\xE3/g,"a");
      strChaine = strChaine.replace(/\xE4/g,"a");
      strChaine = strChaine.replace(/\xE5/g,"a");
      strChaine = strChaine.replace(/\xE6/g,"a");
      
      strChaine = strChaine.replace(/\xE7/g,"c");
      
      strChaine = strChaine.replace(/\xE8/g,"e");
      strChaine = strChaine.replace(/\xE9/g,"e");
      strChaine = strChaine.replace(/\xEA/g,"e");
      strChaine = strChaine.replace(/\xEB/g,"e");
      
      strChaine = strChaine.replace(/\xEC/g,"i");
      strChaine = strChaine.replace(/\xED/g,"i");
      strChaine = strChaine.replace(/\xEE/g,"i");
      strChaine = strChaine.replace(/\xEF/g,"i");
      
      strChaine = strChaine.replace(/\xF0/g,"o");
      strChaine = strChaine.replace(/\xF2/g,"o");
      strChaine = strChaine.replace(/\xF3/g,"o");
      strChaine = strChaine.replace(/\xF4/g,"o");
      strChaine = strChaine.replace(/\xF5/g,"o");
      strChaine = strChaine.replace(/\xF6/g,"o");
      
      strChaine = strChaine.replace(/\xF1/g,"n");
      
      strChaine = strChaine.replace(/\xF9/g,"u");
      strChaine = strChaine.replace(/\xFA/g,"u");
      strChaine = strChaine.replace(/\xFB/g,"u");
      strChaine = strChaine.replace(/\xFC/g,"u");
      
      strChaine = strChaine.replace(/\xFD/g,"y");
      strChaine = strChaine.replace(/\xFF/g,"y");
      
      return strChaine;
   },
   isNumeric : function(number)
   {
   	return ('number' == typeof(number) ||
   				("NaN" != String(parseInt(number, 10)).toString()));
   },
   
   strReplace : function(str, find, rep) 
   {
      while (str.search(find) != -1) 
      {
         str = str.replace(find, rep);
      }
      return(str)
   },
   urlDecode : function(encoded)
   {
      var HEXCHARS = "0123456789ABCDEFabcdef"; 
      var plaintext = "";
      var i = 0;
      while (i < encoded.length) 
      {
         var ch = encoded.charAt(i);
   	   if (ch == "+") 
         {
   	       plaintext += " ";
   		   i++;
   	   } 
         else if (ch == "%") 
         {
   			if (i < (encoded.length-2) 
   					&& HEXCHARS.indexOf(encoded.charAt(i+1)) != -1 
   					&& HEXCHARS.indexOf(encoded.charAt(i+2)) != -1 ) 
            {
   				plaintext += unescape( encoded.substr(i,3) );
   				i += 3;
   			} 
            else 
            {
   				plaintext += "%[ERROR]";
   				i++;
   			}
   		} 
         else 
         {
   		   plaintext += ch;
   		   i++;
   		}
   	} // while
      return plaintext;
   },
   
   utf8Decode : function( utftext )
   {
      var string = "";
      var i = 0;
      var c = c1 = c2 = 0;

      while ( i < utftext.length ) {
         c = utftext.charCodeAt(i);

         if (c < 128) {
         	string += String.fromCharCode(c);
         	i++;
         }
         else if((c > 191) && (c < 224)) {
         	c2 = utftext.charCodeAt(i+1);
         	string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
         	i += 2;
         }
         else {
         	c2 = utftext.charCodeAt(i+1);
         	c3 = utftext.charCodeAt(i+2);
         	string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
         	i += 3;
         }
      }
      return string;
   },
   
   ltrim : function (aString) 
   {
      var regExpBeginning = /^\s+/;
      return aString.replace(regExpBeginning, "");
   },

   capitalize : function (aString) {
      return aString.substr(0,1).toUpperCase() + aString.substr(1, aString.length);
   }
}
