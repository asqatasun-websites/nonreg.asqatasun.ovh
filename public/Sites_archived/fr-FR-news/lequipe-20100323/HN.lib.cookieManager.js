if(!window.HN)var HN=window.HN={};
HN.CookieManager=function(){};
HN.CookieManager.prototype={
read:function(name){
var arg=name+"=";
var i=0;
while(i<document.cookie.length){
var offset=i+arg.length;
if(document.cookie.substring(i,offset)==arg){
var endstr=document.cookie.indexOf(";",offset);
if(endstr==-1)endstr=document.cookie.length;
return unescape(document.cookie.substring(offset,endstr));
}
i=document.cookie.indexOf(" ",i)+1;
if(i==0)break;
}
return null;
},
write:function(name,value){
var argv=this.write.arguments;
var argc=this.write.arguments.length;
var expires=(argc>2)?argv[2]:null;
var path=(argc>3)?argv[3]:null;
var domain=(argc>4)?argv[4]:null;
var secure=(argc>5)?argv[5]:false;
document.cookie=name+"="+escape(value)+
((expires==null)?"":("; expires="+expires.toGMTString()))+
((path==null)?"":("; path="+path))+
((domain==null)?"":("; domain="+domain))+
((secure==true)?"; secure":"");
},
del:function(name){
var date=new Date(0);
this.write(name,null,date);
}
};