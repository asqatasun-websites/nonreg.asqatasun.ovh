	
	// Contr�le la validit� de l'email saisi
	function controleEmail(e) {
	
		ok = "1234567890qwertyuiop[]asdfghjklzxcvbnm.@-_QWERTYUIOPASDFGHJKLZXCVBNM";
		for(i=0; i < e.length ;i++){
			if(ok.indexOf(e.charAt(i))<0){
				return (false);
			}
		}
		if (document.images) {
			re = /(@.*@)|(\.\.)|(^\.)|(^@)|(@$)|(\.$)|(@\.)/;
			re_two = /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
			if (!e.match(re) && e.match(re_two)) {
				return (-1);
			}
		}
	}
	
	
	// Retourne TRUE si la chaine en param�tre est num�rique
	function isNumeric(sText) {
		var ValidChars = "0123456789";
		var IsNumber=true;
		var Char;
		
		for (i = 0; i < sText.length && IsNumber == true; i++) { 
			Char = sText.charAt(i); 
			if (ValidChars.indexOf(Char) == -1) {
				IsNumber = false;
			}
		}
		return IsNumber;
	}
	
	// Retourne TRUE si la chaine en param�tre est d�cimal
	function isDecimal(sText) {
		var ValidChars = "0123456789.";
		var IsNumber=true;
		var Char;
		
		for (i = 0; i < sText.length && IsNumber == true; i++) { 
			Char = sText.charAt(i); 
			if (ValidChars.indexOf(Char) == -1) {
				IsNumber = false;
			}
		}
		return IsNumber;
	}
	
	function afficherUnesPourDate(){
		location.replace(contextPath + "/Unes-@/index.jspz?type=unes&date=" + $F("datesUnes"));
		return false;
	}