//js du chargement des images dynamiquement (blogs invites)
//   et zone defile
//@version       $Revision: 44041 $
//@date          $Date: 2010-01-04 11:13:29 +0100 (Mon, 04 Jan 2010) $
if ("undefined" == typeof MIA) MIA={};
if ("undefined" == typeof MIA.Blogs) MIA.Blogs={};
if ("undefined" == typeof MIA.Blogs.Img) {
   
   MIA.Blogs.Img = {

      liste_objets: null,
      liste_id: null,
      hauteurBlocImg: null,
      
      init: function(liste, hauteur)   
      {
         MIA.Blogs.Img.liste_objets = JSON.parse(liste);
         MIA.Blogs.Img.liste_id = MIA.Blogs.Img.liste_objets.map(function (objet) { return objet.id; });
         MIA.Blogs.Img.hauteurBlocImg = hauteur;
         MIA.Blogs.Img.src();
      },

      src: function()
      {
         var sumClientHeight = 0;
         var node_blog_invite = document.getElementById("blogContentId");
         var min_height = node_blog_invite.scrollTop;
         var max_height = node_blog_invite.scrollTop + node_blog_invite.clientHeight;
         
         var objets_affiches = MIA.Blogs.Img.liste_objets.filter(function (objet) {
            var index = MIA.Blogs.Img.liste_id.indexOf(objet.id);
            node_min_height = index * MIA.Blogs.Img.hauteurBlocImg;
            sumClientHeight = sumClientHeight + MIA.Blogs.Img.hauteurBlocImg; //node.clientHeight;
            node_max_height = node_min_height + MIA.Blogs.Img.hauteurBlocImg;
            
            if ((node_min_height >= min_height && node_min_height < max_height) ||
                (node_max_height > min_height && node_max_height < max_height))
                return true;
            return false;
         });

         objets_affiches.forEach(function (objet) {
            var node = document.getElementById(objet.id);
            node.src = objet.src;
            node.onload = function() { document.getElementById(objet.id + 'Throbber').style.display = "none"; }; 
            node.style.display = "block"; 
            
         });

         MIA.Blogs.Img.liste_objets = MIA.Blogs.Img.liste_objets.notIn(objets_affiches);
      }
   }
}


var multimedia_bloc_slider = null;
var tab_image = new Array();

/*
 * Zone Player
 * Le code la zone player n'est pas utilise, 
 *    mais on garde les methodes suivantes qui permettent de charger les images en temps voulu et non au chargement de la page
 */

function afficher_image()
{
   for (cpt = 0 ; cpt < parseInt(tab_image.length) ; cpt++)
   {
      $('#image_player_' + tab_image[cpt][0]).html(tab_image[cpt][1]);
   }
}

function charger_image(num_bloc, image, picto)
{
   tab_tmp = new Array();
   tab_tmp[0] = num_bloc;
   tab_tmp[1] = picto + image;   
   
   tab_image[parseInt(tab_image.length)] = tab_tmp;
}

/*
 * Zone Defile
 */
var defile_bloc_slider = null;

function griseBoutonGaucheDefile()
{
   // Pour masquer progressiement : $('.pasManquerBlock .arrowLeft').animate({ width: 'hide' });
   $('.pasManquerBlock .arrowLeft a').removeClass('on');
   $('.pasManquerBlock .arrowLeft a').addClass('off');
}
function activeBontonGaucheDefile()
{
   // Pour afficher progressiement :  $('.pasManquerBlock .arrowLeft').animate({ width: 'show' });
   $('.pasManquerBlock .arrowLeft a').removeClass('off');
   $('.pasManquerBlock .arrowLeft a').addClass('on');
}
function griseBoutonDroiteDefile()
{
   $('.pasManquerBlock .arrowRight a').removeClass('on');
   $('.pasManquerBlock .arrowRight a').addClass('off');
   
}
function activeBoutonDroiteDefile()
{
   $('.pasManquerBlock .arrowRight a').removeClass('off');
   $('.pasManquerBlock .arrowRight a').addClass('on');
}

/*
 * Bloc "Vos reactions"
 */
 function griseBoutonGaucheReaction()
{
   $('.reactionBlock .arrowLeft a').removeClass('on');
   $('.reactionBlock .arrowLeft a').addClass('off');
}
function activeBontonGaucheReaction()
{
   $('.reactionBlock .arrowLeft a').removeClass('off');
   $('.reactionBlock .arrowLeft a').addClass('on');
}
function griseBoutonDroiteReaction()
{
   $('.reactionBlock .arrowRight a').removeClass('on');
   $('.reactionBlock .arrowRight a').addClass('off'); 
}
function activeBoutonDroiteReaction()
{
   $('.reactionBlock .arrowRight a').removeClass('off');
   $('.reactionBlock .arrowRight a').addClass('on'); 
}

/*
 * Zone appel sport
 */
var appel_sport_slider = null;

function griseBoutonGaucheAppelSport()
{   
   $('.rotationBlock .rotationNav .arrowPrevious').removeClass('arrowOn');
   $('.rotationBlock .rotationNav .arrowPrevious').addClass('arrowOff');
}
function activeBontonGaucheAppelSport()
{
   $('.rotationBlock .rotationNav .arrowPrevious').removeClass('arrowOff');
   $('.rotationBlock .rotationNav .arrowPrevious').addClass('arrowOn');
}
function griseBoutonDroiteAppelSport()
{
   $('.rotationBlock .rotationNav .arrowNext').removeClass('arrowOn');
   $('.rotationBlock .rotationNav .arrowNext').addClass('arrowOff');
   
}
function activeBoutonDroiteAppelSport()
{
   $('.rotationBlock .rotationNav .arrowNext').removeClass('arrowOff');
   $('.rotationBlock .rotationNav .arrowNext').addClass('arrowOn');
}

/*
 * Bloc "Service"
 */
 function griseBoutonGaucheService()
{
   $('.serviceBlock .arrowLeft a').removeClass('on');
   $('.serviceBlock .arrowLeft a').addClass('off');
}
function activeBontonGaucheService()
{
   $('.serviceBlock .arrowLeft a').removeClass('off');
   $('.serviceBlock .arrowLeft a').addClass('on');
}
function griseBoutonDroiteService()
{
   $('.serviceBlock .arrowRight a').removeClass('on');
   $('.serviceBlock .arrowRight a').addClass('off'); 
}
function activeBoutonDroiteService()
{
   $('.serviceBlock .arrowRight a').removeClass('off');
   $('.serviceBlock .arrowRight a').addClass('on'); 
}