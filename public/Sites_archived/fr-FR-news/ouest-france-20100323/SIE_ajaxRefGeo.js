<!--

// Initialisation des variables
  
selectionAncien = 0 ;
selectionNouveau = -1 ;
ancienneLongueurSaisie = 0 ;
idZoneSuggestion = "" ;
idPlusieursVilles = "" ;
idZoneRechercheVille = "" ;
idVilleModifier = "" ;
idVilleTrouvee = "" ;
submitAuto = false;
  
    
CHP = new Array() ;
//lbe Ajout du param CHP_rinsee pour g�rer page locale
CHP_rinsee = "";

function suggereListeVille(){

  // Case recherche du formulaire

  try{existeCHP=CHP_saisieVille;}catch(e){CHP_saisieVille = document.forms[FORM_name].saisieVille ;}
  
  // Tableau des champs � remplir
  
  /* 0 : Code insee
     1 : Code Postal
     2 : Libell�
     3 : Article
     5 : Type de commune
  */
    
  // Combo � cacher
  try{existeCMB = CMB_liste;}catch(e){CMB_liste = new Array();}
  try{CHP[0] = CHP_insee ;}catch(e){CHP[0]=null};
  try{CHP[1] = CHP_cp ;}catch(e){CHP[1]=null};
  try{CHP[2] = CHP_libelle ;}catch(e){CHP[2]=null};
  try{CHP[3] = CHP_article ;}catch(e){CHP[3]=null};
  try{CHP[4] = CHP_type ;}catch(e){CHP[4]=null};
  try{CHP[5] = CHP_libelleSansArt ;}catch(e){CHP[5]=null};
  //lbe Ajout du param CHP_rinsee pour g�rer page locale
  try{CHP_rinsee = CHP_codeInsee ;}catch(e){CHP_rinsee=null};

  saisie = CHP_saisieVille.value ;

  longueurSaisie = saisie.length ;
  
  if ((longueurSaisie!=ancienneLongueurSaisie)){
    if((longueurSaisie>=3 && isNaN(saisie)) || (longueurSaisie==5 && !isNaN(saisie))){

      if(window.XMLHttpRequest){ // Firefox
        xhr_object = new XMLHttpRequest(); 
      }else if(window.ActiveXObject){ // Internet Explorer 
        xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
      }else { // XMLHttpRequest non support� par le navigateur 
        alert("La version de votre navigateur Internet ne vous permet\npas de rechercher une commune, merci de le mettre � jour"); 
        return;
      } 
              
      url = "/ofm_lib/refGeo.php?" ;
      
      // Ajout des param�tres de la recherche, s'ils sont valoris�s
      try{url = url + "&lieudit="+lieudit;}catch(e){}
      try{url = url + "&max="+max;}catch(e){}
      try{url = url + "&approche="+approche;}catch(e){}
      try{url = url + "&filtre="+filtre;}catch(e){}
      try{url = url + "&zone="+zone;}catch(e){}
      
      // Recherche par nom de ville
      if(longueurSaisie>=3 && isNaN(saisie)){
        saisie = encodeChaine(saisie) ;
        url = url+"&nom="+saisie ;
      }
    
      // Recherche par code postal
      if(longueurSaisie==5 && !isNaN(saisie)){
        url = url+"&cp="+saisie ;
      }
      
      //prompt('',url) ;


      xhr_object.open("GET", url, true);
      afficheSuggestionVille() ;
      selectionNouveau = -1 ;
      
      if (idZoneSuggestion==""){
         idElement = "zoneSuggestionVille"
      }
      else{
         idElement = idZoneSuggestion;
      }
      
      document.getElementById(idElement).innerHTML = "Veuillez patienter..." ;
      
      try{clearTimeout(timerRech) ;}catch(e){} ;
      timerRech = setTimeout("afficheResultatRecherche()",700) ;
    }else{
      try{clearTimeout(timerRech) ;}catch(e){} ;

      afficheComboSup() ;
      effaceSuggestionVille() ;
    }
  }
 
  ancienneLongueurSaisie = longueurSaisie ;
}


function vide(){}
//---------------------- COMBO SUP -------------------------------------
function afficheComboSup(){
  for(i=0;i<CMB_liste.length;i++){
    document.getElementById(CMB_liste[i]).style.display = "none" ;
  }
}

function effaceComboSup(){
  for(i=0;i<CMB_liste.length;i++){
    document.getElementById(CMB_liste[i]).style.display = "block" ;
  }
}

//----------------------- SUGGESTIONS VILLE -----------------------------
function afficheResultatRecherche(){

  if (idZoneSuggestion==""){
     idElement = "zoneSuggestionVille"
  }
  else{
     idElement = idZoneSuggestion;
  }

  xhr_object.onreadystatechange = function afficheSuggestions(){ 
      if(xhr_object.readyState == 4){
        //alert(xhr_object.responseText) ;
        document.getElementById(idElement).innerHTML = xhr_object.responseText ;
        affichePhrasePlusieursVilles() ;
        if(!document.forms[FORM_name].elements["aucuneVille"]){
          choixSurvolSel(0) ;
        }
      }
    }
  xhr_object.send(null);
  
}

function afficheSuggestionVille(){
       
  if (idZoneSuggestion==""){
     idElement = "zoneSuggestionVille"
  }
  else{
     idElement = idZoneSuggestion;
  }
       
  document.getElementById(idElement).style.display = "block" ;
  effaceComboSup() ;
}

function affichePhrasePlusieursVilles(){

  if (idPlusieursVilles==""){
     idElement = "plusieursVilles"
  }
  else{
     idElement = idPlusieursVilles;
  }

  if(document.getElementById(idElement)){
  if(!document.forms[FORM_name].elements["aucuneVille"]){
    if(!document.forms[FORM_name].elements["uniqueSuggestion"]){
      document.getElementById(idElement).innerHTML = "Nous avons trouv&#233; plusieurs villes&#160;:" ;
    }else{
      document.getElementById(idElement).innerHTML = "Nous avons trouv&#233; une ville&#160;:" ;
    }
    //document.getElementById(idElement).style.display = "none" ;
  }else{
    document.getElementById(idElement).style.display = "block" ;
  }
}
}

function effaceSuggestionVille(){
  
  if (idZoneSuggestion==""){
     idElementSuggest = "zoneSuggestionVille"
  }
  else{
     idElementSuggest = idZoneSuggestion;
  }
  
  if (idPlusieursVilles==""){
     idElementPlus = "plusieursVilles"
  }
  else{
     idElementPlus = idPlusieursVilles;
  }
  
  document.getElementById(idElementSuggest).style.display = "none" ;
  //document.getElementById(idElementPlus).style.display = "none" ;
}

function effaceRechercheVille(){

  if (idZoneSuggestion==""){
     idElementSuggest = "zoneSuggestionVille"
  }
  else{
     idElementSuggest = idZoneSuggestion;
  }

  document.getElementById(idElementSuggest).style.display = "none" ;
  afficheComboSup() ;
  CHP_saisieVille.value = "" ;
  
  document.getElementById("zoneRechercheVille").style.display = "none" ;
}

//----------------------- MODIFIER VILLE -----------------------------
function afficherModifierVille(){

  if (idVilleModifier==""){
     idElementMod = "villeModifier"
  }
  else{
     idElementMod = idVilleModifier;
  }
  
  if (idVilleTrouvee==""){
     idElementTrou = "villeTrouvee"
  }
  else{
     idElementTrou = idVilleTrouvee;
  }
  

  document.getElementById(idElementMod).style.display = "none" ;
  if(document.getElementById(idElementTrou).type=="text")
     document.getElementById(idElementTrou).value = "" ;
  else
     document.getElementById(idElementTrou).innerHTML = "" ;
  document.getElementById("zoneRechercheVille").style.display = "block" ;
  CHP_saisieVille.focus() ;

  for(i=0;i<CHP.length;i++){
    if(CHP[i]!=null){
      CHP[i].value = "" ;
    }
  }

  selectionAncien = 0 ;
  selectionNouveau = -1 ; 
}

//----------------------- CHOISI VILLE -----------------------------
function choisiVille(donneesTab)
{
  if (idVilleModifier==""){
     idElementMod = "villeModifier"
  }
  else{
     idElementMod = idVilleModifier;
  }
  
  if (idVilleTrouvee==""){
     idElementTrou = "villeTrouvee"
  }
  else{
     idElementTrou = idVilleTrouvee;
  }
  
  //lbe champ input servant � la rech n'est plus affich�  
  document.getElementById(idElementMod).style.display = "none" ;
  
  
  //document.getElementById("villeTrouvee").innerHTML = donneesTab[2] ;
  //lbe champ input servant � la rech n'est plus renseign� avec la valeur trouv�e
  //document.getElementById(idElementTrou).value = donneesTab[2] ;

  for(i=0;i<CHP.length;i++){
    if(CHP[i]!=null){
      CHP[i].value = donneesTab[i] ;
    }
  }

  
  //lbe Ajout du param CHP_rinsee pour g�rer page locale -- est �gal au 1er param de donneesTab � savoir codeInsee
  if(CHP_rinsee != null){
  	CHP_rinsee.value = donneesTab[0];
  }

  if (submitAuto)
  { //lbe traitement de la redirection vers les urls reecrites 
  	if(CHP_rinsee != null){    
	
		//on recupere le code insee pour les arguments de l'url
		var arguments = donneesTab[0];	
		
		//on regarde si il n'y as pas deja un argument sp�cifique dans l'action du formulaire
		var reg = new RegExp("[?]", "g");
		action = document.forms[FORM_action].action.split(reg);
		if(action[1]){
			//on r�cup�re les arguments existants
			reg = new RegExp("[&]", "g");
			param = action[1].split(reg);
			//on recupere la valeur de iLoc si elle existe
			reg = new RegExp("[=]", "g");
			for(i = 0; i < param.length; i++){			
				value = param[i].split(reg);
				if((value[0] == "iLoc")&&(value[1])){
					arguments += "-"+value[1];
				}
			}
		}
		
		//v�rif que l'action concerne bien la redirection vers actuLocale
		if (document.forms[FORM_action].action.indexOf("/actu/actuLocale",0) != -1) {		
			//formatage du libelle de la ville pour l'url
			libelleVille = donneesTab[5];	  	
					
			//remplacement des apostrophes par un tiret
			reg = new RegExp("[\']", "g");
			libelleVille = libelleVille.replace(reg,"-");	  

			//remplacement des caracteres d'echappement par rindutou
			reg = new RegExp("(\\\\)", "g");
			libelleVille = libelleVille.replace(reg,"");			
		
			//remplacement des [���� + accent] par e 	
			libelleVille = libelleVille.replace(/[\u00E8 \u00E9 \u00EA \u00EB]/g,"e")
			libelleVille = libelleVille.replace(/[\u00C8 \u00C9 \u00CA \u00CB]/g,"E")	 	  	
			
			//remplacement des [�� + accent] par a
			libelleVille = libelleVille.replace(/[\u00E0 \u00E2]/g,"a")
			libelleVille = libelleVille.replace(/[\u00C0 \u00C2]/g,"A")	  	
			
			//remplacement des [�� + accent] par o
			libelleVille = libelleVille.replace(/[\u00F4 \u00F6]/g,"o")
			libelleVille = libelleVille.replace(/[\u00D4 \u00D6]/g,"O")	  	
			
			//remplacement des [��+ accent] par i
			libelleVille = libelleVille.replace(/[\u00EE \u00EF]/g,"i")
			libelleVille = libelleVille.replace(/[\u00CE \u00CF]/g,"I")	  	
			
			//remplacement des [���+ accent] par u
			libelleVille = libelleVille.replace(/[\u00FB \u00FC \u00F9]/g,"u")
			libelleVille = libelleVille.replace(/[\u00DB \u00DC \u00D9]/g,"U")
							
			//remplacement des [�+ accent] par c
			libelleVille = libelleVille.replace(/[\u00E7]/gi,"c")
			libelleVille = libelleVille.replace(/[\u00C7]/gi,"C")	

			document.forms[FORM_action].action = "/actu/actuLocale_-"+ libelleVille + "_"+arguments+"_actuLocale.Htm";
		}else{
			var reg=new RegExp("(.php)", "g");
			var rewrite = "_-_"+donneesTab[0]+"_actuLocale.Htm";
			document.forms[FORM_action].action = document.forms[FORM_action].action.replace(reg, rewrite);
		}
  	}
	
    document.forms[FORM_action].submit();
  }

  effaceSuggestionVille() ;
  
  try{fct = eval(FCT_choixVille);}catch(e){}  ;
  
  // envoi des informations sur le formulaire vers xiti
  envoyerInfosXiti();
  
}

// envoi d'informations xiti sur le type de formulaire
// utilise pour la recherche de commune(pour of) 
function envoyerInfosXiti()
{
  try
  {
	// recuperation du nom du formulaire utilise (tetiere ou dans un bloc)
	var formActionSearch = document.forms[FORM_action].name;
	// par defaut, on definit la recherche comme provenant du bloc
	var clickName = "recherche commune - bloc carte";
	// si le nom du formulaire contient tetiere, on redefinit l'element
	if( formActionSearch.indexOf("LocalTetiere") != -1 )
	{
      clickName = "recherche commune - tetiere";
	}
	//
	return xt_click(this, 'C', '1', clickName, 'N');
  }
  catch(e){
  
  };

}



   
function choisiVilleClavier(selectionNouveau){
  donneesVille = document.forms[FORM_name].elements["champ"+selectionNouveau].value ;
  donneesVilleTab = donneesVille.split(",") ;
  choisiVille(donneesVilleTab) ;
}
   
function toucheClavier(evenement){
       
  // Touche saisie
  touche = evenement.keyCode ;

  // Touche Haut
  if(touche==38){
               
    if(selectionNouveau>=0){
      selectionNouveau = parseInt(selectionAncien) - 1 ;
      if(!document.getElementById("ville"+selectionNouveau)){selectionNouveau=selectionAncien ;}
      document.getElementById("ville"+selectionAncien).className = "item" ;
      document.getElementById("ville"+selectionNouveau).className = "itemSel" ;
      selectionAncien = selectionNouveau ;
    }
    
  // Touche Bas             
  }else if(touche==40){
          
    if(selectionNouveau>=0){
      selectionNouveau = parseInt(selectionAncien) + 1 ;
      if(!document.getElementById("ville"+selectionNouveau)){selectionNouveau=selectionAncien ;}
      document.getElementById("ville"+selectionAncien).className = "item" ;
      document.getElementById("ville"+selectionNouveau).className = "itemSel" ;
      selectionAncien = selectionNouveau ;
    }
  }
     
  // Touche Entr�e
  if(touche==13){
    if(selectionNouveau!=-1){
      CHP_saisieVille.value = "" ;
      effaceSuggestionVille() ;
      effaceRechercheVille() ;
      choisiVilleClavier(selectionNouveau) ;
    }
  }
}
    
function choixSurvolSel(idDiv){

  if (idZoneSuggestion==""){
     idElementSuggest = "zoneSuggestionVille"
  }
  else{
     idElementSuggest = idZoneSuggestion;
  }

  selectionNouveau = idDiv ;
     
  if(document.getElementById(idElementSuggest).style.display == "block"){
    if(document.getElementById("ville"+selectionAncien)){

      document.getElementById("ville"+selectionAncien).className = "item" ;
      document.getElementById("ville"+idDiv).className = "itemSel" ;
      selectionAncien = idDiv ;
    }
  }
}

//----------------------- FONCTIONS -----------------------------
function encodeChaine(ch) {
  ch = encodeURI(ch.replace("/[ ]/g","+")) ;
  return ch ;
}

  // *** Op�rations � lancer � la cr�ation de l'objet ***
  // Lance la recherche en cas de pr�remplissage du champ de recherche au chargement
  //suggereListeVille(false);
     
  // Lance la validation de la ville si un des champs cach�s est pr�rempli
  champRempli = false ;
  tabChamp = new Array() ;
  
  if(CHP!=undefined)
  {
	  for(i=0;i<CHP.length;i++){
	    if(CHP[i]!=null){
	      if(!(CHP[i].value==null || CHP[i].value=="")){
	        champRempli = true ;
	      }
	      tabChamp[i] = CHP[i].value ;
	    }else{
	      tabChamp[i] = "" ;
	    }
	  }
  }
  
  if(champRempli){
    choisiVille(tabChamp) ;
    //lbe on n'efface plus la ville s�lectionn�e
    //effaceRechercheVille() ;
  }

//-->
