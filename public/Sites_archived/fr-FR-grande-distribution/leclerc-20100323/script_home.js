﻿// Fonctions permettant la gestion de l'indentification
// et la perte du mot de passe
// 05/03/2002

function validCpost()
{
	document.codepostal.Mask.value = "";
	// Le departement n'est pas un chiffre
	if ((document.codepostal.dept.value != "") && (document.codepostal.dept.value>='a'))
	{
	  alert("Code postal incorrect : saisissez des chiffres.");
	  return (false);
	}
	if (document.codepostal.dept.value.length == 1 )
	{
	  // Ajouter un zro devant
	  document.codepostal.dept.value = "0" + document.codepostal.dept.value;
	}
	if (document.codepostal.dept.value.length == 5 )
	{

		document.codepostal.cityTxt.value = document.codepostal.dept.value;
		document.codepostal.dept.value = '';
		document.codepostal.from.value = 'form_ville';
		//document.codepostal.action="http://fc1.1bis.com/leclerc/valid_city.asp";
		document.codepostal.action="/c2k/portail/storelocator/home.asp?from=nav_ville&codePostal="+document.codepostal.cityTxt.value;

	}
	else
	{
		alert("Vous devez obligatoirement saisir 5 chiffres.");
	  	return (false);
		//document.codepostal.from.value = 'zip';
		//document.codepostal.action="http://fc1.1bis.com/leclerc/list.asp";
	}
	document.codepostal.typeId.value = "LE_M";
	return (true);
}

function validIdent()
{
	if(document.ident.ident.value=='' || document.ident.pass.value=='')
	{
		alert("Pour vous identifier, saisissez votre password et votre identifiant");
		return false;
	}
	else
	{
		return true;
	}
}
function newsletter()
{
	if((document.inscrip.mail.value=='')||(document.inscrip.mail.value=='votre e-mail'))
	{
		alert("Pour vous inscrire à la newsletter, saisissez une adresse e-mail");
		return false;
	}
	else
	{
		str = document.inscrip.mail.value
		valAt = str.indexOf("@")
		valSpace = str.indexOf(" ")
		valAt2 = str.indexOf("@",valAt+1)
		valDot = str.indexOf(".")
		valid = true;

		if(valSpace != -1) // if there is a space
			valid = false
		else if(valAt == -1) // if there isn't an @
			valid = false
		else if(valAt == 0)	// the @ is the first character
			valid = false
		else if(valAt == (valDot +1)) // the dot cames just before the @
			valid = false
		else if(valDot == -1)	// there is no dot
			valid = false
		else if(valAt2 != -1)	// there is a second @
			valid = false
		else if(!isNaN(str.substring(0,1)))		// the first character is a number
			valid = true
		else if(valDot == (str.length - 1))		// we have a final dot
			valid = false

		if(!valid)
			{
			alert('Entrez une adresse e-mail valide');
			return false ;
			}
		else
			{
			//document.inscrip.submit();
			return true;
			}
	}
}
function passPerdu()
{
	//document.ident.action="/c2k/magasin/espace/transac/perdu_motpasse.asp";
	//document.ident.submit();
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}


if ("Netscape" == navigator.appName) {
document.write ("<link rel='stylesheet' href='/c2k/magasin/style_ns.css' type='text/css'>");
}else{
document.write ("<link rel='stylesheet' href='c2k/magasin/style.css' type='text/css'>");
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);


// GESTION POPUP UNIQUE PAR VISITEUR
// Get et Set Cookie
function GetCookie (name) {
var arg = name + "=";
var alen = arg.length;
var clen = document.cookie.length;
var i = 0;
while (i < clen) {
var j = i + alen;
if (document.cookie.substring(i, j) == arg)
return getCookieVal (j);
i = document.cookie.indexOf(" ", i) + 1;
if (i == 0) break;
}
return null;
}
function SetCookie (name, value) {
var argv = SetCookie.arguments;
var argc = SetCookie.arguments.length;
var expires = (argc > 2) ? argv[2] : null;
var path = (argc > 3) ? argv[3] : null;
var domain = (argc > 4) ? argv[4] : null;
var secure = (argc > 5) ? argv[5] : false;
document.cookie = name + "=" + escape (value) +
((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
((path == null) ? "" : ("; path=" + path)) +
((domain == null) ? "" : ("; domain=" + domain)) +
((secure == true) ? "; secure" : "");
}
function DeleteCookie (name) {
var exp = new Date();
exp.setTime (exp.getTime() - 1);
var cval = GetCookie (name);
document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString();
}
var expDays = 30;
var exp = new Date();
exp.setTime(exp.getTime() + (expDays*24*60*60*1000));
function amt(){
var count = GetCookie('count')
if(count == null) {
SetCookie('count','1')
return 1
}
else {
var newcount = parseInt(count) + 1;
DeleteCookie('count')
SetCookie('count',newcount,exp)
return count
}
}
function getCookieVal(offset) {
var endstr = document.cookie.indexOf (";", offset);
if (endstr == -1)
endstr = document.cookie.length;
return unescape(document.cookie.substring(offset, endstr));
}


// Ouverture d'une pop up une seule fois par session
function PopOneTime(url, name, features)
{
var top=25;
var left=25;
var Xtime=amt()
 if (Xtime < 1) {
  window.open(url,name,"top="+top+",left="+left+","+features);
 }
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_showHideLayers() { //v6.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

/*******************************************************
FLASH DETECT 2.5
All code by Ryan Parman and mjac, unless otherwise noted.
(c) 1997-2004 Ryan Parman and mjac
http://www.skyzyx.com
*******************************************************/

// This script will test up to the following version.
flash_versions = 20;

// Initialize variables and arrays
var flash = new Object();
flash.installed=false;
flash.version='0.0';

// Dig through Netscape-compatible plug-ins first.
if (navigator.plugins && navigator.plugins.length) {
	for (x=0; x < navigator.plugins.length; x++) {
		if (navigator.plugins[x].name.indexOf('Shockwave Flash') != -1) {
			flash.version = navigator.plugins[x].description.split('Shockwave Flash ')[1];
			flash.version = flash.version.split('.')[0]
			flash.installed = true;
			break;
		}
	}
}

// Then, dig through ActiveX-style plug-ins afterwords
else if (window.ActiveXObject) {
	for (x = 2; x <= flash_versions; x++) {
		try {
			oFlash = eval("new ActiveXObject('ShockwaveFlash.ShockwaveFlash." + x + "');");
			if(oFlash) {
				flash.installed = true;
				flash.version = x + '.0';
			}
		}
		catch(e) {}
	}
}

// Create sniffing variables in the following style: flash.ver[x]
// Modified by mjac
flash.ver = Array();
for(i = 4; i <= flash_versions; i++) {
	eval("flash.ver[" + i + "] = (flash.installed && parseInt(flash.version) >= " + i + ") ? true : false;");
}
function xt_clic(typecl,page,url,nvlle)
{
hsh = new Date();
hsd = document;
hsr = hsd.referrer.replace(/[<>]/g, '');
xt_img = new Image();
hsi = 'http://logi6.xiti.com/hit.xiti?s=119142';
hsi += '&p='+page+'&clic='+typecl+'&hl=' + hsh.getHours() + 'x' + hsh.getMinutes() + 'x' + hsh.getSeconds();
if(parseFloat(navigator.appVersion)>=4)
{Xiti_s=screen;hsi += '&r=' + Xiti_s.width + 'x' + Xiti_s.height + 'x' + Xiti_s.pixelDepth + 'x' + Xiti_s.colorDepth;}
hsi += '&ref=' + hsr.replace(/&/g, '$');
xt_img.src = hsi;
if (url != null)
{ if ((nvlle=='')||(nvlle==null)) { hsd.location = url;} else {xfen = window.open(url,'xfen',''); xfen.focus();}}
else
{return;}
}


// focus sur combo code postal

function FocusText(BoxName)
{ if (BoxName.value == BoxName.defaultValue) {
		BoxName.value = ''; } }

function BlurText(BoxName)
{ if (BoxName.value == '') {
		BoxName.value = BoxName.defaultValue; } }
		
