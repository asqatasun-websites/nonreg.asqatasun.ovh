		__TICKER_ALLOWED__=true;
	    __ARR_BANDO_CONTENT__=[];
		__START_POINT__=0;
		__END_POINT__=0;
		__CURRENT_TEXT__=0;
		__PX_STEP__=1;
		__TEXT_WIDTH__=0;
		__TIMEOUT__=0;
		__DEFIL_ALLOWED__=false;
		__DEFIL_CONTAINER__="defiltextcontainer";
		__DEFIL_CONTENT__="defiltextcontent";
		var __REFRESH__ = 2;
		var __SPEED__ = 15;
	
	
		function add_text(strHTMLContent){__ARR_BANDO_CONTENT__[__ARR_BANDO_CONTENT__.length]=strHTMLContent;}
	
		function launch_defil_scripts(repositionne){
			__START_POINT__=dlib_layers(__DEFIL_CONTAINER__, WIDTH);			
			dlib_layers(__DEFIL_CONTAINER__, LEFT, dlib_anchors("defilpos", LEFT));
			dlib_layers(__DEFIL_CONTAINER__, TOP, dlib_anchors("defilpos", TOP));
			dlib_layers(__DEFIL_CONTENT__, LEFT, __START_POINT__);
			dlib_layers(__DEFIL_CONTENT__, CONTENT, "<table cellpadding='0' cellspacing='0' border='0'><tr><td nowrap class=\"texte\">"+__ARR_BANDO_CONTENT__[__CURRENT_TEXT__]+"</td></tr></table>");
			dlib_layers(__DEFIL_CONTAINER__, CAPTURE_EVENT, [MOUSE_OVER, "defil_stop"]);
			dlib_layers(__DEFIL_CONTAINER__, CAPTURE_EVENT, [MOUSE_OUT, "defil_start"]);
			dlib_layers(__DEFIL_CONTAINER__, VISIBLE, true);
			__DEFIL_ALLOWED__=true;
			if(!repositionne){
			//alert('from launch_defil_scripts');
			__TIMEOUT__=setTimeout("defil_text()", 5);
			}
			
		}

		function Ticker(){		
		if (document.getElementById(__DEFIL_CONTAINER__))
			launch_defil_scripts(true);
		}

	
		function init_defil_text(){
		    if (document.getElementById(__DEFIL_CONTAINER__))
		    {
			    dlib_anchors("defilpos");
			    setTimeout("launch_defil_scripts(false)",__REFRESH__);
			}
		}
	
		function defil_stop(){
			__DEFIL_ALLOWED__=false;
			if(__TIMEOUT__) clearTimeout(__TIMEOUT__);
			return true;
		}
	
		function defil_start(){
			__DEFIL_ALLOWED__=true;
			if(__TIMEOUT__) clearTimeout(__TIMEOUT__);			
			defil_text();
			return true;
		}
	
		function defil_text(){
			if(__DEFIL_ALLOWED__){
				if(dlib_layers(__DEFIL_CONTENT__, RIGHT, add(-__PX_STEP__))<__END_POINT__){
					__CURRENT_TEXT__++;
					if(__CURRENT_TEXT__==__ARR_BANDO_CONTENT__.length) __CURRENT_TEXT__=0;
					dlib_layers(__DEFIL_CONTENT__, LEFT, __START_POINT__);
					dlib_layers(__DEFIL_CONTENT__, CONTENT, "<table cellpadding=0 cellspacing=0 border=0><tr><td nowrap class=\"texte\">"+__ARR_BANDO_CONTENT__[__CURRENT_TEXT__]+"</td></tr></table>");
				}
				__TIMEOUT__=setTimeout("defil_text()", __SPEED__);
				return true;
			}
			if(__TIMEOUT__) clearTimeout(__TIMEOUT__);
			return false;
		}		
		// addind text
