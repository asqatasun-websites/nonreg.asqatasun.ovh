var backText;
var backPath = "0";
var path = new Array;
var displayList = new Array;
var indexName = "index.htm";
var root = " ";
var quarter1Count = 0;
var quarter2Count = 0;
var quarter3Count = 0;
var quarter4Count = 0;
var newsCount = 0;
function dateConvert(dateNum1, dateNumExp, type)
{
	var months=new Array(13);
	months[1]="Jan";
	months[2]="Feb";
	months[3]="Mar";
	months[4]="Apr";
	months[5]="May";
	months[6]="Jun";
	months[7]="Jul";
	months[8]="Aug";
	months[9]="Sep";
	months[10]="Oct";
	months[11]="Nov";
	months[12]="Dec";

	year = dateNum1.substring(2,4);
	month = dateNum1.substring(4,6)
                     monthInt = parseInt(month,10);
                     newMonth = months[monthInt];
	day = dateNum1.substring(6,8);
                     if (type == 'news')
                     {
	           dateString = day + "." + newMonth + "." + year;
                     }
                     if (type == 'event')
                     {
                                dayInt = parseInt(day,10);
                                dateString = dayInt + "." + newMonth + "." + year;
                                if (parseInt(dateNum1.substring(0,8)) < parseInt(dateNumExp.substring(0,8)))
                                {
                                             yearExp = dateNumExp.substring(2,4);
	                       monthExp = dateNumExp.substring(4,6)
                                            monthIntExp = parseInt(monthExp,10);
                                            newMonthExp = months[monthIntExp];
	                       dayExp = dateNumExp.substring(6,8);
                                             dayIntExp = parseInt(dayExp,10);
                                             dateString = dateString + " - " + dayIntExp + "." + newMonthExp + "." + yearExp;
                                }
                     }

                      return dateString;
}
//this script resizes the text box on the homepage of IE or NS

function textboxresize(){
var netscapeb ="<input type=\"text\" name=\"qstr\" size=\"10\">";
var microsoftb = "<input type=\"text\" name=\"qstr\" size=\"15\">";

	if(navigator.appName == "Netscape"){
	return netscapeb;

	}else{

	return microsoftb;
	}
	
}

//Used for changing IDX_site to upper case readables
//location: above search box heading alt tag


function firstletterup(s, cseg) {

var finalresult = "";

//custom for about section
if(cseg == 'investors'){
finalresult = 'Investors';
return finalresult;
}

if(cseg == 'news_a_events'){
finalresult = 'News & Events';
return finalresult;
}

var passedvar = s;

var firstletter = passedvar.charAt(0);
//for normal function
if(passedvar == 'es7000__servers '){
finalresult = 'ES7000 Servers';
return finalresult;
}

if(passedvar.indexOf("_") >= 0){

var loc = passedvar.indexOf("_");
var startstring = passedvar.substr(1, loc-1);
var firstcap = passedvar.charAt(0).toUpperCase();
var capposition = Number(loc + 1);
var othercap = passedvar.charAt(capposition).toUpperCase();
var endstring = passedvar.substr(capposition + 1);

return  firstcap + startstring + " " + othercap + endstring;


}
else
{

var finalresult = firstletter.toUpperCase() + passedvar.substr(1, passedvar.length);
return finalresult;

}
}//adjusted to be able to feed quarter translation and back to top translation - jwl 11.07.05
function quarterCheck(newsDate, year, q1, q2, q3, q4, backtotop)
{
   
	q1Text = "1st Quarter"
	q2Text = "2nd Quarter"
	q3Text = "3rd Quarter"
	q4Text = "4th Quarter"
	backtotopText = "Back to Top"
	if (q1)
	{
		if (q1!='')
		{
			q1Text = q1
		}
	}
	if (q2)
	{
		if (q2 != "")
		{
			q2Text = q2
		}
	}
	if (q3)
	{
		if (q3 != "")
		{
			q3Text = q3
		}
	}
	if (q4)
	{
		if (q4 != "")
		{
			q4Text = q4
		}
	}
	if (backtotop)
	{
		if (backtotop != "")
		{	   
			backtotopText = backtotop
		}
	}
   
	year = parseInt(year,10);
	newsDate = parseInt(newsDate,10);
	var qtrOne = year + parseInt("01010000",10)
	var qtrTwo = year + parseInt("04010000",10)
	var qtrThree = year + parseInt("07010000",10)
	var qtrFour = year + parseInt("10010000",10)
	var qtrString = "";
	var hrString = '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="right" height="30" valign="top"> '
	var hrString = hrString + '<a href="#top" class="backTop">' + backtotopText + '</a>'
	var hrString = hrString + '</td></tr>'
	var hrString = hrString + '<tr><td><img height=1 src="/corporate/images/admin/misc/greypixel.gif" width=100%></td></tr>'
	var hrString = hrString + '</table><br>'
  
	if (newsCount == 0)
	{
		hrString = "";
		newsCount++;
	}
	if ((newsDate > qtrOne) && (newsDate < qtrTwo) && (quarter1Count == 0))
	{
		qtrString = hrString + '<a name="#qtrOne"></a><h5>' + q1Text + '</h5>';
		quarter1Count++;
	}
	if ((newsDate > qtrTwo) && (newsDate < qtrThree) && (quarter2Count == 0))
	{
		qtrString = hrString + '<a name="#qtrTwo"></a><h5>' + q2Text + '</h5>';
		quarter2Count++;
	}
	if ((newsDate > qtrThree) && (newsDate < qtrFour) && (quarter3Count == 0))
	{
		qtrString = hrString + '<a name="#qtrThree"></a><h5>' + q3Text + '</h5>';
		quarter3Count++;
	}
	if ((newsDate > qtrFour) && (quarter4Count == 0))
	{
		qtrString = hrString + '<a name="#qtrFour"></a><h5>' + q4Text + '</h5>';
		quarter4Count++;
	}

	return qtrString;
}

function verifyCompatibleBrowser(){ 
    this.ver=navigator.appVersion 
    this.dom=document.getElementById?1:0 
    this.ie5=(this.ver.indexOf("MSIE 5")>-1 && this.dom)?1:0; 
    this.ie4=(document.all && !this.dom)?1:0; 
    this.ns5=(this.dom && parseInt(this.ver) >= 5) ?1:0; 
    this.ns4=(document.layers && !this.dom)?1:0; 
    this.bw=(this.ie5 || this.ie4 || this.ns4 || this.ns5) 
    return this 
} 
bw=new verifyCompatibleBrowser() 
 
var speed=50 
 
var loop, timer 
 
function ConstructObject(obj,nest){ 
    nest=(!nest) ? '':'document.'+nest+'.' 
    this.el=bw.dom?document.getElementById(obj):bw.ie4?document.all[obj]:bw.ns4?eval(nest+'document.'+obj):0; 
    this.css=bw.dom?document.getElementById(obj).style:bw.ie4?document.all[obj].style:bw.ns4?eval(nest+'document.'+obj):0; 
    this.scrollHeight=bw.ns4?this.css.document.height:this.el.offsetHeight 
    this.clipHeight=bw.ns4?this.css.clip.height:this.el.offsetHeight 
    this.up=MoveAreaUp;this.down=MoveAreaDown; 
    this.MoveArea=MoveArea; this.x; this.y; 
    this.obj = obj + "Object" 
    eval(this.obj + "=this") 
    return this 
} 
function MoveArea(x,y){ 
    this.x=x;this.y=y 
    this.css.left=this.x 
    this.css.top=this.y 
} 
 
function MoveAreaDown(move){ 
	if(this.y>-this.scrollHeight+objContainer.clipHeight){ 
    this.MoveArea(0,this.y-move) 
    if(loop) setTimeout(this.obj+".down("+move+")",speed) 

	} 
} 
function MoveAreaUp(move){ 
	if(this.y<0){ 
    this.MoveArea(0,this.y-move) 
	if(loop) setTimeout(this.obj+".up("+move+")",speed) 
	
	} 
} 
 
function PerformScroll(speed){ 
	if(initialised){ 
		loop=true; 
		if(speed>0) objScroller.down(speed) 
		else objScroller.up(speed) 
	} 
} 
 
function CeaseScroll(){ 

    loop=false 
    if(timer) clearTimeout(timer) 

} 

var initialised; 

//On Load Function - Displays Default List
function InitialiseScrollableArea(divContent,divContainer){ 
    var selCat = (document.forms[0].clientcategories.options[document.forms[0].clientcategories.selectedIndex].value)
    
    if (selCat == '')
    {
    	var divConArray=document.forms[0].divContainers.value.split("|")
		selCat = divConArray[0]
    }

	var default_divContent = (divContent + "_" + selCat)
    var default_divContainer = (divContainer + "_" + selCat)

    objContainer=new ConstructObject(default_divContainer) 
    objScroller=new ConstructObject(default_divContent,default_divContainer) 
    objScroller.MoveArea(0,0) 
	objContainer.css.visibility='visible'

    initialised=true; 
} 

//This function displays the clients for the chosen solution
function display_client_list(form){ 
	var val = form.clientcategories.options[form.clientcategories.selectedIndex].value
	var divContainer = "divContainer_" + val
	var divContent = "divContent_" + val

	var divConArray=document.forms[0].divContainers.value.split("|")
   
    //Hide all lists
    for (var i = 0; i < divConArray.length-1; i=i+2)
    { 

        var tempDivContainer = "divContainer_" + divConArray[i+1]
		tempObjContainer=new ConstructObject(tempDivContainer) 
	    tempObjContainer.css.visibility='hidden' 
        
  }
	//show the chosen list
	objContainer=new ConstructObject(divContainer) 
    objScroller=new ConstructObject(divContent,divContainer) 
    objScroller.MoveArea(0,0) 
    objContainer.css.visibility='visible' 
 
    initialised=true; 

} 

//This function takes care of writing the divs to the browser. Using more eprise events was too slow  JWL 031302
function writeClientDivs()
{

	//We will write out to the browser with this
	var divSet = '';
	//This splits an Input tag holding the Solutions and Solution IDS
	var divConArray=document.forms[0].divContainers.value.split("|")
	
	//This builds each div set (one for each solution)	
    for (var i = 0; i < divConArray.length-1; i=i+2)
    { 

		divSet+= '<div id="divContainer_' + divConArray[i+1] + '" class="divContainer">\r\n' 
		divSet+= '<div id="divContent_' + divConArray[i+1] + '" class="divContent">\r\n'
		divSet+= '<table cellpadding="2" cellspacing="0" border="0" width="188">\r\n'
	    divSet+= '<tr valign="top">\r\n'
        divSet+= '<td><img src="/corporate/images/admin/misc/spacer.gif" width="15" height="1"></td>\r\n'
		divSet+= '<td><img src="/corporate/images/admin/misc/spacer.gif" width="15" height="1"></td>\r\n'
        divSet+= '<td><img src="/corporate/images/admin/misc/spacer.gif" width="142" height="1"></td></tr>\r\n'
		
		//This takes care if there is only one client
		if (!document.forms[0].client_hidden.length &&  document.forms[0].client_hidden.value.length >0)
		{
			var clientString = document.forms[0].client_hidden.value
			var client = clientString.substr(0,clientString.indexOf("|")) 

			//If the beginning of display name matches term (except for case)
			var toReplace = clientString.substr(0,clientString.lastIndexOf("|")+1)
			
			//to handle breaker bars & parens
			toReplace = toReplace.replace(/[\|]/g,"[|]")
			toReplace = toReplace.replace(/[\(]/g,"[(]")
			toReplace = toReplace.replace(/[\)]/g,"[)]")

			var re = new RegExp(toReplace,['i'])
			var solutions =  clientString.replace(re,"")

			//added this in case one solution has same wording in it as another - JWL 031802
			var solutionsArray=solutions.split(",")

			//to handle breaker bars & parens
			var toReplace2 = client
			toReplace2 = toReplace2.replace(/[\(]/g,"[(]")
			toReplace2 = toReplace2.replace(/[\)]/g,"[)]")
			var re2 = new RegExp(toReplace2+"[\|]")

			var noClientString = clientString.replace(re2, "") 

			var moviePath = noClientString.substr(0,noClientString.indexOf("|"))	
			
			//to handle breaker bars
			var re3 = new RegExp(moviePath + "[\|]")
			var noMoviePath = noClientString.replace(re3, "")

			var caseStudyPath = noMoviePath.substr(0,noMoviePath.indexOf("|"))						
			var movieTitle = moviePath.substr(moviePath.lastIndexOf("/")+1)			
			
			//added this in case one solution has same wording in it as another - JWL 031802
			//This will loop throught the solutions a client is assigned to comparing to those in divcontent array
			for (k=0; k <= solutionsArray.length; k++)
			{ 

				//If the client is assigned to the solution put it in the hidden div
				//	if (solutions.indexOf(divConArray[i]) >= 0) 		
				if (solutionsArray[k]==divConArray[i]) 		

				{
				
					divSet+='<tr valign="top">\r\n<td>\r\n'

						//If there is a movie associated with client
					if (moviePath.length>0)
					{

//						divSet+='<a href="javascript:play_movie(\'' + moviePath + '\')" onClick=\'dcs_1_1C_ex("'+ movieTitle +'")\'><img src="/micro/images/admin/clients/iconVideo.gif" border="0" alt="Video"></a>\r\n'
						divSet+= '<a href="#" onClick="popwinclients(\'' + moviePath +'\',\'' + movieTitle + '\')"><img src="/corporate/images/admin/clients/iconVideo.gif" border="0" alt="Video"></a>'


					}
					else
					{
						divSet+='&nbsp;'	
					}

					divSet+='</td>\r\n'
					divSet+='<td>\r\n'
				
					//If there is a case study associated with the client
					if (caseStudyPath.length>0)
					{
						divSet+='<a href="' + caseStudyPath + '" target="CaseStudy" onClick="newCaseStudyWindow()"><img src="/corporate/images/admin/clients/iconCase.gif" border="0" alt="Case Study"></a>'
	
					}
					else
					{
						divSet+='&nbsp;'	
					}

				
					divSet+='</td>\r\n'
					divSet+='<td class="clientlist">'+ client + '</td>\r\n'
					divSet+='</tr>\r\n'
					
				}
		
			}
		}
		else
		{

			//Runs through the hidden Input tags that hold info on clients
			for (var j=0; j < document.forms[0].client_hidden.length; j++)
			{	
				var clientString = document.forms[0].client_hidden[j].value
				var client = clientString.substr(0,clientString.indexOf("|")) 

				//If the beginning of display name matches term (except for case)
				var toReplace = clientString.substr(0,clientString.lastIndexOf("|")+1)

				//to handle breaker bars & parens
				toReplace = toReplace.replace(/[\|]/g,"[|]")
				toReplace = toReplace.replace(/[\(]/g,"[(]")
				toReplace = toReplace.replace(/[\)]/g,"[)]")

				var re = new RegExp(toReplace,['i'])
				var solutions =  clientString.replace(re,"")
				
				//added this in case one solution has same wording in it as another - JWL 031802
				var solutionsArray=solutions.split(",")
	
				var toReplace2 = client
				toReplace2 = toReplace2.replace(/[\(]/g,"[(]")
				toReplace2 = toReplace2.replace(/[\)]/g,"[)]")
	
				var re2 = new RegExp(toReplace2+"[\|]")

				var noClientString = clientString.replace(re2, "") 
				var moviePath = noClientString.substr(0,noClientString.indexOf("|"))	

				//to handle breaker bars
				var re3 = new RegExp(moviePath + "[\|]")
				var noMoviePath = noClientString.replace(re3, "")
	
				var caseStudyPath = noMoviePath.substr(0,noMoviePath.indexOf("|"))						
				var movieTitle = moviePath.substr(moviePath.lastIndexOf("/")+1)			
				//added this in case one solution has same wording in it as another - JWL 031802
				//This will loop throught the solutions a client is assigned to comparing to those in divcontent array
				for (k=0; k <= solutionsArray.length; k++)
				{ 

					//If the client is assigned to the solution put it in the hidden div
					//if (solutions.indexOf(divConArray[i]) >= 0) 		
					if (solutionsArray[k]==divConArray[i]) 		

					{
						divSet+='<tr valign="top">\r\n<td>\r\n'

						//alert ("ML: " + moviePath.length +" Path: " +moviePah)
						//If there is a movie associated with client
						if (moviePath.length>0)
						{
 			
							//divSet+='<a href="javascript:play_movie(\'' + moviePath + '\')" onClick=\'dcs_1_1C_ex("'+ movieTitle +'")\'><img src="/micro/images/admin/clients/iconVideo.gif" border="0" alt="Video"></a>\r\n'
							divSet+= '<a href="#" onClick="popwinclients(\'' + moviePath +'\',\'' + movieTitle + '\')"><img src="/corporate/images/admin/clients/iconVideo.gif" border="0" alt="Video"></a>'
						}
						else
						{
							divSet+='&nbsp;'	
						}

						divSet+='</td>\r\n'
						divSet+='<td>\r\n'
				
						//If there is a case study associated with the client
						if (caseStudyPath.length>0)
						{
							divSet+='<a href="' + caseStudyPath + '" target="CaseStudy" onClick="newCaseStudyWindow()"><img src="/corporate/images/admin/clients/iconCase.gif" border="0" alt="Case Study"></a>'
						}
						else
						{
							divSet+='&nbsp;'	
						}
				
						divSet+='</td>\r\n'
						divSet+='<td class="clientlist">'+ client + '</td>\r\n'
						divSet+='</tr>\r\n'

					}
				}
			}
		}

		//close div set
		divSet+= '</table>\r\n'
		divSet+= '</div>\r\n'
		divSet+= '</div>\r\n'
    }

	return divSet;

}



function dcsTrk(dcs_src)
{
	if (document.images)
	{
		dcsTrk.dcs_img[dcsTrk.dcs_ptr] = new Image;
		dcsTrk.dcs_img[dcsTrk.dcs_ptr].src = dcs_src;
		dcsTrk.dcs_ptr++; 
	}
}
dcsTrk.dcs_img = new Array; 
dcsTrk.dcs_ptr = 0;


function dcs_1_1C_ex(dcs_URI,dcs_QRY,dcs_EXT)
{
	var dCurrent = new Date();
	var P = "";
	P+="http"+(window.location.protocol.indexOf('https:')==0?'s':'')+"://dcs4.unisys.com/dcs.gif?";
	if (typeof(dcs_URI) == "string") P+="dcsuri="+escape(dcs_URI)+"VideoClipIsHere";
	else P+= "dcsuri=/invalidjavascript";
	if (typeof(dcs_QRY) == "string") P+="&dcsqry="+escape(dcs_QRY);
	if ((window.document.referrer != "") && (window.document.referrer != "-"))
	{
		if (!(navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) < 4) )
		{
			P+="&dcsref="+window.document.referrer+"VideoClipReferalIsHere";
		}
	}
	if (typeof(dcs_EXT) == "string") P+=dcs_EXT;
	P +="&dcssip="+escape(window.document.domain);  //For Cross domain tracking, replace.
	P+="&dcsdat="+escape(dCurrent.getTime());
	dcsTrk(P);
}
var moviePath_O
var winWidt;
var winHeig;

function popwinclients(movPath,movTitle) 
{
	moviePath_O = movPath

if (moviePath_O.indexOf('SS16X9')>-1){
	winWidt='344';
	winHeig='204';
	}else{
	winWidt='264';
	winHeig='205';
}

    var newWindow = window.open('/corporate/video/player/movie_viewer.htm','movie','toolbars=no,width='+winWidt+',height='+winHeig+'');
winWidtr = parseFloat(winWidt)+8; 
winHeigr = parseFloat(winHeig)+35;


//resize win in case it's open
newWindow.resizeTo(winWidtr,winHeigr);

if (newWindow.opener == null) newWindow.opener = self;

    newWindow.focus();
    dcs_1_1C_ex(movTitle);

} 
function flashintro(command, args) {
	if (command == "start_movie") { 
		play_movie('/corporate/video/clients/client_montage')
	}
    hideflashlayer();

}

function hideflashlayer()
  {
      if(document.layers)
      {alert('Help');
        //nn4 code
        document.layers.flashlayer.visibility = "hide";
      }
      
      else if(document.all)
      {
         //ie4 code
         document.all.flashlayer.style.visibility = "hidden";
      }

      else if(document.getElementById) 
      {
         //nn6 code
         obj = document.getElementById.flashlayer;  obj.style.visibility = "hidden";
      }
  }





function SetOnBoardCookie()
{
	onBoardPW = ""
	
	if (document.mainform.OnBoardAuto.checked == true)
	{
		onBoardPW = document.mainform.VerPassword.value
	}
	
	document.cookie = "onBoardPW=" + onBoardPW + "; path=/; expires=Thu, 31-Dec-2037 23:00:00 GMT" 

}
function SetGartnerCookie()
{
	GartnerPW = "";
        GartnerUID = "";
	
	if (document.mainform.GartnerAuto.checked == true)
	{
		GartnerPW = document.mainform.VerPassword.value;
		GartnerUID = document.mainform.VerLoginID.value;
	}
	
	document.cookie = "GartnerUID =" + GartnerUID + "; path=/; expires=Thu, 31-Dec-2037 23:00:00 GMT" ;

document.cookie = "GartnerPW=" + GartnerPW + ";path=/; expires=Thu, 31-Dec-2037 23:00:00 GMT" ;

}
function SetFCPCookie(idx_fcp_site)
{
	//alert ("idx_fcp_site: " + idx_fcp_site )
	fcp_team_PW = ""
	fcp_recruitment_PW = ""
	
	if (idx_fcp_site == 'team')
	{
		if (document.mainform.FCPAuto.checked == true)
		{

			fcp_team_PW = document.mainform.VerPassword.value
			//alert ("Setting Team Cookie with: " + fcp_team_PW )

		}
		document.cookie = "fcp_team_PW=" + fcp_team_PW + "; path=/; expires=Thu, 31-Dec-2037 23:00:00 GMT" 
	}
	
	if (idx_fcp_site == 'recruitment')
	{
		if (document.mainform.FCPAuto.checked == true)
		{

			fcp_recruitment_PW = document.mainform.VerPassword.value
			//alert ("Setting Recruitment Cookie with: " + fcp_recruitment_PW )

		}
		document.cookie = "fcp_recruitment_PW=" + fcp_recruitment_PW + "; path=/; expires=Thu, 31-Dec-2037 23:00:00 GMT" 
	}

}
sfHover = function() 
{ 
	if (document.getElementById("nav"))
	{
		var sfEls = document.getElementById("nav").getElementsByTagName("li"); 
		for (var i=0; i<sfEls.length; i++) 
		{ 
			sfEls[i].onmouseover=function() 
			{ 
				this.className+=" sfhover"; 
			} 
			sfEls[i].onmouseout=function() 
			{ 
				this.className=this.className.replace(new RegExp(" sfhover\\b"), ""); 
			} 
		} 
	}
} 
if (window.attachEvent) window.attachEvent("onload", sfHover);
