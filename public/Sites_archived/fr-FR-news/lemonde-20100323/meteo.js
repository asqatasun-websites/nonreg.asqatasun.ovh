
/**
 * fonctions pour la recherche météo
 */

function validate_leform(leformamoi) {
   longueurVille = leformamoi.ville.value.length;
   if ( longueurVille == 0 ) {
      var eAccentAigu = String.fromCharCode(233);
      var aAccentGrav = String.fromCharCode(224);
      alert ("Vous avez oubli"+eAccentAigu+" d'"+eAccentAigu+"crire le nom de ville "+aAccentGrav+" rechercher." );
      leformamoi.ville.focus();
      return false;
   }
   else if ( longueurVille < 2 ) {
      alert ("Vous devez saisir au moins 2 lettres." );
      leformamoi.ville.focus();
      return false;
   }
   leformamoi.ville.value = leformamoi.ville.value.toUpperCase();
   return true;
}
