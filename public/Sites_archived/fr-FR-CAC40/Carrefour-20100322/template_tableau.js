	function Collapse(what)
	{	
		var collapseRow;
		var collapseRowId;
		var i = parseInt(what) + 1;
		
		
		while(1)
		{
			collapseRowId = what + "-" + i;
			collapseRow = document.getElementById(collapseRowId);
			if(!collapseRow)
			{
				return;
			}
		
			if (collapseRow.style.display == "")
			{
				
				collapseRow.style.display = "none";    
				i=CollapseR(i,"none"); 
			}
			else
			{				
				collapseRow.style.display = "";
				i=CollapseR(i,""); 
			}
			i = i + 1
		}
	}
	
	function CollapseR(what,displaytype)
	{	
		var collapseRow;
		var collapseRowId;
		var i = parseInt(what) + 1;
		
		while(1)
		{
			collapseRowId = what + "-" + i;
			collapseRow = document.getElementById(collapseRowId);
			if(!collapseRow)
			{
				return i-1;
			}
			if( displaytype != "" )
				collapseRow.style.display = displaytype;
			if (collapseRow.style.display == "none")
			{
				i=CollapseR(i,"none"); 				
			}
			i = i + 1
		}
		return i-1;
	}
	