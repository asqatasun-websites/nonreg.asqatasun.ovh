
			/** variables **/
			var nbimg=0;
			var isTiming=0;
			var zoneImg = 'diapoImg';
			var zoneThumbnails = 'diapoThb';
			var arrowLeft = 'fleche_gauche';
			var arrowRight = 'fleche_droite';
			/** classes **/
			var arrowClass="available";
			var initialImage;
			//var thmbClass="diapoOn";

			/** diaporama **/
			var tempoDiapo = 2000;//en millisecondes
			//var nb_img_affichees = 5;
			var premiere_img_affichee = 0;
			var diapo = {
			init:function(){
				if(document.getElementById("diapoThbPopin")){
					zoneThumbnails = "diapoThbPopin";
					zoneImg = "diapoImgPopin";
					arrowLeft = 'fleche_gauche_Popin';
					arrowRight = 'fleche_droite_Popin';
				}
   			if(document.getElementById && document.getElementsByTagName && document.createElement && document.getElementById(zoneImg) &&  document.getElementById(arrowRight) && document.getElementById(arrowLeft))
   			{
					/** init des �l�ments pour g�rer les fl�ches **/

					document.getElementById(arrowLeft).className=arrowClass;
					document.getElementById(arrowRight).className=arrowClass;
					//document.getElementById(zoneThumbnails).className=thmbClass;


					document.getElementById(arrowLeft).onclick = function()
					{
						diapo.previous();
                  return false;
					}

					document.getElementById(arrowRight).onclick = function()
					{
						diapo.next();
                  return false;
					}

					if(document.getElementById(zoneThumbnails).getElementsByTagName('a'))
					{
						nbimg = document.getElementById(zoneThumbnails).getElementsByTagName('a');
						total_de_depart = parseInt(nbimg);
					}


					for(i=0;i<nbimg.length;i++)
					{
						nbimg[i].tmp = nbimg[i].href;
						nbimg[i].onclick = function()
						{
                     /** si on a un diaporama en cours, on le stoppe **/
							diapo.stop();

							var allSpecial = getElementsByClass('thumbon',document,'a');
							for(o=0;o<allSpecial.length;o++)
							{
								allSpecial[o].className="thumboff";
							}

							if(document.getElementById(zoneImg) && document.getElementById(zoneImg).getElementsByTagName('img') && document.getElementById(zoneImg).getElementsByTagName('a'))
							{
								document.getElementById(zoneImg).getElementsByTagName('img')[0].src=this.tmp;

								//document.getElementById(zoneImg).getElementsByTagName('a')[0].href="#";
							}
							this.className="thumbon";
							return(false);
						}
					}
			}else{
				return false;
			}
		},
		play:function(){
			isTiming = window.setTimeout(diapo.playloop,tempoDiapo);
		},
		stop:function(){
			clearTimeout(isTiming);
		},
		playloop:function(){
			diapo.next();
			diapo.play();
		},
		next:function(){
				var Node = document.getElementById(zoneThumbnails);
            var items = Node.getElementsByTagName('a');
            for(var i=0;i<items.length;i++){
               if(items[i].className == "thumbon"){
                  premiere_img_affichee = i;
               }
            }
            if(premiere_img_affichee==items.length-1){
               oldObj = items[items.length-1];
               premiere_img_affichee=0;            
            }
            else{
               oldObj = items[premiere_img_affichee];
               premiere_img_affichee++;
            }               
               oldObj.className = "thumboff";
               next = items[premiere_img_affichee];
               next.className = "thumbon";  
               if(items[premiere_img_affichee].onclick)items[premiere_img_affichee].onclick();
		},
		previous:function(){
				var Node = document.getElementById(zoneThumbnails);
				var items = Node.getElementsByTagName('a');
            for(var i=0;i<items.length;i++){
               if(items[i].className == "thumbon"){
                  premiere_img_affichee = i;
               }
            }
            if(premiere_img_affichee==0){
               oldObj =items[items.length-1];
               premiere_img_affichee=items.length-1;            
            }
            else{
               oldObj = items[premiere_img_affichee];
               premiere_img_affichee--;
            }
               oldObj.className = "thumboff";
               last = items[premiere_img_affichee];
               last.className = "thumbon";  
               if(items[premiere_img_affichee].onclick)items[premiere_img_affichee].onclick();
		}
		
	};

	var myImg;
	var myPrev;
	var myNext;	
	
	function showLayer(myLayer) {
		var opacitylayer = document.getElementById(myLayer);
			adjustLayer(myLayer);
	};			
	
	function adjustLayer(myLayer) {
		var opacitylayer = document.getElementById(myLayer);
		var clientHeight = document.documentElement.clientHeight;
		var clientWidth = document.documentElement.clientWidth;
		var scrollHeight = document.documentElement.scrollHeight;
		/*if(scrollHeight > clientHeight){
			opacitylayer.style.height = scrollHeight + 'px';
		}else{*/
			opacitylayer.style.height = clientHeight + 'px';
		//}
		//on corrige pour la largeur du layer
		opacitylayer.style.width = clientWidth;
	};

	function test(){
		//console.log('tada');
		return false;
	}
	
	/* zoom produit
	********************************************/		
	function openZoom(){
		myImg = $('.thumbon:eq(0)').children('.bigImg').attr('src');
		
		if($.browser.mozilla) $('.servicesBlocInner, .detailContentScroll').css('overflow','hidden');
		
		if(jQuery("#flashContent").length == 0){
			
			jQuery("#contentAllWebsite").before('<div id="flashContent"></div>');
			
		}else{
			
			jQuery("#flashContent").show();
			
		}
		
		if($.browser.msie){ $("select").css("visibility","hidden"); }
		
		showLayer("flashContent");
		
		var clientHeight = getWindowHeight();
		var clientWidth = getWindowWidth();
		
		if ($.browser.safari) {
			so = new SWFObject("/wcsstore/Conforama/medias/zoom/swf/loader.swf", "zoom", clientWidth, clientHeight, "9", "#ffffff");
		}else{
			so = new SWFObject("/wcsstore/Conforama/medias/zoom/swf/loader.swf", "zoom", "100%", "100%", "9", "#ffffff");
		}
		so.addVariable("confFile", "/wcsstore/Conforama/medias/zoom/xml/conf.xml");
		so.addVariable("img", myImg);
		if ($('#diapoThb a').length == 1) so.addVariable("nb", 1); // si une seule image
		else so.addVariable("nb", 2); // sinon
		so.addParam("allowScriptAccess" , "always");
		so.addParam("wmode", "transparent" );
		so.write("flashContent");
		
		
		jQuery(window).bind('resize', function() {
			var opacitylayer = document.getElementById("flashContent");
			clientHeight = document.documentElement.clientHeight;
			clientWidth = document.documentElement.clientWidth;
			opacitylayer.style.height = clientHeight + 'px';
			opacitylayer.style.width = clientWidth + 'px';
		});
	   
	   $(this).blur();
	   return false;
	}
	
	function onZoom(){
		//jQuery("#contentAllWebsite").hide();
	}
	function onClose(){
		//jQuery("#contentAllWebsite").show();
		
	}
	function closeZoom(){
		if($.browser.mozilla) $('.servicesBlocInner, .detailContentScroll').css('overflow-y','auto');
		if($.browser.msie){ $("select").css("visibility","visible"); }
		if (!$.browser.safari) {	
			jQuery("#flashContent").empty();
			jQuery("#btZoom a, #diapoImg a").bind("click",openZoom);
		}
		jQuery("#flashContent").hide();
	}
	
	function getPosition () {
		var obj = { offsetLeft:document.getElementById("embededPoductSheet").offsetParent.offsetLeft,
					offsetTop:document.getElementById("embededPoductSheet").offsetParent.offsetTop, 
					scrollTop:document.documentElement.scrollTop};
		return obj;
	}
	
	function nextImg (){
		
		var myNewNextA;
		
		if ($('.thumbon:eq(0)').next('a.thumboff').attr('href') == $('.thumbon:eq(0)').attr('href') ){
			myNewNextA = $('#diapoThb a:first').children('.bigImg');
		} else {
			myNewNextA = $('.thumbon:eq(0)').next('a').children('.bigImg');
		}
		
		$('#diapoThb a').removeClass('thumbon');
		$('#diapoThb a').addClass('thumboff');
		//myNewNextA.addClass('thumbon');
		
		diapo.next();
		return myNewNextA.attr('src');
	}
	
	function prevImg () {
	
		var myNewPrevA;
		
		if ($('.thumbon:eq(0)').prev('a.thumboff').attr('href')==$('.thumbon:eq(0)').attr('href')) {
			myNewPrevA = $('#diapoThb a:last').children('.bigImg');
		} else {
			myNewPrevA = $('.thumbon:eq(0)').prev('a').children('.bigImg');
		}
		
		$('#diapoThb a').removeClass('thumbon');
		$('#diapoThb a').addClass('thumboff');
		//myNewPrevA.parent("a").addClass('thumbon');
		
		diapo.previous();
		
		return myNewPrevA.attr('src');
	}
	
	
$(document).ready(function(){
	
	jQuery("#btZoom a, #diapoImg a").bind("click",openZoom);
	
});

/** on load**/
//addEvent(window, 'load',diapo.init, false);