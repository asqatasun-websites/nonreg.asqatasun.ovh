// Boucle de validation
function validateForm(form,lang) {
	lang_id=1;
	if(lang!="undefined" && lang!=''){
		lang_id=lang;
	}
	//alert(lang_id);
	var retour = true;
	var tab_erreur = new Array();
	var str_erreur ="";
	var oInput = "";
	var oTextarea = "";
	document.getElementById("message").innerHTML = "";
	document.getElementById("message").style.display='none';

	var oInput = document.getElementById(form).getElementsByTagName("INPUT");

	for(var i=0;i<oInput.length;i++){
		oInput[i].style.border = '';
		//alert(oInput[i].className);
		switch(oInput[i].className){
			case "required_text":
			//alert(" dans switch "+oInput[i].name +"="+ oInput[i].value);
			if(!isNotEmpty(oInput[i].value)){
				//oEm[i].innerHTML = "&nbsp;&raquo; Le champ est obligatoire";
				//alert(oInput[i].value);
				var tab_obj_type = new Array();
				tab_obj_type['obj'] = oInput[i];
				tab_obj_type['type'] = 'text';
				tab_erreur.push(tab_obj_type);
				retour = false;
			}
			break;
			case "required_email":
			if(!isEMailAddr(oInput[i].value)){
				var tab_obj_type = new Array();
				tab_obj_type['obj'] = oInput[i];
				tab_obj_type['type'] = 'email';
				tab_erreur.push(tab_obj_type);
				retour = false;
			}
			break;
			case "required_number":
			if(!isNumber(oInput[i].value)){
				var tab_obj_type = new Array();
				tab_obj_type['obj'] = oInput[i];
				tab_obj_type['type'] = 'number';
				tab_erreur.push(tab_obj_type);
				retour = false;
			}
			break;
			case "required_pourcent":
			if((oInput[i].value>100) || (!isNotEmpty(oInput[i].value))){
				var tab_obj_type = new Array();
				tab_obj_type['obj'] = oInput[i];
				tab_obj_type['type'] = 'pourcent';
				tab_erreur.push(tab_obj_type);
				retour = false;
			}
			break;

			/*
			case "required_nicename":
			var exp = new RegExp("[azertyuiopqsdfghjklmwxcvbn123456789]")
			alert(exp.test(oInput[i].value));
			if(exp.test(oInput[i].value)){
			var tab_obj_type = new Array();
			tab_obj_type['obj'] = oInput[i];
			tab_obj_type['type'] = 'nicename';
			tab_erreur.push(tab_obj_type);
			retour = false;
			}
			break;
			*/
		}
	}
	var oTexarea = document.getElementById(form).getElementsByTagName("TEXTAREA");
	for(var i=0;i<oTexarea.length;i++){
		oTexarea[i].style.border = '';
		oTexarea[i].style.fontWeight = '';
		switch(oTexarea[i].className){
			case "required_text":
			//alert(" dans switch "+oInput[i].name +"="+ oInput[i].value);
			if(!isNotEmpty(oTexarea[i].value)){
				//			oEm[i].innerHTML = "&nbsp;&raquo; Le champ est obligatoire";
				var tab_obj_type = new Array();
				tab_obj_type['obj'] = oTexarea[i];
				tab_obj_type['type'] = 'text';
				tab_erreur.push(tab_obj_type);
				retour = false;
			}
			break;
			case "required_email":
			if(!isEMailAddr(oTexarea[i].value)){
				var tab_obj_type = new Array();
				tab_obj_type['obj'] = oTexarea[i];
				tab_obj_type['type'] = 'email';
				tab_erreur.push(tab_obj_type);
				retour = false;
			}
			break;
			case "required_number":
			if(!isNumber(oTexarea[i].value)){
				var tab_obj_type = new Array();
				tab_obj_type['obj'] = oTexarea[i];
				tab_obj_type['type'] = 'number';
				tab_erreur.push(tab_obj_type);
				retour = false;
			}
			break;
			case "required_pourcent":
			if((oTexarea[i].value>100) || (!isNotEmpty(oTexarea[i].value))){
				var tab_obj_type = new Array();
				tab_obj_type['obj'] = oTexarea[i];
				tab_obj_type['type'] = 'pourcent';
				tab_erreur.push(tab_obj_type);
				retour = false;
			}
			break;
		}
	}
	var oSelect = document.getElementById(form).getElementsByTagName("SELECT");

	for(var i=0;i<oSelect.length;i++){
		oSelect[i].style.border = '';
		switch(oSelect[i].className){
			case "required_select":
			if(oSelect[i].options[oSelect[i].selectedIndex].value==""){
				var tab_obj_type = new Array();
				tab_obj_type['obj'] = oSelect[i];
				tab_obj_type['type'] = 'text';
				tab_erreur.push(tab_obj_type);
				retour = false;
			}
			break;
		}
	}
	var nom;
	for(var i=0;i<tab_erreur.length;i++){
		nom = tab_erreur[i]['obj'].id;
		var oLabel = document.getElementById(form).getElementsByTagName("LABEL");
		for(var indice=0;indice<oLabel.length;indice++){
			if(oLabel[indice].htmlFor == nom){
				//oLabel[indice].style.fontWeight = 'bold';
				myid = new String(oLabel[indice].htmlFor);
				rExp = /:/gi;
				results = myid.replace(rExp, "");
				myid = new String(results);

				champ = oLabel[indice].innerHTML;
				champ = champ.substring(0,(champ.length-1));
				rExp = "*";
				myString = new String(oLabel[indice].innerHTML);
				results = myString.replace(rExp, "");
				rExp = /:/gi;
				myString = new String(results);
				champ = myString.replace(rExp, "");
				switch(tab_erreur[i]['type']){
					case "text":
					if(lang_id==1){
						str_erreur += '<a href=\'javascript:void(0);\' class=\'alert_info\' onclick=\'setfocus("'+nom+'")\'> Le champ <b>"'+champ+'"</b> doit &ecirc;tre renseign&eacute;</a> <br>';
						//				str_erreur += '- Le champ <b>"'+champ+'"</b> doit &ecirc;tre renseign&eacute;<br>';
					}
					if(lang_id==2){
						str_erreur += '<a href=\'javascript:void(0);\' class=\'alert_info\' onclick=\'setfocus("'+nom+'")\'> Field <b>"'+champ+'"</b> required</a> <br>';
					}
					if(lang_id==3){
						str_erreur += '<a href=\'javascript:void(0);\' class=\'alert_info\' onclick=\'setfocus("'+nom+'")\'> Field <b>"'+champ+'"</b> required</a> <br>';
					}
					if(lang_id==4){
						str_erreur += '<a href=\'javascript:void(0);\' class=\'alert_info\' onclick=\'setfocus("'+nom+'")\'> Field <b>"'+champ+'"</b> required</a> <br>';
					}
					if(lang_id==5){
						str_erreur += '<a href=\'javascript:void(0);\' class=\'alert_info\' onclick=\'setfocus("'+nom+'")\'> Field <b>"'+champ+'"</b> required</a> <br>';
					}
					if(lang_id==6){
						str_erreur += '<a href=\'javascript:void(0);\' class=\'alert_info\' onclick=\'setfocus("'+nom+'")\'> Field <b>"'+champ+'"</b> required</a> <br>';
					}
					break;
					case "email":
					if(lang_id==1){
						str_erreur += '<a href=\'javascript:void(0);\' class=\'alert_info\' onclick=\'setfocus("'+nom+'")\'>  Le champ <b>"'+champ+'"</b> doit contenir un email </a><br>';
					}
					if(lang_id==2){
						str_erreur += '<a href=\'javascript:void(0);\' class=\'alert_info\' onclick=\'setfocus("'+nom+'")\'>  Field <b>"'+champ+'"</b> must contain a valid mail adress </a><br>';
					}
					break;
					case "number":
					if(lang_id==1){
						str_erreur += '<a href=\'javascript:void(0);\' class=\'alert_info\' onclick=\'setfocus("'+nom+'")\'>  Le champ <b>"'+champ+'"</b> doit contenir un nombre</a><br>';
					}
					if(lang_id==2){
						str_erreur += '<a href=\'javascript:void(0);\' class=\'alert_info\' onclick=\'setfocus("'+nom+'")\'>  Field <b>"'+champ+'"</b> must contain a number</a><br>';
					}
					break;
					case "pourcent":
					if(lang_id==1){
						str_erreur += '<a href=\'javascript:void(0);\' class=\'alert_info\' onclick=\'setfocus("'+nom+'")\'>  Le champ <b>"'+champ+'"</b> doit contenir un nombre entre 0 et 100</a><br>';
					}
					if(lang_id==2){
						str_erreur += '<a href=\'javascript:void(0);\' class=\'alert_info\' onclick=\'setfocus("'+nom+'")\'>  Field <b>"'+champ+'"</b> must contain a number 0 and 100</a><br>';
					}
					break;
					case "nicename":
					if(lang_id==1){
						str_erreur += '<a href=\'javascript:void(0);\' class=\'alert_info\' onclick=\'setfocus("'+nom+'")\'>  Le champ <b>"'+champ+'"</b> doit contenir uniquement des lettre de a->z</a><br>';
					}
					if(lang_id==2){
						str_erreur += '<a href=\'javascript:void(0);\' class=\'alert_info\' onclick=\'setfocus("'+nom+'")\'>  Field <b>"'+champ+'"</b> must contain a letter beetween a->z</a><br>';
					}
					break;
				}
			}
		}
		document.getElementById("message").innerHTML = '<div class="errorMessage">'+str_erreur+'</div>';
		document.getElementById("message").style.display='block';
		tab_erreur[i]['obj'].style.border = '2px solid red';

	}
	return retour;
}
// Verifie si un champ a bien ete renseigne
function isNotEmpty(str) {
	var re = /.+/;
	return(str.match(re));
}

// Verifie si l'adresse e-mail est valide
function isEMailAddr(str) {
	var re = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
	return(str.match(re));
}

//Verifie si un champ ne contient que des chiffres
function isNumber(str) {
	if(str==""){
		return false;
	}else{
		var re = /^[-]?\d*\.?\d*$/;
		str = str.toString();
		//alert(str);
		return(str.match(re));
	}
}
function setfocus(nom) {

	if(document.getElementById(nom).type != "hidden"){
		document.getElementById( nom ).focus();
	}
	return;
}
function getElementsByClassName( clsName )   {
	var arr = new Array();
	var elems = document.getElementsByTagName("*");
	for ( var cls, i = 0; ( elem = elems[i] ); i++ ) {
		if ( elem.className == clsName ) {
			arr[arr.length] = elem;
		}
	}
	return arr;
}
