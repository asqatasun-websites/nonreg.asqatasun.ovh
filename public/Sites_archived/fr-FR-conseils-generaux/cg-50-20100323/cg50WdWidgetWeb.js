/******************************************************************************
* cg50WdWidgetWeb.js
*******************************************************************************

*******************************************************************************
*                                                                             *
* Copyright 2006									                          *
*                                                                             *
******************************************************************************/
function CG50WidgetWeb(pVmarquee, pMarqueecontainer)
{
	var delayb4scroll = 0;
	var marqueespeed = 2;
	var pauseit = 1;
	var typeScroll = 'top';
	var isInit = false;
	
	////NO NEED TO EDIT BELOW THIS LINE////////////
	var copyspeed = marqueespeed;
	var pausespeed = (pauseit == 0) ? copyspeed : 0;
	var actualheight = '';
	var cross_marquee = null;
	var marqueeheight = null;
	var actualheight = null;
	var vmarquee = pVmarquee;
	var marqueecontainer = pMarqueecontainer;
	
		
	function scrollMarquee()
	{
		if(typeScroll == 'top')
		{
			if (parseInt(cross_marquee.style.top) > (actualheight * (-1) + 8))
			{
				cross_marquee.style.top = parseInt(cross_marquee.style.top) - copyspeed + "px";
			}
			else
			{
				cross_marquee.style.top = parseInt(marqueeheight) + 8 + "px";
			}
		}
		if(typeScroll == 'bottom')
		{
			if (parseInt(cross_marquee.style.top) < (marqueeheight + 8))
			{
				cross_marquee.style.top = parseInt(cross_marquee.style.top) + copyspeed + "px";
			}
			else
			{
				cross_marquee.style.top = "0px";
			}
		}
	}
	
	this.initializeMarquee = function()
	{
		cross_marquee = document.getElementById(vmarquee);
		cross_marquee.style.top = 0;
		marqueeheight = document.getElementById(marqueecontainer).offsetHeight;
		actualheight = cross_marquee.offsetHeight;
		if (window.opera || navigator.userAgent.indexOf("Netscape/7")!=-1)
		{ //if Opera or Netscape 7x, add scrollbars to scroll and exit
			cross_marquee.style.Height = marqueeheight+"px";
			cross_marquee.style.overflow = "scroll";
			return;
		}
		setInterval(scrollMarquee, 30);
	}
	
	this.init = function()
	{
		if(isInit == false)
		{
			isInit = true;
			this.initializeMarquee();
		}
	}
	
	this.setTypeScroll = function(pTypeScroll)
	{
		typeScroll = pTypeScroll;
	}

	this.setCopySpeed = function(pTypeCopySpeed)
	{
		copyspeed = pTypeCopySpeed;
	}
}