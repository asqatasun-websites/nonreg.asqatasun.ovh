/****************************************************************
**	For more information please visit our website
**	© 2000-2005 All Rights Reserved Javeline B.V. i.o.
****************************************************************/

Animate = {
	sequences : [],
	objects : [],
	registerHTML : function(oHTML){
		return this.objects.push(oHTML) - 1;
	},
	getHTML : function(id){
		return this.objects[id];
	},
	register : function(o){
		return this.sequences.push(o) - 1;
	},
	destroy : function(id){
		this.sequences[id] = null;
	},
	findSeq : function(id){
		return this.sequences[id];
	},
	
	//Animation Modules
	left : function(oHTML, value){oHTML.style.left = value + "px";},
	right : function(oHTML, value){oHTML.style.right = value + "px";},
	top : function(oHTML, value){oHTML.style.top = value + "px";},
	bottom : function(oHTML, value){oHTML.style.bottom = value + "px";},
	width : function(oHTML, value, center){oHTML.style.width = value + "px";},
	height : function(oHTML, value, center){oHTML.style.height = value + "px";},
	scrollTop : function(oHTML, value, center){oHTML.scrollTop = value;},
	
	mleft : function(oHTML, value){oHTML.style.marginLeft = value + "px";},
	
	scrollwidth : function(oHTML, value){
		oHTML.style.width = value + "px";
		oHTML.scrollLeft = oHTML.scrollWidth;
	},
	
	scrollheight : function(oHTML, value){
		oHTML.style.height = value + "px";
		oHTML.scrollTop = value;
	},
	
	clipright : function(oHTML, value, center){
		oHTML.style.clip = "rect(auto, auto, auto, " + value + "px)";
		oHTML.style.marginLeft = (-1*value) + "px";
	},
	
	backgroundLeft : function(oHTML, value, center){
		oHTML.style.backgroundPosition = Math.round(value) + "px";
	}
}

if(document.all) Animate.fade = function(oHTML, value){oHTML.style.filter = "alpha(opacity=" + parseInt(value*100) + ")" + (oHTML.extrafilter ? "\n " + oHTML.extrafilter : "");}
else Animate.fade = function(oHTML, value){
	if(oHTML.style.MozOpacity != null) oHTML.style.MozOpacity = value-0.000001;
	else oHTML.style.opacity = value;
}

function Anim(oHTML, type, fromValue, toValue, animtype, frames, interval, onfinish, userdata){
	this.uniqueId = Animate.register(this);
	this.method = Animate[type];
	this.oHTML = oHTML;
	this.onfinish = onfinish;
	this.userdata = userdata;
	this.interval = interval;
	this.frames = frames;
	
	//Compile steps
	this.steps = [fromValue];
	this.step = 0;
	
	var steps = parseInt(frames);
	var scalex = (toValue - fromValue)/((Math.pow(steps,2)+2*steps+1)/(4*steps));
	for(var i=0;i<frames;i++){
		if(animtype == 0 && !value) var value = (toValue - fromValue)/frames; 
		else if(animtype == 1) var value = scalex*Math.pow(((steps-i))/steps,3); 
		else if(animtype == 2) var value = scalex * Math.pow(i/frames, 3); 

		this.steps.push(Math.max(0, this.steps[this.steps.length-1] + value - (value > 1 && i==0?1:0)));
	}
	this.steps[this.steps.length-1] = toValue > 1 ? Math.max(0, toValue-1) : toValue;

	//Stop
	this.stop = function(){
		clearTimeout(this.timer);
		Animate.destroy(this.uniqueId);
	}

	//Play
	AnimateStep(0, this.uniqueId);
}

function AnimateStep(step, uniqueId){
	var o = Animate.findSeq(uniqueId);
	if(!o || !o.oHTML || !o.steps) return;

	o.method(o.oHTML, o.steps[step]);

	if(step < o.frames) o.timer = setTimeout('AnimateStep(' + (step+1) + ', ' + uniqueId + ')', o.interval);
	else if(o.onfinish) o.onfinish(o.oHTML, o.userdata);
}

//Object container several AnimationSequences combining to a full Animation
function Animation(frameperiod, oHTMLLookup){
	this.uniqueId = Animate.register(this);
	
	this.frames = [];
	//this.sequence = [];
	//this.framerate = framerate;
	this.interval = frameperiod;//parseInt(1000/framerate);
	this.oHTMLLookup = oHTMLLookup;
	
	this.addScript = 
	this.addSequence = function(framenr, animseq){
		if(!this.frames[framenr]) this.frames[framenr] = [];
		this.frames[framenr].push(animseq);
		
		return this;
	}
	
	this.stripFunction = function(str){
		q = str.replace(/^\s*function\s*\w*\s*\([^\)]*\)\s*\{/, "");
		q = q.replace(new RegExp("\}$", isIE50 ? "" : "m"), "");
		return q;
	}
	
	this.compile = function(){
		var f = this.frames;
		var q = this.sequence = [];
		
		this.step = 0;
		
		for(var i=0;i<f.length;i++){
			if(f[i]){
				for(var k=0;k<f[i].length;k++){
					//Sequence
					if(f[i][k].compile){
						//if(!f[i][k].compiled) 
						f[i][k].compile(null, this.oHTMLLookup);
					
						for(var j=0;j<f[i][k].steps.length;j++){
							if(!q[i+j]) q[i+j] = [];
							q[i+j].push(this.stripFunction(f[i][k].method.toString()).replace(/value/g, f[i][k].steps[j]).replace(/oHTML/g, f[i][k].oHTMLLookup));
						}
					}
					//Script
					else{
						if(!q[i]) q[i] = [];
						q[i].push(this.stripFunction(f[i][k].toString()));
					}
				}
			}
		}

		//Finalize Sequence
		for(var i=0;i<q.length;i++)
			q[i] = new Function(q[i] ? "var oHTML = " + this.oHTMLLookup + ";\n" + q[i].join("\n") : "");
	}
	
	this.clone = function(oHTMLLookup){
		var o = new Animation(this.interval, oHTMLLookup);
		for(var i=0;i<this.frames.length;i++)
			o.frames[i] = this.frames[i] && this.frames[i].clone ? this.frames[i].clone() : this.frames[i];
		
		o.onfinish = this.onfinish;
		
		return o;
	}
	
	this.timer = null;
	this.play = function(){
		clearInterval(this.timer);
		this.timer = setInterval("var o = Animate.findSeq(" + this.uniqueId + ");if(o.step < o.sequence.length){o.sequence[o.step++]()}else{;o.stop();if(o.onfinish) o.onfinish()}", this.interval);
	}
	this.playreverse = function(){
		if(!this.step) this.step = this.sequence.length-1;
		clearInterval(this.timer);
		this.timer = setInterval("var o = Animate.findSeq(" + this.uniqueId + ");if(o.step >= 0){o.sequence[o.step--]()}else{if(o.onfinishreverse) o.onfinishreverse();o.stop()}", this.interval);
	}
	
	this.start = function(){
		this.stop();
		this.compile();
		this.play();
	}
	this.stop = function(){
		clearInterval(this.timer);
		this.timer = null;
		this.step = 0;
	}
	
	this.isPlaying = function(){
		return this.timer ? true : false;
	}
}

//Object containing the information of a single movement
function AnimationSequence(oHTML, type, fromValue, toValue, animtype, frames, interval){
	this.uniqueId = Animate.register(this);
	
	this.setObject = function(oHTML){
		if(typeof oHTML == "string"){
			this.oHTMLName = oHTML;
			try{this.oHTML = eval(oHTML);}catch(e){}
			return;
		}
		
		if(this.oHTML && this.oHTML.id == "o" + this.uniqueId) this.oHTML.id = "";
		this.oHTML = oHTML;
		if(!this.oHTML.id) this.oHTML.id = "o" + this.uniqueId;
		this.oHTMLName = "document.getElementById('" + this.oHTML.id + "')";
	}
	
	this.type = type;
	this.animtype = animtype || 0;
	this.method = Animate[type];
	if(oHTML) this.setObject(oHTML);
	this.frames = parseInt(frames);
	this.interval = interval;
	
	this.step = 0;
	this.timer = null;
	this.compiled = false;
	
	this.compile = function(ease, oHTMLLookup){
		this.compiled = true;
		this.oHTML = eval("var oHTML=" + oHTMLLookup + ";" + this.oHTMLName);
		this.oHTMLLookup = "Animate.getHTML(" + Animate.registerHTML(this.oHTML) + ")";
		this.toValue = eval(toValue);
		this.fromValue = eval(fromValue);
		this.steps = [this.fromValue];
		this.step = 0;
		
		var steps = this.frames;
		var scalex = (this.toValue - this.fromValue)/((Math.pow(steps,2)+2*steps+1)/(4*steps));
		for(var i=0;i<this.frames;i++){
			switch(this.animtype){
				case 0: var value = (this.toValue - this.fromValue)/this.frames; break;
				case 1: var value = scalex*Math.pow(((steps-i))/steps,3); break;
				case 2: var value = scalex * Math.pow(i/this.frames, 3); break;
			}
			this.steps.push(this.steps[this.steps.length-1] + value - (value > 1 && i==0?1:0));//Math.max(0, );
		}
		this.steps[this.steps.length-1] = toValue > 1 ? Math.max(0, toValue-1) : toValue;//Math.max(0, );

		return this;
	}
	
	this.clone = function(){
		return AnimationSequence(this.oHTMLName, this.type, fromValue, toValue, this.animtype, this.frames, this.interval);
	}
	
	this.execute = function(step){
		this.method(this.steps[step]);
	}
	
	this.start = function(){
		this.compile();
		this.play();
	}
	
	this.play = function(step, oHTML){
		this.stop();
		if(oHTML) this.setObject(oHTML);
		
		if(this.step >= this.steps.length || this.step < 0) this.step = 0;
		this.timer = setInterval("var o = Animate.findSeq(" + this.uniqueId + ");if(o.step < o.steps.length){o.method(o.oHTML, o.steps[o.step++])}else{if(o.onfinish) o.onfinish(o.oHTML);o.stop();}", this.interval);
	}
	
	this.reverseplay = function(oHTML){
		this.stop();
		if(oHTML) this.setObject(oHTML);
		
		if(this.step >= this.steps.length) this.step = this.steps.length - 1;
		this.timer = setInterval("var o = Animate.findSeq(" + this.uniqueId + ");if(o.step >= 0 && o.step < o.steps.length){o.method(o.oHTML, o.steps[o.step--])}else{if(o.onfinishreverse) o.onfinishreverse(o.oHTML);o.stop();}", this.interval);
	}
	
	this.stop = function(){
		clearInterval(this.timer);
		this.timer = null;
	}
	
	this.reset = function(){
		this.stop();
		this.step = 0;
	}
	
	this.isPlaying = function(){
		return this.timer ? true : false;
	}
}

var isIE50 = navigator.appVersion.indexOf("MSIE 5.0") != -1;
var isIE7 = navigator.appVersion.indexOf("MSIE 7") != -1;
//var isSafari = navigator.userAgent.indexOf("Safari") != -1;

if(!Array.prototype.push){
	Array.prototype.push = function(o){
		this[this.length] = o;
		return this.length;
	}
}


var PNGElements = new Array(
	["document.getElementById('logo')", "static/img/logo.gif"],
	["document.getElementById('topmenu')", "static/img/bg_topmenu.gif"],
	["document.getElementById('drivetypes').firstChild", "static/img/lower_whitetrans.gif"],
	["document.getElementById('tiretest').firstChild", "static/img/lower_whitetrans.gif"],
	["document.getElementById('drivecomf').firstChild", "static/img/lower_whitetrans.gif"],
	["document.getElementById('runonflat')", "static/img/runonflat_top.gif"]
);
function fixIE50PNG(){
	if(!isIE50) return;
	
	for(var i=0;i<PNGElements.length;i++){
		eval(PNGElements[i][0]).style.background = "url(" + PNGElements[i][1] + ") no-repeat";
	}
}