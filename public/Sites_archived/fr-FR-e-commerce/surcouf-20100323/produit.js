// *********************** 1�re m�thode pour afficher le top des ventes, on r�cup�re les particularit�s du produit s�par�ment **************

function product_old() {
	var image ;
	var title ;
	var productLink ;
	var by ;
	var price ;
	var typePrice;
	var pricebarre;
	var note ;
	var noteLink;
	var notePicto;
	var imageFlash ;
	var ecoPart ;
	var quantity ;
}

function initProduitDepuisNoeud_old(p_noeud) {
	this.image 			= initValeurDepuisNoeud_old(p_noeud,'image') ;
	this.title 			= initValeurDepuisNoeud_old(p_noeud,'title') ;
	this.productLink 	= initValeurDepuisNoeud_old(p_noeud,'productLink') ;
	this.by			 	= initValeurDepuisNoeud_old(p_noeud,'by') ;
	this.price 			= initValeurDepuisNoeud_old(p_noeud,'price') ;
	this.typePrice 			= initValeurDepuisNoeud_old(p_noeud,'typePrice') ;
	this.pricebarre 			= initValeurDepuisNoeud_old(p_noeud,'pricebarre') ;
	this.note 			= initValeurDepuisNoeud_old(p_noeud,'note') ;
	this.noteLink 	= initValeurDepuisNoeud_old(p_noeud,'noteLink') ;
	this.notePicto  =  initValeurDepuisNoeud_old(p_noeud,'notePicto') ;
	this.picto			= initValeurDepuisNoeud_old(p_noeud,'picto') ;
	this.ecoPart		= initValeurDepuisNoeud_old(p_noeud,'ecoPart') ;
	this.quantity		= initValeurDepuisNoeud_old(p_noeud,'quantity') ;
}

function initValeurDepuisNoeud_old(p_noeud, p_element) {
	if (!p_noeud.getElementsByTagName(p_element)[0]) return null ;
	if (p_noeud.getElementsByTagName(p_element)[0].firstChild)
		return p_noeud.getElementsByTagName(p_element)[0].firstChild.nodeValue
	else return null ;
}

function creerProduitDepuisNoeud_old(p_noeud) {
	var p = new product_old() ;
	p.initDepuisNoeud_old(p_noeud) ;
	return p ;
}

product_old.prototype.initDepuisNoeud_old = initProduitDepuisNoeud_old ;


// *********************** fin de la 1�re m�thode **************

// *********************** 2�me m�thode pour afficher le top des ventes, on r�cup�re les particularit�s du produit globalement, et d�j� format�es en HTML **************

function product() {
	var detail ;
}

function initProduitDepuisNoeud(p_noeud) {
	this.detail = initValeurDepuisNoeud(p_noeud,'detailProduit') ;
}

function initValeurDepuisNoeud(p_noeud, p_element) {
	if (!p_noeud.getElementsByTagName(p_element)[0]) return null ;
	if (p_noeud.getElementsByTagName(p_element)[0].firstChild)
		return p_noeud.getElementsByTagName(p_element)[0].firstChild.nodeValue
	else return null ;
}

function creerProduitDepuisNoeud(p_noeud) {
	var p = new product() ;
	p.initDepuisNoeud(p_noeud) ;
	return p ;
}

product.prototype.initDepuisNoeud = initProduitDepuisNoeud ;

// *********************** fin 2�me m�thode **************
