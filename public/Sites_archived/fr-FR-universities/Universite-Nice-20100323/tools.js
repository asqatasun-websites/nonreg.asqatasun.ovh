/**
*ouvre une fenetre interne � unice2003
* v1.0 - 12112003
*/
function ouvrePopupCentreeSansScrollInterne(name, url, largeur,hauteur){
  var top=(screen.height-hauteur)/2;
  var left=(screen.width-largeur)/2;
  window.open('/jahia/jsp/jahia/templates/myjahiasite/unice2003/'+url,name,"top="+top+",left="+left+",width="+largeur+",height="+hauteur+",menubar=no,scrollbars=no,statusbar=no");
}

/**
* Supprime tous les espaces � droite et � gauche d'une chaine de caract�res
* v1.0 - 17112003
*/
function lRTrim(chaine){
	nouvChaine = "";
	
	if (chaine.length>0){
		tabChar = chaine.split("");
		tabCharSize = tabChar.length;
		premierChar = -1;
		dernierChar = -1;
		for (i=0;i<tabCharSize;i++){
			if (tabChar[i]!=" "){
				if (premierChar==-1) premierChar=i;		
				nouvChaine = nouvChaine + tabChar[i];
			}
			else {
				if (premierChar!=-1){
					if (tabChar[i+1]!=" "){
						nouvChaine = nouvChaine + tabChar[i];		
					}
				}	
			}			
		}	
	}
	return nouvChaine;
}