var LOGIN = {};
LOGIN.frRetorno = null;
LOGIN.htmlLogin = '';
LOGIN.htmlForgot = '';
LOGIN.htmlRegistro = '';
LOGIN.htmlInicioSesion = '';
LOGIN.msg = null;
LOGIN.msg2 = null;

LOGIN.templates = new Array();

LOGIN.cargarPlantillas = function() {

	$.ajax({
	    type: 'GET',
        url: '/js/plantillas/login.html',
        async: false,
        success: function(data) {
            html_comentarios = data;
        }
    });

	var plantillas = html_comentarios.split('###');

	LOGIN.templates['login'] = plantillas[0];
	LOGIN.templates['forgot-password'] = plantillas[1];
	LOGIN.templates['registro'] = plantillas[2];
	LOGIN.templates['inicio-sesion'] = plantillas[3];
};

LOGIN.registroBox = function() {
	if($('div#registrobox').html() == null) {
		$('div#doc-que').before('<div id="registrobox-trans" ></div><div id="registrobox"><div id="registrobox-contenido"></div></div>');
	}
};

LOGIN.cerrar = function() {
	$('div#registrobox,div#registrobox-trans').remove();
};


LOGIN.login = function(fr) {
	
	if(fr != null) LOGIN.frRetorno = fr;
	
	LOGIN.registroBox();
	
	if(LOGIN.htmlLogin == '') {
		
		var contenido = $('<div></div>');
		
		var t = $.template(LOGIN.templates['login']);

		contenido.html(t, {
			queid_master: queid_master
		});
		
		LOGIN.htmlLogin = contenido.html();
	}

	$('div#registrobox-contenido').html(LOGIN.htmlLogin);
	document.frmLogLogeo.email.focus();

	$('div#registrobox-contenido #formLogin-acceder a').click(function() {
		LOGIN.logeo();
	});

	LOGIN.calculaRegistroBox();
};

LOGIN.forgotPass = function() {
	LOGIN.registroBox();
	if(LOGIN.htmlForgot == '') {
		
		var contenido = $('<div></div>');
		
		var t = $.template(LOGIN.templates['forgot-password']);

		contenido.html(t, {});
		
		LOGIN.htmlForgot = contenido.html();
	}
	$('div#registrobox-contenido').html(LOGIN.htmlForgot);
	document.frmLogForgot.email.focus();
	LOGIN.calculaRegistroBox();
};

LOGIN.registro = function() {
	LOGIN.registroBox();
	
	if(LOGIN.htmlRegistro == '') {
		
		var contenido = $('<div></div>');
		
		var t = $.template(LOGIN.templates['registro']);

		contenido.html(t, {
			queid_master: queid_master
		});
		
		LOGIN.htmlRegistro = contenido.html();
	}
	
	$('div#registrobox-contenido').html(LOGIN.htmlRegistro);
	document.formuLog.nombre.focus();
	$('div#registrobox-contenido #formLogin-registro a.btn-form').click(function() {
				LOGIN.nuevo_usuario();
	});
	LOGIN.calculaRegistroBox();
};

LOGIN.inicioSesion = function() {
	LOGIN.registroBox();
	if(LOGIN.htmlInicioSesion == '') {
		
		var contenido = $('<div></div>');
		
		var t = $.template(LOGIN.templates['inicio-sesion']);

		contenido.html(t, {});
		
		LOGIN.htmlInicioSesion = contenido.html();
	}
	$('div#registrobox-contenido').html(LOGIN.htmlInicioSesion);
	LOGIN.calculaRegistroBox();
};

LOGIN.calculaRegistroBox = function() {
	var valor = (($(window).height() - $("#registrobox-contenido").height()) / 2) - 29;
	// si la altura del contenido de registrobox es mayor que la de la ventana
	if (valor > 29) {
		$("#registrobox-contenido").css('top', valor);
	} else {
		//alert("mayor que contenido");
		$("#registrobox").css('position', 'absolute');
		$("#registrobox").height($(document).height());
	}

	$(window).resize(function() {
		var valor = (($(window).height() - $("#registrobox-contenido").height()) / 2) - 29;
		if (valor > 29) {
			$("#registrobox-contenido").css('top', valor);
		} else {
			//alert("mayor que contenido");
			$("#registrobox,#registrobox-trans").css('position', 'absolute');
			$("#registrobox,#registrobox-trans").height($(document).height());
		}
	});
};


LOGIN.emailValido = function(valor) {
	var EmailOk = true;
	var AtSym = valor.indexOf('@');
	var Period = valor.lastIndexOf('.');
	var Space = valor.indexOf(' ');
	var Length = valor.length - 1;
	if ((AtSym < 1) || (Period <= AtSym+1) || (Period == Length ) || (Space  != -1)) {
		  EmailOk = false;
	}
	return EmailOk;
};

LOGIN.reestablecerPass = function() {
	var frm = document.frmLogForgot;
	if(!LOGIN.emailValido(frm.email.value)) {
		$('div#cambio_key #error_datos').show();
	} else {
		$.ajax({type: "POST", url: "/backend/LOGIN.registro.php", data: {accion: 'forgotPass', email: frm.email.value}, async: false, dataType: "json", success:
			function(data) {
				if(data.estado == 'OK') {
					$('.caja-formLogin .formLogin .contenedor #ok_datos').show();
					html = '<div id="ok_datos" class="bocadillo">' + $('.caja-formLogin .formLogin .contenedor #ok_datos').html().replace('##EMAIL##', frm.email.value) + '</div>';
					$('.caja-formLogin .formLogin .contenedor').replaceWith(html);
				}
			}
		});



	}
};

LOGIN.passValida = function(valor) {
	if(valor.length < 5) {
		return false;
	}
	return true;
};

LOGIN.nickValido = function(valor) {
	return true;
}

LOGIN.compruebaNick = function(nick) {

	$.post('/backend/LOGIN.registro.php',
		{
			nick: nick,
			accion: 'usuario'
		},
		function(resp) {

			var div = $('div#error_datos').eq(1);

			if (resp.estado == 'KO') {
				div.html(GENERAL.str_replace('(john)', '(' + nick + ')', div.html()));
				div.html(GENERAL.str_replace('()', '(' + nick + ')', div.html()));
				div.show();
			}
			else {

				div.hide();
				div.html(GENERAL.str_replace('(' + nick + ')', '(john)', div.html()));
				div.html(GENERAL.str_replace('()', '(john)', div.html()));
			}
		},
		'json'
	);
}

LOGIN.compruebaEmail = function(nick) {

	$.post('/backend/LOGIN.registro.php',
		{
			nick: nick,
			accion: 'email'
		},
		function(resp) {

			var div = $('div#error_datos').eq(3);

			if (resp.estado == 'KO') {
				div.html(GENERAL.str_replace('(john)', '(' + nick + ')', div.html()));
				div.show();
			}
			else {

				div.hide();
				div.html(GENERAL.str_replace('(' + nick + ')', '(john)', div.html()));
			}
		},
		'json'
	);
};


LOGIN.logeo = function() {
	var frm = document.frmLogLogeo;
	if(!LOGIN.emailValido(frm.email.value)) {
		$('.contenedor').find('div#error_datos').eq(0).show();
		return;
	}
	if(!LOGIN.passValida(frm.pass2.value)) {
		$('.contenedor').find('div#error_datos').eq(1).show();
		return;
	}

	frm.user.value = frm.email.value;
	datosServicio = LOGIN.servicio(frm.pass2.value);
	frm.servicio.value = datosServicio['servicio'];
	frm.time.value = datosServicio['time'];
	frm.hash.value = datosServicio['hash'];
	frm.pass.value = datosServicio['pass'];

	$.ajax(
		{
			type: "POST",
			url: "/backend/LOGIN.registro.php",
			data: {
				accion: 'logeo',
				email: frm.email.value,
				pass: frm.pass2.value,
				publi: $('[name=publi]:checked').val()
			},
			async: false,
			dataType: "json",
			success:
				function(data) {
					if(data.estado == 'OK') {
						GENERAL.ajaxPost(
							frm,
							function() {
								queid_barra();
								USUARIOS.login();
								
								// acciones despu�s del login								
								$('.avatar-usuario img').attr('src', USUARIOS.sesion.avatar_30);
								HERRAMIENTAS.gusta();
								$('[name=email_remitente]').val(USUARIOS.sesion.email);
								
								if(LOGIN.frRetorno != null) {
									LOGIN.frRetorno();
								}
							},
							''
						);
						LOGIN.cerrar();
					} else {
						if(LOGIN.msg == null) {
							if(data.msg == 'Acceso no autorizado') {
								data.msg = 'Combinaci�n de correo electr�nico/contrase�a incorrecta.';
							}
							$(".contenedor").eq(0).append('<div class="bocadillo" id="error_datos" style=""><div class="contenido clearfix"><div><span class="esquina"></span><p><strong id="msg_no_aut">' + data.msg + '</strong></p></div></div></div>');
							LOGIN.msg = data.msg;
						}
						//alert(data.msg);
					}

				}
		}
	);
};

LOGIN.nuevo_usuario = function() {

	var frm = document.formuLog;

	if(frm.nombre.value == '') {
		$('.contenedor').find('div#error_datos').eq(0).show();
		return;
	}

	if(!LOGIN.emailValido(frm.email.value)) {
		$('.contenedor').find('div#error_datos').eq(3).show();
		return;
	}
	if(!LOGIN.passValida(frm.pass2.value)) {
		$('.contenedor').find('div#error_datos').eq(2).show();
		return;
	}

	if(frm.condiciones.checked == false) {
		$('.contenedor').find('div#error_datos').eq(4).show();
		return;
	} else {
		$('.contenedor').find('div#error_datos').eq(4).hide();
	}

	datosServicio = LOGIN.servicio(frm.pass2.value);
	frm.user.value = frm.email.value;
	frm.pass.value = datosServicio['pass'];
	frm.servicio.value = datosServicio['servicio'];
	frm.time.value = datosServicio['time'];
	frm.hash.value = datosServicio['hash'];
	frm.pass.value = datosServicio['pass'];

	var array = {'servicio':datosServicio['servicio'],'time':datosServicio['time'],'hash':datosServicio['hash'],'id_servicio':datosServicio['servicio'],'id_registro':'2','registro_id_usuario':encodeURIComponent(frm.id_usuario.value),'registro_email_usuario':frm.email.value,'registro_password':datosServicio['pass'],'registro_nombre':encodeURIComponent(frm.nombre.value), 'registro_publi': $('[name=publi]:checked').val(), 'registro_confirmar_condiciones_legales':1, 'registro_express':1};

	vocid_registro(array, 'LOGIN.registroRespuesta');
};

LOGIN.servicio = function(pass) {
	var rtdo = '';
	$.ajax(
		{
			type: "POST",
			url: "/backend/USUARIOS.getServicio.php",
			data: {
				pass: pass
			},
			async: false,
			dataType: "json",
			success:
				function(data) {
					//GENERAL.console(data);
					rtdo = data;
					return rtdo;
				}
		}
	);
	return rtdo;
};

LOGIN.registroRespuesta = function(datos) {
	//GENERAL.console('LOGIN.registroRespuesta');
	//GENERAL.console(datos);
	var frm = document.formuLog;

	if(datos.indexOf('<resp tipo="OK">') >= 0) { // Abro la sesion local
		$.ajax(
			{
				type: "POST",
				url: "/backend/LOGIN.registro.php",
				data: {
					accion: 'alta',
					usuario: frm.id_usuario.value,
					email: frm.email.value,
					time: frm.time.value,
					hash: frm.hash.value
				},
				async: false,
				dataType: "json",
				success:
					function(data) {
						//GENERAL.console(data);
						if(data['estado'] == 'OK') {
							USUARIOS.login();
							queid_barra();
							if(LOGIN.frRetorno != null) {
								LOGIN.frRetorno();
							}
							LOGIN.cerrar();
						}
					}
			}
		);

	} else if(datos.indexOf('<msg id="532">') >= 0) {
		if(LOGIN.msg == null) {
			LOGIN.msg = 'El id del usuario ya est� registrado';
			$(".contenedor").eq(0).append('<div class="bocadillo" id="error_datos" style=""><div class="contenido clearfix"><div><span class="esquina"></span><p><strong>' + LOGIN.msg + '</strong></p></div></div></div>');
		}

	} else if(datos.indexOf('<msg id="533">') >= 0) {
		if(LOGIN.msg2 == null) {
			LOGIN.msg2 = 'El email especificado ya est� registrado';
			$(".contenedor").eq(0).append('<div class="bocadillo" id="error_datos" style=""><div class="contenido clearfix"><div><span class="esquina"></span><p><strong>' + LOGIN.msg2 + '</strong></p></div></div></div>');
		}


	}
};

LOGIN.cargarPlantillas();