var TRU = {};

// init des effets
TRU.effects = function() {
	var init = DOM.byClass('*', 'init');
	var ref = DOM.byId('wrapper');
	if(!ref || !init || init.length < 1) {return;}
	DOM.addClass(ref, 'dynJS');
	for(var i = 0;i < init.length;i++) {
		var o = init[i];
		if(DOM.hasClass(o, 'onShowHide')) {
			if(DOM.isTag(o, 'select')) {
				TRU.showHide(o, 'showHide', DOM.byTag('option', o).length - 2);
				DOM.event.add(o, 'change', function(e) {TRU.showHide(this, 'showHide', DOM.byTag('option', this).length - 2);});
			} else {
				var lk = document.createElement('a');
				lk.href = '#';
				lk.innerHTML = o.innerHTML;
				o.replaceChild(lk, o.firstChild);
				DOM.event.add(lk, 'click', function(e) {
					TRU.showHide(this, 'showHide');
					if(DOM.hasClass(this.parentNode, 'onFocus')) {TRU.focus(this, 'isFocus');}
					e.preventDefault();
				});
			}
		} else if(DOM.hasClass(o, 'onShow')) {
			DOM.event.add(o, 'click', function(e) {TRU.showHide(this, 'show');});
		} else if(DOM.hasClass(o, 'onHide')) {
			DOM.event.add(o, 'click', function(e) {TRU.showHide(this, 'hide');});
		}
		if(DOM.hasClass(o, 'item') && DOM.hasClass(o, 'isFocus')) {
			DOM.event.add(o, 'mouseover', function(e) {TRU.focus(this, 'isFocus');});
			DOM.event.add(o, 'mouseout', function(e) {TRU.focus(this, 'isFocus');});
		}
	}
};

// masquer/montrer
TRU.showHide = function(o, effect, nbr) {
	var id = (o.className || o.parentNode.className).match(/EX|EFX|E[0-9]{2}$/i)[0];
	if((id == 'EX' || id == 'EFX') && effect == 'showHide') {
		var cur = parseInt(o.value);
		for(var i = 1;i <= nbr;i++) {
			var tg = DOM.byId(id.replace(/X/i, '0'+i));
			if(!tg) {return;}
			if(i <= cur) {
				DOM.removeClass(tg, 'hide');
				DOM.addClass(tg, 'show');
			} else {
				DOM.removeClass(tg, 'show');
				DOM.addClass(tg, 'hide');
			}
		}
	} else {
		var tg = DOM.byId(id);
		if(!tg) {return;}
		if(effect == 'showHide') {
			DOM.swapClass(tg, 'show', 'hide') || DOM.swapClass(tg, 'hide', 'show');
		} else {
			DOM.swapClass(tg, effect == 'show' ? 'hide' : 'show', effect);
		}
	}
};

// focus sur une zone
TRU.focusLast = null;
TRU.focus = function(o, cName) {
	while(o && !DOM.hasClass(o, cName)) {o = o.parentNode;}
	if(!o) {return;}
	DOM.addClass(o, 'focus') || DOM.removeClass(o, 'focus');
};

// gestion des formulaires
TRU.forms = {
	// ajout champs dynamiques / dont on ne conna�t pas le nombre
	addFieldsDatas: function(o, addEvent, grp) {
		for(var i = 0;i < o.length;i++) {
			var id1 = o[i].getAttribute('id');
			var id2 = id1.replace(/[0-9]*$/, '');
			if(!DATAS.addFieldsChecks[id2]) {continue;}
			DATAS.validate[id1] = DATAS.addFieldsChecks[id2];
			if(addEvent && !grp) {DOM.event.add(o[i], 'change', function(e) {TRU.forms.control(this);});};
		}
	},
	
	init: function() {
		var f = document.forms.length > 0 ? document.forms : null;
		if(!f) {return;}
		for(var i = 0;i < f.length;i++) {
			if(f[i].elements.length < 1 || !f[i].getAttribute('id')) {continue;}
			for(var j = 0;j < f[i].elements.length;j++) {
				if(/^(input|select|textarea)$/i.test(f[i].elements[j].nodeName)) {
					DOM.event.add(f[i].elements[j], 'focus', function(e) {TRU.focus(this, 'item');});
					DOM.event.add(f[i].elements[j], 'blur', function(e) {TRU.focus(this, 'item');});
				}
			}
			TRU.forms.checks('direct', f[i]);
			DOM.event.add(f[i], 'submit', function(e) {
				TRU.forms.checks('submit', this);
				var valid = TRU.forms.checkJournal();
				if(DATAS.inform.disable.indexOf(this.id) == -1 && !valid) {TRU.forms.inform(this);}
				if(!valid) {e.preventDefault();}
			});
		}
	},
	
	// pour submit direct depuis le HTML
	submit: function(id) {
		var f = DOM.byId(id);
		if(!f) {return;}
		TRU.forms.checks('submit', f);
		var valid = TRU.forms.checkJournal();
		if(DATAS.inform.disable.indexOf(id) == -1 && !valid) {TRU.forms.inform(f);}
		if(valid) {f.submit();}
	},
	
	journal: {},
	checkJournal: function() {
		//alert("checkJournal");
		for(var i in TRU.forms.journal) {if(TRU.forms.journal[i] == false) {return false;}}
		return true;
	},
	
	checks: function(m, form) {
		for(var i in DATAS.validate) {
			var o = DOM.byId(i) || TRU.forms.getBoxes(i);
			if(!o || (o[0] || o).form != form) {continue;}
			if(o.length && DOM.isTag(o[0], 'input')) {
				for(var j = 0;j < o.length;j++) {
					o[j].grp = i;
					m == 'submit' ? TRU.forms.control(o[j], true) : DOM.event.add(o[j], 'click', function(e) {TRU.forms.control(this, true);});
				}
			} else {
				m == 'submit' ? TRU.forms.control(o) : DOM.event.add(o, 'change', function(e) {TRU.forms.control(this);});
			}
		}
	},
	
	control: function(o, grp) {
		var n = o.grp ? o.grp : o.name, checks = DATAS.validate[n], ref = null, msg;
		// traitement des conditions...
		if(DATAS.conditions[n]) {
			var c = DATAS.conditions[n];
			ref = DOM.byId(c.field);
			if(DATAS.rules[c.verif].test(ref)) {checks = c.checks;}
		}
		if(!checks.length) {return;}
		// cas d'un groupe de radio/checkbox
		if(grp) {
			o = TRU.forms.getBoxes(n);
			var l = o.length, c = 0;
			for(var i = 0; i < l; i++) {
				if(o[i].checked) {break;}
				c++;
			}
			c == l ? TRU.forms.displayError(o[0], DATAS.rules.required.msg) : TRU.forms.displayError(o[0]);
		// les autres champs
		} else {
			if(checks[0] == 'required') {
				if(DATAS.rules.required.test(o) == false) {
					msg = DATAS.msgSpecs[o.id] && DATAS.msgSpecs[o.id][0] ? DATAS.msgSpecs[o.id][0] : DATAS.rules.required.msg;
					TRU.forms.displayError(o, msg);
				} else {
					if(checks[1] && DATAS.rules[checks[1]].test(o, ref) == false) {
						msg = DATAS.msgSpecs[o.id] && DATAS.msgSpecs[o.id][1] ? DATAS.msgSpecs[o.id][1] : DATAS.rules[checks[1]].msg;
						TRU.forms.displayError(o, msg);
					} else {
						TRU.forms.displayError(o);
					}
				}
			} else {
				if(o.value.length > 0 && o.value != o.defaultValue && DATAS.rules[checks[0]].test(o, ref) == false) {
					msg = DATAS.msgSpecs[o.id] && DATAS.msgSpecs[o.id][0] ? DATAS.msgSpecs[o.id][0] : DATAS.rules.required.msg;
					TRU.forms.displayError(o, DATAS.rules[checks[0]].msg);
				} else {
					TRU.forms.displayError(o);
				}
			}
		}
	},
	
	getBoxes: function(n) {
		var o = n.split(',');
		for(var i = 0; i < o.length; i++) {
			o[i] = DOM.byId(o[i]);
			if(!o[i]) {return null;}
		}
		return o;
	},
	
	displayError: function(o, m) {
		var n = o.name, id = 'msg'+n, el = DOM.byId(id);
		// enlever le message
		if(!m) {
			if(el) {el.parentNode.removeChild(el);}
			TRU.forms.journal[n] = true;
			if(TRU.forms.checkJournal()) {TRU.forms.inform(o.form, true);}
		// afficher le message
		} else {
			if(el) {
				el.innerHTML = m;
				el.className = 'erreur';
			} else {
				el = document.createElement('em');
				el.setAttribute('id', id);
				el.className = 'erreur';
				el.innerHTML = m;
				// si le champs est un input de type text
				if(TRU.forms.get(o).type == 'text' || TRU.forms.get(o).type == 'password')
				{
					// on recupere le parent pour pouvoir retrouver le label
					var aux = TRU.forms.get(o).parentNode.parentNode.getElementsByTagName('label')[0];
					if(aux)
					{
						// si le label existe on lui ajoute l'erreur
						aux.appendChild(el);
					}
				}
				else
						// s'il s'agit d'une selectBox ou de bouton radio le message d'erreur est inserer directement dans la div
						TRU.forms.get(o).appendChild(el);
			}
			TRU.forms.journal[n] = false;
		}
	},

	get: function(o) {
		if(DOM.isTag(o.parentNode, 'label')) {
			do {o = o.parentNode;} while(!DOM.isTag(o, 'div'));
		}
		while(o.previousSibling) {
			o = o.previousSibling;
			if(DOM.isElement(o)) {
				if(/^(input|select|textarea)$/i.test(o.nodeName) || DOM.hasClass(o, 'sr')) {continue} else {break};
			}
		}
		return o;
	},

	inform: function(o, hide) {
		var verif = DOM.byId('inform'+o.name);
		if(!verif && hide) {return;}
		if(verif) {
			if(hide) {
				var timer = setTimeout(function() {verif.parentNode.removeChild(verif)}, 150);
				delete timer;
			} else {
				verif.focus();
			}
			return;
		}
		var el = document.createElement('p');
		el.className = DATAS.inform.cName;
		el.setAttribute('id','inform'+o.name);
		el.setAttribute('tabIndex',-1);
		el.innerHTML = DATAS.inform.msg;
		o.parentNode.insertBefore(el,o);
		el.focus();
	}
};

// mise en place des contr�les pour les champs dont on ne conna�t pas le nombre
TRU.addFields = function() {
	var ref = DOM.byClass('*', 'addChecks');
	if(ref.length < 1) {return;}
	TRU.forms.addFieldsDatas(ref);
};

// creation d'une nouvelle liste
TRU.createList = function() {
	var p = DOM.byClass('p', 'creerListe');
	if(p.length < 1) {return;}
	for(var i = 0;i < p.length;i++) {
		(function() {
			var lk = p[i].firstChild;
			var form = DOM.next(p[i]);
			var id = form.getAttribute('id') == 'fGestionListesCadeaux' ? 'cadeaux' : 'naissance';
			DOM.event.add(lk, 'click', function(e) {
				TRU.createListElements(form, id);
				e.preventDefault();
			});
		}) ();
	}
};

TRU.createListElements = function(form, id) {
	id = id == 'cadeaux' ? 'LC' : 'LN';
	var nb = DOM.byTag('fieldset', form).length + 1;
	var cur = DOM.byId(id+'ListeNb');
	if(!cur) {return;}
	cur.value = nb;
	
	var field = document.createElement('fieldset');
	//field.innerHTML = '<legend><span>Nouvelle liste</span></legend><div class="nomListe"><label for="'+id+'Nom'+nb+'"><span class="sr">Nom de la liste</span></label><input type="text" id="'+id+'Nom'+nb+'" name="'+id+'Nom'+nb+'" value="Nouvelle liste"></div><div class="motDePasse"><label for="'+id+'MDP'+nb+'"><span class="sr">Mot de passe enfant</span></label><input type="text" id="'+id+'MDP'+nb+'" name="'+id+'MDP'+nb+'" value="motdepasse"></div><div class="actions"><a href="#">Voir la liste</a> - <a href="#">Envoyer la liste</a></div><div class="suppression"><label for="'+id+'Suppr'+nb+'"><span class="sr">Supprimer</span></label><input type="checkbox" id="'+id+'Suppr'+nb+'" name="'+id+'Suppr'+nb+'"></div>';
	//<div class="motDePasse"><label for="'+id+'MDP'+nb+'"><span class="sr"></span></label><input type="hidden" id="'+id+'MDP'+nb+'" name="'+id+'MDP'+nb+'" value="mot2passe" /></div>
	if (form.getAttribute('id') == 'fGestionListesCadeaux') {
		field.innerHTML = '<legend><span>Nouvelle liste</span></legend><div class="nomListe"><label for="'+id+'Nom'+nb+'"><span class="sr">Nom de la liste</span></label><input type="text" id="'+id+'Nom'+nb+'" name="'+id+'Nom'+nb+'" value="Nouvelle liste"></div><div class="motDePasse"><label for="'+id+'MDP'+nb+'"><span class="sr">Mot de passe enfant</span></label><input type="text" id="'+id+'MDP'+nb+'" name="'+id+'MDP'+nb+'" value=""></div><div class="actions"><a href="espace-perso-les-listes.php">Annuler</a></div><div class="suppression"></div>';
		
		if (document.getElementById("ref1")){
		document.getElementById("ref1").className="nomListe show";
		document.getElementById("ref2").className="motDePasse show";}
	} else {
		field.innerHTML = '<legend><span>Nouvelle liste</span></legend><div class="nomListe"><label for="'+id+'Nom'+nb+'"><span class="sr">Nom de la liste</span></label><input type="text" id="'+id+'Nom'+nb+'" name="'+id+'Nom'+nb+'" value="Nouvelle liste"></div><input type="hidden" id="'+id+'MDP'+nb+'" name="'+id+'MDP'+nb+'" value="mot2passe" /><div class="actions"><a href="espace-perso-les-listes.php">Annuler</a> - <a href="javascript:document.getElementById(\'fGestionListesNaissance\').submit()">Valider</a></div><div class="suppression"></div>';
	if (document.getElementById("ref3")){
			document.getElementById("ref3").className="nomListe show";
		}	
	}

	var ref = DOM.isElement(form.lastChild) ? form.lastChild : DOM.prev(form.lastChild);
	var validate = DOM.byAttr('input', 'type', 'text', field);
	TRU.forms.addFieldsDatas(validate, true);
	form.insertBefore(field, ref);
};

// desinscription
TRU.desinscription = function() {
	var ref = DOM.byId('fSuppression');
	if(!ref) {return;}
	DOM.event.add(ref, 'submit', function(e) {
		var suppr = DOM.byId('UsrSuppression');
		if(suppr.checked) {window.open(this.action, 'desinscription', DATAS.popups.TRU['desinscription.php']);}
		e.preventDefault();
	});
};

// gestion des popups
TRU.openPopups = function() {
	var lk = DOM.byClass('a', 'popup');
	if(lk.length < 1) {return;}
	for(var i = 0;i < lk.length;i++) {
		DOM.event.add(lk[i], 'click', function(e) {
			var href = this.href.match(/[a-z0-9_-]+\.php/)[0];
			var cur = document.location.href.indexOf('/babies/') != -1 ? DATAS.popups.BRU : DATAS.popups.TRU;
			var attrs = cur[href] || cur['default'];
			window.open(this.href, href.replace(/[.-]*/g, ''), attrs);
			e.preventDefault();
		});
	}
};
TRU.colonne = function() {
		var colonneDroite =0;
		if(document.getElementById('sideDivers')){
			 colonneDroite = parseInt(document.getElementById('sideDivers').offsetHeight) + parseInt(document.getElementById('sideMenus').offsetTop);
		}
		var colonneGauche =0;
		if(document.getElementById('sideMenus')){
			colonneGauche = parseInt(document.getElementById('sideMenus').offsetHeight) + parseInt(document.getElementById('sideMenus').offsetTop);
		}
		var plusGrandeTaille = colonneGauche;
		var centre = parseInt(document.getElementById('wrapper').offsetHeight);
	//	alert("colonneDroite   :    "+colonneDroite);
		//alert("colonneGauche   :    "+colonneGauche);
		//alert("centre   :    "+centre);
		var aux = 0;
		//alert('colonneDroite   '+colonneDroite+'  colonneGauche    '+colonneGauche+'    plusGrandeTaille'+ plusGrandeTaille+'         document.getElementById("sideMenus").style.top   '+document.getElementById('sideMenus').offsetTop);
		if(colonneGauche < colonneDroite)
		{
			plusGrandeTaille = colonneDroite;
		}
		// Si la colonne centrale est plus grande on ne rajoute pas le top de side menus
		aux = plusGrandeTaille;
		if(plusGrandeTaille < centre)
		{
			plusGrandeTaille = centre;
			aux = plusGrandeTaille;
		}
		else{
			//aux = plusGrandeTaille + document.getElementById('sideMenus').offsetTop;
			
		}
		//alert("colonneDroite   :    "+colonneDroite);
		//alert("colonneGauche   :    "+colonneGauche);
		//alert("centre   :    "+centre);
		document.getElementById('wrapper').style.height=parseInt(aux)+ parseInt(20)-parseInt(157)+'px';
		document.getElementsByTagName("body")[0].style.height=parseInt(aux)+ parseInt(20)-parseInt(157)+'px';
		//inserttion d'un footer
		/** supression du footer noel **/
		/*var htmlFooterSalah ="<div id='footerNoel'><div id='maisonNoel'></div></div>";

		var htmlFooterSalahNode = document.createElement("div");
		htmlFooterSalahNode.id="footerNoel";
		document.getElementsByTagName('body')[0].appendChild(htmlFooterSalahNode);*/
		/** fin de la supressio n**/
	}
//fin bulle
// chargement des fonctions
DOM.event.add(window, 'load', function() {
	if(!DOM.support) {return;}
	TRU.effects();
	TRU.forms.init();
	TRU.addFields();
	TRU.openPopups();
	TRU.createList();
	TRU.desinscription();
	TRU.colonne();
	setMargeTitre();
	centerTxt();
});
/* gestion de la patate des titres */
	function setMargeTitre(){
		
		if(document.getElementById('flir'))
		{
			/** marge horisentale **/
			var largeur_temp = document.getElementById('flir').offsetWidth;
			var largeurLeft = 53;
			var largeurRight = 63;
			var largeurTitre = document.getElementById('titreH2').offsetWidth;
			
			var sousLargeur = largeurLeft+largeurRight+largeur_temp;
			var marge = (largeurTitre - sousLargeur) / 2;
			if(document.getElementById('spanLeft'))
				document.getElementById('spanLeft').style.marginLeft = marge+'px';
			/** marge Verticale **/
			var aux = document.getElementById('flir');
			
			var emxx = aux.getElementsByTagName('img')[0];
			//if(emxx)
				var hauteurTemp = Math.round(document.getElementById('flir').offsetHeight);
			
			//console.log(hauteurTemp);
			//alert(aux.style.height);
			//alert(aux.getElementsByTagName('img')[0].style.filter);
			var hauteurTotale =60;
			
			var margeHaute =  hauteurTotale - hauteurTemp;
			var hauteurDuEm = hauteurTotale - (margeHaute/2);
			document.getElementById('flir').style.height = hauteurDuEm+'px';
			document.getElementById('flir').style.paddingTop = (margeHaute/2)+'px';
			document.getElementById('enGlobe').style.visibility="visible";
			if(document.getElementById('desCription')){
				document.getElementById('flir').style.width=document.getElementById('flir').offsetWidth+"px";
				//document.getElementById('enGlobe').style.paddingLeft = (document.getElementById('enGlobe').offsetWidth - largeur_temp) / 2 + 'px';
			}
		}
	}
	function centerTxt()
	{
		if(document.getElementById("flir"))
		{
			var largeur = document.getElementById("flir").offsetWidth;
			if(document.getElementById("flir").getElementsByTagName("img").length > 0)
				document.getElementById("flir").getElementsByTagName("img")[0].style.marginLeft = (largeur - document.getElementById("flir").getElementsByTagName("img")[0].offsetWidth) /2 +'px';
		}
		
	}
/*Ouverture popups*/
function openPopup(url,label,attrs) {window.open(url,label,attrs); return false;}
/*Ouverture popups depuis flash*/
function openPopupFlash(url,label,attrs) {window.open(url,label,attrs);}
/*Ouvertures catalogues*/
function openCatalogue(urlID, winTitle) {window.open(urlID, winTitle, 'height=700, width=875, top=10, left=20, toolbar=no, menubar=no, location=no, resizable=no, scrollbars=no, status=no');}
function openCatalogue2(urlID, winTitle) {window.open(urlID, winTitle, 'height=670, width=875, top=10, left=20, toolbar=no, menubar=no, location=no, resizable=no, scrollbars=no, status=no');}
/*Ouverture fenetre ami depuis catalogue*/
function openPopUpAmi(urlID) {window.open(urlID, 'ami','top=0,left=100,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,width=500,height=450');}
/*Ouverture fenetre minisite nooel2007*/
function openNoel2007MiniSite() {window.open("http://www.toysrus.fr/actualite/noel2007-minisite/index.html", 'Noel2007','height=575, width=795, top=10, left=20, toolbar=no, menubar=no, location=no, resizable=no, scrollbars=no, status=no');  }
function openNoel2007Lettres() {window.open("http://www.toysrus.fr/actualite/noel2007-lettres/index.html", 'Noel2007lettres','height=470, width=400, top=10, left=20, toolbar=no, menubar=no, location=no, resizable=no, scrollbars=no, status=no');  }