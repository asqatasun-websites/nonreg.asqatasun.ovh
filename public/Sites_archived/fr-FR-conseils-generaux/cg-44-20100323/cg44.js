/* Fonction d'ajout d'evenement */
function cg44_addEventLst(EventTarget,type,listener,useCapture) {
  useCapture = typeof(useCapture)=="boolean"?useCapture:false;
  if (EventTarget.addEventListener) {
   EventTarget.addEventListener(type, listener, useCapture);
  } else if ((EventTarget==window) && document.addEventListener) {
   document.addEventListener(type, listener, useCapture);
  } else if (EventTarget.attachEvent) {
   EventTarget["e"+type+listener] = listener;
   EventTarget[type+listener] = function() {EventTarget["e"+type+listener]( window.event );}
   EventTarget.attachEvent("on"+type, EventTarget[type+listener]);
  } else {
   EventTarget["on"+type] = listener;
  }
}

/* Fonctions du pager */
var im = new Image();
im.src = "css/portal/images/boutonPrecedent_over.gif";
im.src = "css/portal/images/boutonSuivant_over.gif";

function pagerPrevOn(img) {
  if (img) img.src = "css/portal/images/boutonPrecedent_over.gif";
}

function pagerPrevOut(img) {
  if (img) img.src = "css/portal/images/boutonPrecedent.gif";
}

function pagerSuivOn(img) {
  if (img) img.src = "css/portal/images/boutonSuivant_over.gif";
}

function pagerSuivOut(img) {
  if (img) img.src = "css/portal/images/boutonSuivant.gif";
}

/* Fonctions cg en 1 clic */

function afficherLienIdentite() {
  if (document.getElementById('lien1clic')) document.getElementById('lien1clic').style.display= 'inline';
}

/* Fonctions AUTRE */

function findPos(obj) {
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		curleft = obj.offsetLeft
		curtop = obj.offsetTop
		while (obj = obj.offsetParent) {
			curleft += obj.offsetLeft
			curtop += obj.offsetTop
		}
	}
	return [curleft,curtop];
}

function afficheId(baliseId) {

	if (baliseId == "canton") {
		document.getElementById("itemCanton").className="visible";

		//on cache les autres
		document.getElementById("itemCommune").className="invisible";
		document.getElementById("itemElu").className="invisible";
	}
	else if (baliseId == "commune") {
		document.getElementById("itemCommune").className="visible";

		//on cache les autres
		document.getElementById("itemCanton").className="invisible";
		document.getElementById("itemElu").className="invisible";
	}
	else if (baliseId == "elu") {
		document.getElementById("itemElu").className="visible";

		//on cache les autres
		document.getElementById("itemCanton").className="invisible";
		document.getElementById("itemCommune").className="invisible";
	}
	redirection('');
}

function redirection(choix) {
	document.forms["formlieu"].idChoix.value = choix;
}

function affichage(baseUrl) {
	if(document.forms["formlieu"].idChoix.value == '') {
		alert('Vous devez s�lectionner un �l�ment dans la liste');
	}
	else {
		document.location.href=baseUrl + 'display.jsp?id='  + document.forms["formlieu"].idChoix.value;
	}
}

function positionnebasdepage() {
	pos_y_menugauche = findPos(document.getElementById('basmenugauche'))[1]
	pos_y_basdecontenu = findPos(document.getElementById('basdecontenu'))[1]
	if (pos_y_menugauche > pos_y_basdecontenu) {
		document.getElementById('basdecontenu').style.marginTop = pos_y_menugauche - pos_y_basdecontenu + "px";
	}
}