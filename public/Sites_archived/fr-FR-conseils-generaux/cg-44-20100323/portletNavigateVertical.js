// ---------------------------------------
// Vertical Menu Functions |||||||||||||||
// ---------------------------------------

var expandMenu = false;

function vMenuOver(obj, iconOpen, iconClosed) {
  expandMenu = true;
  icon = obj.className=="open" ? iconClosed : iconOpen;
  obj.src = icon;
}
function vMenuOut(obj, iconOpen, iconClosed) {
  expandMenu = false;
  icon = obj.className=="open" ? iconOpen : iconClosed;
  obj.src = icon;
}

// Expand menu only if the mouse is over an expand icon
function vMenuLink(obj, iconOpen, iconClosed) {
  if (expandMenu == true) {
    toggleMenu(obj, iconOpen, iconClosed);
    return false;
  }
}

// Toggle the menu visibility and change the expand icon
function toggleMenu(obj, iconOpen, iconClosed) {
  if (obj.parentNode.lastChild.className=="open") {
    obj.parentNode.lastChild.className = obj.firstChild.className = "close";
    obj.firstChild.src = iconOpen;
  } else {
    obj.parentNode.lastChild.className = obj.firstChild.className = "open";
    obj.firstChild.src = iconClosed;
  }
}