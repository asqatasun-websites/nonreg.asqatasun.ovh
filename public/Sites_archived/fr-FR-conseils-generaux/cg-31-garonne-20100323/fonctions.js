function addEvent(elm, evType, fn, useCapture){if(elm.addEventListener){elm.addEventListener(evType, fn, useCapture);return true;}else if (elm.attachEvent){var r = elm.attachEvent('on' + evType, fn);return r;}else{elm['on' + evType] = fn;}} 

function ExecAJAX () 
{

	//premier parametre = nom du script serveur
	
	var args = ExecAJAX.arguments;
	var erreur = 0;
	var script = args[0];
	
	var xhr_object1 = null;   
    if(window.XMLHttpRequest) xhr_object1 = new XMLHttpRequest();   // Firefox     
	else if(window.ActiveXObject) xhr_object1 = new ActiveXObject("Microsoft.XMLHTTP");   // Internet Explorer  
	else return;   // XMLHttpRequest non supporte par le navigateur   

	xhr_object1.open("POST", script, true);   
    
	xhr_object1.onreadystatechange = function() { if(xhr_object1.readyState == 4) eval(xhr_object1.responseText); }   
		
	xhr_object1.setRequestHeader("Content-type", "application/x-www-form-urlencoded");   

	var data = "";
	
	for (i=1;i<args.length;i++)
	{
		if (i>1) data = data + "&";
		data = data + "param" + i + "=" + escape(args[i]);		
		
	}
	
	//data = "param1=valider&param2=6&param3=test%2024"
	
	xhr_object1.send(data);  
}

//ouvre une fenetre fille retaillable    	
function new_browser(src,name,w,h)
{
	size=",width="+w+",height="+h
	browser=window.open(src,name,"resizable=yes,scrollbars=yes,toolbar=no,status=no,menubar=no,location=0,directories=no"+size)
}


//ouvre une fenetre fille de taille fixe   
function new_browser3(src,name,w,h)
{
	size=",width="+w+",height="+h
	browser=window.open(src,name,"resizable=no,scrollbars=no,toolbar=no,status=no,menubar=no,location=0,directories=no"+size)
}	


//ouvre une photo dans une fenetre fille ala taille de la photo
function fenetrephoto2 (repimages, lo, ha, nomimage,id)
{	
	browser=window.open("visualisation.asp?image="+repimages+"&id="+id, nomimage,"resizable=no,scrollbars=auto,toolbar=no,status=no,menubar=no,location=0,directories=no,width="+lo+",height="+ha)
}
function ChangeClass(id)
	{
		ident = document.getElementById(id);
		identClass = document.getElementById(id).className;
	
		if (identClass.indexOf("_over",0)>0)
		{
			str = identClass.substring(0,identClass.indexOf("_over",0))
			
			ident.className=str;
		}
		else
		{
			ident.className=identClass+"_over";
		}
	}

function OuvreTout(nomclasse)
	{
		var touslesouvre = getElementsByClass(nomclasse);
		for(i=0;i<touslesouvre.length;i++)
			{
				touslesouvre[i].style.display = "block";
			}
	}
	
function OuvreFerme(param, nomclasse, reponse,paramLien)
	{
		//On identifie l'element clique
		objDiv = document.getElementById(param);
		objLien = document.getElementById(paramLien);
		//s'il est deja ouvert on ne fait que le fermer (a condition qu'il soit le seul ouvert)
		if(objDiv.style.display == "block"){
			var touslesouvert = getElementsByClass(nomclasse);
			var touslesLiensouvert =  document.getElementsByName("Question");
			var CptOuvert = 0;
			//on teste s'il est le seul a etre ouvert
			for(i=0;i<touslesouvert.length;i++)
				{
					if (touslesouvert[i].style.display == "block"){
						CptOuvert ++;
					}
				}			
			//s'il y en a 1 seul d'ouvert on le ferme
			if(CptOuvert == 1){
				objDiv.style.display = "none";
				objLien.className = "Question_close";
				
			}
			//s'ils sont tous ouverts on laisse celui clique ouvert et ferme les autres
			if(CptOuvert > 1){
				for(i=0;i<touslesouvert.length;i++)
					{
						touslesouvert[i].style.display = "none";
						touslesLiensouvert[i].className = "Question_close";
						
					}
				objDiv.style.display = "block";
				objLien.className = "Question_open";
			}
		}
		//si c'est un autre qui est ouvert on ferme tout et on ouvre celui clique
		else{
			//astuce pour que ie reconnaisse l'element (ie connait pas getElementsByName)
			var touslesferme = getElementsByClass(nomclasse);
			var touslesLiensferme = document.getElementsByName("Question");
			for(i=0;i<touslesferme.length;i++)
				{
					if(touslesferme[i].style.display=="block")
					{
						touslesferme[i].style.display = "none";
						touslesLiensferme[i].className = "Question_close";
						
					}
				}
			//on ouvre seulement s'il y a une reponse
			if(reponse == 1){
				objDiv.style.display = "block";
				objLien.className = "Question_open";
			}
		}
	}

function getElementsByClass(maClass) {
      var tabRetour = new Array();
      var tabTmp = new Array();
      tabTmp = document.getElementsByTagName("div");
      j=0;
       for (i=0; i<tabTmp.length; i++) {
           if (tabTmp[i].className==maClass) {
             tabRetour[j]=tabTmp[i];
         j++;
           }
        }
       return tabRetour;
    }

