/***********************
 * Cr�� le 31/07/2008 par S�bastien Ang�le
 * Modification :
 *
 */

var CompteSite = Class.create();

CompteSite.prototype = Object.extend(new Compte(),{
	cookiedomaine:'newexpress',	
	//Initialisation des �v�nements sur les formulaires et les boutons
	initialize: function()
	{
		//Bouton pour faire apparaitre le calque de login
		if(null!=$(this.btn_identification))
		{
			this.initBtnIdentification();
		}
		
		//Bouton pour faire apparaitre le calque de login
		this.initBtnDeconnexion();

//Bouton pour faire apparaitre le calque d'inscription
		if(null!=$(this.btn_preinscription))
		{
			this.initBtnPreinscription();
		}									
		
		//Deuxi�me formulaire d'inscription (page d'inscription)
		var form_inscription = $("form_inscription");
		if(null!=form_inscription)
		{
			this.initFormInscription(form_inscription);
		}
		
		//Formulaire de modification
		var form_modification=$('form_modification_email');
		if(null!=form_modification)
		{
			this.initFormModification(form_modification,false);
		}
		var form_modification=$('form_modification_mdp');
		if(null!=form_modification)
		{
			this.initFormModification(form_modification,true);
		}
		
		//formulaire d'envoi � un ami
		var form_amis=$('form_compte_amis');
		if(null!=form_amis)
		{
			this.initFormAmis(form_amis);
		}
		
		var form_pleinepage = $('form_identification2');
		if(null!=form_pleinepage )
		{
			this.initFormIdentificationPleinePage(form_pleinepage);
		}
		
		
	},	
	inscriptionSite: function(compte,form,etat)
	{				
	
		new Ajax.Request("/includes/ajax/compte_site.asp",
		{
			method:'post',
			postBody:serializeForm(form),
			encoding:form.readAttribute('accept-charset'),
			onSuccess:function(req)
			{														
					window.location = compte.url_inscription;					
			},
			onFaillure:function (req)
			{
				alert('Erreur serveur.!!!');
			}
		});				
	},
	modificationSite: function(compte,form,etat)
	{					
		new Ajax.Request("/includes/ajax/compte_site.asp",
		{
			method:'post',
			postBody:serializeForm(form),
			encoding:form.readAttribute('accept-charset'),
			onSuccess:function(req)
			{																			
       				if(form['subaction'].value=='1')
				{	
					$("compte_modification_email_ok").show();					
					$("compte_modification_password_ok").hide();					
				}
				else
				{
					$("compte_modification_email_ok").hide();
					$("compte_modification_password_ok").show();					
				}
			},
			onFaillure:function (req)
			{
				alert('Erreur serveur.!!!');
			}
		});				
	}
})

var DelayedEventCompte;
var insCompte;

DelayedEventCompte={
	handler:null,
	load:function(event){
		Event.stop(event);
		insCompte = new CompteSite();		
	}
}
DelayedEventCompte.handler = DelayedEventCompte.load.bindAsEventListener(DelayedEventCompte)
Event.observe(window,'load',DelayedEventCompte.handler,false);