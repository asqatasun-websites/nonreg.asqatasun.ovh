function KenBurnsFader( img, windowSize )
{
  this.img = img;
  this.windowSize = windowSize;
}

KenBurnsFader.prototype.apply = function( percent )
{
  var opacity = 100;

  if ( percent <= this.windowSize )
    opacity = ( percent / this.windowSize ) * 100;
  else if ( percent >= ( 100 - this.windowSize ) )
    opacity = ( ( 100 - percent ) / this.windowSize ) * 100;

  this.img.set_opacity( opacity );
}

function KenBurnsZoomer( img, start, end, cw, ch )
{
  this.start = start;
  this.end = end;
  this.img = img;

  var wr = this.img.width / cw;
  var nw = this.img.width * wr; 
  var nh = this.img.height * wr; 

  this.sw = ( nw * ( this.start / 100 ) );
  this.ew = ( nw * ( this.end / 100 ) );
  this.sh = ( nh * ( this.start / 100 ) );
  this.eh = ( nh * ( this.end / 100 ) );
  this.dw = ( this.ew - this.sw ) / 100;
  this.dh = ( this.eh - this.sh ) / 100;
}

KenBurnsZoomer.prototype.apply = function( percent )
{
  this.img.set_size(
    this.sw + ( this.dw * percent ),
    this.sh + ( this.dh * percent ) );
}

function KenBurnsMover( img, sx, sy, ex, ey, cw, ch )
{
  this.img = img;
  this.sx = sx / 100;
  this.ex = ex / 100;
  this.sy = sy / 100;
  this.ey = ey / 100;
  this.cw = cw;
  this.ch = ch;
  this.wr = this.img.width / this.cw;
}

KenBurnsMover.prototype.apply = function( percent )
{
  var nw = this.img.current_width * this.wr;
  var nh = this.img.current_height * this.wr;

  var cntw = ( ( this.cw / 2 ) - ( nw / 2 ) );
  var cnth = ( ( this.ch / 2 ) - ( nh / 2 ) );

  var sx = ( nw * this.sx );
  var ex = ( nw * this.ex );
  var sy = ( nh * this.sy );
  var ey = ( nh * this.ey );
  var dx = ( ex - sx ) / 100;
  var dy = ( ey - sy ) / 100;
  var x = cntw + sx + ( dx * percent );
  var y = cntw + sy + ( dy * percent );

  this.img.set_position( x, y );
}

function Animation( am, img, seconds, effects )
{
  this.img = img;
  this.animationManager = am;
  this.seconds = seconds;
  this.effects = effects;
  this.startMS = 0;
}
