// +----------------------------------------------------------------------+
// | CMS V5.0.0
// | Auteur: jeremy@eolas.fr - (Inspiration Harmen Christophe)
// | Action: Statistiques XiTi - Marqueur de lien externe et document
// +----------------------------------------------------------------------+

function xt_addEventLst(EventTarget,type,listener,useCapture) {
	useCapture = typeof(useCapture)=="boolean"?useCapture:false;
	if (EventTarget.addEventListener) {
		EventTarget.addEventListener(type, listener, useCapture);
	} else if ((EventTarget==window) && document.addEventListener) {
		document.addEventListener(type, listener, useCapture);
	} else if (EventTarget.attachEvent) {
		EventTarget["e"+type+listener] = listener;
		EventTarget[type+listener] = function() {EventTarget["e"+type+listener]( window.event );}
		EventTarget.attachEvent("on"+type, EventTarget[type+listener]);
	} else {
		EventTarget["on"+type] = listener;
	}
}

function xt_hasClassName(oNode, className) {
	return (oNode.nodeType==1)?
		((" "+oNode.className+" ").indexOf(" "+className+" ")!=-1):false;
}

function xt_getInnerText(oNode) {
	try {
		if (oNode.tagName == 'IMG') return oNode.getAttributeNode("ALT").value;
		if (typeof(oNode.textContent)!="undefined") {return oNode.textContent;}
		switch (oNode.nodeType) {
			case 3: // TEXT_NODE
			case 4: // CDATA_SECTION_NODE
				return oNode.nodeValue;
				break;
			case 7: // PROCESSING_INSTRUCTION_NODE
			case 8: // COMMENT_NODE
				if (getTextContent.caller!=getTextContent) {
					return oNode.nodeValue;
				}
				break;
			case 9: // DOCUMENT_NODE
			case 10: // DOCUMENT_TYPE_NODE
			case 12: // NOTATION_NODE
				return null;
				break;
		}
		
		var _textContent = "";
		oNode = oNode.firstChild;
		while (oNode) {
			_textContent += xt_getInnerText(oNode);
			oNode = oNode.nextSibling;
		}
	} catch(e) {
		xt_afficherInfo('xt_getInnerText - ' + e.description);
	}
	return _textContent;
}

function xt_afficherInfo(txt) {
	xt_txt = txt;
 	var tid = setInterval(xt_statuswriter, 1000);
}
function xt_statuswriter() {
	window.status = xt_txt;
}

function xt_initialisation() {
	try {
		var oAs = document.getElementsByTagName("A");
		for (i=0;oAs[i];i++)  {
			xt_addEventLst(oAs[i], 'click',
				function() {
					try {
						ahref = this.href;
						aRel = this.rel;
						aText = '';
						if(aRel == '') {
   						if (ahref.substr(0,11)!= 'javascript:' && ahref.substr(0,7)!= 'mailto:') {
   							if (this.getAttributeNode("TITLE")) aText = this.getAttributeNode("TITLE").value;
   							if (xt_trim(aText) == '') aText = xt_getInnerText(this);
   							aText = xt_trim(aText).replace(/[^A-Za-z0-9_~\\\/\-]+/g, '_');
   							if (xt_hasClassName(this, 'document')) {
									xt_med('C', xtn2, aText, 'T');
   							} else if (xt_hasClassName(this, 'external')) {
   								xt_med('C', xtn2, ahref, 'S');
   							}
   							return true;
   						}
						}
					} catch (e) {
						xt_afficherInfo('xt_aclick - ' + e.description);
					}
				});
		}
	} catch(e) {
		xt_afficherInfo('xt_initialisation - ' + e.description);
	}
}

function xt_trim(str) {
	try {
		str = str.replace(/(^[\s:*]+)|([\s:*]+$)/g,'');
	} catch (e) {
		str = '';
		xt_afficherInfo('xt_trim - ' + e.description);
	}
	return str;
}

var xt_txt = '';
xt_addEventLst(window,'load',xt_initialisation);
