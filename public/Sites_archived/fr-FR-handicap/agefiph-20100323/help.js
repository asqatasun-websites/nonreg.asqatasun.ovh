////////////////////////////////////////////////////////////////////////////////////////////////////////
var IE = document.all?true:false;
var tempX;
var tempY;

if (!IE) {document.captureEvents(Event.MOUSEMOVE);}
document.onmousemove = getMouseXY;

function getMouseXY(e) {

if (!e) e = window.event;

  if (e)
  {
    if (e.pageX || e.pageY)
    {
      tempX = e.pageX;
      tempY = e.pageY;
    }
    else if (e.clientX || e.clientY)
    {
      tempX = e.clientX + document.documentElement.scrollLeft;
      tempY = e.clientY + document.documentElement.scrollTop;
    }
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
function help(id,w,hideSelect,obj) {
	var popup = $(id);
    if (!popup) return;
	popup.className = "popupStatus";

	popup.setStyles({
		width: w+"px"
	});
	var temp_y = $(obj).getPosition().y - 100;	
	temp_y = Math.min(temp_y, $("container").getCoordinates().height-popup.getCoordinates().height-200);
	popup.setStyles({
		top: temp_y+"px"
	});	
    if(hideSelect == true){
        selects = document.getElementsByTagName("select");
	    for (i = 0; i != selects.length; i++) {
	        selects[i].style.visibility = "hidden";
	    }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////
function closeHelp (id) {
    var popup = document.getElementById(id);
	if (popup == null) return;
	popup.className = "popupStatusClosed";
    selects = document.getElementsByTagName("select");
    for (i = 0; i != selects.length; i++) {
        selects[i].style.visibility = "visible";
    }
}