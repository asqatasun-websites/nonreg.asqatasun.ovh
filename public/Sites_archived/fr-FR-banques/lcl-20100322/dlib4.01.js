//   Default file for dhtml library.
//  This file, provide basical tools and must be loaded before any other extentions.

// config
cfg_default_path="../v_1.0/js/dlib4/";
cfg_default_rollover_extension=["_on.", "_off."]; // set default rollOver extension

// overide config by setting config variables in document object
if(document.cfg_default_path) cfg_default_path=document.cfg_default_path;
if(document.cfg_default_rollover_extension) cfg_default_rollover_extension=document.cfg_default_rollover_extension; // change rollOver extensions

// variables
	// navigators type
	__IE__  = false;
	__NS__  = false;
	__DOM__ = false;
	__KONQUEROR__=false;
	__SAFARI__=false;

	// plateform type
	__WIN__=false;
	__MAC__=false;
	__LINUX__=false;
	__OTHER__=false;

	// true plugins are loaded
	__LOADED_NAV_PLUGIN__=false;

	// true when debug mode is on
	__DEBUG_ALLOWED__=false;
// /variables

// Determine which navigator is used
if(typeof(__CURRENT_NAVIGATOR__)=="undefined")
{
        //test for konqueror
	if(window.clientInformation && window.clientInformation.appName=="Konqueror"){
		__KONQUEROR__=true;
		__CURRENT_NAVIGATOR__="__KONQUEROR__";
	}
        //test fo Safari
	else if(window.clientInformation && window.clientInformation.appVersion.indexOf("Safari")>-1){
		__SAFARI__=true;
		__CURRENT_NAVIGATOR__="__SAFARI__";
	}
	// test for IE
	else if(document.all){
		 __IE__ = true;
		__CURRENT_NAVIGATOR__ = "__IE__";
	}
	// test for NS < 6 (4.7x)
	else if (document.layers){
		 __NS__ = true;
		__CURRENT_NAVIGATOR__ = "__NS__";
	}
	// test for DOM compliant (NS 6+, opera 6+, etc.)
	else {
		 __DOM__ = true;
		__CURRENT_NAVIGATOR__ = "__DOM__";
	}
	// !!! ERROR !!! Navigator not supported
	//else alert("dhtml lib:\nError: unsupported Navigator!");
};

// Determine on which platefome we are
if(typeof(__CURRENT_PLATEFORME__)=="undefined")
{
	// test for Windows
	if(navigator.appVersion.indexOf('Windows')>-1){
		__WIN__ = true;
		__CURRENT_PLATEFORME__= "__WIN__";
	}
	// test for Machintosh
	else if(navigator.appVersion.indexOf('Mac')>-1){
		__MAC__ = true;
		__CURRENT_PLATEFORME__ = "__MAC__";
	}
	// test for Linux
	else if (navigator.appVersion.indexOf('Linux')>-1 || navigator.appVersion.indexOf('X11')>-1){
		__LINUX__ = true;
		__CURRENT_PLATEFORME__ = "__LINUX__";
	}
	else {
		__OTHER__=true;
		__CURRENT_PLATEFORME__ = "__OTHER__";
	}
};

// tools that help you getting var sended with get method
__dlib_get=[];
__dlib_getVarsName=[];
if(self.location.search){
	_tmpget=self.location.search.substring(1).split("&");
	for (i=0; i<_tmpget.length; i++){
		_tmpValues=_tmpget[i].split("=");
		__dlib_get[unescape(_tmpValues[0])]=unescape(_tmpValues[1]);
		__dlib_getVarsName[__dlib_getVarsName.length]=unescape(_tmpValues[0]);
	}
};

// HTML object type
__LAYER__="layer";
__IMAGE__="image";
__ANCHOR__="anchor";
__FORM__="form";
__DOCUMENT__="document";
// /HTML object type

//defined properties
HEIGHT="height";
WIDTH="width";
//defined properties

// privat functions

	//abstract functions
	        // return HTML object
		function __dlib_get_element(strObjectName, strObjectType){
			if(__DEBUG_ALLOWED__) dlib_debugLog("<b>!!! WARNING !!!</b>", false, false, __ERRORS__[16], true);
			return false;
		}

		// send a message to the debugger.
		//function dlib_debugMessage(strMessage, isError){}

	// object relative functions
	function __dlib_get_object(strObjectName, strObjectType, oObjectArgs){
		oObject=__dlib_get_element(strObjectName, strObjectType);		
		if(!oObject) {
			if(__DEBUG_ALLOWED__) dlib_debugLog(strObjectName, strObjectType, false, __ERRORS__[0], true);
			return false;
		}                
	    
		if(typeof oObjectArgs=="undefined" || typeof oObjectArgs[1]=="undefined") return oObject;
					    
		strPropertyName=oObjectArgs[1];
		if(__DLIB_FUNC__[strObjectType]){		    
			if(__DLIB_FUNC__[strObjectType][strPropertyName]){			   
				if(typeof oObjectArgs[2]=="undefined"){						            
					if(__DLIB_FUNC__[strObjectType][strPropertyName]["GET"]) {
						eval("returnedValue="+__DLIB_FUNC__[strObjectType][strPropertyName]["GET"][0]+"(oObject)");
						if(__DEBUG_ALLOWED__) dlib_debugLog(strObjectName, strObjectType, strPropertyName,  returnedValue, false);
						
						return returnedValue;
					}
					else if(__DEBUG_ALLOWED__) dlib_debugLog(strObjectName, strObjectType, false, strPropertyName+" :"+__ERRORS__[13] , true);
				}
				else {
					if(__DLIB_FUNC__[strObjectType][strPropertyName]["SET"]){
						eval("isValidParam="+__DLIB_FUNC__[strObjectType][strPropertyName]["SET"][1][2]+"(oObjectArgs[2]);");
						if(!isValidParam){
							if(__DEBUG_ALLOWED__) dlib_debugLog(strObjectName, strObjectType, strPropertyName, __ERRORS__[15]+" <b>"+ __DLIB_FUNC__[strObjectType][strPropertyName]["SET"][1][intParamNum][0]+"</b> must be a "+__DLIB_FUNC__[strObjectType][strPropertyName]["SET"][1][intParamNum][1], true);
							return false;
						}
						eval("returnedValue="+__DLIB_FUNC__[strObjectType][strPropertyName]["SET"][0]+"(oObject, oObjectArgs[2])")
						if(typeof returnedValue!="undefined"){
							if(__DEBUG_ALLOWED__) dlib_debugLog(strObjectName, strObjectType, strPropertyName, returnedValue, false);
							return returnedValue;
						}
						return true;
					}
					else if(__DEBUG_ALLOWED__) dlib_debugLog(strObjectName, strObjectType, strPropertyName, __ERRORS__[12] , true);
				}
			}
			else if(__DEBUG_ALLOWED__) dlib_debugLog(strObjectName, strObjectType, false, __ERRORS__[11]+strPropertyName , true);
		}
		else if(__DEBUG_ALLOWED__) dlib_debugLog(strObjectName, strObjectType, false, __ERRORS__[10] , true);
		return false;
	};
// /privat functions

// public functions
	// basical functions
		// return variable value sended in get method
		function dlib_get(strvarname){
			return (__dlib_get[strvarname])? __dlib_get[strvarname]:false;
		};

		// function that return true when a sended parameter is an undefined variable
		function isUndefined(Var){
			return (Var!=0 && !Var || typeof Var=="undefined")?true:false;
		};

		// function that return true when a sended parameter is an object
		function isObject(Object){
			return (typeof Object=="object")? true:false;
		};

		// function that return true when a sended parameter is a string
		function isString(strstr){
			return (typeof strstr=="string")? true:false;
		};

		// function that return true when a sended parameter is a number
		function  isNumber(intint){
			return (typeof intint=="number")? true:false;
		};

		// function that return a numeric value when a sended parameter has a numeric value
		function  hasNumericValue(strstr){
			return (!isNaN(parseInt(strstr)))? parseInt(strstr):false;
		};

		// function that return true when a sended parameter is a boolean
		function  isBoolean(intBool){
			return (intBool==true || intBool==false || isNumber(intBool))? true:false;
		};

		// function that always return true :)
		function returnTrueValue(){ return true; }

		// function that let you increment numerical value when sending a value
		function add(intint){
			return (arguments.length)?(isNumber(intint))?intint+"add":intint:0;
		};

		// useful only function for layers clip method. Allow you not to give every clip values. Just fill params with keep function when you
		//   do not need to change a value
		function keep(){ return "1k";};

	// tools function
		//function that define a dlib function parameter
		function dlib_define_param(strParamName, strParamType){
			switch(strParamType.toLowerCase()){
				case "integer":
					strParamTypeFunc="isInteger";
					strParamType="integer";
					break;
				case "object":
					strParamTypeFunc="isObject";
					strParamType="object";
					break;
				case "boolean":
					strParamTypeFunc="isBoolean";
					strParamType="boolean";
					break;
				case "array":
					strParamTypeFunc="isObject";
					strParamType="array";
					break;
				case "string":
					strParamTypeFunc="isString";
					strParamType="string";
					break;
				default:
					strParamTypeFunc="returnTrueValue";
					strParamType="untyped";
			}
			return Array(strParamName, strParamType, strParamTypeFunc);
		}

		// function that declare a dlib fonction to set or get a property value
		// strFuncName -> function to be called
		// strFuncAct -> GET or SET as string
		// strObjectType -> type of Object
		// strPropertyName -> property name that will be sent by user to call this function
		// FunctionParameterDefinition -> parameter properties. Use dlib_define_param to set parameter property.
		__DLIB_FUNC__=[];
		function dlib_declare_func(strFuncName, strObjectType, strPropertyName /*, FunctionParameterDefinition*/){
			if(!__DLIB_FUNC__[strObjectType]) __DLIB_FUNC__[strObjectType]=[];
			if(!__DLIB_FUNC__[strObjectType][strPropertyName]) __DLIB_FUNC__[strObjectType][strPropertyName]=[];
			if(typeof arguments[3]=="undefined" || arguments.length<4) __DLIB_FUNC__[strObjectType][strPropertyName]["GET"]=[strFuncName];
			else __DLIB_FUNC__[strObjectType][strPropertyName]["SET"]=[strFuncName, arguments[3]];
			return true;
		}

		// function that loads required libs
		function load_required_lib(strFilePath, strFileName){
			if(__DEBUG_ALLOWED__) dlib_debugLog("dhtml library v4", false, false, "loading : "+strFilePath+strFileName , false);
			if(arguments.length>1) document.write("<script language=\"javascript\" src=\""+strFilePath+strFileName+"\"> </script>");
			return true;
		};

		// function that loads dlib4 extensions
		function dlib_load_extension(strExtName){
			strExtName=strExtName.toLowerCase();			
			load_required_lib(cfg_default_path+strExtName+"/", "dlib4_"+strExtName+"_ext.js");
			return true;
		};

		__DEBUG_LOADER__=0;
		function dlib_debug(){
			dlib_load_extension("debug");
			__dlib_check_and_active_debug();
			return true;
		};

		function __dlib_check_and_active_debug(){
			if(typeof __DEBUGGER_IS_LOADED__!="undefined"){
				dlib_debug();
				return true;
			}
			if(__DEBUG_LOADER__<10){
				__DEBUG_LOADER__++;
				setTimeout("__dlib_check_and_active_debug()", 200);
			}
			else alert("cannot load and activate dlib4 debug extension !\n\n:( ");
			return false;
		};

		// function that return platforme name or false is strPlateforme is sended is not equal to __CURRENT_PLATEFORME__
		function dlib_plateforme(strPlateforme){
			if (arguments.length && strPlateforme.toUpperCase()!=__CURRENT_PLATEFORME__) return 0;
			return __CURRENT_PLATEFORME__;
		};

		// function that return navigator name or false is strNavigator is sended is not equal to __CURRENT_NAVIGATOR__
		function dlib_navigator (strNavigator){
			if (arguments.length && strNavigator.toUpperCase()!=__CURRENT_NAVIGATOR__) return 0;
			return __CURRENT_NAVIGATOR__;
		};

		function dlib_screenLeft(intValue){
			if(arguments.length && isInteger(intValue)) return __dlib_setScreenLeft((intValue.toString().indexOf("add")>-1)?parseInt(intValue)+parseInt(__dlib_getScreenLeft()) :parseInt(intValue));
			return __dlib_getScreenLeft();
		};

		function dlib_screenTop(intValue){
   			if(arguments.length && isInteger(intValue)) return __dlib_setScreenTop((intValue.toString().indexOf("add")>-1)?parseInt(intValue)+parseInt(dlib.getScreenTop()) :parseInt(intValue));
			return __dlib_getScreenTop();
		};

	// object relative functions

	// set or get layer object property
	function dlib_layer_exists(strLayerName){ return __dlib_get_element(strLayerName, __LAYER__);};
	function dlib_layers(strLayerName /*, strPropertyName, mixedValue */){
		if(typeof arguments[0]=="undefined") return false;
		return __dlib_get_object(strLayerName, __LAYER__, arguments);
	};

	// set or get image object property
	function dlib_image_exists(strImageName){ return __dlib_get_element(strImageName, __IMAGE__);};
	function dlib_images(strImageName /*, strPropertyName, mixedValue */){
		if(typeof arguments[0]=="undefined") return false;
		return __dlib_get_object(strImageName, __IMAGE__, arguments);
	};

	// set or get anchor object property
	function dlib_anchor_exists(strAnchorName){ return __dlib_get_element(strAnchorName, __ANCHOR__);};
	function dlib_anchors(strAnchorName /*, strPropertyName, mixedValue */){
		if(typeof arguments[0]=="undefined") return false;						
		return __dlib_get_object(strAnchorName, __ANCHOR__, arguments);
	};

	// set or get form object property and childs properties
	function dlib_form_exists(strFormName){ return __dlib_get_element(strFormName, __FORM__);};
	function dlib_forms(strFormName /*, strPropertyName, mixedValue */){
		if(typeof arguments[0]=="undefined") return false;
		return __dlib_get_object(strFormName, __FORM__, arguments);
	};
// /public functions


// images private functions
dlib_declare_func("__dlib_get_image_height", __IMAGE__, HEIGHT);
function __dlib_get_image_height(oObject){ return oObject.height;};

dlib_declare_func("__dlib_get_image_width", __IMAGE__, WIDTH);
function __dlib_get_image_width(oObject){ return oObject.width; };

// images public functions
ROLLOVER="rollover";
dlib_declare_func("__dlib_image_rollover", __IMAGE__, ROLLOVER, dlib_define_param("intPos", "boolean"));
function __dlib_image_rollover(oObject, intPos){
        if(__arr_dlib_images_rollover_src[__dlib_getName(oObject)]) oImage.src=(intPos)?__arr_dlib_images_rollover_src[__dlib_getName(oObject)][0]:__arr_dlib_images_rollover_src[__dlib_getName(oObject)][1];
	else oObject.src=(intPos)?oObject.src.replace(new RegExp(cfg_default_rollover_extension[1]),cfg_default_rollover_extension[0]):oObject.src.replace(new RegExp(cfg_default_rollover_extension[0]),cfg_default_rollover_extension[1])
	return oObject.src;
};

__arr_dlib_images_rollover_src=[];
function dlib_images_setRollOver(strImgName, strImgSrcOn, strImgSrcOff){
	dlib.preloadImg(strImgSrcOn, strImgSrcOff);
	__arr_dlib_images_rollover_src[strImgName]=[];
	__arr_dlib_images_rollover_src[strImgName]=[strImgSrcOn,strImgSrcOff];
};

__arr_dlib_images_preload=[];
function dlib_images_preload(){
	for(intImgSrc=0; intImgSrc<arguments.length; intImgSrc++){
                count=__arr_dlib_images_preload.length;
		__arr_dlib_images_preload[count]=new Image();
		__arr_dlib_images_preload[count].src=arguments[intImgSrc];
	}
	return true;
};

// loading needed plugin
load_required_lib(cfg_default_path+"plugins/", __CURRENT_NAVIGATOR__+".js");
dlib_load_extension("layers");
dlib_load_extension("events");
		