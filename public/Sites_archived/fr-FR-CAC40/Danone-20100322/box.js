/*
 * Gestion des ?v?nements
 * ajout/suppression/suppression de tous (en unload pour ?viter les fuites m?moires)
 * d'apr?s les travaux de Dean Edwards, Matthias Miller, John Resig, Rob Cherny...
 * "Remise en forme" + ajouts : Manuel Catez
 */

var Box = {};

Box.events = (function() {
	var guid = 1;
	var observers = [];
	function handle(event) {
		var returnValue = true;
		event = event || fix(window.event);
		var handlers = this.events[event.type];
		for(var i in handlers) {
			this.$$handle = handlers[i];
			if(this.$$handle(event) === false) {returnValue = false;}
		}
		return returnValue;
	};
	function fix(event) {
		event.preventDefault = function() {this.returnValue = false;};
		event.stopPropagation = function() {this.cancelBubble = true;};
		return event;
	};
	var onload = [];
	function loaded() {
		if(arguments.callee.done) {return;}
		arguments.callee.done = true;
		for(var i = 0; i < onload.length; i++) {onload[i]();}
	};
	return {
		add: function(element, type, handler) {
			if(element.addEventListener) {
				element.addEventListener(type, handler, false);
			} else {
				if(!handler.$$guid) {handler.$$guid = guid++;}
				if(!element.events) {element.events = {};}
				var handlers = element.events[type];
				if(!handlers) {
					handlers = element.events[type] = {};
					if(element["on" + type]) {handlers[0] = element["on" + type];}
				}
				handlers[handler.$$guid] = handler;
				element["on" + type] = handle;
			}
			observers[observers.length] = {element: element, type: type, handler: handler};
		},
		remove: function(element, type, handler) {
			if (element.removeEventListener) {
				element.removeEventListener(type, handler, false);
			} else {
				if (element.events && element.events[type]) {
					delete element.events[type][handler.$$guid];
				}
			}
		},
		clean: function() {
			for(var i = 0, oi; oi = observers[i]; i++) {
				Box.events.remove(oi.element, oi.type, oi.handler);
			}
			observers = null;
			onload = null;
		},
		load: function(fireThis) {
			onload[onload.length] = fireThis;
			if(document.addEventListener) {document.addEventListener('DOMContentLoaded', loaded, null);}
			if(/KHTML|WebKit/i.test(navigator.userAgent)) { 
				var _timer = setInterval(function() {
					if(/loaded|complete/.test(document.readyState)) {
						clearInterval(_timer);
						delete _timer;
						loaded();
					}
				}, 10);
			}
			/*@cc_on @*/
			/*@if (@_win32)
			var proto = "src='javascript:void(0)'";
			if(location.protocol == "https:") proto = "src=//0";
			document.write("<scr"+"ipt id=__ie_onload defer " + proto + "><\/scr"+"ipt>");
			var script = document.getElementById("__ie_onload");
			script.onreadystatechange = function() {if(this.readyState == "complete") {loaded();}};
			/*@end @*/
			window.onload = loaded;
		}
	};
})();

// Suppression des ?v?nements en unload (fuites m?moire))
Box.events.add(window, 'unload', Box.events.clean);

// transfert
if(!$E) {var $E = Box.events;}

/*
 * R?cup?ration d'?l?ments
 * ? la fa?on d'un s?lecteur CSS *simple*
 * ?ventuellement contraint par un contexte
 * (contexte = ?l?ment unique ou 1er ?l?ment d'une collection)
 * renvoit toujours un tableau ou collection d'?l?ments
 * Auteur : Manuel Catez
 */

Box.nodes = (function() {
	var expr = {
		'tagIdClasses': /^([a-z0-9]+)?(#)?([a-z0-9_-]+)?(\.)?([\.a-z0-9_-]+)?/i,
		'attributes': /\[[^\]]+\]/g,
		'pseudo': /(:)([^:#\.\[\]]+)$/,
		'=': function(a, v) {
			return a == v;
		},
		'!=': function(a, v) {
			return a != v;
		},
		'~=': function(a, v) {
			a = ' '+a+' '; v = ' '+v+' ';
			return a.indexOf(a) != -1;
		},
		'|=': function(a, v) {
			v = v+'-';
			return a.substr(0, v.length) == v;
		},
		'^=': function(a, v) {
			return a.substr(0, v.length) == v;
		},
		'$=': function(a, v) {
			return a.substr(a.length-v.length) == v;
		},
		'*=': function(a, v) {
			return a.indexOf(v) != -1;
		}
	};
	function byTag(tag, context) {
		return tag == '*' && document.all ? context.all : context.getElementsByTagName(tag);
	};
	function byId(id) {
		return document.getElementById(id);
	};
	function isTag(n, tag) {
		return n.nodeName.toLowerCase() == tag;
	};
	function isNode(n, s) {
		var f = s.match(expr['tagIdClasses']);
		var a = s.match(expr['attributes']) || [];
		if(f[2] == '#') {
			if(Box.nodes.attribute(n, 'id') != f[3]) {return false;}
		}
		if(f[1]) {
			if(!isTag(n, f[1])) {return false;}
		}
		if(f[5]) {
			var v = f[5].split('.');
			if(!Box.nodes.hasClass(n, v)) {return false;}
		}
		if(a.length && !hasAttribute(n, a)) {return false;}
		return true;
	};
	function hasAttribute(n, a) {
		var m, verif = true;
		for(var i = 0, ai; ai = a[i]; i++) {
			m = ai.match(/([a-z]+)([!~|*^$]?=?)([^\]]*)/);
			if(!m[1]) {verif = false;}
			var v = Box.nodes.attribute(n, m[1]);
			if(!v) {return false;} 
			verif = m[1] && !m[2] ? !!v : expr[m[2]](v, m[3]);
			if(!verif) {return false;}
		}
		return true;
	};
	function isPseudo(n, p, m) {
		if(p == 'first-child') {
			return Box.nodes.prev(n) ? false : true;
		} else if(p == 'last-child') {
			return Box.nodes.next(n) ? false : true;
		} else if(p == 'even' || p == 'nth-of-type(even)') {
			return nth(n, 2, 0, true);
		} else if(p == 'odd' || p == 'nth-of-type(odd)') {
			return nth(n, 2, 1, true);
		} else if(p.indexOf('nth-child') != -1 || p.indexOf('nth-of-type') != -1) {
			var m = p.match(/^[^(]+\(([^)]+)\)$/);
			if(m[1]) {
				if(m[1].indexOf('n') == -1) {
					m[1] = parseInt(m[1]);
					m[0] = 0;
				} else {
					m = m[1].match(/[0-9-]+/g);
					m[0] = m[0] == '-' ? -1 : parseInt(m[0]);
					m[1] = !m[1] ? 0 : parseInt(m[1]);
				}
				return p.indexOf('nth-child') != -1 ? nth(n, m[0], m[1]) : nth(n, m[0], m[1], true);
			}
		}
		return false;
	};
	function nth(n, coeff, start, type) {
		if((coeff == 1 && start == 1) || (coeff == 1 && start == 0)) {return true;}
		var p = (n[0] || n).parentNode.childNodes;
		if(!p.length) {return false;}
		var c = 0, i = 0, pi;
		for(var i = 0, pi; pi = p[i]; i++) {
			if(pi.nodeType != 1 || (type && pi.nodeName != n.nodeName)) {continue;}
			c++;
			if(coeff < 0) {
				if(c <= start && pi == n && (start-c)%(-coeff) == 0) {return true;}
			} else {
				if(c >= start && pi == n && (c == start || (c-start)%coeff == 0)) {return true;}	
			}
		}
		return false;
	};
	function getNodes(str, context) {
		var r1, r2 = [], m = null;
		var f = str.match(expr['tagIdClasses']);
		var a = str.match(expr['attributes']) || [];
		var p = str.match(expr['pseudo']) || [];
		if(f[2] == '#') {
			r1 = byId(f[3]);
			if(!r1 || (f[1] && !isTag(r1, f[1]))) {return [];}
			r1 = [r1];
		} else {
			r1 = byTag(f[1] || '*', context);
		}
		if(!r1.length || (!f[5] && !a.length && !p.length)) {return r1;}
		if(f[5]) {var v = f[5].split('.');}
		for(var i = 0, ri; ri = r1[i]; i++) {
			if(v && !Box.nodes.hasClass(ri, v)) {continue;}
			if(a.length && !hasAttribute(ri, a)) {continue;}
			if(p.length && !isPseudo(ri, p[2])) {continue;}
			r2[r2.length] = ri;
		}
		return r2;
	};
	function create(s, callbacks) {
		var r = [], i = 0, ni;
		var div = document.createElement('div');
		div.innerHTML = s;
		while(ni = div.childNodes[i]) {
			if(callbacks) {assign(ni, callbacks);}
			r[r.length] = ni;
			i++;
		}
		div = null;
		return r;
	};
	function assign(n, events) {
		if(isNode(n, events.tag)) {
			for(var c in events) {
				if(c != 'tag') {Box.events.add(n, c, events[c]);}
			}
		} else {
			var o = Box.nodes.get(events.tag, [n]);
			for(var i = 0, oi; oi = o[i]; i++) {
				for(var c in events) {
					if(c != 'tag') {Box.events.add(oi, c, events[c]);}
				}
			}
		}
	};
	return {
		prev: function(n) {
			n = n.nodeName ? n : n[0];
			while((n = n.previousSibling) && n.nodeType != 1);
			return n;
		},
		next: function(n) {
			n = n.nodeName ? n : n[0]
			while((n = n.nextSibling) && n.nodeType != 1);
			return n;
		},
		get: function(str, context) {
			if(!context) {
				context = document;
			} else {
				context = !context[0] ? document : context[0];
			}
			return getNodes(str, context);
		},
		hasClass: function(n, v) {
			var a = ' '+n.className+' ';
			if(a == '  ') {return false;}
			if(v.constructor == String) {
				v = v.indexOf('.') != -1 ? v.split('.') : [v];
			}
			for(var i = 0, vi; vi = v[i]; i++) {
				if(a.indexOf(' '+vi+' ') == -1) {return false;}
			}
			return true;
		},
		addClass: function(n, v) {
			if(Box.nodes.hasClass(n, v)) {return false;}
			n.className += n.className ? ' '+v : v;
			return true;
		},
		removeClass: function(n, v) {
			if(!Box.nodes.hasClass(n, v)) {return false;}
			var r = n.className.indexOf(' '+v) != -1 ? ' '+v : v;
			n.className = n.className.replace(r, '');
			return true;
		},
		swapClass: function(n, v0, v1) {
			if(!Box.nodes.hasClass(n, v0)) {return false;}
			n.className = n.className.replace(v0, v1);
			return true;
		},
		attribute: function(n, a, v) {
			if(!a || a.constructor != String) {return '';}
			if(!v) {
				var v;
				if(a == 'class') {
					v = n.className;
				} else if(a == 'for') {
					v = n.htmlFor;
				} else if(a == 'href' || a == 'src' || a == 'action') {
					v = n.getAttribute(a, 2);
				} else {
					v = n.getAttribute(a);
				}
			} else {
				a == 'class' ? Box.nodes.addClass(v) : n.setAttribute(a, v);
			}
			return v;
		},
		insert: function(s, cible, where, events) {
			var n = create(s, events);
			var p, t, i = 0, ni;
			if(where == 'after') {
				p = cible.parentNode;
				t = Box.nodes.next(cible);
				if(!t) {
					while(ni = n[i]) {p.appendChild(ni); i++;}
				} else {
					while(ni = n[i]) {p.insertBefore(ni, t); i++;}
				}
			} else {
				if(where == 'first') {
					p = cible.firstChild;
					while(ni = n[i]) {cible.insertBefore(ni, p); i++;}
				} else if(where == 'last') {
					while(ni = n[i]) {cible.appendChild(ni); i++;}
				} else if(where == 'before') {
					p = cible.parentNode;
					while(ni = n[i]) {p.insertBefore(ni, cible); i++;}
				}
			}
			n = null; p = null; t = null; cible = null;
		},
		exe: function(t, fn) {
			for(var i = 0, ti; ti = t[i]; i++) {
				fn.call(ti);
			}
		},
		findPos: function(o) {
			var left = 0, top = 0;
			if(o.offsetParent) {
				left = o.offsetLeft;
				top = o.offsetTop;
				while(o = o.offsetParent) {
					left += o.offsetLeft;
					top += o.offsetTop;
				}
			}
			return [left, top];
		}
	};
})();

// transferts
if(!$) {var $ = Box.nodes.get;}
if(!$N) {var $N = Box.nodes;}

/*
 * Gestion d'Ajax
 * D'apr?s John Resig et Jeremy Keith
 */

// Si IE, cr?er l'objet XMLHttpRequest
if(typeof XMLHttpRequest == 'undefined' && window.ActiveXObject) {
	XMLHttpRequest = function() {
		var xml = false;
		try {
			xml = new ActiveXObject("Msxml2.XMLHTTP");
		} catch(e) {
			try {
				xml = new ActiveXObject("Microsoft.XMLHTTP");
			} catch(e) {
				xml = false;
			}
		}
		return xml;
	};
}

Box.ajax = function(options) {
	options = {
		type: options.type || "POST",
		url: options.url || "",
		timeout: options.timeout || 5000,
		onComplete: options.onComplete || function(){},
		onError: options.onError || function(){},
		onSuccess: options.onSuccess || function(){},
		onSuccessDatas: options.onSuccessDatas || null,
		data: options.data || null
	};
	
	var xml = new XMLHttpRequest();
	
	xml.open(options.type, options.url, true);
	if(options.type == 'POST') {
		xml.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	}
	
	var timeoutLength = options.timeout;
	
	var requestDone = false;
	
	setTimeout(function(){
		requestDone = true;
	}, timeoutLength);
	
	xml.onreadystatechange = function(){
		if(xml.readyState == 4 && !requestDone) {
			if(httpSuccess(xml)) {
				options.onSuccess(httpData(xml, options.type), options.onSuccessDatas);
			} else {
				options.onError();
			}
			options.onComplete();
			xml = null;
		}
	};
	
	xml.send(options.data);
	
	function httpSuccess(r) {
		try {
			return !r.status && location.protocol == "file:" ||
			(r.status >= 200 && r.status < 300) ||
			r.status == 304 ||
			navigator.userAgent.indexOf("Safari") >= 0 && typeof r.status == "undefined";
		} catch(e) {}
		return false;
	}
	
	function httpData(r, type) {
		var ct = r.getResponseHeader("content-type");
		var data = null;
		data = ct.indexOf("xml") > -1 ? r.responseXML : r.responseText;
		if(ct.indexOf("script") > -1) {eval.call(window, data);}
		return data;
	}
};

// transfert
if(!$A) {var $A = Box.ajax;}
