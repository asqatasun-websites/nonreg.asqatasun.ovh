function lib_bwcheck(){ //Browsercheck (needed)
	this.ver=navigator.appVersion;
	this.agent=navigator.userAgent;
	this.dom=document.getElementById?1:0;
	this.opera5=(navigator.userAgent.indexOf("Opera")>-1 && document.getElementById)?1:0;
	this.ie5=(this.ver.indexOf("MSIE 5")>-1 && this.dom && !this.opera5)?1:0;
	this.ie6=(this.ver.indexOf("MSIE 6")>-1 && this.dom && !this.opera5)?1:0;
	this.ie7=(this.ver.indexOf("MSIE 7.0")>-1 && this.dom && !this.opera5)?1:0;
	this.ie4=(document.all && !this.dom && !this.opera5)?1:0;
	this.ie=this.ie4||this.ie5||this.ie6;
	this.mac=this.agent.indexOf("Mac")>-1;
	this.ns6=(this.dom && parseInt(this.ver) >= 5) ?1:0; 
	this.ns4=(document.layers && !this.dom)?1:0;
	this.bw=(this.ie7 || this.ie6 || this.ie5 || this.ie4 || this.ns4 || this.ns6 || this.opera5)
	return this
}
var bw = new lib_bwcheck()
/**************************************************************************
Variables to set.
***************************************************************************/
sLeft = 93;       //The left placement of the menu
sTop = 0;        //The top placement of the menu
sMenuheight = 73;  //The height of the menu
sArrowwidth = 0;  //Width of the arrows
sScrollspeed = 20; //Scroll speed: (in milliseconds, change this one and the next variable to change the speed)
sScrollPx = 8;     //Pixels to scroll per timeout.
sScrollExtra = 15; //Extra speed to scroll onmousedown (pixels)
/**************************************************************************
Scrolling functions
***************************************************************************/
var tim = 0;
var noScroll = true;
function mLeft(obj) {
   obj1 = obj;
   if (!noScroll && obj.x < sArrowwidth) {
       obj.moveBy(sScrollPx, 0);
       tim = setTimeout('mLeft(obj1)', sScrollspeed);
   }
}


function moveMatin(){
       if (document.getElementById('matin1')) oMenu1.moveIt(-document.getElementById('matin1').offsetLeft,null);
       if (document.getElementById('matin2')) oMenu2.moveIt(-document.getElementById('matin2').offsetLeft,null);
       if (document.getElementById('matin3')) oMenu3.moveIt(-document.getElementById('matin3').offsetLeft,null);
       if (document.getElementById('matin4')) oMenu4.moveIt(-document.getElementById('matin4').offsetLeft,null);
       if (document.getElementById('matin5')) oMenu5.moveIt(-document.getElementById('matin5').offsetLeft,null);
       if (document.getElementById('matin6')) oMenu6.moveIt(-document.getElementById('matin6').offsetLeft,null);
       if (document.getElementById('matin7')) oMenu7.moveIt(-document.getElementById('matin7').offsetLeft,null);

}

function moveMidi(){

       if (document.getElementById('12h1')) oMenu1.moveIt(-document.getElementById('12h1').offsetLeft,null);
       if (document.getElementById('12h2')) oMenu2.moveIt(-document.getElementById('12h2').offsetLeft,null);
       if (document.getElementById('12h3')) oMenu3.moveIt(-document.getElementById('12h3').offsetLeft,null);
       if (document.getElementById('12h4')) oMenu4.moveIt(-document.getElementById('12h4').offsetLeft,null);
       if (document.getElementById('12h5')) oMenu5.moveIt(-document.getElementById('12h5').offsetLeft,null);
       if (document.getElementById('12h6')) oMenu6.moveIt(-document.getElementById('12h6').offsetLeft,null);
       if (document.getElementById('12h7')) oMenu7.moveIt(-document.getElementById('12h7').offsetLeft,null);

}

function moveDebSoir(){

       if (document.getElementById('17h1')) oMenu1.moveIt(-document.getElementById('17h1').offsetLeft,null);
       if (document.getElementById('17h2')) oMenu2.moveIt(-document.getElementById('17h2').offsetLeft,null);
       if (document.getElementById('17h3')) oMenu3.moveIt(-document.getElementById('17h3').offsetLeft,null);
       if (document.getElementById('17h4')) oMenu4.moveIt(-document.getElementById('17h4').offsetLeft,null);
       if (document.getElementById('17h5')) oMenu5.moveIt(-document.getElementById('17h5').offsetLeft,null);
       if (document.getElementById('17h6')) oMenu6.moveIt(-document.getElementById('17h6').offsetLeft,null);
       if (document.getElementById('17h7')) oMenu7.moveIt(-document.getElementById('17h7').offsetLeft,null);

}
function moveSoir(){
       if (document.getElementById('soiree1')) oMenu1.moveIt(-document.getElementById('soiree1').offsetLeft,null);
       if (document.getElementById('soiree2')) oMenu2.moveIt(-document.getElementById('soiree2').offsetLeft,null);
       if (document.getElementById('soiree3')) oMenu3.moveIt(-document.getElementById('soiree3').offsetLeft,null);
       if (document.getElementById('soiree4')) oMenu4.moveIt(-document.getElementById('soiree4').offsetLeft,null);
       if (document.getElementById('soiree5')) oMenu5.moveIt(-document.getElementById('soiree5').offsetLeft,null);
       if (document.getElementById('soiree6')) oMenu6.moveIt(-document.getElementById('soiree6').offsetLeft,null);
       if (document.getElementById('soiree7')) oMenu7.moveIt(-document.getElementById('soiree7').offsetLeft,null);

}
function moveOne(myHour,divId){

       if (document.getElementById(myHour+'div'+divId)){
              if(divId==1){ 
                     oMenu1.moveIt(-document.getElementById(myHour+'div'+divId).offsetLeft,null);
              }
              if(divId==2){
                     oMenu2.moveIt(-document.getElementById(myHour+'div'+divId).offsetLeft,null);
              }
              if(divId==3){
                     oMenu3.moveIt(-document.getElementById(myHour+'div'+divId).offsetLeft,null);
              }
              if(divId==4){
                     oMenu4.moveIt(-document.getElementById(myHour+'div'+divId).offsetLeft,null);
              }
              if(divId==5){
                     oMenu5.moveIt(-document.getElementById(myHour+'div'+divId).offsetLeft,null);
              }
              if(divId==6){
                     oMenu6.moveIt(-document.getElementById(myHour+'div'+divId).offsetLeft,null);
              }
              if(divId==7){
                     oMenu7.moveIt(-document.getElementById(myHour+'div'+divId).offsetLeft,null);
              }
       }else{
              myHour = myHour -1;
              if (myHour<=9) myHour = '0'+myHour;
              if(divId==1){
                     oMenu1.moveIt(-document.getElementById(myHour+'div'+divId).offsetLeft,null);
              }
              if(divId==2){
                     oMenu2.moveIt(-document.getElementById(myHour+'div'+divId).offsetLeft,null);
              }
              if(divId==3){
                     oMenu3.moveIt(-document.getElementById(myHour+'div'+divId).offsetLeft,null);
              }
              if(divId==4){
                     oMenu4.moveIt(-document.getElementById(myHour+'div'+divId).offsetLeft,null);
              }
              if(divId==5){
                     oMenu5.moveIt(-document.getElementById(myHour+'div'+divId).offsetLeft,null);
              }
              if(divId==6){
                     oMenu6.moveIt(-document.getElementById(myHour+'div'+divId).offsetLeft,null);
              }
              if(divId==7){
                     oMenu7.moveIt(-document.getElementById(myHour+'div'+divId).offsetLeft,null);
              }
       }
}

function move(myHour){
      for (dayCounter=1;dayCounter<=7;dayCounter++)
              moveOne(myHour,dayCounter);
}


function mRight(object) {
    object1 = object;
    if (!noScroll && object.x > - (object.scrollWidth-(pageWidth)) - sArrowwidth) {
        object.moveBy(-sScrollPx, 0);
        tim = setTimeout('mRight(object1)', sScrollspeed);
    }
}
function noMove() {
	clearTimeout(tim);
	noScroll = true;
	sScrollPx = sScrollPxOriginal;
}

/**************************************************************************
Object part
***************************************************************************/
function makeObj(obj,nest,menu){
	nest = (!nest) ? "":'document.'+nest+'.';
	this.elm = bw.ns4?eval(nest+"document.layers." +obj):bw.ie4?document.all[obj]:document.getElementById(obj);
   	this.css = bw.ns4?this.elm:this.elm.style;
	this.scrollWidth = bw.ns4?this.css.document.width:this.elm.offsetWidth;
	this.x = bw.ns4?this.css.left:this.elm.offsetLeft;
	this.y = bw.ns4?this.css.top:this.elm.offsetTop;
	this.moveBy = b_moveBy;
	this.moveIt = b_moveIt;
	this.clipTo = b_clipTo;
	return this;
}
var px = bw.ns4||window.opera?"":"px";
function b_moveIt(x,y){
	if (x!=null){this.x=x; this.css.left=this.x+px;}
	if (y!=null){this.y=y; this.css.top=this.y+px;}
}
function b_moveBy(x,y){this.x=this.x+x; this.y=y; this.css.left=this.x+px; this.css.top=y+px;}

function b_clipTo(t,r,b,l){
	if(bw.ns4){this.css.clip.top=t; this.css.clip.right=r; this.css.clip.bottom=b; this.css.clip.left=l;}
	else this.css.clip="rect("+t+"px "+r+"px "+b+"px "+l+"px)";
}
/**************************************************************************
Object part end
***************************************************************************/
/**************************************************************************
Init function. Set the placements of the objects here.
***************************************************************************/
var sScrollPxOriginal = sScrollPx;
function sideInit(){
	//Width of the menu, Currently set to the width of the document.
	//If you want the menu to be 500px wide for instance, just 
	//set the pageWidth=500 in stead.
	//pageWidth = (bw.ns4 || bw.ns6 || window.opera)?innerWidth:document.body.clientWidth;
	pageWidth = 421;
	//Making the objects...
	oBg1 = new makeObj('divBg1');
	oBg2 = new makeObj('divBg2');
	oBg3 = new makeObj('divBg3');
	oBg4 = new makeObj('divBg4');
	oBg5 = new makeObj('divBg5');
	oBg6 = new makeObj('divBg6');
	oBg7 = new makeObj('divBg7');
	oMenu1 = new makeObj('divMenu1','divBg1', 1);
	oMenu2 = new makeObj('divMenu2','divBg2', 1);
	oMenu3 = new makeObj('divMenu3','divBg3', 1);
	oMenu4 = new makeObj('divMenu4','divBg4', 1);
	oMenu5 = new makeObj('divMenu5','divBg5', 1);
	oMenu6 = new makeObj('divMenu6','divBg6', 1);
	oMenu7 = new makeObj('divMenu7','divBg7', 1);
	oArrowRight1 = new makeObj('divArrowRight1','divBg1');
	oArrowRight2 = new makeObj('divArrowRight2','divBg2');
	oArrowRight3 = new makeObj('divArrowRight3','divBg3');
	oArrowRight4 = new makeObj('divArrowRight4','divBg4');
	oArrowRight5 = new makeObj('divArrowRight5','divBg5');
	oArrowRight6 = new makeObj('divArrowRight6','divBg6');
	oArrowRight7 = new makeObj('divArrowRight7','divBg7');
	//Placing the menucontainer, the layer with links, and the right arrow.
	oBg1.moveIt(sLeft,34); //Main div, holds all the other divs. (20 = sTop)
	oMenu1.moveIt(sArrowwidth,null);
	oBg2.moveIt(sLeft,105); 
	oMenu2.moveIt(sArrowwidth,null);
	oBg3.moveIt(sLeft,176); 
	oMenu3.moveIt(sArrowwidth,null);
	oBg4.moveIt(sLeft,247); 
	oMenu4.moveIt(sArrowwidth,null);
	oBg5.moveIt(sLeft,318); 
	oMenu5.moveIt(sArrowwidth,null);
	oBg6.moveIt(sLeft,389); 
	oMenu6.moveIt(sArrowwidth,null);
	oBg7.moveIt(sLeft,460); 
	oMenu7.moveIt(sArrowwidth,null);
	//Setting the width and the visible area of the links.
	//if (!bw.ns4) oBg1.css.overflow = "hidden";
	//if (bw.ns6) oMenu1.css.position = "relative";
	oBg1.css.width = pageWidth+px;
	oBg1.clipTo(0,pageWidth,sMenuheight,0);
	oBg1.css.visibility = "visible";
	oBg2.css.width = pageWidth+px;
	oBg2.clipTo(0,pageWidth,sMenuheight,0);
	oBg2.css.visibility = "visible";
	oBg3.css.width = pageWidth+px;
	oBg3.clipTo(0,pageWidth,sMenuheight,0);
	oBg3.css.visibility = "visible";
	oBg4.css.width = pageWidth+px;
	oBg4.clipTo(0,pageWidth,sMenuheight,0);
	oBg4.css.visibility = "visible";
	oBg5.css.width = pageWidth+px;
	oBg5.clipTo(0,pageWidth,sMenuheight,0);
	oBg5.css.visibility = "visible";
	oBg6.css.width = pageWidth+px;
	oBg6.clipTo(0,pageWidth,sMenuheight,0);
	oBg6.css.visibility = "visible";
	oBg7.css.width = pageWidth+px;
	oBg7.clipTo(0,pageWidth,sMenuheight,0);
	oBg7.css.visibility = "visible";

//move('10');
}
//executing the init function on pageload if the browser is ok.
var url = String(window.location);
if(url.indexOf("guide_tv") > -1){
if (bw.bw) onload = sideInit;
}
