<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<link href="vol.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--
Cette page est en charge de la g�n�ration du tableau d'affichage des horaires de vols au d�part et � l'arriv�e de l'a�roport de Fort de France
Chaque ligne du tableau est compos�e typiquement de 3 cellules :
	Destination ou provenance
	Num�ro de vol (compos� du code IATA et du num�ro de vol)
	Horaire pr�vue ou remarque
-->


<!--
D�termination du jour en fonction de la date du serveur.
Le jour sera affich� dans l'ent�te du tableau de pr�sentation des horaires
-->


<!--
D�termination du mois en fonction de la date du serveur.
Le mois sera affich� dans l'ent�te du tableau de pr�sentation des horaires
-->


<!--
V�rification des minutes pour affichage sur 2 caract�res (ex : 01 ou 12 et non 1 ou 12)
Les minutes seront affich�es dans l'ent�te du tableau de pr�sentation des horaires
-->


<!--
Chaine de connexion � la base de donn�es du site de l'a�roport de Fort de France
Chaine obtenue via la cr�ation d'un fichier UDL
-->


<!--
Cr�ation de la requ�te de r�cup�ration des vols au d�part
-->

													<table cellpadding="0" cellspacing="0" border="0" width="291" align="right">
														<tr>
															<td>
																<div id="vol">
																	<div class="date">Lundi&nbsp;22&nbsp;Mars&nbsp;2010&nbsp;|&nbsp;15h00</div>
																	<!--Lien vers la page de recherche des vols au d�part (doit avoir pour identifiant "departs"-->
																	<div class="titreDepart"><ul><li><a href="http://www.martinique.aeroport.fr/departs.asp" target="_top">Tous les d�parts</a></li></ul></div>
																	<div class="horaire">
																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
<!--Si fonce=0 alors ligne paire donc claire --><!--Si pas de remarque alors pas d'activit� donc texte en bleu -->
																				<tr  class="encours">
																					<td width="50%"><ul><li>BELEM               </li></ul></td>
																					<td width="20%">TX&nbsp;3261 </td>
																					<td width="30%">ENREGISTREMENT      </td>
																				</tr>
	<!--Si fonce=1 alors ligne impaire donc fonc�e --><!--Si fonce=0 alors ligne paire donc claire --><!--Si pas de remarque alors pas d'activit� donc texte en bleu -->
																				<tr class="fonce">
																					<td width="50%"><ul><li>CAYENNE             </li></ul></td>
																					<td width="20%">AF&nbsp;3974 </td>
																					<td width="30%">16h30</td>
																				</tr>
	<!--Sinon activit� en cours donc texte en rouge --><!--Si fonce=0 alors ligne paire donc claire --><!--Si pas de remarque alors pas d'activit� donc texte en bleu -->
																				<tr>
																					<td width="50%"><ul><li>ST LUCIE            </li></ul></td>
																					<td width="20%">TX&nbsp;303  </td>
																					<td width="30%">16h45</td>
																				</tr>
	<!--Sinon activit� en cours donc texte en rouge --><!--Si fonce=1 alors ligne impaire donc fonc�e --><!--Si fonce=0 alors ligne paire donc claire --><!--Si pas de remarque alors pas d'activit� donc texte en bleu -->
																				<tr  class="fonceEncours">
																					<td width="50%"><ul><li>PARIS ORLY          </li></ul></td>
																					<td width="20%">TX&nbsp;511  </td>
																					<td width="30%">PREVU A             </td>
																				</tr>
	<!--Si fonce=0 alors ligne paire donc claire --><!--Si pas de remarque alors pas d'activit� donc texte en bleu -->
																				<tr  class="encours">
																					<td width="50%"><ul><li>PARIS ORLY          </li></ul></td>
																					<td width="20%">SS&nbsp;868  </td>
																					<td width="30%">ENREGISTREMENT      </td>
																				</tr>
	<!--Si fonce=1 alors ligne impaire donc fonc�e --><!--Si fonce=0 alors ligne paire donc claire --><!--Si pas de remarque alors pas d'activit� donc texte en bleu -->
																				<tr class="fonce">
																					<td width="50%"><ul><li>POINTE-A-PITRE      </li></ul></td>
																					<td width="20%">TX&nbsp;414  </td>
																					<td width="30%">18h15</td>
																				</tr>
	<!--Sinon activit� en cours donc texte en rouge --><!--Si fonce=0 alors ligne paire donc claire --><!--Si pas de remarque alors pas d'activit� donc texte en bleu -->
																				<tr  class="encours">
																					<td width="50%"><ul><li>PARIS ORLY          </li></ul></td>
																					<td width="20%">AF&nbsp;653  </td>
																					<td width="30%">ENREGISTREMENT      </td>
																				</tr>
	<!--Si fonce=1 alors ligne impaire donc fonc�e -->
																			</table>
																		</div>
																		<!--Lien vers la page de recherche des vols � l'arriv�e (doit avoir pour identifiant "arrivee"-->
			  															<div class="titreArrivee"><ul><li><a href="http://www.martinique.aeroport.fr/arrivees.asp" target="_top">Toutes les arriv�es</a></li></ul></div>
																		<div class="horaire">
				
<!--
Cr�ation de la requ�te de r�cup�ration des vols � l'arriv�e
-->

																			<table cellpadding="0" cellspacing="0" border="0" width="100%">
<!--Si fonce=0 alors ligne paire donc claire --><!--Si pas de remarque alors pas d'activit� donc texte en bleu -->
																				<tr  class="encours">
																					<td width="50%"><ul><li>ST MARTIN JUL.      </li></ul></td>
																					<td width="20%">SS&nbsp;868  </td>
																					<td width="30%">PREVU A 15:45       </td>
																				</tr>
	<!--Si fonce=1 alors ligne impaire donc fonc�e --><!--Si fonce=0 alors ligne paire donc claire --><!--Si pas de remarque alors pas d'activit� donc texte en bleu -->
																				<tr  class="fonceEncours">
																					<td width="50%"><ul><li>PARIS ORLY          </li></ul></td>
																					<td width="20%">TX&nbsp;510  </td>
																					<td width="30%">PREVU A 16:10       </td>
																				</tr>
	<!--Si fonce=0 alors ligne paire donc claire --><!--Si pas de remarque alors pas d'activit� donc texte en bleu -->
																				<tr>
																					<td width="50%"><ul><li>POINTE-A-PITRE      </li></ul></td>
																					<td width="20%">AF&nbsp;3974 </td>
																					<td width="30%">15h40</td>
																				</tr>
	<!--Sinon activit� en cours donc texte en rouge --><!--Si fonce=1 alors ligne impaire donc fonc�e --><!--Si fonce=0 alors ligne paire donc claire --><!--Si pas de remarque alors pas d'activit� donc texte en bleu -->
																				<tr class="fonce">
																					<td width="50%"><ul><li>POINTE-A-PITRE      </li></ul></td>
																					<td width="20%">TX&nbsp;303  </td>
																					<td width="30%">16h30</td>
																				</tr>
	<!--Sinon activit� en cours donc texte en rouge --><!--Si fonce=0 alors ligne paire donc claire --><!--Si pas de remarque alors pas d'activit� donc texte en bleu -->
																				<tr  class="encours">
																					<td width="50%"><ul><li>PARIS ORLY          </li></ul></td>
																					<td width="20%">AF&nbsp;652  </td>
																					<td width="30%">PREVU A 17:10       </td>
																				</tr>
	<!--Si fonce=1 alors ligne impaire donc fonc�e --><!--Si fonce=0 alors ligne paire donc claire --><!--Si pas de remarque alors pas d'activit� donc texte en bleu -->
																				<tr class="fonce">
																					<td width="50%"><ul><li>ST LUCIE            </li></ul></td>
																					<td width="20%">TX&nbsp;414  </td>
																					<td width="30%">17h45</td>
																				</tr>
	<!--Sinon activit� en cours donc texte en rouge --><!--Si fonce=0 alors ligne paire donc claire --><!--Si pas de remarque alors pas d'activit� donc texte en bleu -->
																				<tr>
																					<td width="50%"><ul><li>POINTE-A-PITRE      </li></ul></td>
																					<td width="20%">3S&nbsp;901  </td>
																					<td width="30%">18h45</td>
																				</tr>
	<!--Sinon activit� en cours donc texte en rouge --><!--Si fonce=1 alors ligne impaire donc fonc�e -->
																			</table>
																		</div>
																		<div class="tousVol"><img src="vol_18.gif" border="0"><a href="http://www.martinique.aeroport.fr/touslesvols.asp" target="_top">Tous les vols</a></div>
																	</div>
																</td>
															</tr>
														</table>

</body>
</html>
