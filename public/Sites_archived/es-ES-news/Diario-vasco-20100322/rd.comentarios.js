/***************************** Comentarios **********************************/

var ids_noticias = '';

function getNumComentariosPortada(id) {

	ids_noticias = (ids_noticias != '') ? ids_noticias + '|' + id : id;

	if (window.document.getElementById('num-comentarios-' + id)) window.document.getElementById('num-comentarios-' + id).innerHTML = 'Comenta esta noticia';

 }

 function setNumComentariosPortada(id, v) {

 	v = typeof(v) != 'undefined' ? v : '';

 	var dir;

 	if (id) {
 		var http = createRequestObject();

 		dir = '/backend/comentarios' + v + '/getComentariosPortada.php?id=' + id + '&by=date';

                    http.open('get', dir);
                    http.onreadystatechange = function () {
   				 if(http.readyState == 4){
                            	if (http.responseText) {
                                            var campos = http.responseText.split('|');
                                            var limite = campos.length;
                                            var i = limite - 1;
                                            do {
                                            	campos2 = campos[i].split('@');

                                                   	var num = campos2[1];

                                                    switch (num) {
                                                    	case '0':
                                                    		if (window.document.getElementById('num-comentarios-' + campos2[0])) window.document.getElementById('num-comentarios-' + campos2[0]).innerHTML = 'Comenta esta noticia';
                                                    		break;
                                                    	case '1':
                                                    		if (window.document.getElementById('num-comentarios-' + campos2[0])) window.document.getElementById('num-comentarios-' + campos2[0]).innerHTML = '<strong>' + num + '</strong> Comentario';
                                                    		break;
                                                    	default:
                                                    		if (window.document.getElementById('num-comentarios-' + campos2[0])) window.document.getElementById('num-comentarios-' + campos2[0]).innerHTML = '<strong>' + num + '</strong> Comentarios';
                                                    		break;
                                                    }
                                            }
                                           while (i--);

                                    }
                            }
                    };
                    http.send(null);
 	}
 }

   	function getNumComentarios(id) {
	if (id) {
		var http = createRequestObject();
		//http.open("get", "/backend/comentarios/getComentarios.php?id=" + id + "&by=date&r=" + Math.random());
		http.open("get", "/backend/comentarios/getComentarios.php?id=" + id + "&by=date");
		http.onreadystatechange = function () {

			if(http.readyState == 4){
				if (http.responseXML) {
					if (http.responseText.indexOf('comments') != -1) {
					//if (http.responseXML.getElementsByTagName('comments')) {
						var num = http.responseXML.getElementsByTagName('total_comments')[0].firstChild.data;
						window.document.getElementById('num-comentarios').innerHTML = '(' + num + ')';
					}

				}
			}
		};
		http.send(null);
	}
}


function getComentarios(id, by, v, a) {

	v = typeof(v) != 'undefined' ? v : '';
	a = typeof(a) != 'undefined' ? a : 1;

	var dir;

	if (id) {
		var http = createRequestObject();

		dir = '/backend/comentarios' + v + '/getComentarios.php?id=' + id + '&by=' + by + (a ? '' : '&r=' + Math.random());

		http.open('get', dir);
		http.onreadystatechange = function () {

			if(http.readyState == 4){
				if (http.responseXML) {
					if (http.responseText.indexOf('comments') != -1) {

						eval('muestraComentarios' + v + '(http.responseXML)');

						window.document.getElementById('lomasreciente').className = (by == 'date') ? 'activo' : '';
						window.document.getElementById('lomasvalorado').className = (by == 'votes') ? 'activo' : '';
					}

				}
			}
		};
		http.send(null);
	}
}



function muestraComentarios(response) {

	var contenido;
	var html;
	var fecha_load = new Date();
	var browser = navigator.appName;
	var positive_votes;
	var negative_votes;

	var limite = response.getElementsByTagName('comment').length;
	html = '<div class="numopiniones"><span>' + response.getElementsByTagName('total_comments')[0].firstChild.data + ' OPINIONES</span></div>';

	limite_b = (limite > 5) ? 5 : limite;

	for (i = 0; i < limite_b; i++) {

		id = response.getElementsByTagName('id')[i].firstChild.data;
		fecha = response.getElementsByTagName('date')[i].firstChild.data;
		campos = fecha.split(' ');
		hora = campos[3];
		nombre = response.getElementsByTagName('name')[i].firstChild ? response.getElementsByTagName('name')[i].firstChild.data : '';
		if (browser == 'Microsoft Internet Explorer'){
			texto = response.getElementsByTagName('content')[i].firstChild.data;
		}
		else {
			texto = response.getElementsByTagName('content')[i].childNodes[1].data;
		}

		positive_votes = response.getElementsByTagName('positive_votes')[i].firstChild.data;
		negative_votes = response.getElementsByTagName('negative_votes')[i].firstChild.data;

		fecha = campos[2] + "/" + campos[1] + "/" + campos[5];

		contenido = '	<div class="opinion">';
              	contenido += '<div class="fecha">' + fecha + ' | <span class="hora">' + hora + '</span></div>';
   		contenido += '<h3>' + unescape(nombre) + '</h3>';
              	contenido += '<div class="texto">' + unescape(texto) + '</div>';
              	contenido += '<div class="detalles-opinion">';
                contenido += '<div class="favorcontra" id="favorcontra-' + id + '"><a href="javascript:votarComentario(' + id+ ', 1);" class="afavor">A';
                contenido += '    favor (' + positive_votes + ')</a><a href="javascript:votarComentario(' + id + ', 2);" class="encontra">En';
                contenido += '    contra (' + negative_votes + ')</a></div>';
                contenido += '<div class="inadecuado" id="inadecuadoboton"><a href="javascript:mostrarDenunciar(' + id + ');" class="boton" title="Comentario inadecuado"><img src="/img/ico_inadecuado.gif" alt="Comentario inadecuado" />Comentario';
                contenido += '    inadecuado</a></div>';
                contenido += '<div class="clear"></div>';
              	contenido += '</div>';
              	contenido += '<div class="formdenunciar" id="formdenunciar-' + id + '" style="display:none;">';
              	contenido += '<form name="formulario_' + id + '" action="/backend/COMENTARIOS.denunciarComentario.php" method="post">';
                contenido += '<input type="hidden" name="load_time" value="' + fecha_load.getTime() + '">';
		contenido += '<input type="hidden" name="submit_time" value="0">';
		contenido += '<input type="hidden" name="id" value="' + id + '">';
		contenido += '<input type="hidden" name="anchor" value="comentarios">';
                contenido += '<p>Por favor selecciona el motivo por el que crees que este comentario';
                contenido += '  es inadecuado </p>';
                contenido += '<textarea name="denuncia_' + id + '">Escriba aquí el motivo de la denuncia.</textarea>';
                contenido += '<a href="javascript:denunciarComentario(' + id + ');" class="boton">denunciar comentario</a>';
                contenido += '</label>';
                contenido += '<div class="clear"></div>';
              	contenido += '</div>';
              	contenido += '<div class="clear"></div>';
              	contenido += '</form>';
            	contenido += '</div>';

            	html += contenido;
        	}

        	window.document.getElementById('listado-comentarios').innerHTML = html;

        	verMas(limite);

}

function muestraComentarios2(response, pagina) {
	var contenido;
	var html;
	var fecha_load = new Date();
	var browser = navigator.appName;
	var positive_votes;
	var negative_votes;
	var max_comentarios = 5;

	pagina = typeof(pagina) != 'undefined' ? pagina : 1;

	var limite = response.getElementsByTagName('comment').length;
	html = '<div class="numopiniones"><span>' + response.getElementsByTagName('total_comments')[0].firstChild.data + ' OPINIONES</span></div>';

	if (window.document.getElementById('num-comentarios')) {
		window.document.getElementById('num-comentarios').innerHTML = '(' + response.getElementsByTagName('total_comments')[0].firstChild.data + ')';
	}

	var limite_b = ((((pagina - 1) * max_comentarios) + max_comentarios) > limite) ? limite : (((pagina - 1) * max_comentarios) + max_comentarios);
	var inicio = (pagina - 1) * max_comentarios;
	var paginas = Math.ceil(limite / max_comentarios);

	var pagina_ver = (pagina < 10) ? '0' + pagina : pagina;
	var paginas_ver = (paginas < 10) ? '0' + paginas : paginas;

	for (var i = inicio; i < limite_b; i++) {

		var id = response.getElementsByTagName('id')[i].firstChild.data;

		var fecha = response.getElementsByTagName('date')[i].firstChild.data;
		var campos = fecha.split(' ');
		var hora = campos[3];

		fecha = campos[2] + "/" + campos[1] + "/" + campos[5];

		/*
		var fecha = response.getElementsByTagName('date')[i].firstChild.data;
		var hora = fecha.substr(8,2) + ':' + fecha.substr(10,2)
		*/
		var nombre = response.getElementsByTagName('name')[i].firstChild ? response.getElementsByTagName('name')[i].firstChild.data : '';
		if (browser == 'Microsoft Internet Explorer'){
			var texto = response.getElementsByTagName('content')[i].firstChild.data;
		}
		else {
			var texto = response.getElementsByTagName('content')[i].childNodes[1].data;
		}

		positive_votes = response.getElementsByTagName('positive_votes')[i].firstChild.data;
		negative_votes = response.getElementsByTagName('negative_votes')[i].firstChild.data;
		//fecha = fecha.substr(6,2) + "/" + fecha.substr(4,2) + "/" + fecha.substr(0,4);
		contenido = '	<div class="opinion">';
		contenido += '<div class="fecha">' + fecha + ' | <span class="hora">' + hora + '</span></div>';
   		contenido += '<h3>' + unescape(nombre) + '</h3>';
		contenido += '<div class="texto">' + unescape(texto) + '</div>';
		contenido += '<div class="detalles-opinion">';
		contenido += '<div class="favorcontra" id="favorcontra-' + id + '"><a href="javascript:votarComentario(' + id+ ', 1, 2);" class="afavor">A';
		contenido += '    favor (' + positive_votes + ')</a><a href="javascript:votarComentario(' + id + ', 2, 2);" class="encontra">En';
		contenido += '    contra (' + negative_votes + ')</a></div>';
		contenido += '<div class="inadecuado" id="inadecuadoboton"><a href="javascript:mostrarDenunciar(' + id + ');" class="boton" title="Comentario inadecuado"><img src="/img/ico_inadecuado.gif" alt="Comentario inadecuado" />Comentario';
		contenido += '    inadecuado</a></div>';
		contenido += '<div class="clear"></div>';
		contenido += '</div>';
		contenido += '<div class="formdenunciar" id="formdenunciar-' + id + '" style="display:none;">';
		contenido += '<form name="formulario_' + id + '" action="/backend/comentarios2/denunciarComentario.php" method="post">';
		contenido += '<input type="hidden" name="load_time" value="' + fecha_load.getTime() + '">';
		contenido += '<input type="hidden" name="submit_time" value="0">';
		contenido += '<input type="hidden" name="id" value="' + id + '">';
		contenido += '<input type="hidden" name="anchor" value="comentarios">';
		contenido += '<p>Por favor selecciona el motivo por el que crees que este comentario';
		contenido += '  es inadecuado </p>';
		contenido += '<textarea name="denuncia">Escriba aquí el motivo de la denuncia.</textarea>';
		contenido += '<a href="javascript:denunciarComentario(' + id + ');" class="boton">denunciar comentario</a>';
		contenido += '</label>';
		contenido += '<div class="clear"></div>';
		contenido += '</div>';
		contenido += '<div class="clear"></div>';
		contenido += '</form>';
		contenido += '</div>';

		html += contenido;
	}

	if (limite > max_comentarios) {
		html += '<div class="botones-listado"><a id="anterior" href="javascript:nada()" class="boton anterior">&lt;&lt;';
              	html += 'Anterior</a>&nbsp;&nbsp;<span><strong>' + pagina_ver + '</strong>/' + paginas_ver + '</span>&nbsp;&nbsp;<a id="siguiente" href="javascript:nada()" class="boton siguiente">Siguiente';
              	html += '&gt;&gt; </a></div>';
	}

	window.document.getElementById('listado-comentarios').innerHTML = html;


	if (limite > max_comentarios) {

		window.document.getElementById('anterior').onclick = function () {
			if (pagina != 1) {
				muestraComentarios2(response, (pagina - 1));
			}
			else {
				muestraComentarios2(response, pagina);
			}

		}

		window.document.getElementById('siguiente').onclick = function () {
			if (pagina != paginas) {
				muestraComentarios2(response, (pagina + 1));
			}
			else {
				muestraComentarios2(response, pagina);
			}

		}
	}

}

function nada() {
	return;
}


function desconectar() {
	window.location = '/backend/desconectar.php?url=' + window.location.href;

}

function conectar() {

	var direccion;

	if (window.document.location.href.indexOf('?') > 0) {
		var campos = window.document.location.href.split('?');
		direccion = campos[0];
	}
	else {
		direccion = window.document.location.href;
	}

	var qs = 'url=' + direccion;
	qs += '&email=' + window.document.logueo.email.value;
	qs += '&contrasena=' + window.document.logueo.contrasena.value;

	window.location = '/backend/conectar.php?' + qs;
}

function enviaComentario() {
  var fecha_submit = new Date();
      var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

  if (window.document.formulario.author_name.value == '') {
  	alert('Por favor, introduce tu nombre');
  	window.document.formulario.author_name.focus();
  	return;
  }

  if (window.document.formulario.author_email.value == '') {
  	alert('Por favor, introduce tu email');
  	window.document.formulario.author_email.focus();
  	return;
  }

  if (!filter.test(window.document.formulario.author_email.value)) {
	alert('El email introducido no es válido');
	 window.document.formulario.author_email.focus();
            return;

  }

  if (window.document.formulario.message.value == '') {
  	alert('Por favor, introduce tu opinión');
  	window.document.formulario.message.focus();
  	return;
  }

  if (!window.document.formulario.acepto.checked) {
  	alert('Tienes que aceptar las condiciones de uso');
  	return;
  }

  var registrado = getCookie('registrado') ? ' (Usuario registrado)' : '';

  window.document.formulario.message.value = window.document.formulario.message.value;
  window.document.formulario.author_name.value = window.document.formulario.author_name.value + registrado;

  window.document.formulario.submit_time.value = fecha_submit.getTime();
  window.document.formulario.submit();
  alert('Gracias por escribir un comentario');
}

function votarComentario(id, tipo, v) {

	v = typeof(v) != 'undefined' ? v : '';

	var dir;

	if (id) {
		var http = createRequestObject();

		dir = '/backend/comentarios' + v + '/votarComentario.php?id=' + id + '&tipo=' + tipo + '&r=' + Math.random();

		http.open('get', dir);

		http.onreadystatechange = function () {
			if(http.readyState == 4){
				if (http.responseText) {
					var response = http.responseText;

					if (response.indexOf('ok')) {
						if (tipo == 1) {
							window.document.getElementById('favorcontra-' + id).innerHTML = '<span class="afavor">A favor</span> <span class="mensaje">| Gracias por votar</span>';
						}
						else {
							window.document.getElementById('favorcontra-' + id).innerHTML = '<span class="encontra">En contra</span> <span class="mensaje">| Gracias por votar</span>';
						}
					}

				}
			}
		};
		http.send(null);
	}
}

function mostrarDenunciar(id) {
	var display = window.document.getElementById('formdenunciar-' + id).style.display;
	/*
	if (display == 'none' && !getCookie('registrado')) {
		alert('Para denunciar un comentario Regístrate');
		return;
	}
	*/
	window.document.getElementById('formdenunciar-' + id).style.display = (display == 'none') ? 'block' : 'none';
}

function denunciarComentario(id) {
	var fecha_submit = new Date();
	eval('var formu = window.document.formulario_' + id);
	formu.submit_time.value = fecha_submit.getTime();
	formu.submit();
	 alert('Gracias por denunciar el comentario');

}

function muestraLogueo() {
	window.document.getElementById('artlogeo').style.display = '';
}

function borrarTexto() {
	if (window.document.formulario.message.value == 'escribe aquí tu comentario') {
		window.document.formulario.message.value = '';
	}
}

/***************************** FIN Comentarios **********************************/
