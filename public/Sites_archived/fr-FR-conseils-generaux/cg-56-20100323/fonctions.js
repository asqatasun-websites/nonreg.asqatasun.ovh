/*
	FONCTIONS COMMUNES
*/


	// Fonctions Communes de cookies
	// Ecriture
	function createCookie(name,value,days) {
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();
		}
		else var expires = "";
		document.cookie = name+"="+value+expires+"; path=/";
	}
	// Lecture
	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}
	// Suppression
	function eraseCookie(name) {
		createCookie(name,"",-1);
	}





	// Ajout aux favoris
	function fav() {
		 
		if ($.browser.msie) { 
			window.external.AddFavorite(location.href, document.title);
		} else {
			alert('Faîtes CTRL + D (ou pomme + D) pour ajouter cette page à vos favoris.')
		} 
	}
	
	// Modification de la taille de police de l’élément racine (#page)
	function fontSize(sensZoom) {
		maxSize = 12;
		
		var actualSize = $("#page").css('font-size');
		var actualSize = Number(actualSize.replace('px',''));

		if (sensZoom == 'in' && actualSize < maxSize) {
			var newSize = actualSize + 1 + "px";
			$("#page").css('font-size', newSize);
			// document.cookie = 'fSize='+newSize+'; expires=Thu, 2 Aug 2050 20:00:00 UTC; path=/'
			createCookie('fSize',newSize);
		}
		else if (sensZoom == 'out') {
			$("#page").css('font-size', '10px');
			// document.cookie = 'fSize=1em; expires=Thu, 2 Aug 2050 20:00:00 UTC; path=/'
			createCookie('fSize','10px');
		}

	}




/*
	NÉCESSITE JQUERY
*/

	// Permet d’ouvrir un élément particulier de la navigation
	function openNav(id) {
		$("#nav ul").hide();
		$(id).addClass("open");
		$(id).parent().prev().addClass("open");
		$(id).parent().next().slideDown("fast");
	}


$(document).ready(function(){

	// Utilisation de la fonte fixée en cookie (fontSize())

	$('#page').css('font-size',readCookie('fSize'));
	
	
	// Affichage en accordéon de la navigation
	$("#nav ul").hide();
	// Gestion du fonctionnement de la navigation principale
	$("#nav p a").click(
		function() {
			$("#nav p a").removeClass("open");
			$("#nav h2").removeClass("open");
			
			if ($(this).parent().next().is(":visible")) {
				$(this).parent().next().slideUp("fast");
			} else {
				$("#nav ul:visible").slideUp("fast");
				$(this).addClass("open");
				$(this).parent().next().slideDown("fast");
				$(this).parent().prev().addClass("open"); // Modifie la classe des H2
				$(this).parent().next().slideDown("fast");
			}
			
			return false;
			}
	);
	// Permet d’afficher un element comme actif dans la navigation selon la classe du div main
	if ($("#main").attr("class") == "themeServices clearfix") {openNav("#opener1 a");}
	if ($("#main").attr("class") == "themeMorbihan clearfix") {openNav("#opener2 a");}
	if ($("#main").attr("class") == "themeCg clearfix") {openNav("#opener3 a");}


	
	// Génération des tools (imprimer, favoris et NL)
	var tools = '<ul id="tools">';
		tools+='<li id="to1"><a href="#" title="Imprimer cette page">Imprimer cette page</a></li>';
		tools+='<li id="to2"><a href="#" title="Ajouter cette page à vos favoris">Ajouter aux favoris</a></li>';
		if($('#navNautisme').length){
		    tools+='<li id="to3"><a href="/nautisme/envoi-ami.aspx" title="Envoyer cette page à un ami">Envoyer à un ami</a></li>';
		}else{
		    tools+='<li id="to3"><a href="/pages/envoi-ami.aspx" title="Envoyer cette page à un ami">Envoyer à un ami</a></li>';
		}
		tools+='<li id="to4"><a href="#" title="Augmenter le corps de la police de caractères">+</a></li>';
		tools+='<li id="to5"><a href="#" title="Diminuer le corps de la police de caractères">-</a></li></ul>';
	
	
	
	//$(tools).prependTo("#sideBar");

	// Attribution des actions aux éléments de tools
	$("#to1").click(function() {print();});
	$("#to2").click(function() {fav();});
	$("#to4").click(function() {fontSize('in');});
	$("#to5").click(function() {fontSize('out');});
	



	
	// MODIFICATION DE STYLES
	// Marges sur les images de contenus wysiwyg
	$(".artContent img[@align=left]").css("margin", "0.3em 5px 5px 0");
	$(".artContent img[@align=right]").css("margin", "0.3em 0 5px 5px");
	

	// ZEBRA TABLE
	$("tr:nth-child(odd)").addClass("odd");
	
	
	// // Modules de gauche
	// // Suppression du padding-op du premier element, et si un seul module suppression du fond image
	// $(".modContent:first").css("padding-top", "0");
	// if ($(".modContent").size() <= 1) {
	// 	$(".modContent").css("background", "none");
	// }
	
	// Extension des Select dont la largeur est fixée en CSS (pour IE, les options sont tronquées…)
	// if ($.browser.msie) {
	// 	$(".modContent select").hover (
	// 		function () {
	// 			$(this).css('width', 'auto')
	// 		}
	// 		);
	// 	$(".modContent select").blur(function() {
	// 			$(this).css('width', '95%')
	// 	});
	// 
	// }
	
	if($('#rechAv').length == false) $('.module .modContent').parent().addClass("extension");
	if($('.modContent img').length ) $('.module .modContent').parent().removeClass("extension");
	
	
	//----- Clic formulaire dynamique
	$(".FormDynamiqueInptSubmit").click(function() {
		var idElement = $(this).attr("id");
		idElement = idElement.replace('formValide','');
		if (validerFormulaire(idElement)){
			return true;
		}else{
			return false;
		}
	});
	
	//----- Diaporama
	if($('#_div_moduleContent').length){
	    $('#_div_moduleContent .modContent .extensionDiaporama a').lightBox(
        {
	        overlayBgColor: '#063b73',
		        overlayOpacity: 0.7,
		        imageLoading: '/global/images/lightbox-ico-loading.gif',
		        imageBtnClose: '/global/images/lightbox-btn-close.gif',
		        imageBtnPrev: '/global/images/lightbox-btn-prev.gif',
		        imageBtnNext: '/global/images/lightbox-btn-next.gif',
		        containerResizeSpeed: 350,
		        txtImage: 'Image(s)',
		        txtOf: 'de'
        }
        );
    }

});
function validerFormulaire(element){
    
    
    var i = 1;
	var myElementLibelle = element + "_P" + i;
	
	while($("#" + myElementLibelle).length ){
	    
	   
		if ($("#" + myElementLibelle + "_elementVerif").length){
	        myChamp = $("#" + myElementLibelle + "_elementChamp").val();
	        myVerif = $("#" + myElementLibelle + "_elementVerif").val();
		    
		    if (myVerif.value!=""){
				//----- Specificite
				var TAB_verif=myVerif.split(";");
				if (TAB_verif.length>0) {
					
					//----- Champs obligatoires
					if (TAB_verif[1]=="1"){
						//----- Case a cocher + radio
						if ((TAB_verif[0]=="RADIO")||(TAB_verif[0]=="CHECK")){
							j = 1;
							myChampLibelle = myElementLibelle + "_elementChamp" + j;
							isCheck = false;
							
							while($("#" + myChampLibelle).length ){
								if ($("#" + myChampLibelle + "[@checked]").length == "1"){
									isCheck = true;
								}
								j++;
								myChampLibelle = myElementLibelle + "_elementChamp" + j;
							}
							if (!isCheck){
								alert("Merci de saisir les champs obligatoires");
								return false;
							}
						}else{
							if (myChamp==""){
								alert("Merci de saisir les champs obligatoires");
								return false;
							}
						}
						
					}
					
					//----- Vérification
					if (TAB_verif[3]=="MAIL"){
						if ((TAB_verif[0]!="RADIO")&&(TAB_verif[0]!="CHECK")){
							if (!verifiermail(myChamp)){
								alert("Adresse email incorrecte");
								return false;
							}
						}
					}
				}
			}
		}
	    
	    
	    i++;
		myElementLibelle = element + "_P" + i;
	}
	
    return true;
}

function verifiermail(mail) {
    if ((mail.indexOf("@")>=0)&&(mail.indexOf(".")>=0)) {
        return true
    } else {
        return false
    }
}
