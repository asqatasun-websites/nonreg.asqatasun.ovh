/***************************************************************************
 *	INITIALISATION DE LA REDIMENSION DES TEXTES
 ****************************************************************************/
//gestion de l'agrandissemente de la police de caracteres (outil) 
var tailleTexte = 11;
//taille initial du text
var tailleInit = 11;
//taille maximum autorise 
var tailleMax = 17;
//nom de la div ou ce trouve les texte a agrandir
var divPrincipal = 'article';

/***************************************************************************
 *	LIBRAIRIE DE FONCTIONS
 ****************************************************************************/
function displayZoomTools(chemin,zoom_moins,zoom_plus) {
	document.write('<div id="zoomOutils">\n');
	document.write('	<ul>\n');
	document.write('		<li><a href="javascript:changeTaille(-1);" id="zoomMoins"><img src="'+ chemin +'zoomMoins.gif" alt="'+ zoom_moins +'"/></a></li>\n');
	document.write('		<li><a href="javascript:changeTaille(1);" id="zoomPlus"><img src="'+ chemin +'zoomPlus.gif" alt="'+ zoom_plus +'"/></a></li>\n');
	document.write('	</ul>\n');
	document.write('</div>\n');
}

function displayPrinterAndMailToFriend(chemin, mail_sujet, mail_body, imprimer, mailer) {

	document.write('<ul class="pictos">\n');
	document.write('	<li><a id="envoyerAmi" href="mailto:?subject='+ mail_sujet +'&amp;body='+ mail_body +'"><img src="'+ chemin +'envoyerAmi.gif" alt="'+ mailer +'"/></a></li>\n');
	document.write('	<li><a href="javascript:window.print();"><img src="'+ chemin +'imprimer.gif" alt="'+  imprimer +'"/></a></li>\n');
	document.write('</ul>\n');
}

function mesure(_MesurePro, _page)
{
	scr_w = screen.width;
	scr_h = screen.height;
	color = screen.colorDepth;
	ref = escape(window.document.referrer);
	document.write("<IMG src='http://noeinteractive.stats-pro.com/stats/satvac/marqueur.pl/"+ "?page="+ _page+ "&n="+ Math.round (Math.random () * 1000000000000000)+ "&reso_w="+ scr_w+ "&reso_h="+ scr_h+ "&color="+ color+ "&referer="+ ref+"' border=0>");
}

function PopUp2(URL,WIDTH,HEIGHT) {
	options="toolbar=no,location=no,directories=no,status=no,menubar=no,top=20,left=30, scrollbars=no,resizable=no,width=1,height=1"
	Box = window.open (URL,"Windows",options)
	Box.close()
	options="toolbar=no,location=no,directories=no,status=no,menubar=no,top=20,left=30, scrollbars=no,resizable=no,width="+WIDTH+",height="+HEIGHT
	Box = window.open (URL,"Windows",options)
	Box.focus();
}

/*GESTION DU ZOOM */

/*@
    Fonction : changeTaille 
    Description : change la taille de la police d'un page
    Variable : pas (int) -> Valeur indiquant de combien de pixel la taille doit etre diminue ou
                        augmenter (souvent -1 ->diminution ou 1 -> augmentation) 
    
@*/
function changeTaille(pas)
{
  
	//determine si on veut changer de la taille de la poilce ou non (en rapport au taille max et initial defini)     
	var changeSize = true;

	tailleTexte = tailleTexte + pas;

	//on va verifier si la taille du texte n'est pas inferieur a la taille minimum
	if(tailleTexte < tailleInit)
	{
		//si la taille minumm a ete atteinte, on remet notre tailleTexte a la taille minumum, et
		//on considereque ce n'est pas la peine de changer la taille des element
		tailleTexte = tailleInit;
		changeSize = false;
	}        
	else
	{
		//cette fois ci on verifier que la taille maximum n'a ete atteinte
		if (tailleTexte > tailleMax)
		{
			tailleTexte = tailleMax;
			changeSize = false;
		}
	}

	//si changesize est true, c partie on change la taille de la police !!
	if (changeSize)
	{
		var text = document.getElementById(divPrincipal);

		//ici on va changer la taille de tout les p de la page
		//ICI ON MET TOUT LES TAGS QUI N'AUGMENTE PAS CA TAILLE AVEC LA SIMPLE INSTRUCTION 
		// text.style.fontSize = tailleTexte + 'px';

		updateTaille(text,'p',tailleTexte)
		updateTaille(text,'h6',tailleTexte)

		//on rajoute quand meme cette ligne pour augmenter d'autre element
		text.style.fontSize = tailleTexte + 'px';
	}
}

/*@
    Fonction  : updateTaille 
    Description : Change la taille des bloc d'une page(fonction appele dans changeTaille)
    Variables : 
		- > divParent(object) : div de notre page pour laquel on veux changer la taille du texte
		- > tag (text) -> : tag html de l'element qui fera l'objet d'un changement de taille 
                              (ex : p, si l'on veut changer la taille de tout les p)
		- > tailleText : taille du texte
    
@*/
function updateTaille(divParent,tag,tailleTexte)
{

	//determine si on veut changer de la taille de la poilce ou non (en rapport au taille max et initial defini)  
	var changeSize = true;
	
	//on va augmenter la taille de tout ce qui ce trouve dans p
	var textPart = divParent.getElementsByTagName(tag);
	for (var i = 0; i < textPart.length; i++) 
	{
		textPart[i].style.fontSize = tailleTexte + 'px';
	}  
}