function autoResizeIFrame(id, keepResizing, currentHeight){
var newheight;
var newwidth;

if(document.getElementById(id).src != window.location){
		if(document.getElementById(id).contentWindow.document.body){
			newheight=document.getElementById(id).contentWindow.document.body.scrollHeight;
			if(currentHeight != (newheight+50)) {
				document.getElementById(id).height= (newheight+50) + "px";
			}			
		}

		if(keepResizing == 'true') {
			var f = function() {autoResizeIFrame(id,keepResizing,newheight+50); };
			setTimeout(f, 500);
		}
	}
}