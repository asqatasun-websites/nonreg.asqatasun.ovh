// $Id: blocs.js 9011 2008-07-22 09:50:06Z xethorn $
// -*- c_mode -*-
//
var cObjHub = getXMLHttpRequestObject();
var cObjOnAir = getXMLHttpRequestObject();
var cObjZapping = getXMLHttpRequestObject();
var cObjSkynautes = getXMLHttpRequestObject();
var hubRefreshValue = 60; // en secondes!
var onairFreq = 20; // en secondes
var skynautesFreq = 25; // en secondes
var ope;


function getXMLHttpRequestObject()
{
    if (typeof XMLHttpRequest != 'undefined')
        return new XMLHttpRequest();
    else if (typeof ActiveXObject != 'undefined')
        return new ActiveXObject('MSXML2.XmlHttp');
    else
        return false;
}

_onloads.push(updateHubWrapper);
_onloads.push(updateOnairWrapper);
 // _onloads.push(updateSkynautesWrapper);

// si ya un hub sur la page, on lance la proc�dure d'update auto via xmlhttprequest
function updateHubWrapper()
{
    var hub = document.getElementById('hub');
    ope = document.getElementById('ope');

    if (hub && ope)
    {
        window.setInterval(updateHub, hubRefreshValue * 1000);
    }
}


function updateOnairWrapper() {
    if (!document.getElementById('animateurs')) {
        return;
    }
    window.setInterval(updateOnair, onairFreq * 1000);
    updateOnair();
}

function updateSkynautesWrapper() {
    if (!document.getElementById('nb-rencontres-minous')) {
        return;
    }
    window.setInterval(updateSkynautes, skynautesFreq * 1000);
    updateSkynautes();
}

function updateOnair()
{
    if (cObjOnAir.readyState != 0)
    {
        cObjOnAir.abort();
    }
    cObjOnAir.onreadystatechange = callbackOnAir;
    cObjOnAir.open('GET', '/front/xml/onair.xml?t=' + (new Date().getTime()), true);
    cObjOnAir.send(null);
}

function updateSkynautes()
{
    if (cObjSkynautes.readyState != 0)
    {
        cObjSkynautes.abort();
    }
    cObjSkynautes.onreadystatechange = callbackSkynautes;
    cObjSkynautes.open('GET', '/rencontres/genfiles/skynautes_stats.xml?t=' + (new Date().getTime()), true);
    cObjSkynautes.send(null);
}

function updateHub()
{
    if (cObjHub.readyState != 0)
    {
        cObjHub.abort();
    }
    cObjHub.onreadystatechange = callbackHub;
    cObjHub.open('GET', 'modules/hub.php?t=' + (new Date().getTime()), true);
    cObjHub.send(null);
}

function callbackHub()
{
    if (cObjZapping.readyState == 4 && cObjZapping.status == 200)
    {
        ope.innerHTML = cObjZapping.responseText;
    }
}

function callbackOnAir() {
    if (cObjOnAir.readyState == 4 && cObjOnAir.status == 200)
    {
        onair = cObjOnAir.responseXML.getElementsByTagName('onair')[0];
        if (!onair) {
            return;
        }
        emission = onair.getElementsByTagName('emission')[0];
        if (emission) {
            title = emission.getElementsByTagName('title')[0];
            caption = emission.getElementsByTagName('caption')[0];
        if(caption.firstChild)
        {
        document.getElementById('animateurs').firstChild.data = caption.firstChild.data;
        document.getElementById('animateurs').setAttribute('href', 'http://www.skyrock.fm/front/index.php?m=6&l=8&id=' + caption.getAttribute('id'));
        }
            if(title.firstChild)
        {
        document.getElementById('emission-title').firstChild.data = title.firstChild.data;
        document.getElementById('emission-title').setAttribute('href', 'http://www.skyrock.fm/front/index.php?m=6&l=4&id=' + title.getAttribute('id'));
        }

        }
        playing = onair.getElementsByTagName('playing')[0];
        k_artiste = playing.getElementsByTagName('artiste')[0];
        if (k_artiste && k_artiste.firstChild) {
            artiste = k_artiste.firstChild.data;
            titre = playing.getElementsByTagName('titre')[0].firstChild.data;
            id_yacast = playing.getElementsByTagName('id_yacast')[0].firstChild.data;
            document.getElementById('playing-artiste').firstChild.data = artiste;
            document.getElementById('playing-titre').firstChild.data = titre;
            document.getElementById('player_title').firstChild.data = artiste + ':' + titre;
        initGallery(); // met � jour le syteme de "gallery" (scroll) qui d�pend de la largeur du contenu
            document.getElementById('sonnerieLink').href = 'http://www.skyrockmobile.com/40001-' + encodeURIComponent(titre) + '.html';
        }
        k_cover_url = playing.getElementsByTagName('cover_url')[0];
        if (k_cover_url && k_cover_url.firstChild) {
            cover_url = k_cover_url.firstChild.data;
            ts = playing.getElementsByTagName('ts')[0].firstChild.data;
            document.getElementById('photo').src = cover_url + '?ts=' +
                escape(ts);
        }

    }
}

function callbackSkynautes() {
    if (cObjSkynautes.readyState != 4 || cObjSkynautes.status != 200) {
        return;
    }
    rencontres_counts =
        cObjSkynautes.responseXML.
        getElementsByTagName('rencontres_counts')[0];
    if (rencontres_counts === undefined) {
        return;
    }
    nb_rencontres_minous = rencontres_counts.
        getElementsByTagName('nb_rencontres_minous')[0].firstChild.data;
    nb_rencontres_teubs = rencontres_counts.
        getElementsByTagName('nb_rencontres_teubs')[0].firstChild.data;
    document.getElementById('nb-rencontres-minous').firstChild.data
        = nb_rencontres_minous;
    document.getElementById('nb-rencontres-teubs').firstChild.data
        = nb_rencontres_teubs;

    tofz = cObjSkynautes.responseXML.getElementsByTagName('tofz')[0];
    if (tofz === undefined) {
        return;
    }
    teub = tofz.getElementsByTagName('teub')[0];
    minou = tofz.getElementsByTagName('minou')[0];
   random_profil =
        cObjSkynautes.responseXML.getElementsByTagName('random_profil')[0];
    if (teub !== undefined) {
        pseudo = teub.getElementsByTagName('pseudo')[0].firstChild.data;
        fiche_url =
            teub.getElementsByTagName('fiche_url')[0].firstChild.data;
        obj = document.getElementById('rencontres-link-teub');
        obj.href = fiche_url;
        k_avatar = teub.getElementsByTagName('avatar_url')[0];
        if (k_avatar !== undefined) {
            avatar = k_avatar.firstChild.data;
            img = obj.firstChild;
            img.src = avatar;
            img.alt = pseudo;
        }
        document.getElementById('rencontres-pseudo-teub').firstChild.data
            = pseudo;
    }
    if (minou !== undefined) {
        pseudo = minou.getElementsByTagName('pseudo')[0].firstChild.data;
        fiche_url =
            minou.getElementsByTagName('fiche_url')[0].firstChild.data;
        obj = document.getElementById('rencontres-link-minou');
        obj.href = fiche_url;
        k_avatar = minou.getElementsByTagName('avatar_url')[0];
        if (k_avatar !== undefined) {
            avatar = k_avatar.firstChild.data;
            img = obj.firstChild;
            img.src = avatar;
            img.alt = pseudo;
        }
        document.getElementById('rencontres-pseudo-minou').firstChild.data
            = pseudo;
    }
    if (random_profil !== undefined) {
        pseudo = random_profil.
            getElementsByTagName('pseudo')[0].firstChild.data;
        fiche_url =
            random_profil.getElementsByTagName('fiche_url')[0].firstChild.data;
        obj = document.getElementById('random-profil');
        obj.href = fiche_url;
        k_avatar = random_profil.getElementsByTagName('avatar_url')[0];
        if (k_avatar !== undefined) {
            avatar = k_avatar.firstChild.data;
            img = obj.firstChild;
            img.src = avatar;
            img.alt = pseudo;
        }
    }
}

if (typeof OasInnerHtml == 'undefined')
{
    function OasInnerHtml(element, content)
    {
        if(typeof document.getElementById(element) != 'object')
        {
            return;
        }
        document.getElementById(element).innerHTML = content;
    }
}
