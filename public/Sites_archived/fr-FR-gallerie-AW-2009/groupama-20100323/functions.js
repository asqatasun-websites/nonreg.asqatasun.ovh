function test_email(email) {
	var new_string = new String(email);
	if (!new_string.match('^[-_\.0-9a-zA-Z]{1,}@[-_\.0-9a-zA-Z]{1,}[\.][0-9a-zA-Z]{2,}$'))
		return false;
	else
		return true;
}

function checkMail(champ, div, check) {
	var elt=champ;
	var cplx=false;
	if (arguments.length==4) { var elt1=arguments[3]; cplx=true;}
	if(!test_email(elt.value)) {
			if (cplx) ajouterClasseBg(div, 'bgFalse',elt1); else ajouterClasseBg(div, 'bgFalse');
			ajouterClasseCheck(check, 'checkFalse');
		}else{
			if (cplx) ajouterClasseBg(div, 'bgTrue',elt1); else  ajouterClasseBg(div, 'bgTrue');
			ajouterClasseCheck(check, 'checkTrue');
		}
}

function checkTextArea(champ, div) {
	var elt = document.getElementById(champ);
	var cplx=false;
	if (arguments.length==3) { var elt1=arguments[2];cplx=true;}
	if(elt.value.length < 2) {
		if (cplx) ajouterClasseBg(div, 'bgTextAreaFalse',elt1); else ajouterClasseBg(div, 'bgTextAreaFalse');
	}else{
		if (cplx) ajouterClasseBg(div, 'bgTextAreaTrue',elt1); else ajouterClasseBg(div, 'bgTextAreaTrue');
	}
}

function checkNonVide(champ, div, check){	
	var elt=document.getElementById(champ);
	var cplx=false;
	if (arguments.length==4){ var elt1=arguments[3];cplx=true;}
	if(elt.value.length < 2) {
		if (cplx) ajouterClasseBg(div, 'bgFalse',elt1); else ajouterClasseBg(div, 'bgFalse');
		ajouterClasseCheck(check, 'checkFalse');
	}else{
		if (cplx) ajouterClasseBg(div, 'bgTrue',elt1); else ajouterClasseBg(div, 'bgTrue');
		ajouterClasseCheck(check, 'checkTrue');
	}
}

function checkForm() {
	
	var dest = document.getElementById('form_dest').value;
	var exp = document.getElementById('form_exp').value;
	var nom = document.getElementById('form_nom').value;
	var mess = document.getElementById('form_message').value;
	var erreur = 0;
	var message = '<strong>Tous les champs sont obligatoires : </strong><br /><ul>';
		
	if(!test_email(dest)){
		ajouterClasseBg('bg_dest', 'bgFalse');
		ajouterClasseCheck('check_dest', 'checkFalse');
		erreur = 1;
		message = message + '<li>- <a onclick="javascript:document.getElementById(\'form_dest\').focus();">Erreur champ destinataire</a></li>';
	}else{
		ajouterClasseBg('bg_dest', 'bgTrue');
		ajouterClasseCheck('check_dest', 'checkTrue');
	}
	if(!test_email(exp)){
		ajouterClasseBg('bg_exp', 'bgFalse');
		ajouterClasseCheck('check_exp', 'checkFalse');
		erreur = 1;
		message = message + '<li>- <a onclick="javascript:document.getElementById(\'form_exp\').focus();">Erreur champ expediteur</a></li>';
	}else{
		ajouterClasseBg('bg_exp', 'bgTrue');
		ajouterClasseCheck('check_exp', 'checkTrue');
	}
	if(nom.length < 2){
		ajouterClasseBg('bg_nom', 'bgFalse');
		ajouterClasseCheck('check_nom', 'checkFalse');
		erreur = 1;
		message = message + '<li>- <a onclick="javascript:document.getElementById(\'form_nom\').focus();">Erreur champ nom</a></li>';
	}else{
		ajouterClasseBg('bg_nom', 'bgTrue');
		ajouterClasseCheck('check_nom', 'checkTrue');
	}
	if(mess.length < 2){
		erreur = 1;
		ajouterClasseBg('bg_message', 'bgTextAreaFalse');
		message = message + '<li>- <a onclick="javascript:document.getElementById(\'form_message\').focus();">Erreur champ message</a></li>';
	}else
		ajouterClasseBg('bg_message', 'bgTextAreaTrue');		
	
	message = message + '</ul>';
	
	if(erreur == 0) {
		document.getElementById('form_recommander').submit();
		document.getElementById('form_recommander').style.display = 'none';
		document.getElementById('recommander_confirm').style.display = 'block';
	}else{
		document.getElementById('erreurs').style.display = 'block';
		document.getElementById('erreurs').innerHTML = message;
	}
	
}

function ajoute_class(element, newClasse) {
	var elt = document.getElementById(element);
	var classe = elt.className;
	var pos = classe.indexOf('teq-print-');
	var classetemp = classe.substring(0, pos);

	classetemp = classetemp + classe.substring(pos+11, classe.length) + ' ' + newClasse;
	
	elt.className = classetemp;
	
	fermerPopin('popin-imprimer');
	
	window.print();
}

function ajouterClasseBg(element, newClasse) {
	var elt = document.getElementById(element);
	if (arguments.length==3) elt=arguments[2];
	var classe = elt.className;
	var pos = classe.indexOf('bg');

	var classetemp = classe.substring(0, pos);
	classetemp = classe.substring(pos, 2) + ' ' + newClasse;
	
	elt.className = classetemp;
}

function ajouterClasseCheck(element, newClasse) {
	var elt = document.getElementById(element);
	if (arguments.length==3) elt=arguments[2];
	var classe = elt.className;
	var pos = classe.indexOf('check');

	var classetemp = classe.substring(0, pos);
	classetemp = classe.substring(pos, 5) + ' ' + newClasse;
	
	elt.className = classetemp;
}

function overCompte(value){
	var text = '';
	text = '<img src="file/design/banq/images/compte-'+value+'.jpg" alt="" />';
	document.getElementById("textDivCpt").innerHTML = text;
}

function overPrint(value){
	var text = '';
	text = '<img src="file/design/banq/images/print-'+value+'.jpg" alt="" />';
	document.getElementById("textDivImp").innerHTML = text;
}

function overReseaux(value){
	var text = '';
	text = '<img src="file/design/banq/images/reseaux-'+value+'.jpg" alt="" />';
	document.getElementById("textDivRes").innerHTML = text;
}

function checkFormSuggestion() {
	var mess = document.getElementById('form_message').value;
	var nom = document.getElementById('form_nom').value;
	var email = document.getElementById('form_exp').value;
	
	var erreur = 0;
	var message = '<strong>Tous les champs sont obligatoires : </strong><br /><ul>';
	
	if(mess.length < 2){
		erreur = 1;
		ajouterClasseBg('bg_message', 'bgTextAreaFalse');
		message = message + '<li>- <a onclick="javascript:document.getElementById(\'form_message\').focus();">Erreur champ message</a></li>';
	}else
		ajouterClasseBg('bg_message', 'bgTextAreaTrue');
	
	if(nom.length < 2){
		ajouterClasseBg('bg_nom', 'bgFalse');
		ajouterClasseCheck('check_nom', 'checkFalse');
		erreur = 1;
		message = message + '<li>- <a onclick="javascript:document.getElementById(\'form_nom\').focus();">Erreur champ nom</a></li>';
	}else{
		ajouterClasseBg('bg_nom', 'bgTrue');
		ajouterClasseCheck('check_nom', 'checkTrue');
	}
	
	if(!test_email(email)){
		ajouterClasseBg('bg_exp', 'bgFalse');
		ajouterClasseCheck('check_exp', 'checkFalse');
		erreur = 1;
		message = message + '<li>- <a onclick="javascript:document.getElementById(\'form_exp\').focus();">Erreur champ mail</a></li>';
	}else{
		ajouterClasseBg('bg_exp', 'bgTrue');
		ajouterClasseCheck('check_exp', 'checkTrue');
	}
	
	message = message + '</ul>';
	
	if(erreur == 0) {
		document.getElementById('form_suggestion').submit();
		document.getElementById('form_suggestion').style.display = 'none';
		document.getElementById('suggesstion_confirm').style.display = 'block';
	}else{
		document.getElementById('erreurs').style.display = 'block';
		document.getElementById('erreurs').innerHTML = message;
	}
	
}

function ajouterFavoris(adresse, titre) {
	createBookmark(adresse,titre);
	return;
    if (document.all) {
        window.external.AddFavorite(adresse, titre);
    }
    else if (window.sidebar) {
        window.sidebar.addPanel(adresse, titre, "");
    }
    else {
        alert("D�sol�! Votre navigateur ne supporte pas cette fonction.");
    }
}
function createBookmark ()
{
	var title=""
	var url=""
	if (arguments.length==2) {
		 url=arguments[0];
		 title=arguments[1];
	} else {
		title = prompt ("Saisir un titre pour votre favori",window.top.document.title);
		url= window.location.href;
	}
	if (title==null || title=="") return;
	try {
		if (window.sidebar) { // Mozilla Firefox Bookmark
			window.sidebar.addPanel(title, url,"");
		} 
		else if( window.external ) { // IE Favorite
			window.external.AddFavorite( url, title);
		}
	} catch (errBk) {
		alert("D�sol�! Votre navigateur ne supporte pas cette fonction.");
	}
}
