// http://www.developpement-web.net/astuces/javascript/diaporamas_images_script_aculo_us/ 

var effect = 1;
var time = 3000;

var current_image = 0;
var next_image = 1;
var image = new Array();

function init() {
	if(document.getElementsByClassName("image")) {
		image = document.getElementsByClassName("image");
		for(i=1; i < image.length; i++) {
			image[i].style.display = "none";
		}
	}
	
	if(image.length > 1) galerie();
}

function galerie() {
	self.setTimeout("nextimage()",time);	
}

function nextimage() {
	if(effect == 1) { new Effect.Fade(image[current_image]); new Effect.Appear(image[next_image]); }
	if(effect == 2) { new Effect.BlindUp(image[current_image]); new Effect.BlindDown(image[next_image]); }
	
	if(next_image == (image.length-1)) {
		current_image = next_image;
		next_image = 0;
	} else {
		current_image = next_image;
		next_image++;
	}
	galerie();
}


