// Sets cookie value. Expiration date is optional
function setCookie(name, value, expiredays) {
	var ExpireDate = new Date ();
	ExpireDate.setTime(ExpireDate.getTime() + (expiredays * 24 * 3600 * 1000));
	document.cookie = name + "=" + escape(value) + ((ExpireDate == null) ? "" : (";expires=" + ExpireDate.toGMTString()));
	//alert("setCookie:"+name+"="+value);
}
// Gets cookie value
function getCookie(name) {
	var search = name + "=" ;
	if (document.cookie.length > 0) {
	// if there are any cookies
		offset = document.cookie.indexOf(search);
		if (offset != -1) {
		// if cookie exists
			offset += search.length;
		// set index of beginning of value
			end = document.cookie.indexOf(";", offset);
		// set index of end of cookie value
			if (end == -1)end = document.cookie.length;
			//alert("getCookie:"+name+"="+(unescape(document.cookie.substring(offset, end))));
			return unescape(document.cookie.substring(offset, end));
		}
	}
}
// Delete a cookie by setting a past expiration date
function deleteCookie(name) {
	var ExpireDate = new Date ();
	ExpireDate.setTime(ExpireDate.getTime() - (24 * 3600 * 1000));
	document.cookie = name + "=" + "expired" + ";expires=" + ExpireDate.toGMTString();
	//alert("deleteCookie:"+name);
}


function writeCookie(name, value) {
	document.cookie = name + "=" + value;
}
function readCookie(name) {
	var search = name + "=" ;
	if (document.cookie.length > 0) {
		offset = document.cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = document.cookie.indexOf(";", offset);
			if (end == -1)end = document.cookie.length;
			return document.cookie.substring(offset, end);
		}
	}
}
