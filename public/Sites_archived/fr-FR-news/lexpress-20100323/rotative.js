/**
 * @author sangele
 */

var Rotative = Class.create();
Rotative.prototype = {
	largeur:0,
	elembydiv:0,
	container:null,
	nombre:0,
	index:0,
	initialize: function(divcontainer,btnprec,btnsuiv,nbrcontenu,l,elembydiv) {
	this.container = divcontainer;
	this.elembydiv = elembydiv;
	this.largeur = l;
	this.nombre = nbrcontenu;
	this.index = 0;
	var DelayedEventRotative={
			handler:null,
			click :function(event,rotative,decalage){
				Event.stop(event);
				rotative.move(decalage);
				}
		}	
	DelayedEventRotative.handler = DelayedEventRotative.click.bindAsEventListener(DelayedEventRotative,this,-1)		
	Event.observe(btnprec,"click",DelayedEventRotative.handler);
	DelayedEventRotative.handler = DelayedEventRotative.click.bindAsEventListener(DelayedEventRotative,this,1)		
	Event.observe(btnsuiv,"click",DelayedEventRotative.handler);	
	},
	move: function(decalage) {
		flagstop = false;
		this.index= this.index + decalage
		if(decalage==-1 && this.index < 0)
		{
			flagstop = true;
			this.index = 0
		}

		if(decalage==1 && this.index >= (this.nombre-this.elembydiv+1))
		{
			flagstop = true;
			this.index = this.index - 1
		}				
		if(flagstop==false)
		{
			position = -this.largeur*decalage;		
			new Effect.Move (this.container,{ x: position, y: 0, mode: 'relative',duration:0.5,queue: 'end'}); 		
		} 		
	}

};

function initRotative(module){
	var as_button=module.down('div.x_nav').select('a');
	var titre=module.select('div.x_produit');
	var evt;
	var i;
	var page=module.down('div.x_nav').down('span');
	evt={
		handler:null,
		click:function(event,increment,nblink,titre,page){
			Event.stop(event);
			var j;
			var cur_index;
			cur_index=-1;
			j=0;
			while(j<nblink && cur_index==-1)
			{
				if(titre[j].visible())
				{
					cur_index=j;
				}
				j++;
			}
			if(cur_index>-1)
			{
				titre[cur_index].hide();
				cur_index=cur_index+increment;
				if(cur_index<0)
				{
					cur_index=nblink-1;
				}
				else
				{
					if(cur_index>=nblink)
					{
						cur_index=0;
					}
				}
				titre[cur_index].show();
				page.update((cur_index+1) + '/' + titre.length);
				}
		}
	}

	as_button[0].writeAttribute('href','#');
	as_button[1].writeAttribute('href','#');
	page.update(1 + '/' + titre.length);
	evt.handler = evt.click.bindAsEventListener(evt,-1,titre.length,titre,page);
	Event.observe(as_button[0],'click',evt.handler,false);
	evt.handler = evt.click.bindAsEventListener(evt,+1,titre.length,titre,page);
	Event.observe(as_button[1],'click',evt.handler,false);
}
