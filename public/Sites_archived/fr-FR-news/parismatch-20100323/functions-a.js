// Remove 'Commander un ancien numéro'
$('.abonnement p a:eq(3)').next('br:eq(0)').remove().end().remove();
$('#sommaire-couv p a:eq(2)').next('br:eq(0)').remove().end().remove();

$.extend({
	isIE : $.browser.msie,
	isIE6 : $.browser.msie && $.browser.version <= 6,
	isIE7 : $.browser.msie && $.browser.version == 7,
	nbDesc : function(a,b){ return b-a; },
	getMax : function(arr){
		arr.sort(this.nbDesc);
		return arr[0];
	}
});
if( $.isIE ) $.getScript('/includes/cobrand/js/ie.js');

/* Sondage
------------------------------------------------------------ */
if( $('#sondage-match').length ){
// Vars
	var id_sondage = $('#sondage-match input[name=sondage]').val();
	var cookie_name = 'SONDAGE_ID_' + id_sondage;
	var urlToRequest = '/includes/Sondages/' + id_sondage + '/';
// Functions
	var _voted = function(){
		if( $.cookie(cookie_name) === null ) return false;
		else if( !$.cookie(cookie_name) ) return false;
		else return true;
	};
	var _verif = function(cb){
		if( $('#sondage-match :radio:checked').val() === undefined ) return false;
		else return true;
	};
	var _success = function(r){
		$('#sondage-loader').prepend(r);
		$('#sondage-match').css({ display: 'block' });
	};
// Load question || ansers
	urlToRequest += (_voted()) ? 'answers.htm' : 'question.htm';
	$.ajax({
		url: urlToRequest,
		dataType: 'html',
		success: function(html){ _success(html); }
	});
// Events
	$('#sondage-match :submit').live('click',function(){
		if( _verif() ){
			$.cookie(cookie_name, 'TRUE',{ expires: 1, path: '/', domain: 'parismatch.com' });
			return true;
		}
		else{
			$('#sondage-loader ul').next('.error').remove().end().after('<p class="error">Merci de choisir une réponse avant de voter.</p>');
			return false;
		}
	});	
}

/* Toolbar
------------------------------------------------------------ */
$('.toolbar').each(function(){
// vars
	var _this = this;
// Functions
	var resizeText = function(dir){
		var currentFontSizeNum = parseInt($('#article-body > p').css('font-size'));
		if( currentFontSizeNum >= 8 && currentFontSizeNum <= 23 ){
			if(dir > 0){
				var newFontSize = currentFontSizeNum + 5;
				if( newFontSize > 23 ) newFontSize = 23;
			}
			else{
			    var newFontSize = currentFontSizeNum - 5;
			    if( newFontSize < 8 ) newFontSize = 8;
			}
			$('#article-body > p').css('font-size', newFontSize);
		}
	};
	var sendMail = function(url){};
// Events & tests
	$('li',_this).css({ cursor: 'pointer' }).click(function(){
		if ($(this).hasClass('p-sizeup'))	resizeText(1);
		if ($(this).hasClass('p-sizedown'))	resizeText(-1);
		if ($(this).hasClass('p-print'))	window.print();
		if ($(this).hasClass('p-sendmail'))	sendMail(window.location);
	});
});
/* Sharing
------------------------------------------------------------ */
/*
$('.sharing').each(function(){
	var getShareLink = function(o){
		switch(o.className){
			case 'facebook'	: return 'http://www.facebook.com/sharer.php?u='; break;
			case 'wikio'	: return 'http://www.wikio.fr/vote?url='; break;
			case 'scoopeo'	: return 'http://www.scoopeo.com/scoop/new?newurl='; break;
			case 'myspace'	: return 'http://www.myspace.com/Modules/PostTo/Pages/?c='; break;
			case 'viadeo'	: return 'http://www.viadeo.com/shareit/share/?url='; break;
			case 'yahoobuzz': return 'http://fr.buzz.yahoo.com/buzz?publisherurn=parismatchco_798&amp;targetUrl='; break;
		}
	}
	$('a', this).each(function(){
		var shareLink = getShareLink(this) + location.href;
		$(this).attr('href',shareLink).attr('target','_blank');
	});
});
*/

/* Wall roller ( Home photos )
------------------------------------------------------------ */
$('.wall .w-roll').each(function(){
	$('.w-txt',this).css({ opacity: 0, top: 0 });
	$(this).hover(
		function(){ $('.w-txt',this).stop().fadeTo('normal', .8); },
		function(){ $('.w-txt',this).stop().fadeTo('normal', 0); }
	);
});

/* ADS
------------------------------------------------------------ */
$.fn.adsLoader = function(){
	var get = function(id){
		return ( null === document.getElementById(id) ) ? false : document.getElementById(id);
	};
	$('div',this).each(function(){
		var cible = get(this.id.substring(5,this.id.length));
		//( cible ) ? cible.appendChild(get(this.id)) : $(this).remove();
		( cible ) ? cible.appendChild(get(this.id)) : false;
	});
}
/* Sondages
------------------------------------------------------------ */
$("#sondage-select-category").change(function(event){
	window.location.replace( $("#sondage-select-category option:selected").val() );
});

/* Menu Carte points chauds
------------------------------------------------------------ */
function toggle_menu(menu, items){
	var menu = $(menu);
	var menu_elements = $(menu).children(items);
	$(menu_elements).click(
		function(){
			$('.cur', menu).removeClass('cur');
			$(this).addClass('cur');
		}
	);
}
if($('body.googlize #menu')){ toggle_menu('body.googlize #menu', 'span') }

/* Créer une popup
------------------------------------------------------------ */
function popup(trigger, postype, launch){
	// trigger should be expressed with jQuery syntax without "$()"
	// postype can be 'centered' or 'relative' (to event position)
	// launch can be 'auto' or any jQuery event
	var trigger = ('') ? '' : $(trigger);
	var pop = $('#popup-outer');
	var wth = $(pop).width();
	var hgt = $(pop).height();
	var go;
	if(postype == 'centered'){
		go = function(){
			// determine window geometry to place popup
			var ypos = $(window).height() / 3;
			var xpos = $(window).width() / 2 - wth / 2;
			// show popup
			$('#popup-outer').css({
				top:ypos,
				left:xpos,
				display:'block'
			});
			return false;
		}
	}
	else if(postype == 'relative'){
		go = function(event){
			// position
			var xpos = event.pageX;
			var ypos = event.pageY;
			// show popup
			$(pop).css({
				top:ypos - hgt*1.6,
				left:xpos - wth/2,
				display:'block'
			});
			return false;
		}
	}
	// bind function or launch popup
	if(launch == 'auto'){ go(); }
	else{ $(trigger).bind(launch,go); }
	// function to kill popup
		var kill = function(){ $(pop).css({display:'none'}); }
	// by clicking "close" button
		$('#puclose').bind('click', kill);
	// by pressing "ESC"
		$(document).keypress(function(event){
			var key;
			if(event.keyCode){ key = event.keyCode; }
			else if(event.which){ key = event.which; }
			if(key == 27){ kill(); }
		});
}

/* Sélectionner tout un texte en un clic
------------------------------------------------------------ */
function selectall(content){
	$(content).click(
		function(){
			this.focus(); this.select();
		}
	);
}