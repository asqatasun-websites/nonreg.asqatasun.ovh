/* $Id$ */
/* cf. depart.js, english.js */

var langue;
var naviguerVersArboId;
var naviguerVersArboVersion;

function naviguerVers(url)
{
	document.location = 
		url + 
		"&ARBO_ID=" + naviguerVersArboId + 
		"&ARBO_VERSION=" + naviguerVersArboVersion;
}

function surChargementPageArrivee(arboId, arboVersion)
{
	langue = "fr";
	naviguerVersArboId = arboId;
	naviguerVersArboVersion = arboVersion;
	getPubIdForm("flux.aspx?action=PudIdForm");
	if (document.getElementById("btValideForm"))
		document.getElementById("btValideForm").value = "";
}
