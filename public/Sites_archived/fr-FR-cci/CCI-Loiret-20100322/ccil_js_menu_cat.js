
		// transforme un tableau en chaine
		// les elements sont separes par une virgule
		function tab2chaine (tab) {
			chaine = '';
			for (i=0; i < tab.length; i++) {
				chaine = chaine + ',' + tab[i];
			}
			if (chaine.length > 0) {
				chaine = chaine.substring(1, chaine.length);
			}
			return (chaine);
		}

		// transforme une chaine en tableau
		// les elements de la chaine sont separes par une virgule
		function chaine2tab (chaine) {
			tab = chaine.split(',');
			return (tab);
		}

		// initialisation du menu
		function ccil_init_menu() {
			ccil_menu_b2 = getCookie('ccil_menu_b2');
			if (ccil_menu_b2) {
				a = chaine2tab(ccil_menu_b2);
				for (i=0; i < a.length; i++) {
					img = document.getElementById(a[i]);
					node = img;
					if (node) {
						while (node.nodeName != 'UL') {
							node = node.nextSibling;
						}
					}
					if (node) {
						if (node.nodeName == 'UL') {
							node.style.display = 'block';
						}
					}
					if (img) {
	                                	img.src = 'img/minus.gif';
					}
				}
			}
		}

		function getCookie(name) {
			var dc = document.cookie;
			var prefix = name + "=";
			var begin = dc.indexOf("; " + prefix);
			if (begin == -1) {
				begin = dc.indexOf(prefix);
				if (begin != 0) return null;
			}
			else {
				begin += 2;
			}
			var end = document.cookie.indexOf(";", begin);
			if (end == -1) {
				end = dc.length;
			}
			return unescape(dc.substring(begin + prefix.length, end));
		}

		function ccil_menu_expand(n) {
			var node = n;
			while ( node.nodeName != "UL" ) {
				node = node.nextSibling;
			}
			if ( node.style.display == 'block' ) {
				node.style.display = 'none';
				n.src = 'img/plus.gif';
			}
			else {
				node.style.display = 'block';
				n.src = 'img/minus.gif';
			}

			// sauvegarde dans cookie
			ccil_svg_menu_cookie();
		}

		function ccil_svg_menu_cookie() {
			var date_fin = new Date();
			date_fin.setTime(date_fin.getTime() + (7*24*60*60*1000)); // 1 semaine
			contenu_cookie = null;
			a = new Array();
			l = document.getElementsByTagName('img');
			for (i=0; i < l.length; i++) {
				img = l[i];
				id = img.id;
				if (id.substring(0,3) == 'img') {
					sub_src = img.src.substring(img.src.length, img.src.length - 13);
					if (sub_src == 'img/minus.gif') {
						a[a.length] = id;
					}
				}
			}
			contenu_cookie = tab2chaine(a);
			document.cookie = "ccil_menu_b2=" + escape(contenu_cookie) + "; path=/; expires=" + date_fin.toGMTString();
		}
	
