var isNS = (document.layers) ? 1 : 0;
var isIE = (document.all) ? 1 : 0;
var isMZ = (!isIE && document.getElementById) ? 1 : 0;
var isSF = (navigator.appVersion.indexOf("Safari") > -1);

var isMac = (navigator.appVersion.indexOf("Macintosh") > -1);

function getImage(divName, imgName) {
	if (divName == '')
		return document.images[imgName];
	else
	{
		if (isNS) 
			return eval("document." + divName).document.images[imgName];
		else
			return document.images[imgName];
	}
}

function getLayer(name) {
	if (isMZ)
		return document.getElementById(name);
	else if (isIE)
		return eval('document.all.' + name);
    else {
		var layer;
		for (var i = 0; i < document.layers.length; i++) {
			layer = document.layers[i];
			if (layer.name == name)
				return layer;
		}
	}
}

function getLeft(layer) {
	if (isNS)
		return(layer.left);
	else
		return(layer.style.pixelLeft);
}

function getTop(layer) {
	if (isNS)
		return(layer.top);
	else
	return(layer.style.pixelTop);
}

function getRight(layer) {
	return(getLeft(layer) + getWidth(layer));
}

function getBottom(layer) {
	return(getTop(layer) + getHeight(layer));
}

function getPageLeft(layer) {
	if (isNS)
		return(layer.pageX);
	else
		return(layer.offsetLeft);
}

function getPageTop(layer) {
	if (isNS)
		return(layer.pageY);
	else
		return(layer.offsetTop);
}

function getPageRight(layer) {
	return(getPageLeft(layer) + getWidth(layer));
}

function getPageBottom(layer) {
	return(getPageTop(layer) + getHeight(layer));
}

function getWidth(layer) {
	if (isIE)
		return(layer.style.pixelWidth);
	else if (isNS)
		return(layer.document.width);
	else {
		var clip = layer.style.clip;
		var i = clip.indexOf(",") + 1;
		var j = clip.indexOf("p", i);
		return(parseInt(clip.substring(i, j), 10));
	}
}

function getHeight(layer) {
	if (isIE)
		return(layer.style.pixelHeight);
	else if (isNS)
		return(layer.document.height);
	else {
		var clip = layer.style.clip;
		var i = clip.indexOf(",") + 1;
		i = clip.indexOf(",", i) + 1;
		var j = clip.indexOf("p", i);
		return(parseInt(clip.substring(i, j), 10));
	}
}

function hideLayer(layer) {
	if (isNS)
		layer.visibility = "hide";
	else
		layer.style.visibility = "hidden";
}

function showLayer(layer) {
	if (isNS)
		layer.visibility = "show";
	else
		layer.style.visibility = "visible";
}

function isVisible(layer) {
	if (isNS)
		return(layer.visibility == "show");
	else
		return(layer.style.visibility == "visible");
}

function moveLayerTo(layer, x, y) {
	if (isNS)
		layer.moveTo(x, y);
	else {
		layer.style.left = x + 'px';
		layer.style.top  = y + 'px';
	}
}

function moveLayerBy(layer, x, y) {
	if (isNS)
		layer.moveBy(x, y);
	else if (isMZ) {
		var pixelLeft = layer.style.left;
		pixelLeft = pixelLeft.substring(0, pixelLeft.indexOf('px'));

		var pixelTop = layer.style.top;
		pixelTop = pixelTop.substring(0, pixelTop.indexOf('px'));

		layer.style.left = (parseInt(pixelLeft, 10) + x) + 'px';
		layer.style.top = (parseInt(pixelTop, 10) + y) + 'px';
	}
	else {
		layer.style.pixelLeft += x;
		layer.style.pixelTop  += y;
	}
}

/*
function moveLayerBy(layer, x, y) {
	if (isNS)
		layer.moveBy(x, y);
	else {
		layer.style.pixelLeft += x;
		layer.style.pixelTop  += y;
	}
}
*/

function getzIndex(layer) {
	if (isNS)
		return(layer.zIndex);
	else
		return(layer.style.zIndex);
}

function setzIndex(layer, z) {
	if (isNS)
		layer.zIndex = z;
	else
		layer.style.zIndex = z;
}

function clipLayer(layer, clipleft, cliptop, clipright, clipbottom) {
	if (isNS) {
		layer.clip.left   = clipleft;
		layer.clip.top    = cliptop;
		layer.clip.right  = clipright;
		layer.clip.bottom = clipbottom;
	}
	else
		layer.style.clip = 'rect(' + cliptop + ' ' +  clipright + ' ' + clipbottom + ' ' + clipleft +')';
}

function getClipLeft(layer) {
	if (isNS)
		return(layer.clip.left);
	else {
		var str = layer.style.clip;
		if (!str)
			return(0);
		var clip = getIEClipValues(layer.style.clip);
		return(clip[3]);
	}
}

function getClipTop(layer) {
	if (isNS)
		return(layer.clip.top);
	else {
		var str = layer.style.clip;
		if (!str)
			return(0);
		var clip = getIEClipValues(layer.style.clip);
		return(clip[0]);
	}
}

function getClipRight(layer) {
	if (isNS)
		return(layer.clip.right);
	else {
		var str = layer.style.clip;
		if (!str)
			return(layer.style.pixelWidth);
		var clip = getIEClipValues(layer.style.clip);
		return(clip[1]);
	}
}

function getClipBottom(layer) {
	if (isNS)
		return(layer.clip.bottom);
	else {
		var str = layer.style.clip;
		if (!str)
			return(layer.style.pixelHeight);
		var clip = getIEClipValues(layer.style.clip);
		return(clip[2]);
	}
}

function getClipWidth(layer) {
	if (isNS)
		return(layer.clip.width);
	else {
		var str = layer.style.clip;
		if (!str)
			return(layer.style.pixelWidth);
		var clip = getIEClipValues(layer.style.clip);
		return(clip[1] - clip[3]);
	}
}

function getClipHeight(layer) {
	if (isNS)
		return(layer.clip.height);
	else {
		var str = layer.style.clip;
		if (!str)
			return(layer.style.pixelHeight);
		var clip = getIEClipValues(layer.style.clip);
		return(clip[2] - clip[0]);
	}
}

function getIEClipValues(str) {
	var clip = new Array();
	var i;

	// Parse out the clipping values for IE layers.
	i = str.indexOf("(");
	clip[0] = parseInt(str.substring(i + 1, str.length), 10);
	i = str.indexOf(" ", i + 1);
	clip[1] = parseInt(str.substring(i + 1, str.length), 10);
	i = str.indexOf(" ", i + 1);
	clip[2] = parseInt(str.substring(i + 1, str.length), 10);
	i = str.indexOf(" ", i + 1);
	clip[3] = parseInt(str.substring(i + 1, str.length), 10);
	return(clip);
}

function scrollLayerTo(layer, x, y, bound) {
	var dx = getClipLeft(layer) - x;
	var dy = getClipTop(layer) - y;
	scrollLayerBy(layer, -dx, -dy, bound);
}

function scrollLayerBy(layer, dx, dy, bound) {
	var cl = getClipLeft(layer);
	var ct = getClipTop(layer);
	var cr = getClipRight(layer);
	var cb = getClipBottom(layer);

	if (bound) {
		if (cl + dx < 0)
			dx = -cl;
		else if (cr + dx > getWidth(layer))
			dx = getWidth(layer) - cr;

		if (ct + dy < 0)
			dy = -ct;
		else if (cb + dy > getHeight(layer))
			dy = getHeight(layer) - cb;
	}
	clipLayer(layer, cl + dx, ct + dy, cr + dx, cb + dy);
	moveLayerBy(layer, -dx, -dy);
}

function setBgColor(layer, color) {
	if (isNS)
		layer.bgColor = color;
	else
		layer.style.backgroundColor = color;
}

function setBgImage(layer, src) {
	if (isNS)
		layer.background.src = src;
	else
	layer.style.backgroundImage = "url(" + src + ")";
}

function getWindowWidth() {
	if (isNS)
		return(window.innerWidth);
	else
		return(document.body.clientWidth);
}

function getWindowHeight() {
	if (isNS)
		return(window.innerHeight);
	else
		return(document.body.clientHeight);
}

function getPageWidth() {
	if (isNS)
		return(document.width);
	else
		return(document.body.scrollWidth);
}

function getPageHeight() {
	if (isNS)
		return(document.height);
	else
		return(document.body.scrollHeight);
}

function getPageScrollX() {
	if (isNS)
		return(window.pageXOffset);
	else
		return(document.body.scrollLeft);
}

function getPageScrollY() {
	if (isNS)
		return(window.pageYOffset);
	else
		return(document.body.scrollTop);
}
