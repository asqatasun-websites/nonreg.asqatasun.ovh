    /* <![CDATA[ */
    function initDomMenu() {
            var builder = new DomMenuBuilder();
	    var verticalSubMenuOffsetY = -1;
       if(domLib_isIE){
          verticalSubMenuOffsetY = -22;
       }
            
       builder.setSettings(new Hash(
			       'axis','vertical',
			       'subMenuWidthCorrection', -1,  //-1
			       'verticalSubMenuOffsetX', -1,   //-1
			       'verticalSubMenuOffsetY', verticalSubMenuOffsetY,  //-1
			       //     'horizontalSubMenuOffsetX', 1,  
			       'openMouseoverMenuDelay', 50,
			       'closeMouseoutMenuDelay', 200,
			       'distributeSpace', false,
			       'subMenuWidthCorrection', -1,
			       'horizontalSubMenuOffsetX', 1,  
			       'horizontalSubMenuOffsetY', 1,  
			       'expandMenuArrowUrl', '/fileadmin/templates/menu/arrow.gif'
			       ));
       // dommenu ne se positionne pas bien si non visible
       document.getElementById('domMenu_main').style.display = 'block';
       builder.replace('domMenu_main');
    }
    addInitCommand("initDomMenu()");
    /* ]]> */
