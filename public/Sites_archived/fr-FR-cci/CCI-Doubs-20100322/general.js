function MM_showHideLayers() { //v6.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function cacherTout() {
	MM_showHideLayers('ssmpresent','','hide')
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function fermecharge() {
	opener.parent.location="index.html" ;
	window.close() ;
}

function verifyCompatibleBrowser() { 
    this.ver=navigator.appVersion 
    this.dom=document.getElementById?1:0 
    this.ie5=(this.ver.indexOf("MSIE 5")>-1 && this.dom)?1:0; 
    this.ie4=(document.all && !this.dom)?1:0; 
    this.ns5=(this.dom && parseInt(this.ver) >= 5) ?1:0; 
 
    this.ns4=(document.layers && !this.dom)?1:0; 
    this.bw=(this.ie5 || this.ie4 || this.ns4 || this.ns5) 
    return this 
} 
bw=new verifyCompatibleBrowser(); 
 
 
var speed=50; 
 
var loop, timer; 
function ConstructObject(obj,nest) { 
    
	nest=(!nest) ? '':'document.'+nest+'.' 
    this.el=bw.dom?document.getElementById(obj):bw.ie4?document.all[obj]:bw.ns4?eval(nest+'document.'+obj):0; 
    this.css=bw.dom?document.getElementById(obj).style:bw.ie4?document.all[obj].style:bw.ns4?eval(nest+'document.'+obj):0; 
    this.scrollHeight=bw.ns4?this.css.document.height:this.el.offsetHeight 
    this.clipHeight=bw.ns4?this.css.clip.height:this.el.offsetHeight 
    this.up=MoveAreaUp;this.down=MoveAreaDown; 
    this.MoveArea=MoveArea; this.x; this.y; 
    this.obj = obj + "Object" 
    eval(this.obj + "=this") 
    return this 
} 
function MoveArea(x,y) { 
    this.x=x;this.y=y; 
    this.css.left=this.x; 
    this.css.top=this.y; 
} 
 
 
function MoveAreaDown(move) { 
	if(this.y>-this.scrollHeight+objContainer.clipHeight) { 
    this.MoveArea(0,this.y-move); 
    if(loop) {setTimeout(this.obj+".down("+move+")",speed);} 
	} 
} 
function MoveAreaUp(move) { 
	if(this.y<0) { 
    this.MoveArea(0,this.y-move); 
    if(loop) {setTimeout(this.obj+".up("+move+")",speed);} 
	} 
} 
function PerformScroll(speed) { 
	if(initialised) { 
		loop=true; 
		if(speed>0) {objScroller.down(speed);} 
		else {objScroller.up(speed);} 
	} 
} 
 
function CeaseScroll() { 
    loop=false; 
    if(timer) {clearTimeout(timer);} 
} 
var initialised; 
function InitialiseScrollableArea(){ 
    objContainer=new ConstructObject('divContainer') 
    objScroller=new ConstructObject('divContent','divContainer') 
    objScroller.MoveArea(0,0) 
    objContainer.css.visibility='visible' 
    initialised=true; 
} 

//*****************************************************************************************
//D'autres scripts sur http://www.toutjavascript.com
//Si vous utilisez ce script, merci de m'avertir !  < webmaster@toutjavascript.com >
//Auteur original :Olivier Hondermarck  <webmaster@toutjavascript.com>
//Modifs compatibilit� Netscape 6/Mozilla : C�dric Lamalle 09/2001 <cedric@cpac.embrapa.br>
//*****************************************************************************************

var IB=new Object;
var posX=0;posY=0;
var xOffset=10;yOffset=10;
function AffBulle(texte) {
  contenu="<TABLE border=0 cellspacing=0 cellpadding="+IB.NbPixel+"><TR bgcolor='"+IB.ColContour+"'><TD><TABLE border=0 cellpadding=2 cellspacing=0 bgcolor='"+IB.ColFond+"'><TR><TD><FONT size='-1' face='arial' color='"+IB.ColTexte+"'>"+texte+"</FONT></TD></TR></TABLE></TD></TR></TABLE>";
  var finalPosX=posX-xOffset;
  if (finalPosX<0) finalPosX=0;
  if (document.layers) {
    document.layers["bulle"].document.write(contenu);
    document.layers["bulle"].document.close();
    document.layers["bulle"].top=posY+yOffset;
    document.layers["bulle"].left=finalPosX;
    document.layers["bulle"].visibility="show";}
  if (document.all) {
    //var f=window.event;
    //doc=document.body.scrollTop;
    bulle.innerHTML=contenu;
    document.all["bulle"].style.top=posY+yOffset;
    document.all["bulle"].style.left=finalPosX;//f.x-xOffset;
    document.all["bulle"].style.visibility="visible";
  }
  //modif CL 09/2001 - NS6 : celui-ci ne supporte plus document.layers mais document.getElementById
  else if (document.getElementById) {
    document.getElementById("bulle").innerHTML=contenu;
    document.getElementById("bulle").style.top=posY+yOffset;
    document.getElementById("bulle").style.left=finalPosX;
    document.getElementById("bulle").style.visibility="visible";
  }
}
function getMousePos(e) {
  if (document.all) {
  posX=event.x+document.body.scrollLeft; //modifs CL 09/2001 - IE : regrouper l'�v�nement
  posY=event.y+document.body.scrollTop;
  }
  else {
  posX=e.pageX; //modifs CL 09/2001 - NS6 : celui-ci ne supporte pas e.x et e.y
  posY=e.pageY;
  }
}
function HideBulle() {
	if (document.layers) {document.layers["bulle"].visibility="hide";}
	if (document.all) {document.all["bulle"].style.visibility="hidden";}
	else if (document.getElementById){document.getElementById("bulle").style.visibility="hidden";}
}

function InitBulle(ColTexte,ColFond,ColContour,NbPixel) {
	IB.ColTexte=ColTexte;IB.ColFond=ColFond;IB.ColContour=ColContour;IB.NbPixel=NbPixel;
	if (document.layers) {
		window.captureEvents(Event.MOUSEMOVE);window.onMouseMove=getMousePos;
		document.write("<LAYER name='bulle' top=0 left=0 visibility='hide'></LAYER>");
	}
	if (document.all) {
		document.write("<DIV id='bulle' style='position:absolute;top:0;left:0;visibility:hidden;z-index:9999'></DIV>");
		document.onmousemove=getMousePos;
	}
	//modif CL 09/2001 - NS6 : celui-ci ne supporte plus document.layers mais document.getElementById
	else if (document.getElementById) {
	        document.onmousemove=getMousePos;
	        document.write("<DIV id='bulle' style='position:absolute;top:0;left:0;visibility:hidden;z-index:9999'></DIV>");
	}

}

//*********** cote formulaire ********************

function FP_SelText(oZone) {
	var LaSelection = '';
	if (document.all && oZone.createTextRange) {
		LaSelection = document.selection.createRange().text;
   		oZone.oSelection = document.selection.createRange().duplicate();
	}
	return LaSelection;
}

function FP_MakeHTML(oZone,sTag) {
	var LaSelection = FP_SelText(oZone);
	if (LaSelection.length > 0) {
		LaSelection = '<' + sTag + '>' + LaSelection + '<fin>'
		with (oZone.oSelection) {
			text = LaSelection
			moveStart('word',-1)
			moveEnd('word')
		}
		FP_SelText(oZone)
	}
}

function FP_StripHTML(oZone) {
	var LaSelection = FP_SelText(oZone);
	var iMax = LaSelection.length;
	if ( iMax > 0) {
		var Ok = true;
		var sTxt = '';
		var Car = '';
		for (var i = 0; i < iMax; i++) {
			Car = LaSelection.charAt(i);
			if (Car == '<') Ok = false;
			if (Ok) sTxt += Car;		
			if (Car == '>') Ok = true;
		}
		oZone.oSelection.text = sTxt
		FP_SelText(oZone)
	}	
}

//*********** menus d�roulant ********************
function BW_centerLayers() { //v4.1.1
	if (document.layers || document.all || document.getElementById)
		{
		onresize = BW_reload;

		winWidth = (document.all)?document.body.clientWidth:window.innerWidth;
		winHeight = (document.all)?document.body.clientHeight:window.innerHeight;
		}
}

function BW_reload(){
	location.reload();
}

