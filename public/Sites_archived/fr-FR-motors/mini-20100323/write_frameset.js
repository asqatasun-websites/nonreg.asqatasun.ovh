// Version       : 1.3
// Last modified : (E.Suerek) 30.01.2006 - implementation of onloadFunctions
//                 (E.Suerek) 22.09.2004 - implementation of onUnloadFunctions
// Author        : BBDO InterOne Hamburg (W. Rauch)
// Copyright     : BMW Group 2002-2004

// Unique scripting solutions that are provided for  BMW Group sites are for use exclusively
// within  BMW Group projects.  No other use of these solutions is permitted.

if(top.frames.length != 0){
  if(self.location.search == ""){
    top.location.href = self.location.href;
  } else {
    top.location.href = self.location.href.substring(0, self.location.href.indexOf(self.location.search));
  }
}
onloadFunctions          = ((typeof onloadFunctions   != "undefined") ? onloadFunctions   : "");
onUnloadFunctions        = ((typeof onUnloadFunctions != "undefined") ? onUnloadFunctions : "");
var topFrameHeight       = 93;
var bottomFrameHeight    = ((document.layers) ? 17:19);
var windowHeight         = window.innerHeight;
var height_percent       = (windowHeight/100);
var nn4TopFrameHeight    = 0;
var nn4BottomFrameHeight = 0;
var checkTopHeight       = true;
var checkBottomHeight    = true;
var topFrameName         = "frameTop";
var contentFrameName     = "frameContent";
var bottomFrameName      = "frameBottom";
var framesetHeader       = "";
var topFrame;
var contentFrame;
var bottomFrame;

// This script defines the heights of the Top and bottom Frames depending on the Browser being used.
// The variable framesetHeader, which contains the HTML-Code for the "frameset"-Tag, is constructed from these values
if (document.layers) {
  for (i = 1; i < 100; i++) {
    if (checkTopHeight && Math.floor(height_percent*i) >= topFrameHeight) {
      nn4TopFrameHeight = i;
      checkTopHeight = false;
    }
    if (checkBottomHeight && Math.floor(height_percent*i) >= bottomFrameHeight) {
      nn4BottomFrameHeight = i;
      checkBottomHeight = false;
    }
  }
  framesetHeader = '<frameset rows="' + nn4TopFrameHeight + '%,*,' + nn4BottomFrameHeight + '%" framespacing="0" frameborder="no" border="0"' + onloadFunctions + " " + onUnloadFunctions + '>';
} else {
  framesetHeader = '<frameset rows="' + topFrameHeight + ',*,' + bottomFrameHeight + '" framespacing="0" frameborder="no" border="0"' + onloadFunctions + " " + onUnloadFunctions + '>';
}

function setMacVariables(){
  topFrame       = parent.frames[0]; //Top-Frame Object
  contentFrame   = parent.frames[1]; //Content-Frame Object
  bottomFrame    = parent.frames[2]; //Bottom-Frame Object
  window.clearTimeout(macTimer);
}

// The frameset is written here
macTimer = window.setTimeout('setMacVariables()',500);
document.writeln(framesetHeader);
document.writeln('<frame src="' + topFrameSource    + topFrameParam    + '" name="'+ topFrameName     +'" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"   noresize="noresize" />');
document.writeln('<frame src="' + initContentURL    +                    '" name="'+ contentFrameName +'" frameborder="0" marginwidth="0" marginheight="0" scrolling="auto" noresize="noresize" />');
document.writeln('<frame src="' + bottomFrameSource + bottomFrameParam + '" name="'+ bottomFrameName  +'" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"   noresize="noresize" />');
document.writeln('</frameset>');

if(navigator.appVersion.indexOf("Mac") == -1) {
  topFrame       = parent.frames[0]; //Top-Frame Object
  contentFrame   = parent.frames[1]; //Content-Frame Object
  bottomFrame    = parent.frames[2]; //Bottom-Frame Object
}
