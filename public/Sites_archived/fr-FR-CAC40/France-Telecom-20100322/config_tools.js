		//Tableau des id et des pages gabarits des boites outils
		var tab_pages_outils = [];
		// Chemins des diff�rents documents param�trant les outils
		var sCheminPageStock = 'http://francetelecom.is-teledata.com/javatools/share_price.html?ID_NOTATION=195948&STARTWIDTH=&STARTHEIGHT=&LANG=fr';

		// configuration des outils
		// chaque outil a sa propre configuration
		// config = new tools_config('Libell�',id de loutil,'nom du lien',
		// 'lien','nom du lien edit','valeur par defaut du filtre','lien xml')
		// mettre � vide s'il n y'a pas de valeur
		// attention pour les filtres par defaut de respecter les majuscules si besoin
		var tools_config = new Class({
			initialize: function(libelle,id_outil,name_link,the_link,edit,libelle_defaut,link_xml,swf_large){
				this.libelle = libelle;
				this.id_outil = id_outil;
				this.name_link = name_link;
				this.the_link  = the_link;
				this.edit = edit;
				this.libelle_defaut = libelle_defaut;
				this.link_xml = link_xml;
				this.swf_large = swf_large;
			}
		});
		var outils_id = [];
		var b_outils = [];
		var outils_coordinates = [];
		var sCheminXMLNavDroite = '/fr_FR/outils.xml';

		
		var config_alaune = new tools_config('','outil_alaune','','','','','','');outils_id.push('alaune_element');b_outils.push('/fr_FR/tools/b_alaune.html');
		tab_pages_outils['alaune_element'] = '/fr_FR/tools/b_alaune.html';
		outils_coordinates['alaune_element']={'x':'0', 'y':''};
		var config_hightlights = new tools_config('','outil_highlight','','','','','','#');outils_id.push('highlight_element');b_outils.push('/fr_FR/tools/b_hilight.html');
		tab_pages_outils['highlight_element'] = '/fr_FR/tools/b_hilight.html';
		
		var config_news = new tools_config('','outil_news','','','','','','/fr_FR/att00008593/box_news_big_V2.swf');outils_id.push('curiculum-vitae_element');b_outils.push('/fr_FR/tools/b_news.html');
		tab_pages_outils['curiculum-vitae_element'] = '/fr_FR/tools/b_news.html';
		
		var config_bubble = new tools_config('','outil_bubble','','','','','','');outils_id.push('bubble_element');b_outils.push('/fr_FR/tools/b_bubble.html');
		tab_pages_outils['bubble_element'] = '/fr_FR/tools/b_bubble.html';
		outils_coordinates['bubble_element']={'x':'0', 'y':'326'};
		var config_media = new tools_config('','outil_media','acc\u00E8s','http://www.mediatheque.orange.com/','','','/fr_FR/');outils_id.push('televideo_element');b_outils.push('/fr_FR/tools/b_mediacenter.html');
		tab_pages_outils['televideo_element'] = '/fr_FR/tools/b_mediacenter.html';
		
		var config_bookmarks = new tools_config('','outil_bookmark','tous','/fr_FR/outils/boites/bookmarks/index.jsp','','','/fr_FR/');outils_id.push('bookmarks_element');b_outils.push('/fr_FR/tools/b_bookmarks.html');
		tab_pages_outils['bookmarks_element'] = '/fr_FR/tools/b_bookmarks.html';
		var sNbrArticlesPageBookmarks = 10;
		var config_docs = new tools_config('','outil_doccenter','tous','/fr_FR/outils/boites/documents/documentation.jsp','filtrer','','/fr_FR/outils/boites/documents/index.xml');outils_id.push('docs_center_element');b_outils.push('/fr_FR/tools/b_doccenter.html');
		tab_pages_outils['docs_center_element'] = '/fr_FR/tools/b_doccenter.html';
		
		var libelle_all_doccenter = 'tous';
		var sNbrArticlesPageDoccenter = 0;
		var config_mymag = new tools_config('','outil_mymag','','','','','/fr_FR/outils/boites/magazines/index.xml');outils_id.push('mymag_element');b_outils.push('/fr_FR/tools/b_mymag.html');
		tab_pages_outils['mymag_element'] = '/fr_FR/tools/b_mymag.html';
		var libelle_all_mymag = 'tous';var sNbrArticlesPageMymag = 10;
		var config_products = new tools_config('','outil_products','','','','','/fr_FR/outils/boites/produits/index.xml');outils_id.push('production_element');b_outils.push('/fr_FR/tools/b_products.html');
		tab_pages_outils['production_element'] = '/fr_FR/tools/b_products.html';
		outils_coordinates['production_element']={'x':'300', 'y':'325'};var select_pays_product = "pays";var select_services_product = "Produits &amp; Services";var select_site_product = "Sites";
		var config_press = new tools_config('','outil_presse','tous','/fr_FR/presse/communiques/index.jsp','filtrer','','/fr_FR/presse/communiques/index.xml,/fr_FR/presse/communiques/index_light.xml');outils_id.push('press_releases_element');b_outils.push('/fr_FR/tools/b_press.html');
		tab_pages_outils['press_releases_element'] = '/fr_FR/tools/b_press.html';
		outils_coordinates['press_releases_element']={'x':'0', 'y':'162'};
		var libelle_all_press = 'tous';
		var sNbrArticlesPagePresse = 10;
		var config_jobs = new tools_config('','outil_job','tous','http://jobs.orange.com/jobs/fr.do','','','/fr_FR/');outils_id.push('jobs_element');b_outils.push('/fr_FR/tools/b_joinus.jsp');
		tab_pages_outils['jobs_element'] = '/fr_FR/tools/b_joinus.jsp';
		
		var config_calendar = new tools_config('','outil_calendar','voir','/fr_FR/outils/boites/calendrier/index.jsp','filtrer','collectivit\u00E9s locales,finance,groupe,m\u00E9c\u00E9nat,recrutement','/fr_FR/outils/boites/calendrier/index.xml,/fr_FR/outils/boites/calendrier');outils_id.push('calendar_element');b_outils.push('/fr_FR/tools/b_calendar.html');
		tab_pages_outils['calendar_element'] = '/fr_FR/tools/b_calendar.html';
		outils_coordinates['calendar_element']={'x':'300', 'y':'162'};
		var libelle_all_calendar = 'tous';
	
		var config_share = new tools_config('','outil_share','voir','/fr_FR/finance/action-capital/fiche/index.jsp','','','');outils_id.push('stocks_element');b_outils.push('/fr_FR/tools/b_share.html');
		tab_pages_outils['stocks_element'] = '/fr_FR/tools/b_share.html';
		
		var config_glossary = new tools_config('','outil_glossary','','','','','/fr_FR/outils/boites/glossaire/index.xml');outils_id.push('glossary_element');b_outils.push('/fr_FR/tools/b_glossary.html');
		tab_pages_outils['glossary_element'] = '/fr_FR/tools/b_glossary.html';
		
		var config_flash = new tools_config('','outil_flash','tous','/fr_FR/outils/boites/flashcards/index.jsp','',' ','/fr_FR/outils/boites/flashcards/index.xml');outils_id.push('cards_element');b_outils.push('/fr_FR/tools/b_cards.html');
		tab_pages_outils['cards_element'] = '/fr_FR/tools/b_cards.html';
		
		var config_poll = new tools_config('','outil_poll','les r\u00E9sultats','/fr_FR/outils/dites-nous/index.jsp','','00000044','/do/poller/');var config_poll_int = new tools_config('','outil_poll','les r\u00E9sultats','/fr_FR/outils/dites-nous/index.jsp','','00000044','/do/poller/');var config_poll_moodal = new tools_config('','outil_poll','les r\u00E9sultats','/fr_FR/outils/dites-nous/index.jsp','','00000044','/do/poller/');outils_id.push('poll_element');b_outils.push('/fr_FR/tools/b_poll.html');
		tab_pages_outils['poll_element'] = '/fr_FR/tools/b_poll.html';
		outils_coordinates['poll_element']={'x':'454', 'y':'324'};

		var tools = b_outils.associate(outils_id);


		var image_chargement = "/fr_FR/att00008593/loader.gif";
		var sLangSite = "fr";

		// Messages multilingues
		var bookmarks_message_default = [];
		var nav_message_default = [];
		var navdroite_message_default = [];
		var calendar_noevent_libelle = [];
		var sLblTxtLegalDoc = [];
		var sLblTxtOtherDoc = [];
		var sLblLinkRead = [];
		var sLblLinkDownload = [];
		var sLblLinkArchive = [];
		var sMsgGlossaire = [];
		var vNbCharsByLine = 32;

		/* POLLER */
		var useCookiesToRememberCastedVotes = true;

		

		nav_message_default['fr'] = "glissez et d\u00E9posez pour personnaliser votre page";

		bookmarks_message_default['fr'] = "vous n\'avez pas de signet";

		navdroite_message_default['fr'] = "";

		calendar_noevent_libelle['fr'] = "";

		sLblTxtLegalDoc['fr'] = "documents l\u00E9gaux";

		sLblTxtOtherDoc['fr'] = "autres documents";

		sLblLinkRead['fr'] = "lire ";

		sLblLinkDownload['fr'] = "t\u00E9l\u00E9chargement ";

		sLblLinkArchive['fr'] = "archive ";

		sMsgGlossaire['fr'] = "\u00E9crivez le mot, le sigle ou l\'expression dont vous cherchez la d\u00E9finition dans le champ,<br/>cliquez sur le bouton.";//Messages
		var products_message_tools = "Vous cherchez un produit ou un service ?<br/>Utilisez les crit\u00E8res de recherche ci-contre pour le trouver.";
		var glossary_empty_message = "d\u00E9sol\u00E9, ce terme ne fait pas partie du glossaire,<br/>essayez une autre d\u00E9finition.";