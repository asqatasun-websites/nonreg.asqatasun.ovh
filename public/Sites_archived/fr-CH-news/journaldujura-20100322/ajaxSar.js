var Netscape = new Boolean();
navigator.appName == "Netscape"?Netscape=true:Netscape=false;

var strFailure = "";

function createXMLHttp(){
	if(typeof XMLHttpRequest != "undefined"){
		return new XMLHttpRequest();
	}else if(window.ActiveXObject){
		var arrOfVersions = [
			"MSXML2.XMLHttp.5.0",
			"MSXML2.XMLHttp.4.0",
			"MSXML2.XMLHttp.3.0",
			"MSXML2.XMLHttp",
			"Microsoft.XMLHttp"
		];
		for(var i = 0; i < arrOfVersions.length; i++){
			try{
				var oXmlHttp = new ActiveXObject(arrOfVersions[i]);
				if(oXmlHttp){
					//break;	TH auskommentiert am 19. Nov f�r IE 6 R�ckw�rtskompatibilit�t
					return oXmlHttp;
				}

			}catch(oError){
				alert("Error, update your browser... " + oError.description);
			}
		}
	}
}

//Linus Vorshclag
/*
for(var i = 0; i < arrOfVersions.length; i++){
	try{
		var oXmlHttp = new ActiveXObject(arrOfVersions[i]);
		if(oXmlHttp){
			break;	
			return oXmlHttp;
		}

	}catch(oError){
		alert("Error, update your browser... " + oError.description);
	}
}

*/

function loadFile(strFile, strContainer, bAsync, intCacheTime, strCallBack){
	//debug("loadFile " + strFile + " cont :" + strContainer + "<br/>", true);
	try{
		intCache = 0;
		if(typeof intCacheTime != "undefined"){
			intCache = intCacheTime;
		}
		/* Searches if given container is in a form so we will send a POST request */
		objForm = getForm(document.getElementById(strContainer));
		strTransport = "GET";
		strElems = null;
		if(objForm.nodeName == "FORM"){
			strTransport = "POST";
			strElems = getElems(objForm);
			strElems = addUrlParams(strElems, strFile);
		}
		var oXMLHttp = createXMLHttp();
		strFile = doCacheFile(strFile,intCache);
		oXMLHttp.open(strTransport,strFile,bAsync);
		if(objForm.nodeName == "FORM"){
			//oXMLHttp.setRequestHeader('Content-Type', 'multipart/form-data');
			oXMLHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;');		
		}
		try{
			oXMLHttp.onreadystatechange = function(){
				if(oXMLHttp.readyState == 4){
					if(oXMLHttp.status == 200){
						if(strContainer.length > 0){
							processFileData(oXMLHttp.responseText,strContainer);
							if(strCallBack){
								eval(strCallBack);
							}
						}else{
							alert("Keine Angabe 'container'");
						}
					}else if(oXMLHttp.status == 0){
						alert("Kein server welcher ein Status ausgeben kann gefunden");
					}else{
						alert("Server status = " + oXMLHttp.status);
					}
				}
			}
			
			oXMLHttp.send(strElems);
		}catch(exSend){
			alert("a) " + exSend.description);
		}
	}catch(ex){
		var strFailure = ex.description + 
			"\n\nstrFile = " + strFile + 
			"\nstrContainer = " + strContainer + 
			"\nbAsync = " + bAsync;
		alert(strFailure);
	}
}

function processFileData(strText, strContainerId){
	if(strText.length > 0){
		//alert(strText);
		document.getElementById(strContainerId).innerHTML = strText;
		evaluateScriptData(strText);
	}else{
		document.getElementById(strContainerId).innerHTML = "<img src='images/spacer.gif' border='0' />";
	}
}

function evaluateScriptData(strText){
	strRegExpression = '(?:<script.*?>)((\n|\r|.)*?)(?:<\/script>)';
	var strMatch = new RegExp(strRegExpression,'img');
	var strTextData = strText.replace(strMatch,'');
	var strScriptData = strText.match(strMatch);
	strMatchScript = new RegExp(strRegExpression, 'im');
	if(strScriptData && typeof strMatchScript != "undefined"){
		for(var i = 0; i < strScriptData.length; i++){
			scriptCode = strScriptData[i].match(strMatchScript)[1];
		   eval(scriptCode);
		}
	}
}

function getForm(objElem){
	while(objElem.nodeName != "FORM" && objElem.nodeName != "BODY"){
			objElem = objElem.parentNode;
	}
	return objElem;
}

function getElems(objForm){
	strDelim = "";
	strReturn = "";
	for(var i = 0; i < objForm.elements.length; i++){
		if(objForm.elements[i].name.length > 0){
			strReturn += strDelim + objForm.elements[i].name + "=" + encodeURIComponent(objForm.elements[i].value);
			strDelim = "&";
		}
	}
	return strReturn;
}

function doCacheFile(strFile, intCacheTime){
	intCache = 0;
	if(typeof intCacheTime != "undefined"){
		intCache = intCacheTime;
	}
	if(intCache == 0){
		strNoCacheParam = "tmstmp=" + parseInt(new Date().getTime());
	}else{
		strNoCacheParam = "tmstmp=" + (parseInt(new Date().getTime() / 1000) * intCache);	
	}
	if(strFile.indexOf("?") > -1){
		strFile += "&" + strNoCacheParam;
	}else{
		strFile += "?" + strNoCacheParam;
	}
	return strFile;
}

function addUrlParams(strElems, strFile){
	if(strFile.indexOf('?') > -1){
		if(strElems.indexOf('&') > -1){
			strElems = strElems + "&" + strFile.substring(strFile.indexOf('?')+1)
		}else{
			strElems = strFile.substring(strFile.indexOf('?')+1)
		}
	}
	return strElems;
}

function debug(strMessage, blnAppend){
	if((typeof document.getElementById("debug") == "undefined") || (document.getElementById("debug") == null)){
		//root = document.getElementsByTagName("BODY")[0];
		elmDebug = document.createElement("div");
		elmDebug.id = "debug";
		document.body.appendChild(elmDebug);
	}
	objDebug = document.getElementById("debug");
	objDebug.style.border = "2px solid red";
	if(blnAppend){
		objDebug.innerHTML = objDebug.innerHTML + strMessage;
	}else{
			objDebug.innerHTML = strMessage;
	}
}

function trimLeft(strTrim){
	while (strTrim.substring(0,1) == ' ') {
    strTrim = strTrim.substring(1,strTrim.length);
  }
	return strTrim;
}

function trimRight(strTrim){
	while (strTrim.substring(strTrim.length-1,strTrim.length) == ' ') {
    strTrim = strTrim.substring(0,strTrim.length-1);
  }
  return strTrim;
}

function trim(strTrim){
	return strTrim.replace(/\s/g,'');
}

function test()
{
	alert("test");
}
