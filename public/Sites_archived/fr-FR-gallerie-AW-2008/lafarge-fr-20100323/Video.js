function $i(id) { return document.getElementById(id); }
function $r(parent,child) { (document.getElementById(parent)).removeChild(document.getElementById(child)); }
function $t(name) { return document.getElementsByTagName(name); }
function $c(code) { return String.fromCharCode(code); }
function $h(value) { return ('0'+Math.max(0,Math.min(255,Math.round(value))).toString(16)).slice(-2); }
function $0(value) { return (((value.toString()).length<2)?'0':'')+value; }
function _i(id,value) { $t('div')[id].innerHTML+=value; }

var wmp_client=((navigator.appName=='Microsoft Internet Explorer')?'ie':(((navigator.appVersion).indexOf('Safari')!=-1)?'sa':((navigator.appName=='Opera')?'op':'ff')));

var wmp_state=new Array();
wmp_state[0]='undefined';
wmp_state[1]='stopped';
wmp_state[2]='paused';
wmp_state[3]='playing';
wmp_state[4]='scan forward';
wmp_state[5]='scan reverse';
wmp_state[6]='buffering';
wmp_state[7]='waiting';
wmp_state[8]='media ended';
wmp_state[9]='transitioning';
wmp_state[10]='ready';
wmp_state[11]='reconnecting';

var wmp_id=0;
var wmp_action=new Array();
var wmp_status=new Array();
var wmp_playcount=new Array();
var wmp_file=new Array();
var wmp_duration=new Array();
var wmp_rate=new Array();
var wmp_volume=new Array();
var wmp_object=new Array();
var wmp_buffering=new Array();
var wmp_debug_state=new Array();
var wmp_timer_left=new Array();
var wmp_timer_right=new Array();
var wmp_slider=new Array();
var wmp_slider_w=new Array();
var wmp_slider_cursor_x=new Array();
var wmp_slider_cursor_y=new Array();
var wmp_slider_cursor_w=new Array();
var wmp_slider_mouse_x=new Array();

var bundle=new Array();
var button=new Array();
var gfx_path;
var current_wmp=0;
var volume_rate=10;
var latency=500;
var debug=false;

document.onmousemove=move;
document.onmouseup=release;

function video_player_bundle(string)
	{
	bundle=string.split(';');
	}

function new_image(file)
	{
	var image=new Image();
	image.src=file;
	return image;
	}

function video_player_preload(string)
	{
	gfx_path=string;
	button[0]=new_image(gfx_path+'pause_off.gif');
	button[1]=new_image(gfx_path+'pause_on.gif');
	button[2]=new_image(gfx_path+'play_off.gif');
	button[3]=new_image(gfx_path+'play_on.gif');
	button[4]=new_image(gfx_path+'stop_off.gif');
	button[5]=new_image(gfx_path+'stop_on.gif');
	button[6]=new_image(gfx_path+'rewind_off.gif');
	button[7]=new_image(gfx_path+'rewind_on.gif');
	button[8]=new_image(gfx_path+'forward_off.gif');
	button[9]=new_image(gfx_path+'forward_on.gif');
	button[10]=new_image(gfx_path+'volume_less_off.gif');
	button[11]=new_image(gfx_path+'volume_less_on.gif');
	button[12]=new_image(gfx_path+'volume_off.gif');
	button[13]=new_image(gfx_path+'volume_on.gif');
	button[14]=new_image(gfx_path+'volume_more_off.gif');
	button[15]=new_image(gfx_path+'volume_more_on.gif');
	button[16]=new_image(gfx_path+'download_off.gif');
	button[17]=new_image(gfx_path+'download_on.gif');
	}

function video_player(file,con,w,h,duration,start,loop,download)
	{
	wmp_id++;
	wmp_duration[wmp_id]=duration;
	wmp_rate[wmp_id]=1;
	wmp_volume[wmp_id]=50;
	wmp_action[wmp_id]=(start)?'pause':'play';
	wmp_playcount[wmp_id]='count_'+((loop)?'loop':'1x');
	wmp_file[wmp_id]=file;
	wmp_buffering[wmp_id]=-1;
	initialize_video_player(con,w,h,start,download,50);
	setTimeout('update_video_player('+w+','+h+','+duration+')',100);
	setTimeout('anim('+wmp_id+')',150);
	}

function update_video_player(w,h,duration)
	{
	if(wmp_client=='ie')
		{
		wmp_object[wmp_id]=$t('object')[wmp_id-1]; //alert('id='+wmp_id+' -> '+wmp_object[wmp_id])
		wmp_slider[wmp_id]=$i('video_player_slider_'+wmp_id);
		wmp_timer_left[wmp_id]=$i('video_bar_left_'+wmp_id);
		wmp_timer_right[wmp_id]=$i('video_bar_right_'+wmp_id);
		wmp_slider_w[wmp_id]=(w-30-33-4-4);
		wmp_slider_cursor_w[wmp_id]=Math.round(wmp_slider_w[wmp_id]/(duration/10)+6);
		wmp_slider_cursor_x[wmp_id]=1;
		wmp_slider_cursor_y[wmp_id]=2;
		wmp_slider[wmp_id].style.width=wmp_slider_cursor_w[wmp_id]+'px';
		$i('video_bar_center_'+wmp_id).style.width=(wmp_slider_w[wmp_id]+4)+'px';
		$i('video_player_slider_center_'+wmp_id).style.width=(wmp_slider_cursor_w[wmp_id]-6)+'px';
		$i('video_player_slider_right_'+wmp_id).style.left=($i('video_player_slider_center_'+wmp_id).offsetLeft+$i('video_player_slider_center_'+wmp_id).offsetWidth)+'px';
		wmp_slider[wmp_id].style.left=wmp_slider_cursor_x[wmp_id]+'px';
		wmp_slider[wmp_id].style.top=wmp_slider_cursor_y[wmp_id]+'px';
		if(debug)
			{
			$i('video_player_version_'+wmp_id).innerHTML='wmp version '+wmp_object[wmp_id].versionInfo;
			wmp_debug_state[wmp_id]=$i('video_player_state_'+wmp_id);
			}
		}
	}

function initialize_video_player(con,w,h,start,download)
	{
	var video_title=wmp_file[wmp_id].replace(/_/gi,' ');
	video_title=video_title.substring(video_title.indexOf('/')+1,video_title.length-4);
	video_title=(video_title.substr(0,1)).toUpperCase()+video_title.substring(1,video_title.length);
	var video_format=wmp_file[wmp_id].substring(wmp_file[wmp_id].length-3,wmp_file[wmp_id].length);
	var string='<div id="video_player_'+wmp_id+'" class="video_player">\n';
	string+='	<div class="screen" style="width:'+w+'px;height='+h+'px">\n';
	string+='		<object type="application/x-oleobject" id="video_plugin_'+wmp_id+'" width="'+w+'px" height="'+h+'px" classid="CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6">\n';
	string+='			<param name="URL" VALUE="'+wmp_file[wmp_id]+'">\n';
	string+='			<param name="SendPlayStateChangeEvents" value="true">\n';
	string+='			<param name="autoStart" value="'+((start)?'1':'0')+'">\n';
	string+='			<param name="uiMode" value="none">\n';
	string+='			<param name="PlayCount" value="'+((wmp_playcount[wmp_id]=='count_1x')?'1':'1000000')+'">\n';
	string+='			<param name="enableContextMenu" value="false">\n';
	//string+='			<param name="ShowWhileBuffering" value="true">\n';
	string+='			<param name="volume" value="'+wmp_volume[wmp_id]+'">\n';
	string+='			<embed type="application/x-mplayer2" id="video_plugin_'+wmp_id+'" name="video_plugin_'+wmp_id+'" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" ';
	string+='				src="'+wmp_file[wmp_id]+'" ';
	string+='				width="'+w+'px" height="'+(h+45)+'px" ';
	string+='				controlType="2" ';
	string+='				showControls="1" ';
	string+='				showStatusbar="0" ';
	string+='				enableContextMenu="0" ';
	//string+='				showWhileBuffering="1" ';
	string+='				volume="'+wmp_volume[wmp_id]+'" ';
	string+='				autoStart="'+((start)?'1':'0')+'" ';
	string+='				playcount="'+((wmp_playcount[wmp_id]=='count_1x')?'1':'0')+'">\n';
	string+='			</embed>\n';
	string+='		</object>\n';
	string+='	</div>\n';
	if(wmp_client=='ie')
		{
		string+='	<div id="video_bar_'+wmp_id+'" class="bar" style="width:'+w+'px">\n';
		string+='		<div id="video_bar_left_'+wmp_id+'" class="left">'+format_time(0)+'</div>\n';
		string+='		<div class="left_space"></div>\n';
		string+='		<div id="video_bar_center_'+wmp_id+'" class="center">\n';
		string+='			<div id="video_player_slider_'+wmp_id+'" class="slider" onmousedown="wmp_slider_mouse_x['+wmp_id+']=wmp_slider['+wmp_id+'].offsetLeft-window.event.x;current_wmp='+wmp_id+'">\n';
		string+='				<div id="video_player_slider_left_'+wmp_id+'" class="left"></div>\n';
		string+='				<div id="video_player_slider_center_'+wmp_id+'" class="center" style="background-image:url(\''+gfx_path+'slider.gif\')"></div>\n'; //bug IE!
		string+='				<div id="video_player_slider_right_'+wmp_id+'" class="right"></div>\n';
		string+='			</div>\n\n';
		string+='		</div>\n';
		string+='		<div id="video_bar_right_'+wmp_id+'" class="right">'+format_time(-wmp_duration[wmp_id])+'</div>\n';
		string+='		<div class="right_space"></div>\n';
		string+='	</div>\n';
		if(w>320||download=='')
			{
			string+='	<div class="button" style="width:'+Math.round((((download!='')?w:w+54)-320)/2-3)+'px"></div>\n';
			string+='	<div class="space"></div>\n';
			}
		string+='	<div class="button" style="width:47px">\n';
		string+='		<a onmousedown="video_action('+wmp_id+',wmp_action['+wmp_id+'],\'press\')" onmouseup="video_action('+wmp_id+',wmp_action['+wmp_id+'],\'release\')"><img id="video_play_'+wmp_id+'" src="'+gfx_path+wmp_action[wmp_id]+'_off.gif" alt="'+bundle[(wmp_action[wmp_id]=='pause')?0:1]+'"></img></a>\n';
		string+='		<span id="video_play_caption_'+wmp_id+'">'+bundle[(wmp_action[wmp_id]=='pause')?0:1]+'</span>\n';
		string+='	</div>\n';
		string+='	<div class="space"></div>\n';
		string+='	<div class="button" style="width:47px">\n';
		string+='		<a onmousedown="video_action('+wmp_id+',\'stop\',\'press\')" onmouseup="video_action('+wmp_id+',\'stop\',\'release\')"><img id="video_stop_'+wmp_id+'" src="'+gfx_path+'stop_off.gif" alt="'+bundle[2]+'"></img></a>\n';
		string+='		<span id="video_stop_caption_'+wmp_id+'">'+bundle[2]+'</span>\n';
		string+='	</div>\n';
		string+='	<div class="space"></div>\n';
		string+='	<div class="button" style="width:47px">\n';
		string+='		<a onmousedown="video_action('+wmp_id+',\'rewind\',\'press\')" onmouseup="video_action('+wmp_id+',\'rewind\',\'release\')"><img id="video_rewind_'+wmp_id+'" src="'+gfx_path+'rewind_off.gif" alt="'+bundle[3]+'"></img></a>\n';
		string+='		<span id="video_rewind_caption_'+wmp_id+'">'+bundle[3]+'</span>\n';
		string+='	</div>\n';
		string+='	<div class="space"></div>\n';
		string+='	<div class="button" style="width:47px">\n';
		string+='		<a onmousedown="video_action('+wmp_id+',\'forward\',\'press\')" onmouseup="video_action('+wmp_id+',\'forward\',\'release\')"><img id="video_forward_'+wmp_id+'" src="'+gfx_path+'forward_off.gif" alt="'+bundle[4]+'"></img></a>\n';
		string+='		<span id="video_forward_caption_'+wmp_id+'">'+bundle[4]+'</span>\n';
		string+='	</div>\n';
		string+='	<div class="space"></div>\n';
		string+='	<div class="button volume" style="width:65px">\n';
		string+='		<div class="volume_less"><img id="video_volume_less_'+wmp_id+'" src="'+gfx_path+'volume_less_off.gif" style="margin:0;cursor:pointer" alt="'+bundle[5]+'" onmousedown="video_action('+wmp_id+',\'volume_less\',\'press\')" onmouseup="video_action('+wmp_id+',\'volume_less\',\'release\')"></img></div>\n';
		string+='		<div id="video_volume_'+wmp_id+'" class="volume_stop"></div>\n';
		string+='		<div class="volume_more"><img id="video_volume_more_'+wmp_id+'" src="'+gfx_path+'volume_more_off.gif" style="margin:0;cursor:pointer" alt="'+bundle[7]+'" onmousedown="video_action('+wmp_id+',\'volume_more\',\'press\')" onmouseup="video_action('+wmp_id+',\'volume_more\',\'release\')"></img></div>\n';
		string+='		<div id="video_volume_caption_'+wmp_id+'" class="volume_caption">'+bundle[6]+'</div>\n';
		string+='	</div>\n';
		if(download!='')
			{
			string+='	<div class="space"></div>\n';
			string+='	<div class="button" style="width:52px">\n';
			string+='		<a href="'+download+'" onmousedown="video_action('+wmp_id+',\'download\',\'press\')" onmouseup="video_action('+wmp_id+',\'download\',\'release\')"><img id="video_download_'+wmp_id+'" src="'+gfx_path+'download_off.gif" alt="'+bundle[8]+'"></img></a>\n';
			string+='		<span id="video_download_caption_'+wmp_id+'">'+bundle[8]+'</span>\n';
			string+='	</div>\n';
			}
		if(w>320||download=='')
			{
			string+='	<div class="space"></div>\n';
			string+='	<div class="button" style="width:'+Math.round((((download!='')?w:w+55)-320)/2-3)+'px"></div>\n';
			}
		}
	string+='</div>\n\n';
	if(wmp_client=='ie')
		{
		if(debug)
			{
			string+='<div id="video_player_version_'+wmp_id+'" class="video_player_state" style="width:'+Math.round(w/2+1)+'px;text-align:left;clear:both;float:left"></div>\n';
			string+='<div id="video_player_state_'+wmp_id+'" class="video_player_state" style="width:'+Math.round(w/2+1)+'px;text-align:right;float:right"></div>\n';
			}
		}
	$t('div')[con].innerHTML=string;
	}

function video_action(id,action,state)
	{
	var video_button=$i('video_'+((action=='pause')?'play':((action=='count_loop')?'count_1x':action))+'_'+id);
	switch(action)
		{
		case 'play': case 'pause':
			if(state=='press')
				{
				video_button.src=button[(action=='play')?3:1].src;
				wmp_action[id]=(action=='play')?'pause':'play';
				}
			else
				{
				video_button.src=button[(action=='pause')?0:2].src;
				video_button.alt=bundle[(action=='pause')?0:1];
				$i('video_play_caption_'+id).innerHTML=bundle[(action=='pause')?0:1];
				if(action=='pause')
					wmp_object[id].controls.play();
				else
					wmp_object[id].controls.pause();
				}
			break;
		case 'stop':
			video_button.src=button[(state=='release')?4:5].src;
			if(state=='release')
				{
				wmp_object[id].controls.stop();
				wmp_object[id].controls.play();
				wmp_object[id].controls.stop();
				video_action(id,'pause','press');
				video_action(id,'play','release');
				}
			break;
		case 'rewind':
			video_button.src=button[(state=='release')?6:7].src;
			if(state=='release')
				{
				if(wmp_rate[id]==4) wmp_rate[id]=1; else wmp_rate[id]=-4;
				wmp_object[id].settings.rate=wmp_rate[id];
				}
			break;
		case 'forward':
			video_button.src=button[(state=='release')?8:9].src;
			if(state=='release')
				{
				if(wmp_rate[id]==-4) wmp_rate[id]=1; else wmp_rate[id]=4;
				wmp_object[id].settings.rate=wmp_rate[id];
				}
			break;
		case 'volume_less': case 'volume_more':
			video_button.src=gfx_path+action+'_'+((state=='release')?'off':'on')+'.gif';
			//video_button.style.backgroundImage='url(\''+gfx_path+action+'_'+((state=='release')?'off':'on')+'.gif\')';
			if(state=='release')
				{
				if(action=='volume_less')
					{
					if(wmp_volume[id]>0) wmp_volume[id]-=volume_rate;
					}
				else
					{
					if(wmp_volume[id]<100) wmp_volume[id]+=volume_rate;
					}
				wmp_object[id].settings.volume=wmp_volume[id];
				}
			break;
		case 'dowload':
			video_button.src=button[(state=='release')?16:17].src;
			break;
		}
	}

function format_time(time)
	{
	var negative=(time<0)?true:false;
	time=Math.abs(time);
	//var hour=Math.floor(time/3600); if(hour<10) hour='0'+hour;
	var minute=Math.floor(time/60); if(minute<10) minute='0'+minute;
	var second=Math.floor(time%60); if(second<10) second='0'+second;
	return ((negative)?'-':'')+minute+':'+second;
	}

function anim(id)
	{
	if(current_wmp!=id&&wmp_object[id])
		{
		wmp_status[id]=wmp_state[wmp_object[id].playState];
		update_timer(id);
		wmp_slider[id].style.left=Math.round(wmp_slider_cursor_x[id]+(wmp_slider_w[id]-wmp_slider_cursor_w[id]+2)/wmp_duration[id]*wmp_object[id].controls.currentPosition)+'px';
		if(debug) wmp_debug_state[id].innerHTML=wmp_status[id]+' ('+wmp_object[id].settings.rate+'x) | '+wmp_playcount[id].substr(6,wmp_playcount[id].length-6)+' | '+wmp_object[id].settings.volume+'% | '+format_time(wmp_object[id].controls.currentPosition);
		if(wmp_action[id]=='play'&&wmp_buffering[id]==100)
			{
			wmp_buffering[id]=-1;
			setTimeout('wmp_object['+id+'].controls.play()',500);
			setTimeout('wmp_object['+id+'].controls.stop()',500+latency);
			}
		else
			{
			if(wmp_buffering[id]>-1)
				{
				wmp_buffering[id]=wmp_object[id].network.bufferingProgress;
				//wmp_buffering[id]=wmp_object[id].network.downloadProgress;
				}
			}
		if(debug) wmp_debug_state[id].innerHTML+='<br>buffering progress: '+wmp_buffering[id]+'% | framerate: '+wmp_object[id].network.frameRate;
		}
	setTimeout('anim('+id+')',20);
	}

function move(evt)
	{
	if(current_wmp!=0)
		{
		evt=evt||event;
		var cursor_x=window.event.x;
		var x=Math.round(wmp_slider_mouse_x[current_wmp]+cursor_x);
		if(x<wmp_slider_cursor_x[current_wmp]) x=wmp_slider_cursor_x[current_wmp];
		if(x>wmp_slider_cursor_x[current_wmp]+wmp_slider_w[current_wmp]-wmp_slider_cursor_w[current_wmp]+2) x=wmp_slider_cursor_x[current_wmp]+wmp_slider_w[current_wmp]-wmp_slider_cursor_w[current_wmp]+2;
		wmp_slider[current_wmp].style.left=x+'px';
		}
	}

function release(evt)
	{
	if(current_wmp!=0)
		{
		var x=wmp_slider[current_wmp].offsetLeft;
		wmp_object[current_wmp].controls.currentPosition=(x-wmp_slider_cursor_x[current_wmp])*(wmp_duration[current_wmp]/(wmp_slider_w[current_wmp]-wmp_slider_cursor_w[current_wmp]+2));
		update_timer(current_wmp);
		}
	current_wmp=0;
	}

function update_timer(id)
	{
	wmp_timer_left[id].innerHTML=format_time(wmp_object[id].controls.currentPosition);
	wmp_timer_right[id].innerHTML=format_time(wmp_object[id].controls.currentPosition-wmp_duration[id]);
	}

function get_border(id)
	{
	var value=0;
	var string=id.style.borderWidth;
	if(string)
		{
		string=string.replace(/px/gi,'');
		string=string.replace(/em/gi,'');
		string=string.replace(/pt/gi,'');
		value=parseInt(string);
		}
	return value;
	}

function dowload(id)
	{
	/*
	Content-Type: application/octet-stream\n
	Content-Disposition: attachment; filename=[Your file name here]\n\n
	[Your file content here]
	*/
	}