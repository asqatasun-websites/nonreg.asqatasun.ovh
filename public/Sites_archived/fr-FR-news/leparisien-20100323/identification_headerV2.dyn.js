// Fonction de gestion de l'acces a "Mon Compte"

//Get Cookie Function --------------------------------------------------------------------------
function getCookie_header(name) {
	var cname = '; ' + name + "=";
	var dc = '; '+document.cookie;
	if (dc.length > 0) {
		begin = dc.indexOf(cname);
		if (begin != -1) {
			begin += cname.length;
			end = dc.indexOf(";", begin);
			if (end == -1) end = dc.length;
				s=new String("");
				s=unescape(dc.substring(begin, end));
				myRegExp = /\+/gi;
				s1=s.replace(myRegExp,' ');
				return(s1);
				//return unescape(dc.substring(begin, end));
		}
	}
	return null;
}

//Set Cookie Function ---------------------------------------------------------------------------
function setCookie_header(name, value, expires, path, domain, secure) {
	document.cookie = name + "=" + escape(value) + 
	((expires == null) ? "" : "; expires=" + expires.toGMTString()) +
	((path == null) ? "" : "; path=" + path) +
	((domain == null) ? "" : "; domain=" + domain) +
	((secure == null) ? "" : "; secure");
}

//Delete Cookie Function ------------------------------------------------------------------------
function delCookie_header (name,path,domain) {
	if (getCookie_header(name)) {
		document.cookie = name + "=" +
		((path == null) ? "" : "; path=" + path) +
		((domain == null) ? "" : "; domain=" + domain) +
		"; expires=Thu, 01-Jan-70 00:00:01 GMT";
	}
}

// Fonction principale, appelee depuis les pages HTML -------------------------------------------

function affiche_bandeau_header(){
	nom	=getCookie_header('nom');		if(nom==null)nom='';
	prenom	=getCookie_header('prenom');		if(prenom==null)prenom='';
	civilite=getCookie_header('civilite');		if(civilite==null)civilite='';
	id_client=getCookie_header('id_client');	if(id_client==null)id_client='';

	if(id_client == ''){
						affiche_bandeau_header_non_identifie_header();
	}
	else				affiche_bandeau_header_identifie_header(civilite,nom,prenom);
}

//Fonction pour la fonction affiche_bandeau_header_non_identifie
function toPass_header(obj)
{
        if(obj.value=="Mot de passe" || obj.value=="")
        {
                obj.value="";
                obj.type="password";
        }

}

function toText_header(obj)
{
        if(obj.value=="")
        {
                obj.value="Mot de passe";
                obj.type="text";
        }
}

function affiche_bandeau_header_non_identifie_header(){

document.write("\<p id=\"non-identifie\"\>\<span\>ESPACE ABONNÉS\</span\> \<a href=\"#\" title=\"Je me connecte\" class=\"sep\"\>Je me connecte\</a\> \<a href=\"/abonnement/\" title=\"Je m'abonne\"\>Je m'abonne\</a\>\</p\>")

}

function affiche_bandeau_header_identifie_header(civilite,nom,prenom){

document.write("\<p id=\"identifie\"\>\<span class=\"nom\"\>Bonjour "+prenom+" "+nom+"\</span\>")
document.write("\<a href=\"/abo-faitdujour/\" title=\"Articles\"\>Articles\</a\>")
document.write("\<a href=\"/pdf/\" title=\"PDF\"\>PDF\</a\>")
document.write("\<a href=\"/acces/moncompte/\" title=\"Mon compte\"\>Mon compte\</a\>")
document.write("\<a href=\"/acces/deconnexion.php\" style=\"background:none;\" title=\"Déconnexion\"\>D&eacute;connexion\</a\>\</p\> ")

}

function old_affiche_bandeau_header_identifie_header(civilite,nom,prenom){

document.write("");
document.write("<!-- ident connecte -->");
document.write("     <div class=\"ident\" style=\"margin-top:13px;\">");
document.write("	  <a class=\"link\" href=\"/acces/deconnexion.php\">Déconnexion</a><br />");
document.write("	  <a class=\"link\" href=\"/acces/moncompte/\">Mon compte</a><br />");
document.write("	  </div>");
document.write("	  <!--ident-->");
document.write("	  <div class=\"ident\" style=\"height:50px;margin: 5px 10px 0px 0px;border-right:1px solid #fff;\">");
document.write("	  &nbsp;");
document.write("	  <!--<img src=\"/icons/filet_verti_top2.png\" width=\"2\" height=\"60\" border=\"1\" />-->");
document.write("	  </div>");
document.write("	  <!--ident-->");
document.write("	  <div class=\"ident\" style=\"margin-top:13px;text-align:right;\">");
document.write("	  Bonjour "+prenom+" "+nom+"<br />");
document.write("	  <a href=\"/abo-faitdujour/\">Les articles du Parisien</a>");
document.write("	  &nbsp;|&nbsp;");
document.write("	  <a href=\"/pdf/\">Le journal en PDF</a>");
document.write("	  </div>");
document.write("	 <!-- fin ident connecte -->");

}