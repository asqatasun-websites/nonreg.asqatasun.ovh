<html><head>
<title>Chambre de Commerce et d'Industrie - CCI LENS</title>
<meta name="description"content="La CCI de l'arrondissement de LENS est au service des entreprises : aide � l'implantation, au d&eacute;veloppement &eacute;conomique, � la formation des chefs d'entreprises et des salari&eacute;s, aides � la cr&eacute;ation">
<meta name="KEYWORDS"content="CCI, Chambre de Commerce, entreprises, entreprise, d&eacute;veloppement &eacute;conomique, implantation d'entreprises, cr&eacute;ation, cr&eacute;ation d'entreprise, nord, pas de calais">
<link REL=StyleSheet TYPE=text/css HREF="cci-lens.css">
<SCRIPT>var nblay = 6 ;  // nombre total de calques</SCRIPT>
<SCRIPT src="inc_actualites.js"></SCRIPT>
</head>
<body topmargin=0 marginheight="0">
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr valign="bottom"><td height="18" colspan="2"> 
<table width="750" border="0" cellspacing="0" cellpadding="0"><tr><td width="597"><img src="bandeau-cci.gif" width="597" height="151"></td>
    <td width="153" valign="top" background="tmp.gif">&nbsp;</td>
  </tr></table>
</td></tr><tr><td height="18" colspan="2" valign="top"> 
<style>
.contact {PADDING-RIGHT: 9px; PADDING-LEFT: 9px; CURSOR: hand; WHITE-SPACE: nowrap; HEIGHT: 17px; BACKGROUND-COLOR: #ffffff; FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #006393; FONT-FAMILY: Arial;}
.contacttxt {FONT-WEIGHT: bold; FONT-SIZE: 11px;	COLOR: #006393;	FONT-FAMILY: Arial;	text-decoration: none;}
.contact2 {PADDING-RIGHT: 9px; PADDING-LEFT: 9px; CURSOR: hand; WHITE-SPACE: nowrap; HEIGHT: 17px; BACKGROUND-COLOR: #e0f5fb; FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #006393;	FONT-FAMILY: Arial;}
.liens {PADDING-RIGHT: 9px; PADDING-LEFT: 9px; CURSOR: hand; WHITE-SPACE: nowrap; HEIGHT: 17px; BACKGROUND-COLOR: #ffffff; FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #ea861b; FONT-FAMILY: Arial;}
.liens2 {PADDING-RIGHT: 9px; PADDING-LEFT: 9px; CURSOR: hand; WHITE-SPACE: nowrap; HEIGHT: 17px; BACKGROUND-COLOR: #FEEDCD; FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #ea861b;	FONT-FAMILY: Arial;}
.lienstxt {FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #ea861b;	FONT-FAMILY: Arial;}
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_showHideLayers() { //v6.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
//-->
function navon(mytd){mytd.className='contact2'};
function navoff(mytd){mytd.className='contact'};
function navigon(mytd){mytd.className='liens2'};
function navigoff(mytd){mytd.className='liens'};
var timerHideMenu; 
function hideMenu2()
{
  clearTimeout(timerHideMenu);      
  MM_showHideLayers('creation','','hidden', 'developpement','','hidden', 'transmission','','hidden', 'implantation','','hidden', 'projets','','hidden');
}   
// cache le menu
function hideMenu()
{
  timerHideMenu = setTimeout("hideMenu2()", 500);      
}
</script>
<div id="divMain" style="z-index:2">
<div id="creation" style="position:absolute; left:114px; top:24px; width:139px; height:184px; z-index:2; visibility: hidden;" onMouseOut="hideMenu();" onMouseOver="hideMenu2(); MM_showHideLayers('creation','','show')"> 
<table border="0" cellspacing="0" cellpadding="0"><tr><td bgcolor="#D6D6D6"><table width="138" border="0" cellpadding="0" cellspacing="1"><tr><td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/creation-entreprise/information-creation-entreprise.php" class="contacttxt">Information / Sensibilisation</a></td></tr><tr><td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/creation-entreprise/formation-creation.php" class="contacttxt">Formation</a></td></tr><tr><td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/creation-entreprise/accompagnement-creation.php" class="contacttxt">Accompagnement</a></td></tr><tr><td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/creation-entreprise/aides-subventions-creation-entreprise.php" class="contacttxt">Aides et subventions</a></td></tr><tr><td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/creation-entreprise/choix-statut-creation.php" class="contacttxt">Choix des statuts</a></td></tr><tr><td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/creation-entreprise/aspects-juridiques.php" class="contacttxt">Aspects juridiques</a></td></tr><tr><td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/creation-entreprise/sites-utiles.php" class="contacttxt">Sites et adresses utiles</a></td></tr><tr><td class="liens" onmouseover=navigon(this) onmouseout=navigoff(this)><a href="http://www.lens.cci.fr/creation-entreprise/contacts.php" class="lienstxt">Vos contacts</a></td></tr></table></td></tr></table>
</div>
<div id="developpement" style="position:absolute; left:201px; top:24px; width:149px; height:184px; z-index:2; visibility: hidden;" onMouseOut="hideMenu();" onMouseOver="hideMenu2(); MM_showHideLayers('developpement','','show')">
<table border="0" cellspacing="0" cellpadding="0"><tr><td bgcolor="#D6D6D6"><table width="150" border="0" cellpadding="0" cellspacing="1">
            <tr>
              <td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/developpement-entreprise/commerce/commerce.php" class="contacttxt">Commerce 
                / Services aux particuliers</a></td>
            </tr>
            <tr>
              <td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/developpement-entreprise/developpement-entreprises.php" class="contacttxt">Industrie 
                / Services aux entreprises</a></td>
            </tr>
            <tr>
              <td height="2" bgcolor="#FFFFFF"></td>
            </tr>
            <tr>
              <td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/developpement-entreprise/international/cci-international.php" class="contacttxt">International</a></td>
            </tr>
            <tr>
              <td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/developpement-entreprise/organisation-ressources-humaines.php" class="contacttxt">RH 
                et Formation</a></td>
            </tr>
            <tr>
              <td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/developpement-entreprise/apprentissage/apprentissage.php" class="contacttxt">Apprentissage</a></td>
            </tr>
            <tr>
              <td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/developpement-entreprise/technologies-information-communication.php" class="contacttxt">Technologies 
                de l'information et de la <br>
                communication</a></td>
            </tr>
            <tr>
              <td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/developpement-entreprise/juridique/juridique.php" class="contacttxt">Juridique</a></td>
            </tr>
            <tr>
              <td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/developpement-entreprise/documentation.php" class="contacttxt">Documentation</a></td>
            </tr>
            <tr>
              <td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/developpement-entreprise/sites-utiles.php" class="contacttxt">Sites 
                utiles</a></td>
            </tr>
            <tr>
              <td class="liens" onmouseover=navigon(this) onmouseout=navigoff(this)><a href="http://www.lens.cci.fr/developpement-entreprise/contacts.php" class="lienstxt">Vos 
                contacts</a></td>
            </tr>
          </table></td></tr></table>
</div>
<div id="transmission" style="position:absolute; left:350px; top:23px; width:134px; height:74px; z-index:2; visibility: hidden;" onMouseOut="hideMenu();" onMouseOver="hideMenu2(); MM_showHideLayers('transmission','','show')">
<table border="0" cellspacing="0" cellpadding="0"><tr><td bgcolor="#D6D6D6"><table width="132" border="0" cellpadding="0" cellspacing="1"><tr><td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/transmission-entreprise/cedant-entreprise.php" class="contacttxt">C&eacute;dants</a></td></tr><tr><td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/transmission-entreprise/repreneur-entreprise.php" class="contacttxt">Repreneurs</a></td></tr><tr><td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/transmission-entreprise/sites-utiles.php" class="contacttxt">Sites utiles</a></td></tr><tr><td class="liens" onmouseover=navigon(this) onmouseout=navigoff(this)><a href="http://www.lens.cci.fr/transmission-entreprise/contacts.php" class="lienstxt">Vos contacts</a></td></table></td></tr></table>
</div>
  <div id="implantation" style="position:absolute; left:480px; top:24px; width:139px; height:85px; z-index:2; visibility: hidden;" onMouseOut="hideMenu();" onMouseOver="hideMenu2(); MM_showHideLayers('implantation','','show')"> 
    <table border="0" cellspacing="0" cellpadding="0"><tr><td bgcolor="#D6D6D6"><table width="138" border="0" cellpadding="0" cellspacing="1">
            <tr> 
              <td height="32" class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/implantation-entreprise/implantation-entreprise.php" class="contacttxt">Notre 
                mission </a></td>
            </tr>
            <tr> 
              <td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/implantation-entreprise/batiments.php" class="contacttxt">Locaux 
                disponibles</a></td>
            </tr>
            <tr> 
              <td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/implantation-entreprise/projet-implantation.php" class="contacttxt">Vous 
                avez un projet ?</a></td>
            </tr>
            <tr> 
              <td class="liens" onmouseover=navigon(this) onmouseout=navigoff(this)><a href="http://www.lens.cci.fr/implantation-entreprise/contacts.php" class="lienstxt">Vos 
                contacts</a></td>
          </table></td></tr></table>
</div>
<div id="projets" style="position:absolute; left:600px; top:24px; width:130px; height:109px; z-index:2; visibility: hidden;" onMouseOut="hideMenu();" onMouseOver="hideMenu2(); MM_showHideLayers('projets','','show')">
<table border="0" cellspacing="0" cellpadding="0"><tr><td bgcolor="#D6D6D6"><table width="130" border="0" cellpadding="0" cellspacing="1"><tr><td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/projets-economiques/euralogistic-pole-logistique.php" class="contacttxt">Euralogistic, p&ocirc;le <br>d'excellence logistique</a></td></tr><tr><td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/projets-economiques/sport-economie.php" class="contacttxt">P&ocirc;le Sport- Economie</a></td></tr><tr><td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/projets-economiques/eco-industries.php" class="contacttxt">Fili&egrave;re Eco-industries</a></td></tr><tr><td class="contact" onmouseover=navon(this) onmouseout=navoff(this)><a href="http://www.lens.cci.fr/projets-economiques/projets-agglomeration.php" class="contacttxt">Autres projets <br>structurants</a></td></tr><tr><td class="liens" onmouseover=navigon(this) onmouseout=navigoff(this)><a href="http://www.lens.cci.fr/projets-economiques/contacts.php" class="lienstxt">Vos contacts</a></td></table></td></tr></table>
</div>
</div><table width="750" border="0" cellspacing="0" cellpadding="0"><tr><td><img src="deroulant.jpg" width="750" height="63" border="0" usemap="#Map"></td></tr></table>
<map name="Map">
  <area shape="rect" coords="95,1,203,30" href="home.php#" onMouseOut="hideMenu();" onmouseover="hideMenu2(); MM_showHideLayers('creation','','show');">
  <area shape="rect" coords="202,1,355,30" href="home.php#" onMouseOut="hideMenu();" onmouseover="hideMenu2(); MM_showHideLayers('developpement','','show');">
  <area shape="rect" coords="354,1,486,30" href="home.php#" onMouseOut="hideMenu();" onmouseover="hideMenu2(); MM_showHideLayers('transmission','','show');">
  <area shape="rect" coords="486,1,607,30" href="home.php#" onMouseOut="hideMenu();" onmouseover="hideMenu2(); MM_showHideLayers('implantation','','show');">
  <area shape="rect" coords="608,1,746,30" href="home.php#" onMouseOut="hideMenu();" onmouseover="hideMenu2(); MM_showHideLayers('projets','','show');">
  <area shape="rect" coords="17,1,95,30" href="index.html" alt="Chambre de Commerce et d'Industrie de l'arrondissement de LENS">
</map></td></tr><tr><td width="176" valign="top" background="fd-cci.gif"><script language="JavaScript" type="text/JavaScript">
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
</script>
<link rel="stylesheet" href="cci-lens.css" type="text/css">
<div id="divmenu" style="z-index:5"><div id="menu" style="position:absolute; left:2px; top:106px; width:175px; height:34px; z-index:1"> 
<form name="form1"><select name="menu1" onChange="MM_jumpMenu('parent',this,0)"><option value="#" selected>Acc�s direct ...</option><option value="http://www.lens.cci.fr/developpement-entreprise/commerce/commerce.php">Commerce</option><option value="http://www.lens.cci.fr/creation-entreprise/information-creation-entreprise.php">Cr�ation d'entreprise</option><option value="http://www.lens.cci.fr/developpement-entreprise/developpement-strategique.php">D�veloppement strat�gique</option><option value="http://www.lens.cci.fr/developpement-entreprise/qualite-securite-environnement.php">Environnement s�curit� qualit�</option><option value="http://www.lens.cci.fr/entreprises/consultation-annuaire.php">Fichier des entreprises</option><option value="http://www.lens.cci.fr/creation-entreprise/centre-de-formalites-des-entreprises-cfe.php">Formalit�s des entreprises</option><option value="http://www.lens.cci.fr/projets-economiques/euralogistic-pole-logistique.php">Grands projets</option><option value="http://www.lens.cci.fr/developpement-entreprise/innovation-technologique.php">Innovation</option><option value="http://www.lens.cci.fr/developpement-entreprise/international/cci-international.php">International</option><option value="http://www.lens.cci.fr/developpement-entreprise/juridique/juridique.php">Juridique</option><option value="http://www.lens.cci.fr/implantation-entreprise/batiments.php">Locaux disponibles</option><option value="http://www.lens.cci.fr/marches-publics-2007.pdf">March�s publics 2007</option><option value="http://www.lens.cci.fr/mentions-legales.php">Mentions l�gales</option><option value="http://www.lens.cci.fr/developpement-entreprise/ressources-humaines.php">Ressources Humaines</option><option value="http://www.lens.cci.fr/developpement-entreprise/taxe-apprentissage.php">Taxe d'apprentissage</option><option value="http://www.lens.cci.fr/developpement-entreprise/technologies-information-communication.php">Tic</option><option value="http://www.lens.cci.fr/transmission-entreprise/cedant-entreprise.php">Transmission - Reprise</option></select></form></div></div><table width="176" border="0" cellpadding="0" cellspacing="0" background="fd-cci.gif"><tr><td height="90" valign="top"><img src="coordonnees-cci.gif" width="176" height="90"></td></tr><tr><td height="19" valign="top"><img src="acces.gif" width="176" height="28"></td></tr>
<tr><td><table width="168" border="0" align="right" cellpadding="0" cellspacing="0"><tr><td colspan="2" class="menu_vtre_cci">&nbsp;</td></tr><tr><td width="11"><img src="puce-cci.gif" width="6" height="6"></td><td width="149"><a href="http://www.lens.cci.fr/creation-entreprise/centre-de-formalites-des-entreprises-cfe.php" class="menu_acces_direct">Formalit�s des entreprises</a></td></tr><tr><td width="11"><img src="puce-cci.gif" width="6" height="6"></td><td width="149"><a href="http://www.lens.cci.fr/developpement-entreprise/ressources-humaines.php" class="menu_acces_direct">Ressources Humaines</a></td></tr><tr><td width="11"><img src="puce-cci.gif" width="6" height="6"></td><td width="149"><a href="http://www.lens.cci.fr/implantation-entreprise/batiments.php" class="menu_acces_direct">Locaux disponibles</a></td></tr><tr><td colspan="2" class="menu_vtre_cci">&nbsp;</td></tr></table></td></tr><tr><td><a href="http://www.lens.cci.fr/votre-cci/cci-lens.php"><img src="cci-lens.gif" width="176" height="29" border="0"></a></td></tr><tr><td height="100"><table width="168" border="0" align="right" cellpadding="0" cellspacing="0"><tr><td height="10" colspan="2"></td></tr><tr><td width="11"><img src="puce-cci.gif" width="6" height="6"></td><td width="149"><a href="http://www.lens.cci.fr/votre-cci/cci-membres-elus.php" class="menu_vtre_cci">Les &eacute;lus</a></td></tr><tr><td><img src="puce-cci.gif" width="6" height="6"></td><td><a href="http://www.lens.cci.fr/votre-cci/services-cci.php" class="menu_vtre_cci">Les services</a></td></tr><tr><td><img src="puce-cci.gif" width="6" height="6"></td><td><a href="http://www.lens.cci.fr/votre-cci/contacts-services.php" class="menu_vtre_cci">Les contacts</a></td></tr><tr><td><img src="puce-cci.gif" width="6" height="6"></td><td><a href="http://www.lens.cci.fr/votre-cci/acces-cci-lens.php" class="menu_vtre_cci">Le plan d'acc&egrave;s</a></td></tr><tr><td height="12" colspan="2"></td></tr></table></td></tr><tr><td><a href="http://www.lens.cci.fr/territoire/localisation-lens.php"><img src="lens.gif" alt="Le territoire / CCI LENS" width="176" height="29" border="0"></a></td></tr><tr><td>&nbsp;</td></tr><tr><td><a href="http://www.lens.cci.fr/entreprises/consultation-annuaire.php"><img src="entreprises-cci-lens.gif" alt="Les entreprises de l'arrondissement de LENS" width="176" height="29" border="0"></a></td></tr><tr><td>&nbsp;</td></tr><tr><td height="56" valign="bottom"><div align="center"><a href="http://www.cci.fr" target="_blank"><img src="cci.fr.gif" alt="Portail des Chambres de Commerce et d'industrie" width="136" height="43" border="0"></a></div></td></tr><tr><td height="22">&nbsp;</td></tr></table></td>
    <td width="601" height="623" valign="top"> 
      <table width="574" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="26" colspan="3" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="50%" height="80" valign="top"><img src="arr-ardan.gif" width="287" height="80"></td>
                <td width="50%" valign="top"><div align="left"><img src="metiers-art2010.gif" width="287" height="80"></div></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td width="316" valign="top"> <table width="316" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr> 
                <td width="316" height="188" valign="top" class="gris_arial_13"> 
                  <div align="justify"> 
                    <table width="97%" border="0" align="center" cellpadding="0" cellspacing="0">
                      <tr valign="middle"> 
                        <td height="195" valign="middle"> 
                          <div align="justify"> 
                            <p class="gris_arial_12-nl">Vous voulez am&eacute;liorer 
                              la performance de votre entreprise, par la conqu&ecirc;te 
                              de nouveaux march&eacute;s, la mise en place d&#8217;un 
                              nouveau produit ou tout simplement la structuration 
                              d&#8217;une nouvelle activit&eacute; dans votre 
                              entreprise (r&eacute;organisation de vos services) 
                              et pour cela vous envisagez le recrutement d&#8217;un 
                              nouveau collaborateur. <br>
                              Pour le mettre en oeuvre avec un risque et un co&ucirc;t 
                              minimums, le dispositif ARDAN vous permet de tester 
                              dans l&#8217;entreprise ce collaborateur pendant 
                              une p&eacute;riode de six mois. Au-del&agrave; de 
                              la mission, vous &ecirc;tes libre de recruter ou 
                              de ne pas recruter le collaborateur en fonction 
                              des r&eacute;sultats obtenus. </p>
                          </div>
                          </td>
                      </tr>
                      <tr valign="bottom"> 
                        <td height="18"> 
                          <div align="left"><img src="fleche.gif" width="10" height="8"> 
                            <span class="gris_arial_13_or_bold">Contact :</span> 
                            <a href="mailto:jfbiel@lens.cci.fr" class="gris_arial_13_or_bold">Jean-Fran&ccedil;ois 
                            BIEL</a> </div></td>
                      </tr>
                    </table>
                  </div></td>
              </tr>
            </table></td>
          <td width="1" height="204" valign="top">&nbsp;</td>
          <td width="258" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="192" class="gris_arial_13"> 
                  <div align="justify"> 
                    <table width="99%" border="0" cellspacing="0" cellpadding="0">
                      <tr valign="bottom"> 
                        <td height="96" colspan="2" class="gris_arial_13"> 
                          <div align="justify"> 
                            <p>Cette manifestation, soutenue par le Conseil R&eacute;gional 
                              et la Chambre de M&eacute;tiers, regroupera les 
                              artisans d&#8217;art et les restaurateurs du patrimoine 
                              de toute la r&eacute;gion, les Meilleurs Ouvriers 
                              de France et les ateliers de restauration du LOUVRE.</p>
                          </div></td>
                      </tr>
                      <tr> 
                        <td width="40%" height="60" class="gris_arial_13"> <div align="justify"> 
                            <p> <img src="puce2.gif" width="18" height="9"> 
                              N&#8217;h&eacute;sitez pas &agrave; r&eacute;server 
                              d&egrave;s maintenant votre stand pour y participez.</p>
                          </div></td>
                        <td width="60%"><div align="center"><img src="salon2010.gif" alt="Salon des m&eacute;tiers d'Art 2010" width="140" height="75" border="0"></div></td>
                      </tr>
                      <tr valign="bottom"> 
                        <td height="16" colspan="2" class="gris_arial_13"> 
                          <p align="left"><img src="fleche.gif" width="10" height="8"> 
                            <a href="http://salonmetiersdart.canalblog.com/" target="_blank" class="gris_arial_13_or_bold">Plus 
                            d'infos sur le site internet</a> <strong>/</strong> 
                            <span class="gris_arial_13_or_bold">T&eacute;l : 03.21.69.23.11</span></p></td>
                      </tr>
                    </table>
                  </div></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td height="117" colspan="3"> <div align="center"> 
              <table width="99%" border="0" align="center" bgcolor="#AAC6AA">
                <tr> 
                  <td width="53%" height="22"><strong><font color="#336699" size="2" face="Arial, Helvetica, sans-serif"><img src="puce2.gif" width="18" height="9"> 
                    <font color="#30569C">Rapprochement Ecole / Entreprises</font></font></strong></td>
                  <td width="47%" rowspan="2"> <p align="center" class="gris_arial_13"><img src="formation-bassin-lens.gif" width="240" height="90"></p></td>
                </tr>
                <tr> 
                  <td height="79"> <p align="justify" class="gris_arial_13">Consulter 
                      notre base de donn&eacute;es des formations professionnelles 
                      :</p>
                    <p align="justify" class="gris_arial_13"> <span class="gris_arial_13_or_bold">--&gt;</span> 
                      <a href="http://www.formation-bassin-lens.com" target="_blank" class="gris_arial_13_or_bold">www.formation-bassin-lens.com</a><br>
                    </p></td>
                </tr>
              </table>
            </div></td>
        </tr>
        <tr> 
          <td height="157" colspan="3"> <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr> 
                <td width="46%" height="157"> <table width="87%" height="147" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr valign="bottom"> 
                      <td height="76"> <div align="center"><strong><font color="#336699" size="2" face="Arial, Helvetica, sans-serif"><a href="http://www.nordpasdecalais.net/" target="_blank"><img src="fichier-regional.gif" alt="Le fichier r&eacute;gional Nord Pas de Calais" width="250" height="90" border="0"></a></font></strong></div></td>
                    </tr>
                    <tr valign="bottom"> 
                      <td height="56" valign="bottom"> <div align="center"><strong><font color="#336699" size="2" face="Arial, Helvetica, sans-serif"><a href="http://www.artois.cci.fr" target="_blank"><img src="cci-artois.jpg" alt="CCI Artois / Janvier 2010" width="181" height="50" border="0"></a></font></strong></div></td>
                    </tr>
                  </table></td>
                <td width="54%"><table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr valign="middle"> 
                      <td height="39" colspan="2"><strong><font color="#336699" size="2" face="Arial, Helvetica, sans-serif"><img src="puce.gif" width="22" height="13"> 
                        POUR SATISFAIRE VOS BESOINS EN COMPETENCES, choisissez 
                        l&#8217;APPRENTISSAGE !!</font></strong></td>
                    </tr>
                    <tr> 
                      <td height="19" colspan="2" valign="middle"> <div align="right"> 
                          <p align="justify" class="gris_arial_13"><a href="http://www.artois-apprentissage.com" target="_blank" class="gris_arial_13_or_bold">www.artois-apprentissage.com</a></p>
                        </div></td>
                    </tr>
                    <tr> 
                      <td width="58%" height="32" valign="bottom" class="gris_arial_13"> 
                        <div align="center"> 
                          <p align="left">Un site gratuit pour : <br>
                            - D&eacute;posez votre offre<br>
                            - Consultez les CV<br>
                            - B&eacute;n&eacute;ficiez d&#8217;un accompagnement</p>
                        </div></td>
                      <td width="42%" valign="middle" class="gris_arial_13"><div align="center"><strong><font color="#336699" size="2" face="Arial, Helvetica, sans-serif"><a href="http://artois-apprentissage.com" target="_blank"><img src="artois-apprentissage.jpg" alt="Artois apprentissage" width="150" height="83" border="0"></a></font></strong></div></td>
                    </tr>
                    <tr> 
                      <td height="16" colspan="2" valign="middle" class="gris_arial_13"> 
                        tout au long de votre d&eacute;marche de recrutement. 
                      </td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td height="12" colspan="3"><font size="1">&nbsp;</font></td>
        </tr>
        <tr> 
          <td height="188" colspan="3"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="68%" height="188" valign="top"> <table width="99%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td colspan="2">&nbsp;&nbsp;<img src="puce.gif" width="22" height="13"><img src="manifestation.gif" width="195" height="17"></td>
                    </tr>
                    <tr valign="middle"> 
                      <td height="140" colspan="2"><div align="left"> 
                          <table width="375" height="135" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr> 
                              <td height="9"><img src="barre-agenda.gif" width="375" height="9"></td>
                            </tr>
                            <tr> 
                              <td valign="top" background="fd-agenda.gif"> 
                                <dIV id=actualites style="Z-INDEX: 1; OVERFLOW: hidden; WIDTH: 340px; POSITION: absolute; HEIGHT: 120px;" name="actualites"> 
                                  <div id=elt1 style="Z-INDEX: 2; LEFT: 3px; OVERFLOW: hidden; WIDTH: 328px; POSITION: absolute; TOP: 10px; HEIGHT: 120px"><table height=120 cellspacing=0 cellpadding=0 width=328 border=0><tbody><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Lundi <b>1/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1205" class="gris_arial_12-nl"><font color="#000000">R�union Union Commerciale CARVIN </font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Mardi <b>2/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1155" class="gris_arial_12-nl"><font color="#000000">L'EXPORT, L'ALTERNATIVE A LA CRISE</font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Jeudi <b>4/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1109" class="gris_arial_12-nl"><font color="#000000">R�union cr�ateurs</font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Vendredi <b>5/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1204" class="gris_arial_12-nl"><font color="#000000">6�me Salon des Vignerons de Bourgogne et du Jura</font></a></td></tr></tbody></table></div><div id=elt2 style="Z-INDEX: 2; LEFT: 3px; OVERFLOW: hidden; WIDTH: 328px; POSITION: absolute; TOP: 140px; HEIGHT: 120px"><table height=120 cellspacing=0 cellpadding=0 width=328 border=0><tbody><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Vendredi <b>5/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1211" class="gris_arial_12-nl"><font color="#000000">Salon des Artistes Contemporains et des Antiquaires ROUBAIX</font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Jeudi <b>11/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=908" class="gris_arial_12-nl"><font color="#000000">PAYER ET ETRE PAYE A L'INTERNATIONAL</font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Jeudi <b>11/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1094" class="gris_arial_12-nl"><font color="#000000">club qualit� s�curit� environnement</font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Jeudi <b>11/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1187" class="gris_arial_12-nl"><font color="#000000">2�me salon r�gional Achats/Handicap</font></a></td></tr></tbody></table></div><div id=elt3 style="Z-INDEX: 2; LEFT: 3px; OVERFLOW: hidden; WIDTH: 328px; POSITION: absolute; TOP: 270px; HEIGHT: 120px"><table height=120 cellspacing=0 cellpadding=0 width=328 border=0><tbody><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Jeudi <b>11/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1213" class="gris_arial_12-nl"><font color="#000000">Portes Ouvertes � l'occasion de l'INAUGURATION du Centre d'Affaires Entreprises</font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Vendredi <b>12/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1209" class="gris_arial_12-nl"><font color="#000000">pavillon d'or remise des troph�es</font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Vendredi <b>12/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1218" class="gris_arial_12-nl"><font color="#000000">2�me Salon de l'Habitat  LOGIRENOV</font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Samedi <b>13/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1212" class="gris_arial_12-nl"><font color="#000000">Vernissage de l'exposition PRODUITS D'IRLANDE </font></a></td></tr></tbody></table></div><div id=elt4 style="Z-INDEX: 2; LEFT: 3px; OVERFLOW: hidden; WIDTH: 328px; POSITION: absolute; TOP: 400px; HEIGHT: 120px"><table height=120 cellspacing=0 cellpadding=0 width=328 border=0><tbody><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Lundi <b>15/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1090" class="gris_arial_12-nl"><font color="#000000">forum des unions commerciales</font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Lundi <b>15/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1193" class="gris_arial_12-nl"><font color="#000000">4�mes Rencontres R�gionales des Unions Commerciales</font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Jeudi <b>18/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=906" class="gris_arial_12-nl"><font color="#000000">VOTRE PASSEPORT POUR L'EXPORT</font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Jeudi <b>18/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1110" class="gris_arial_12-nl"><font color="#000000">R�union cr�ateurs</font></a></td></tr></tbody></table></div><div id=elt5 style="Z-INDEX: 2; LEFT: 3px; OVERFLOW: hidden; WIDTH: 328px; POSITION: absolute; TOP: 530px; HEIGHT: 120px"><table height=120 cellspacing=0 cellpadding=0 width=328 border=0><tbody><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Jeudi <b>18/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1224" class="gris_arial_12-nl"><font color="#000000">uc libercourt rencontre mairie</font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Vendredi <b>19/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1111" class="gris_arial_12-nl"><font color="#000000">R�union Experts Comptables & Services Fiscaux</font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Samedi <b>20/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1222" class="gris_arial_12-nl"><font color="#000000">Journ�es Portes Ouvertes Lyc�e PASTEUR - HENIN BEAUMONT</font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Lundi <b>22/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1221" class="gris_arial_12-nl"><font color="#000000">Vernissage exposition </font></a></td></tr></tbody></table></div><div id=elt6 style="Z-INDEX: 2; LEFT: 3px; OVERFLOW: hidden; WIDTH: 328px; POSITION: absolute; TOP: 660px; HEIGHT: 120px"><table height=120 cellspacing=0 cellpadding=0 width=328 border=0><tbody><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Mardi <b>23/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1076" class="gris_arial_12-nl"><font color="#000000">ASSEMBLEE GENERALE</font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Mercredi <b>24/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1203" class="gris_arial_12-nl"><font color="#000000">SALON SITL - PARIS VILLEPINTE</font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Mercredi <b>24/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=1219" class="gris_arial_12-nl"><font color="#000000">Nordbat  Salon professionnel du b�timent</font></a></td></tr><tr><td valign=top width="90" class="gris_arial_12-nl"><font color="#000000">Jeudi <b>25/3</b></font></td><td valign=top><a href="http://www.lens.cci.fr/details-agenda.php?id=907" class="gris_arial_12-nl"><font color="#000000">VOTRE PASSEPORT POUR L'EXPORT</font></a></td></tr></tbody></table></div>                                  <script language=javascript>
init(); // lance l'initialisation	
setTimeout("montee()",5000); // lancement du processus de d�filement
</script>
                                </DIV></td>
                            </tr>
                          </table>
                        </div></td>
                    </tr>
                    <tr> 
                      <td width="52%" height="27"> <div align="right"></div></td>
                      <td width="48%"><div align="right"><font color="#000066"><strong><a href="http://www.lens.cci.fr/agenda.php" class="arial_12">Consulter 
                          l'agenda g&eacute;n&eacute;ral</a></strong></font><font color="#003399" size="2" face="Arial"><strong>&nbsp;&nbsp;&nbsp;</strong></font></div></td>
                    </tr>
                  </table></td>
                <td width="32%" valign="top"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td height="22"><img src="clubs.gif" width="149" height="22" hspace="5"></td>
                    </tr>
                    <tr> 
                      <td><p><span class="gris_arial_13">-</span><a href="http://www.lens.cci.fr/clubs/clubs.php" class="gris_arial_13"> 
                          Pr&eacute;sentation des Clubs</a></p></td>
                    </tr>
                    <tr> 
                      <td height="18"><img src="services.gif" width="147" height="18" hspace="5"></td>
                    </tr>
                    <tr> 
                      <td><span class="gris_arial_13">-</span><a href="http://www.lens.cci.fr/vente/new-catalogue.php" class="gris_arial_13"> 
                        Achat et demande de documents</a><br> <span class="gris_arial_13">- 
                        <a href="http://www.lens.cci.fr/entreprises/consultation-annuaire.php" class="gris_arial_13">Fichiers 
                        des entreprises</a></span></td>
                    </tr>
                    <tr> 
                      <td><img src="liens.gif" width="147" height="16" hspace="5"></td>
                    </tr>
                    <tr> 
                      <td height="35"> <p> <span class="gris_arial_13">- <a href="http://www.lens.cci.fr/pratiques/informations-entreprises.php" class="gris_arial_13">Le 
                          r&eacute;seau des CCI</a></span><br>
                          <span class="gris_arial_13">- <a href="http://www.lens.cci.fr/votre-cci/logo-cci-lens.php" class="gris_arial_13">Le 
                          logo CCI &agrave; t&eacute;l&eacute;charger</a></span></p></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table></td></tr><tr><td height="20" colspan="2" valign="top">
<table width="750" border="0" cellspacing="0" cellpadding="0"><tr><td height="31" valign="middle" background="bs.gif" class="cci"><p align="right"><em><strong><font color="#FFFFFF" size="2" face="Arial">CCI de l'arrondissement de LENS - 3 avenue Elie Reumaux / SP 14 / 62307 LENS CEDEX&nbsp;&nbsp;&nbsp;&nbsp;</font></strong></em></p></td></tr></table></td></tr></table>
</body>
</html>