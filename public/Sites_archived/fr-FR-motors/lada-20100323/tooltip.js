/*
*	Voici un fichier qui coince.
*	Ce qui ne va pas c'est l'affichage sous IE (qui marchait avec Op�ra)
*	En fait c'est lors des s�lections d'objets de la page HTML que surviennent les difficult�es.
*	C�d que je ne sais pas bien quand et comment employer :
*		document.all[]
*		document.getElementById()
*		document.getElementsByName()
*	Ainsi que la selection des attributs suivant la m�thode de s�lection ci-dessus choisie :
*		pour aller chercher le checked, style, disabled (qui sont des attributs existant de base)
*		mais aussi l'attribut etat (que j'ai ajout� moi-m�me), avec par exemple attributes['etat'].nodeValue
*	Et puis pour le style j'ai un autre probl�me, je m�lange :
*		.style = 'display:none;text-decoration:none;'
*		.style.display = 'none'
*
*	Donc le probl�me ne porte pas vraiment sur l'algorithme et le raisonnement, mais sur l'affichage.
*	Pour essayer de r�soudre les probl�mes j'ai essay� de tout faire par des id (et non plus m�langer les "id" et les "name" et le "document.all")
*
*	Fait : tout pass� en getElementById.
*		normalement c'est bon pour checked et disabled.
*		pour style j'ai tout pass� sous la forme getElementById('...').style.display = 'none';
*		tout les acc�s � des attributs 'persos' sont fait par attributes['...'].nodeValue.
*		En fait j'ai essay� de respecter les normes, mais je sais pas vraiment comment se comporte IE par rapport aux normes.
*/

t_num_select = new Array('haut','bas');

function isInt(str)
{
	var i = parseInt(str);
	if (isNaN(i))
	{
		return false;
	}
	i = i.toString();
	if(i != str)
	{
		return false;
	}
	return true;
}

function CreatElem(Id, Longueur, Message) //Si Longueur==0 alors l'info bulle sera automatiquement dimension en fonction de la longeur du message ;)
{
	var MonStyle = "STYLE='{filter:alpha(opacity=100); position: absolute; visibility: hidden; left: 0; top: 0; font-family: Verdana, Arial; font-size: 11px; color: #162CAA;border-width: 2; border-color: #C8DBF0; border-style: dashed; padding: 3; background-color: #FFFAD3; width: "   
	MonStyle += (Longueur == 0)? "'}"  : Longueur + "'}";
	document.write("<DIV align='justify' width='" + Longueur + "' ID='" + Id + "' " + MonStyle + ">" + Message + "</DIV>");
}

function RendElemVisible(id)
{
	var Elem =  document.getElementById(id).style;
	Elem.left = event.clientX + 30 + document.body.scrollLeft;
	Elem.top = event.clientY + 5 + document.body.scrollTop;
	Elem.visibility =  "visible";
}

function RendElemInvisible(id) 
{
	var Elem =  document.getElementById(id).style;
	Elem.visibility =  "hidden";
	Elem.left = 0;
	Elem.top = 0;
}

/********************************************************************
*********************************************************************
*********************************************************************
*********************************************************************





LES FONCTIONS CI DESSOUS SERVENT POUR LE MOTEUR DE RECHERCHE
ELLES ONT ETE EXTERNALISEES DANS LE MODULE DU MOTEUR DE RECHERCHE
MAIS JE NE SAIS PAS SI ELLES SERVENT POUR D'AUTRES MODULES
DONC JE LES LAISSE ICI, C'EST CADEAU





*********************************************************************
*********************************************************************
*********************************************************************
********************************************************************/



function cocher_site(input_appelant, t_id_site)
{
	if(input_appelant == 'sites')
	{
		if(document.getElementById(input_appelant).checked)
		{
			document.getElementById(input_appelant).attributes['etat'].nodeValue = 1;
			for(var i = 0; i < t_id_site.length; i += 1)
			{
				var id_input = 'site_'+t_id_site[i];
				if(!document.getElementById(id_input).checked)
				{
					document.getElementById(id_input).attributes['etat'].nodeValue = 1;
					document.getElementById(id_input).checked = true;
				}
			}
		}
		else
		{
			document.getElementById(input_appelant).attributes['etat'].nodeValue = 0;
		}
	}
}

function decocher_site(input_appelant, t_id_site)
{
	if(!document.getElementById(input_appelant).checked)
	{
		
		document.getElementById(input_appelant).attributes['etat'].nodeValue = 0;
		if(document.getElementById('sites')!=null)
		{
			document.getElementById('sites').attributes['etat'].nodeValue = 0;
			document.getElementById('sites').checked = false;
		}
	}
	else
	{
		document.getElementById(input_appelant).attributes['etat'].nodeValue = 1;
		var flag = true;
		for(var i = 0; i < t_id_site.length; i += 1)
		{
			var id_input = 'site_'+t_id_site[i];
			if(!document.getElementById(id_input).checked)
			{
				flag = false;
			}
		}
		if(flag == true && document.getElementById('sites')!=null)
		{
			document.getElementById('sites').attributes['etat'].nodeValue = 1;
			document.getElementById('sites').checked = true;
		}
	}
}

/*function selection_site(id_max, nb_page_max_par_pagination)
{
	var select_site = document.getElementById('select_site');
	var select_nb_result_max_par_page = document.getElementById('select_nb_resultats_max_par_page');
	var nb_result_par_page = parseInt(select_nb_result_max_par_page.options[select_nb_result_max_par_page.selectedIndex].value);
	
	// On commence par tout cacher (les pages et les r�sultats)
	for(var j = 0; j < t_num_select.length; j++)
	{
		// On cache tous les boutons d'affichage des pages de r�sultats
		cache_elements_naviguation('precedent');
		cache_elements_naviguation('suivant');
		var i = 1;
		while(document.getElementById('page'+i+'_'+t_num_select[j]))
		{
			document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue = 0;
			document.getElementById('page'+i+'_'+t_num_select[j]).style.fontweight = 'bold';
			document.getElementById('page'+i+'_'+t_num_select[j]).style.fontSize = '12pt';
			document.getElementById('page'+i+'_'+t_num_select[j]).style.display = 'none';
			document.getElementById('page'+i+'_'+t_num_select[j]).style.textDecoration = 'none';
			//document.getElementById('page'+i+'_'+t_num_select[j]).style.display = 'none';
			//document.getElementById('page'+i+'_'+t_num_select[j]).style.textDecoration = 'none';
			i += 1;
		}

	}
	//
	//	Si on a plus de r�sultats que le nombre de r�sultats par page on affiche :
	//		-> les boutons d'affichage des pages r�sultats n�cessaires, on se limite aux $nb_page_max_par_pagination premi�res pages
	//		-> s'il y a plus de $nb_page_max_par_pagination pages on affiche le lien vers les pages suivantes (et peut-�tre fin)
	//	Il faut penser � afficher les 'nb_result_par_page' premiers r�sultats
	//
	// On calcule le nombre de r�sultats correspondant au 'filtre' choisit dans le select, tout en affichant les bons r�sultats
	var cpt = 0;
	var valeur_select_site = select_site.options[select_site.selectedIndex].value;
	var select_module = document.getElementById('select_module');
	var valeur_select_module = select_module.options[select_module.selectedIndex].value;
	if(valeur_select_site == 'Tous les sites' && valeur_select_module == 'Tous les modules')
	{
		cpt = id_max;
		for(i = 1; i <= id_max; i++)
		{
			var div_resultat = document.getElementById(i);
			if(i <= nb_result_par_page)
			{
				// On affiche les premiers r�sultats
				div_resultat.attributes['etat'].nodeValue = 2;
				div_resultat.style.display = 'block';
			}
			else
			{
				// On met les autres r�sultats � l'�tat 1
				div_resultat.attributes['etat'].nodeValue = 1;
				div_resultat.style.display = 'none';
			}
		}
	}
	else if(valeur_select_site == 'Tous les sites')
	{
		// On ne controle que les modules
		for(i = 1; i <= id_max; i++)
		{
			var div_resultat = document.getElementById(i);
			var div_resultat_module = div_resultat.attributes['module'].nodeValue;
			if(div_resultat_module == valeur_select_module)
			{
				cpt += 1;
				if(cpt <= nb_result_par_page)
				{
					// On affiche le r�sultat
					div_resultat.attributes['etat'].nodeValue = 2;
					div_resultat.style.display = 'block';
				}
				else
				{
					// On passe l'�tat du r�sultat � 1 et on le cache
					div_resultat.attributes['etat'].nodeValue = 1;
					div_resultat.style.display = 'none';
				}
			}
			else
			{
				// Si le r�sultat ne correspond pas au module, on ne l'affiche pas
				div_resultat.attributes['etat'].nodeValue = 0;
				div_resultat.style.display = 'none';
			}
		}
	}
	else if(valeur_select_module == 'Tous les modules')
	{
		// On ne controle que les sites
		for(i = 1; i <= id_max; i++)
		{
			var div_resultat = document.getElementById(i);
			var div_resultat_site = div_resultat.attributes['site'].nodeValue;
			if(div_resultat_site == valeur_select_site)
			{
				cpt += 1;
				if(cpt <= nb_result_par_page)
				{
					// On affiche le r�sultat
					div_resultat.attributes['etat'].nodeValue = 2;
					div_resultat.style.display = 'block';
				}
				else
				{
					// On passe l'�tat du r�sultat � 1 et on le cache
					div_resultat.attributes['etat'].nodeValue = 1;
					div_resultat.style.display = 'none';
				}
			}
			else
			{
				// Si le r�sultat ne correspond pas au site, on ne l'affiche pas
				div_resultat.attributes['etat'].nodeValue = 0;
				div_resultat.style.display = 'none';
			}
		}
	}
	else
	{
		// On controle � la fois les sites et les modules
		for(i = 1; i <= id_max; i++)
		{
			var div_resultat = document.getElementById(i);
			var div_resultat_module = div_resultat.attributes['module'].nodeValue;
			var div_resultat_site = div_resultat.attributes['site'].nodeValue;
			if(div_resultat_site == valeur_select_site && div_resultat_module == valeur_select_module)
			{
				cpt += 1;
				if(cpt <= nb_result_par_page)
				{
					// On affiche le r�sultat
					div_resultat.attributes['etat'].nodeValue = 2;
					div_resultat.style.display = 'block';
				}
				else
				{
					// On passe l'�tat du r�sultat � 1 et on le cache
					div_resultat.attributes['etat'].nodeValue = 1;
					div_resultat.style.display = 'none';
				}
			}
			else
			{
				// Si le r�sultat ne correspond ni au site ni au module, on ne l'affiche pas
				div_resultat.attributes['etat'].nodeValue = 0;
				div_resultat.style.display = 'none';
			}
		}
	}
	// On sait combien de r�sultats on a, et on a affich� les premiers
	for(j = 0; j < t_num_select.length; j++)
	{
		document.getElementById('nb_resultats').innerHTML = cpt;
	}
	// Maintenant on peut refaire la pagination proprement dite
	if(cpt > nb_result_par_page)
	{
		var	nb_page = Math.ceil(cpt / nb_result_par_page) ;
		i = 1;
		while(document.getElementById('page'+i+'_haut'))
		{
			for(j = 0; j < t_num_select.length; j++)
			{
				var page = document.getElementById('page'+i+'_'+t_num_select[j]);
				if(i == 1)
				{
					// La premi�re page est color�e et affich�e
					page.attributes['etat'].nodeValue = 2;
					page.style.display = 'block';
					page.style.textDecoration = 'none';
					page.style.color = 'black';
					page.style.fontweight = 'bold';
					page.style.fontSize = '15pt';
				}
				else if(i <= nb_page_max_par_pagination && i <= nb_page)
				{
					// Les premi�res pages sont affich�es
					page.attributes['etat'].nodeValue = 2;
					page.style.display = 'block';
					page.style.textDecoration = 'none';
					page.style.color = '#e04020';
					page.style.fontweight = 'bold';
					page.style.fontSize = '12pt';
				}
				else if(i <= nb_page)
				{
					// Le nombre de pages correspondantes aux r�sultats (cpt) sont pass�es � l'�tat 1 et cach�es
					page.attributes['etat'].nodeValue = 1;
					page.style.display = 'none';
					page.style.textDecoration = 'none';
					page.style.color = '#e04020';
					page.style.fontweight = 'bold';
					page.style.fontSize = '12pt';
				}
			}
			i += 1;
		}
		for(j = 0; j < t_num_select.length; j++)
		{
			if(nb_page > nb_page_max_par_pagination)
			{
				document.getElementById('suivant_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
				document.getElementById('suivant_'+t_num_select[j]).style.display = 'block';
				document.getElementById('suivant_'+t_num_select[j]).style.textDecoration = 'none';
			}
			if(nb_page > (2*nb_page_max_par_pagination))
			{
				document.getElementById('fin_'+t_num_select[j]).attributes['etat'] = 2;
				document.getElementById('fin_'+t_num_select[j]).style.display = 'block';
				document.getElementById('fin_'+t_num_select[j]).style.textDecoration = 'none';
			}
		}
	}
}*/

function selection_module(id_max,nb_page_max_par_pagination,nb_ppage)
{
	var valeur_select_module = document.getElementById('select_module').value;
	var nb_result_par_page = nb_ppage;
	
	// On commence par tout cacher (les pages et les r�sultats)
	for(var j = 0; j < t_num_select.length; j++)
	{
		// On cache tous les boutons d'affichage de page de r�sultat
		cache_elements_naviguation('precedent');
		var i = 1;
		while(document.getElementById('page'+i+'_'+t_num_select[j]))
		{
			document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue = 0;
			document.getElementById('page'+i+'_'+t_num_select[j]).style.display = 'none';
			document.getElementById('page'+i+'_'+t_num_select[j]).style.textDecoration = 'none';
			document.getElementById('page'+i+'_'+t_num_select[j]).style.fontweight = 'bold';
			i += 1;
		}
	}
	// 
	//	Si on a plus de r�sultats que le nombre de r�sultats par page on affiche :
	//		-> les boutons d'affichage des pages r�sultats n�cessaires, on se limite aux $nb_page_max_par_pagination premi�res pages
	//		-> s'il y a plus de $nb_page_max_par_pagination pages on affiche le lien vers les pages suivantes (et peut-�tre fin)
	//	Il faut penser � afficher les 'nb_result_par_page' premiers r�sultats
	//
	// On calcule le nombre de r�sultats correspondant au 'filtre' choisit dans le select, tout en affichant les bons r�sultats
	//
	
	
	var cpt = 0;
	
	// la selection des sites n'existe plus (pour une raison d'affichage)
	// on affiche donc par d�faut les r�sultats sans les filtrer par site
	var valeur_select_site = 'Tous les sites';
	if(valeur_select_site == 'Tous les sites' && valeur_select_module == 'Tous les modules')
	{
		cpt = id_max;
		for(i = 1; i <= id_max; i++)
		{
			if(i <= nb_result_par_page)
			{
				// On affiche les premiers r�sultats
				document.getElementById(i).attributes['etat'].nodeValue = 2;
				document.getElementById(i).style.display = 'block';
			}
			else
			{
				// On met les autres r�sultats � l'�tat 1
				document.getElementById(i).attributes['etat'].nodeValue = 1;
				document.getElementById(i).style.display = 'none';
			}
		}
	}
	else if(valeur_select_site == 'Tous les sites')
	{
		// On ne controle que les modules
		for(i = 1; i <= id_max; i++)
		{
			var div_resultat_module = document.getElementById(i).attributes['module'].nodeValue;
			if(div_resultat_module == valeur_select_module)
			{
				cpt += 1;
				if(cpt <= nb_result_par_page)
				{
					// On affiche le r�sultat
					document.getElementById(i).attributes['etat'].nodeValue = 2;
					document.getElementById(i).style.display = 'block';
				}
				else
				{
					// On passe l'�tat du r�sultat � 1 et on le cache
					document.getElementById(i).attributes['etat'].nodeValue = 1;
					document.getElementById(i).style.display = 'none';
				}
			}
			else
			{
				// Si le r�sultat ne correspond pas au module, on ne l'affiche pas
				document.getElementById(i).attributes['etat'].nodeValue = 0;
				document.getElementById(i).style.display = 'none';
			}
		}
	}
	else if(valeur_select_module == 'Tous les modules')
	{
		// On ne controle que les sites
		for(i = 1; i <= id_max; i++)
		{
			var div_resultat_site = document.getElementById(i).attributes['site'].nodeValue;
			if(div_resultat_site == valeur_select_site)
			{
				cpt += 1;
				if(cpt <= nb_result_par_page)
				{
					// On affiche le r�sultat
					document.getElementById(i).attributes['etat'].nodeValue = 2;
					document.getElementById(i).style.display = 'block';
				}
				else
				{
					// On passe l'�tat du r�sultat � 1 et on le cache
					document.getElementById(i).attributes['etat'].nodeValue = 1;
					document.getElementById(i).style.display = 'none';
				}
			}
			else
			{
				// Si le r�sultat ne correspond pas au site, on ne l'affiche pas
				document.getElementById(i).attributes['etat'].nodeValue = 0;
				document.getElementById(i).style.display = 'none';
			}
		}
	}
	else
	{
		// On controle � la fois les sites et les modules
		for(i = 1; i <= id_max; i++)
		{
			var div_resultat_module = document.getElementById(i).attributes['module'].nodeValue;
			var div_resultat_site = document.getElementById(i).attributes['site'].nodeValue;

			if(div_resultat_site == valeur_select_site && div_resultat_module == valeur_select_module)
			{
				cpt += 1;
				if(cpt <= nb_result_par_page)
				{
					// On affiche le r�sultat
					document.getElementById(i).attributes['etat'].nodeValue = 2;
					document.getElementById(i).style.display = 'block';
				}
				else
				{
					// On passe l'�tat du r�sultat � 1 et on le cache
					document.getElementById(i).attributes['etat'].nodeValue = 1;
					document.getElementById(i).style.display = 'none';
				}
			}
			else
			{
				// Si le r�sultat ne correspond ni au site ni au module, on ne l'affiche pas
				document.getElementById(i).attributes['etat'].nodeValue = 0;
				document.getElementById(i).style.display = 'none';
			}
		}
	}
	// On sait combien de r�sultats on a, et on a affich� les premiers
	for(j = 0; j < t_num_select.length; j++)
	{
		document.getElementById('nb_resultats_haut').innerHTML = cpt;
		document.getElementById('nb_resultats_bas').innerHTML = cpt;
	}
	
	// Maintenant on peut refaire la pagination proprement dite
	if(cpt > nb_result_par_page)
	{
		var	nb_page = Math.ceil(cpt / nb_result_par_page);
		i = 1;
		while(document.getElementById('page'+i+'_haut'))
		{
			for(j = 0; j < t_num_select.length; j++)
			{
				if(i == 1)
				{
					// La premi�re page est color�e et affich�e
					document.getElementById('page'+i+'_'+t_num_select[j]).style.display = 'block';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.textDecoration = 'none';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.color = 'black';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontweight = 'bold';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontSize = '15pt';
					document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
				}
				else if(i <= nb_page_max_par_pagination && i <= nb_page)
				{
					// Les premi�res pages sont affich�es (mais pas color�es)
					document.getElementById('page'+i+'_'+t_num_select[j]).style.display = 'block';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.textDecoration = 'none';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.color = '#e04020';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontweight = 'arial';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontSize = '12pt';
					document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
				}
				else if(i <= nb_page)
				{
					// Le nombre de pages correspondantes aux r�sultats (cpt) sont pass�es � l'�tat 1 et cach�es
					document.getElementById('page'+i+'_'+t_num_select[j]).style.display = 'none';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.textDecoration = 'none';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.color = '#e04020';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontweight = 'arial';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontSize = '12pt';
					document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue = 1;
				}
			}
			i += 1;
		}
		
		
		/*if(nb_page > nb_page_max_par_pagination)
			affiche_elements_naviguation('precedent');
		else
			cache_elements_naviguation('precedent');
			
		if(nb_page > (2*nb_page_max_par_pagination))
			affiche_elements_naviguation('suivant');
		else
			cache_elements_naviguation('suivant');*/
			
	}
	changer_page_resultats(1,nb_ppage);
}


// ancienne fonction de s�lection qui intergaissait avec un select permettant de ne choisir que
// les r�ponses issues de certains modules
// le select a �t� supprim�, elle ne sert plus
/*function selection_module(id_max, nb_page_max_par_pagination)
{
	var select_module = document.getElementById('select_module');
	var select_nb_result_max_par_page = document.getElementById('select_nb_resultats_max_par_page');
	var nb_result_par_page = parseInt(select_nb_result_max_par_page.options[select_nb_result_max_par_page.selectedIndex].value);
	
	// On commence par tout cacher (les pages et les r�sultats)
	for(var j = 0; j < t_num_select.length; j++)
	{
		// On cache tous les boutons d'affichage de page de r�sultat
		document.getElementById('debut_'+t_num_select[j]).attributes['etat'].nodeValue = 0;
		document.getElementById('debut_'+t_num_select[j]).style.display = 'none';
		document.getElementById('debut_'+t_num_select[j]).style.textDecoration = 'none';
		document.getElementById('precedent_'+t_num_select[j]).attributes['etat'].nodeValue = 0;
		document.getElementById('precedent_'+t_num_select[j]).style.display = 'none';
		document.getElementById('precedent_'+t_num_select[j]).style.textDecoration = 'none';
		var i = 1;
		while(document.getElementById('page'+i+'_'+t_num_select[j]))
		{
			document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue = 0;
			document.getElementById('page'+i+'_'+t_num_select[j]).style.display = 'none';
			document.getElementById('page'+i+'_'+t_num_select[j]).style.textDecoration = 'none';
			document.getElementById('page'+i+'_'+t_num_select[j]).style.fontweight = 'bold';
			i += 1;
		}
		document.getElementById('suivant_'+t_num_select[j]).attributes['etat'].nodeValue = 0;
		document.getElementById('suivant_'+t_num_select[j]).style.display = 'none';
		document.getElementById('suivant_'+t_num_select[j]).style.textDecoration = 'none';
		document.getElementById('fin_'+t_num_select[j]).attributes['etat'].nodeValue = 0;
		document.getElementById('fin_'+t_num_select[j]).style.display = 'none';
		document.getElementById('fin_'+t_num_select[j]).style.textDecoration = 'none';
	}
	
	// 
	//	Si on a plus de r�sultats que le nombre de r�sultats par page on affiche :
	//		-> les boutons d'affichage des pages r�sultats n�cessaires, on se limite aux $nb_page_max_par_pagination premi�res pages
	//		-> s'il y a plus de $nb_page_max_par_pagination pages on affiche le lien vers les pages suivantes (et peut-�tre fin)
	//	Il faut penser � afficher les 'nb_result_par_page' premiers r�sultats
	//
	// On calcule le nombre de r�sultats correspondant au 'filtre' choisit dans le select, tout en affichant les bons r�sultats
	var cpt = 0;
	var valeur_select_module = select_module.options[select_module.selectedIndex].value;

	var select_site = document.getElementById('select_site');
	var valeur_select_site = select_site.options[select_site.selectedIndex].value;
	if(valeur_select_site == 'Tous les sites' && valeur_select_module == 'Tous les modules')
	{
		cpt = id_max;
		for(i = 1; i <= id_max; i++)
		{
			if(i <= nb_result_par_page)
			{
				// On affiche les premiers r�sultats
				document.getElementById(i).attributes['etat'].nodeValue = 2;
				document.getElementById(i).style.display = 'block';
			}
			else
			{
				// On met les autres r�sultats � l'�tat 1
				document.getElementById(i).attributes['etat'].nodeValue = 1;
				document.getElementById(i).style.display = 'none';
			}
		}
	}
	else if(valeur_select_site == 'Tous les sites')
	{
		// On ne controle que les modules
		for(i = 1; i <= id_max; i++)
		{
			var div_resultat_module = document.getElementById(i).attributes['module'].nodeValue;
			if(div_resultat_module == valeur_select_module)
			{
				cpt += 1;
				if(cpt <= nb_result_par_page)
				{
					// On affiche le r�sultat
					document.getElementById(i).attributes['etat'].nodeValue = 2;
					document.getElementById(i).style.display = 'block';
				}
				else
				{
					// On passe l'�tat du r�sultat � 1 et on le cache
					document.getElementById(i).attributes['etat'].nodeValue = 1;
					document.getElementById(i).style.display = 'none';
				}
			}
			else
			{
				// Si le r�sultat ne correspond pas au module, on ne l'affiche pas
				document.getElementById(i).attributes['etat'].nodeValue = 0;
				document.getElementById(i).style.display = 'none';
			}
		}
	}
	else if(valeur_select_module == 'Tous les modules')
	{
		// On ne controle que les sites
		for(i = 1; i <= id_max; i++)
		{
			var div_resultat_site = document.getElementById(i).attributes['site'].nodeValue;
			if(div_resultat_site == valeur_select_site)
			{
				cpt += 1;
				if(cpt <= nb_result_par_page)
				{
					// On affiche le r�sultat
					document.getElementById(i).attributes['etat'].nodeValue = 2;
					document.getElementById(i).style.display = 'block';
				}
				else
				{
					// On passe l'�tat du r�sultat � 1 et on le cache
					document.getElementById(i).attributes['etat'].nodeValue = 1;
					document.getElementById(i).style.display = 'none';
				}
			}
			else
			{
				// Si le r�sultat ne correspond pas au site, on ne l'affiche pas
				document.getElementById(i).attributes['etat'].nodeValue = 0;
				document.getElementById(i).style.display = 'none';
			}
		}
	}
	else
	{
		// On controle � la fois les sites et les modules
		for(i = 1; i <= id_max; i++)
		{
			var div_resultat_module = document.getElementById(i).attributes['module'].nodeValue;
			var div_resultat_site = document.getElementById(i).attributes['site'].nodeValue;
			alert(div_resultat_site);
			if(div_resultat_site == valeur_select_site && div_resultat_module == valeur_select_module)
			{
				cpt += 1;
				if(cpt <= nb_result_par_page)
				{
					// On affiche le r�sultat
					document.getElementById(i).attributes['etat'].nodeValue = 2;
					document.getElementById(i).style.display = 'block';
				}
				else
				{
					// On passe l'�tat du r�sultat � 1 et on le cache
					document.getElementById(i).attributes['etat'].nodeValue = 1;
					document.getElementById(i).style.display = 'none';
				}
			}
			else
			{
				// Si le r�sultat ne correspond ni au site ni au module, on ne l'affiche pas
				document.getElementById(i).attributes['etat'].nodeValue = 0;
				document.getElementById(i).style.display = 'none';
			}
		}
	}
	// On sait combien de r�sultats on a, et on a affich� les premiers
	for(j = 0; j < t_num_select.length; j++)
	{
		document.getElementById('nb_resultats').innerHTML = cpt;
	}
	// Maintenant on peut refaire la pagination proprement dite
	if(cpt > nb_result_par_page)
	{
		var	nb_page = Math.ceil(cpt / nb_result_par_page);
		i = 1;
		while(document.getElementById('page'+i+'_haut'))
		{
			for(j = 0; j < t_num_select.length; j++)
			{
				if(i == 1)
				{
					// La premi�re page est color�e et affich�e
					document.getElementById('page'+i+'_'+t_num_select[j]).style.display = 'block';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.textDecoration = 'none';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.color = 'black';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontweight = 'bold';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontSize = '15pt';
					document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
				}
				else if(i <= nb_page_max_par_pagination && i <= nb_page)
				{
					// Les premi�res pages sont affich�es (mais pas color�es)
					document.getElementById('page'+i+'_'+t_num_select[j]).style.display = 'block';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.textDecoration = 'none';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.color = '#e04020';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontweight = 'arial';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontSize = '12pt';
					document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
				}
				else if(i <= nb_page)
				{
					// Le nombre de pages correspondantes aux r�sultats (cpt) sont pass�es � l'�tat 1 et cach�es
					document.getElementById('page'+i+'_'+t_num_select[j]).style.display = 'none';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.textDecoration = 'none';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.color = '#e04020';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontweight = 'arial';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontSize = '12pt';
					document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue = 1;
				}
			}
			i += 1;
		}
		for(j = 0; j < t_num_select.length; j++)
		{
			if(nb_page > nb_page_max_par_pagination)
			{
				document.getElementById('suivant_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
				document.getElementById('suivant_'+t_num_select[j]).style.display = 'block';
				document.getElementById('suivant_'+t_num_select[j]).style.textDecoration = 'none';
			}
			if(nb_page > (2*nb_page_max_par_pagination))
			{
				document.getElementById('fin_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
				document.getElementById('fin_'+t_num_select[j]).style.display = 'block';
				document.getElementById('fin_'+t_num_select[j]).style.textDecoration = 'none';
			}
		}
	}
}*/

function affichage_module(module, id_max, nb_page_max_par_pagination,nb_result)
{
	// On met le premier select des modules � la bonne valeur et on appel le fonction comme si c'�tait le select qui l'appelait
	document.getElementById('select_module').value=module;
	selection_module(id_max, nb_page_max_par_pagination,nb_result);
}

function affiche_elements_naviguation(type)
{
	document.getElementById('fleches_'+type+'es_haut').style.display = '';
	document.getElementById('fleches_'+type+'es_bas').style.display = '';
}

function cache_elements_naviguation(type)
{
	document.getElementById('fleches_'+type+'es_haut').style.display = 'none';
	document.getElementById('fleches_'+type+'es_bas').style.display = 'none';
}

function changer_pagination(sens,nb_page_max_par_pagination,nb_resultats_par_page)
{
	// r�cup�ration de l'identifiant de la page actuelle
	num_page_actuelle = parseInt(document.getElementById('page_selectionnee').value);
	num_page_suivante = num_page_actuelle + 1;	
	num_page_precedente = num_page_actuelle - 1;
		
	// ****************************************************
	//
	// si le sens est SUIVANT
	//
	// ****************************************************	
	if(sens == "suivant")
	{
		//on v�rifie s'il y a une page apr�s l'actuelle
		if(document.getElementById('page'+num_page_suivante+'_haut')!=null && document.getElementById('page'+num_page_suivante+'_haut').attributes['etat'].nodeValue==2)
		{
			// on affiche les liens pr�c�dents
			affiche_elements_naviguation('precedent');
			//on commande l'affichage des r�sultats de la page demand�e
			changer_page_resultats(num_page_suivante,nb_resultats_par_page);
		}

		//s'il y a encore des page apr�s la pge que l'on souhaite afficher, 
		//on r�affiche les liens de pagination suivant		
		if(document.getElementById('page'+(num_page_suivante+1)+'_haut')!=null  && document.getElementById('page'+(num_page_suivante+1)+'_haut').attributes['etat'].nodeValue==2)
			affiche_elements_naviguation('suivant');		
		else
			cache_elements_naviguation('suivant');		
	}
	// ****************************************************
	//
	// si le sens est FIN
	//
	// ****************************************************		
	else if(sens == "fin")
	{
		// on recherche la derni�re page 
		t_pages = document.getElementById('liste_page_haut').childNodes;
		num_derniere_page_affichee = num_page_actuelle;
		
		//on fait une boucle sur le span contenant les diff�rents num�rose de page
		for(i=0;i<t_pages.length;i++)
		{
			t_contenu_span_page = t_pages[i].childNodes;
			//regex qui va servir pour r�cup�rer le num�ro de la page
			var reg = new RegExp("[0-9]","g");
			//chacun des num�ros est contenu dans un autre span
			//on refait donc une boucle sur celui-ci, bien qu'il n'ai qu'un seul noeud fils
			for(j=0;j<t_contenu_span_page.length;j++)
			{		
				//si la page est affich�e
				if(t_contenu_span_page[j].attributes['etat'].nodeValue==2)
				{
					//on r�cup�re son num�ro que le dernier
					num_derniere_page_affichee = reg.exec(t_contenu_span_page[j].attributes['id'].nodeValue);
				}
			}
		}
		//s'il elle est > � la page actuelle
		if(num_derniere_page_affichee > num_page_actuelle)
		{
			//on commande l'affichage des r�sultats de la page demand�e
			changer_page_resultats(num_derniere_page_affichee,nb_resultats_par_page);
			//on affiche les liens pr�c�dents
			affiche_elements_naviguation('precedent');		
		}
		//on cache les �l�ments de naviguation suivant
		cache_elements_naviguation('suivant');		
	}
	// ****************************************************
	//
	// si le sens est PRECEDENT
	//
	// ****************************************************	
	else if(sens=="precedent")
	{	
		//on v�rifie s'il y a une page avant l'actuelle
		if(document.getElementById('page'+num_page_precedente+'_haut')!=null && document.getElementById('page'+num_page_precedente+'_haut').attributes['etat'].nodeValue==2)
		{
			//on affiche les �l�ments de naviguation suivant et fin
			affiche_elements_naviguation('suivant');
			//on commande l'affichage des r�sultats de la page demand�e
			changer_page_resultats(num_page_precedente,nb_resultats_par_page);
		}

		//s'il y a encore des page apr�s la pge que l'on souhaite afficher, 
		//on r�affiche les liens de pagination suivant		
		if(document.getElementById('page'+(num_page_precedente-1)+'_haut')!=null  && document.getElementById('page'+(num_page_precedente-1)+'_haut').attributes['etat'].nodeValue==2)
			affiche_elements_naviguation('precedent');		
		else
			cache_elements_naviguation('precedent');		
	}
	// ****************************************************
	//
	// si le sens est DEBUT
	//
	// ****************************************************	
	else if(sens=="debut")
	{
		num_premiere_page = 1;
		
		//s'il elle est < � la page actuelle
		if(num_premiere_page < num_page_actuelle)
		{
			//on commande l'affichage des r�sultats de la page demand�e
			changer_page_resultats(num_premiere_page,nb_resultats_par_page);
			//on affiche les liens suivants
			affiche_elements_naviguation('suivant');		
		}
		//on cache les �l�ments de naviguation precedent
		cache_elements_naviguation('precedent');		
	}
}


//Revoir changer_pagination pour les select
/*function changer_pagination(sens, nb_page_max_par_pagination)
{
	//
	//	Dans tous les commentaires de cette fonction le X correspond toujours � 'nb_page_max_par_pagination'
	//
	//	Si on veut afficher les pages suivantes, on va d�j� chercher � savoir quelles sont celles qui sont affich�es
	//	Sachant celles qui sont affich�es, on va afficher les suivantes
	//
	if(sens == 'suivant')
	{
		var i = 1;
		var flag = false;
		//affiche_proprietes(document.getElementById('testpage1_haut'));
		while(document.getElementById('page'+i+'_haut') && flag == false)
		{
			var page = document.getElementById('page'+i+'_haut');
			// On teste les pages pour savoir qu'elles sont celles qui sont affich�es (on en test 1 sur 5 �a suffit et que celles de la premi�re pagination)
			if(page.attributes['etat'].nodeValue == 2)
			{

				flag = true;
				// Si la page est affich�e, on va cacher celle-ci et les (X-1) suivantes (de m�me pour toutes les paginations)
				for(var j = 0; j < t_num_select.length; j++)
				{
					for(var k = i; k < (i+nb_page_max_par_pagination); k++)
					{
						if(document.getElementById('page'+k+'_'+t_num_select[j])!=null)
						{
							document.getElementById('page'+k+'_'+t_num_select[j]).attributes['etat'].nodeValue = 1;
							document.getElementById('page'+k+'_'+t_num_select[j]).style.display = 'none';
							document.getElementById('page'+k+'_'+t_num_select[j]).style.textDecoration = 'none';
						}
					}
					
					
					if(document.getElementById('precedent_'+t_num_select[j]).attributes['etat'].nodeValue == 2)
					{
						// Si le lien vers les pages pr�c�dentes est d�j� affich�, on affiche le lien vers le d�but
						document.getElementById('debut_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
						document.getElementById('debut_'+t_num_select[j]).style.display = 'block';
						document.getElementById('debut_'+t_num_select[j]).style.textDecoration = 'none';
											
					}
					else
					{
						// Sinon on affiche le lien vers les pages pr�c�dentes et on cache le lien vers le d�but
						document.getElementById('precedent_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
						document.getElementById('precedent_'+t_num_select[j]).style.display = 'block';
						document.getElementById('precedent_'+t_num_select[j]).style.textDecoration = 'none';
						
						document.getElementById('debut_'+t_num_select[j]).attributes['etat'].nodeValue = 0;
						document.getElementById('debut_'+t_num_select[j]).style.display = 'none';
						document.getElementById('debut_'+t_num_select[j]).style.textDecoration = 'none';
					}

					// Et on va afficher les X d'apr�s, dans la limite de nb_page_max_par_pagination et dont l'�tat est � 1
					for(k = (i+nb_page_max_par_pagination); k < (i+(2*nb_page_max_par_pagination)); k++)
					{	
						// On est pas s�r que les X suivantes existent alors on v�rifie que leur �tat est � 1
						if(document.getElementById('page'+k+'_'+t_num_select[j]) && document.getElementById('page'+k+'_'+t_num_select[j]).attributes['etat'].nodeValue == 1)
						{
							// Si l'�tat de la page est � 1 et qu'elle existe, on l'affiche
							document.getElementById('page'+k+'_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
							document.getElementById('page'+k+'_'+t_num_select[j]).style.display = 'block';
							document.getElementById('page'+k+'_'+t_num_select[j]).style.textDecoration = 'none';
						}
					}
				}// Fin for(var j = 0; j < t_num_select.length; j++)

				//
				//	Maintenant qu'on a cach� puis affich� les pages qu'il fallait,
				//	il faut v�rifier si on doit permettre d'afficher les X encore suivantes et le lien vers la fin
				//

				if(document.getElementById('page'+(i+(3*nb_page_max_par_pagination))+'_haut') && document.getElementById('page'+(i+(3*nb_page_max_par_pagination))+'_haut').attributes['etat'].nodeValue == 1)
				{
					// Si y'a des pages apr�s le lien suivant et que leur �tat est � 1, il faut afficher les liens fin et suivant
					for(j = 0; j < t_num_select.length; j++)
					{
						document.getElementById('suivant_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
						document.getElementById('suivant_'+t_num_select[j]).style.display = 'block';
						document.getElementById('suivant_'+t_num_select[j]).style.textDecoration = 'none';
						
						document.getElementById('fin_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
						document.getElementById('fin_'+t_num_select[j]).style.display = 'block';
						document.getElementById('fin_'+t_num_select[j]).style.textDecoration = 'none';
					}
				}
				else if(document.getElementById('page'+(i+(2*nb_page_max_par_pagination))+'_haut') && document.getElementById('page'+(i+(2*nb_page_max_par_pagination))+'_haut').attributes['etat'].nodeValue == 1)
				{
		
					// Si y'a des pages que pour le lien suivant et que leur �tat est � 1, on l'affiche lui et on cache celui vers la fin
					for(j = 0; j < t_num_select.length; j++)
					{
						document.getElementById('suivant_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
						document.getElementById('suivant_'+t_num_select[j]).style.display = 'block';
						document.getElementById('suivant_'+t_num_select[j]).style.textDecoration = 'none';
						
						document.getElementById('fin_'+t_num_select[j]).attributes['etat'].nodeValue = 0;
						document.getElementById('fin_'+t_num_select[j]).style.display = 'none';
						document.getElementById('fin_'+t_num_select[j]).style.textDecoration = 'none';
					}
				}
				else
				{					
					// Si pas de page pour le lien suivant (existe pas ou etat 0) on cache les liens suivant et fin
					for(j = 0; j < t_num_select.length; j++)
					{
						document.getElementById('suivant_'+t_num_select[j]).attributes['etat'].nodeValue = 0;
						document.getElementById('suivant_'+t_num_select[j]).style.display = 'none';
						document.getElementById('suivant_'+t_num_select[j]).style.textDecoration = 'none';
						
						document.getElementById('fin_'+t_num_select[j]).attributes['etat'].nodeValue = 0;
						document.getElementById('fin_'+t_num_select[j]).style.display = 'none';
						document.getElementById('fin_'+t_num_select[j]).style.textDecoration = 'none';
					}
				}
			}// Fin du if(bouton.attributes['etat'].nodeValue == 1)
			i += nb_page_max_par_pagination;
		}// Fin while(document.getElementById('page'+i+'_haut') && flag == false), teste pour trouver les pages qui sont affich�es
	}// Fin if(sens == 'suivant'), pour savoir dans quel sens on veut 'parcourir' la liste des pages
	//
	//	Si on veut afficher les pages pr�c�dentes, on va d�j� chercher � savoir quelles sont celles qui sont affich�es
	//	Sachant celles qui sont affich�es, on va afficher les pages pr�c�dentes
	//
	else if(sens == 'precedent')
	{
		var i = 1;
		var flag = false;
		while(document.getElementById('page'+i+'_haut') && flag == false)
		{
			var bouton = document.getElementById('page'+i+'_haut');
			// On teste les pages pour savoir quelles sont celles qui sont affich�es (on en test 1 sur X �a suffit)
			if(bouton.attributes['etat'].nodeValue == 2)
			{
				flag = true;
				// Si la page est affich�e, on va cacher celle-ci et les (X-1) suivantes, si elles existent toutes
				for(var k = 0; k < t_num_select.length; k++) // Pour toutes les paginations
				{
					for(var j = i; j < (i+nb_page_max_par_pagination); j++)
					{
						if(document.getElementById('page'+j+'_'+t_num_select[k]) && document.getElementById('page'+j+'_'+t_num_select[k]).attributes['etat'].nodeValue == 2)
						{
							bouton = document.getElementById('page'+j+'_'+t_num_select[k]);
							bouton.attributes['etat'].nodeValue = 1;
							bouton.style.display = 'none';
							bouton.style.textDecoration = 'none';
						}
					}
					
					if(document.getElementById('suivant_'+t_num_select[k]).attributes['etat'].nodeValue == 2)
					{
						// Si le lien vers les pages suivantes est d�j� affich�, on va afficher le lien vers la fin
						var lien_fin = document.getElementById('fin_'+t_num_select[k]);
						lien_fin.attributes['etat'].nodeValue = 2;
						lien_fin.style.display = 'block';
						lien_fin.style.textDecoration = 'none';
					}
					else
					{
						// Si ce n'est pas le cas on l'affiche lui et on cache le lien vers la fin
						var lien_suivant = document.getElementById('suivant_'+t_num_select[k]);
						lien_suivant.attributes['etat'].nodeValue = 2;
						lien_suivant.style.display = 'block';
						lien_suivant.style.textDecoration = 'none';
						
						var lien_fin = document.getElementById('fin_'+t_num_select[k]);
						lien_fin.attributes['etat'].nodeValue = 0;
						lien_fin.style.display = 'none';
						lien_fin.style.textDecoration = 'none';
					}
					
					// Et on va afficher les X d'avant, leur �tat est d�j� forc�ment � 1
					for(j = (i-nb_page_max_par_pagination); j < i; j++)
					{
						// On est s�r que les X pr�c�dentes existent et que leur �tat est 1 (sisi c'est vrai!)
						bouton = document.getElementById('page'+j+'_'+t_num_select[k]);
						bouton.attributes['etat'].nodeValue = 2;
						bouton.style.display = 'block';
						bouton.style.textDecoration = 'none';
					}
				}
				
				//
				//	Maintenant qu'on a cach� puis affich� les pages qu'il fallait,
				//	il faut v�rifier si on doit permettre d'afficher les X encore pr�c�dentes et le lien vers le d�but
				//
				if(document.getElementById('page'+(i-(3*nb_page_max_par_pagination))+'_haut'))
				{
					// Si y'a des pages avant le lien pr�c�dent (leur �tat est forc�ment � 1), il faut afficher les liens d�but et pr�c�dent
					for(j = 0; j < t_num_select.length; j++)
					{
						var lien_precedent = document.getElementById('precedent_'+t_num_select[j]);
						lien_precedent.attributes['etat'].nodeValue = 2;
						lien_precedent.style.display = 'block';
						lien_precedent.style.textDecoration = 'none';
						
						var lien_debut = document.getElementById('debut_'+t_num_select[j]);
						lien_debut.attributes['etat'].nodeValue = 2;
						lien_debut.style.display = 'block';
						lien_debut.style.textDecoration = 'none';
					}
				}
				else if(document.getElementById('page'+(i-(2*nb_page_max_par_pagination))+'_haut'))
				{
					// Si y'a des pages que pour le lien pr�c�dent (�tat forc�ment � 1), on l'affiche lui et on cache celui vers le d�but
					for(j = 0; j < t_num_select.length; j++)
					{
						var lien_precedent = document.getElementById('precedent_'+t_num_select[j]);
						lien_precedent.attributes['etat'].nodeValue = 2;
						lien_precedent.style.display = 'block';
						lien_precedent.style.textDecoration = 'none';
						
						var lien_debut = document.getElementById('debut_'+t_num_select[j]);
						lien_debut.attributes['etat'].nodeValue = 0;
						lien_debut.style.display = 'none';
						lien_debut.style.textDecoration = 'none';
					}
				}
				else
				{
					// Si pas de page pour le lien pr�c�dent on cache les liens pr�c�dent et d�but
					for(j = 0; j < t_num_select.length; j++)
					{
						var lien_precedent = document.getElementById('precedent_'+t_num_select[j]);
						lien_precedent.attributes['etat'].nodeValue = 0;
						lien_precedent.style.display = 'none';
						lien_precedent.style.textDecoration = 'none';
						
						var lien_debut = document.getElementById('debut_'+t_num_select[j]);
						lien_debut.attributes['etat'].nodeValue = 0;
						lien_debut.style.display = 'none';
						lien_debut.style.textDecoration = 'none';
					}
				}
			}// Fin if(bouton.attributes['etat'].nodeValue == 1)
			i += nb_page_max_par_pagination;
		}// Fin while(document.getElementById('page'+i+'_haut') && flag == false), teste pour trouver les pages qui sont affich�es
	}// Fin else if(sens == 'precedent'), pour savoir dans quel sens on veut 'parcourir' la liste des pages
	//
	//	Si on veut afficher les premi�res pages
	/
	else if(sens == 'debut')
	{
		for(var j = 0; j < t_num_select.length; j++)
		{
			// On commence par cacher les liens vers le d�but et les pages pr�c�dentes
			var lien_debut = document.getElementById('debut_'+t_num_select[j]);
			lien_debut.attributes['etat'].nodeValue = 0;
			lien_debut.style.display = 'none';
			lien_debut.style.textDecoration = 'none';
			
			var lien_precedent = document.getElementById('precedent_'+t_num_select[j]);
			lien_precedent.attributes['etat'].nodeValue = 0;
			lien_precedent.style.display = 'none';
			lien_precedent.style.textDecoration = 'none';
			
			// On montre les pages 1 � nb_page_max_par_pagination et on cache les suivantes
			var i = 1;
			while(document.getElementById('page'+i+'_'+t_num_select[j]))
			{
				if(document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue == 1 && i <= nb_page_max_par_pagination)
				{
					// On affiche les premi�res pages
					var page = document.getElementById('page'+i+'_'+t_num_select[j]);
					page.attributes['etat'].nodeValue = 2;
					page.style.display = 'block';
					page.style.textDecoration = 'none';
				}
				else if(document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue == 2)
				{
					// On cache les pages d�j� affich�es
					var page = document.getElementById('page'+i+'_'+t_num_select[j]);
					page.attributes['etat'].nodeValue = 1;
					page.style.display = 'none';
					page.style.textDecoration = 'none';
				}
				i += 1;
			}
			
			// On montre les liens suivant et fin, car si on a pu cliquer sur |<< c'est qu'on a suffisemment de pages
			var lien_suivant = document.getElementById('suivant_'+t_num_select[j]);
			lien_suivant.attributes['etat'].nodeValue = 2;
			lien_suivant.style.display = 'block';
			lien_suivant.style.textDecoration = 'none';
			
			var lien_fin = document.getElementById('fin_'+t_num_select[j]);
			lien_fin.attributes['etat'].nodeValue = 2;
			lien_fin.style.display = 'block';
			lien_fin.style.textDecoration = 'none';
		}
	}// Fin else if(sens == 'debut'), pour savoir dans quel sens on veut 'parcourir' la liste des pages
	//
	//	Si on veut afficher les derni�res pages
	//
	else if(sens == 'fin')
	{
		for(var j = 0; j < t_num_select.length; j++)
		{
			// On affiche les liens d�but et pr�c�dent, ayant cliqu� sur 'fin' c'est qu'on a suffisemment de pages
			var lien_debut = document.getElementById('debut_'+t_num_select[j]);
			lien_debut.attributes['etat'].nodeValue = 2;
			lien_debut.style.display = 'block';
			lien_debut.style.textDecoration = 'none';
			
			var lien_precedent = document.getElementById('precedent_'+t_num_select[j]);
			lien_precedent.attributes['etat'].nodeValue = 2;
			lien_precedent.style.display = 'block';
			lien_precedent.style.textDecoration = 'none';
			
			// On cache toutes les pages qui sont affich�es
			var i = 1;
			var cpt = 0;
			while(document.getElementById('page'+i+'_'+t_num_select[j]))
			{
				if(document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue == 2)
				{
					var page = document.getElementById('page'+i+'_'+t_num_select[j]);
					page.attributes['etat'].nodeValue = 1;
					page.style.display = 'none';
					page.style.textDecoration = 'none';
					cpt += 1;
				}
				else if(document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue == 1)
				{
					cpt += 1;
				}
				i += 1;
			}
			// 'i' contient le nombre de pages + 1 (totales : etat 1 et 0, et donc c'est pas interressant)
			// 'cpt' contient le nombre de page correct : etat 1, on va s'appuyer dessus pour afficher les derni�res pages
			var modulo_nb_page = cpt % nb_page_max_par_pagination;
			if(modulo_nb_page == 0)
			{
				// Afficher les nb_page_max_par_pagination derni�res pages
				modulo_nb_page = nb_page_max_par_pagination;
			}
			// Afficher les modulo_nb_page derni�res pages
			for(var k = cpt; k > (cpt - modulo_nb_page); k--)
			{
				var page = document.getElementById('page'+k+'_'+t_num_select[j]);
				page.attributes['etat'].nodeValue = 2;
				page.style.display = 'block';
				page.style.textDecoration = 'none';
			}
			
			// On cache les liens suivant et fin
			var lien_suivant = document.getElementById('suivant_'+t_num_select[j]);
			lien_suivant.attributes['etat'].nodeValue = 0;
			lien_suivant.style.display = 'none';
			lien_suivant.style.textDecoration = 'none';
			
			var lien_fin = document.getElementById('fin_'+t_num_select[j]);
			lien_fin.attributes['etat'].nodeValue = 0;
			lien_fin.style.display = 'none';
			lien_fin.style.textDecoration = 'none';
		}
	}// Fin else if(sens == 'fin'), pour savoir dans quel sens on veut 'parcourir' la liste des pages
}*/

function changer_page_resultats(num_page,nb_result_par_page)
{
	var id_from = (num_page - 1) * nb_result_par_page + 1;
	
	// On s'occupe de coloriser le lien vers la page en cours
	var i = 1;
	var nb_pages = 0;
	var num_page = Math.ceil(id_from / nb_result_par_page);

	while(document.getElementById('page'+i+'_haut'))
	{
		for(var j = 0; j < t_num_select.length; j++)
		{
			/*
			* Les if, else et else if suivants (dans le for), paraissent contenir des commandes inutiles
			*	Mais lorsque l'on cherche � modifier le style seul, le display est modifi� inoportun�ment et automatiquement
			*	par javascript... donc pour �viter ce probl�me on a rajout� ces commandes 'inutiles' pour �tre s�r d'avoir
			*	l'affichage que l'on souhaite
			*/
			if(i == num_page)
			{
				document.getElementById('page'+i+'_'+t_num_select[j]).style.color = 'black';
				document.getElementById('page'+i+'_'+t_num_select[j]).style.fontweight = 'bold';
				document.getElementById('page'+i+'_'+t_num_select[j]).style.fontSize = '15pt';
				document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
				document.getElementById('page_selectionnee').value = num_page;
				document.getElementById('page'+i+'_'+t_num_select[j]).style.display = 'block';
			}
			else
			{
				if(document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue == 2)
				{
					document.getElementById('page'+i+'_'+t_num_select[j]).style.color = '#e04020';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontweight = 'arial';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontSize = '12pt';
					document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
					document.getElementById('page'+i+'_'+t_num_select[j]).style.display = 'block';
				}
				else if(document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue == 1)
				{
					document.getElementById('page'+i+'_'+t_num_select[j]).style.color = '#e04020';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontweight = 'arial';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontSize = '12pt';
					document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue = 1;
					document.getElementById('page'+i+'_'+t_num_select[j]).style.display = 'none';
				}
			}
		}
		
		
		if(document.getElementById('page'+i+'_haut').attributes['etat'].nodeValue == 2)
		{
			nb_pages += 1;
		}
		
		i += 1;
	}

	//on s'occupe maintenant des fl�che pr�c�dentes et suivantes
	
	/* si on se trouve sur la premi�re page,
		- on n'affiche pas les fl�ches permettant d'aller aux pages pr�c�dentes
		- on affiche les pages permettant d'aller au liens suivant si il y a plusieurs page pour la recherche
	*/
	
	/* si on se trouve sur une page sup�rieure � la premi�re page : 
		- on affiche les fl�ches permettant d'aller � la page pr�c�dente
		- on affiche les fl�ches permettant d'aller au pages suivantes si il y  d'aures pages apr�s
	*/
	
	//on affiche les fl�ches pr�c�dentes
	if(num_page > 1)
		affiche_elements_naviguation('precedent');
	else
		cache_elements_naviguation('precedent');
		
		
	//on affiche les fl�ches suivantes
	if(num_page < nb_pages)
		affiche_elements_naviguation('suivant');
	else
		cache_elements_naviguation('suivant');
	
	var cpt = 0;
	i = 1;
	while(document.getElementById(i))
	{
		// On fait d�filer toutes les r�sultats
		if(document.getElementById(i).attributes['etat'].nodeValue == 1)
		{
			cpt += 1;
		}
		else if(document.getElementById(i).attributes['etat'].nodeValue == 2)
		{
			// On cache les r�sultats affich�s
			document.getElementById(i).attributes['etat'].nodeValue = 1;
			document.getElementById(i).style.display = 'none';
			cpt += 1;
		}
		if(cpt >= id_from && cpt < (id_from + nb_result_par_page) && document.getElementById(i).attributes['etat'].nodeValue == 1)
		{
			// On affiche les r�sultats qu'il faut
			document.getElementById(i).attributes['etat'].nodeValue = 2;
			document.getElementById(i).style.display = 'block';
		}
		i += 1;
	}
}

function change_nb_result_max_par_page(nb_page_max_par_pagination)
{
	var select_nb_result_max_par_page = document.getElementById('select_nb_resultats_max_par_page');
	var nb_result_par_page = parseInt(select_nb_result_max_par_page.options[select_nb_result_max_par_page.selectedIndex].value);
	
	// Il faut changer la pagination
	// Affichage des premiers r�sultats, on en profite pour compter le nombre de r�sultats qui satisfont le 'tri' des select
	var i = 1;
	var cpt = 0;
	while(document.getElementById(i))
	{
		if(document.getElementById(i).attributes['etat'].nodeValue == 2)
		{
			cpt += 1;
			document.getElementById(i).attributes['etat'].nodeValue = 1;
			document.getElementById(i).style.display = 'none';
		}
		else if(document.getElementById(i).attributes['etat'].nodeValue == 1)
		{
			cpt += 1;
		}
		if(cpt <= nb_result_par_page && document.getElementById(i).attributes['etat'].nodeValue == 1)
		{
			document.getElementById(i).attributes['etat'].nodeValue = 2;
			document.getElementById(i).style.display = 'block';
		}
		i += 1;
	}
	
	// On va maintenant pouvoir refaire la pagination
	
	// On commence par tout cacher (les pages et les r�sultats)
	for(var j = 0; j < t_num_select.length; j++)
	{
		// On cache tous les boutons d'affichage des pages de r�sultats
		cache_elements_naviguation('precedent');
		cache_elements_naviguation('suivant');
		
		i = 1;
		while(document.getElementById('page'+i+'_'+t_num_select[j]))
		{
			document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue = 0;
			document.getElementById('page'+i+'_'+t_num_select[j]).style.fontweight = 'arial';
			document.getElementById('page'+i+'_'+t_num_select[j]).style.display = 'none';
			i += 1;
		}
	}
	
	// On affiche les liens qu'il faut
	if(cpt > nb_result_par_page)
	{
		var	nb_page = Math.ceil(cpt / nb_result_par_page);
		i = 1;
		while(document.getElementById('page'+i+'_haut'))
		{
			for(j = 0; j < t_num_select.length; j++)
			{
				if(i == 1)
				{
					// La premi�re page est color�e et affich�e
					document.getElementById('page'+i+'_'+t_num_select[j]).style.color = 'black';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontweight = 'bold';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontSize = '15pt';
					document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
					document.getElementById('page'+i+'_'+t_num_select[j]).style.display = 'block';
				}
				else if(i <= nb_page_max_par_pagination && i <= nb_page)
				{
					// Les premi�res pages sont affich�es (mais pas color�es)
					document.getElementById('page'+i+'_'+t_num_select[j]).style.color = '#e04020';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontweight = 'arial';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontSize = '12pt';
					document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
					document.getElementById('page'+i+'_'+t_num_select[j]).style.display = 'block';
				}
				else if(i <= nb_page)
				{
					// Le nombre de pages correspondantes aux r�sultats (cpt) sont pass�es � l'�tat 1 et cach�es
					document.getElementById('page'+i+'_'+t_num_select[j]).style.color = '#e04020';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontweight = 'arial';
					document.getElementById('page'+i+'_'+t_num_select[j]).style.fontSize = '12pt';
					document.getElementById('page'+i+'_'+t_num_select[j]).attributes['etat'].nodeValue = 1;
					document.getElementById('page'+i+'_'+t_num_select[j]).style.display = 'none';
				}
			}
			i += 1;
		}
		for(j = 0; j < t_num_select.length; j++)
		{
			if(nb_page > nb_page_max_par_pagination)
			{
				document.getElementById('suivant_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
				document.getElementById('suivant_'+t_num_select[j]).style.display = 'block';
			}
			if(nb_page > (2*nb_page_max_par_pagination))
			{
				document.getElementById('fin_'+t_num_select[j]).attributes['etat'].nodeValue = 2;
				document.getElementById('fin_'+t_num_select[j]).style.display = 'block';
			}
		}
	}
}