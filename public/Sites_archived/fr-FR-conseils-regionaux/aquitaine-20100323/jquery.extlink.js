// -----------------------------------------------------------------------------
// TODO : documentation, licence etc...
// TODO : rename this as 'links' or 'urls' since it's finally more about links
//        and urls in general
// -----------------------------------------------------------------------------
(function($) {
    // -------------------------------------------------------------------------
    // Query Strings
    // -------------------------------------------------------------------------
    function parseQueryString(qs) {
        // Parse a query string into an object
        // in: the query string (NOT the whole url nor a link object)
        
        // clean up if necessary
        qs = qs.replace(/^\?/,'');
        var qargs = {};        
        var parts = qs.split('&');
        for (var i = 0; i < parts.length; i++) {
            var kv = parts[i].split('=');
            var k = kv.shift();
            var v = kv.shift();
            // handle multiple values for a same key
            if (k in qargs) {
                var tmp = qargs[k];
                if ((tmp instanceof Array)) {
                    tmp.push(v);
                }
                else {
                    qargs[k] = [tmp, v];
                }
            }
            else {
                // common case
                qargs[k] = v;
            }
        }
        return qargs;
    };
    
    function buildQueryString(qargs) {
        // Reverse of the above : rebuild a query string from a qargs object
        var qs = [];
        for (key in qargs) {
            qs.push(key + '=' + qargs[key]); 
        }
        return qs.join('&');
    };
    
    function updateQueryString(obj, updates) {
        // update a querystring based on key/values in updates
        // obj : either a link object or the querystring itself (cf parseQueryString)
        var isLink = (obj instanceof HTMLElement && 'search' in obj); 
        var qs = isLink ? obj.search : obj;
        var qargs = parseQueryString(qs);      
        $.extend(qargs, updates);        
        if (isLink) {
            obj.search = buildQueryString(qargs);
        }
        else {
            obj = buildQueryString(qargs);
        }
        return obj;
    };
    // -------------------------------------------------------------------------
    $.urls = {
       parseQueryString:parseQueryString,
       buildQueryString:buildQueryString,
       updateQueryString:updateQueryString
    };
    
    // -------------------------------------------------------------------------
    // utils
    // -------------------------------------------------------------------------
    function sluggify(title) {
        // version javascript du filtre spip wsblib::wsb_options::texte_slug
        // attention à garder les deux en synchro.
        var dict = {
            '\'':' ',
            '-':' ',
            'à':'a',        
            'á':'a',
            'â':'a',
            'ä':'a',
            'ç':'c',
            'è':'e',
            'é':'e',
            'ê':'e',
            'ë':'e',
            'í':'i',
            'î':'i',
            'ï':'i',
            'ñ':'n',
            'ó':'o',
            'ô':'o',
            'ö':'o',
            'ù':'u',
            'û':'u',
            'ü':'u'
        };
    
        var slug = title.toLowerCase();
        slug = slug.replace(/([àáâäçèéêëíîïñóôöùûü\' -])/g, function(c) { return dict[c] || ' '; });
        slug = $.trim(slug.replace(/[^a-z0-9\s_~ -]/g, ""));
        slug = slug.replace(/[\-\s]+/g, "-");
        return slug;
    };
    $.sluggify = sluggify;

    // -------------------------------------------------------------------------
    // ExtLink
    // -------------------------------------------------------------------------
    function buildDomainRegexp(options) {
        var hostname = window.location.hostname;
        var parts = hostname.split('.');
            
        if (options.sameDomainOnly) {
            var re = "^" + parts.join('\\.');
        }
        else {
            var domain = parts.slice(-2).join('\\.');
            var re = "^(?:[a-zA-Z0-9_-]+\\.)*?" + domain;
        }
        return new RegExp(re);
    };
    
    function isExtLink(a, options) {
        // XXX : rewrite this using the link element attributes, cf flanagan p.650
        var href = a.href;
        if (! href) {
            return false;
        }
        var parts = href.split(/\:\/\//);
        if (parts.length == 1) {
            // no protocol, can't be an external link
            return false;
        }
        if (parts[0] == 'javascript') {
            return false;
        }
        var domain = parts[1].split('/')[0];
        if (domain.search(options.domainRegexp) != -1) {
            return false;
        }
        return true;
    };
     
    $.fn.ExtLink = function(options) {
        options = $.extend({}, $.fn.ExtLink.defaults, options);
        if (! options.domainRegexp) {
            options.domainRegexp = buildDomainRegexp(options);
        }        
        var isExtLink = options.isExtLink;        
        
        return $('a', this).filter(function() { 
                return isExtLink(this, options); 
            });        
    };
    
    $.fn.ExtLink.defaults = {
            sameDomainOnly: false, // treat subdomains (or parent domain) as external
            isExtLink: isExtLink
    };
    
 })(jQuery);


