
#
# General rules
#
User-agent: *

#
# Disallowed directories
#
Disallow: /export/
Disallow: /city/
Disallow: /test/
Disallow: /MTE_CP/blogs/stubs/
Disallow: /MTE_CP/blogs/template/
Disallow: /MTE_CP/blogs/category_master/
Disallow: /MTE_CP/roxenfeeds/

#
# Google Sitemaps
#
Sitemap: http://www.metrofrance.com/sitemap.xml
