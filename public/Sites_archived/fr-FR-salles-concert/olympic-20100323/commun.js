function changedisplay (id) {
	var element = document.getElementById(id);
	if (element.style.display == 'block') {
		element.style.display = 'none';
	} else { 
		element.style.display = 'block';
	}
}

function plusminus(id) {
	var element = document.getElementById('div_plusminus'+id);
	var lien = document.getElementById('a_plusminus'+id);
	if (element.style.display == 'block') {
		element.style.display = 'none';
		lien.className = 'plus';
	} else { 
		element.style.display = 'block';
		lien.className = 'minus';		
	}
}

function montre(id) {
var d = document.getElementById('smenu'+id);
var a = document.getElementById('lien_menu'+id);
	for (var i = 0; i<=10; i++) {
		if (document.getElementById('smenu'+i)) {document.getElementById('smenu'+i).style.display='none';}
		if (document.getElementById('lien_menu'+i)) {document.getElementById('lien_menu'+i).className='';}
	}
if (d) {d.style.display='block';}
if (a) {a.className='on';}
}
