<!--

// JavaScript Document
// Navigation Nouvelliste

function draw_navig(){
	var main = 0;
	var sub = 0;
	var subsub = 0;
	for(i=0;i<navig.length;i++){
		document.write('<div class="');
		if(navig[i][0] == 0){
			document.write('navig_0');
			main++;
			sub = 0;
			subsub = 0;
		} else if(navig[i][0] == 1) {
			document.write('navig_1');
			sub++;
			subsub = 0;
		} else {
			document.write('navig_2');
			subsub++;
		}
		document.write('" id="nav_'+main+'_'+sub+'_'+subsub+'"');
		if(navig[i][0] > 1){
			document.write(' style="padding-right:10px;"');
		}   
		document.write('>');
		document.write('<a id="navlink_'+main+'_'+sub+'_'+subsub+'" href="javascript:show('+main+','+sub+','+subsub+');"');
		navig_num[i] = main+'_'+sub+'_'+subsub;
		document.write('>');
		document.write(navig[i][1]+'</a>');
		document.write('</div>');
	}
}

function hide_all_sub(){
for(i=0;i<navig_num.length;i++){
val = navig_num[i].split("_");
if(val[0] != 0 && val[1] != 0){
hide_item(val[0],val[1],val[2]);
}
}
visible = '0_0_0';
}

function show(m,s,ss){
	// open URL
	if(open_url(m,s,ss) == 0){
		vis = visible.split("_");
		if(visible == '0_0_0'){
			show_sub(m,0,0);
			visible = m+'_0_0';
		} else if(ss == 0) {
			if(vis[0] != m){
				//## change sub
				hide_sub(vis[0],0,0);
				show_sub(m,0,0);
				visible = m+'_0_0';
			} else if(vis[0] == m && vis[1] == s){
				if(s == 0){
					//## hide sub & subsub and go to start view
					if(navigator.userAgent.indexOf("Safari") == -1){
						hide_sub(m,0,0);
						visible = '0_0_0';
					}
				} else {
					//## hide subsub
					if(navigator.userAgent.indexOf("Safari") == -1){
						hide_sub(m,vis[1],0);
						visible = m+'_0_0';
					}
				}
			} else if(vis[0] == m && vis[1] != s && s != 0){
				//## show subsub
				if(vis[1] != 0){
					hide_sub(m,vis[1],0);
				}
				show_sub(m,s,ss);
				visible = m+'_'+s+'_'+0;
			} else if(vis[0] == m && vis[1] != s && s == 0){
				//## hide sub [2]
					if(navigator.userAgent.indexOf("Safari") == -1){
					hide_sub(m,0,0);
					visible = '0_0_0';
				}
			}
		}
	}
}

function show_sub(m,s,ss){
if(m != 0){
// show
for(i=0;i<navig_num.length;i++){
val = navig_num[i].split("_");
if(s == 0 && ss == 0 && val[0] == m && val[2] == 0){
// show sub
show_item(val[0],val[1],val[2]);
} else if(ss == 0 && val[0] == m && val[1] == s){
show_item(val[0],val[1],val[2]);
}
}
}
}

function hide_sub(m,s,ss){
if(m != 0){
// hide
for(i=0;i<navig_num.length;i++){
val = navig_num[i].split("_");
if(s != 0 && ss == 0 && (val[0] == m && val[1] == s && val[2] != 0)){
// hide subsub
hide_item(val[0],val[1],val[2]);
} else if(s == 0 && ss == 0 && val[0] == m && val[1] != 0){
// hide sub
hide_item(val[0],val[1],val[2]);
}
}
}
}

function show_item(m,s,ss){
if(s != 0 || ss != 0) {
var elem = document.getElementById("nav_"+m+"_"+s+"_"+ss+"");
elem.style.display = "block";
}
}

function hide_item(m,s,ss){
if(s != 0 || ss != 0){
//alert(m+'_'+s+'_'+ss);
var elem = document.getElementById("nav_"+m+"_"+s+"_"+ss+"");
elem.style.display = "none";
}
}

function open_selected(m,s,ss){
if(m != 0){
if(s != 0){
show_sub(m,0,0);
}
show_sub(m,s,0);
visible = m+'_'+s+'_0';
selected_item(m,s,ss);
}
}

function open_selected_item(url){
  navig_r = navig.reverse();
  navig_num_r = navig_num.reverse();
  for(i=0;i<navig_r.length;i++){
    //alert("[test] "+navig_r[i][2]);
    if(navig_r[i][2].indexOf(url) != -1){
      level = navig_num_r[i].split("_");
      open_selected(level[0],level[1],level[2]);
      break;
    }
  }
}
/* use with:
<script language="JavaScript" type="text/JavaScript">
open_selected_item('<?= $_SERVER['PHP_SELF'];?>');
</script>
*/

function selected_item(m,s,ss){
var col = '#ffffff';

var elem = document.getElementById("navlink_"+m+"_"+s+"_"+ss+"");
elem.style.color = col;
elem.style.fontWeight = 'bold';
if(ss != 0){
elem = document.getElementById("navlink_"+m+"_"+s+"_0");
elem.style.color = col;
elem.style.fontWeight = 'bold';
}
if(s != 0){
elem = document.getElementById("navlink_"+m+"_0_0");
elem.style.color = col;
elem.style.fontWeight = 'bold';
}
}

function open_url(m,s,ss){
	var url = '#';
	for(i=0;i<navig_num.length;i++){
		val = navig_num[i].split("_");
		if(val[0] == m && val[1] == s && val[2] == ss){
			url = navig[i][2];
			break;
		}
	}
	if(url != '#'){
		if(typeof base_url != 'undefined'){
		// add base_url before link only if it's not a *tp:// link
			if(url.indexOf("tp://") == -1){
				url = base_url+url;
			}
		}
		if(navig[i][3] == '_blank'){
			window.open(url,'link_new_window','');
		} else if (navig[i][3] == '_parent'){
			eval("parent.location = '"+url+"'");
		} else {
			eval("window.location = '"+url+"'");
		}
		return 1;
	} else {
		return 0;
	}
}
// -->
