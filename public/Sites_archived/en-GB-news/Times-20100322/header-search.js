
// Fetching host url for TLS search
var url = window.location.host;

// -----------------------
// FUNCTION: fValidateData
// DESCRIPTION: A function that validates the text field and assigns the action urls as per the the selected value 
// ARGUMENTS: None
// RETURN: true or false
// Added for artf624540
// -----------------------

function fValidateData() {
	

	var aSelectedOption=aActionUrlsTopSearch[document.getElementById("searchOption").value];
	if( (aSelectedOption.action == "/tol/viewSearchResults.arc" || aSelectedOption.action == "/tol/sitesearch.do") && fTrim(document.getElementById("top-search-query").value).length<=0) {
		alert("Please enter any keyword to search ");
		return false;
	}
	else {
		document.getElementById("top-search-query").value = fTrim(document.getElementById("top-search-query").value); //Removing leading & trailing spaces
		if(aSelectedOption.action == "/tol/viewSearchResults.arc") {
			if(!fAlphaNumericValidate(document.getElementById("top-search-query").value)) {
				alert("Please enter a valid serach term");
				return false;
			}
			document.getElementById("top-search-query").value=document.getElementById("top-search-query").value.replace(/\"/g,'');
		}
		
		document.forms["searchHeaderId"].action=aSelectedOption.action;	

		if(aSelectedOption.action == "/tol/tlskeywordsearch.tls") {
			var isKeywordSplChar = fValidateTLSSearchTextinTOL(document.getElementById('top-search-query').value);
			if(isKeywordSplChar == 0){
				document.forms["searchHeaderId"].action='http://'+ url + aSelectedOption.action;
			}else if(isKeywordSplChar == 1){
				alert("You have not entered any search terms. Please enter at least one word into the search box.");
				return false;
			}else{
				alert("Please enter a valid search term.");
				return false;
			}
			
		}
		
		document.getElementById("top-search-query").name=aSelectedOption.searchTextName;
		fDynamicAssignInputTags(aSelectedOption,"searchHeaderId");
		return true;
	}
}


// -----------------------
// FUNCTION: fValidateDataFooter
// DESCRIPTION: A function that validates the text field and assigns the action urls as per the the selected value 
// ARGUMENTS: None
// RETURN: true or false
// Added for artf624540
// -----------------------
function fValidateDataFooter(){
	var aSelectedOption=aActionUrlsBottomSearch[document.getElementById("searchOptionFooter").value];
	if( (aSelectedOption.action == "/tol/viewSearchResults.arc" || aSelectedOption.action == "/tol/sitesearch.do") && document.getElementById("bottom-search-query").value == ""){
		alert("Please enter any keyword to search ");
		return false;
	}
	else {
		document.getElementById("bottom-search-query").value = fTrim(document.getElementById("bottom-search-query").value); //Removing leading & trailing spaces
		if(aSelectedOption.action == "/tol/viewSearchResults.arc") {
			if(!fAlphaNumericValidate(document.getElementById("bottom-search-query").value)) {
               	alert("Please enter a valid search term");
				return false;
			}
			document.getElementById("bottom-search-query").value=document.getElementById("bottom-search-query").value.replace(/\"/g,'');
		}
		
		document.forms["searchFooterId"].action=aSelectedOption.action;	

		if(aSelectedOption.action == "/tol/tlskeywordsearch.tls") {
			var isKeywordSplChar = fValidateTLSSearchTextinTOL(document.getElementById('bottom-search-query').value);
			if(isKeywordSplChar == 0){
				document.forms["searchFooterId"].action='http://'+ url + aSelectedOption.action;
			}else if(isKeywordSplChar == 1){
				alert("You have not entered any search terms. Please enter at least one word into the search box.");
				return false;
			}else{
				alert("Please enter a valid search term.");
				return false;
		}
		
		}
		
		document.getElementById("bottom-search-query").name=aSelectedOption.searchTextName;
		fDynamicAssignInputTags(aSelectedOption,"searchFooterId");
		return true;
	}
}


// -----------------------
// FUNCTION: fValidateDataSearchResults
// DESCRIPTION: A function that validates the text field and assigns the action urls as per the the selected value for search results page and changes the name of serach boxes dynamically.
// ARGUMENTS: None
// RETURN: true or false
// -----------------------
function fValidateDataSearchResults() {
	var aSelectedOption=aActionUrlsSearchResult[document.getElementById("search-results-search-options").value];
	if( (aSelectedOption.action == "/tol/viewSearchResults.arc" || aSelectedOption.action == "/tol/sitesearch.do") && document.getElementById("search-results-search").value == "") {
		alert("Please enter any keyword to search");
		return false;
	}
	else {
		document.getElementById("search-results-search").value = fTrim(document.getElementById("search-results-search").value); //Removing leading & trailing spaces
		if(aSelectedOption.action == "/tol/viewSearchResults.arc") {
			if(!fAlphaNumericValidate(document.getElementById("search-results-search").value)) {
                alert("Please enter a valid search term");
				return false;
			}
			document.getElementById("search-results-search").value=document.getElementById("search-results-search").value.replace(/\"/g,'');
		}
		
		document.forms["searchResultId"].action=aSelectedOption.action;	

		if(aSelectedOption.action == "/tol/tlskeywordsearch.tls") {
			var isKeywordSplChar = fValidateTLSSearchTextinTOL(document.getElementById('search-results-search').value);
			if(isKeywordSplChar == 0){
				document.forms["searchResultId"].action='http://'+ url + aSelectedOption.action;
			}else if(isKeywordSplChar == 1){
				alert("You have not entered any search terms. Please enter at least one word into the search box.");
				return false;
			}else{
				alert("Please enter a valid search term.");
				return false;
			}
			
		}

		document.getElementById("search-results-search").name=aSelectedOption.searchTextName;
		fDynamicAssignInputTags(aSelectedOption,"searchResultId");
		return true;
	}	
}

// -----------------------
// FUNCTION: fDynamicAssignInputTags
// DESCRIPTION: A function that assigns dynamic hiddens tags toa  form elements
// ARGUMENTS: Array, eForm
// RETURN: true or false
// -----------------------
function fDynamicAssignInputTags(aArray, eForm) {
	document.getElementById(eForm+"-additional-parameters").innerHTML="";
	for(i in aArray.parameters) {
		var tempInput=document.createElement('input');
		tempInput.type="hidden";
		tempInput.name=i;
		tempInput.id=i;
		if(aArray.parameters[i] != ""){
			tempInput.value=aArray.parameters[i];
		}
		document.getElementById(eForm+"-additional-parameters").appendChild(tempInput);
	}			
}

// -----------------------
// FUNCTION: fTrim
// DESCRIPTION: A function that trim the leading and trailing spaces
// ARGUMENTS: sString 
// RETURN: Trimmed String(Removing the leading & trailing spaces)
// -----------------------
function fTrim(sString){
 var sRegEx=/\s*((\S+\s*)*)/;
 sString=sString.replace(sRegEx, "$1");
 sRegEx=/((\s*\S+)*)\s*/;
 sString=sString.replace(sRegEx, "$1");
 return sString;
}

// -----------------------
// FUNCTION: fAlphaNumericValidate
// DESCRIPTION: A function that checks the alpha sring for alphabets & numbers as well as null values
// ARGUMENTS: sString. There should be no leading or trailing spaces in it.
// RETURN: true or false
// -----------------------
function fAlphaNumericValidate(sString){
	return /^([a-z]|[A-Z]|[0-9]|['"]|\s)*$/.test(sString); //"'balancing the quotes.
}


//-----------------------
//FUNCTION: fValidateTLSSearchTextinTOL
//DESCRIPTION: A function that validates the text field for alphanumeric chars. 
//ARGUMENTS: queryKeywords
//RETURN: Integer value
//AUTHOR: Vikas Parmar
//-----------------------

function fValidateTLSSearchTextinTOL(searchFname){
	var myChar = "";
	var totArrLength = 0;
	var isValid;
	var isThereValue = 0;
	var specialChars= new Array('!','�','#','$','%','*','/',':',';','\\','^','|','~','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�',' ','@','&','(',')','=','+','-','_','[','{','}',']','\"','<','>','','?','`','\'',',','.');
	
	searchFname = fTrim(searchFname);
	
	if(searchFname  == "" && searchFname.length<=0){
		isThereValue = 1;
	}else{
		for(i=0;i<26;i++){
			//A to Z
			myChar = myChar + String.fromCharCode(65+i);
		}
		for(i=0;i<26;i++){
			//a- z
			myChar = myChar + String.fromCharCode(97+i);
		}
		for(i=0;i<10;i++){
			//0 to 9
			myChar = myChar + String.fromCharCode(48+i);
		}
		for(i=0;i<specialChars.length;i++){
			//0 to 9
			myChar = myChar + specialChars[i];
		}
		for(i=0;i<searchFname.length;i++){
			var char = searchFname.charAt(i);
			isValid = myChar.indexOf(char);
			//alert("char["+i+"]: "+char+", isValid : "+isValid );
			if(isValid == -1){
				isThereValue = 2;
			}
		}
	}
	return isThereValue;
}
