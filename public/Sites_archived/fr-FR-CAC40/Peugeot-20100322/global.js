function pop(url,title,parameters)
   {
     window.open(url,title,parameters);
   }


function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      oldonload();
      func();
    }
  }
}
function resetFields(whichform) {
  for (var i=0; i<whichform.elements.length; i++) {
    var element = whichform.elements[i];
    if (element.type == "submit") continue;
    if (!element.defaultValue) continue;
    element.onfocus = function() {
    if (this.value == this.defaultValue) {
      this.value = "";
     }
    }
    element.onblur = function() {
      if (this.value == "") {
        this.value = this.defaultValue;
      }
    }
  }
}
function prepareForms() {
//  for (var i=0; i<document.forms.length; i++) {
    var thisform = document.forms['extranav_search'];
    resetFields(thisform);
    thisform.onsubmit = function() {
      return validateForm(this);
    }
//  } 
 
}

//function validateForm(e)
//{
  //dummy validation function
//  return e;
//}

function addClass(element,value) {
  if (!element.className) {
    element.className = value;
  } else {
    newClassName = element.className;
    newClassName+= " ";
    newClassName+= value;
    element.className = newClassName;
  }
}
// FUNCTION: A function shows or hides an element.
// ARGUMENTS: The id of the element to be shown or hidden
function fShowHideElement(sElementId) {
	var eElement = document.getElementById(sElementId);
	var sClassName = eElement.className;
	if (sClassName.match(' hide') || sClassName.match('hide')) {
		// Shows element
		eElement.className = eElement.className.replace(/hide/, '');
	} else {
		// Hides element
		eElement.className = sClassName + ' hide';
	}
}
// FUNCTION: A function that hides one element and shows another - uses fShowHideElement 
// ARGUMENTS: The ID of the element to be shown/hidden, The name of the variable holding the id of the current open element
function fShowHideSwapper(sElementId, sVariableName) {

	var sHideElement = eval(sVariableName);
	if (sHideElement == sElementId) {
		fShowHideElement(sElementId);
		sVariableName += ' = \'none\';';
		eval(sVariableName);
	}else {
		if (sHideElement != 'none') {
			fShowHideElement(sHideElement);
		}
		fShowHideElement(sElementId);
		sVariableName += ' = \'' + sElementId + '\';';
		eval(sVariableName);
	}
}
addLoadEvent(prepareForms);