/*
 * Copyright (C) 2008 Us Media Holding B.V. All rights reserved.
 *
 * This software is the proprietary information of Us Media Holding B.V.
 * Use is subject to license terms.
 */
function navHover(){
    $(this).fadeTo(150, 1);
};
function navBlur(){
    $(this).fadeTo(300, 0);
};

$(document).ready(function(){
    if ($.browser.msie &&
        $.browser.version.substring(0, 1) == '6') {
    
        $('#navigation li.active > a').css('background-position', '0 -32px');
        $('#navigation li li.active > a').css('background-position', '0 -29px');
    }
    if ($.browser.mozilla &&
        $.browser.version.substring(0, 3) == '1.8' &&
        navigator.appVersion.indexOf("Mac") > -1) {
    
        // future hackery
    }
    else {
        $('#navigation ul > li').not('.active').children('a').css('opacity', '0').hoverIntent(navHover, navBlur);
    }
    
    $('input[type=checkbox]').next('label').css('font-size', '11px');
    $('#overview_list .column + .column, #overview_list .column + .column + .column').equalizeCols();
    $('#overview_list .column + .column').css('background', '#141414');
    
    $("a#forgot_password").click(function(){
        $("div#password_form").show();
        return false;
    });
    
    $('#mycarousel').jcarousel();
    
    $("div#top_banner>a>img").fadeTo(700, 0.5).fadeTo(700, 1);
    
    $('.page_bar ul > li:even span').replaceWith('<span>&laquo;</span>');
    $('.page_bar ul > li:odd span').replaceWith('<span>&raquo;</span>');
    if ($.browser.msie) {
        $('.page_bar ul > li:odd').attr('id', 'page_right');
        // Terrible tricks for a terrible browser
    };
    
    $('.more a, .special_link a').hover(function(){
            $(this).parent().css('background-position', '0 -43px')
        }, function(){
            $(this).parent().css('background-position', '0 2px')
        } // I'm not sure if this is awesome or terrible
    )
    
    
    $('button').hover(function(){
        $(this).css('cursor', 'pointer')
    }, function(){
        $(this).css('cursor', 'default');
    });
    
    $('table.hotels tbody tr:even td').css('background-color', '#1a1a1a');
    $('table.hotels tbody tr:even th').css('background-color', '#1a1a1a');
    
    /*
     * links that need to be opened in new browser windows
     * note: the #container specifity is a workaround
     */
    $('#sponsors_box a').attr('target', '_blank');
    $('#container a[href$=.pdf]').attr('target', '_blank');
    $('#container a[@href^="http://"]').attr('target', '_blank');
    $('div.sponsors ul li a').attr('target', '_blank');
    
    /* Accordion starts */
    $('ul#side_navigation ul').hide();
    $('ul#side_navigation li.expanded ul').show();
    $('li.active ul').show();

    $('ul#side_navigation li a').click(function(){

        var checkElement = $(this).next();
        var parent = this.parentNode.parentNode.id;

        if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            if ($('#' + parent).hasClass('collapsible')) {
                $('#' + parent + ' ul:visible').slideUp('normal');
            }
            return false;
        }
        if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
         
            if( $('li.active').next().not('ul') ) {
                $('li.active').parents('li.expanded').addClass('keepme');             
            }

            $('#' + parent + ' li.expanded > ul:visible').not( $('li.active ul') ).not($('li.keepme ul')).slideUp('normal');
            $('ul#side_navigation li.expanded').not( $('li.active') ).not($('li.keepme')).removeClass('expanded');    
       
            checkElement.slideDown('normal');
            $(this).parent('li').addClass('expanded');
    
            return false;
        } 
    }); 
    
    if ($.browser.msie) {
        $('.columns_3_1 .column').each(function(index){
            $(this).addClass('column_' + (index + 1));
        });
        $('.columns_1_2_1 .column').each(function(index){
            $(this).addClass('column_' + (index + 1));
        });
        $('.columns_1_3 .column').each(function(index){
            $(this).addClass('column_' + (index + 1));
        });
    };
});
