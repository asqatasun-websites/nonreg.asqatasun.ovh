/*
* A-175 : Suppression de messages d'alert de debug
*/

var url_auto = "tyreSelector.ajax"
var url_moto = "data_2r/Controller?controller=TsAjaxController";
var TYRE_TYPE = "tyreType";           
var VALIDE_MAKE = "validerMarque";
var MAKE = "make";


var marqueValue;
/*
* Sur chargement du formulaire on evalue les combo qui doivent disabled
*/

function onLoad()
{	
 	evaluateCombos([TYRE_TYPE,MAKE]);
    evaluateValider();
}

function evaluateValider() {
    function selectionDone(param) {
    
   
        var target = $(param);
        var result = false;
    	if (target != null && target.options.selectedIndex != 0) {
    		result = true;
    	}
        return result
    };

    if (selectionDone(MAKE)
            && selectionDone(TYRE_TYPE)) 
    	{
        $(VALIDE_MAKE).disabled = false;
    } else {
    	$(VALIDE_MAKE).disabled = true;
    }
}


function evaluateCombos(params){
    for (var i = 0; i < params.length; i++) {
        evaluateCombo(params[i]);
    }
}

function evaluateCombo(comboId){
    var target = $(comboId);
    if (target != null && target.options.length > 1) {        
        target.disabled = false;
        target.style.backgroundColor='#C5C9D6';
    }
}

// TS par recherche de véhicule

//Evenement sur changement de la make
function choice_ts(){
   // deSelect([TYRE_TYPE,MAKE])
   
   
   
    marqueValue = $F(TYRE_TYPE);
    
    if ("" != marqueValue) {
	    if ("auto" == marqueValue) {	    	 
	    	 getModelesAuto();	      
	    }
	    if ("moto" == marqueValue) {
	    	//getModelesMoto();
	    	target = $(id);   
    		// vidage de la combobox
    		target.options.length = 0;
    		target.disable();	     
	    }
    	
    }
}
function getModelesAuto() {
    var pars = "method=getMarques";    
    var myAjax = new Ajax.Request(
            url_auto,
    {
        method: 'get',
        parameters: pars,
        onComplete: loadFromXml
    });
}

function getModelesMoto() {
    var pars = "&action=findMarque";    
    var myAjax = new Ajax.Request(
            url_moto,
    {    	
        method: 'get',
        parameters: pars,
        onComplete: loadFromXml
    });
    
}
function deSelect(params)
{
    for (var i = 0; i < params.length; i++)
    {
        deselectAndPauseCombo(params[i]);
    }
}
/*
* Fonction qui déselectionne la combo et la met en attente
*/
function deselectAndPauseCombo(comboId)
{
    var target = $(comboId);
    // vidage de la combobox
    target.style.backgroundColor='#EDEEF2';
    target.options.length = 1;
    target.disabled = true;
}




function loadFromXml(originalRequest)
{
	
	//put returned XML in the textarea
    result = originalRequest.responseText;
    //parsing XML
    try {
        // xmlDoc.LoadXML result;   // parse JS
        
        var xmlError
        var xmlDoc = new XMLDoc(result, xmlError);       
        var action = xmlDoc.docNode;       
    }
    catch (e) {
        alert('exception sur parseXML ' + result + ' ' + e);
        return;
    } 
     if ("auto" == marqueValue) {
	    	 populateComboBoxTargetAuto(action);	      
	    }
	if ("moto" == marqueValue) {
	    	populateComboBoxTargetMoto(action);	     
	    }  
}
function populateComboBoxTargetMoto(node)
{
	
	id = node.getAttribute("type");
    target = $(id);   
    // vidage de la combobox
    target.options.length = 0;
	
    // remplissage dynamique de la combobox
    var values = node.getElements("value");   
    for (j = 0; j < values.length; j++) {    	
        var value = values[j];
        //alert('value '+ value.getAttribute("id") + '\n unescapedvalue '+ unescape(value.getAttribute("name")));
        var val = urldecode(value.getAttribute("id"));
        var tex = urldecode(value.getAttribute("name"));
        target.options[j] = new Option(tex, val);
    }    
    //    target.disabled = false;
    evaluateCombos([MAKE]);  
   // evaluateValider();
}



function populateComboBoxTargetAuto(node)
{
    id = node.getAttribute("type");
    target = $(id);    
    // vidage de la combobox
    target.options.length = 0;

    // remplissage dynamique de la combobox
    var values = node.getElements("value");
    for (j = 0; j < values.length; j++) {
    
        var value = values[j];
        //alert('value '+ value.getText() + '\n escapedvalue '+ escape(value.getText()) + '\n unescapedvalue '+ unescape(value.getText()));
        var val = urldecode(value.getText());
        var tex = urldecode(value.getText());

        target.options[j] = new Option(tex, val);
    }
    //    target.disabled = false;
    evaluateCombos([MAKE]);
   // evaluateValider();
}

function urldecode(ch) {
    ch = ch.replace(/[+]/g, " ")
    return unescape(ch)
}
