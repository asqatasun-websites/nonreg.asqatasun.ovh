
$(document).ready(function(){
	
	//***********************************************
	//
	// init COLL_EXP
	//
	$(".coll_exp").children("li").addClass("exp");
	$(".coll_exp li").children("ul").hide();
	//
	$(".coll_exp span").click( function() {
		
		var me = $(this);
		//
		$(".coll_exp span").each( function () {
			if($(this) != me){
				$(this).parent("li").removeClass("coll");
				$(this).parent("li").addClass("exp");
				$(this).parent("li").children("ul").hide();
			}
		});
		//
		me.parent("li").removeClass("exp");
		me.parent("li").addClass("coll");
		me.parent("li").children("ul").slideDown("fast");
	} );
	//***********************************************
	
	
	//***********************************************
	//
	// init OPEN_VIDEO
	//
	$(".video_blk").hide();
	//
	$(".open_video_blk").click( function() {
		var blk_id = this.getAttribute('rel');
		var target_blk = $("#"+blk_id);
		target_blk.slideDown("fast");
		
	} );
	//***********************************************
	
	
	//***********************************************
	//
	// init INSTANTS
	//
	// ajoute #main autour de .main_instants
	$(".main_instants").wrap("<div id=\"main\"></div>");
	// copie les blocks instants dans la block .main_instants
	$(".instant").appendTo( ".main_instants" );
	// supprime les blocks inutiles
	$(".instants_no_javascript").remove();
	$(".no_javascript").remove();
	// cache tous les block instants
	hideallinstant();
	// si un instant doit �tre selectionn� : 
	if(getInstantFromUrl()){
		instant(getInstantFromUrl());
	}
	//***********************************************
	
	//***********************************************
	//
	// init MQC
	//
	// cherche un selected
	setMqcSelected($("#mqc .nav li.selected"));
	// pour les H3 et H4 qui n'ont pas de lien
	$("#mqc .nav :header").each( function() {
		var sub = $(this).children("a");
		if(sub.length == 0) {
			$(this).css({ cursor:"pointer" });
			$(this).click(function(){
				if (! ($(this).parent().hasClass("selected")) ) setMqcSelected($(this)); 
			});
		}
	});
	// les liens des li qu'in n'ont qu'un lien enfant
	$("#mqc .nav li").each ( function(){
		var sub = $(this).children("a");
		if(sub.length == 1) {
			sub.click(function(){
				if (! ($(this).parent().hasClass("selected")) ) setMqcSelected($(this)); 
			});
		}
	});
	// les liens des header qu'in n'ont qu'un lien enfant
	$("#mqc .nav :header a").click(function(){
		if (! ($(this).parent().hasClass("selected")) ) setMqcSelected($(this)); 
	});
	//***********************************************
});



//***********************************************
//
// function INSTANTS
//
// affiche un instant
function instant(a){
	hideallinstant();
	$("#instant_"+a).show();
}
// cache les instants
function hideallinstant(){
	$("#main .instant").hide();
}
//extraire des variables d'uns URL
function getInstantFromUrl () {
	//recupere l'URL
	var url = String(window.location);
	
	if (url.indexOf("#instant") != -1) {
		// renvois la valeur de l'instant
		var instant = url.substring(url.indexOf("#instant")+8);
		return instant;
	}else{
		// renvois rien
		return 1;
	}
} 

//***********************************************
//
// function MQC
//
// affiche un instant
function setMqcSelected(item){ // item == h3, h4, a
	// supprime les class .selected
	//$("#mqc .nav .selected").toggleClass("selected");
	// le li
	$(item).parents("li").addClass("selected");
	// cache et affiche
	$("#mqc .nav li").not(".selected").children("ul").hide();
	$("#mqc .nav li.selected").children("ul").show("fast");
}