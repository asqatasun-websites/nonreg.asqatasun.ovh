window.getOpener = function()
{
	return this;
};

function creerTableauAffiches()
{
	var arguments = new Array();
	arguments["theme"] = document.getElementsByName('theme')[0].value;
	arguments["serie"] = document.getElementsByName('serie')[0].value;
	arguments["titre"] = document.getElementsByName('titre')[0].value;
	arguments["lieu"] = document.getElementsByName('lieu')[0].value;
	arguments["region"] = document.getElementsByName('region')[0].value;
	arguments["dateDeb"] = document.getElementsByName('dateDeb')[0].value;
	arguments["dateFin"] = document.getElementsByName('dateFin')[0].value;

	return arguments;
}

function creerTableauResoc()
{
	var arguments = new Array();
	arguments["type"] = document.getElementsByName('type')[0].value;
	arguments["departement"] = document.getElementsByName('departement')[0].value;

	return arguments;
}

function creerTableauAuteur()
{
	var arguments = new Array();
	arguments["nom"] = document.getElementsByName('nom')[0].value;

	return arguments;
}

function creerTableauCartePostale(categorie)
{
	var arguments = new Array();
	if (categorie == 1)
		arguments["motClef"] = document.getElementsByName('motClef')[0].value;
	else if (categorie == 2)
	{
		arguments["categorie"] = document.getElementsByName('categorie')[0].value;
		arguments["sousCategorie"] = document.getElementsByName('sousCategorie')[0].value;
	}
	else if (categorie == 3)
	{
		arguments["departement"] = document.getElementsByName('departement')[0].value;
		arguments["ville"] = document.getElementsByName('ville2')[0].value;
		arguments["date"] = document.getElementsByName('dateCP')[0].value;
	}

	return arguments;
}

function changerLangue(langue)
{
	window.location = "./index.php?langue="+langue;
}

function montreRechercheAvancee()
{
	document.getElementById("recherche").style.display = 'block';
	document.getElementById("activerRecherche").style.display = 'none';
	document.getElementById("titreRecherche").innerHTML = 'Recherche avanc�e';
}

function cacherRechercheAvancee()
{
	document.getElementById("recherche").style.display = 'none';
	document.getElementById("activerRecherche").style.display = 'block';
	document.getElementById("titreRecherche").innerHTML = 'Recherche simple';
}

function afficherImage(lien)
{
	img = new Image();
	img.src = lien;
	waitForLoading(lien);
//	window.open(lien, lien, 'toolbar=0, location=0, directories=0, status=0, scrollbars=0, resizable=0, copyhistory=0, menuBar=0, '+img.width+', '+img.height+', left=800, top=500');
}

function waitForLoading(nom)
{
	if (!img.complete)
	{ 
		setTimeout(waitForLoading, 30);
		return; 
	}
	var w = window.open('about:blank', nom, 'width='+img.width+',height='+img.height);
	w.document.write('<html><body style = "margin: 0px;"><img src = "'+img.src+'" alt="" /></body></html>');
}

function html_entity_decode(texte)
{
	texte = texte.replace(/&quot;/g,'"'); // 34 22
	texte = texte.replace(/&amp;/g,'&'); // 38 26	
	texte = texte.replace(/&#39;/g,"'"); // 39 27
	texte = texte.replace(/&lt;/g,'<'); // 60 3C
	texte = texte.replace(/&gt;/g,'>'); // 62 3E
	texte = texte.replace(/&circ;/g,'^'); // 94 5E
	texte = texte.replace(/&lsquo;/g,'�'); // 145 91
	texte = texte.replace(/&rsquo;/g,'�'); // 146 92
	texte = texte.replace(/&ldquo;/g,'�'); // 147 93
	texte = texte.replace(/&rdquo;/g,'�'); // 148 94
	texte = texte.replace(/&bull;/g,'�'); // 149 95
	texte = texte.replace(/&ndash;/g,'�'); // 150 96
	texte = texte.replace(/&mdash;/g,'�'); // 151 97
	texte = texte.replace(/&tilde;/g,'�'); // 152 98
	texte = texte.replace(/&trade;/g,'�'); // 153 99
	texte = texte.replace(/&scaron;/g,'�'); // 154 9A
	texte = texte.replace(/&rsaquo;/g,'�'); // 155 9B
	texte = texte.replace(/&oelig;/g,'�'); // 156 9C
	texte = texte.replace(/&#357;/g,'�'); // 157 9D
	texte = texte.replace(/&#382;/g,'�'); // 158 9E
	texte = texte.replace(/&Yuml;/g,'�'); // 159 9F
	texte = texte.replace(/&nbsp;/g,' '); // 160 A0
	texte = texte.replace(/&iexcl;/g,'�'); // 161 A1
	texte = texte.replace(/&cent;/g,'�'); // 162 A2
	texte = texte.replace(/&pound;/g,'�'); // 163 A3
	texte = texte.replace(/&curren;/g,' '); // 164 A4
	texte = texte.replace(/&yen;/g,'�'); // 165 A5
	texte = texte.replace(/&brvbar;/g,'�'); // 166 A6
	texte = texte.replace(/&sect;/g,'�'); // 167 A7
	texte = texte.replace(/&uml;/g,'�'); // 168 A8
	texte = texte.replace(/&copy;/g,'�'); // 169 A9
	texte = texte.replace(/&ordf;/g,'�'); // 170 AA
	texte = texte.replace(/&laquo;/g,'�'); // 171 AB
	texte = texte.replace(/&not;/g,'�'); // 172 AC
	texte = texte.replace(/&shy;/g,'�'); // 173 AD
	texte = texte.replace(/&reg;/g,'�'); // 174 AE
	texte = texte.replace(/&macr;/g,'�'); // 175 AF
	texte = texte.replace(/&deg;/g,'�'); // 176 B0
	texte = texte.replace(/&plusmn;/g,'�'); // 177 B1
	texte = texte.replace(/&sup2;/g,'�'); // 178 B2
	texte = texte.replace(/&sup3;/g,'�'); // 179 B3
	texte = texte.replace(/&acute;/g,'�'); // 180 B4
	texte = texte.replace(/&micro;/g,'�'); // 181 B5
	texte = texte.replace(/&para/g,'�'); // 182 B6
	texte = texte.replace(/&middot;/g,'�'); // 183 B7
	texte = texte.replace(/&cedil;/g,'�'); // 184 B8
	texte = texte.replace(/&sup1;/g,'�'); // 185 B9
	texte = texte.replace(/&ordm;/g,'�'); // 186 BA
	texte = texte.replace(/&raquo;/g,'�'); // 187 BB
	texte = texte.replace(/&frac14;/g,'�'); // 188 BC
	texte = texte.replace(/&frac12;/g,'�'); // 189 BD
	texte = texte.replace(/&frac34;/g,'�'); // 190 BE
	texte = texte.replace(/&iquest;/g,'�'); // 191 BF
	texte = texte.replace(/&Agrave;/g,'�'); // 192 C0
	texte = texte.replace(/&Aacute;/g,'�'); // 193 C1
	texte = texte.replace(/&Acirc;/g,'�'); // 194 C2
	texte = texte.replace(/&Atilde;/g,'�'); // 195 C3
	texte = texte.replace(/&Auml;/g,'�'); // 196 C4
	texte = texte.replace(/&Aring;/g,'�'); // 197 C5
	texte = texte.replace(/&AElig;/g,'�'); // 198 C6
	texte = texte.replace(/&Ccedil;/g,'�'); // 199 C7
	texte = texte.replace(/&Egrave;/g,'�'); // 200 C8
	texte = texte.replace(/&Eacute;/g,'�'); // 201 C9
	texte = texte.replace(/&Ecirc;/g,'�'); // 202 CA
	texte = texte.replace(/&Euml;/g,'�'); // 203 CB
	texte = texte.replace(/&Igrave;/g,'�'); // 204 CC
	texte = texte.replace(/&Iacute;/g,'�'); // 205 CD
	texte = texte.replace(/&Icirc;/g,'�'); // 206 CE
	texte = texte.replace(/&Iuml;/g,'�'); // 207 CF
	texte = texte.replace(/&ETH;/g,'�'); // 208 D0
	texte = texte.replace(/&Ntilde;/g,'�'); // 209 D1
	texte = texte.replace(/&Ograve;/g,'�'); // 210 D2
	texte = texte.replace(/&Oacute;/g,'�'); // 211 D3
	texte = texte.replace(/&Ocirc;/g,'�'); // 212 D4
	texte = texte.replace(/&Otilde;/g,'�'); // 213 D5
	texte = texte.replace(/&Ouml;/g,'�'); // 214 D6
	texte = texte.replace(/&times;/g,'�'); // 215 D7
	texte = texte.replace(/&Oslash;/g,'�'); // 216 D8
	texte = texte.replace(/&Ugrave;/g,'�'); // 217 D9
	texte = texte.replace(/&Uacute;/g,'�'); // 218 DA
	texte = texte.replace(/&Ucirc;/g,'�'); // 219 DB
	texte = texte.replace(/&Uuml;/g,'�'); // 220 DC
	texte = texte.replace(/&Yacute;/g,'�'); // 221 DD
	texte = texte.replace(/&THORN;/g,'�'); // 222 DE
	texte = texte.replace(/&szlig;/g,'�'); // 223 DF
	texte = texte.replace(/&agrave;/g,'�'); // 224 E0
	texte = texte.replace(/&aacute;/g,'�'); // 225 E1
	texte = texte.replace(/&acirc;/g,'�'); // 226 E2
	texte = texte.replace(/&atilde;/g,'�'); // 227 E3
	texte = texte.replace(/&auml;/g,'�'); // 228 E4
	texte = texte.replace(/&aring;/g,'�'); // 229 E5
	texte = texte.replace(/&aelig;/g,'�'); // 230 E6
	texte = texte.replace(/&ccedil;/g,'�'); // 231 E7
	texte = texte.replace(/&egrave;/g,'�'); // 232 E8
	texte = texte.replace(/&eacute;/g,'�'); // 233 E9
	texte = texte.replace(/&ecirc;/g,'�'); // 234 EA
	texte = texte.replace(/&euml;/g,'�'); // 235 EB
	texte = texte.replace(/&igrave;/g,'�'); // 236 EC
	texte = texte.replace(/&iacute;/g,'�'); // 237 ED
	texte = texte.replace(/&icirc;/g,'�'); // 238 EE
	texte = texte.replace(/&iuml;/g,'�'); // 239 EF
	texte = texte.replace(/&eth;/g,'�'); // 240 F0
	texte = texte.replace(/&ntilde;/g,'�'); // 241 F1
	texte = texte.replace(/&ograve;/g,'�'); // 242 F2
	texte = texte.replace(/&oacute;/g,'�'); // 243 F3
	texte = texte.replace(/&ocirc;/g,'�'); // 244 F4
	texte = texte.replace(/&otilde;/g,'�'); // 245 F5
	texte = texte.replace(/&ouml;/g,'�'); // 246 F6
	texte = texte.replace(/&divide;/g,'�'); // 247 F7
	texte = texte.replace(/&oslash;/g,'�'); // 248 F8
	texte = texte.replace(/&ugrave;/g,'�'); // 249 F9
	texte = texte.replace(/&uacute;/g,'�'); // 250 FA
	texte = texte.replace(/&ucirc;/g,'�'); // 251 FB
	texte = texte.replace(/&uuml;/g,'�'); // 252 FC
	texte = texte.replace(/&yacute;/g,'�'); // 253 FD
	texte = texte.replace(/&thorn;/g,'�'); // 254 FE
	texte = texte.replace(/&yuml;/g,'�'); // 255 FF
	return texte;
}