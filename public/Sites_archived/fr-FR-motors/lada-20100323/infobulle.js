// code permettant l'affichage d'une infobulle
// ajout� le 04 avril 2006 par Pierrick BOYER

var decal_x = 5;
var decal_y = 10;
document.onmousemove = suivre_souris0;

function suivre_souris0(e)
{
	if (navigator.appName=="Microsoft Internet Explorer")
	{
		var x = event.x + document.body.scrollLeft;	var y = event.y + document.body.scrollTop;
	}
	else
	{
		var x =  e.pageX;var y =  e.pageY;
	}
	if (document.getElementById("bulle"))
	{
		document.getElementById("bulle").style.left = Math.min(x + decal_x, document.body.scrollLeft + document.body.offsetWidth - document.getElementById("bulle").offsetWidth - 20);
		document.getElementById("bulle").style.top  = (y + decal_y + document.getElementById("bulle").offsetHeight + 20 < document.body.scrollTop + document.body.offsetHeight)  ? 
			y + decal_y : y - document.getElementById("bulle").offsetHeight - decal_y;
	}
}


function info_bulle(action, contenu, couleur_bordure, couleur_fond, couleur_texte)
{
	if (!couleur_bordure) couleur_bordure = '#999999';
	if (!couleur_fond) couleur_fond = '#f9f9ff';
	if (!couleur_texte) couleur_texte = '#666666';
	if (action == 'afficher')
	{
		v_info_bulle  = "<table ";
		v_info_bulle += "' style='border:1px solid " + couleur_bordure + ";background-color:" +  couleur_fond + ";'cellpadding='2' cellspacing='1'>";
		v_info_bulle += "<tr><td nowrap><font style='font-family : verdana, arial; font-size: 11px '><font color='" + couleur_texte + "'>";
		v_info_bulle += ajouter_br(contenu);
		v_info_bulle += "</font></font></td></tr></table>";
		document.getElementById("bulle").innerHTML = v_info_bulle;
	}
	else
	{
		document.getElementById("bulle").innerHTML = '';
	}
	
}


function ajouter_br(montexte)
{
	var texte2 = "";

	montexte = montexte.split(/\%0D\%0A/);
	
	if (montexte.length > 1)
	{
		for (i=0; i < montexte.length ; i++)
		{
			if (i == 0)
			{
				texte2 = texte2 + unescape(montexte[i]);
			}
			else
			{
				texte2 = texte2 + '<br/>'+ unescape(montexte[i]);
			}		
		}		 
	}
	else
	{
		texte2 = unescape(montexte);
	}
	
	
	return(texte2);
}

		
