var xhr = null;

function getXhr(){
	if(window.XMLHttpRequest) // Firefox et autres
	   xhr = new XMLHttpRequest();
	else if(window.ActiveXObject){ // Internet Explorer
	   try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
	}
	else { // XMLHttpRequest non support� par le navigateur
	   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
	   xhr = false;
	}
}

function ajax(element,fichier,chaine){
	getXhr();
	xhr.onreadystatechange = function(){
		// On ne fait quelque chose que si on a tout re�u et que le serveur est ok
		if(xhr.readyState == 4 && xhr.status == 200){
			leselect = xhr.responseText;
			if(element != ''){
				document.getElementById(element).innerHTML = leselect;
			}
		}
	}

	xhr.open("POST",fichier,true);
	xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=iso-8859-1');
	xhr.send(chaine);
	return true;
}