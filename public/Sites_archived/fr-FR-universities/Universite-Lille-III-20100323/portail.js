function next(elem) {
    do {
        elem = elem.nextSibling;
    } while (elem && elem.nodeType != Node.ELEMENT_NODE);
    return elem;                
}

function accordion_close(){
    var anchors = document.getElementsByTagName("a") ;
    for(var i = 0; i < anchors.length; i++){
        var anchor = anchors[i] ;
        anchor.className = anchor.className.replace(new RegExp("accordion_selected\\b"), "") ;
        if(/accordion/.test(anchor.className)){
            next(anchor).style.display = 'none';
        }
    }
}

function accordion() {
    if(!document.getElementsByTagName) return ;
    var anchors = document.getElementsByTagName("a") ;
    for(var i = 0; i < anchors.length; i++){
        var anchor = anchors[i] ;
        if(/accordion/.test(anchor.className)){
            next(anchor).style.display = 'none' ;
            anchor.onclick = function(){
                accordion_close() ;
                this.className += ' accordion_selected' ;
                next(this).style.display = '' ;
                return false ;
            }
        }
    }
}

function externalLinks() {
    if(!document.getElementsByTagName) return ;
    var anchors = document.getElementsByTagName("a") ;
    for(var i = 0; i < anchors.length; i++){
        var anchor = anchors[i] ;
        if(anchor.getAttribute("href") && anchor.hasAttribute("rel")){
            var rel = anchor.getAttribute("rel").split(" ") ;
            for(var j = 0; j < rel.length; j++){
                if(rel[j] == 'external'){
                    anchor.target = "_blank" ;
                }
            }
        }
    }
}

function findSite(){
    //TODO : ENtree, ca marche, pas d'appel � cette fonction mais clic sur le bouton => marche pas href=""
    var find = document.getElementById('fmFind') ;
    if(find != null){
        return find.submit() ;
    }
}

function findDir(){
    var dir = document.getElementById('fmDir') ;
    if(dir != null){
        return dir.submit();
    }
}

function flipActu(id){
  var actuid = document.getElementById("actu" + id) ;
  if(actuid != null){
      if(actuid.style.display == "none"){
        actuid.style.display = "block" ;
    	document.getElementById("imgactudown" + id).style.display = "none" ;
    	document.getElementById("imgactuup" + id).style.display = "inline" ;
      }else{
        actuid.style.display = "none" ;
    	document.getElementById("imgactudown" + id).style.display = "inline" ;
    	document.getElementById("imgactuup" + id).style.display = "none" ;
      }  
  }
}

function sfHoverIE(){
    var elements = document.getElementById('menu').getElementsByTagName('li') ;
    for (var i = 0; i < elements.length; i++)  {

        elements[i].onmouseover = function(){
            this.className += ' sfhover' ;

        }

        elements[i].onmouseout = function(){
            this.className = this.className.replace(new RegExp("sfhover\\b"), "") ;
        }
    }
}

if(document.all){
    // Pour IE :@
    window.attachEvent('onload', externalLinks) ;
    window.attachEvent('onload', sfHoverIE) ;
    window.attachEvent('onload', accordion) ;
}else{
    // Pour les vrais browsers
    window.addEventListener('load', externalLinks, false);
    window.addEventListener('load', accordion, false);
}



