/**
	provides helper functions for better usage of cookies with JavaScript
	(see http://www.elated.com/articles/javascript-and-cookies/)

*/

/**
	sets a cookie to a value
	name: the name of the cookie
	value: the value to store within the cookie
	validity: (optional) the duration of validity for this cookie (in ms)
	path: (optional) the path for which the cookie is valid
	domain: (optional) the domain for which the cookie is valid
	
*/
function /*void*/ set_cookie(/*String*/name, /*String*/value, /*int*/validity, /*String*/path, /*String*/domain, /*boolean*/secure )
{
	var cookie_string = name + "=" + escape (value);
	if (validity) {
		var curDate = new Date();
		var expiry = new Date(curDate.getTime() + validity);
		cookie_string += "; expires=" + expiry.toGMTString();
	}
	if (path) {
		cookie_string += "; path=" + escape (path);
	}
	if (domain) {
		cookie_string += "; domain=" + escape (domain);
	}
	if (secure) {
		cookie_string += "; secure";
	}
	document.cookie = cookie_string;
}

/**
	deletes a cookie
	name: the name of the cookie to delete
*/
function /*void*/ delete_cookie(/*String*/name)
{
	var cookie_date = new Date ( );  // current date & time
	cookie_date.setTime ( cookie_date.getTime() - 1 );
	document.cookie = name += "=; expires=" + cookie_date.toGMTString();
}

/**
	gets a cookie value
	name: the name of the cookie whose value should be retrieved
*/
function /*String*/ get_cookie(/*String*/name)
{
	var results = document.cookie.match(name + '=(.*?)(;|$)');
	if (results)
	  return (unescape(results[1]));
	else
	  return null;
}

/**
 * 
 * @return all valid cookie names as array
 */
function get_cookie_list() {
	// list all matches (
	var list = new Array();
	results = document.cookie.split(";");
	for (var result in results) {
		var xxx = String(results[result]) + ";";
		var groups = xxx.match(/([^\s]*?)=(.*?)(;|$)/);
		// only the first group contains the cookie name
		if(groups != null){
			list.push(groups[1]);
		}
	}
	return list;
}

/**
 * Checks if the user has cookies enabled. If cookies are enabled the form
 * from which this method is called will be submitted. Otherwise the user will be 
 * forwarded to the given landing page with a login layer and a reminder to enable 
 * cookies.
 *
 */
function checkCookieAvailability(funcCheckOk, funcCheckFailed){
	set_cookie('dccookieenabled', 'enabled', '', '/', '', '' );
	if (get_cookie('dccookieenabled')){
		funcCheckOk();
	}
	else{
		funcCheckFailed();
	}
	delete_cookie('dccookieenabled');
}

/**
 * adds a Loginlayer type "ll3 - no cookies activated"
 * to the given link
 */
function getLinkWithLoginLayer(){
	var pos = snCurHref.indexOf('/content/');
	var part = snCurHref.substring(pos);
	var pos2 = part.indexOf('.htm');
	var final = part.substring(0, pos2) + '.ll3' + part.substring(pos2);
	window.location.href = final;
}