// Copyright (c) 1996-1997 Athenia Associates.
// http://www.webreference.com/js/
// License is granted if and only if this entire
// copyright notice is included. By Tomer Shiran.

function setCookie (name, value, expires, path, domain, secure) {
    var curCookie = name + "=" + escape(value) + (expires ? "; expires=" + expires : "") +
        (path ? "; path=" + path : "") + (domain ? "; domain=" + domain : "") + (secure ? "secure" : "");
    document.cookie = curCookie;
}

function getCookie (name) {
    var prefix = name + '=';
    var c = document.cookie;
    var nullstring = '';
    var cookieStartIndex = c.indexOf(prefix);
    if (cookieStartIndex == -1)
        return nullstring;
    var cookieEndIndex = c.indexOf(";", cookieStartIndex + prefix.length);
    if (cookieEndIndex == -1)
        cookieEndIndex = c.length;
    return unescape(c.substring(cookieStartIndex + prefix.length, cookieEndIndex));
}

function deleteCookie (name, path, domain) {
    if (getCookie(name))
        document.cookie = name + "=" + ((path) ? "; path=" + path : "") +
            ((domain) ? "; domain=" + domain : "") + "; expires=Thu, 01-Jan-70 00:00:01 GMT";
}

function fixDate (date) {
    var base = new Date(0);
    var skew = base.getTime();
    if (skew > 0)
        date.setTime(date.getTime() - skew);
}

function rememberMe (f) {
    var now = new Date();
    fixDate(now);
    now.setTime(now.getTime() + 365 * 24 * 60 * 60 * 1000);
    now = now.toGMTString();
    if (f.author != undefined)
       setCookie('mtcmtauth', f.author.value, now, '/', '', '');
    if (f.email != undefined)
       setCookie('mtcmtmail', f.email.value, now, '/', '', '');
    if (f.url != undefined)
       setCookie('mtcmthome', f.url.value, now, '/', '', '');
}

function forgetMe (f) {
    deleteCookie('mtcmtmail', '/', '');
    deleteCookie('mtcmthome', '/', '');
    deleteCookie('mtcmtauth', '/', '');
    f.email.value = '';
    f.author.value = '';
    f.url.value = '';
}

function hideDocumentElement(id) {
    var el = document.getElementById(id);
    if (el) el.style.display = 'none';
}

function showDocumentElement(id) {
    var el = document.getElementById(id);
    if (el) el.style.display = 'block';
}

var commenter_name;

function individualArchivesOnLoad(commenter_name) {




    var mtcmtauth;
    var mtcmthome;
    if (document.comments_form) {
        if (!commenter_name && (document.comments_form.email != undefined) &&
            (mtcmtmail = getCookie("mtcmtmail")))
            document.comments_form.email.value = mtcmtmail;
        if (!commenter_name && (document.comments_form.author != undefined) &&
            (mtcmtauth = getCookie("mtcmtauth")))
            document.comments_form.author.value = mtcmtauth;
        if (document.comments_form.url != undefined && 
            (mtcmthome = getCookie("mtcmthome")))
            document.comments_form.url.value = mtcmthome;
        if (document.comments_form["bakecookie"]) {
            if (mtcmtauth || mtcmthome) {
                document.comments_form.bakecookie.checked = true;
            } else {
                document.comments_form.bakecookie.checked = false;
            }
        }
    }
}

function writeTypeKeyGreeting(commenter_name, entry_id) {

}



/* COMMUNITY PACK FUNCTIONS */

req = null;
function getCommenterName() {
	cValue = "";
	dc = document.cookie;
	cookies = dc.split(";");
	for (x=0;x<cookies.length;x++) {
		if (cookies[x].match(/commenter_name=/)) {
		cValue = cookies[x].replace(/commenter_name=/, "");
		}
	}
	if (cValue.length>1) {
		return unescape(cValue);
	} else {
		return "";
	}
}

function trimString (str) {
  str = this != window? this : str;
  return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
}

function urlParameter(name)
{
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var tmpURL = window.location.href;
  var results = regex.exec( tmpURL );
  if( results == null )
    return "";
  else
    return results[1];
}

loginWhere = "";

function doLogin(where) {
	var agt=navigator.userAgent.toLowerCase();
	loginWhere=where;
	document.getElementById("loginform_" + where).style.display = "block";
	if (where == "top") { document.getElementById("account-info").style.display = "none"; }
	return "";
}

function ajax(username, password) {
	username = escape(username);
	password = escape(password);
	req = false;
	if(window.XMLHttpRequest) {
		try {
				req = new XMLHttpRequest();
		} catch(e) {
				req = false;
		}
	} else if(window.ActiveXObject) {
		try {
			req = new ActiveXObject("Msxml2.XMLHTTP");
		} catch(e) {
			try {
				req = new ActiveXObject("Microsoft.XMLHTTP");
			} catch(e) {
				req = false;
			}
		}
	}
	if(req) {
		req.onreadystatechange = login_response;
		req.open("POST", "/cgi-bin/mte_10/plugins/Profile/profile.cgi", true);
		req.send("__mode=login&ajax=true&profile_username=" + username + "&profile_password=" + password);
	} else {
		// something didn't work
	}
}

function login_response() {
	if (req.readyState == 4) {
		if (req.responseText.match(/Success/)) {
			uname = req.responseText.substring(8);
			if (document.getElementById("loginform_bottom")) {
				document.getElementById("loginform_bottom").style.display = "none";
				document.getElementById("post-comment").style.display = "block";
			}
			if (document.getElementById("display_name")) {
				document.getElementById("display_name").value = uname;
			}
			if (document.getElementById('talkform')) {
				document.getElementById('talkform').onsubmit = null;
				document.getElementById('talkform').submit();
			}
			document.getElementById("loginform_top").style.display = "none";
			document.getElementById("account-info").style.display = "block";
			document.getElementById("membername").innerHTML = "Hello, <a href=\"/user/profile/" + trimString(uname) + "\">" + uname + "!</a>";
			document.getElementById("account-info").innerHTML = "You are logged in. <a href=\"/user/logout\">Log Out</a> | View/edit <a href=\"/user/profile.html\">My Account</a>";

		} else {
			document.getElementById("loginMsg_" + loginWhere).innerHTML = req.responseText;
			document.getElementById("loginMsg_" + loginWhere).style.background = "#ffffff";
			document.getElementById("loginMsg_" + loginWhere).style.border = "2px solid #efdf2a";
		}
	}
}


/* **** BEHAVIORS **** */
/*
Event.addBehavior({
  '#pagination p a': Remote.Link({ update: 'commentlist' })
});
*/

var linkObj = {
  commentPager: function(e) {
	//Replace .html from path with '/' (mantis case:1276)
	var strArticlePath = MTArticlePath;
	//MTArticlePath = strArticlePath.replace(/(\.html)$/, '/');
	MTArticlePath = MTArticlePath.substr(0,MTArticlePath.lastIndexOf('/')+1);
	var url = MTArticlePath + Event.element(e).readAttribute('href');
        url = parseUrlToGetFileName(url);     //Fixed mantis case (1319)

    new Ajax.Updater('commentlist', url, {
      evalScripts: true,
           method: 'get'
    });
    e.stop();
  }
};

function parseUrlToGetFileName(href)
{
  winpath=location.href;
  winpath=winpath.replace('?'+location.search,'');
  winpath=winpath.substring(0,winpath.lastIndexOf("/")+1);
  href=href.replace(winpath,'');
  return href;
}

linkObj.bPager = linkObj.commentPager.bindAsEventListener(linkObj);

function observePagination() {
   $$('#pagination p a').each( function(s) {
	  s.stopObserving('click', linkObj.bPager);
	  s.observe('click', linkObj.bPager);
   });
}

document.observe('dom:loaded', observePagination );

Ajax.Responders.register( {
	onComplete: observePagination
});



