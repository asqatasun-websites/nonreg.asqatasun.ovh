/****************************************************************/
/*					BOITE AJAX									*/
/****************************************************************/


window.addEvent('domready', function()
{
	loadBox();
	var sort = new Sortables($('services'), {
		handles : $$('.handle'),		
		onDragStart: function(element, ghost){
				var w = element.getCoordinates();
				element.setStyle('opacity', 0.0);
				ghost.setStyles({'opacity': 0.8,'width': w.width,'text-align':'left'});
		},
		onDragComplete: function(element, ghost){
				ghost.destroy();
				element.setStyle('opacity', 1.0);
		},
		onComplete : function(item){
				removeScotch();
				addScotch();
				
				var listCols = new Array ;
				sort.serialize(function(element, index){
					listCols.push(element.getProperty('id'));
				});
				var order = JSON.encode(listCols);
			
				Cookie.dispose('cosmoBoxs');
				Cookie.write('cosmoBoxs', order, {duration: 30});

		}.bind(sort)
	});			
});

function loadBox()
{
	var cookie = Cookie.read("cosmoBoxs");
	if(cookie)
	{
		var data = eval('(' + cookie + ')');
		for (i=0;i<data.length;i++) {
			if($(data[i]) != null)	$('services').appendChild($(data[i]));
		 } 
	}
	addScotch();	
}

function removeScotch()
{
	var elements = $$('#services .scotch');
	elements.each(function(element){ 
		element.destroy();
	});
}

function addScotch()
{
	var types = $$('#services .roundedBox-1');
	var nbs = types.length-1;
	var i=0;
	for(i;i<nbs;i++)
	{
		var identifiant = (i%3)+1;
		var wrapper = document.createElement('span');
		wrapper.innerHTML = "<span></span>";
		wrapper.className = 'scotch scotch-'+identifiant;
		$(types[i].id).appendChild(wrapper);
	}
}