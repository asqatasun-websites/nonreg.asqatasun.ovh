//*****************************************************************************************
//
// JavaScript File
//
// Name:         HomePage.js
// Description:  List of java script functions used by FlashPlaceholder control.
//
//*****************************************************************************************
//
// FUNCTIONS
//
// Name:         DetectBrowser 
// Description:  Detects browser if flash-enabled.
// Accepts:      None
// Returns:      None
//
// Name:         detectPlugin 
// Description:  Detects browser if flash-enabled.
// Accepts:      None
// Returns:      None
//*****************************************************************************************
//
// REVISION HISTORY
//
// Date Created: 07/10/2006
// Author:       DAV
//
// Revision Date     Author     Description 
// -------------     ------     -----------
// 01/15/07			 RDM        RMT 1738 - Removed LookUpViewType function. Removed redirection
//								of page if page is flash enabled. Used the style attribute in
//								hiding and showing of fields
// 
//*****************************************************************************************

var detectableWithVBHomePage="false";

function DetectBrowser(CmsEditMode, WithFlash)
{
	var nonFlash;
	var flash;
	var rNav;
	var mode;
	
	//get specific table rows for flash, non flash and rightnav
	nonFlash = GetElementId('NonFlashSection');
	flash = GetElementId('FlashSection');
	rNav = GetElementId('RHNav');

	//use the detectPlugin function for Flash plugins
	pluginFound = detectPlugin('Shockwave','Flash'); 
	
	// if not found, try to detect with VisualBasic
    if(!pluginFound && detectableWithVBHomePage)
    {
		pluginFound = detectActiveXControl('ShockwaveFlash.ShockwaveFlash.1');		
    }
    
    if (CmsEditMode == 'true')
    {
		flash.style.display = '';
		nonFlash.style.display = '';
		rNav.style.display = '';
    }

	else
	{
		if (pluginFound && WithFlash == 'true')
		{
			flash.style.display = '';
			nonFlash.style.display = 'none';
			rNav.style.display = 'none';
		}	
		else
		{
			flash.style.display = 'none';
			nonFlash.style.display = '';
			rNav.style.display = '';
		}
	}
}

/******************************************/

function DetectBrowserV2(CmsEditMode, WithFlash)
{
	var nonFlash;
	var flash;
	var rNav;
	var mode;
	
	//get specific table rows for flash, non flash and rightnav
	nonFlash = GetElementId('NonFlashSection');
	flash = GetElementId('FlashSection');
	rNav = GetElementId('RHNav');

	//use the detectPlugin function for Flash plugins
	pluginFound = detectPlugin('Shockwave','Flash'); 
	
	// if not found, try to detect with VisualBasic
    if(!pluginFound && detectableWithVBHomePage)
    {
		pluginFound = detectActiveXControl('ShockwaveFlash.ShockwaveFlash.1');		
    }
    
    if (CmsEditMode == 'true')
    {
		flash.style.display = '';
		nonFlash.style.display = '';
		rNav.style.display = '';
    }

	else
	{
	
		if (pluginFound && WithFlash == 'true'  && !IsFlashHidden())
		{
			flash.style.display = '';
			nonFlash.style.display = 'none';
			rNav.style.display = 'none';
		}	
		else
		{
			flash.style.display = 'none';
			nonFlash.style.display = '';
			rNav.style.display = '';
		}
	}
}

function IsFlashHidden()
{

var TopNavDHTML;
var OS = navigator.platform.toLowerCase();


TopNavDHTML = GetElementId('TopNavWithDHTML');


	if (TopNavDHTML != null)
	{
		if ((OS.indexOf("mac") != -1) && (navigator.userAgent.indexOf("Firefox") != -1 || navigator.userAgent.indexOf("Opera") != -1 ))
		{
			return true;
		}
		
	}
	return false;


}



/*******************************************/



function GetElementId(elemId)
{

   if( document.getElementById ) 
   {
            return document.getElementById(elemId); 
   }
   if( document.all ) 
   {
            return document.all[elemId]; 
   }
}


function detectPlugin() {
    // allow for multiple checks in a single pass
    var daPlugins = detectPlugin.arguments;
    // consider pluginFound to be false until proven true
    var pluginFound = false;
    // if plugins array is there and not fake
    if (navigator.plugins && navigator.plugins.length > 0) 
    {
		var pluginsArrayLength = navigator.plugins.length;
		// iterate plugins
		for (pluginsArrayCounter=0; pluginsArrayCounter < pluginsArrayLength; pluginsArrayCounter++ )
		 {
			// loop through all desired names and check each against the current plugin name
			var numFound = 0;
			for(namesCounter=0; namesCounter < daPlugins.length; namesCounter++) 
			{
				// if desired plugin name is found in either plugin name or description
				if( (navigator.plugins[pluginsArrayCounter].name.indexOf(daPlugins[namesCounter]) >= 0) || 
				(navigator.plugins[pluginsArrayCounter].description.indexOf(daPlugins[namesCounter]) >= 0) ) 
				{
					// this name was found
					numFound++;
				}   
			}
	    // if the number we found matches the total number provided then we were successful
	    if(numFound == daPlugins.length)
			 {
				pluginFound = true;				
				break;
			 }
		}
    }
    return pluginFound;
}


// VBScript to detect plugins for IE browsers
if ((navigator.userAgent.indexOf('MSIE') != -1) && (navigator.userAgent.indexOf('Win') != -1)) {
    document.writeln('<script language="VBscript">');

    document.writeln('\'do a one-time test for a version of VBScript that can handle this code');
    document.writeln('detectableWithVBHomePage = False');
    document.writeln('If ScriptEngineMajorVersion >= 2 then');
    document.writeln('  detectableWithVBHomePage = True');
    document.writeln('End If');

    document.writeln('\'this next function will detect most plugins');
    document.writeln('Function detectActiveXControl(activeXControlName)');
    document.writeln('  on error resume next');
    document.writeln('  detectActiveXControl = False');
    document.writeln('  If detectableWithVBHomePage Then');
    document.writeln('     detectActiveXControl = IsObject(CreateObject(activeXControlName))');
    document.writeln('  End If');
    document.writeln('End Function');    

    document.writeln('</scr' + 'ipt>');
}

function AC_AddExtension(src, ext)
{
  if (src.indexOf('?') != -1)
    return src.replace(/\?/, ext+'?'); 
  else
    return src + ext;
}

function AC_Generateobj(objAttrs, params, embedAttrs) 
{ 
  var str = '<object ';
  for (var i in objAttrs)
    str += i + '="' + objAttrs[i] + '" ';
  str += '>';
  for (var i in params)
    str += '<param name="' + i + '" value="' + params[i] + '" /> ';
  str += '<embed ';
  for (var i in embedAttrs)
    str += i + '="' + embedAttrs[i] + '" ';
  str += ' ></embed></object>';

  document.write(str);
}

function PlayFlash(){
	var ret = 
	AC_GetArgs
	(  arguments, ".swf", "movie", "clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
	, "application/x-shockwave-flash"
	); 
	AC_Generateobj(ret.objAttrs, ret.params, ret.embedAttrs);	
}

function AC_SW_RunContent(){
  var ret = 
    AC_GetArgs
    (  arguments, ".dcr", "src", "clsid:166B1BCA-3F9C-11CF-8075-444553540000"
     , null
    );
  AC_Generateobj(ret.objAttrs, ret.params, ret.embedAttrs);
}

function AC_GetArgs(args, ext, srcParamName, classid, mimeType){
  var ret = new Object();
  ret.embedAttrs = new Object();
  ret.params = new Object();
  ret.objAttrs = new Object();
  for (var i=0; i < args.length; i=i+2){
    var currArg = args[i].toLowerCase();    

    switch (currArg){	
      case "classid":
        break;
      case "pluginspage":
        ret.embedAttrs[args[i]] = args[i+1];
        break;
      case "src":
      case "movie":	
        args[i+1] = AC_AddExtension(args[i+1], ext);
        ret.embedAttrs["src"] = args[i+1];
        ret.params[srcParamName] = args[i+1];
        break;
      case "onafterupdate":
      case "onbeforeupdate":
      case "onblur":
      case "oncellchange":
      case "onclick":
      case "ondblClick":
      case "ondrag":
      case "ondragend":
      case "ondragenter":
      case "ondragleave":
      case "ondragover":
      case "ondrop":
      case "onfinish":
      case "onfocus":
      case "onhelp":
      case "onmousedown":
      case "onmouseup":
      case "onmouseover":
      case "onmousemove":
      case "onmouseout":
      case "onkeypress":
      case "onkeydown":
      case "onkeyup":
      case "onload":
      case "onlosecapture":
      case "onpropertychange":
      case "onreadystatechange":
      case "onrowsdelete":
      case "onrowenter":
      case "onrowexit":
      case "onrowsinserted":
      case "onstart":
      case "onscroll":
      case "onbeforeeditfocus":
      case "onactivate":
      case "onbeforedeactivate":
      case "ondeactivate":
      case "type":
      case "codebase":
        ret.objAttrs[args[i]] = args[i+1];
        break;
      case "width":
      case "height":
      case "align":
      case "vspace": 
      case "hspace":
      case "class":
      case "title":
      case "accesskey":
      case "name":
      case "id":
      case "tabindex":
        ret.embedAttrs[args[i]] = ret.objAttrs[args[i]] = args[i+1];
        break;
      default:
        ret.embedAttrs[args[i]] = ret.params[args[i]] = args[i+1];
    }
  }
  ret.objAttrs["classid"] = classid;
  if (mimeType) ret.embedAttrs["type"] = mimeType;
  return ret;
}

//START - Functions for TopNavHotSpots
boxOn = new Image();
boxOn.src = "../images/topnav_box_on.gif";
boxOff = new Image();
boxOff.src = "../images/topnav_box_off.gif";

pipeOn = new Image();
pipeOn.src = "../images/topnav_pipe_on.gif";
pipeOff = new Image();
pipeOff.src = "../images/topnav_pipe_off.gif";

function turnOn(imgType, imgName) { 
	imgOn = eval(imgType +"On.src");
	document.getElementById(imgName).src = imgOn; 
}

function turnOff(imgType, imgName) { 
	imgOff = eval(imgType + "Off.src");
	document.getElementById(imgName).src = imgOff; 
}

//buttonX, navPipeX
function buttonRollOver(buttonNum){
	var rightPipeNum = buttonNum + 1;
	var leftPipe = "navDivider" + buttonNum;
	var boxName = "topnav" + buttonNum;
	var rightPipe = "navDivider" + rightPipeNum;
	
	turnOn('pipe', leftPipe);
	turnOn('box', boxName);
	turnOn('pipe', rightPipe);
}

function buttonRollOut(buttonNum){
	var rightPipeNum = buttonNum + 1;
	var leftPipe = "navDivider" + buttonNum;
	var boxName = "topnav" + buttonNum;
	var rightPipe = "navDivider" + rightPipeNum;
	
	turnOff('pipe', leftPipe);
	turnOff('box', boxName);
	turnOff('pipe', rightPipe);
}
//END - Functions for TopNavHotSpots

/* Funtion for finding HTML Control */
function getHomePageImage(id) 
{
	if (document.getElementById) {
		return document.getElementById(id);
		}
	else if (document.all) {
		return document.all[id];
		}
}


function homepage_random_image()
{
	if (getHomePageImage("sPageImage")==null) return;
	getHomePageImage("sPageImage").style.display = ''
	if (document.all)
	    {
		document.images("imgPageImage").src = random_pageImage[random_pageImage_number];
		document.images("imgPageImage").Alt = random_pageImage_alt[random_pageImage_number];
		document.images("imgPageImage").title = random_pageImage_alt[random_pageImage_number];
		document.images("imgPageImage").useMap = random_pageImage_hotspot[random_pageImage_number];
		getHomePageImage("HomePageHeight").height = document.images("imgPageImage").height
		}
	else
	    {
		document.getElementById("imgPageImage").setAttribute("src", random_pageImage[random_pageImage_number]);
		document.getElementById("imgPageImage").setAttribute("alt", random_pageImage_alt[random_pageImage_number]);
		document.getElementById("imgPageImage").setAttribute("title", random_pageImage_alt[random_pageImage_number]);
		document.getElementById("imgPageImage").setAttribute("usemap", random_pageImage_hotspot[random_pageImage_number]);
		document.getElementById("HomePageHeight").height = document.getElementById("imgPageImage").height
		}
}

