// JavaScript Document
// Emule le focus sous IE
function focusfix(selector, className) {
	$(selector).focus(function() {
		$(this).addClass(className);
	});
	$(selector).blur(function() {
		$(this).removeClass(className);
	});
}
$(document).ready(function() {
	// Emule le focus sous IE
	focusfix('a', 'focus');
	// Onglets
	$('ul.onglets').tabs({selected:0, fx:{opacity:"toggle"}});
	// Identifie les liens pointant vers des sites externes (ajout d'un pictogramme � droite du lien)
	$("#sous_principal a[@href^=\"http\"]").addClass("lien_externe");
	$("a.lightbox").removeClass("lien_externe");
	$("div.vue_line a").removeClass("lien_externe");
	$("div.object-left a").removeClass("lien_externe");
	$("div.object-right a").removeClass("lien_externe");
	$("div.object-center a").removeClass("lien_externe");
	$("#liens_utiles a").removeClass("lien_externe");
	// Aspect des lignes (<tr>) des tableaux de donn�es au survol
	$('table.tableau_donnees tr').mouseover(function(){$(this).addClass('survol');}).mouseout(function(){$(this).removeClass('survol');});
	// Infobulle
	$('a.infobulle').cluetip({activation: 'click', width: 500, sticky: true, closePosition: 'title'});
	// Lightbox
	$('a.lightbox').lightBox();
	
	$("#communes_nom").autocomplete("/suggest/commune", {
				multiple: true,
				width: 150,
				selectFirst: false
			});
	$("#communes_nom").result(function(event, data, formatted) {
			if (data)
				$(this).parent().next().find("input").val(data[1]);
		});
	$("#territoire_nom").autocomplete("/suggest/territoire", {
				multiple: true,
				width: 150,
				selectFirst: false
			});
	$("#territoire_nom").result(function(event, data, formatted) {
			if (data)
				$(this).parent().next().find("input").val(data[1]);
		});	
		
});