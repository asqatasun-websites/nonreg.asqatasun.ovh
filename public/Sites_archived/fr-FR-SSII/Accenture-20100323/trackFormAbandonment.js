if(typeof(ms_formAbandonmentName) == "undefined")
    var ms_formAbandonmentName = ""

var msTrackFormAbandonment = new TrackFormAbandonment(ms_formAbandonmentName)

function TrackFormAbandonment(trackForm)
{
    var enValue;
    var isMicrositesList = true;
    if(!trackForm)
    {
        isMicrositesList = false;
        var forms = document.getElementsByTagName("FORM");
        if(forms)
            trackForm = forms[0].id;
    }
    var trackFormTemplate = trackForm;
    if(window.addEventListener)
        window.addEventListener('load', startTrackFormAbandonment, false);
    else
        window.attachEvent('onload', startTrackFormAbandonment);
   
    function getLastFocusValue()
    {
		var lastFocusValue = null;
        var path = getCurrentPathName();
        var cookieValue = getCookieValue("formAbandonmentLastFocus")
               
        if(cookieValue)
        {
            //check last cookie save for this form
            var startPos = cookieValue.indexOf(path);
            if(startPos >= 0)
            {
                if(cookieValue.indexOf("|") >= 0)
                    lastFocusValue = cookieValue.substr(startPos, cookieValue.indexOf("|", startPos) - startPos + 1);
            }
        }
       
        return lastFocusValue;
    }
    
    function saveLastfocus(e)
    {
        var srcElem = e.srcElement? e.srcElement:e.target;
        var id;
        var elemType = srcElem.type;
        if(elemType && elemType.toLowerCase() == "radio")
        {
            id = srcElem.parentNode.getAttribute("name"); 
        }
        if(!id)
        {
            id = srcElem.getAttribute("name");
            if(!id)
                id = srcElem.getAttribute("name");
        }
        if(id)
        {
			// get entered value
        	//enValue = document.getElementById(id).value; 
        	enValue = srcElem.value
            var path = getCurrentPathName();
            var curFocusValue = path + ":" + getFormTitle() + ":(" + id + ")|";
            var cookieValue = getCookieValue("formAbandonmentLastFocus")
            if(cookieValue)
            {
                var lastFocusValue = getLastFocusValue();
                if(lastFocusValue)
                {
                    cookieValue = cookieValue.replace(lastFocusValue, "");
                }
                document.cookie = "formAbandonmentLastFocus=" + cookieValue + curFocusValue + "; path=/;";    
            }
            else
                document.cookie = "formAbandonmentLastFocus=" + curFocusValue + "; path=/;";             
        }
    }
    
    function getCurrentPathName()
    {
		var ifCentralProfile = new String(document.location);
		
		if(ifCentralProfile.toLowerCase().indexOf("centralprofileform.aspx") != -1 ||
			ifCentralProfile.toLowerCase().indexOf("ean.aspx") != -1 ||
			ifCentralProfile.toLowerCase().indexOf("emailalerts.aspx") != -1)
		{
			return unescape(document.location.pathname.toLowerCase().substr(1));
		}
		else
		{
			var urlString = new String(document.location);
	
			if(urlString.indexOf(".aspx") == -1)
			{
				return unescape(document.location.pathname.toLowerCase().substr(1));
			}
			else
			{
				var _urlString1 = document.referrer.toLowerCase();
				var pos1 = _urlString1.indexOf("/");			
				var _urlString = _urlString1.substring(_urlString1.indexOf("/", pos1 + 2));
				_urlString = _urlString.substr(1);
		
				if(_urlString.indexOf("?") == -1)
				{
					return _urlString;
				}
				else
				{
					var _url = _urlString.substring(0, _urlString.indexOf("?"))
					return _url;
				}
			}
		}
		
    }
      
    function setNotAbandonment(e)
    {
        var cookieValue = getCookieValue("ignoreFormAbandonment")
        var path = getCurrentPathName();
        if(cookieValue)
        {
            if(cookieValue.indexOf(path) == -1)
                document.cookie = "ignoreFormAbandonment=" + cookieValue + path + "|;path=/;";    
        }
        else
            document.cookie = "ignoreFormAbandonment=" + path + "|;path=/;";    
    }
    
    function checkAbandomentStatus()
    {
        var cookieValue = getCookieValue("ignoreFormAbandonment"); 
        var path = getCurrentPathName();		
        if(cookieValue && cookieValue.indexOf(path + "|") >= 0)
        {
            cookieValue = cookieValue.replace(path + "|", "");           
            document.cookie = "ignoreFormAbandonment=" + cookieValue + "; path=/;";  
        }
        else
        {
            cookieValue = getCookieValue("formAbandonmentLastFocus");
           
            if(cookieValue)
            {
                var lastFocusValue = getLastFocusValue();
                if(lastFocusValue)
                {
                    cookieValue = cookieValue.replace(lastFocusValue, "");
                    document.cookie = "formAbandonmentLastFocus=" + cookieValue + ";path=/;";
                    lastFocusValue = lastFocusValue.substr(0, lastFocusValue.length -1);                    
                }
            }
           
            if (!enValue)
            {
                lastFocusValue = path + ":" + getFormTitle() + ":(No Data Entered)"
            }
            
          
            s.linkTrackVars='prop29,eVar41,events';
            s.linkTrackEvents='event34';
            s.eVar41=lastFocusValue// Value of field last interacted with
            s.events="event34";
            s.tl(true,"o","Form Abandon");
            
        }
    }
    
    function getFormTitle()
    {
        if(isMicrositesList)
            return document.title;
        else
            return trackFormTemplate;
    }
    
    function getCookieValue(cookieName)
    {
        if (document.cookie.length>0)
        {
            c_start=document.cookie.indexOf(cookieName + "=");
            if (c_start!=-1)
            { 
                c_start = c_start + cookieName.length+1;
                c_end = document.cookie.indexOf(";",c_start);
                if (c_end==-1)
                    c_end=document.cookie.length;    
                return unescape(document.cookie.substring(c_start,c_end));
            } 
        }
        return null;
    }
    
    function attachSaveLastFocusToTagName(tagName, type)
    {
        var elems = document.getElementsByTagName(tagName);
        var i = 0;
        var attach;
        if(elems && elems.length > 0)
        {
        
            for(i=0;i<elems.length;i++)
            {
                attach = true;
                if(type && elems[i].type.toLowerCase() != type.toLowerCase())
                    attach = false;            
                    
                if(attach == true)
                {
                    if(window.addEventListener)
                    {
                        elems[i].addEventListener('blur', saveLastfocus, false);
                        elems[i].addEventListener('change', saveLastfocus, false);
                        elems[i].addEventListener('click', saveLastfocus, false);
                        elems[i].addEventListener('keypress', saveLastfocus, false);
                        elems[i].addEventListener('keyup', saveLastfocus, false);
                        
                    }
                    else
                    {
                        elems[i].attachEvent('onblur', function(){saveLastfocus(event);});
                        elems[i].attachEvent('onchange', function(){saveLastfocus(event);});
                        elems[i].attachEvent('onclick', function(){saveLastfocus(event);});
                        elems[i].attachEvent('onkeypress', function(){saveLastfocus(event);});
                        elems[i].attachEvent('onkeyup', function(){saveLastfocus(event);});
                    }
                }
            }
        }
    }
    
    function attachIgnoreAbandonmentEvent(objTrackForm)
    {
        var tagNameValue = objTrackForm.postBackControlsByTagName;
        if(tagNameValue.length > 0)
        {
            var splitTagNameValue = tagNameValue.split(";");
            var i = 0;
            if(splitTagNameValue.length > 0)
            {
                for(i=0;i<splitTagNameValue.length;i++)
                {
                    var splitValue = splitTagNameValue[i].split("=");
                    if(splitValue && splitValue.length>0)
                    {
                        for(var ii=0;ii<splitValue.length;ii++)
                        {
                            var elems = document.getElementsByTagName(splitValue[ii]);
                            if(elems && elems.length > 0)
                            {
                                for(var iii=0;iii<elems.length;iii++)
                                {
                                    var splitIds = splitValue[1].split(":");
                                    if(splitIds && splitIds.length>0)
                                    { 
                                        for(var iiii=0;iiii<splitIds.length;iiii++)
                                        {
                                            var elemId = elems[iii].id.toLowerCase();
                                            if(elemId.substr(elemId.length - splitIds[iiii].length) == splitIds[iiii].toLowerCase())
                                            {
												if(splitValue[ii].toLowerCase() == "select")
												{	
													var attribs = elems[iii].attributes;
													if(attribs && attribs.length>0)
													{
														for(var iiiii=0;iiiii<attribs.length;iiiii++ )
														{
															var attriValue = attribs[iiiii].value;
															if(attriValue.indexOf("doPostBack")>=0)
															{
																if(window.addEventListener)
																{
																	elems[iii].addEventListener('click', setNotAbandonment, false);
																}
																else
																{
																	elems[iii].attachEvent('onclick', setNotAbandonment);
																}
															}
														}
													}	
												}
												else
												{
													if(window.addEventListener)
													{
														elems[iii].addEventListener('click', setNotAbandonment, false);
													}
													else
													{
														elems[iii].attachEvent('onclick', setNotAbandonment);
													}
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        var controlsById = objTrackForm.postBackControlsById;
        if(controlsById)
        { 
            var splitControlsById = controlsById.split(";");
            if(splitControlsById.length > 0)
            {
                for(i=0;i<splitControlsById.length;i++)
                {
                    var elem = document.getElementById(splitControlsById[i]);
                    if(elem)
                    {
                        if(window.addEventListener)
                        {
                            elem.addEventListener('click', setNotAbandonment, false);
                        }
                        else
                        {
                            elem.attachEvent('onclick', setNotAbandonment);
                        }
                    }
                }
            }
        }
    }
   
        
    function attachEventFocus(retValue)
    {
        if(window.addEventListener)
            window.addEventListener('unload', checkAbandomentStatus, false);              
        else
            window.attachEvent('onunload', checkAbandomentStatus);
          
        attachSaveLastFocusToTagName("INPUT", "text");
        attachSaveLastFocusToTagName("INPUT", "password");
        attachSaveLastFocusToTagName("INPUT", "radio");
        attachSaveLastFocusToTagName("INPUT", "checkbox");
        attachSaveLastFocusToTagName("IFRAME");
        attachSaveLastFocusToTagName("SELECT");
        attachSaveLastFocusToTagName("TEXTAREA");
        attachIgnoreAbandonmentEvent(retValue);
    }
    
    function TrackForm()
    {
        this.formID = "";
        this.postBackControlsByTagName = "";
        this.postBackControlsById = "";
        this.isPostBack = false;
    }
    
    var msTrackFormListCollection = new Array();
    var ms_formList = new TrackForm();
    //FEMT Update        
    /*ms_formList.formID = "MicrositesList";
    ms_formList.postBackControlsByTagName = "input=NextPage:SaveItem";
    msTrackFormListCollection[0] = ms_formList;*/
    ms_formList.formID = "GenericTemplate";
    ms_formList.postBackControlsById = "dynamicForm_PresentationModeControlsContainer__ctl0__ctl10_ddlIndustry;dynamicForm_PresentationModeControlsContainer__ctl0__ctl14_ddlCountry"
    ms_formList.postBackControlsByTagName = "input=btnSubmit:btnClear:btnContinue;select=ddlCountry:ddlIndustry"
    msTrackFormListCollection[0] = ms_formList;
    
    ms_formList = new TrackForm();
    ms_formList.formID = "IMFormTemplate";
    ms_formList.postBackControlsById = "dynamicForm_PresentationModeControlsContainer__ctl0__ctl9_ddlCountry"
    ms_formList.postBackControlsByTagName = "input=btnSubmit:btnClear:btnContinue;select=ddlCountry:ddlIndustry"
    msTrackFormListCollection[1] = ms_formList;
    
    ms_formList = new TrackForm();
    ms_formList.formID = "CentralProfileForm2";
    //ms_formList.postBackControlsById = "dynamicForm_PresentationModeControlsContainer__ctl0__ctl10_ddlIndustry;dynamicForm_PresentationModeControlsContainer__ctl0__ctl14_ddlCountry"
    ms_formList.postBackControlsById = "IndustryFieldControl1_ddlIndustry;CountryStateControl1_ddlCountry;btnSubmit;btnClear;btnContinue"
    msTrackFormListCollection[2] = ms_formList;
    
    ms_formList = new TrackForm();
    ms_formList.formID = "EANForm";
    ms_formList.postBackControlsById = "dynamicForm_PresentationModeControlsContainer__ctl0__ctl10_ddlIndustry;dynamicForm_PresentationModeControlsContainer__ctl0__ctl14_ddlCountry;EANCheckboxes1_Button2;LoginUI_btnSubmit"
    msTrackFormListCollection[3] = ms_formList;
    
    ms_formList = new TrackForm();
    ms_formList.formID = "EmailAlertsForm";
    ms_formList.postBackControlsById = "ddlIndustry;ddlCountry;btnSubmit;btnClear"
    msTrackFormListCollection[4] = ms_formList;
    
    
    //this.startTrackFormAbandonment = function ()
    function startTrackFormAbandonment()
    {
        if(trackFormTemplate && trackFormTemplate != "")
        {
            if(msTrackFormListCollection && msTrackFormListCollection.length > 0)
            {
                for(var i=0;i<msTrackFormListCollection.length;i++)
                {
                    if(msTrackFormListCollection[i].formID.toLowerCase() == trackFormTemplate.toLowerCase())
                    {
                        attachEventFocus(msTrackFormListCollection[i]);
                        break;
                    }
                }
            }
        }
    }
}
