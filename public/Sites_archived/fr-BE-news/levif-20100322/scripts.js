$(function() {

	setTimeout(function(){
		try{
			if(hasReactions == false){
				$("#comments").css({ display: "none" });
			}
		} catch (e){
			setTimeout(function(){
				try{
					if(hasReactions == false){
						$("#comments").css({ display: "none" });
					}
				} catch (e){}
			},1000);
		}
	},500);

    if($("#login").length > 0) {
		if($("#source").html() == 'vifclub'){
	        $.get(
	            "/includes/services/login-vif-club.jsp",
	            { siteURL: siteURL, rnd: Math.random() },
	            function(data) {
	                $("#login").html(data);
	            }
	        );		
		}else{
	        $.get(
	            "/includes/services/login.jsp",
	            { siteURL: siteURL, rnd: Math.random() },
	            function(data) {
	                $("#login").html(data);
	            }
	        );		
		}	
    }
    
    if($("#prom-form").length > 0) {
        var data = $("#prom-form").attr("class") + "";
        $.get(
            "/includes/prom/form.jsp",
            {CAMP: getParam(data, "campaign"), XSL: getParam(data, "stylesheet"), siteURL: siteURL, rnd: Math.random() },
            function(data) {
                $("#prom-form").html(data);
                fillFields();
				$.getScript("promform.js", function(){
				  //alert("Script loaded and executed.");
				});            }
        );
		
    }

    if($("#winners").length > 0) {
        var data = $("#winners").attr("class") + "";
        $.get(
            "/includes/prom/form.jsp",
            {CAMP: getParam(data, "campaign"), XSL: getParam(data, "stylesheet"), WINNAARS: "J", LIJST: "J", REGI: "N", rnd: Math.random() },
            function(data) {
                $("#winners").html(data);
            }
        );
    }
    
    if($("#registerFrame").length > 0) {
        iFrameHeight("registerFrame");
    }

    if($("#reservationFrame").length > 0) {
        iFrameHeight("reservationFrame");
    }
    
    if($("#articleContainer .page").length > 0) {
        var slides = new Array();
        var i = 0;
        $('#articleContainer .page').each(function() {
            i++;
            slides.push(i);
        });
        $('#articleContainer').pager('.page', {
              navClass: 'paging',
              linkText: slides,
              height: '500px'
        });        
    }
    
    if($("#mail-form").length > 0) {
        var action = $("#mail-form").attr("action");
        action = action.substring(action.indexOf("/", 1));
        $("#mail-form").attr("action", action);
    }
    
    if($("#queryField").length > 0) {
        var searchField = $("#queryField");
        searchField.attr("style", "color: #808080");
        searchField.attr("value", "recherche dans le site");
        searchField.focus(function() {
            if($(this).attr("value") == "recherche dans le site") {
                $(this).attr("style", "color: inherit");
                $(this).attr("value", "");
            }
        });
        searchField.blur(function() {
            if($(this).attr("value") == "") {
                $(this).attr("style", "color: #808080");
                $(this).attr("value", "recherche dans le site");
            }
        });
    }
    
});

function getParam(data, name) {
    var value = null;
    var pos = data.indexOf(name + ":");
    if(pos > -1) {
        var subData = data.substring(pos);
        if(subData.indexOf(" ") != -1) {
            value = subData.substring(subData.indexOf(":") + 1, subData.indexOf(" "));
        } else {
            value = subData.substring(subData.indexOf(":") + 1);
        }
    }
    return value;
}

function iFrameHeight(frame) {
    try {
        if(!document.all) {
            h = document.getElementById(frame).contentDocument.body.offsetHeight;
            if (h < 400) { h = 400; }
            document.getElementById(frame).style.height = h + 50 + "px";
        }
        else {
            h = document.frames[frame].document.body.scrollHeight;
            if (h < 400) { h = 400; }
            document.getElementById(frame).height = h + 25;
        }
    } catch(e) {
        // iframe is nog niet geladen, geen ramp
    }
    setTimeout("iFrameHeight()", 500);
}

function playVideo(siteID, articleID, paragraphID) {
    var url = "http://cm.roularta.be/Streaming/asx.asp?siteID=" + siteID + "&articleID=" + articleID + "&parID=" + paragraphID;
    var html = '<object id="mediaPlayer" CLASSID="CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6" height="250" width="375">';
    html += '<param name="url" value="' + url + '"/>';
    html += '<param name="AutoStart" value="true"/>';
    html += '<param name="ShowControls" value="true"/>';
    html += '<param name="ShowStatusBar" value="true"/>';
    html += '<param name="loop" value="false"/>';
    html += '<param name="Name" value="mediaPlayer"/>';
    html += '<param name="Id" value="mediaPlayer"/>';
    html += '<embed type="application/x-mplayer2" ';
    html += ' pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" ';
    html += ' clsid="{6BF52A52-394A-11d3-B153-00C04F79FAA6}" ';
    html += ' src="' + url + '" ';
    html += ' height="250" ';
    html += ' width="375" ';
    html += ' ShowStatusBar="1"/>';
    html += '</object>'
    $("#player").html(html);
}