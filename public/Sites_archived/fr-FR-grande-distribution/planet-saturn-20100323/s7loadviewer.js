/**
Adds Scene7 flash viewer to HTML page in the place this methods was called. Should not be executed after the document is ready.
@param inURL:String flash viewer URL
@param inWidth:String flash object width
@param inHeight:String flash object height
@param inID:String flash object id (ID= attribute of OBJECT tag and NAME= attribute of EMBED tag)
@param inBgColor:String flash object background color, in #RRGGBB format
@param inWmode:String (optional) wmode attribute value, usually is "window", "opaque" or transparent. Default is "window"
@param inParentId
*/
function s7loadViewer(inURL, inWidth, inHeight, inID, inBgColor, inWmode, inParentId) {
	var html = s7GetViewerHTML(inURL, inWidth, inHeight, inID, inBgColor, inWmode);
	if(inParentId != null) {
		$(inParentId).set('html', html);
	} else {
		document.writeln(html);	
	}
}

/**
Returns HTML markup suitable for embedding Scene7 flash viewer into the page.
@param inURL:String flash viewer URL
@param inWidth:String flash object width
@param inHeight:String flash object height
@param inID:String flash object id (ID= attribute of OBJECT tag and NAME= attribute of EMBED tag)
@param inBgColor:String flash object background color, in #RRGGBB format
@param inWmode:String (optional) wmode attribute value, usually is "window", "opaque" or transparent. Default is "window"
@return :String HTML code for embedding the viewer
*/
function s7GetViewerHTML(inURL, inWidth, inHeight, inID, inBgColor, inWmode) {
	if (!inWmode) {
		inWmode = 'window';
	}
	var html = '';
	html += "<object ";
	html += "	classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' ";
	html += "	codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0' ";
	html += "	width='" + inWidth + "' ";
	html += "	height='" + inHeight + "' ";
	html += "	id='" + inID + "' ";
	html += "	align=''>";
	html += "	<param name='movie' value='" + inURL + "'>";
	html += "	<param name='quality' value='high'>";
	html += "	<param name='bgcolor' value='" + inBgColor + "'>";
	html += "	<param name='AllowScriptAccess' value='always'>";
	html += "	<param name='wmode' value='" + inWmode + "'>";
	html += "	<embed ";
	html += "		src='" + inURL + "' ";
	html += "		quality='high' ";
	html += "		bgcolor='" + inBgColor + "'  ";
	html += "		wmode='" + inWmode + "' ";
	html += "		width='" + inWidth + "' ";
	html += "		height='" + inHeight + "' ";
	html += "		name='" + inID + "' ";
	html += "		AllowScriptAccess='always' ";
	html += "		align='' ";
	html += "		type='application/x-shockwave-flash' ";
	html += "		pluginspage='http://www.macromedia.com/go/getflashplayer'>";
	html += "	</embed>";
	html += "</object>";
	return html;
}
