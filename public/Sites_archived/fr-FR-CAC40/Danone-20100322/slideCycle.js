var slideCycle = function(datas) {
	datas.start = datas.start - 1 || 0;
	datas.animDiv = datas.animDiv || 5;
	datas.animDelay = datas.animDelay || 40;
	
	var context, items, prev, next, css = datas.direction || 'left', itemsNb = 0, timer, inAnim = false, iRef = false, where, lastMove, tabs = [], tabsPos = [], tabsPosTo = [];
	
	function getElements() {
		if(iRef === false) {
			for(var i = datas.start; i < (datas.itemsDisplayed + datas.start); i++) {tabs.push(items[verifIndex(i)]);}
		} else if(!lastMove || where == lastMove) {
			if(where == 'next') {
				if(lastMove) {tabs.shift();}
				tabs.push(items[iRef]);
			} else {
				if(lastMove) {tabs.pop();}
				tabs.unshift(items[iRef]);
			}
		}
		if(lastMove) {datas.start = 0;}
		reposition();
	}
	
	function computeTabs() {
		var i = -1, l = tabs.length, move = where == 'next' ? - datas.move : datas.move;
		while(++i < l) {
			tabsPos[i] = parseInt(tabs[i].style[css]);
			tabsPosTo[i] = tabsPos[i] + move;
		}
	}
	
	function reposition() {
		if(where) {
			if(where == 'next') {var i = tabs.length - 1, move = datas.move * datas.itemsDisplayed;}
			else {var i = 0, move = - datas.move;}
			tabs[i].style[css] = move +'px';
		} else {
			for(var i = 0; i < datas.itemsDisplayed; i++) {tabs[i].style[css] = (i * datas.move) +'px';}
		}
	}
	
	function verifIndex(i) {
		if(i >= itemsNb) {i = i - itemsNb;}
		else if(i < 0) {i = i + itemsNb;}
		return i;
	}
	
	function slideEnd() {
		tabs[where == 'next' ? 0 : datas.itemsDisplayed].style[css] = datas.offScreen +'px';
		clearTimeout(timer);
		inAnim = false;
	}
	
	function slideCheck(cur, pos) {
		return where == 'next' ? cur <= pos : cur >= pos;
	}
	
	function slideCompute(cur, pos) {
		return cur + Math[where == 'next' ? 'floor' : 'ceil']((pos - cur) / datas.animDiv);
	}
	
	function slide() {
		for(var i = 0, tab; tab = tabs[i]; i++) {
			var cur = parseInt(tab.style[css]);
			if(slideCheck(cur, tabsPosTo[i])) {
				tab.style[css] = tabsPosTo[i] +'px';
				if(i == datas.itemsDisplayed) {slideEnd();}
			} else {
				tab.style[css] = slideCompute(cur, tabsPosTo[i]) +'px';
				if(i == datas.itemsDisplayed) {timer = setTimeout(slide, datas.animDelay);}
			}
		}
	}
	
	function slideStart() {
		if(inAnim) {return false;}
		if(navigator.appName == 'Microsoft Internet Explorer'){
			where = this == next ? 'next' : 'prev';
		}else{
			where = this == prev ? 'prev' : 'next';
		}
		if(where == 'next') {
			iRef = iRef === false ? verifIndex(datas.itemsDisplayed + datas.start) : verifIndex(iRef + (lastMove == 'prev' ? datas.itemsDisplayed : 1));
		} else {
			iRef = iRef === false ? verifIndex(itemsNb - 1 + datas.start) : verifIndex(iRef - (lastMove == 'next' ? datas.itemsDisplayed : 1));
		}
		getElements();
		computeTabs();
		lastMove = where;
		inAnim = true;
		slide();
		return false;
	}
	
	
	context = datas.context.nodeName ? datas.context : document.getElementById(datas.context);
	if(!context) {return;}
	items = datas.items.length && datas.items[0].nodeName ? datas.items : context.getElementsByTagName(datas.items);
	itemsNb = items.length;
	if(itemsNb > datas.itemsDisplayed) {
		next = datas.next.nodeName ? datas.next : document.getElementById(datas.next);
		prev = datas.prev.nodeName ? datas.prev : document.getElementById(datas.prev);
		getElements(items);
		next.onclick = prev.onclick = slideStart;
	}
	
	window.setInterval(slideStart,4000);
};

