/******************************************************************************
* cmJQuery.js
*******************************************************************************

*******************************************************************************
*                                                                             *
* Copyright 2010									                          *
*                                                                             *
******************************************************************************/

$(document).ready(function() 
{

	$('ul.cmMenuBar').superfish(
	{ 
        delay:       1000,                            // one second delay on mouseout 
        animation:   {opacity:'show'},                // fade-in and slide-down animation 
        speed:       'slow',                          // faster animation speed 
        autoArrows:  false,                           // disable generation of arrow mark-up 
        dropShadows: false                            // disable drop shadows 
    });
});