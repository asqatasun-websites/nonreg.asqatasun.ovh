/**
 * @author jeremy@eolas.fr
 */

YAHOO.namespace("YAHOO.ownFrame");
YAHOO.ownFrame.divId = 'ownFrame';


YAHOO.util.Event.addListener(window, 'load', function() {
	oDoc = document.getElementById('document');
	YAHOO.ownFrame.initialisation();
}); 

YAHOO.ownFrame.initialisation = function() {
	
	YAHOO.util.Dom.getElementsByClassName('popupVideo','a','document', function() {

		this.setAttribute('rel',this.href);
		this.href = 'javascript:void(0)';
		
		/* Server @ FLV @ Titre*/
		aURL = this.getAttribute('rev').split("@");
		var srv = aURL[0];
		var url = aURL[1];
		var titre = aURL[2];
		
		var lien = this.getAttribute('rel');

		YAHOO.util.Event.addListener(this, 'click', function() {
			/*YAHOO.ownFrame.show(this.getAttribute('rev'),this.getAttribute('title'),this.getAttribute('rel'));*/
			YAHOO.ownFrame.show(srv,url,titre,lien);
			return false;
		});	
		//this.setAttribute('title','toto@tata');
	});
}

YAHOO.ownFrame.coverFocus = function(evt) {
    YAHOO.util.Event.stopEvent(evt);
}

YAHOO.ownFrame.show = function(srv,url,titre,urlzapping) {
	YAHOO.ownFrame.showCover();

	oBloc = document.getElementById(YAHOO.ownFrame.divId);

	winH = YAHOO.util.Dom.getViewportHeight();
	winW = YAHOO.util.Dom.getViewportWidth();	
	if (!oBloc) {
		var objBody = document.getElementsByTagName("body").item(0);
		oBloc = document.createElement("div");
		oBloc.setAttribute('id', YAHOO.ownFrame.divId);
		YAHOO.util.Dom.setStyle(oBloc, "position", "absolute");
		YAHOO.util.Dom.setStyle(oBloc, "z-index", 200);
		/*YAHOO.util.Dom.setStyle(oBloc, "overflow", "hidden");*/
		YAHOO.util.Dom.setStyle(oBloc, "padding-top", 0);
		
		objBody.appendChild(oBloc);
	}
	
	for(i=0;i<document.getElementsByTagName('select').length;i++){
		document.getElementsByTagName('select').item(i).style.visibility='hidden';
	}
	for(i=0;i<document.getElementsByTagName('object').length;i++){
		document.getElementsByTagName('object').item(i).style.visibility='hidden';
	}
	
	var bloc_apparition = {opacity:{from:0,to:0.8}};
    var animApparition = new YAHOO.util.Anim('cover', bloc_apparition,.5);
    animApparition.animate();

		
	YAHOO.util.Dom.setStyle(oBloc, "display", "block");
	YAHOO.util.Dom.setStyle(oBloc, "height", "0");
	YAHOO.util.Dom.setStyle(oBloc, "width", '700px'); // 500 = taille du teaser + 82 = logo client

	

	var bloc_deplierVert = {height:{from:0,to:460}};
    var animDeplier = new YAHOO.util.Anim('ownFrame', bloc_deplierVert,.6);
	animApparition.onComplete.subscribe(function(){animDeplier.animate()});

	/*Tableau avec url en premiere cellule et titre en deuxieme*/
/*	aFLV = url.split("@");
	if(aFLV[1]==undefined) {
		aFLV[0]=url;
		aFLV[1]='&nbsp;';
	}*/
    animDeplier.onComplete.subscribe(function(){YAHOO.ownFrame.printContent(srv,url,urlzapping,titre);});
   
	YAHOO.ownFrame.positionUpdate();
}

YAHOO.ownFrame.printContent = function(srv,url,urlzapping,titre) {
    var innerHTML = "<div id=\"popupAffichageVideo\"><h3>"+titre+"</h3>";
    var javascriptID = 'mediaJsID_' + Math.round(Math.random()*1000);

	/*
	innerHTML += '<embed src="'+SERVER_ROOT+'include/flashplayer/mediaplayer.swf" width="500" height="360" allowscriptaccess="always" allowfullscreen="true" flashvars="height=360&width=500&autostart=true&file='+srv+'&id='+url+'&backcolor=0x000000&frontcolor=0xFFFFFF&lightcolor=0xf8bc00&screencolor=0x000000"/>';
	innerHTML += '<div id="bottomNav"><a href="'+urlzapping+'" class="toutesLesVideos">Voir toutes les vidéos</a> <a href="#" class="close" onclick="YAHOO.ownFrame.close();return false;"><span>Fermer</span></a></div>';
	*/
	innerHTML += '<embed src="'+SERVER_ROOT+'include/flashplayer/mediaplayer.swf" width="500" height="360" allowscriptaccess="always" allowfullscreen="true" flashvars="javascriptid='+javascriptID+'&enablejs=true&height=360&width=500&autostart=true&file='+srv+'&id='+url+'&backcolor=0x000000&frontcolor=0xFFFFFF&lightcolor=0xf8bc00&screencolor=0x000000" title="'+titre+'" id="'+javascriptID+'"/>';
	if(sit_short_lang == 'fr'){
		innerHTML += '<div id="bottomNav"><a href="'+urlzapping+'" class="toutesLesVideos">Voir toutes les vidéos</a> <a href="#" class="close" onclick="YAHOO.ownFrame.close();return false;"><span>Fermer</span></a></div>';
	} else {
		innerHTML += '<a href="#" class="close" onclick="YAHOO.ownFrame.close();return false;"><span>Fermer</span></a></div>';
	}
	innerHTML += '</div>';
	
	oBloc = document.getElementById(YAHOO.ownFrame.divId);
	oBloc.innerHTML = innerHTML;

	xt_mediaAdd(javascriptID, titre);	
}

YAHOO.ownFrame.updateHeight = function(taille) {
	iFrame = document.getElementById('frameEditor');
	oBloc = document.getElementById(YAHOO.ownFrame.divId);
	winH = YAHOO.util.Dom.getViewportHeight();
	winW = YAHOO.util.Dom.getViewportWidth();	
	// Ajout marge inner
	taille += 40;
    oTemplatecover = document.getElementById('cover');
    if(YAHOO.util.Dom.getDocumentHeight() < taille) {
        YAHOO.util.Dom.setStyle(oTemplatecover, "height", (taille+100) + 'px');
    } else {
        YAHOO.util.Dom.setStyle(oTemplatecover, "height", YAHOO.util.Dom.getDocumentHeight() + 'px');
    }
	YAHOO.util.Dom.setStyle(oBloc, "height", (taille+50) + 'px');
    YAHOO.util.Dom.setStyle(iFrame, "height", (taille) + 'px');
	YAHOO.ownFrame.positionUpdate();
	iFrame.focus();
}

YAHOO.ownFrame.positionUpdate = function() {
	clearTimeout(YAHOO.ownFrame.timeScroll)
	oBloc = document.getElementById(YAHOO.ownFrame.divId);
	winH = YAHOO.util.Dom.getViewportHeight();
	winW = YAHOO.util.Dom.getViewportWidth();
	try {
		// Mise a jour positionnement
		eleW = oBloc.offsetWidth;
		eleH = oBloc.offsetHeigh;
		if (winW >= eleW) 
		YAHOO.util.Dom.setX(oBloc, YAHOO.util.Dom.getDocumentScrollLeft() + (winW - eleW) / 2);
		YAHOO.util.Dom.setY(oBloc, YAHOO.util.Dom.getDocumentScrollTop() + (Math.ceil((winH-430)/2)) );
	} catch(ex) {}
}

YAHOO.ownFrame.timeScroll = 0;
YAHOO.ownFrame.moveScroll = function(){
	YAHOO.ownFrame.timeScroll = setTimeout(YAHOO.ownFrame.positionUpdate, 200);
}

YAHOO.ownFrame.close = function() {
	oBloc = document.getElementById(YAHOO.ownFrame.divId);
	if (oBloc) YAHOO.util.Dom.setStyle(oBloc, 'display', 'none');
	/*oBloc.innerHTML = '<div id="popupAffichageVideo">&nbsp;</div>';*/
	oBloc.innerHTML = '';
	YAHOO.ownFrame.hideCover();
	for(i=0;i<document.getElementsByTagName('select').length;i++){
		document.getElementsByTagName('select').item(i).style.visibility='visible';
	}
	for(i=0;i<document.getElementsByTagName('object').length;i++){
		document.getElementsByTagName('object').item(i).style.visibility='visible';
	}

}
YAHOO.ownFrame.showCover = function() {
    var oTemplatecover = document.getElementById('cover');
	if (!oTemplatecover) {
		var oTemplatecover = document.createElement('div');
		oTemplatecover.id = 'cover';
		document.getElementsByTagName('body')[0].appendChild(oTemplatecover);
	}
	oTemplatecover.style.opacity = "0";
	oTemplatecover.style.display = "block";
	oTemplatecover.style.height = YAHOO.util.Dom.getDocumentHeight() + 'px';
	oTemplatecover.style.width = YAHOO.util.Dom.getDocumentWidth() + 'px';
}
YAHOO.ownFrame.hideCover = function() {

	var oTemplatecover = document.getElementById('cover');

	var bloc_disparition = {opacity:{from:1,to:0}};
    var animDisparition = new YAHOO.util.Anim(oTemplatecover, bloc_disparition,.5);
    animDisparition.animate();

	if (oTemplatecover) oTemplatecover.style.display = "none"; 
}




