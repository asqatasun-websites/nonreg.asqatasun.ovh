//------------------------------------------------------------------------------
// FICHIER : $RCSfile: gs_scroll.js,v $
// AUTEUR  : $Author: yvan $
// VERSION : $Revision: 1.6 $
// DATE    : $Date: 2007-09-26 13:40:10 $
//------------------------------------------------------------------------------

/* -----------------------------------------------------------------------------
* File Name: gs_scroll.js
*
* GS_Scroll Class: represents an Object to scroll html information 
* automatically.
* 
* Constructor variables :
*   - h_fen  : visible height of the window <div>  which contains 
*             the marquee <div>.
*
*   - marqId : Identifiant of the marquee div.
*   - fenId  : Identifiant of the window wich contains the information to scroll.
*   - pas    : step of the scrolling. ('0': no scrolling).
*
*   - mode   : if 'alert' display an error message if an error occurs else no 
*             is displayed.
*   - fromtop: The scrolling will begin from the top if fromtop=0
*              The scrolling will begin from 50 px from the top if fromtop=50 
*              The scrolling will begin from the bottom if fromtop='' ...
*
* ------------------------------------------------------------------------------
* How start the scrolling ?
*
* - 1) Include this file in the web page.
*
* - 2) Put two overlap <div> tags in the web page around the html information
*      wich must be scrolling with the two Id property to pass in the GS_Scroll
*      parameters.
*
* - 3) Declare a new GS_Scroll object in the html page where the information
*      to scroll must be displayed. This Object must be in an global variable
*      in the javascript code (i.e: declare out of any javascript functions).
*
* - 4) Build a new function which must :
*      2.1) Init this GS_Scroll object (call init_mrq () method);
*      2.2) lauch an timer wich call the scrollmrq () method of this object.
* 
* Example:
*   {-- JAVASCRIPT SIDE --}
*
*   <script src="gs_scroll.js" type="text/javascript"></script>
*   
*   <script type="text/javascript">
*   // begin from the top
*   var sScroll1 = new GS_Scroll ("100px", "MARQUEE", "FENETRE",    1, '',0);
*   // begin from the bottom
*   var sScroll1 = new GS_Scroll ("100px", "MARQUEE", "FENETRE",    1, '','');
*
*
*   function gsOnload () {
* 
*     sScroll1.init_mrq ();
*     setInterval ("sScroll1.scrollmrq ()", sScroll1.interval);
*   }
* 
*   window.onload=gsOnload;
*   </script>
*
*   {-- HTML SIDE --}
*
*   <div style="padding:5px 5px 5px;text-align:center">
*   <div id="FENETRE" onmouseover="sScroll1.stoc=sScroll1.pas;sScroll1.pas=0;" 
*   onmouseout="sScroll1.pas=sScroll1.stoc;">
*   <div id="MARQUEE">  
*   Code html to scrolling.... 
*   </div>
*   </div>
*   </div>
* 
* Note:
*  To load specific css syle to the two div used by the GS_Scroll Object, all 
*  you need to decalre two style wich the name is : #{divname}.
*  For example, #FENETRE { color:FFFF00 } in the previous example.
*  
*
*-------------------------------------------------------------------------------
*/
first_time = true;
function GS_Scroll (h_fen, marqId, fenId, pas, mode,fromtop) {
  
  this.h_fen    = h_fen;
  this.marqId   = marqId;
  this.fenId    = fenId;
  this.mode     = mode;
		this.fromtop  = fromtop;
  this.stoc     = 0;
  this.pas      = pas;
  this.h_mrq    = 0;
  this.interval = 100;
  this.mrq      = null;
  this.fen      = null;
  this.error    = false;
}

//------------------------------------------------------------------------------
// Init GS_SCroll object.
//------------------------------------------------------------------------------
GS_Scroll.prototype.init_mrq = function () {

  this.mrq = document.getElementById(this.marqId);
  this.fen = document.getElementById(this.fenId);
  
  if (   (this.mrq = document.getElementById(this.marqId))
      && (this.fen = document.getElementById(this.fenId) )) {
    
    
  if (this.mrq.style.display == 'none') {this.mrq.style.display = 'block';}
      
    //this.fen.onmouseover  = function(){this.stoc=this.pas;this.pas=0};
    //this.fen.onmouseout   = function(){this.pas=this.stoc};
    this.fen.style.height = this.h_fen;
    
    with (this.fen.style) {
      position        = "relative";
      overflow        = "hidden";
      verticalAlign   = "top";
      height          = this.h_fen;
    } 
    
    this.h_mrq=this.mrq.offsetHeight;
    
    with(this.mrq.style) {
      position  = "relative";
    }
    
    //setInterval ("this.scrollmrq ()", this.interval);
  
  } else {
    if (this.mode == 'alert') alert ('Error: GS_Scroll::init_mrq failed.');
    this.error = true;
  }

}

//------------------------------------------------------------------------------
// Scroll this GS_SCroll object.
//------------------------------------------------------------------------------
GS_Scroll.prototype.scrollmrq = function () {
  
  if (this.error) return;
		
  if (this.mrq) {
    if ((styleVisibility = this.mrq.style.visibility) && (styleVisibility != 'visible')) {
      this.mrq.style.visibility = 'visible';
				
    }
    
    if ( parseInt(this.mrq.style.top) > -this.h_mrq ) {
			  if (!isNaN(this.fromtop) && this.fromtop<this.h_mrq && first_time){
				
					 this.mrq.style.top = this.fromtop+"px";
					 first_time = false;
					 
				}else this.mrq.style.top = parseInt(this.mrq.style.top)-this.pas+"px";
      
    
    } else {
      this.mrq.style.top=parseInt(this.h_fen)+"px";
    }
    this.first_time = false;
  } else {
    if (this.mode == 'alert') alert ('Error: GS_Scroll nit initialized.');
    this.error = true;
  }
}

//------------------------------------------------------------------------------
// $Log: gs_scroll.js,v $
// Revision 1.6  2007-09-26 13:40:10  yvan
// use the 6th parameter of the constructor "new GS_Scroll "
// in the pattern.html file if you want that scrolling begin from the top
//
// Revision 1.5  2005/07/26 17:53:16  autran
// - Correction [2] on last update on this.mrq.display=none dans initmrq
// - Update the styl.visibility of <marquee> to 'visible' in the scrollmrq function.
//
// NOTE:
// - The goal of this update it's to avoid to the 'marquee' div display without
// overflow hidden while the page is not loaded.
// - In the page which contains the marquee, the style.display of the <marquee> div
// must be equal to 'none', and visibility to 'hidden'.
//
// Revision 1.4  2005/07/26 17:20:16  autran
// - Add a test on this.mrq.display=none dans scrollmrq function.
// (allow to display the div of the marque on the first call by the timer
// of this function).
// NOTE:
// - The goal of this update it's to avoid to the 'marquee' div display without
// overflow hidden while the page is not loaded.
// - In the page which contains the marquee, the style.display of the <marquee> div
// must be equal to 'none'.
//
// Revision 1.3  2005/07/01 15:11:53  autran
// Add revision version information.
//
//-- End of source  ------------------------------------------------------------