
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    
    <!-- meta tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    
    <!-- style -->

    <link rel="stylesheet" href="reset.css" type="text/css" media="all" />
    <link rel="stylesheet" href="type.css" type="text/css" media="all" />
    <link rel="stylesheet" href="style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="register.css" type="text/css" media="all" />
    <link rel="stylesheet" href="changes.css" type="text/css" media="all" />

    <!-- javascript -->
    <script type="text/javascript" src="jquery-1.3.1.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function() {
            
            document.getElementById('siteUrl').value = top.window.location;

            $("input[type=text]").each(function(i) {
                if( this.defVal == null ) {
                    this.defVal = $(this).val();
                }
            });

            $("input[type=text]").focus(function() {
                if( this.defVal == null ) {
                    this.defVal = $(this).val();
                }
                if( this.value == this.defVal ) {
                    this.value = "";
                }
            });
            $("input[type=text]").blur(function() {
                if( this.value == "" ) {
                    this.value = this.defVal;
                }
            });

            $("form").submit(function() {
                $("input[type=text]").each(function(i) {
                    if( this.value == this.defVal ) {
                        this.value = "";
                    }
                });
            });
            
        });
    
    </script>
    
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    
    <title>Rock Werchter :: Envoye</title>
    
</head>
<body id="sendToFriendBody">
    <div id="pnlMessage">
	
        <span id="lblStatus"></span>
    
</div>
    <div id="pnlForm">
	
        <form name="sendToFriend" method="post" action="sendToFriend.aspx" id="sendToFriend">
	<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMjA4NTQyNjY5Mg9kFgICBQ9kFgICAQ9kFgICHw8PFgIeBFRleHQFBkVudm95ZWRkZJK5g4XVJpZ5lMoFnGW4FNjTJMk/" />

	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEWBwKi9J2/DQLEhISFCwKE8/26DALugcLABgLepo2rDALY5LjEBQLVrYuEBo6Znv14x4JEYoca8oBVhRVh0aVk" />	
        <fieldset>
            
            <ol>
                <li>
                    <label>
                        Votre nom:
                        &nbsp;
                    </label>
                    <input name="txtName" type="text" value="Votre nom" id="txtName" class="text" />
                </li>
                <li>
                    <label>
                        Votre adresse e-mail :
                        
                        
                    </label>
                    
                    <input name="txtEmail" type="text" value="Votre adresse e-mail " id="txtEmail" class="text" />
                </li>
                <li>
                    <label>
                        Nom destinataire:
                        
                    </label>
                    <input name="txtFriendName" type="text" value="Nom destinataire" id="txtFriendName" class="text" />
               </li>
                <li>
                    <label>
                        E-mail du destinataire:
                        
                        
                    </label>
                    <input name="txtFriendEmail" type="text" value="E-mail du destinataire" id="txtFriendEmail" class="text" />
                </li>
            </ol>
        </fieldset>
        <fieldset class="submit">
            <input name="siteUrl" type="hidden" id="siteUrl" />
            <input type="submit" name="sumbit" value="Envoye" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;sumbit&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" id="sumbit" />
        </fieldset>
    </form>
    
</div>
    

</body>
</html>