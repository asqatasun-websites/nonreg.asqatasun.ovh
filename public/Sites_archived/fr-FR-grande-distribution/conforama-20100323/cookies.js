// ****
// Lecture d'un cookie
//
// @param name : nom du cookie � lire
// @return la valeur du cookie lu. Si le cookie est absent, ou si les cookies sont d�sactiv�s, renvoie null.
//
// ****
function getCookie(name) {
	var res = null;
	if(document.cookie) {
		var prefix = name + "=";
		var pLength = prefix.length; // longueur pr�fixe
		var cLength = document.cookie.length; // longueur totale
		var pStart = 0; // index d�but pr�fixe
		while (pStart < cLength) {
			var pEnd = pStart + pLength; // index fin pr�fixe
			if (document.cookie.substring(pStart, pEnd) == prefix) {
				var nStart = pEnd;
				var nEnd = document.cookie.indexOf (";", nStart);
				if (nEnd == -1) {
					nEnd = cLength;
				}
				res = unescape(document.cookie.substring(nStart,nEnd));
				break;
			} else {
				pStart = document.cookie.indexOf(" ",pStart)+1;
				if (pStart == 0) {
					break;
				}
			}
		}	
	}
	return res;
}

// ****
// Ecriture d'un cookie
// 
// syntaxe g�n�rale : nom1=valeur1; nom2=valeur2; ... nomN=valeurN;
// 
// @param name [string]: nom du cookie
// @param value : valeur du cookie
// ***
// Param�tres optionnels
// @param expires [date] : date d'expiration du cookie
// @param path [string] : path
// @param domain [string] : domaine
// @param secure [boolean] : 
// ****
function setCookie(name, value) {
	var argv = setCookie.arguments;
	var argc = setCookie.arguments.length;
	var expires = (argc > 2) ? argv[2] : null;
	var path = (argc > 3) ? argv[3] : null;
	var domain = (argc > 4) ? argv[4] : null;
	var secure = (argc > 5) ? argv[5] : false;
	document.cookie = name + "=" + escape(value) +
		((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
		((path == null) ? "" : ("; path=" + path)) +
		((domain == null) ? "" : ("; domain=" + domain)) +
		((secure == true) ? "; secure" : "");
}