/* --------------------- Marqueurs WEBORAMA -----------------*/

/* Compte d'acc�s par banque */
var WRP_ID = new Array();
WRP_ID["Cr�dit du Nord"] = 366985;
WRP_ID["Courtois"] = 366980;
WRP_ID["Kolb"] = 366982;
WRP_ID["Laydernier"] = 366981;
WRP_ID["Nuger"] = 366984;
WRP_ID["Rh�ne-Alpes"] = 366979;
WRP_ID["Tarneaud"] = 366983;
WRP_ID["Autres"] = 366987;

// var WRP_SECTION = 'Particuliers';
var WRP_CHANNEL = '';

/* Profondeur Frame */
var WRP_ACC;


$().ready(function(){	

/* -------------------------------------------------- AFFICHAGE TRANSVERSE-------------------------------------------------------------------*/
	/*Header - TEMPORAIRE*/
	$('div.wlp-bighorn-header > table').hide();
	
	/* Footer*/	
	$('div#footer > span:last-child').removeClass("trait");
	
	/* Home Banque*/	
	$('#institutional_portal_book_19').parent().addClass("bodyhome");
	$('#institutional_portal_book_19').removeClass("marges").addClass("contenuhome");
	$('#institutional_portal_book_19').prev().addClass("no-border");
	$('#institutional_portal_book_19').next().children('div').children('div#footer').addClass("no-border");
			
	/* Ajout de la bordure droite et de la couleur de fond de la premiere colonne du portail transac et du portail instit */			
	//$('body > div > div > div > table > tbody > tr > td:first-child').addClass("td_gauche");
	
	/* Ajout de la bordure et de la couleur de fond de la deuxieme colonne du portail transac et du portail instit */
	//$('td.td_gauche').next('td').addClass("td_centre");
			
	/* Ajout de la bordure gauche et de la couleur de fond de la troisieme colonne du portail transac */			
	$('body > div#transactional_portal_book_19 > div > div > table > tbody > tr > td:nth-child(3)').addClass("td_droite");			
			
	/* Home Espace*/
	$('div#triptyquepart > div#division_horizontale:last-child').hide();
		
	/* Activation du bloc BODY de la page */
	//$('div.wlp-bighorn-book-content').show();
	
	/* Style sp�cifique pour les listbox contenues dans des blocs lat�raux contenant �galement des liens */
	$('div.divliensimple').next('div.divliste').css("margin-top","12px");
	$('div.divliensimple').next('div.divliste').css("margin-left","-6px");
	
	/* Ajout d'une marge uniquement pour les pages V1 */
	$("div[id*='V1_']").css("margin-left","1px");
	$("div[id*='V1_']").css("margin-top","10px");
	
	/* Uniquement pour les pages Onglet, affichage du premier onglet */
	$('div#ordre1 > a').addClass("onglet_on");
	$('div.contentlex.ordre1').show();

/* -------------------------------------------------- AFFICHAGE MENU TRANSAC-------------------------------------------------------------------*/
/* Applique un style particulier au 1er item du menu (pas de border-top) */
	$('.wlp-bighorn-menu-menu-panel ul:first > li:first-child').addClass("item1");		
	
/* -----------------------------Comportement dynamique des items de 1er niveau ---------------------------- */				
	$('.wlp-bighorn-menu-menu-panel > ul > li:not(.wlp-bighorn-menu-active) > a').addClass("arrow-folded"); 
	$('.wlp-bighorn-menu-menu-panel > ul:first > li.wlp-bighorn-menu-active > a').addClass("active-arrow-unfolded"); 
	
	
/* -----------------------------Comportement dynamique des items de second niveau ------------------------- */		
/* Initialisation de l'etat des items actifs de 2nd niveau ayant des sous-menus */
	$('.wlp-bighorn-menu-menu-panel ul:first > li > ul > li.wlp-bighorn-menu-active.sous-menu-parent > a').addClass("sub-arrow-unfolded");	
	
/* Ajout des double fl�ches avant les titres des sous-menus/sous-sous-menus actifs */
	$('.wlp-bighorn-menu-menu-panel > ul > li > ul > li.wlp-bighorn-menu-active > a').addClass("dblfleche_transpa");
	$('.wlp-bighorn-menu-menu-panel > ul > li > ul > li > ul > li.wlp-bighorn-menu-active >  a').addClass("dblfleche_transpa");	
	
/* -------------------------------------------------- AFFICHAGE PAGE PRODUIT -------------------------------------------------------------------*/

/* Inititialisation : le 1er regroupement (de type Classique depliable) d'une fiche produit est deplie */	
		$('div#centrage div.dp2').show();

/*	Ajout d'une bordure haute sur les encarts pliables plies */
		$('div#centrage  div.entete').next('div.dp11').css("border-top","2px solid #C9C9C9");
		$('div#centrage  div.entete').next('div.dp11').next('div.dp12').css("border-top","2px solid #C9C9C9");		
		$('div#centrage  div.entetentp').next('div.dp11').css("border-top","2px solid #C9C9C9");
		$('div#centrage  div.entetentp').next('div.dp11').next('div.dp12').css("border-top","2px solid #C9C9C9");
		$('div#centrage  div.entete').next('div.borderpatr').css("border-top","2px solid #C9C9C9");
		$('div#centrage > div.entetentp').next('div.borderpatr').css("border-top","2px solid #C9C9C9");
		$('div#centrage > div.borderpatr:nth-child(2)').css("border-top","2px solid #C9C9C9");
		$('div#centrage > div.borderpatr:last-child()').css("border-top","2px solid #C9C9C9");
		$('div#centrage div#comment').prev('div.borderpatr').css("border-top","2px solid #C9C9C9");

/* Ajout d'une bordure basse sur les regroupements En Bref deplies lorsqu'ils sont suivis par autre chose qu'un regroupement depliable */		
		$('div.dp2').next('div.borderpatr').css("margin-top","0px");
		$('div.savoir').prev('div.dp2').children('div#bref').css("border-bottom","2px solid #C9C9C9");
		$('div.disclaimer').prev('div.dp2').children('div#bref').css("border-bottom","2px solid #C9C9C9");
		
/* Retrait d'une bordure basse sur les regroupements En Bref non deplies lorsqu'ils sont suivis par autre chose qu'un regroupement depliable */	
		$('div.borderpatr').prev('div.dp2').prev('div.dp1').css("border-bottom","none");
}
);	
