$(document).ready(function($){
 
  $('.lightbox').lightBox({overlayOpacity: 0.8});
  
  $("#annoncesLegalesTexteTitre").hover(function() {
	  $("#annoncesLegalesTexte").animate({height: "150"}, "slow");
	}, function() {
	  $("#annoncesLegalesTexte").animate({height: "30"}, "fast");
	});
  
  $(".popup").click(function(){ 
    window.open($(this).attr("href")); 
    return false; 
	});  

});

