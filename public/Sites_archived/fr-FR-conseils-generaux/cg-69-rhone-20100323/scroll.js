
function gcaScrollBoucle() {

	if (gcaPOS < gcaPOS_MAX) {
	
		gcaPOS=0;
		
	} else if (gcaPOS > gcaPOS_MIN) {
	
		gcaPOS=gcaPOS_MAX;
		
	}
	
}

function gcaScrollMove() {
	
	if ($("gcaContVign")) { 	
	
		$("gcaContVign").style.left=gcaPOS + "px";
		
	}
	
}

function gcaScrollAuto() {

	gcaPOS-=gcaAUTO_D;
	gcaScrollBoucle();
	gcaScrollMove();
	gcaScrollAutoTO=setTimeout("gcaScrollAuto()", gcaAUTO_T);

}

function gcaScrollL() {

	clearTimeout(gcaScrollAutoTO);
	gcaPOS-=gcaD;
	gcaScrollBoucle();
	gcaScrollMove();
	gcaScrollLeftTO=setTimeout("gcaScrollL()", gcaT);
	
}

function gcaScrollR() {

	clearTimeout(gcaScrollAutoTO);
	gcaPOS+=gcaD;
	gcaScrollBoucle();
	gcaScrollMove();
	gcaScrollRightTO=setTimeout("gcaScrollR()", gcaT);

}

function gcaStopScroll() {

	clearTimeout(gcaScrollRightTO);
	clearTimeout(gcaScrollLeftTO);
	clearTimeout(gcaScrollAutoTO); 

}

function gcaMouseOut() {

	gcaStopScroll();
	gcaScrollAuto();

}

function demarrage() {

	
	$("gcaContVign").innerHTML = $("gcaContVign").innerHTML + $("gcaContVign").innerHTML ;
	images = document.getElementsByClassName("gcaImg") ;

	Event.observe('gcaFlecheD', 'mouseover', gcaScrollR) ;
	Event.observe('gcaFlecheG', 'mouseover', gcaScrollL) ;
	Event.observe('gcaFlecheD', 'mouseout', gcaMouseOut) ;
	Event.observe('gcaFlecheG', 'mouseout', gcaMouseOut) ;
	
	Event.observe('stopscroll', 'click', gcaStopScroll) ;
	Event.observe('scrollauto', 'click', gcaScrollAuto) ;
	
	gcaScrollAuto() ;

	for (i=0;i<images.length;i++) {
	
		images[i].onmouseover = function(){gcaStopScroll() ; Element.addClassName(this,"gcaImgZoom");}
		images[i].onmouseout = function(){gcaScrollAuto() ; Element.removeClassName(this,"gcaImgZoom");}
			
	}	

}
