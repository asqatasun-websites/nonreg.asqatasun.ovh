// ***************************************************************************************************************
// ***************************************************************************************************************
// Librairie principale
// ***************************************************************************************************************
// ***************************************************************************************************************
// regroupe toutes les fonctions g�n�rales ainsi que les fonctions d'acc�s aux �l�ments DOM
// ***************************************************************************************************************
// ***************************************************************************************************************
// code pour conserver le codage UTF-8 dans C-g�nie : ���



//---------------------------------------------------------------------------------------
// VARIABLES GLOBALES :
//		@profondeur_recursivite_max (int) : indique la profondeur maximale autoris�e pour la fonction "convertir_objet_en_texte"
//---------------------------------------------------------------------------------------
var profondeur_recursivite_max=10;




//---------------------------------------------------------------------------------------
// NOM FONCTION : copy_array
// DESCRIPTION COURTE :	
// DESCRIPTION :	
//		.
// ENTREES :
//		@[nom_variable] (type) : 
//			STRUCT :
//				[][dflsdflk]
//				  [dqsdqd][]
//			VALEURS :
//				* false : truc
//				* 1 : truc
//				* "toto" : truc
//		VARIABLES GLOBALES :
// NECESSITES :
//		.
// SORTIES :
//		@nom_variable (type) : utilit�
//		VARIABLES GLOBALES :
// VALEURS DE RETOUR :	
//		* 0 : aucune erreur
// AUTEUR : DANIEL Eric
// DERNIERE MODIF : 10/12/2005
// ETAT : dev
// RESTE A FAIRE :
//		*
// REMARQUES :
// VOIR AUSSI :
//---------------------------------------------------------------------------------------
function copy_array( source )
{
	// --------------
	// initialisation
	// --------------
	var destination = new Array();


	// ------------------------------------
	// v�rification des param�tres d'entr�e
	// ------------------------------------


	// -----------
	// traitements
	// -----------
	for( var indice in source )
		if( ( typeof( source[indice] ) == "object" ) || ( typeof( source[indice] ) == "array" ) )
			destination[indice] = copy_array( source[indice] );
		else
			destination[indice] = source[indice];

	return destination;


	// ---------------------
	// sortie de la fonction
	// ---------------------
	// 0 : aucune erreur
	return 0;
}




//---------------------------------------------------------------------------------------
// NOM FONCTION : supprimer_eventbubble
// DESCRIPTION COURTE :	
// DESCRIPTION :	
//		.
// ENTREES :
//		@[nom_variable] (type) : 
//			STRUCT :
//				[][dflsdflk]
//				  [dqsdqd][]
//			VALEURS :
//				* false : truc
//				* 1 : truc
//				* "toto" : truc
//		VARIABLES GLOBALES :
// NECESSITES :
//		.
// SORTIES :
//		@nom_variable (type) : utilit�
//		VARIABLES GLOBALES :
// VALEURS DE RETOUR :	
//		* 0 : aucune erreur
// AUTEUR : DANIEL Eric
// DERNIERE MODIF : 10/12/2005
// ETAT : dev
// RESTE A FAIRE :
//		*
// REMARQUES :
// VOIR AUSSI :
//---------------------------------------------------------------------------------------
function supprimer_eventbubble( event )
{
	// --------------
	// initialisation
	// --------------


	// ------------------------------------
	// v�rification des param�tres d'entr�e
	// ------------------------------------


	// -----------
	// traitements
	// -----------
	if( typeof( window.event ) != 'undefined' )
		window.event.cancelBubble = true;
	else
		event.cancelBubble = true; 


	// ---------------------
	// sortie de la fonction
	// ---------------------
	// 0 : aucune erreur
	return 0;
}




if( typeof( HTMLElement ) != "undefined" )
	{
	HTMLElement.prototype.__defineSetter__("innerText", function (sText_old) {
		var sText = new String( sText_old );
		this.innerHTML = sText.replace(/\&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
	});
	HTMLElement.prototype.__defineGetter__("innerText", function () {
		var r = this.ownerDocument.createRange();
		r.selectNodeContents(this);
		return r.toString();
	});

	HTMLElement.prototype.__defineGetter__("outerHTML", function () {
		var _emptyTags = {
			"IMG":   true,
			"BR":    true,
			"INPUT": true,
			"META":  true,
			"LINK":  true,
			"PARAM": true,
			"HR":    true
			};
		var attrs = this.attributes;
		var str = "<" + this.tagName;
		for (var i = 0; i < attrs.length; i++)
			str += " " + attrs[i].name + "=\"" + attrs[i].value + "\"";

		if (_emptyTags[this.tagName])
			return str + ">";

		return str + ">" + this.innerHTML + "</" + this.tagName + ">";
	});
	HTMLElement.prototype.__defineSetter__("outerHTML", function (sHTML) {
		var r = this.ownerDocument.createRange();
		r.setStartBefore(this);
		var df = r.createContextualFragment(sHTML);
		this.parentNode.replaceChild(df, this);
	});
}




//---------------------------------------------------------------------------------------
// NOM FONCTION : xreplace
// DESCRIPTION COURTE :	
// DESCRIPTION :	
//		.
// ENTREES :
//		@[nom_variable] (type) : 
//			STRUCT :
//				[][dflsdflk]
//				  [dqsdqd][]
//			VALEURS :
//				* false : truc
//				* 1 : truc
//				* "toto" : truc
//		VARIABLES GLOBALES :
// NECESSITES :
//		.
// SORTIES :
//		@nom_variable (type) : utilit�
//		VARIABLES GLOBALES :
// VALEURS DE RETOUR :	
//		* 0 : aucune erreur
// AUTEUR : DANIEL Eric
// DERNIERE MODIF : 10/12/2005
// ETAT : dev
// RESTE A FAIRE :
//		*
// REMARQUES :
// VOIR AUSSI :
//---------------------------------------------------------------------------------------
function xreplace( chaine_a_traiter, a_remplacer, remplacer_par )
{
	// --------------
	// initialisation
	// --------------
	var chaine_finale = "";
	var deb_capture = 0;
	var fin_capture = 0;


	// ------------------------------------
	// v�rification des param�tres d'entr�e
	// ------------------------------------


	// -----------
	// traitements
	// -----------
	fin_capture = chaine_a_traiter.indexOf( a_remplacer );
	
	while( fin_capture > -1 )
		{
		chaine_finale += chaine_a_traiter.substring( deb_capture, fin_capture );
		chaine_finale += remplacer_par;
		deb_capture = fin_capture + a_remplacer.length
		fin_capture = chaine_a_traiter.indexOf( a_remplacer, deb_capture );
		}
	chaine_finale += chaine_a_traiter.substring( deb_capture );
	
	return chaine_finale + "";


	// ---------------------
	// sortie de la fonction
	// ---------------------
	// 0 : aucune erreur
	return 0;
}




//---------------------------------------------------------------------------------------
// NOM FONCTION : hasAttribute
// DESCRIPTION COURTE :	
// DESCRIPTION :	
//		.
// ENTREES :
//		@[nom_variable] (type) : 
//			STRUCT :
//				[][dflsdflk]
//				  [dqsdqd][]
//			VALEURS :
//				* false : truc
//				* 1 : truc
//				* "toto" : truc
//		VARIABLES GLOBALES :
// NECESSITES :
//		.
// SORTIES :
//		@nom_variable (type) : utilit�
//		VARIABLES GLOBALES :
// VALEURS DE RETOUR :	
//		* 0 : aucune erreur
// AUTEUR : DANIEL Eric
// DERNIERE MODIF : 10/12/2005
// ETAT : dev
// RESTE A FAIRE :
//		*
// REMARQUES :
// VOIR AUSSI :
//---------------------------------------------------------------------------------------
function hasAttribute( objet, nom_attribut )
{
	// --------------
	// initialisation
	// --------------
	var retour = false;


	// ------------------------------------
	// v�rification des param�tres d'entr�e
	// ------------------------------------
	if( document.all )
		{
		if( nom_attribut == "class" )
			nom_attribut = "className";
		}
	else if( nom_attribut == "className" )
		nom_attribut = "class";


	// -----------
	// traitements
	// -----------
	try // code compatible DOM
		{
		retour = objet.hasAttribute( nom_attribut );
		}
	catch( ex ) // patch fait pour IE
		{
		if( typeof( objet[nom_attribut] ) != "undefined" )
			retour = true;
		else
			retour = false;
		}
	return retour;


	// ---------------------
	// sortie de la fonction
	// ---------------------
	// 0 : aucune erreur
	return 0;
}




//---------------------------------------------------------------------------------------
// NOM FONCTION : getAttribute
// DESCRIPTION COURTE :	
// DESCRIPTION :	
//		.
// ENTREES :
//		@[nom_variable] (type) : 
//			STRUCT :
//				[][dflsdflk]
//				  [dqsdqd][]
//			VALEURS :
//				* false : truc
//				* 1 : truc
//				* "toto" : truc
//		VARIABLES GLOBALES :
// NECESSITES :
//		.
// SORTIES :
//		@nom_variable (type) : utilit�
//		VARIABLES GLOBALES :
// VALEURS DE RETOUR :	
//		* 0 : aucune erreur
// AUTEUR : DANIEL Eric
// DERNIERE MODIF : 10/12/2005
// ETAT : dev
// RESTE A FAIRE :
//		*
// REMARQUES :
// VOIR AUSSI :
//---------------------------------------------------------------------------------------
function getAttribute( objet, nom_attribut )
{
	// --------------
	// initialisation
	// --------------
	var retour = null;


	// ------------------------------------
	// v�rification des param�tres d'entr�e
	// ------------------------------------
	if( document.all )
		{
		if( nom_attribut == "class" )
			nom_attribut = "className";
		}
	else if( nom_attribut == "className" )
		nom_attribut = "class";


	// -----------
	// traitements
	// -----------
	try
		{
		retour = objet.getAttribute( nom_attribut );
		}
	catch( ex )
		{
		if( typeof( objet[nom_attribut] ) != "undefined" )
			retour = objet[nom_attribut];
		else
			retour = null;
		}
	return retour;


	// ---------------------
	// sortie de la fonction
	// ---------------------
	// 0 : aucune erreur
	return 0;
}




//---------------------------------------------------------------------------------------
// NOM FONCTION : setAttribute
// DESCRIPTION COURTE :	
// DESCRIPTION :	
//		.
// ENTREES :
//		@[nom_variable] (type) : 
//			STRUCT :
//				[][dflsdflk]
//				  [dqsdqd][]
//			VALEURS :
//				* false : truc
//				* 1 : truc
//				* "toto" : truc
//		VARIABLES GLOBALES :
// NECESSITES :
//		.
// SORTIES :
//		@nom_variable (type) : utilit�
//		VARIABLES GLOBALES :
// VALEURS DE RETOUR :	
//		* 0 : aucune erreur
// AUTEUR : DANIEL Eric
// DERNIERE MODIF : 10/12/2005
// ETAT : dev
// RESTE A FAIRE :
//		*
// REMARQUES :
// VOIR AUSSI :
//---------------------------------------------------------------------------------------
function setAttribute( objet, nom_attribut, valeur )
{
	// --------------
	// initialisation
	// --------------


	// ------------------------------------
	// v�rification des param�tres d'entr�e
	// ------------------------------------
	if( document.all )
		{
		if( nom_attribut == "class" )
			nom_attribut = "className";
		}
	else if( nom_attribut == "className" )
		nom_attribut = "class";


	// -----------
	// traitements
	// -----------
	try
		{
		objet.setAttribute( nom_attribut, valeur );
		}
	catch( ex )
		{
		objet[nom_attribut] = valeur;
		}
	return true;


	// ---------------------
	// sortie de la fonction
	// ---------------------
	// 0 : aucune erreur
	return 0;
}




//---------------------------------------------------------------------------------------
// NOM FONCTION : removeAttribute
// DESCRIPTION COURTE :	
// DESCRIPTION :	
//		.
// ENTREES :
//		@[nom_variable] (type) : 
//			STRUCT :
//				[][dflsdflk]
//				  [dqsdqd][]
//			VALEURS :
//				* false : truc
//				* 1 : truc
//				* "toto" : truc
//		VARIABLES GLOBALES :
// NECESSITES :
//		.
// SORTIES :
//		@nom_variable (type) : utilit�
//		VARIABLES GLOBALES :
// VALEURS DE RETOUR :	
//		* 0 : aucune erreur
// AUTEUR : DANIEL Eric
// DERNIERE MODIF : 10/12/2005
// ETAT : dev
// RESTE A FAIRE :
//		*
// REMARQUES :
// VOIR AUSSI :
//---------------------------------------------------------------------------------------
function removeAttribute( objet, nom_attribut )
{
	// --------------
	// initialisation
	// --------------


	// ------------------------------------
	// v�rification des param�tres d'entr�e
	// ------------------------------------
	if( document.all )
		{
		if( nom_attribut == "class" )
			nom_attribut = "className";
		}
	else if( nom_attribut == "className" )
		nom_attribut = "class";


	// -----------
	// traitements
	// -----------
	try
		{
		objet.removeAttribute( nom_attribut );
		}
	catch( ex )
		{
		
		}


	// ---------------------
	// sortie de la fonction
	// ---------------------
	// 0 : aucune erreur
	return 0;
}




//---------------------------------------------------------------------------------------
// NOM FONCTION : getElementsByAttribute
// DESCRIPTION COURTE :	
// DESCRIPTION :	
//		.
// ENTREES :
//		@[nom_variable] (type) : 
//			STRUCT :
//				[][dflsdflk]
//				  [dqsdqd][]
//			VALEURS :
//				* false : truc
//				* 1 : truc
//				* "toto" : truc
//		VARIABLES GLOBALES :
// NECESSITES :
//		.
// SORTIES :
//		@nom_variable (type) : utilit�
//		VARIABLES GLOBALES :
// VALEURS DE RETOUR :	
//		* 0 : aucune erreur
// AUTEUR : DANIEL Eric
// DERNIERE MODIF : 10/12/2005
// ETAT : dev
// RESTE A FAIRE :
//		*
// REMARQUES :
// VOIR AUSSI :
//---------------------------------------------------------------------------------------
function getElementsByAttribute( nom_attribut, valeur )
{
	// --------------
	// initialisation
	// --------------
	var tabRetour = new Array();
	var tabTmp = new Array();


	// ------------------------------------
	// v�rification des param�tres d'entr�e
	// ------------------------------------


	// -----------
	// traitements
	// -----------
	tabTmp = document.body.getElementsByTagName( "*" );
	j=0;
	for (i=0; i<tabTmp.length; i++)
		if( hasAttribute( tabTmp[i], nom_attribut ) )
			if( ( typeof( valeur ) == "undefined" )
				|| ( getAttribute( tabTmp[i], nom_attribut ) == valeur ) ) 
				{
				tabRetour[j] = tabTmp[i];
				j++;
				}

	return tabRetour; 


	// ---------------------
	// sortie de la fonction
	// ---------------------
	// 0 : aucune erreur
	return 0;
}




//---------------------------------------------------------------------------------------
// NOM FONCTION : ajouter_evenement
// DESCRIPTION COURTE :	
// DESCRIPTION :	
//		.
// ENTREES :
//		@[nom_variable] (type) : 
//			STRUCT :
//				[][dflsdflk]
//				  [dqsdqd][]
//			VALEURS :
//				* false : truc
//				* 1 : truc
//				* "toto" : truc
//		VARIABLES GLOBALES :
// NECESSITES :
//		.
// SORTIES :
//		@nom_variable (type) : utilit�
//		VARIABLES GLOBALES :
// VALEURS DE RETOUR :	
//		* 0 : aucune erreur
// AUTEUR : DANIEL Eric
// DERNIERE MODIF : 10/12/2005
// ETAT : dev
// RESTE A FAIRE :
//		*
// REMARQUES :
// VOIR AUSSI :
//---------------------------------------------------------------------------------------
function ajouter_evenement( objet, evenement, fonction )
{
	// --------------
	// initialisation
	// --------------


	// ------------------------------------
	// v�rification des param�tres d'entr�e
	// ------------------------------------


	// -----------
	// traitements
	// -----------
	if( document.all )
		objet.attachEvent( 'on' + evenement, fonction );
	else
		objet.addEventListener( evenement, fonction, false );


	// ---------------------
	// sortie de la fonction
	// ---------------------
	// 0 : aucune erreur
	return 0;
}




// Parcourt un objet ou un tableau et le transforme en une chaine de texte pr�sent�e (= print_r en PHP)
// G�re 
//		- les tableaux imbriqu�s
// 		- les types inconnus
// 		- un niveau de profondeur d'exploration maximum r�gl� en variable globale
// 		- les objets vides
//---------------------------------------------------------------------------------------
// NOM FONCTION : convertir_objet_en_texte
// DESCRIPTION COURTE :	
// DESCRIPTION :	
//		.
// ENTREES :
//		@[nom_variable] (type) : 
//			STRUCT :
//				[][dflsdflk]
//				  [dqsdqd][]
//			VALEURS :
//				* false : truc
//				* 1 : truc
//				* "toto" : truc
//		VARIABLES GLOBALES :
// NECESSITES :
//		.
// SORTIES :
//		@nom_variable (type) : utilit�
//		VARIABLES GLOBALES :
// VALEURS DE RETOUR :	
//		* 0 : aucune erreur
// AUTEUR : DANIEL Eric
// DERNIERE MODIF : 10/12/2005
// ETAT : dev
// RESTE A FAIRE :
//		*
// REMARQUES :
// VOIR AUSSI :
//---------------------------------------------------------------------------------------
function convertir_objet_en_texte( objet, profondeur_recursivite )
{
	// --------------
	// initialisation
	// --------------
	var texte;
	var texte_retour = "";
	var objet_vide = true;


	// ------------------------------------
	// v�rification des param�tres d'entr�e
	// ------------------------------------


	// -----------
	// traitements
	// -----------
	if( typeof( profondeur_recursivite ) != "number" ) // rend le param�tre optionnel
		profondeur_recursivite = 0;
		
	if( typeof( objet ) == "object" )
		texte = "Objet :";
	else
		texte = "Tableau :";
		
	for( var indice in objet )
		{
		objet_vide = false;
		if( ( typeof( objet[indice] ) == "object" ) || ( typeof( objet[indice] ) == "array" ) )
			{
			texte_retour = "";
			if( profondeur_recursivite < profondeur_recursivite_max )
				{
				texte_retour = convertir_objet_en_texte( objet[indice], profondeur_recursivite + 1 );
				texte_retour = xreplace( texte_retour, "\r\n", "\r\n\t\t" );
				}
			else
				texte_retour = "Objet : -- PROFONDEUR MAX (" + profondeur_recursivite + ") DEPASSEE -> exploration non effectu�e. --";
			}
		else
			{
			if( typeof( objet[indice] ) == "unknown" )
				texte_retour = "-- TYPE INCONNU -- ";
			else
				texte_retour = objet[indice];
			}
		texte = texte + "\r\n\t\t[" + indice + "] => " + texte_retour;
		}
	
	if( objet_vide == true )
		texte = texte + " -- VIDE -- ";
			
	return texte;

	// ---------------------
	// sortie de la fonction
	// ---------------------
	// 0 : aucune erreur
	return 0;
}




//---------------------------------------------------------------------------------------
// NOM FONCTION : debug
// DESCRIPTION COURTE :	
// DESCRIPTION :	
//		.
// ENTREES :
//		@[nom_variable] (type) : 
//			STRUCT :
//				[][dflsdflk]
//				  [dqsdqd][]
//			VALEURS :
//				* false : truc
//				* 1 : truc
//				* "toto" : truc
//		VARIABLES GLOBALES :
// NECESSITES :
//		.
// SORTIES :
//		@nom_variable (type) : utilit�
//		VARIABLES GLOBALES :
// VALEURS DE RETOUR :	
//		* 0 : aucune erreur
// AUTEUR : DANIEL Eric
// DERNIERE MODIF : 10/12/2005
// ETAT : dev
// RESTE A FAIRE :
//		*
// REMARQUES :
// VOIR AUSSI :
//---------------------------------------------------------------------------------------
function debug( variable )
{
	// --------------
	// initialisation
	// --------------
	var texte = "";


	// ------------------------------------
	// v�rification des param�tres d'entr�e
	// ------------------------------------


	// -----------
	// traitements
	// -----------
	var type = typeof( variable );
	if( ( type == "object" ) || ( type == "array" ) )
		texte = convertir_objet_en_texte( variable );
	else
		texte = variable;

	if( document.getElementById( "debug" ) == null )
		{
		var mon_div = document.createElement( "DIV" );
		mon_div.className = "debug";
		mon_div.id = "debug";
		document.body.appendChild( mon_div );
		document.getElementById( "debug" ).innerText = "Zone de Debug\r\n";
		}
	document.getElementById( "debug" ).innerText += "\r\n" + texte;


	// ---------------------
	// sortie de la fonction
	// ---------------------
	// 0 : aucune erreur
	return 0;
}




//---------------------------------------------------------------------------------------
// NOM FONCTION : FormatNumber2
// DESCRIPTION COURTE :	
// DESCRIPTION :	
//		.
// ENTREES :
//		@num (float) : nombre � formater
//		@decimalNum (int) : nombre de chiffres apr�s la virgule
//		@bolLeadingZero (boolean) : indique s'il faut laisser le z�ro pour les nombres entre -1 exclus et 1 exclus
//		@bolParens (boolean) : indique s'il faut mettre des parenth�ses autour des nombres n�gatifs
//		@bolCommas (boolean) : indique s'il faut mettre des "." comme s�parateur de milliers
// NECESSITES :
// SORTIES :
// VALEURS DE RETOUR :	
//		* (string) : nombre format�
// AUTEUR : INCONNU
// DERNIERE MODIF : 10/12/2005
// ETAT : ok
// RESTE A FAIRE :
//		*
// REMARQUES :
// VOIR AUSSI :
//---------------------------------------------------------------------------------------
function FormatNumber2( num, decimalNum, bolLeadingZero, bolParens, bolCommas )
{
	// --------------
	// initialisation
	// --------------
	var tmpNum = num;
	var iSign = num < 0 ? -1 : 1;


	// ------------------------------------
	// v�rification des param�tres d'entr�e
	// ------------------------------------
	if( isNaN( parseInt( num ) ) )
		return "NaN";


	// -----------
	// traitements
	// -----------
	// on tronque le nombre pour avoir strictement le nombre de chiffres apr�s la virgule demand�e
	tmpNum *= Math.pow(10,decimalNum);
	tmpNum = Math.round(Math.abs(tmpNum))
	tmpNum /= Math.pow(10,decimalNum);
	// on remet le signe
	tmpNum *= iSign;
	
	
	// on cr�e une chaine de caract�res avec le nombre
	var tmpNumStr = new String(tmpNum);

	// gestion de la pr�sence du z�ro pour les nombres entre -1 et 1
	if (!bolLeadingZero && num < 1 && num > -1 && num != 0)
		if (num > 0)
			tmpNumStr = tmpNumStr.substring(1,tmpNumStr.length);
		else
			tmpNumStr = "-" + tmpNumStr.substring(2,tmpNumStr.length);

	// remplacement du s�parateur d�cimal "." par ","
	tmpNumStr = xreplace( tmpNumStr, ".", "," );

	// remplir apr�s la virgule pour obtenir le nombre de chiffres voulus m�me avec un nombre comportant moins de pr�cision
	var emplacement_du_point = tmpNumStr.indexOf(",");
	if( emplacement_du_point < 0 )
		{
		emplacement_du_point = tmpNumStr.length;
		tmpNumStr = tmpNumStr + ",";
		}
	for( var i=tmpNumStr.length-emplacement_du_point-1 ; i<decimalNum ; i++ )
		tmpNumStr = tmpNumStr + "0";

	// ajout du s�parateur de milliers
	if (bolCommas && (num >= 1000 || num <= -1000)) {
		var iStart = tmpNumStr.indexOf(",");
		if (iStart < 0)
			iStart = tmpNumStr.length;

		iStart -= 3;
		while (iStart >= 1) {
			tmpNumStr = tmpNumStr.substring(0,iStart) + "." + tmpNumStr.substring(iStart,tmpNumStr.length)
			iStart -= 3;
		}		
	}

	// ajout des parenth�ses sur les nombres n�gatifs
	if (bolParens && num < 0)
		tmpNumStr = "(" + tmpNumStr.substring(1,tmpNumStr.length) + ")";

	return tmpNumStr;


	// ---------------------
	// sortie de la fonction
	// ---------------------
	// 0 : aucune erreur
	return 0;
}




//---------------------------------------------------------------------------------------
// NOM FONCTION : fermer_fenetre
// DESCRIPTION COURTE :	
// DESCRIPTION :	
//		.
// ENTREES :
//		@[nom_variable] (type) : 
//			STRUCT :
//				[][dflsdflk]
//				  [dqsdqd][]
//			VALEURS :
//				* false : truc
//				* 1 : truc
//				* "toto" : truc
//		VARIABLES GLOBALES :
// NECESSITES :
//		.
// SORTIES :
//		@nom_variable (type) : utilit�
//		VARIABLES GLOBALES :
// VALEURS DE RETOUR :	
//		* 0 : aucune erreur
// AUTEUR : DANIEL Eric
// DERNIERE MODIF : 17/12/2005
// ETAT : dev
// RESTE A FAIRE :
//		*
// REMARQUES :
// VOIR AUSSI :
//---------------------------------------------------------------------------------------
function fermer_fenetre(  )
{
	// --------------
	// initialisation
	// --------------


	// ------------------------------------
	// v�rification des param�tres d'entr�e
	// ------------------------------------


	// -----------
	// traitements
	// -----------
	top.opener=self;
	top.close();


	// ---------------------
	// sortie de la fonction
	// ---------------------
	// 0 : aucune erreur
	return 0;
}





//---------------------------------------------------------------------------------------
// NOM FONCTION : champ_ecrire_valeur
// DESCRIPTION COURTE :	met � jour la valeur d'un �l�ment de formulaire
// DESCRIPTION :	
//		met � jour la valeur d'un �l�ment de formulaire
// ENTREES :
//		@objet (objet) : pointeur sur l'objet dont il faut mettre � jour le contenu
//		@valeur (mixed) : nouveau contenu de l'�l�ment de formulaire
// NECESSITES :
//		l'objet doit �tre un �l�ment de formulaire.
// SORTIES :
// VALEURS DE RETOUR :	
//		* 0 : aucune erreur
//		* null : impossible d'�crire la valeur
// AUTEUR : DANIEL Eric
// DERNIERE MODIF : 09/12/2005
// ETAT : ok
// RESTE A FAIRE :
//		* traiter les listes d�roulantes � choix multiples
// REMARQUES :
// VOIR AUSSI : champ_lire_valeur champ_verifier_modification champ_restaurer_ancienne_valeur champ_memoriser_valeur champ_preparer preparer_tous_les_champs
//---------------------------------------------------------------------------------------
function champ_ecrire_valeur( objet, valeur )
{
	// --------------
	// initialisation
	// --------------
	var liste_elements = null;


	// ------------------------------------
	// v�rification des param�tres d'entr�e
	// ------------------------------------
	// v�rification du param�tre objet
	if( objet == null )
		// null : impossible de lire la valeur
		return null;


	// -----------
	// traitements
	// -----------
	if( objet.tagName == "SELECT" )
		{
		if( objet.options.length == 0 )
			{
			objet.selectedIndex = -1;
			return;
			}

		for( var i=0 ; i<objet.options.length ; i++ )
			if( objet.options[i].value == valeur )
				objet.selectedIndex = i;

		if( objet.options[objet.selectedIndex].value != valeur )
			objet.selectedIndex = -1;
		}

	if( objet.tagName == "TEXTAREA" )
		objet.value = valeur;

	if( objet.tagName == "INPUT" )
		{
		if( ( objet.type == "text" )
			|| ( objet.type == "password" )
			|| ( objet.type == "hidden" )
			|| ( objet.type == "button" ) )
			objet.value = valeur;

		if( ( objet.type == "radio" ) || ( objet.type == "checkbox" ) )
			{
			liste_elements = getElementsByAttribute( "name", objet.name );
			for( num_element=0 ; num_element<liste_elements.length ; num_element++ )
				if( liste_elements[num_element].value == valeur )
					liste_elements[num_element].checked = true;
				else
					liste_elements[num_element].checked = false;
//			champ_verifier_modification( objet );
//			objet.checked = valeur;
			}

		if( objet.type == "file" )
			{
			var parent = objet.parentNode;
			parent.innerHTML = parent.innerHTML;
			objet = parent.firstChild;
			}
		}

	//champ_verifier_modification( objet );		


	// ---------------------
	// sortie de la fonction
	// ---------------------
	// 0 : aucune erreur
	return 0;
}


function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];} }
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


function MM_openBrWindow(theURL,winName,features) {
	if (winName=='pdfreader')
		{
		var cheminSansNomFichier = 'http:';
		var listeRepertoire = window.location.href.split("/");

		for (i=1; i<listeRepertoire.length-1; i++)
			{ cheminSansNomFichier = cheminSansNomFichier+'/'+listeRepertoire[i]; }

		if (window.location.hostname.lastIndexOf("laetis.loc") == -1)
			{
			chemin = 'http://'+listeRepertoire[2]+'/';
			win=window.open(chemin+'includes/plugins/pdf/framepdf.php?pdfPage='+cheminSansNomFichier+'/'+theURL,winName,'scrollbars=yes,resizable=yes');
			}
		else
			{
			chemin = 'http://'+listeRepertoire[2]+'/'+listeRepertoire[3]+'/';
			win=window.open(chemin+'includes/plugins/pdf/framepdf.php?pdfPage='+cheminSansNomFichier+'/'+theURL,winName,'scrollbars=yes,resizable=yes');
			}
		}
	else
		{
		win=window.open(theURL,winName,features);
		}
	win.opener = self;
	win.focus();			
}







function element_select__trier( nom_liste )
{
	var liste1 = new Array();
	var liste2 = new Array();

	for( var i=0 ; i<$( nom_liste ).options.length ; i++ )
		liste1[i] = { value: $( nom_liste ).options[i].value, text: $( nom_liste ).options[i].text };

	liste2 = $A( liste1 ).sortBy( function( valeur, indice )
		{
		if( valeur["value"] == "" )
			return "";
		return valeur["text"];
		});


	element_select__vider( nom_liste );

	for( var i=0 ; i<liste2.length ; i++ )
		element_select__ajouter_item( nom_liste, liste2[i]["text"], liste2[i]["value"] );
}

function element_select__vider( nom_liste )
{
	for( i=$(nom_liste).options.length-1 ; i>=0 ; i-- )
		$(nom_liste).remove( i );
	$(nom_liste).selectedIndex = 0;
}

function element_select__ajouter_item( nom_liste, texte, valeur )
{
	var mon_option = new Option( "", valeur ); 

	try
		{
		$( nom_liste ).add( mon_option, null ); // ne fonctionne pas sous IE
		}
	catch(ex)
		{
		$( nom_liste ).add( mon_option ); // fonctionne sous IE seulement
		}
	$( nom_liste ).options[$( nom_liste ).options.length-1].innerHTML = texte;
}


function lib_general__convertir_dec_hex( valeur )
{
	var reste = 0;
	var chaine = "";
	var lettres = new Array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G");

	while( valeur>15 )
		{
		reste = valeur - 16*Math.floor( valeur / 16 );
		valeur = Math.floor( valeur / 16 );
		chaine = lettres[reste] + chaine;
		}
	chaine = lettres[valeur] + chaine;

	return chaine;
}

function lib_general__forcer_a_quatre_caracteres( chaine )
{
	for( var i=chaine.length ; i<4 ; i++ )
		chaine = "0" + chaine;
	return chaine;
}

function lib_general__convertir_unicode_vers_js( mon_texte )
{
	var mon_texte_old = "";

	while( mon_texte_old != mon_texte )
		{
		mon_texte_old = mon_texte;

		mon_texte = mon_texte.replace( /&#x([0-9a-f][0-9a-f][0-9a-f][0-9a-f]);/, "\\u$1");
		mon_texte = mon_texte.replace( /&#x([0-9a-f][0-9a-f][0-9a-f]);/, "\\u0$1");
		mon_texte = mon_texte.replace( /&#x([0-9a-f][0-9a-f]);/, "\\u00$1");
		mon_texte = mon_texte.replace( /&#x([0-9a-f]);/, "\\u000$1");
		

		var tempo = {"nbsp":160,"iexcl":161,"cent":162,"pound":163,"curren":164,"yen":165,"brvbar":166,"sect":167,"uml":168,"copy":169,"ordf":170,"laquo":171,"not":172,"shy":173,"reg":174,"macr":175,"deg":176,"plusmn":177,"sup2":178,"sup3":179,"acute":180,"micro":181,"para":182,"middot":183,"cedil":184,"sup1":185,"ordm":186,"raquo":187,"frac14":188,"frac12":189,"frac34":190,"iquest":191,"Agrave":192,"Aacute":193,"Acirc":194,"Atilde":195,"Auml":196,"Aring":197,"AElig":198,"Ccedil":199,"Egrave":200,"Eacute":201,"Ecirc":202,"Euml":203,"Igrave":204,"Iacute":205,"Icirc":206,"Iuml":207,"ETH":208,"Ntilde":209,"Ograve":210,"Oacute":211,"Ocirc":212,"Otilde":213,"Ouml":214,"times":215,"Oslash":216,"Ugrave":217,"Uacute":218,"Ucirc":219,"Uuml":220,"Yacute":221,"THORN":222,"szlig":223,"agrave":224,"aacute":225,"acirc":226,"atilde":227,"auml":228,"aring":229,"aelig":230,"ccedil":231,"egrave":232,"eacute":233,"ecirc":234,"euml":235,"igrave":236,"iacute":237,"icirc":238,"iuml":239,"eth":240,"ntilde":241,"ograve":242,"oacute":243,"ocirc":244,"otilde":245,"ouml":246,"divide":247,"oslash":248,"ugrave":249,"uacute":250,"ucirc":251,"uuml":252,"yacute":253,"thorn":254,"yuml":255,"fnof":402,"Alpha":913,"Beta":914,"Gamma":915,"Delta":916,"Epsilon":917,"Zeta":918,"Eta":919,"Theta":920,"Iota":921,"Kappa":922,"Lambda":923,"Mu":924,"Nu":925,"Xi":926,"Omicron":927,"Pi":928,"Rho":929,"Sigma":931,"Tau":932,"Upsilon":933,"Phi":934,"Chi":935,"Psi":936,"Omega":937,"alpha":945,"beta":946,"gamma":947,"delta":948,"epsilon":949,"zeta":950,"eta":951,"theta":952,"iota":953,"kappa":954,"lambda":955,"mu":956,"nu":957,"xi":958,"omicron":959,"pi":960,"rho":961,"sigmaf":962,"sigma":963,"tau":964,"upsilon":965,"phi":966,"chi":967,"psi":968,"omega":969,"thetasym":977,"upsih":978,"piv":982,"bull":8226,"hellip":8230,"prime":8242,"Prime":8243,"oline":8254,"frasl":8260,"weierp":8472,"image":8465,"real":8476,"trade":8482,"alefsym":8501,"larr":8592,"uarr":8593,"rarr":8594,"darr":8595,"harr":8596,"crarr":8629,"lArr":8656,"uArr":8657,"rArr":8658,"dArr":8659,"hArr":8660,"forall":8704,"part":8706,"exist":8707,"empty":8709,"nabla":8711,"isin":8712,"notin":8713,"ni":8715,"prod":8719,"sum":8721,"minus":8722,"lowast":8727,"radic":8730,"prop":8733,"infin":8734,"ang":8736,"and":8743,"or":8744,"cap":8745,"cup":8746,"int":8747,"there4":8756,"sim":8764,"cong":8773,"asymp":8776,"ne":8800,"equiv":8801,"le":8804,"ge":8805,"sub":8834,"sup":8835,"nsub":8836,"sube":8838,"supe":8839,"oplus":8853,"otimes":8855,"perp":8869,"sdot":8901,"lceil":8968,"rceil":8969,"lfloor":8970,"rfloor":8971,"lang":9001,"rang":9002,"loz":9674,"spades":9824,"clubs":9827,"hearts":9829,"diams":9830,"quot":34,"amp":38,"lt":60,"gt":62,"OElig":338,"oelig":339,"Scaron":352,"scaron":353,"Yuml":376,"circ":710,"tilde":732,"ensp":8194,"emsp":8195,"thinsp":8201,"zwnj":8204,"zwj":8205,"lrm":8206,"rlm":8207,"ndash":8211,"mdash":8212,"lsquo":8216,"rsquo":8217,"sbquo":8218,"ldquo":8220,"rdquo":8221,"bdquo":8222,"dagger":8224,"Dagger":8225,"permil":8240,"lsaquo":8249,"rsaquo":8250,"euro":8364};
		for( var caractere in tempo )
			mon_texte = mon_texte.replace( "&" + caractere + ";", "\\u" + lib_general__forcer_a_quatre_caracteres( lib_general__convertir_dec_hex( tempo[caractere] ) ) );
	
		var chaine = mon_texte.match( /&#([0-9]+);/ );
		while( chaine != null )
			{
			mon_texte = mon_texte.replace( chaine[0], "\\u" + lib_general__forcer_a_quatre_caracteres( lib_general__convertir_dec_hex( chaine[1] ) ) );
			chaine = mon_texte.match( /&#([0-9]+);/ );
			}
		}

	mon_texte=xreplace(mon_texte,"\r","\\r");
	mon_texte=xreplace(mon_texte,"\n","\\n");
	mon_texte=xreplace(mon_texte,"\t","\\t");
	mon_texte=xreplace(mon_texte,"\'","\\\'");
	mon_texte=xreplace(mon_texte,"\"","\\\"");

	eval( "var texte='" + mon_texte + "';" );
	return texte;
}
//---------------------------------------------------------------------------------------
// NOM FONCTION : transmettre_log_service_automatise
// DESCRIPTION COURTE :	
// DESCRIPTION :	
//		.
// ENTREES :
//		@[nom_variable] (type) : 
//			STRUCT :
//				[][dflsdflk]
//				  [dqsdqd][]
//			VALEURS :
//				* false : truc
//				* 1 : truc
//				* "toto" : truc
//		VARIABLES GLOBALES :
// NECESSITES :
//		.
// SORTIES :
//		@nom_variable (type) : utilit�
//		VARIABLES GLOBALES :
// VALEURS DE RETOUR :	
//		* 0 : aucune erreur
// AUTEUR : DANIEL Eric
// DERNIERE MODIF : 17/12/2005
// ETAT : dev
// RESTE A FAIRE :
//		*
// REMARQUES :
// VOIR AUSSI :
//---------------------------------------------------------------------------------------
function transmettre_log_service_automatise(id_service)
{

	// --------------
	// initialisation
	// --------------
	var	url="/commun/logs__services_automatises.php";
	var parametres="id_service="+id_service;
	// ------------------------------------
	// v�rification des param�tres d'entr�e
	// ------------------------------------

	// -----------
	// traitements
	// -----------
	var mon_ajax = new Ajax.Request(url, {method: 'get', parameters: parametres} );

	// ---------------------
	// sortie de la fonction
	// ---------------------
	// 0 : aucune erreur
	return 0;
}
