function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

sfHover = function() {
	
	var sfEls = document.getElementById("nav").getElementsByTagName("LI");
	for (var i=0; i<sfEls.length; i++) {
	
		sfEls[i].onmouseover=function() {		
				this.className+=" sfhover";
		}
		
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
		}
	}
	
	var sfEls = document.getElementById("menu").getElementsByTagName("LI");
	for (var i=0; i<sfEls.length; i++) {
	
		sfEls[i].onmouseover=function() {		
				this.className+=" sfhover2";
		}
		
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover2\\b"), "");
		}
	}

	
}

if (window.attachEvent){ window.attachEvent("onload", sfHover); }




function Show_Hide(id_div){
	var item = null;
	
	if (id_div != ''){
		
		if (document.getElementById){
			item = document.getElementById(id_div); //nav r�cents
		}else if (document.all){
			item = document.all[id_div]; //anciens nav
		}else if (document.layers){
			item = document.layers[id_div]; //nav + vieux
		}

		if (!item){;}
		else if (item.style){
			if (item.style.display == "none"){
				item.style.display = "";
			}else{
				item.style.display = "none";
			}
		}else{
			item.visibility = "show";
		}
	}
}