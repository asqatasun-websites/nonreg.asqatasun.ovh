// fonctions utilis�es dans le popup de tri des postings de contenus
// fonction permettant de faire monter le posting choisi dans la liste
function monter()
{
	var tab = document.getElementById("ListeItems");
	var items = document.getElementsByName("PostingItems");

	var checkedItem = null;
	for (var i=0; i < items.length; i++)
	{
		var item = items[i];
		if(item.checked == true)
		{
			checkedItem = item;
		}
	}

	var row = checkedItem;
	while(row != null && row.tagName != "TR")
	{
		row = row.parentNode;
	}

	if(row != null)
	{
		var previous = row.previousSibling;
		if(previous != null)
		{
			row.parentNode.removeChild(row);
			previous.parentNode.insertBefore(row, previous);
		}
		checkedItem.checked = true;
	}
	manageButtons();
}

// fonction permettant de faire descendre le posting choisi dans la liste de postings
function descendre()
{
	alert('descendre:IN');
	var tab = document.getElementById("ListeItems");
	var items = document.getElementsByName("PostingItems");
	var checkedItem = null;
	for (var i=0; i < items.length; i++)
	{
		var item = items[i];
		if(item.checked == true)
		{
			checkedItem = item;
		}
	}

	var row = checkedItem;
	while(row != null && row.tagName != "TR")
	{
		row = row.parentNode;
	}

	if(row != null)
	{
		var next = row.nextSibling;
		if(next != null)
		{
			row.parentNode.removeChild(row);
			if(next.nextSibling != null)
			{
				next.parentNode.insertBefore(row, next.nextSibling);
			}
			else
			{
				next.parentNode.appendChild(row);
			}
		}
		checkedItem.checked = true;
	}
	
	manageButtons();
	alert('descendre:OUT');
}

// fonction permettant de remplir un champs hidden avec les guids ordonn�s des postings de contenus de la liste
// cette liste sera utilis�e par le code cot� serveur pour enregistrer les informations en base
function fillListeValeurs()
{
	alert('fillListeValeurs:IN');
	var tab = document.getElementById("ListePostings");
	var hiddenField = document.getElementById("ListeValeurs");
	
	row = tab.firstChild.firstChild;
	while (row != null)
	{
		hiddenField.value += row.firstChild.firstChild.id+";";
		row = row.nextSibling;
	}
	alert('fillListeValeurs:OUT');
}

// fonction permettant de g�rer les boutons monter/descendre (les griser lorsque cela est necessaire)
function manageButtons()
{
	var items = document.getElementsByName("PostingItems");
	var monter = document.getElementById("bouton_monter");
	var descendre = document.getElementById("bouton_descendre");
	var checkedItem = null;
	for (var i=0; i < items.length; i++)
	{
		if(items[i].checked == true)
		{
			if ((i == 0) && (i == items.length -1))
			{
				monter.disabled = true;
				descendre.disabled = true;	
			}
			else if (i == 0)
			{
				monter.disabled = true;
				descendre.disabled = false;
			}
			else if (i == items.length -1)
			{
				monter.disabled = false;
				descendre.disabled = true;		
			}
			else
			{
				monter.disabled = false;
				descendre.disabled = false;
			}
		}
	}
}

// fonction de chargement : les deux boutons sont gris�s car aucun posting n'est selectionn�
function loadPage()
{
	//document.getElementById("bouton_monter").disabled = true;	
	//document.getElementById("bouton_descendre").disabled = true;
}


function gereChangeNbEncarts(obj)
{
	var orientation = obj.id.replace('selectNbEncarts', 'choixOrientation');
	var nbEncarts = document.getElementById(obj.id);
	if (nbEncarts != null){
		if (nbEncarts.selectedIndex == 0){
			document.getElementById(orientation).disabled = true;
		}
		else{
			document.getElementById(orientation).disabled = false;
		}
	}
}
