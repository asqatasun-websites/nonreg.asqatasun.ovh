// Cree par Nicolas LEVIEIL pour DECATHLON - Studio SQLI 12/2003

 var menus = new Array();
 var _onresize = "";
 var MINT = 1000;
 var MAXB = 0;
 var HGHT = 0;
 var LFT = 0;
 var RGHT = 0;
 var NSW = 0;
 var LASTH = 0;
 var THEW = 0;
 var MOZT = 0;
 var MACIE5 = 0;
 var NSLOAD = "";
 var NSPASS = 0;
 var NSMAX = 0;
 var ended = 0;
 var OZI = 1000;
 
 MOZT = (navigator.appName=="Netscape")&&(navigator.userAgent.indexOf("NT 5.0")>=0)&&(!document.layers);
 MACIE5 = (navigator.appVersion.indexOf("Mac") != -1)&&(document.all)&&(navigator.appVersion.indexOf("5.") != -1);

 function nslaunch(){
  if(NSPASS<NSMAX)
   setTimeout("nslaunch()", 100);
  else{
   eval(NSLOAD);
   onresize = _false;
  }
 }
 
 function _preload(){
  window.il = new Array(); 
  for(i=0;i<arguments.length;i++){
   il[i] = new Image();
   il[i].src = arguments[i];
  }
 }
 
 function init_hide_select(){
  if(arguments.length==0){
   this.hide_select = this.show_select = _false;
  }
  for(i=0;i<arguments.length;i++)
   this.HIDECOL[this.HIDECOL.length] = arguments[i];
 }
 
 function init_hide_flash(){
  if(arguments.length==0||document.all||MOZT){
   this.hide_flash = this.show_flash = _false;
  }
  else{
   for(i=0;i<arguments.length;i++)
    this.FHIDECOL[this.FHIDECOL.length] = arguments[i];     
  }
 }

 function get_xy(o){
  var rd = { x:0 ,y:0 };
  if(document.layers)
   return o;
  rd.x = o.offsetLeft;
  rd.y = o.offsetTop;
  while(o.offsetParent){
   rd.x += o.offsetParent.offsetLeft;
   rd.y += o.offsetParent.offsetTop;
   o = o.offsetParent;
  }
  return rd;
 }

 function def(cssid, lib, lnk, wdth, ns4wdth, algn){
  this.code_menus += this.line(1) + 'id="m'+this.mid+'r'+cssid+'" class="m'+((cssid==this.CUR)?this.mid+'r'+cssid:'')+'"><layer name="m'+this.mid+'r'+cssid+'"></layer><img src="p.gif" width="1" height="1" align="left" hspace="0" name="img'+this.mid+'l'+this.NB+'"><img src="p.gif" width="1" height="1" name="img'+this.mid+'t'+this.NB+'"><div class=test>'+lib+'</div><img src="p.gif" width="1" height="1" align="right" hspace="0" name="img'+this.mid+'r'+this.NB+'"><img src="p.gif" width="1" height="1" name="img'+this.mid+'b'+this.NB+'"></td>';
  if(document.layers)
   this.code_menusnson += this.line(1) + 'class="m'+this.mid+'r'+cssid+'"><img src="p.gif" width="1" height="1" align="left" hspace="0"><img src="p.gif" width="1" height="1"><div>'+lib+'</div><img src="p.gif" width="1" height="1" align="right" hspace="0"><img src="p.gif" width="1" height="1"></td>';
  if(this.NB>0){
   if(document.layers){
    if(this.SNB>0){
     this.code_smenus += ((this.smenu_int_dernier>0)?'<tr><td colspan="2" class="smenu'+this.mid+'off"><img src="p.gif" width="1" height="'+this.smenu_int_dernier+'"></td></tr>':'') + '</table></div></div>';
     this.code_ssmenus += ((this.smenu_int_dernier>0)?'<tr><td colspan="2" class="smenu'+this.mid+'on"><img src="p.gif" width="1" height="'+this.smenu_int_dernier+'"></td></tr>':'') + '</table></div></div>';
    }
    else{
     this.code_smenus += '<tr><td>&nbsp;</td></tr></table></div></div>';
     this.code_ssmenus += '<tr><td>&nbsp;</td></tr></table></div></div>';
    }
    this.code_sslayers += '<layer></layer></div>';
   }
   else
    this.code_smenus += '</div>';
  }
  this.SNB = 0;
  IEW = (wdth-2*this.smenu_bordure);
  NSW = (ns4wdth-2*this.smenu_bordure);
  if(!document.layers)
   this.code_smenus += '<div class="smenu" id="m'+this.mid+'sr'+cssid+'" style="width:'+IEW+'px; padding:'+this.smenu_bordure+'px;">';
  else{
   this.code_smenus += '<div class="smenu" id="m'+this.mid+'sr'+cssid+'" style="width:'+ns4wdth+'px;"><div style="margin-left:'+this.smenu_bordure+'px; margin-top:'+this.smenu_bordure+'px;"><table cellpadding="0" cellspacing="0" border="0" width="'+NSW+'">';
   this.code_ssmenus += '<div class="ssmenunson" id="m'+this.mid+'nsonsr'+cssid+'"><table cellpadding="0" cellspacing="0" border="0" width="'+NSW+'">';
   this.code_sslayers += '<div class="ssmenunslay" id="m'+this.mid+'nslaysr'+cssid+'"><layer></layer>';
  }
  this.conts[this.NB] = {wdth:wdth, ns4wdth:ns4wdth, rwdth:NSW, algn:algn, lnk:lnk, cssid:cssid, snblnk:null};
  this.conts[this.NB].snblnk = new Array();
  this.NB++;
 }

 function add(lib, lnk){
  if(this.SNB==0)
   int_t = this.smenu_int_premier;
  else
   int_t = this.smenu_int_haut;
  int_b = this.smenu_int_bas;
  int_l = this.smenu_int_gauche;
  int_r = this.smenu_int_droite;
  if(!document.layers)
   this.code_smenus += '<div class="smenu'+this.mid+'off" style="padding-top:'+int_t+'px; padding-bottom:'+int_b+'px; padding-left:'+int_l+'px; padding-right:'+int_r+'px;" onclick="go_rub('+"'"+lnk+"'"+')">'+this.code_fleche+lib+'</div>';
  else{
   this.code_smenus += '<tr><td height="5" class="smenu'+this.mid+'off" width="100%">'+((int_t>0)?'<img src="p.gif" width="1" height="'+int_t+'"><br>':'')+'<div class="smenu'+this.mid+'off" style="padding-left:'+int_l+'px;"><ilayer>'+this.code_fleche+lib+((int_b>0)?'<br><img src="p.gif" width="1" height="'+int_b+'">':'')+'</ilayer></div></td>'+((int_r>0)?'<td class="smenu'+this.mid+'off"><img src="p.gif" width="'+int_r+'" height="1"></td>':'')+'</tr>';
   this.code_ssmenus += '<tr><td height="5" class="smenu'+this.mid+'on" width="100%">'+((int_t>0)?'<img src="p.gif" width="1" height="'+int_t+'"><br>':'')+'<div class="smenu'+this.mid+'on" style="padding-left:'+int_l+'px;"><ilayer>'+this.code_fleche+lib+((int_b>0)?'<br><img src="p.gif" width="1" height="'+int_b+'">':'')+'</ilayer></div></td>'+((int_r>0)?'<td class="smenu'+this.mid+'on"><img src="p.gif" width="'+int_r+'" height="1"></td>':'')+'</tr>';
   this.code_sslayers += '<layer></layer>';
  }
  this.conts[this.NB-1].snblnk[this.SNB] = lnk;
  this.SNB++;
 }
 
 function go_rub(u){
  location.href = u;
 }
 
 function ns_go_rub(){
  location.href = this.lnk;
 }
    
 function _outside(e, o){
  if(e){
   x = e.pageX;
   y = e.pageY;
   if(x<=o.c.l||y<=o.c.t||x>=o.c.r||y>=o.c.b)
    return true;
  }
  else if(e=window.event){
   x = e.clientX;
   y = e.clientY-1;
   if(x<=o.c.l||y<=o.c.t||x>=o.c.r||y>=o.c.b)
    return true;
  }
  return false;
 }

 function init_rub_over(){
  if(document.layers){
   NSLOAD += "init_rub_over_ns(menus["+this.mid+"]);";
   NSPASS++;
   return false;
  }
  
  this.cells = cells = document.getElementById("menu"+this.mid).getElementsByTagName("TD");
  
  for(i=0;i<cells.length;i++){
   thecell = cells.item(i);
   thecell.pobj = this;
   thecell.pos = i;
   THEW = this.conts[i].wdth;
   thecell.sid = "m"+this.mid+"sr"+this.conts[i].cssid;
   thecell.sr = thesmenu = document.getElementById(thecell.sid);
   thesmenu.tm = 1;
   thecell.tmfunc = "menus["+this.mid+"].cells["+i+"].hide();";
   thecell.sr.r = thecell;
   thecell.refclass = thecell.className;

   thecell.onmouseover = thecell.over = function(){
    if(!ended)
     return false;
    this.tm = 0;
    if(this.pobj.curcell!=null){
     if(this.pobj.curcell==this){
      if(this.sr.curon!=null)
       this.sr.curon.hide();
      return;
     }
     this.pobj.curcell.tm = 0;
     this.pobj.curcell.hide(1);
    }
    else{
     this.pobj.hide_select();
     this.pobj.hide_flash();
    }
    this.className = this.id;
    this.sr.style.visibility = 'visible';
    this.pobj.curcell = this;
   }

   thecell.hide = function(){
    if(this.tm||(arguments.length==1)){
     this.sr.style.visibility = 'hidden';
     this.className = this.refclass;
     this.tm = 0;
     this.pobj.curcell = null;
     if(arguments.length==0){
      this.pobj.show_select();
      this.pobj.show_flash();
     }
     if(this.sr.curon!=null)
      this.sr.curon.hide();
    }
   }
   
   thecell.tmhide = function(){
    this.tm = 1;
    window.setTimeout(this.tmfunc, 10);
   }

   thecell.onmouseout = function(e){
    if(!ended)
     return false;
    if(_outside(e, this))
      this.tmhide();
   }
   
   thecell.onclick = function(){
    go_rub(this.pobj.conts[this.pos].lnk);
   }
   
   if(thesmenu.innerHTML==""){
    thesmenu.style.display = "none";
   }
   else{
    if(thesmenu.lastChild)
     thesmenu.lastChild.style.paddingBottom = this.smenu_int_dernier;
    
    thesmenu.onmouseover = function(){
     this.r.tm = 0;
    }
    
    thesmenu.onmouseout = thesmenu.out = function(e){
     if(_outside(e, this)&&_outside(e, this.r))
      this.r.tmhide();
    }
  
    for(d=0;d<thesmenu.childNodes.length;d++){
     thechild = thesmenu.childNodes[d];
     thechild.pobj = thesmenu;
     thechild.con = "smenu"+thechild.pobj.r.pobj.mid+"on";
     thechild.coff = "smenu"+thechild.pobj.r.pobj.mid+"off";

     thechild.onmouseover = function(){
      this.pobj.r.tm = 0;
      if(this.pobj.curon!=null)
       this.pobj.curon.hide();
      this.className = this.con;
      this.pobj.curon = this;
     }
     
     thechild.hide = function(){
      this.className = this.coff;
      this.pobj.curon = null;
     }
     
     thechild.onmouseout = function(e){
      if(_outside(e, this.pobj))
       this.pobj.r.tmhide();
      this.hide();
     }
    }
   }
  }
 }

 function _false(){
  return false;
 }

 function place_srub(o){
  findpos(o.mid, o.NB);
  for(i=0;i<o.NB;i++){
   thesmenu = document.getElementById("m"+o.mid+"sr"+o.conts[i].cssid);
   thesmenu.curon = null;
   thesmenu.tm = 1;
   thesmenu.style.top = MAXB;
   if(MACIE5)
    thesmenu.style.top = HGHT;
   lpos = get_xy(document.images["img"+o.mid+"l"+i]);
   rpos = get_xy(document.images["img"+o.mid+"r"+i]);
   switch(o.conts[i].algn){
    case "G":
     thesmenu.style.left = (((lpos.x+o.conts[i].rwdth)>RGHT)?(RGHT-o.conts[i].rwdth):lpos.x) - ((MACIE5)?LFT:0);
    break;
    case "D":
    if(!document.all)
     thesmenu.style.left = ((rpos.x + 1 - o.conts[i].wdth)<LFT)?LFT:(rpos.x + 1 - o.conts[i].wdth);
    else
     thesmenu.style.left = ((rpos.x + 1 - o.conts[i].rwdth)<LFT)?LFT:(rpos.x + 1 - o.conts[i].rwdth) - ((MACIE5)?LFT:0);
    break;
   }
   if(document.all){
    for(d=0;d<thesmenu.childNodes.length;d++){
     thechild=thesmenu.childNodes[d];
     thechild.style.width = thechild.offsetWidth;
    }
   }
   thesmenu.c = {l:parseInt(thesmenu.style.left)+1, r:parseInt(thesmenu.style.left)+parseInt(thesmenu.offsetWidth), t:parseInt(thesmenu.style.top), b:parseInt(thesmenu.style.top)+parseInt(thesmenu.offsetHeight)};
   thesmenu.r.c = {l:thesmenu.c.l, r:thesmenu.c.l+parseInt(thesmenu.r.offsetWidth), t:MINT, b:MAXB};
  }
 }

 function show_clip(){
  this.tm = 0;
  if(this.pobj.curcell!=null){
   this.pobj.curcell.tm = 0;
   document.layers["menu"+this.pobj.curcell.pobj.mid+"nson"].clip.right = 0;
   this.pobj.curcell.sr.visibility = this.pobj.curcell.layevt.visibility = "hide";
  }
  document.layers["menu"+this.pobj.mid+"nson"].clip.left = this.clipl;
  document.layers["menu"+this.pobj.mid+"nson"].clip.right = this.clipr;
  this.pobj.hide_select();
  this.pobj.hide_flash();
  this.sr.visibility = this.layevt.visibility = "show";
  this.pobj.curcell = this;
 }

 function hide_clip(){
  if(this.tm==0)
   return;
  document.layers["menu"+this.pobj.mid+"nson"].clip.right = this.layon.clip.bottom = 0;
  this.sr.visibility = this.layevt.visibility = "hide";
  this.pobj.show_select();
  this.pobj.show_flash();
  this.tm==0;
 }

 function tmhide_clip(){
  this.tm = 1;
  setTimeout(this.tmfunc, 10);
 }
 
 function tmhide_ssclip(){
  this.trgtobj.clip.bottom = 0;
  this.tmobj.tmhide();
 }

 function _release_ns(){
  this.pobj.tm = 0;
 }
 
 function _release_ssns(){
  this.tmobj.tm = 0;
  this.trgtobj.clip.top = this.clips.t;
  this.trgtobj.clip.bottom = this.clips.b;
 }

 function _bubble_ns(){
  this.pobj.tmhide();
 }
 
 function findpos(mid, nb){
  MINT = 1000;
  MAXB = 0;
  imgl = get_xy(document.images["img"+mid+"l0"]);
  imgr = get_xy(document.images["img"+mid+"r"+(nb-1)]);
  for(i=0;i<nb;i++){
   imgt = get_xy(document.images["img"+mid+"t"+i]);
   imgb = get_xy(document.images["img"+mid+"b"+i]);
   MINT = Math.min(MINT, imgt.y);
   MAXB = Math.max(MAXB, imgb.y + 1);
  }
  HGHT = MAXB - MINT;
  LFT = imgl.x;
  inc = 0;
  if(!document.all&&!document.layers)
   inc = 2*parseInt(menus[mid].cells[0].sr.style.paddingLeft);
  RGHT = imgr.x + 1 - inc;
 }

 function init_rub_over_ns(o){
  findpos(o.mid, o.NB);
  document.layers["menu"+o.mid+"nson"].left = LFT;
  document.layers["menu"+o.mid+"nson"].top = MINT;
  document.layers["menu"+o.mid+"nson"].clip.bottom = HGHT;

  int_p = o.smenu_int_premier;
  int_t = o.smenu_int_haut;

  for(i=0;i<o.NB;i++){
   ns = document.layers["m"+o.mid+"r"+o.conts[i].cssid];
   ns.sid = i;
   ns.pid = o.mid;
   o.cells[i] = ns;
   ns.left = document.images["img"+o.mid+"l"+i].x;
   ns.pobj = o;
   ns.clipl = document.images["img"+o.mid+"l"+i].x - LFT;
   ns.clipr = document.images["img"+o.mid+"r"+i].x - LFT + 1;
   ns.zIndex = OZI;
   ns.top = MINT;
   ns.clip.height = HGHT;
   ns.clip.width = ns.clipr-ns.clipl;
   ns.visibility = "show";
   ns.onmouseover = show_clip;
   ns.hide = hide_clip;
   ns.onmouseout = ns.tmhide = tmhide_clip;
   ns.lnk = o.conts[i].lnk;
   ns.document.lnk = o.conts[i].lnk;
   ns.document.captureEvents(Event.CLICK);
   ns.document.onclick = ns_go_rub;
   ns.tmfunc = "menus["+ns.pid+"].cells["+ns.sid+"].hide()";
   ns.sr = document.layers["m"+o.mid+"sr"+o.conts[i].cssid];
   ns.sr.zIndex = OZI-1;
   ns.layon = document.layers["m"+o.mid+"nsonsr"+o.conts[i].cssid];
   ns.sr.pobj = ns;
   ns.sr.onmouseover = _release_ns;
   ns.sr.onmouseout = _bubble_ns;
   ns.sr.top = MAXB;
   ns.sr.clip.width = o.conts[i].wdth;
   lpos = get_xy(document.images["img"+o.mid+"l"+i]);
   rpos = get_xy(document.images["img"+o.mid+"r"+i]);
   switch(o.conts[i].algn){
    case "G":
    ns.sr.left = ((lpos.x+o.conts[i].ns4wdth)>RGHT)?(RGHT - o.conts[i].ns4wdth):lpos.x;
    break;
    case "D":
    ns.sr.left = ((rpos.x + 1 - o.conts[i].ns4wdth)<LFT)?LFT:(rpos.x + 1 - o.conts[i].ns4wdth);
    break;
   }
   ns.layon.left = ns.sr.pageX + o.smenu_bordure;
   ns.layon.top = ns.sr.pageY + o.smenu_bordure;
   ns.layon.clip.right = o.conts[i].wdth;
   ns.layon.zIndex = OZI-1;
   ns.layevt = document.layers["m"+o.mid+"nslaysr"+o.conts[i].cssid];
   ns.layevt.zIndex = OZI;
   ns.layevt.left = ns.sr.pageX + o.smenu_bordure;
   ns.layevt.top = ns.sr.pageY;
   ns.layevt.clip.right = ns.layevt.clip.width = o.conts[i].ns4wdth - 2*o.smenu_bordure;   
   LASTH = o.smenu_bordure;
   
   for(l=0;l<ns.sr.layers.length;l++){
    layoff = ns.sr.layers[l];
    layon = ns.layevt.layers[l+1];
    layon.trgtobj = ns.layon;
    layon.tmobj = ns;
    layon.onmouseover = _release_ssns;
    layon.onmouseout = tmhide_ssclip;
    layon.document.lnk = o.conts[i].snblnk[l];
    layon.document.captureEvents(Event.CLICK);
    layon.document.onclick = ns_go_rub;
    layon.clips = {t:0, b:0};
    layon.clip.width = ns.layevt.clip.width;
    ns.layevt.clip.height = layon.top = LASTH;
    layon.clips.t = LASTH-o.smenu_bordure;
    layon.clip.height = layoff.clip.height + ((l==0)?o.smenu_int_premier:o.smenu_int_haut) + ((l==(ns.sr.layers.length-1))?o.smenu_int_dernier:0);
    LASTH += layon.clip.height;
    layon.clips.b = LASTH-o.smenu_bordure;
   }
   if(ns.layevt.layers.length>2){
    ns.layevt.layers[0].pobj = ns.layevt.layers[ns.layevt.layers.length-1].pobj = ns;
    ns.layevt.layers[0].clip.width = ns.layevt.layers[ns.layevt.layers.length-1].clip.width = ns.layevt.clip.width;
    ns.layevt.layers[0].clip.height = ns.layevt.layers[ns.layevt.layers.length-1].clip.height = o.smenu_bordure;
    ns.layevt.layers[0].onmouseover = ns.layevt.layers[ns.layevt.layers.length-1].onmouseover = _release_ns;
    ns.layevt.layers[ns.layevt.layers.length-1].onmouseout = _bubble_ns;
    ns.layevt.layers[ns.layevt.layers.length-1].top = LASTH;
    ns.layevt.clip.height = LASTH + o.smenu_int_dernier;
    ns.layevt.clip.height = ns.sr.clip.height = ns.sr.clip.height + o.smenu_bordure;
   }
   else
    ns.sr.clip.right = ns.layevt.clip.right = 0;
  }
  for(i=0;i<o.FHIDECOL.length;i+=2){
   thefobj = document.layers[o.FHIDECOL[i]];
   thensfobj = document.layers["ns"+o.FHIDECOL[i]];
   thensfobj.clip.width = thefobj.clip.width;
   thensfobj.clip.height = thefobj.clip.height;
   thensfobj.visibility = "hide";
   thensfobj.document.open();
   thensfobj.document.write('<img src="'+o.FHIDECOL[i+1]+'" width="'+thensfobj.clip.width+'" height="'+thensfobj.clip.height+'">');
   thensfobj.document.close();
   thensfobj.left = thefobj.pageX;
   thensfobj.top = thefobj.pageY;
   thensfobj.zIndex = 9;
  }
  OZI-=3;
 }
 
 function _reload(){
  return false;
 }
 
 function menu(mid, rid){
  this.mid = mid;
  this.smenu_bordure = 0;
  this.smenu_int_gauche = 0;
  this.smenu_int_droite = 0;
  this.smenu_int_premier = 0;
  this.smenu_int_haut = 0;
  this.smenu_int_bas = 0;
  this.smenu_int_dernier = 0;
  this.code_fleche = "";
  this.CUR = rid;
  this.NB = this.SNB = 0;
  this.conts = new Array();
  this.cells = new Array();
  this.HIDECOL = new Array();
  this.FHIDECOL = new Array();
  this.curcell = null;
  this.init_hide = eval("init_hide_select"+mid);
  this.init_fhide = eval("init_hide_flash"+mid);
  this.init_menu = eval("init_menu"+mid);
  this.line = eval("line"+mid);
  this.def = def;
  this.add = add;
  this.init_rub_over = init_rub_over;
  this.init_rub_over_ns = init_rub_over_ns;
  this.init_hide_select = init_hide_select;
  this.init_hide_flash = init_hide_flash;
  this.hide_select = hide_select;
  this.show_select = show_select;
  this.hide_flash = hide_flash;
  this.show_flash = show_flash;
  if(document.layers){
   this.hide_select = hide_select_ns;
   this.show_select = show_select_ns;
   this.hide_flash = hide_flash_ns;
   this.show_flash = show_flash_ns;
  }
  this.code_menus = '<div id="menu'+this.mid+'">' + this.line(0);
  this.code_menusnson = '<div class="menunson" id="menu'+this.mid+'nson">' + this.line(0);
  this.code_smenus = this.code_ssmenus = this.code_sslayers = '';
  this.init_hide();
  this.init_fhide();
  if(document.layers){
   for(i=0;i<this.FHIDECOL.length;i+=2)
    this.code_smenus += '<layer name="ns'+this.FHIDECOL[i]+'"></layer>';
  }
  this.init_menu();
  this.code_menus += this.line(2);
  this.code_menusnson += this.line(2);
  if(document.layers){
   if(this.SNB>0){
    this.code_smenus += ((this.smenu_int_dernier>0)?'<tr><td colspan="2" class="smenu'+this.mid+'off"><img src="p.gif" width="1" height="'+this.smenu_int_dernier+'"></td></tr>':'') + '</table></div></div>';
    this.code_ssmenus += ((this.smenu_int_dernier>0)?'<tr><td colspan="2" class="smenu'+this.mid+'on"><img src="p.gif" width="1" height="'+this.smenu_int_dernier+'"></td></tr>':'') + '</table></div></div>';
   }
   else{
    this.code_smenus += '<tr><td>&nbsp;</td></tr></table></div></div>';
    this.code_ssmenus += '<tr><td>&nbsp;</td></tr></table></div></div>';
   }
   this.code_sslayers += '<layer></layer></div>';
   this.code_menus+=this.code_menusnson+this.code_smenus+this.code_ssmenus+this.code_sslayers;
  }
  else
   this.code_menus+=this.code_smenus+'</div>';
  document.write(this.code_menus);
  this.init_rub_over();
 }
 
// Masquage des objets de type FLASH OBJECT
 function hide_flash(){
  for(i=0;i<this.FHIDECOL.length;i+=2){
   fobj = document.getElementById(this.FHIDECOL[i]);
   if(fobj.refhtml==null){
    fobj.refhtml = fobj.innerHTML;
    fobj.althtml = '<img src="'+this.FHIDECOL[i+1]+'" width="'+fobj.childNodes(0).width+'" height="'+fobj.childNodes(0).height+'" id="'+this.FHIDECOL[i]+'">';
   }
   fobj.innerHTML = fobj.althtml;
  }
 }

// Masquage des objets de type FLASH OBJECT NS4.7
 function hide_flash_ns(){
  for(i=0;i<this.FHIDECOL.length;i+=2){
   document.layers[this.FHIDECOL[i]].visibility = "hide";
   document.layers["ns"+this.FHIDECOL[i]].visibility = "show";
  }
 }

// Masquage des objets de type FLASH OBJECT
 function show_flash(){
  for(i=0;i<this.FHIDECOL.length;i+=2){
   fobj = document.getElementById(this.FHIDECOL[i]);
   fobj.innerHTML = fobj.refhtml;
  }
 }

// Masquage des objets de type FLASH OBJECT NS4.7
 function show_flash_ns(){
  for(i=0;i<this.FHIDECOL.length;i+=2){
   document.layers["ns"+this.FHIDECOL[i]].visibility = "hide";
   document.layers[this.FHIDECOL[i]].visibility = "show";
  }
 }

// Masquage des objets de type SELECT
 function hide_select(){
  for(i=0;i<this.HIDECOL.length;i++)
   document.getElementById(this.HIDECOL[i]).style.visibility = "hidden";
 }
 
// Masquage des objets de type SELECT NS4.7
 function hide_select_ns(){
  for(i=0;i<this.HIDECOL.length;i++)
   document.layers[this.HIDECOL[i]].visibility = "hide";
 }

// Réaffichage des objets de type SELECT
 function show_select(){
  for(i=0;i<this.HIDECOL.length;i++)
   document.getElementById(this.HIDECOL[i]).style.visibility = "visible";
 }

 // Réaffichage des objets de type SELECT NS4.7
 function show_select_ns(){
  for(i=0;i<this.HIDECOL.length;i++)
   document.layers[this.HIDECOL[i]].visibility = "show";
 }

 function write_menus(mid, rid){
  menus[mid] = new menu(mid, rid);
  pstr = "place_srub(menus["+mid+"]);";
  _onresize += pstr;
 }
 
 function write_menus_end(NBMENUS){
  NSMAX = NBMENUS;
  if(document.onreadystatechange!=null){
   document.onreadystatechange = function(){ if(document.readyState=="complete"){ _resize();}}
   onresize = _resize;
  }
  else if(!document.layers){
   _resize();
   onresize = _resize;
  }
  else
   onload = nslaunch;
 }

 function _resize(){
  ended = 0;
  eval(_onresize);
  ended = 1;
 }
 
  function init_hide_select1(){
 this.init_hide_select("sel2");
}

function init_hide_flash1(){
 this.init_hide_flash("flash1", "img1.jpg");
}

function line1(type){
 switch(type){
  case 0:
   return '<table cellpadding="0" cellspacing="0" border="0"><tr>';
   break;
  case 1:
   return '<td width="5%" ';
   break;
  case 2:
   return '</tr></table></div>';
   break;
 }
}