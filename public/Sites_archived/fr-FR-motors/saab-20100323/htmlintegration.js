var params = {
	scale:"noscale",
	salign:"tl",
	allowfullscreen:"true",
	wmode:"opaque"
	
};

var attributes = {
	id:"saab",
	name:"saab"
};

function addMouseWheel() {
	new SWFMacMouseWheel( document.getElementById( attributes.id ) );
}

var fs;
var documentHeight = 1000;

function addResize() {	
	//IE6 Special
	if(navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion <= 7) {
		fs = new SWFForceSize( swfobject.getObjectById( attributes.id ) , 995 , 150 );
	}
	else {

		fs = new SWFForceSize( swfobject.getObjectById( attributes.id ) , 1000 , 150 );
	}
}
function setHeight( h ) {
	if( !fs ) return;
	fs.setMinHeight( h );
	documentHeight = h;
}

var scroll = new Scroll();

function scrollCenter() {
	scroll.center();
}
function scrollReset() {
	scroll.reset();
}

function addMouseWheel() {
	new SWFMacMouseWheel( document.getElementById( attributes.id ) );
}


function load(flashvars)
{
	// cybercom style html integration.
	
	var thedivs = document.getElementsByTagName('div');

	document.body.style.width = '100%'; 
	document.body.style.height = '100%';
	document.body.style.margin = '0'; 
	document.body.style.padding = '0';

	document.getElementById(thedivs[0].id).style.width = '100%'; 
	document.getElementById(thedivs[0].id).style.height = '99%';
	document.getElementById(thedivs[0].id).style.zIndex = 1;
	document.getElementById(thedivs[0].id).style.position = 'absolute';

	swfobject.embedSWF( "/webdav/files/global/lib/main.swf", thedivs[1].id, "100%", "100%", "9.0.0", false, flashvars, params, attributes );
	
/*
	// public class style html integration.
	var bodyHTML = '		<div id="content" align="center">\n';
	bodyHTML += '			<div id="saabHolder">\n';
	bodyHTML += '				<a href="http://www.adobe.com/go/getflashplayer">\n';
	bodyHTML += '					<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />\n';
	bodyHTML += '				</a>\n';
	bodyHTML += '			</div>\n';
	bodyHTML += '		</div>\n';
	
	document.body.innerHTML = bodyHTML;
	
	document.getElementById('content').style.width = '100%'; 
	document.getElementById('content').style.height = '99%';
	document.getElementById('content').style.zIndex = 1;
	document.getElementById('content').style.position = 'absolute';
	
	swfobject.embedSWF( "/webdav/files/global/lib/main.swf", "saabHolder", "100%", "100%", "9.0.0", false, flashvars, params, attributes );
*/
	swfobject.addDomLoadEvent( addResize );
	swfobject.addDomLoadEvent( addMouseWheel );
}


function downloadFile(file)
{
	// document.location.href=file;
	window.open(file, "saabFile", "");
}


function openWindow(url, name, width, height, menubar, toolbar)
{
	window.open(url, name, "menubar=" + menubar + ",width=" + width + ",height=" + height + ",toolbar=" + toolbar);
}
function openEbrochure(file)
{
	// document.location.href=file;
	window.open(file, null, "width=1000,height=720");
}
function openWindow_v2(url, name, width, height, menubar, scrollbar)
{
	window.open (url,name,"\"menubar="+menubar+",resizable=1,width="+width+",height="+height+",scrollbars="+scrollbar+"\""); 
}