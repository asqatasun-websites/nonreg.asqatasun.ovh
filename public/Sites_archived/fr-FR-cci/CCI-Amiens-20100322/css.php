
body {
font-family : Arial, sans-serif;
font-size : 12px;
background-color: #FFFFFF;
color : #000000;
text-align: justify;
}

b {
font-weight : bold;
font-size : 12px;
font-family : Arial, sans-serif;
}

td { 
font-size : 12px;
font-family : Arial, sans-serif;
color: #000000;
}

div {
	margin: 0;
	padding: 0;
}

p,
.txt, 
.txt p,
.textenormalbleugras {
	font-family : Arial, sans-serif;
	font-size : 12px;
	color : #000000;
	text-align: justify;
	padding: 0;
	margin: 0 2px 12px 2px;
}

.colgauche {
    margin-left:20px;
}

.textenormalbleugras {
	font-weight: bold;
	color: #444444;
}

.breadcrumb, 
a .breadcrumb {
	font-family : Arial, sans-serif;
	font-size : 10px;
	color : #444444;
	text-align: right;
}

.breadcrumb:hover,
a:hover .breadcrumb { text-decoration: underline; }

li {
	padding-top: 0em;
	margin-left: -0.6em;
	font-family : Arial, sans-serif;
	color : #000000;
	text-align: left;
/*	list-style-image : url(http://www.amiens.cci.fr/img/structure/pucebleu.gif); */
}

.li-liens-fiches-on {
	padding-top: 0.3em;
	margin-left: -1em;
	font-family : Arial, sans-serif;
	font-size : 12px;
	color : #000000;
	list-style-image : url(puce.gif);
}

.li-liens-fiches {
	padding-top: 0.3em;
	margin-left: -1em;
	font-family : Arial, sans-serif;
	font-size : 12px;
	color : #000000;
	list-style-image : url(puceblanche.gif);
}

.ul-liens-fiches {
	padding-top: 0;
	padding-bottom: 0;
}

.boldblanc {
font-weight : bold;
font-size : 12px;
color : #FFFFFF;
font-family : Arial, sans-serif;
}

.boldmarine {
font-weight : bold;
font-size : 12px;
color : #1B1451;
font-family : Arial, sans-serif;
}

.boldjaune {
font-weight : bold;
font-size : 12px;
color : #FFFF00;
font-family : Arial, sans-serif;
}

.fd_ombre_hg
{
background-image:url(fd_ombre_hg.gif);
background-color: #edf4fc;
}

.fd_ombre_hd
{
background-image:url(fd_ombre_hd.gif);
background-color: #edf4fc;
}

.fd_ombre_bg
{
background-image:url(fd_ombre_bg.gif);
background-color: #edf4fc;
}

.fd_ombre_bd
{
background-image:url(fd_ombre_bd.gif);
background-color: #edf4fc;
}

.fd_ombre_h
{
background-image:url(fd_ombre_h.gif);
background-repeat:repeat-x;
background-color: #edf4fc;
}

.fd_ombre_b
{
background-image:url(fd_ombre_b.gif);
background-repeat:repeat-x;
background-color: #edf4fc;
}

.fd_ombre_g
{
background-image:url(fd_ombre_g.gif);
background-repeat:repeat-y;
background-color: #edf4fc;
}

.fd_ombre_d
{
background-image:url(fd_ombre_d.gif);
background-repeat:repeat-y;
background-color: #edf4fc;
}

.fond
{
/* background-image:url(http://www.amiens.cci.fr/img/structure/fond.jpg); */
background-color: #ffffff;
}

.fond_news
{
background-image:url(index.html.2); 
background-repeat: repeat-x; 
background-color: #a0a0c3;
font-size : 12px;
font-family : Arial, sans-serif;
color: #1B1451;
}

.petit {
font-size : 10px;
font-family : Arial, sans-serif;
}

.blanc { 
font-size : 12px;
font-family : Arial, sans-serif;
color: #FFFFFF;
}

.fluo, 
.orange { 
font-size : 12px;
font-family : Arial, sans-serif;
color: #FF6600;
}

.txt_news,
.news,
.txt_news li,
.txt_news ul li,
.txt_news table,
.txt_news td,
.txt_news table tr td { 
font-size : 12px;
font-family : Arial, sans-serif;
color: #1B1451;
text-align: left;
}

.surtitre,
.surtitre-accueil { 
font-size : 15px;
font-weight : bold;
font-family : Arial, sans-serif;
color : #FFFFFF;
background-color: #0065b5;
width: 750px;
border: 0;
padding: 2px 2px 2px 3px;
text-align: left;
margin: 0;
}

.surtitre-accueil {
width:100%;
}

.titre,
.titreblanc { 
font-size : 20px;
font-weight : normal;
font-family : Arial, sans-serif;
color : #000000;
background-color: transparent;
text-align: left;
}
	
.titre-zoom-short { 
font-size : 16px;
font-weight : bold;
font-family : Arial, sans-serif;
color : #000000;
background-color: transparent;
text-align: left;
}
	
.sstitre,
.titreorange,
.soustitre,
.boldorange { 
font-size : 13px;
font-weight : bold;
font-family : Arial, sans-serif;
color : #000000;
padding: 0;
margin: 0 2px 3px 2px;
}

.sstitre_zoom,
bigtxt_idxactu { 
font-size : 11px;
font-weight : normal;
font-family : Arial, sans-serif;
color : #880000;
padding: 0 2px 0 2px;
margin: 0;
text-align: left;
}

ul.sstitre_zoom {
    margin-left: 16px;
}

li.sstitre_zoom {
    padding: 2px 2px 0 0;
    list-style: square outside;
}

.sstitre_emploi { 
font-size : 11px;
font-weight : bold;
font-family : Arial, sans-serif;
color : #880000;
padding: 0 2px 0 2px;
margin: 0;
text-align: left;
}

.stitre { 
font-size : 15px;
font-weight : bold;
font-family : Arial, sans-serif;
color : #444444;
}

.chapo { 
font-size: 12px;
font-weight: bold;
font-family: Arial, sans-serif;
text-align: justify;
color: #737373;
margin-bottom: 12px;
}

a {
color : #880000;
font-family :    Arial, sans-serif;
text-decoration : none;
}

a:hover {
text-decoration: underline;
}

.amodel {
background-color : #9999CC;
border : 1px #FFFFFF solid;
font-family : Arial, sans-serif;
color : #1B1451;
font-size : 12px;
height : 18px;
}

form { margin: 0px; }
TEXTAREA { BACKGROUND-COLOR: #edf4fc; }
SELECT { BACKGROUND-COLOR: #edf4fc; }
INPUT { BACKGROUND-COLOR: #edf4fc; }
img, a img { BORDER-COLOR:#ffffff; }
img:hover, a:hover img { BORDER-COLOR:#ffff00; }

.recherche-top {
background-color: #FFFFFF;
border: 1px #FFFFFF solid;
height: 15px;
width: 80px;
font-size: 8px;
}

.lien_wz, .lien_wz a, .lien_std, .lien_std a {
	text-align: right;
	color: #000000;
	font-weight: normal;
	text-decoration: underline;
	font-size: 10px;
	margin-top: 5px;
	margin-bottom:5px;
	padding-top: 0px;
	padding-bottom:0px;
}

.lien_wz:hover, .lien_wz a:hover, .lien_std:hover, .lien_std a:hover {
    text-decoration: underline;
}

.lien_zoom, .lien_zoom a {
	margin-top: 0.5em;
	margin-bottom: 1em;
	text-align: right;
	color: #000000;
	font-weight: normal;
	text-decoration: underline;
	font-size: 10px;
	margin-bottom:5px;
}

.lien_zoom:hover, .lien_zoom a:hover {
	text-decoration: underline;
}

.legende {
	font-size: 75%;
	color: #444444;
	text-align: left;
}

.legende_news {
	font-size: 75%;
	color: #1B1451;
}

hr  {
	border: solid 0px #888;
	height: 1px;
	BACKGROUND-COLOR: #888;
	padding: 0;
	margin: 0;
}

.txt > hr  {
	margin: -10px 0 -8px 0;
}

hr.emploi {
	border: none 0;
	border-top: 2px dotted #880000;  /* #df001d */
	width: 100%;
	height: 2px;
	BACKGROUND-COLOR: #FFFFFF;
}

.encart {
	width:95%;
	background:#f0f0f0; 
	padding: 0 3px 0 3px;
	margin: 0 0 0 2px;
	clear: both;
}

.imghr  {
	border: solid 0px white;
	BACKGROUND-COLOR: #e3e3e3;
	height: 1px;
	width: 100%;
	margin: 0 0 0 0;
	padding: 0;
}

#intromenu {
	background-image: url(menu_intro.gif);
	background-repeat: no-repeat;
	font: 12px verdana, arial, sans-serif;
	text-align: center;
	color: #FFFFFF;
	font-weight: normal;
	border: 1px solid white;
	border-left: 0px;
	border-right: 0px;
	text-decoration: none;
	width: 100%;
	padding: 2px 10px 2px 2px;  
	margin: 0 0 7px 0;
	/*display: inline;*/
}

/* Menu gauche en CSS */
.mli1, 
.mli2, 
.mli3, 
.mli4 {
  font: 95% verdana, arial, sans-serif;
  font-weight: normal;
  list-style-type: none;
}

.mul0 {
	width: 225px;
	margin: 0 0 0 0;
	padding: 0 0 0 0;
	border: none;
	list-style-type: none;
}

.mli1 { 
	padding: 3px 0 0 0; 
	list-style-type: none;
}

.mli1, .mli2, .mli3, .mli4 {
	margin: 0 0 0 -20px;
}

.mli2,
.mli3,
.mli4 {
	list-style-image: url(puce_mini.gif);
}

.mli0 {
	padding: 4 0 0 0;
	list-style-type: none;
	margin: -4px 0 7px 0;  	
	width: 201px;
}

.msp0 {
	background-image: url(menu_puce.gif);
	background-repeat: no-repeat;
	font: 15px arial, sans-serif;
	text-align: left;
	color: #FFFFFF;
	font-weight: bold;
background-color: #FF0000;
width: 100%;
border: 2px 2px 2px 2px;
	text-decoration: none;
	width: 100%;
	padding: 2px 2px 2px 22px;  
	/*display: inline;*/
}

a:hover div.msp0 {
	text-decoration: underline;
}

.msp0 {}

.msp1,
.msp2,
.msp3,
.msp4 { color: #000000; font-size: 12px; font-weight: bold; }

a { cursor: pointer; }

.menuO .msp0 { background-image: url(menu_puce_optique.gif); }
.menuE .msp0 { background-image: url(menu_puce_electronique.gif); }
.menuL .msp0 { background-image: url(menu_puce_logiciel.gif); }
.menuA .msp0 { background-image: url(menu_puce_adherents.gif); }

.msp1 { padding-top: 6px; }
.msp2 { }
.msp3 { }
.msp4 { }
.msp5 { }

.menu_bis, .menu_bis a {
font-size : 10px;
font-family : Arial, sans-serif;
color: #000000;
}

/* Menu du haut en CSS */
ul.mhul0 {
	margin: 0 0 0 10;
	padding: 0 0 0 10;
	width: auto;
	position: relative;
//	top: 4;
	left: 4;
}

ul.mhul0 .msp0 {
	width: auto;
}


li.mhli0 {
	float: left;
	padding: 0 10 0 0;
	list-style-type: none;
}


ul.mhul1 {
	position: absolute;
	background-color: #FFFFFF;
	width: 200px;
	left: -25px;
}


li.mhli1 {
	list-style-type: none;
	margin: 0 0 0 0;
	padding: 0 0 0 0;
}

.menu-top, li.mhli0 .msp0 {
	background: none;
	border: none;
	cursor: pointer;
	font: 12px verdana, arial, sans-serif;
	font-weight: bold;
	color: #FFFFFF;
	margin: 0 0 0 0;  
	padding: 2px 2px 2px 2px;  
	display: inline;
}

//.menu_top { position: relative; top: 4px; }
	
/* Deroulement des menus */
.mul1 { 
/*	display: none; */
	display: block;
	position: relative;
	top: -3px;
}

.mhul1 { 
/*	display: none; */
	display: block;
}

.CSStoShow { display: block; }
li.mli0:hover > ul.mul1 { display: block; }
li.mhli0:hover > ul.mhul1 { display: block; }

.bt-uk {
	border: solid 0px black;
	position:relative;
	top: 2px;
}
/* Fin menu gauche en CSS */
/* Couleur de fond des menus niveau 1 et des titres */
/*
#t39, #t29, #t19, #t9, #m0, #m10 { background-color: #94aa4a; }
#t38, #t28, #t18, #t8, #m1, #m11 { background-color: #f78221; }
#t37, #t27, #t17, #t7, #m2, #m12 { background-color: #0065b5; }
#t36, #t26, #t16, #t6, #m3, #m13 { background-color: #ce0000; }
#t35, #t25, #t15, #t5, #m4, #m14 { background-color: #31cb00; }
#t34, #t24, #t14, #t4, #m5, #m15 { background-color: #cea221; }
#t33, #t23, #t13, #t3, #m6, #m16 { background-color: #00cbce; }
#t32, #t22, #t12, #t2, #m7, #m17 { background-color: #ce6918; }
#t31, #t21, #t11, #t1, #m8, #m18 { background-color: #0071ce; }
#t30, #t20, #t10, #t0, #m9, #m19 { background-color: #ce5100; }
*/

.col-gauche {
    background-image: url(fond_gauche.gif); 
    background-repeat: vertical;
}

.printer {
    position: relative;
    top: -18px;
    left: -17px;
}

.bord-a-gauche {
    border: solid 3px #888888; 
    border-top: 0; 
    border-bottom: 0; 
    border-right: 0; 
    padding-left: 5px;"
}

.toplogo {
    border-color: transparent;
    border-width: 0;
}
