var newwindow; 
var swfId = "popupSwf";

    function openWindow(pageUrl) {
        var winName = Math.round(9999*Math.random()) + new Date().getTime();
        var winNew = window.open(pageUrl,winName,"toolbar=1,scrollbars=1,location=1,statusbar=0,menubar=0,resizable=1,width=800,height=700,left=200,top=100");

        if(!winNew) {
            getSwf(swfId).openWindowFromSwf(pageUrl);
        }
        else {
            winNew.focus();
        }
    }

    function getSwf(id) {
        if (navigator.appName.indexOf("Microsoft") != -1) {
            return window[id];
        } 
        else {
            return document[id];
        }
    }    



function setUrl () {
	window.location.href = window.location.href+'#/';
};

if(window.location.hash == "") {
	setUrl();
};

function trackPage( url ) {
	tc_log(url);
	
	/*
 * Local tagging below
 */ 
	if (location.href.indexOf('/nl/nl/') != -1)
	{
		run_tagging_nl( url );	
	}
	else if (location.href.indexOf('/se/sv/') != -1)
	{
		run_tagging_se( url );	
	}
	else if (location.href.indexOf('/ch/de/') != -1)
	{
		run_tagging_ch_de(url);
	}
	else if (location.href.indexOf('/ch/fr/') != -1)
	{
		run_tagging_ch_fr(url);
	}
	else if (location.href.indexOf('/gb/en/') != -1)
	{
		run_tagging_gb(url);
	}
	else if (location.href.indexOf('/de/de/') != -1)
	{
	
		run_tagging_de(url);
	}
	else if (location.href.indexOf('it/it/start#/Cars/9-3convertible/overview/landing') != -1)
	{
	
		run_tagging_it(url);
	}
	else if (location.href.indexOf('it/it/start?version=0&preview=true#/Cars/9-3convertible/overview/landing') != -1)
	{
	
		run_tagging_it(url);
	}
}

// Tagging for CH
function run_tagging_ch_de(taggedUrl){
	run_googleAnalytics_ch_de(taggedUrl);
}

function run_googleAnalytics_ch_de( activeUrl )
{
/*
	* HEKJ: 
	* We don't need these inclusions since ga.js is already included in the start element!
	* If you try and use document.write here it'll trash the whole page!
	*
	* var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	* document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));				
*/
	
	if ( /\/ch\/de\/start\/forms\/xwd-form\/$/.test( activeUrl ) )
	{
		try {
			var pageTracker = _gat._getTracker("UA-5327132-12");
			pageTracker._trackPageview();
		} catch(err) {}
	}
	else if ( /\/ch\/de\/start\/forms\/93_convertible\/$/.test( activeUrl ) )
	{
		try {
			var pageTracker = _gat._getTracker("UA-5327132-12");
			pageTracker._trackPageview();
		} catch(err) {}
	}
	else if ( /\/ch\/de\/start\/forms\/93_x_form\/$/.test( activeUrl ) )
	{
		try {
			var pageTracker = _gat._getTracker("UA-5327132-12");
			pageTracker._trackPageview();
		} catch(err) {}
	}
}

function run_tagging_ch_fr(taggedUrl){
	run_googleAnalytics_ch_fr(taggedUrl);
}

function run_googleAnalytics_ch_fr( activeUrl )
{
/*
	* HEKJ: 
	* We don't need these inclusions since ga.js is already included in the start element!
	* If you try and use document.write here it'll trash the whole page!
	*
	* var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	* document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));				
*/
	
	if ( /\/ch\/fr\/start\/forms\/xwd-form\/$/.test( activeUrl ) )
	{
		try {
			var pageTracker = _gat._getTracker("UA-5327132-12");
			pageTracker._trackPageview();
		} catch(err) {}
	}
	else if ( /\/ch\/fr\/start\/forms\/93_convertible\/$/.test( activeUrl ) )
	{
		try {
			var pageTracker = _gat._getTracker("UA-5327132-12");
			pageTracker._trackPageview();
		} catch(err) {}
	}
	else if ( /\/ch\/fr\/start\/forms\/93_x_form\/$/.test( activeUrl ) )
	{
		try {
			var pageTracker = _gat._getTracker("UA-5327132-12");
			pageTracker._trackPageview();
		} catch(err) {}
	}
}

//  Tagging for the Netherlands
function run_tagging_nl(taggedUrl){
	// Netmining
	// hekj: disabling due to dual tags
	//	nm_image=new Image();
	//		nm_image.src="http://nl-saab.netmining.com/";
}

function run_tagging_se( activeUrl )
{
	var urls = {};

	urls["/se/sv/start/home/"] =								"type=generell;cat=home1";
	urls["/se/sv/start/Cars/Select/"] = 							"type=generell;cat=93des150";
	urls["/se/sv/start/buy-own/landing/"] = 						"type=generell;cat=ppeth992";
	urls["/se/sv/start/buy-own/saab-expressions/landing/"] = 				"type=generell;cat=exp1";
	urls["/se/sv/start/buy-own/accessories/info/"] = 					"type=generell;cat=ass1";
	urls["/se/sv/start/buy-own/business/saab-business/info/"] = 				"type=generell;cat=busi";
	urls["/se/sv/start/buy-own/services/finans/saab-finans/"] = 				"type=generell;cat=finance";
	urls["/se/sv/start/buy-own/sales/diplomat-sales/"] = 					"type=generell;cat=dip";
	urls["/se/sv/start/buy-own/services/more-value/landing/"] = 				"type=generell;cat=more1";
	urls["/se/sv/start/world/landing/"] = 							"type=generell;cat=varld1";
	urls["/se/sv/start/world/news-and-events/newsletter/"] = 				"type=generell;cat=saabi1";
	urls["/se/sv/start/world/news-and-events/news-archive/mervarde/carrousel:all/"] = 	"type=generell;cat=news1";
	urls["/se/sv/start/buy-own/customize-your-saab/find-dealer/"] = 			"type=generell;cat=handl1";
	urls["/se/sv/start/buy-own/customize-your-saab/used-cars/"] = 				"type=generell;cat=begs";
	urls["/se/sv/start/buy-own/customize-your-saab/book-test-drive/"] = 			"type=generell;cat=bokad1";
	urls["/se/sv/start/Cars/9-3sport-combi/overview/landing/"] = 				"type=cars2;cat=93sc";
	urls["/se/sv/start/Cars/9-3sport-combi/overview/features-view/bio-power/"] = 		"type=cars2;cat=93scbio";
	urls["/se/sv/start/Cars/9-3sport-combi/build-your-saab/"] = 				"type=cars2;cat=93bygg";
	urls["/se/sv/start/Cars/9-3sport-sedan/overview/landing/"] = 				"type=cars;cat=93ss";
	urls["/se/sv/start/Cars/9-3sport-sedan/overview/features-view/bio-power/"] = 		"type=cars;cat=93ssbio";
	urls["/se/sv/start/Cars/9-3sport-sedan/build-your-saab/"] = 				"type=cars;cat=93ssbygg";
	urls["/se/sv/start/Cars/9-3convertible/overview/landing/"] = 				"type=cars3;cat=93con";
	urls["/se/sv/start/Cars/9-3convertible/overview/features-view/bio-power/"] = 		"type=cars3;cat=93conbio";
	urls["/se/sv/start/Cars/9-3convertible/build-your-saab/"] = 				"type=cars3;cat=93cobygg";
	urls["/se/sv/start/Cars/9-5sedan/overview/landing/"] = 					"type=cars4;cat=95sed";
	urls["/se/sv/start/Cars/9-5sedan/overview/features-view/bio-power/"] = 			"type=cars4;cat=95sedbio";
	urls["/se/sv/start/Cars/9-5sedan/build-your-saab/"] = 					"type=cars4;cat=95sbygg";
	urls["/se/sv/start/Cars/9-5wagon/overview/landing/"] = 					"type=cars5;cat=95sc";
	urls["/se/sv/start/Cars/9-5wagon/overview/features-view\/bio-power/"] = 		"type=cars5;cat=95scbio";
	urls["/se/sv/start/Cars/9-5wagon/overview/build-your-saab/"] = 				"type=cars5;cat=95scbygg";
	urls["/se/sv/start/world/news-and-events/news-archive/Saab-9-5-Griffin/carrousel:all/"] = "type=other1;cat=griff";
	urls["/se/sv/start/world/innovation/xwd/"] = 						"type=xwd;cat=xwdhome";
	urls["/se/sv/start/Cars/9-3x/overview/landing/"] = 		"type=079-3351;cat=019-3618";
	urls["/se/sv/start/Cars/9-3x/overview/features-view\/bio-power/"] = 		"type=079-3351;cat=029-3788";
	urls["/se/sv/start/Cars/9-3x/build-your-saab/"] = 				"type=079-3351;cat=019-3618";

	caratDoubleClick("http://ad.se.doubleclick.net/activity;src=1596039;", urls, activeUrl);
}

function run_tagging_gb( activeUrl )
{
	
	nm_track(activeUrl);
	var urls = {};
	
	urls["/gb/en/start/home/"] = 									"type=saabs554;cat=saabh416";
	urls["/gb/en/start/Cars/9-3sport-sedan/overview/"] = 			"type=saabs554;cat=93spo627";
	urls["/gb/en/start/Cars/9-3sport-combi/overview/"] = 			"type=saabs554;cat=93spo015";
	urls["/gb/en/start/Cars/9-3convertible/overview/"] = 			"type=saabs554;cat=93con774";
	urls["/gb/en/start/Cars/9-5sedan/overview/"] = 					"type=saabs554;cat=95sal675";
	urls["/gb/en/start/Cars/9-5wagon/overview/"] = 					"type=saabs554;cat=95est605";
	urls["/gb/en/start/buy-own/ownership/insurance/"] = 			"type=saabs554;cat=insur256";
	urls["/gb/en/start/buy-own/saab-approved/approved-used-cars/"] ="type=saabs554;cat=appro057";
	urls["/gb/en/start/buy-own/customize-your-saab/build-your-saab/"] = 	"type=saabs554;cat=build883";
	
	caratDoubleClick("http://fls.doubleclick.net/activityi;src=1637095;", urls, activeUrl);
}

function run_tagging_de( activeUrl )
{
	var urls = {};
	
	//urls["/gb/en/start/home"] = 						"type=saabs554;cat=saabh416";
	
	caratDoubleClick("https://fls.doubleclick.net/activityi;src=1380730;", urls, activeUrl);
}

function caratDoubleClick(urlbase, urls, activeUrl)
{
	ad_parameters = urls[activeUrl];

	if ( ad_parameters != null )
	{
		var axel = Math.random() + "";
		var a = axel * 10000000000000;
		carat_image = new Image();
		carat_image.src = urlbase + ad_parameters + ';ord=' + a + '?';   
	}
}

function run_tagging_it(taggedUrl){
	
	
	var axel2 = Math.random() + "";
	var a2 = axel2 * 10000000000000;
	it_image = new Image();
	it_image.src = 'http://ad.it.doubleclick.net/activity;src=1420339;type=93nhh047;cat=;cabri347ord=' + a2 + '?';   
}