var httpReq;

function f_Change (url)
{
    try
    {
        if(window.XMLHttpRequest)
        {
            httpReq = new XMLHttpRequest();
        }
        else 
        {
            if (window.ActiveXObject) 
            {
                try 
                {
                    httpReq = new ActiveXObject("Msxml2.XMLHTTP");
                } 
                catch (e) 
                {
                    httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                }
            }
        }

        httpReq.onreadystatechange = f_processChange;

        httpReq.open ("GET", url, true);
        httpReq.send(null);
    }
    catch (e)
    {
        alert (e.message + ":" + e.description);
    }
}

function f_processChange ()
{
    if (httpReq.readyState == 4)
    {
        if (httpReq.status == 200)
        {
            document.location.reload (); 
        }
    }
}