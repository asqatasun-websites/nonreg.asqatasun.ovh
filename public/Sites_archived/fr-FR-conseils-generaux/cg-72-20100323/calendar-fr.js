// ** I18N
Calendar._DN = new Array
(objThesaurus.translate("jscalDim"),
 objThesaurus.translate("jscalLun"),
 objThesaurus.translate("jscalMar"),
 objThesaurus.translate("jscalMer"),
 objThesaurus.translate("jscalJeu"),
 objThesaurus.translate("jscalVen"),
 objThesaurus.translate("jscalSam"),
 objThesaurus.translate("jscalDim"));
Calendar._MN = new Array
(objThesaurus.translate("jscalJan"),
 objThesaurus.translate("jscalFev"),
 objThesaurus.translate("jscalMars"),
 objThesaurus.translate("jscalAvr"),
 objThesaurus.translate("jscalMai"),
 objThesaurus.translate("jscalJun"),
 objThesaurus.translate("jscalJul"),
 objThesaurus.translate("jscalAou"),
 objThesaurus.translate("jscalSep"),
 objThesaurus.translate("jscalOct"),
 objThesaurus.translate("jscalNov"),
 objThesaurus.translate("jscalDec"));

// tooltips
Calendar._TT = {};
Calendar._TT["TOGGLE"] = objThesaurus.translate("jscalTOGGLE");
Calendar._TT["PREV_YEAR"] = objThesaurus.translate("jscalPREV_YEAR");
Calendar._TT["PREV_MONTH"] = objThesaurus.translate("jscalPREV_MONTH");
Calendar._TT["GO_TODAY"] = objThesaurus.translate("jscalGO_TODAY");
Calendar._TT["NEXT_MONTH"] = objThesaurus.translate("jscalNEXT_MONTH");
Calendar._TT["NEXT_YEAR"] = objThesaurus.translate("jscalNEXT_YEAR");
Calendar._TT["SEL_DATE"] = objThesaurus.translate("jscalSEL_DATE");
Calendar._TT["DRAG_TO_MOVE"] = objThesaurus.translate("jscalDRAG_TO_MOVE");
Calendar._TT["PART_TODAY"] = objThesaurus.translate("jscalPART_TODAY");
Calendar._TT["MON_FIRST"] = objThesaurus.translate("jscalMON_FIRST");
Calendar._TT["SUN_FIRST"] = objThesaurus.translate("jscalSUN_FIRST");
Calendar._TT["CLOSE"] = objThesaurus.translate("jscalCLOSE");
Calendar._TT["TODAY"] = objThesaurus.translate("jscalTODAY");
Calendar._TT["TIME_PART"] = objThesaurus.translate("jscalTIME_PART");

// date formats
Calendar._TT["DEF_DATE_FORMAT"] = objThesaurus.translate("jscalDEF_DATE_FORMAT");
Calendar._TT["TT_DATE_FORMAT"] = objThesaurus.translate("jscalTT_DATE_FORMAT");

Calendar._TT["WK"] = objThesaurus.translate("jscalWK");
