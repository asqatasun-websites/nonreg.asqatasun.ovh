var initNav = function () {
   $(".nav li ul").hide();
	$(".nav li ul").css("visibility", "visible");
	$(".nav li.rub-active ul").show();
	$(".nav li.navNiveau1").hover(function(){
         if(!$(this).hasClass("rub-active")){
				$(this).addClass("hover");
				$(".nav li.rub-active ul").hide();
	         var subNav = $(this).find("ul");
	         if(subNav){
	            if(!subNav.is(":visible")){					
						subNav.show();
					}
	         }
			}
      }, function(){
         if(!$(this).hasClass("rub-active")){
				$(this).removeClass("hover");
	         var subNav = $(this).find("ul");
	         if(subNav){
	            subNav.hide();
	         }
				$(".nav li.rub-active ul").show();
			}
   });
}

var initMenu= function(){
	 $(".menu li ul").css("display", "none");
	 $(".menu li.menu-actif ul").css("display", "block");
	 $(".menu li.menu-actif ul").addClass("menu-ouvert");
	 $(".menu li.menu-actif ul li ul").css("display", "none");
	 $(".menu li.menu-actif ul li.menu-actif ul").css("display", "block");
	 $(".menu li.menu-actif ul li.menu-actif ul").addClass("menu-ouvert");
	 //Click sur les fleches du menu
	 $(".voir-menu").toggle(function(){
			//Si le sous-menu etait ferme, on l'ouvre
			if(!$(this).hasClass("voir-menu-actif")){				
				$(this).addClass("voir-menu-actif"); //on change l'etat de la fleche en actif
				var menuOuvert = $(this).parent("li").siblings("li.menu-actif").eq(0);
				menuOuvert.find("ul.menu-ouvert").eq(0).css("display", "none");
				menuOuvert.find("ul.menu-ouvert").eq(0).removeClass("menu-ouvert");
				menuOuvert.find("a.voir-menu-actif").eq(0).removeClass("voir-menu-actif");
				menuOuvert.find("a.menu-actif").eq(0).removeClass("menu-actif");
				menuOuvert.removeClass("menu-actif");
				var subMenu = $(this).siblings("ul").eq(0);
	         if(subMenu){		
					subMenu.css("display", "block");
					subMenu.addClass("menu-ouvert");					
	         }				
				$(this).next("a").addClass("menu-actif"); //On met en gras le menu actif
				$(this).parent("li").addClass("menu-actif");
			}
			//Si le sous-menu etait ouvert, on le ferme
			else{				
				$(this).removeClass("voir-menu-actif"); //on change l'etat de la fleche en inactif;				
				var subMenu = $(this).siblings("ul").eq(0);
	         if(subMenu){
	            subMenu.css("display", "none");
					subMenu.removeClass("menu-ouvert");
	         }
				$(this).next("a").removeClass("menu-actif"); //On enleve le gras du menu inactif
				$(this).parent("li").removeClass("menu-actif");
			}
			ajusterHauteur();
      }, function(){
			//Si le sous-menu etait ouvert, on le ferme
			if($(this).hasClass("voir-menu-actif")){
         	$(this).removeClass("voir-menu-actif"); //on change l'etat de la fleche en inactif
         	var subMenu = $(this).siblings("ul").eq(0);
         	if(subMenu){
            	subMenu.css("display", "none");
					subMenu.removeClass("menu-ouvert");
         	}
				$(this).next("a").removeClass("menu-actif"); //On enleve le gras du menu inactif
				$(this).parent("li").removeClass("menu-actif");
			}
			//Si le sous-menu etait ferme, on l'ouvre
			else{
				$(this).addClass("voir-menu-actif"); //on change l'etat de la fleche en actif
				var menuOuvert = $(this).parent("li").siblings("li.menu-actif").eq(0);
				menuOuvert.find("ul.menu-ouvert").eq(0).css("display", "none");
				menuOuvert.find("ul.menu-ouvert").eq(0).removeClass("menu-ouvert");
				menuOuvert.find("a.voir-menu-actif").eq(0).removeClass("voir-menu-actif");
				menuOuvert.find("a.menu-actif").eq(0).removeClass("menu-actif");
				menuOuvert.removeClass("menu-actif");
				var subMenu = $(this).siblings("ul").eq(0);
	         if(subMenu){		
					subMenu.css("display", "block");
					subMenu.addClass("menu-ouvert");
	         }				
				$(this).next("a").addClass("menu-actif"); //On met en gras le menu actif
				$(this).parent("li").addClass("menu-actif");
			}
			ajusterHauteur();
   });
}

var initOnglet = function () {
   $(".ongletLayer").hide();
	$(".ongletLayer").eq(0).show();
	$(".nav-onglet a").click(function(){
   	$(".onglet-actif").removeClass("onglet-actif");
		$(".ongletLayer").hide();
		$(this).addClass("onglet-actif");
		var ongletId = $(this).attr("href");
		$(ongletId).fadeIn("slow");
		return false;
   });
}

var initSPOnglet = function () {
   $(".SPOngletLayer").hide();
	$(".SPOngletLayer").eq(0).show();
	$(".SP-nav-onglet a").click(function(){
   	$(".SP-onglet-actif").removeClass("SP-onglet-actif");
		$(".SPOngletLayer").hide();
		$(this).addClass("SP-onglet-actif");
		var ongletId = $(this).attr("href");
		$(ongletId).fadeIn("slow");
		return false;
   });
}

var tailleText = 2;
var zoomText = function(changerTaille, divId){
	var fonts = ["0.72em", "0.9em", "1em", "1.33em", "1.66em", "2em", "2.33em", "2.66em"]; //8, 10, 12, 16, 20, 24, 28, 32
	if(changerTaille==0) tailleText = 2;
	if(changerTaille==-1 & tailleText>0) tailleText--;
	if(changerTaille==1 & tailleText<7) tailleText++;
	$("#" + divId).css("font-size", fonts[tailleText]);
}

var initImprimer = function(){
	$(".version-imprimable").click( function() {
        window.open($(this).attr("href"), "imprimer", "menubar=no, resizable=no, status=no, scrollbars=yes, toolbar=no, location=no, directories=no, width=700, height=670");
        return false;
   });
}

var HAUTEURCONTENU = 0;

var ajusterHauteur = function(){
	if($(".contenu-large").is("div")){
		if($(".menu").height() > $(".contenu-large").height()){
			$(".contenu-large").css("height", $(".menu").height() + "px");
		}
		else{
			$(".contenu-large").css("height", HAUTEURCONTENU + "px");
		}
	}
	else if($(".contenu").is("div")){
		if($(".menu").height() > $(".contenu").height()){
			$(".contenu").css("height", $(".menu").height() + "px");
		}
		else{
			$(".contenu").css("height", HAUTEURCONTENU + "px");
		}
	}
}

$(document).ready(function () {
   if($(".nav").is("div")) initNav();
	if($(".menu").is("ul")) initMenu();
	if($(".nav-onglet").is("div")) initOnglet();
	if($(".SP-nav-onglet").is("div")) initSPOnglet();
	if($(".version-imprimable").is("a")) initImprimer();
	//Si bandeaux pub
	if($(".slideshow").is("p")){
		$('.slideshow').cycle({
			fx: 'fade',
			timeout: 15000
		});
	}
	if($(".contenu-large").is("div")){
		HAUTEURCONTENU = $(".contenu-large").height();
		if($("#isRubBloc").is("div")) HAUTEURCONTENU = HAUTEURCONTENU + 120;
	}
	else if($(".contenu").is("div")){
		HAUTEURCONTENU = $(".contenu").height();
	}
	ajusterHauteur();
});