

/* 3 : 3  */

    /*
    *********************** ENGINES MENU ****************************
    */



       /*
        main struct of pagebody
        */

        #menuwrapper {
            background: url( "http://www.cr-lorraine.fr/jahia/jsp/jahia/css/menubg.gif" ) #fff repeat-y right 50%;
            display: inline;
            width: 100%;
        }

        /* main content boxes */
        div.content {
            float: left;
            width: 65%;
            margin: auto;
        }

        #editor {
            background-color: #fff;
            margin-left: 10px;
        }

        /* onglets */
        #menubar {
            float: left;
            clear: both;
            display: block;
            width: 100%;
            margin-top: 12px !important;
            margin-top: 70px;
            margin-bottom: 10px;
            border-bottom: 1px solid #336699;
        }

        /* modif joe 4 tabs cross-browser */

        #tabs, #floatright_tabs {
            height: 20px;
            margin: 0;
            padding-left: 0px;
        }

        #floatright_tabs {
                    float: right;
        }

        #tabs li, #floatright_tabs li  {
            display: inline;
            list-style-type: none;
        }

        #tabs span,
            #tabs a,
            #floatright_tabs span,
            #floatright_tabs a
             {
            display: block;
            margin-right: 2px;
        }

        /* 4 links onglets */
        #tabs a:link,
            #tabs a:visited,
                #floatright_tabs a:link,
            #floatright_tabs a:visited {
            float: left;
            background: #398EC3;
            font-weight: bold;
            padding: 2px 4px 2px 4px;
            text-decoration: none;
            color: #FFFFFF;
            margin-top: 4px;
        }

        /* end modif */

        /* on fly hover */
        #tabs a:hover, #floatright_tabs a:hover {
            text-decoration: none;
            background-color: #336699;
            font-weight: bold;
            padding: 4px 4px 4px 4px;
            margin-top: 0px;
        }

        /* current tab */
        #tabs .current, #floatright_tabs .current {
            float: left;
            text-decoration: none;
            background-color: #336699;
            font-weight: bold;
            padding: 4px 4px 4px 4px;
            margin-top: 0px;
            color: #FFFFFF;
        }

        .clearing {
            clear: both;
            height: 0px;
        }

        /* left menu  */
        div.leftMenu {
            background-color: #FFFFFF;
            border-right: #808080 1px solid;
            border-left: #808080 1px solid;
            border-bottom: #808080 1px solid;
            float: left; /* mod joe */
            clear: both; /* end */
            width: 220px;
        }

        div.leftMenu ul a {
            color: #000000;
            text-decoration: none;
            display: block;
        /* width : 100 %; */
        /* height : 100 %; */
        }

        div.leftMenu ul li {
            display: inline;
            margin: 0px;
            padding: 0px;
        }

        div.leftMenu ul li a,
            div.leftMenu ul li span.current {
            background: url( http://www.cr-lorraine.fr/jahia/jsp/jahia/css/mainitembg.gif ) #CCCCCC repeat-x;
            display: block;
            font-size: 13px;
            padding: 3px 10px 3px 10px;
        }

        div.leftMenu ul li a:hover {
            background-position: 0% -32px;
            text-decoration: none;
            color: #000;
        }

        div.leftMenu ul li ul li a {
            background: none;
            background-color: #FFFFFF;
            border-bottom: none;
            font-weight: normal;
            padding: 3px 15px 3px 10px;
            margin-left: 5px;
        }

        div.leftMenu ul li ul li a:hover,
            span.selectedfield {
            text-decoration: underline;
            background-position: 0% 0px;
            color: #398EC3;
        }

        span.selectedfield {
            border-bottom: none;
            display: block;
            font-size: 11px;
            font-weight: bold;
            padding: 4px 11px 5px 16px;
        }

        div.leftMenu ul,
            div.leftMenu ul li ul {
            list-style: none;
            margin: 0px;
            padding: 0px;
        }

        div.leftMenu ul li ul {
            display: block;
        }

        div.leftMenu ul li span.current {
            background-position: 0% -32px;
            display: block;
            color: #000;
            font-weight: bold;
        }







    /* ------------------------------------------------ */
    /*                 ZIMBRA STYLE                     */
    /* ------------------------------------------------ */

    /* Timebase publishing dialog Style */

    .TimeBasePublisningDwtMessageDialog .DialogTable {
        #background-color: #999999;
        width: 350px;
    }

    .TimeBasePublisningDwtMessageDialog .DialogBody {
        background-color: #f1f1f1;
    }

    .TimeBasePublisningDwtMessageDialog .DialogTitle {
      background-color: #66a9da;
      height: 25px;
      font-size: 12px;
    }

    .TimeBasePublisningDwtMessageDialog .DialogTitleCell {
      padding: 5px;
    }

    .TimeBasePublisningDwtMessageDialog .ImgDialogInset_BL{background-image:url('core_css.jsp');background-position:0px 0px;background-repeat:no-repeat;width:10px;height:10px;overflow:hidden;}
    .TimeBasePublisningDwtMessageDialog .ImgDialogInset_BR{background-image:url('core_css.jsp');background-position:0px 0px;background-repeat:no-repeat;width:10px;height:10px;overflow:hidden;}
    .TimeBasePublisningDwtMessageDialog .ImgDialogInset_B__H{background-image:url('core_css.jsp');background-position:0px 0px;background-repeat:repeat-x;height:10px;overflow:hidden;}
    .TimeBasePublisningDwtMessageDialog .ImgDialogInset_L__V{background-image:url('core_css.jsp');background-position:0px 0px;background-repeat:repeat-y;width:10px;overflow:hidden;}
    .TimeBasePublisningDwtMessageDialog .ImgDialogInset_R__V{background-image:url('core_css.jsp');background-position:0px 0px;background-repeat:repeat-y;width:10px;overflow:hidden;}
    .TimeBasePublisningDwtMessageDialog .ImgDialogInset_TL{background-image:url('core_css.jsp');background-position:0px 0px;background-repeat:no-repeat;width:10px;height:10px;overflow:hidden;}
    .TimeBasePublisningDwtMessageDialog .ImgDialogInset_TR{background-image:url('core_css.jsp');background-position:0px 0px;background-repeat:no-repeat;width:10px;height:10px;overflow:hidden;}
    .TimeBasePublisningDwtMessageDialog .ImgDialogInset_T__H{background-image:url('core_css.jsp');background-position:0px 0px;background-repeat:repeat-x;height:10px;overflow:hidden;}
    .TimeBasePublisningDwtMessageDialog .ImgDialogOutset_BL{background-color: #999999;background-image:url('core_css.jsp');background-position:0px 0px;background-repeat:no-repeat;width:4px;height:4px;overflow:hidden;}
    .TimeBasePublisningDwtMessageDialog .ImgDialogOutset_BR{background-color: #999999;background-image:url('core_css.jsp');background-position:0px 0px;background-repeat:no-repeat;width:4px;height:4px;overflow:hidden;}
    .TimeBasePublisningDwtMessageDialog .ImgDialogOutset_B__H{background-color: #999999;background-image:url('core_css.jsp');background-position:0px 0px;background-repeat:repeat-x;height:4px;overflow:hidden;}
    .TimeBasePublisningDwtMessageDialog .ImgDialogOutset_L__V{background-color: #999999;background-image:url('core_css.jsp');background-position:0px 0px;background-repeat:repeat-y;width:4px;overflow:hidden;}
    .TimeBasePublisningDwtMessageDialog .ImgDialogOutset_R__V{background-color: #999999;background-image:url('core_css.jsp');background-position:0px 0px;background-repeat:repeat-y;width:4px;overflow:hidden;}
    .TimeBasePublisningDwtMessageDialog .ImgDialogOutset_TL{background-color: #999999;background-image:url('core_css.jsp');background-position:0px 0px;background-repeat:no-repeat;width:4px;height:4px;overflow:hidden;}
    .TimeBasePublisningDwtMessageDialog .ImgDialogOutset_TR{background-color: #999999;background-image:url('core_css.jsp');background-position:0px 0px;background-repeat:no-repeat;width:4px;height:4px;overflow:hidden;}
    .TimeBasePublisningDwtMessageDialog .ImgDialogOutset_T__H{background-color: #999999;background-image:url('core_css.jsp');background-position:0px 0px;background-repeat:repeat-x;height:4px;overflow:hidden;}

    .TimeBasePublisningDwtMessageDialog .border_shadow_v .ImgShadowBig_R__V {
      height: 25px !important;
    }

    .TimeBasePublisningDwtMessageDialog .dialog_bottom_left_corner {
      background-color: #999999;
    }

    .TimeBasePublisningDwtMessageDialog .dialog_bottom_right_corner {
      background-color: #999999;
    }

    .TimeBasePublisningDwtMessageDialog .timeBasedPublishingTitle {
      height: 35px;
      font-size: 12px;
      font-weight: bold;
      text-align: left;
      vertical-align: top;
    }

    .TimeBasePublisningDwtMessageDialog .timeBasedPublishingLabel,
    .TimeBasePublisningDwtMessageDialog .timeBasedPublishingValue {
      float: left;
      height: 20px;
      font-size: 12px;
      text-align: left;
      vertical-align: top;
      display:inline;
    }

    /*
    * Modifiers
    *
    * A modifier represents a change in state that is the same across several objects. For example, different types of
    * buttons all display activation and triggering the same. There's no "selected" modifier because the different
    * objects that can become selected display it differently.
    * <p>
    * The reason that there are chained selectors is so that the modifier won't lose in precedence due to specificity.
    * By itself, the modifier is a single class, so that component of the specificity is 1. A matching declaration with
    * a conflicting style (namely, background-color) will win on specificity if it has two classes. So we need to give
    * the two-class version of the modifier, which will win because it comes later in this file.
    *
    * So far these just apply to buttons.
    */

    /* button in a toolbar */
    .transparent {
        background-color: transparent;
    }
    /* button in a form or dialog */
    .contrast {
        background-color: rgb(198, 197, 215);
    }
    /* button that is the default for some action */
    .active, .DwtDialog .DwtButton.active {
        background-color: rgb(245, 245, 245);
    }
    /* button onmouseover */
    .activated, .DwtDialog .DwtButton.activated, .DwtDialog .ColorButton.activated {
        background-color: rgb(249, 221, 135);
    }
    /* button onmousedown */
    .triggered, .DwtDialog .DwtButton.triggered, .DwtDialog .ColorButton.triggered {
        background-color: rgb(240, 160, 38);
    }

    /* button toggled on */
    .toggled {background-color: rgb(255, 255, 255);}

    /* button in the tab bar */
    .DwtTabButton {
        background-color: transparent;
    }

    .DwtTabButton_active {
        background-color: rgb(245, 245, 245);
    }

    .DwtTabButton_inactive {
        background-color: rgb(198, 197, 215);
    }

    .inactive {
        background-color: rgb(198, 197, 215);
    }

    .DwtTabButton.active {
        background-color: rgb(245, 245, 245);
    }

    .active {
        background-color: rgb(245, 245, 245);
    }

    .DwtTabViewPage {
        background-color: rgb(245, 245, 245);
        height:600px;
        overflow: auto;
    }


    .DwtTabViewPage .TitleBar {
        background-color: rgb(198, 197, 215);
        filter:progid:DXImageTransform.Microsoft.Gradient(startColorStr=#FFF0F0F0,endColorStr=#FF9997B5);
    }
    .DwtTabViewPage .Title {
        font: bold 12pt Arial, Helvetica, sans-serif;
        padding: 3px;
    }


    .DwtPropertyPage {
        background-color: rgb(245, 245, 245);
    }
    .DwtPropertyPage .TitleBar {
        background-color: rgb(198, 197, 215);
        filter:progid:DXImageTransform.Microsoft.Gradient(startColorStr=#FFF0F0F0,endColorStr=#FF9997B5);
        padding:0px;
        margin: 0px;
    }
    .DwtPropertyPage .Title {
        font: bold 12pt Arial, Helvetica, sans-serif;
        padding: 3px;
    }

    .DwtTabViewPage .DwtButton {
      width: 60px;
    }


    #fieldDiffsContainer{
      margin:0px;
      padding:0px;
      height:600px;
    }

    #fieldDiffsContainer .undefined_type,
      #fieldDiffsContainer .number_type,
      #fieldDiffsContainer .small_type,
      #fieldDiffsContainer .big_type,
      #fieldDiffsContainer .date_type,
      #fieldDiffsContainer .page_type,
      #fieldDiffsContainer .file_type,
      #fieldDiffsContainer .app_type,
      #fieldDiffsContainer .boolean_type,
      #fieldDiffsContainer .color_type,
      #fieldDiffsContainer .category_type,
      #fieldDiffsContainer .small_shared_type,
      #fieldDiffsContainer .undefined_type_big,
      #fieldDiffsContainer .number_type_big,
      #fieldDiffsContainer .small_type_big,
      #fieldDiffsContainer .big_type_big,
      #fieldDiffsContainer .date_type_big,
      #fieldDiffsContainer .page_type_big,
      #fieldDiffsContainer .file_type_big,
      #fieldDiffsContainer .app_type_big,
      #fieldDiffsContainer .boolean_type_big,
      #fieldDiffsContainer .color_type_big,
      #fieldDiffsContainer .category_type_big,
      #fieldDiffsContainer .small_shared_type_big {
        color: #398EC3;
        text-decoration: none;
        font-weight: bold;
    }

    /*
    #fieldDiffsContainer .DwtTabBar {
      border: thin brown solid;
    }*/

    #fieldDiffsContainer .DwtTabButton {
      margin: 0px;
      padding: 0px;
    }

    #fieldDiffsContainer .DwtTabButton .DwtTabButton,
    #fieldDiffsContainer .DwtTabButton .DwtTabButton-activated,
    #fieldDiffsContainer .DwtTabButton .DwtTabButton-triggered
    {
      padding: 5px 5px 5px 5px;
      height: 20px;
    }

    #fieldDiffsContainer .DwtTabButton table {
      margin-top: 0px;
    }

    #fieldDiffsContainer .DwtTabButton .Text, #fieldDiffsContainer .DwtTabButton .DisabledText {
      font-weight: bold;
      font-size: 14px;
    }

    #fieldDiffsContainer .FieldDiffListView .Row {
      padding-top: 10px;
      padding-bottom: 10px;
      border-bottom: 0px;

    }

    #fieldDiffsContainer .FieldDiffListView {
        margin: 0px;
      padding: 0px;
      width: 100%;
      overflow-x: hidden;
    }

    #fieldDiffsContainer .DwtHtmlEditor, #fieldDiffsContainer .DwtHtmlEditor_small {
      width: 100%;
    }

    #fieldDiffsContainer .DwtHtmlEditorIFrame {
      border: 1px black solid;
      width: 99%;
    }

    #fieldDiffsContainer .DwtHtmlEditor, #fieldDiffsContainer .DwtHtmlEditor .DwtHtmlEditorIFrame {
      height: 200px;
    }

    #fieldDiffsContainer .DwtHtmlEditor_small, #fieldDiffsContainer .DwtHtmlEditor_small .DwtHtmlEditorIFrame {
      height: 50px;
    }


    /*
    ********************** TABS **************************
    */


    div.tab {
        float: left;
        padding-left:10px;
    }

    div.tab a {
        background: url(tab_right.gif) no-repeat right top;
        float: left;
        margin-left: 6px;
        margin-top: 10px;
        text-decoration: none;
    }

    div.tab a:link,
    div.tab a:visited,
    div.tab a:active,
    div.tab a:hover {
        color: #515C6A;
        font-size: 11px;
    }

    div.tab a:hover {
        background-position: 100% -26px;
    }

    div.tab a:hover span {
        background-position: 0% -26px;
    }

    div.tab li {
        display: inline;
        margin: 0px;
        padding: 0px;
    }

    div.tab li.selected a {
        background-position: 100% -26px;
    }

    div.tab li.selected span {
        background-position: 0% -26px;
        padding-bottom: 6px;
    }

    div.tab span {
        background: url(tab_left.gif) no-repeat left top;
        display: block;
        float: left;
        padding: 5px 9px;
        white-space: nowrap;
    }

    /* Commented Backslash Hack hides rule from IE5-Mac \*/
    div.tab span {
        float: none;
    }
    /* End IE5-Mac hack */

    div.tab ul {
        list-style: none;
        margin: 0px;
        padding: 0px;
    }








    /*
    ********************** TOPMENU **************************
    */

    div.topmenus {
      clear: both;
        padding-left:10px;
        font-size: 11px;
        width:100%;
        height : 20px;
    }

    div.topmenus a {
        float: left;
        margin-left: 1px;
        margin-top: 3px;
        font-size: 11px;
        text-decoration: none;
    }

    div.topmenus a:link,
    div.topmenus a:visited,
    div.topmenus a:active,
    div.topmenus a:hover {
        color: #515C6A;
    }

    div.topmenus a:hover {
      background-color : #EEEEEE;
    }

    div.topmenus span {
        float: left;
        margin-left: 1px;
        margin-top: 3px;

        /*border: 1px solid;*/
    }

    div.topmenus img {
        float: left;
        margin-left: 1px;
        margin-top: 3px;

        /*border: 1px solid;*/
    }

    #filemanagerLinkBoxFirstRow a,
    #filemanagerLinkBoxFirstRow img {
      margin: 0px;
      padding: 0px;
        /*font-size: 10px; */
    }


    #processingBoxFirstRow a,
    #processingBoxFirstRow img {
      margin: 0px;
      padding: 0px;
      font-size: 11px;
    }

    #chatBoxFirstRow a,
    #chatBoxFirstRow img {
      margin: 0px;
      padding: 0px;
      font-size: 11px;
    }

    .workflowState a {
      margin: 0px;
      padding: 0px;
    }
