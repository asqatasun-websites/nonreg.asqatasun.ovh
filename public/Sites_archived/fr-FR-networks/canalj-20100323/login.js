var baseUrl='';
var baseIdForm='';
var urlVote = '';
function getLoginBoxByCookie(LayerId, editUrl, forgotUrl, registerUrl, logoutUrl, loginUrl, BaseUrl, BaseImg)
{
	var 	baseUrl = BaseUrl;
	var texte = '';
	if (BaseImg == undefined)
		BaseImg = "bugundef";
	if(GetCookie("user_logged") == "oui"){
		var pseudo = GetCookie("login_canalj");
		var prenom = GetCookie("prenom_canalj");
		if(prenom === null || prenom === false || prenom === undefined || prenom === '' || typeof prenom  === 'null' ||  typeof prenom  === 'undefined')
			prenom = '';
      
        texte+='<div class="identified">';
        texte+='Bonjour <span> ' + prenom + '</span>, ça va ?</div> ';
        texte+='<a href="' + editUrl + '" class="mon_compte">mon compte</a>';
        texte+='<a href="' + logoutUrl + '" class="se_deconnecter">se déconnecter</a>';
        
	}else{
		texte+='<div class="identification">';		
		texte+='<form method="post" action="" name="mon_compte" id="mon_compte" onsubmit="return false;">';
		texte+='<input type="text" name="UserLogin" id="pseudo" tabindex="1" onFocus="this.value=\'\'" value="Pseudo" class="log" />';
	    texte+='<input type="password" class="pass" name="UserPassword" id="passe" tabindex="2" onFocus="this.value=\'\'" value="password" onkeypress="if (event.which == 13 || event.keyCode == 13) loginUser( \'' + LayerId + '\', \'' + editUrl + '\', \'' + forgotUrl + '\', \'' + registerUrl + '\', \'' + logoutUrl + '\', \'' + loginUrl + '\', \'' + BaseUrl + '\', \'' + BaseImg + '\', \'loginContent\', \'mon_compte\' ); else return true; "/>';  
	    texte+='<input type="image" name="d" src="'+BaseImg+'bt_identification_connecter.png" class="bt_connecter" onmouseover="this.src=\''+BaseImg+'bt_identif_connecter_on.png\'"  onmouseout="this.src=\''+BaseImg+'bt_identification_connecter.png\'"  onclick="loginUser( \'' + LayerId + '\', \'' + editUrl + '\', \'' + forgotUrl + '\', \'' + registerUrl + '\', \'' + logoutUrl + '\', \'' + loginUrl + '\', \'' + BaseUrl + '\', \'' + BaseImg + '\', \'loginContent\', \'mon_compte\' );"  />';
		texte+='<div class="oubli">';
	    texte+='<input type="checkbox" id="se_souvenir" name="Cookie" /><label for="se_souvenir">Se souvenir de moi ?</label><br />';
	    texte+='<a href="javascript:;" onclick="callModalBox(\''+forgotUrl+'\', {title: \'Login\', height: 383, width: 640, overlayOpacity: 0.75});">Mot de passe oublié</a>';
	    texte+='</div>';
	    texte+='</form>';
	    texte+='</div>';
	    texte+='<a href="' + registerUrl + '" class="inscription">S\'inscrire</a>';
			
	}

	document.getElementById(LayerId).innerHTML = texte + '<div id="loginContent" style="display:none"></div>';
	if(GetCookie("pseudo_has_error") == "oui"){
		setCookie("pseudo_has_error","non");
		window.location.href= "/action/edit";
	}
}



function callModalBox(url, ModalBoxOptions) {
	document.location.href = "#";
	document.body.style.cursor = "wait";
	var opt = {
	    // Use GET
	    method: 'get',
	    // Handle successful response
	    onSuccess: function(t) {
	        document.body.style.cursor = "default";
	        Modalbox.show(t.responseText, ModalBoxOptions);
	    },
	    // Handle 404
	    on404: function(t) {
	        //alert('Error 404: location "' + t.statusText + '" was not found.');
	    },
	    // Handle other errors
	    onFailure: function(t) {
	        //alert('Error ' + t.status + ' -- ' + t.statusText);
	    }

	};
	
	new Ajax.Request(url, opt);
}

function showLoading(LayerId, BaseUrl){
	var loadingpic = new Image();
  	  	
  	document.getElementById(LayerId).style.visibility='hidden';
}

function verifyRules( loginUrl, formName )
{	
	if(GetCookie("user_logged") == "oui"){
		document.getElementById(formName).submit();
	}else{		
		baseIdForm = formName;
		callModalBox(loginUrl, {title: 'Login', height: 383, width: 640, overlayOpacity: 0.75});
	}
}

function verifyUpload( value_image,value_video,type, formName )
{	
	if(type == 1){
		if(value_image == ""){
		alert('Il manque le fichier !');
		}else{
		document.getElementById(formName).submit();
		}
	}else{		
		if(value_video == ""){
			alert('Il manque le fichier !');
		}else{		
			document.getElementById(formName).submit();
		}
	}
}


function loginUserBox(loginUrl, sLoginContent, idForm) {
	
	new Ajax.Updater( sLoginContent, loginUrl, {asynchronous:true, evalScripts:false, method:'post', parameters:Form.serialize(idForm), onComplete:function(){ 
	if(GetCookie("user_logged") == "oui")
	{
		getLoginBoxByCookie('identification','/action/edit','/action/forgotpassword','/action/login','/action/logout?ReturnUrl='+sHost, '/action/login','/','/design/canalj/images/'); 
		if(baseIdForm.substring(0,10) == 'addFavoris')
		{
			new Ajax.Request(sHost+'/action/bookmark',{asynchronous:true,method: 'post', parameters:Form.serialize(baseIdForm),onComplete:	function(t){
				alert('Ce contenu a été ajouté à tes favoris');
				Modalbox.hide();}
			});
		}
		else if(baseIdForm.substring(0,4) == 'vote')
		{
			new Ajax.Request(urlVote,{asynchronous:true,onComplete:	function(t){
				eval(t.responseText);
				$('formulaire').hide();
				if (result.deja_vote == 0)
				{
					$('reponse').show();
				}
				else
				{
					$('deja_vote').show();
				}		
				
			}});
			Modalbox.hide();
		}
		else
		{
			if (document.getElementById(baseIdForm) != null)
			{
				document.getElementById(baseIdForm).submit();
			}
			
			Modalbox.hide();
		}
	}
	}});
	
}

function loginUserEmbedded(LayerId, loginUrl, BaseUrl, returnUrl, idForm ) {
	
	if( typeof(idForm) == 'undefined' ){
		idForm = 'mon_compte';
	}
	
	new Ajax.Updater( 'loginContent', loginUrl, {asynchronous:true, evalScripts:false, method:'post', parameters:Form.serialize(idForm), onComplete:function(){ 

	if(GetCookie("user_logged") == "oui"){
		window.location.href=returnUrl;
	} else {	
		Modalbox.show(document.getElementById('loginContent'), {title: 'Login', height: 383, width: 640, overlayOpacity: 0.75});
	 	document.getElementById(LayerId).style.visibility='visible';
	}
	}});
	
	//showLoading(LayerId, BaseUrl);
}

function loginUser(LayerId, editUrl, forgotUrl, registerUrl, logoutUrl, loginUrl, BaseUrl, BaseImg, sLoginContent, idForm ,bSeSouvenir ) {
	
	if( typeof(idForm) == 'undefined' ){
		idForm = 'mon_compte';
	}

	document.getElementById(LayerId).style.visibility='hidden';
	new Ajax.Updater( sLoginContent, loginUrl, {asynchronous:true, evalScripts:false, method:'post', parameters:Form.serialize(idForm), onComplete:function(){ 
		
		if(GetCookie("user_logged") == "oui"){
			getLoginBoxByCookie(LayerId, editUrl, forgotUrl, registerUrl, logoutUrl, loginUrl, BaseUrl, BaseImg );
			document.getElementById(LayerId).style.visibility='visible';
		} else {
			Modalbox.show(document.getElementById(sLoginContent), {title: 'Login', height: 383, width: 640, overlayOpacity: 0.75});
		 	document.getElementById(LayerId).style.visibility='visible';
		}

	}});
	
	if(window.onUserLogged?true:false) {
		void(onUserLogged());
	}
	//showLoading(LayerId, BaseUrl);
}

function RefreshLoginBoxAvatar () {
	if(GetCookie("user_logged") == "oui"){
		var avatar = GetCookie("avatar_t7j");
	
		if (avatar != '') {
			document.getElementById('loginBoxAvatar').src= baseUrl + avatar + '?' + getTimeStamp();
		}
	}
}

function getTimeStamp() {
	var currentTime = new Date();
	return currentTime.getTime();
}

function loginUserCommentaire(loginUrl, sLoginContent,idForm, noeud) {
	
	new Ajax.Updater( sLoginContent,loginUrl, {asynchronous:true, evalScripts:false, method:'post', parameters:Form.serialize(idForm), onComplete:function(){
		if(GetCookie("user_logged") == "oui")
		{
			getLoginBoxByCookie('identification','/action/edit','/action/forgotpassword','/action/login','/action/logout?ReturnUrl='+sHost, '/action/login','/','/design/gulli/images/'); 
			new Ajax.Updater(sLoginContent, sHost+'/commanage/popup_comment/'+noeud, {asynchronous:true});
			
		}
	}});
}

function addFavori( url ,loginUrl , formName, div)
{	
	baseIdForm = formName;
	if(GetCookie("user_logged") == "oui")
	{
		document.body.style.cursor = "wait";
		new Ajax.Request(sHost+url,{asynchronous:true,method: 'post', parameters:Form.serialize(formName),onComplete:	function(t){
			document.body.style.cursor = "default";
			alert('Ce contenu a été ajouté à tes favoris');}
		});
	}
	else
	{
		callModalBox(loginUrl, {title: 'Login', height: 383, width: 640, overlayOpacity: 0.75});
	}
}

function verfifyAuthentificationVote( loginUrl, formName , iObjectId, value, typeResponse, ini )
{
	urlVote = sHost+'/vote/set_vote/'+iObjectId+'/'+value+'/'+typeResponse+'/'+ini;
	if(GetCookie("user_logged") == "oui"){
			new Ajax.Request(urlVote,{asynchronous:true,onComplete:	function(t){
				eval(t.responseText);
				$('formulaire').hide();
				if (result.deja_vote == 0)
				{
					$('reponse').show();
				}
				else
				{
					$('deja_vote').show();
				}		
			}
		});
	}else{

		baseIdForm = formName;	
		callModalBox(loginUrl, {title: 'Login', height: 800, width: 950, overlayOpacity: 0.75});
	}
}

function userHasVoted(iObjectId, value, typeResponse, ini)
{
	urlVote = sHost+'/vote/get_vote/'+iObjectId+'/'+value+'/'+typeResponse+'/'+ini;
	if(GetCookie("user_logged") == "oui"){
			new Ajax.Request(urlVote,{asynchronous:true,onComplete:	function(t){
				eval(t.responseText);
				if (result.deja_vote == 1)
				{
					$('formulaire').hide();
					$('deja_vote').show();				
				}		
			}
		});
	}
}