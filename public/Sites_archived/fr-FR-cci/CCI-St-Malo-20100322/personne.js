/////////////////////////////////////////////////////////////////////////////////
//                    P R O J E T   - C N P L A B   -                     
/////////////////////////////////////////////////////////////////////////////////
//   INTITULE
//	Nom du fichier : PERSONNE.js
//	Cr�ateur : ML ROBIN//
//	Date de cr�ation :	28/06/01
//
/////////////////////////////////////////////////////////////////////////////////
//   HISTORIQUE DES MODIFICATIONS
//
// Version  Auteur    Date                       Description
// -------  ------  --------  ---------------------------------------------------
//		01			MLR		 28/06/01  Contient les fonctions javascript n�cessaires � la v�rification du formulaire inscription
//
/////////////////////////////////////////////////////////////////////////////////
//   OU EST T-IL APPELE
//
//    Nom du fichier        Emplacement			Description
// ----------------------   --------------   ------------------------------------
//    creerpersonne.inc			prive/lib		D�crit en HTML le formulaire inscription               
// 
/////////////////////////////////////////////////////////////////////////////////
//   DESCRIPTION
//  
//   Contient les fonctions javascript n�cessaires � la v�rification du formulaire inscription
//  
// 
//   GENERALITE
//	Nom de la fonction : confirmation
//	Cr�ateur : PHPGen
//	Date de cr�ation :	28/06/01
//
//*******************************************************************************
//   HISTORIQUE DES MODIFICATIONS
//
// Version  Auteur    Date                         Description
// -------  ------  --------  ---------------------------------------------------
//		01		PHPGen		28/06/01		v�rifie la suppression d'un enregistrement
//
//*******************************************************************************
//   PARAMETRES
//
//        Nom         E/S                       Description
// -----------------  ---  ------------------------------------------------------
//       $1            E   < description du premier param�tre >
//
//	$bool	      S   Code retour ex�cution : TRUE si OK
//
//*******************************************************************************
//   OU EST T-ELLE APPELE
//
//     Nom du fichier        Emplacement			Description
// ----------------------   --------------   ------------------------------------
//                   
//  
//                   
//*******************************************************************************
//   DESCRIPTION
//  
//   v�rifie la suppression d'un enregistrement
//  
// 
//*******************************************************************************

/////////////////////////////////////////////////////////////////////////////////
//fonction qui v�rifie la suppression d'un enregistrement
/////////////////////////////////////////////////////////////////////////////////
function confirmation(key)
{
	bool = confirm("Confirmer la suppression de l'enregistrement " + key);
	return bool; 
}

//   GENERALITE
//	Nom de la fonction : replace
//	Cr�ateur : ML ROBIN
//	Date de cr�ation :	28/06/01
//
//*******************************************************************************
//   HISTORIQUE DES MODIFICATIONS
//
// Version  Auteur    Date                         Description
// -------  ------  --------  ---------------------------------------------------
//		01			MLR		28/06/01	remplace un symbole par un autre dans une cha�ne
//
//*******************************************************************************
//   PARAMETRES
//
//        Nom         E/S                       Description
// -----------------  ---  ------------------------------------------------------
//       s            E   cha�ne � �tudier
//  
//       t            E   symbole � trouver et remplacer
//  
//       u            E   u : symbole de remplacement
//
//				$r	     		 S   Retourne la nouvelle String
//
//*******************************************************************************
//   OU EST T-ELLE APPELE
//
//     Nom du fichier        Emplacement			Description
// ----------------------   --------------   ------------------------------------
//       PERSONNE.js			racine de la partie public            
//  
//                   
//*******************************************************************************
//   DESCRIPTION
//  
//   remplace un symbole par un autre dans une cha�ne
//  
// 
//*******************************************************************************

/////////////////////////////////////////////////////////////////////////////////
//Fonction qui remplace un symbole par un autre dans une cha�ne
//
//Param�tres en entr�e:
//	s : cha�ne � �tudier
//	t : symbole � trouver et remplacer
//	u : symbole de remplacement
/////////////////////////////////////////////////////////////////////////////////
function replace(s, t, u) {
	/*   **  Replace a token in a string
	**    s  string to be processed
	**    t  token to be found and removed
	**    u  token to be inserted
	**  returns new String   */
	
	i = s.indexOf(t);
	r = "";
	if (i == -1) return s;
	r += s.substring(0,i) + u;
	if ( i + t.length < s.length)
		r += replace(s.substring(i + t.length, s.length), t, u);
	return r;
}

//   GENERALITE
//	Nom de la fonction : js_fIsEMail
//	Cr�ateur : ML ROBIN
//	Date de cr�ation :28/06/01	
//
//*******************************************************************************
//   HISTORIQUE DES MODIFICATIONS
//
// Version  Auteur    Date                         Description
// -------  ------  --------  ---------------------------------------------------
//		01			MLR			28/06/01	v�rifie la syntaxe d'une adresse email
//
//*******************************************************************************
//   PARAMETRES
//
//        Nom         E/S                       Description
// -----------------  ---  ------------------------------------------------------
//       jsobj         E   cha�ne � �tudier correspondant � l'adresse email
//
//				$ret	      S   Code retour ex�cution : 0 si l'adresse e-mail n'est pas �crite correctement
//
//*******************************************************************************
//   OU EST T-ELLE APPELE
//
//     Nom du fichier        Emplacement			Description
// ----------------------   --------------   ------------------------------------
//      PERSONNE.js        racine de la partie public
//  
//                   
//*******************************************************************************
//   DESCRIPTION
//  
//   v�rifie la syntaxe d'une adresse email
//  
// 
//*******************************************************************************


/////////////////////////////////////////////////////////////////////////////////
//Fonction qui v�rifie la syntaxe d'une adresse email
//Renvoie 1 si l'adresse n'est pas correct et renvoie 0 sinon
//
//Param�tres en entr�e:
//	jsobj : cha�ne � �tudier correspondant � l'adresse email
/////////////////////////////////////////////////////////////////////////////////
function js_fIsEMail( jsobj )
{
	serie = jsobj.split('@');

	if ( serie.length == 2 )
	{
		virgule_deb = serie[0].indexOf(".");
		virgule_fin = serie[0].lastIndexOf(".");
		if ( virgule_deb != 0 && virgule_fin != serie[0].length-1 )
		{
			virgule_deb = serie[1].indexOf(".");
			virgule_fin = serie[1].lastIndexOf(".");
			lg = serie[1].length
			serie2 = serie[1].split('.')
			if ( virgule_deb != 0 && virgule_fin != lg-1 && serie2.length > 1 )
			{
				return 1;
			}
		}
	}
	return 0;
}


//initialisation des variables
var digit = /^\d+$/
var truecount=0

//   GENERALITE
//	Nom de la fonction : valid_PERSONNE
//	Cr�ateur : ML ROBIN
//	Date de cr�ation :	28/06/01
//
//*******************************************************************************
//   HISTORIQUE DES MODIFICATIONS
//
// Version  Auteur    Date                         Description
// -------  ------  --------  ---------------------------------------------------
//		01			MLR			28/06/01		V�rifie la syntaxe des champs : LOGIN, EMAIL, NOM et PRENOM du formulaire inscription
//
//*******************************************************************************
//   PARAMETRES
//
//        Nom         E/S                       Description
// -----------------  ---  ------------------------------------------------------
//       $1            E   Les champs du formulaire inscription : form_login, form_email, form_nom, form_prenom
//
//	$ret	      S   Affichage d un Messsage d erreur si probl�me avec les champs
//
//*******************************************************************************
//   OU EST T-ELLE APPELE
//
//     Nom du fichier        Emplacement			Description
// ----------------------   --------------   ------------------------------------
//     creerpersonne.inc   prive/lib		D�crit en HTML le formulaire inscription      
//  
//                   
//*******************************************************************************
//   DESCRIPTION
//  
//   V�rifie la syntaxe des champs : LOGIN, EMAIL, NOM et PRENOM du formulaire inscription
//  
// 
//*******************************************************************************

/////////////////////////////////////////////////////////////////////////////////
//Fonction qui cherche des erreurs dans les champs et affiche une alerte si elle en trouve
//
/////////////////////////////////////////////////////////////////////////////////
function valid_PERSONNE()
{
	truecount=0;
	chaine="";
	
/////////////////////////////////////////////////////////////////////
//cas du champ LOGIN 
/////////////////////////////////////////////////////////////////////

	text = replace(document.formulaire.form_login.value," ",""); //cherche les �ventuels espaces et les supprime
	
	if (text == "") {
		if (chaine != "") {
			chaine = chaine + "\r Le Login ne peut pas etre nulle";
		}
		else {
			chaine =" Le Login ne peut pas etre nulle!";
		}
		truecount=0;
	}
	
	else {
		if(text.length < 6) {
			if (chaine != "") {
				chaine = chaine + "\r Le Login doit contenir au moins 6 caract�res";
			}
			else {
				chaine =" Le Login doit contenir au moins 6 caract�res!";
			}
			truecount=0;
		}
		else {
			truecount++;
		}
	}
// fin cas du champs login/////////



/////////////////////////////////////////////////////////////////////
//cas du champ EMAIL
/////////////////////////////////////////////////////////////////////

	text = document.formulaire.form_email.value ;

	if (text == "" ) {
		if (chaine != "") {
			chaine = chaine + "\r L'adresse EMAIL ne peut pas etre nulle";
                }
        	else {
                        chaine =" L'adresse EMAIL ne peut pas etre nulle!";
                }
        	truecount=0;
     	}
	else {
		if (js_fIsEMail(text) == 0) {
			if (chaine != "") {
                        	chaine = chaine + "\r L'adresse EMAIL est incorrecte";
                	}
        		else {
                        	chaine =" L'adresse EMAIL est incorrecte!";
                	}
        		truecount=0;
		}
	
		else truecount++;
	}

// fin cas du champs email/////////


/////////////////////////////////////////////////////////////////////
//cas du champ NOM
/////////////////////////////////////////////////////////////////////

	text = document.formulaire.form_nom.value ;
	if (text == "") {
        	if (chaine != "") {
                        chaine = chaine + "\r Le Nom ne peut pas etre nulle";
                }
                else {
                        chaine =" Le Nom ne peut pas etre nulle!";
                }
                truecount=0;
        }
	else
    	{
        	truecount++;
    	}
// fin cas du champs nom/////////


/////////////////////////////////////////////////////////////////////
//cas du champ PRENOM 
/////////////////////////////////////////////////////////////////////

	text = document.formulaire.form_prenom.value ;
	if (text == "") {
        	if (chaine != "") {
                        chaine = chaine + "\r Le Prenom ne peut pas etre nulle";
                }
        	else {
        	        chaine =" Le Prenom ne peut pas etre nulle!";
        	}
        	truecount=0;
    	}
	else {
        	truecount++;
        }
// fin cas du champs prenom/////////


if (truecount==4)
  {
    return true;
  }
else
  {
    truecount=0; 
  }
if (truecount == 0)
  {
    alert(chaine);
    return false;
  }
}