/* ------------------------------------------------------------------------
	s3Slider
	
	Developped By: Boban Karišik -> http://www.serie3.info/
        CSS Help: Mészáros Róbert -> http://www.perspectived.com/
	Version: 1.0
	
	Copyright: Feel free to redistribute the script/modify it, as
			   long as you leave my infos at the top.
------------------------------------------------------------------------- */


(function($){  

    $.fn.PiSlider = function(vars) {       
        
        var element     = this;
        var timeOut     = (vars.timeOut != undefined) ? vars.timeOut : 4000;
        var current     = null;
        var timeOutFn   = null;
        var faderStat   = true;
        var mOver       = false;
        var items       = $("#" + element[0].id + " ul.slider-slides li");
        var itemsSpan   = $("#" + element[0].id + " ul.slider-links li");
        var currNo		= -1;

        items.each(function(i) {
    
        	$(items[i]).mouseover(function() {
                mOver = true;
                clearTimeout(timeOutFn);
                activate(i);
             });
        	$(itemsSpan[i]).mouseover(function() {
                mOver = true;
                clearTimeout(timeOutFn);
                activate(i);
             });
            
        	$(items[i]).mouseout(function() {
                mOver   = false;
                fadeElement(false);
            });
        	$(itemsSpan[i]).mouseout(function() {
                mOver   = false;
                fadeElement(false);
            });
            
        });
        
        var fadeElement = function(isMouseOut) {
            var thisTimeOut = (isMouseOut) ? (timeOut/2) : timeOut;
            thisTimeOut = (faderStat) ? 10 : thisTimeOut;
            if(items.length > 0) {
                timeOutFn = setTimeout(makeSlider, thisTimeOut);
            } else {
                console.log("Poof..");
            }
        }
        
        var activate = function(j) {
        	items.each(function(i) {
        		$(this).removeClass('active');
        		if(j == i) $(this).addClass('active');
        	});
        	itemsSpan.each(function(i) {
        		$(this).removeClass('active');
        		if(j == i) $(this).addClass('active');
        	});
        	current = items[j];
        }
        
        var makeSlider = function() {
        	current = (current != null) ? current : items[0];
       		currNo		= currNo > (items.length-2) ? 0 : currNo + 1;
            if(!mOver) {
            	items.each(function(i) {
            		$(this).removeClass('active');
            	});
            	itemsSpan.each(function(i) {
            		$(this).removeClass('active');
            	});
                $(items[currNo]).addClass('active').fadeIn((timeOut/6), function() {
                	$(itemsSpan[currNo]).addClass('active');
                	faderStat = false;
                	current = items[currNo];
                	fadeElement(false);
                });
            }
        }
        
        makeSlider();

    };  

})(jQuery);  