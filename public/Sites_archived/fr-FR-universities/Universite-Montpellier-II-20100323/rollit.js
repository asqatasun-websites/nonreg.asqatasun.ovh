
/* ================= rollover FR ================== */

	var tabID = new Array();
    var tabOver = new Array();
    var tabOut = new Array();

    tabID[0] = 0;
    tabID[1] = 0;
    tabID[2] = 0;
    tabID[3] = 0;
    tabID[4] = 0;
	tabID[5] = 0;

    tabOver[0] = './architect/img/menuhaut/menuhautroll/menuhautroll1.gif';
    tabOver[1] = './architect/img/menuhaut/menuhautroll/menuhautroll2.gif';
    tabOver[2] = './architect/img/menuhaut/menuhautroll/menuhautroll3.gif';
    tabOver[3] = './architect/img/menuhaut/menuhautroll/menuhautroll4.gif';
    tabOver[4] = './architect/img/menuhaut/menuhautroll/menuhautroll5.gif';
	tabOver[5] = './architect/img/menuhaut/menuhautroll/menuhautroll6.gif';

    tabOut[0] = './architect/img/menuhaut/menuhaut1/menuhaut_01.gif';
    tabOut[1] = './architect/img/menuhaut/menuhaut1/menuhaut_02.gif';
    tabOut[2] = './architect/img/menuhaut/menuhaut1/menuhaut_03.gif';
    tabOut[3] = './architect/img/menuhaut/menuhaut1/menuhaut_04.gif';
    tabOut[4] = './architect/img/menuhaut/menuhaut1/menuhaut_05.gif';
	tabOut[5] = './architect/img/menuhaut/menuhaut1/menuhaut_06.gif';

	function OverOut(ID,action)
    {
        if (tabID[ID] == 1){return;}
        if (action ==0)
        {
            document.getElementById('ro'+ID).src = tabOver[ID]; // mouseOver
        }
        else
        {
            document.getElementById('ro'+ID).src = tabOut[ID]; // mouseOut
        }
    }
	
	
/* ================= rollover UK ================== */
	
	var tabID_en = new Array();
    var tabOver_en = new Array();
    var tabOut_en = new Array();

    tabID_en[0] = 0;
    tabID_en[1] = 0;
    tabID_en[2] = 0;
    tabID_en[3] = 0;
    tabID_en[4] = 0;
	tabID_en[5] = 0;

    tabOver_en[0] = './architect/img/menuhaut/menuhautroll_en/menuhaut_01.gif';
    tabOver_en[1] = './architect/img/menuhaut/menuhautroll_en/menuhaut_02.gif';
    tabOver_en[2] = './architect/img/menuhaut/menuhautroll_en/menuhaut_03.gif';
    tabOver_en[3] = './architect/img/menuhaut/menuhautroll_en/menuhaut_04.gif';
    tabOver_en[4] = './architect/img/menuhaut/menuhautroll_en/menuhaut_05.gif';
	tabOver_en[5] = './architect/img/menuhaut/menuhautroll_en/menuhaut_06.gif';

    tabOut_en[0] = './architect/img/menuhaut/menuhaut1_en/menuhaut_01.gif';
    tabOut_en[1] = './architect/img/menuhaut/menuhaut1_en/menuhaut_02.gif';
    tabOut_en[2] = './architect/img/menuhaut/menuhaut1_en/menuhaut_03.gif';
    tabOut_en[3] = './architect/img/menuhaut/menuhaut1_en/menuhaut_04.gif';
    tabOut_en[4] = './architect/img/menuhaut/menuhaut1_en/menuhaut_05.gif';
	tabOut_en[5] = './architect/img/menuhaut/menuhaut1_en/menuhaut_06.gif';

	function OverOut_en(ID,action)
    {
        if (tabID_en[ID] == 1){return;}
        if (action ==0)
        {
            document.getElementById('ro'+ID).src = tabOver_en[ID]; // mouseOver
        }
        else
        {
            document.getElementById('ro'+ID).src = tabOut_en[ID]; // mouseOut
        }
    }

  