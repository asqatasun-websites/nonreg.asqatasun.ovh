/******************************************************************************/
/***********************        calendrier              ***********************/
/******************************************************************************/
/***********************  derni�re modif: 30/08/07 YLC  ***********************/
/******************************************************************************/

var date;
var destinationInput;
var currentDiv=null;
var mode=null;
var noDaySelected=false;

var selectedMonth = null;
var selectedYear = null;
//Cr�ation de la table contenant le calendrier pour un mois donn�
//Le mois affich� est celui de la date en param�tre
//Ou du jour
function calendar(date1){
    //If no parameter is passed use the current date.
    if(date1 == null){
       date = new Date();
    }else{date=date1;}

    day = date.getDate();
    month = date.getMonth();
    year = date.getFullYear();

    months = new Array(
    'Janvier',
    'F�vrier',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juillet',
    'Aout',
    'Septembre',
    'Octobre',
    'Novembre',
    'D�cembre');

    this_month = new Date(year, month, 1);
    next_month = new Date(year, month + 1, 1);

    //Find out when this month starts and ends.
    first_week_day = this_month.getDay()-1;
    if (first_week_day==-1) first_week_day=6
    days_in_this_month = Math.round((next_month.getTime() - this_month.getTime()) / (1000 * 60 * 60 * 24));
	
    calendar_html = '<table>';
	
	moisSuivant = (month+2);
	anneeSuivante = year;
	
	moisPrecedent = month;
	anneePrecedent = year;	
	
	if(moisSuivant == 13) {
		moisSuivant = 1;
		anneeSuivante++;
	}
	if(moisPrecedent == 0) {
		moisPrecedent = 12;
		anneePrecedent--;
	}
	
    calendar_html += '<tr><td colspan="7" align="center" >'
    calendar_html += '<a href="?mode='+mode+'&afficheDate=0/'+moisPrecedent+'/'+anneePrecedent+'" title="Mois pr�c�dent"><img src="images/fleche_precedente.jpg" border="0" alt="<" title="Mois pr�c�dent" align=left></a>';  //onClick="calendrier_mois_precedent(date);return false;"
    calendar_html += '<a href="?mode='+mode+'&afficheDate=0/'+moisSuivant+'/'+anneeSuivante+'" title="Mois suivant"><img src="images/fleche_suivante.jpg" border="0" alt=">" title="Mois suivant" align=right></a><a href="?mode='+mode+'&afficheDate=0/'+(month+1)+'/'+year+'">'+ months[month] + ' ' + year +'</a></td></tr>'; //onClick="calendrier_mois_suivant(date);return false;"

    calendar_html += '<tr>';
    calendar_html += '<tr class="enteteSemaine"><td>Lundi</td><td>Mardi</td><td>Mercredi</td><td>Jeudi</td><td>Vendredi</td><td>Samedi</td><td>Dimanche</td></tr>';

    //Fill the first week of the month with the appropriate number of blanks.
    for(week_day = 0; week_day < first_week_day; week_day++)
       {
       calendar_html += '<td class="joursVides"></td>';
       }

    week_day = first_week_day;
    for(day_counter = 1; day_counter <= days_in_this_month; day_counter++)
       {
        day_t=day_counter+"";
        if (day_t.length==1) day_t="0"+ day_counter;
        m=month+1
        month_t=(m)+"";
        if (month_t.length==1) month_t="0"+ month_t;
        date_counter=day_t +"/"+ month_t +"/"+ year
       week_day %= 7;

       if(week_day == 0)
          calendar_html += '</tr><tr>';
       //Do something different for the current day.
       if(day == day_counter && month == selectedMonth && year == selectedYear && !noDaySelected)
          calendar_html += '<td align="center" class="aujourdHui"><a href="?mode='+mode+'&afficheDate='+day_counter+'/'+(month+1)+'/'+year+'" >' + day_counter + '</a></td>';
       else
          calendar_html += '<td align="center" class="joursNormaux"><a href="?mode='+mode+'&afficheDate='+day_counter+'/'+(month+1)+'/'+year+'" > ' + day_counter + '</a></td>';

       week_day++;
       }

    calendar_html += '</tr>';
    calendar_html += '</table>';

    //Display the calendar.
    return calendar_html;
}

//Reinitialise le calendrier au mois pr�c�dent de la date en param�tre
function calendrier_mois_precedent(ladate){
    if(ladate == null)
       ladate = new Date();

    day = ladate.getDate();
    month = ladate.getMonth();
    year = ladate.getFullYear();

    if (month>0){
        month--;
    }else{
        month=11;
        year--;
    }

    div_calendrier=document.getElementById("tableau_calendrier");
    if (div_calendrier!=null){
        div_calendrier.innerHTML=calendar(new Date(year,month,day));

    }
}

//Reinitialise le calendrier au mois suivant de la date en param�tre
function calendrier_mois_suivant(ladate){
    if(ladate == null)
       ladate = new Date();

    day = ladate.getDate();
    month = ladate.getMonth();
    year = ladate.getFullYear();

    if (month<11){
        month++;
    }else{
        month=0;
        year++;
    }

    div_calendrier=document.getElementById("tableau_calendrier");
    if (div_calendrier!=null){
        div_calendrier.innerHTML=calendar(new Date(year,month,day));
    }
}

//Creation du calendrier
//Affichage du calendrier
function initCalendrier(ladate,requestMode){
    mode = requestMode;
    date2=null;
    if (ladate!="" && ladate.length==10){
        lemoi=ladate.substring(3,5);
        if (lemoi.charAt(0)=="0") lemoi=lemoi.substring(1,2);
        JourDate = ladate.substring(0,2);
        if (JourDate == "00") {
            JourDate = "01";
            noDaySelected = true; 
        }
        date2=new Date(ladate.substring(6,10),parseInt(lemoi)-1,JourDate);
        selectedMonth = date2.getMonth();
        
        selectedYear = date2.getFullYear();
    }
    div_calendrier=document.getElementById("tableau_calendrier");

    if (div_calendrier!=null){
        div_calendrier.innerHTML=calendar(date2);
    }
}