function addLoadEvent(func) {
	var oldonload = window.onload;
	if (typeof window.onload != 'function') {
		window.onload = func;
	} else {
		window.onload = function() {
			if (oldonload) {
				oldonload();
			}
			func();
		}
	}
};

function menuFix() {
	$$('ul#root').each(function(link){
		link.setStyle({visibility:'visible'});
	});
	var hlink;
	var shlink;
	var navExists = false
	var tempPath = readCookie("mainPage");
    if (tempPath.length > 1){
    if (tempPath.substring(tempPath.length-1, tempPath.length) == "/")
        tempPath = tempPath.substring(0, tempPath.length-1);
	}
    var fullPath = tempPath;
	while (tempPath.length > 0 && navExists == false) {
		$$('#root li a.mainNav').each(function(link){
        	hlink = link.readAttribute('href');
			if (hlink.length > 1){
				if (hlink.substring(hlink.length-1, hlink.length) == "/")
					hlink = hlink.substring(0, hlink.length-1);
			}
        	if(hlink == tempPath){
                link.up(0).addClassName('highlight');
                if (link.next('ul')) {
                    link.next('ul').addClassName('subStay');
                    navExists = true;
                    var arr = link.next('ul').descendants();
                    arr.each(function(subLink){
                        subLink.descendants().each(function(subLinkA){
							shlink = subLinkA.readAttribute('href');
							if (shlink != null && shlink.length > 1){
								if (shlink.substring(shlink.length-1, shlink.length) == "/")
									shlink = shlink.substring(0, shlink.length-1);
								}
                            if(shlink == fullPath){
                                subLinkA.up(0).addClassName('highlight');
                            }
                        });
                    });
                };
            }
		});
        tempPath = tempPath.substr(0,tempPath.lastIndexOf("/"));

	}
	if (navExists == false) {
		$$('#root li a.mainNav').each(function(link){
	        hlink = link.readAttribute('href');
			if (hlink.length > 1){
				if (hlink.substring(hlink.length-1, hlink.length) == "/")
					hlink = hlink.substring(0, hlink.length-1);
			}
	        if(hlink == "/"){
                link.up(0).addClassName('highlight');
                if (link.next('ul')) {
                    link.next('ul').addClassName('subStay');
                    var arr = link.next('ul').descendants();
                    arr.each(function(subLink){
                        subLink.descendants().each(function(subLinkA){
                            if(subLinkA.readAttribute('href') == tempPath){
                                subLinkA.up(0).addClassName('highlight');
                            }
                        });
                    });
                }
			}
	    });
		eraseCookie("mainPage");
		createCookie("mainPage","/","1");
	}
}

function tribHover() {
	var timeout = 600;
	var cssClass = "highlight";
	var thisPage = readCookie("mainPage");

	var queue = [];
	var reCSS = new RegExp("\\b" + cssClass + "\\b");
	var tribEls = document.getElementById("root").getElementsByTagName("li");

 	for (var i=0; i<tribEls.length; i++) {
	 	if(/subStay/.test(tribEls[i].parentNode.className))
	  	{

		} else {

		// mouseover and mouseout handlers for regular mouse based interface.
		tribEls[i].onmouseover = function() {
			queueFlush();
			this.className += " " + cssClass;
			$$('#root li a.mainNav').each(function(link){
		        if(link.readAttribute('href') == thisPage ){
					link.up(0).removeClassName(cssClass);
				}
		    });
		}
		tribEls[i].onmouseout = function() {
			queue.push([setTimeout(queueTimeout, timeout), this]);
		}
		// focus and blur handlers for keyboard based navigation.
		tribEls[i].onfocus = function() {
			queueFlush();
			this.className += " " + cssClass;
		}
		tribEls[i].onblur = function() {
			queue.push([setTimeout(queueTimeout, timeout), this]);

			// click event handler needed for tablet type interfaces (e.g. Apple iPhone).
			tribEls[i].onclick = function(e) {
				if (this.className.search(reCSS) == -1) {
					// CSS not set, so clear all sibling (and decendants) menus, and then set CSS on this menu...
					var elems = this.parentNode.getElementsByTagName("li");
					for (var i=0; i<elems.length; i++) {
						elems[i].className = elems[i].className.replace(reCSS, "");
					}
					this.className += " " + cssClass;
				} else {
					// CSS already set, so clear all decendant menus and then this menu...
					var elems = this.getElementsByTagName("li");
					for (var i=0; i<elems.length; i++) {
						elems[i].className = elems[i].className.replace(reCSS, "");
					}
					this.className = this.className.replace(reCSS, "");
				}
				if (e && e.stopPropagation)
					e.stopPropagation();
				else
					window.event.cancelBubble = true;
			}
		}

		queueFlush = function () {
			while (queue.length) {
				clearTimeout(queue[0][0]);
				queueTimeout();
			}
		}

		queueTimeout = function() {
			if (queue.length) {
				var el = queue.shift()[1];
				el.className = el.className.replace(reCSS, "");
				$$('#root li a.mainNav').each(function(link){
			        if(link.readAttribute('href') == thisPage){
						link.up(0).addClassName(cssClass);
                        if (link.next('ul')) {
                            var arr = link.next('ul').descendants();
                            arr.each(function(subLink){
                                subLink.descendants().each(function(subLinkA){
                                    if(subLinkA.readAttribute('href') == thisPage){
                                        subLinkA.up(0).addClassName(cssClass);
                                    }
                                });
                            });
                        }
					}
			    });
			}
		}
	}
}
}
