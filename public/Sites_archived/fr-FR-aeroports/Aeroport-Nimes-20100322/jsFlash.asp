
var requiredVersion = 7;	// Version the user needs to view site (max 9, min 2)

// System globals
var flash2Installed = false;    // boolean. true if flash 2 is installed
var flash3Installed = false;    // boolean. true if flash 3 is installed
var flash4Installed = false;    // boolean. true if flash 4 is installed
var flash5Installed = false;    // boolean. true if flash 5 is installed
var flash6Installed = false;    // boolean. true if flash 6 is installed
var flash7Installed = false;    // boolean. true if flash 7 is installed
var flash8Installed = false;    // boolean. true if flash 8 is installed
var flash9Installed = false;    // boolean. true if flash 9 is installed
var maxVersion = 9;             // highest version we can actually detect
var actualVersion = 0;          // version the user really has
var hasRightVersion = false;    // boolean. true if it's safe to embed the flash movie in the page

// Write vbscript detection on ie win. IE on Windows doesn't support regular
// JavaScript plugins array detection.
if(navigator.appVersion.indexOf("MSIE") != -1 && navigator.appVersion.toLowerCase().indexOf("win") != -1){
  document.write('<SCR' + 'IPT LANGUAGE=VBScript\> \n');
  document.write('on error resume next \n');
  document.write('flash2Installed = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.2"))) \n');
  document.write('flash3Installed = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.3"))) \n');
  document.write('flash4Installed = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.4"))) \n');
  document.write('flash5Installed = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.5"))) \n');  
  document.write('flash6Installed = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.6"))) \n');  
  document.write('flash7Installed = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.7"))) \n');
  document.write('flash8Installed = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.8"))) \n');
  document.write('flash9Installed = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.9"))) \n');
  document.write('<\/SCR' + 'IPT\> \n'); // break up end tag so it doesn't end our script
}

function detectFlash() {

	// If navigator.plugins exists...
	if (navigator.plugins) {

		// ...then check for flash 2 or flash 3+.
		if (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]) {

			// Some version of Flash was found. Time to figure out which.
			      
			// Set convenient references to flash 2 and the plugin description.
			var isVersion2 = navigator.plugins["Shockwave Flash 2.0"] ? " 2.0" : "";
			var flashDescription = navigator.plugins["Shockwave Flash" + isVersion2].description;

			// DEBUGGING: uncomment next line to see the actual description.
			// alert("Flash plugin description: " + flashDescription);

			// A flash plugin-description looks like this: Shockwave Flash 4.0 r5
			// We can get the major version by grabbing the character before the period
			// note that we don't bother with minor version detection. 
			// Do that in your movie with $version or getVersion().
			var flashVersion = parseInt(flashDescription.substring(16));

			// We found the version, now set appropriate version flags. Make sure
			// to use >= on the highest version so we don't prevent future version
			// users from entering the site.
			flash2Installed = flashVersion == 2;    
			flash3Installed = flashVersion == 3;
			flash4Installed = flashVersion == 4;
			flash5Installed = flashVersion == 5;
			flash6Installed = flashVersion == 6;
			flash7Installed = flashVersion == 7;
			flash8Installed = flashVersion == 8;
			flash9Installed = flashVersion >= 9;
		}
	}

	// Loop through all versions we're checking, and
	// set actualVersion to highest detected version.
	for (var i = 2; i <= maxVersion; i++) {  
		if (eval("flash" + i + "Installed") == true) actualVersion = i;
	}

	// If we're on msntv (formerly webtv), the version supported is 4 (as of
	// January 1, 2004). Note that we don't bother sniffing varieties
	// of msntv. You could if you were sadistic...
	if(navigator.userAgent.indexOf("WebTV") != -1) actualVersion = 4;  

	// DEBUGGING: uncomment next line to display flash version
	// alert("version detected: " + actualVersion);

	// We're finished getting the version on all browsers that support detection.
	// If the user has a new enough version...
	hasRightVersion = (actualVersion >= requiredVersion)
}

function insertFlash(src, alternateImg, alternativeText, width, eight) {
	var html = '';
  	if (hasRightVersion) { 
  	//if (false) { 
		html = '<object data="http://www.nimes-aeroport.fr/_js/'&#32;+&#32;src&#32;+&#32;'" '
			+ 'width="' + width + '" height="' + eight + '" '
			+ 'name="flashmovie" id="flashmovie" '
			+ 'type="application/x-shockwave-flash">'
			+ '<param name="movie" value="' + src + '">'
			+ '<param name="play" value="true">'
			+ '<param name="quality" value="high">'
			+ '<param name="wmode" value="transparent">'
			+ '</object>';
	} else {
		html = '<img src="http://www.nimes-aeroport.fr/_js/'&#32;+&#32;alternateImg&#32;+&#32;'" width="' + width + '" height="' + eight + '" alt="' + alternativeText + '">';
	}

	// DEBUGGING: uncomment next line to display inserted html
	// alert(html);

	document.write(html);
}

detectFlash();  // call our detector now that it's safely loaded.
