function clearText(thefield){
   if (thefield.defaultValue==thefield.value)
   thefield.value = ""
} 
   
function trim(chaine) { 
    // espaces au debut 
    while (chaine.substring(0,1) == ' ') 
        chaine = chaine.substring(1, chaine.length);

    // espaces a la fin 
    while (chaine.substring(chaine.length-1,chaine.length) == ' ')
        chaine = chaine.substring(0, chaine.length-1);
   return chaine;
} 

function changeTypeRecherche(type_recherche) {
   document.getElementById('sur').value = type_recherche;
   
   document.getElementById('tab_0').setAttribute("class", "tabOff_0");
   document.getElementById('tab_1').setAttribute("class", "tabOff_1");
   
   if ( type_recherche == 'LEMONDE' ) {
      document.getElementById('tab_0').setAttribute("class", "tabOn_0");
      document.getElementById('query').value = 'Recherche sur Le Monde.fr';
      document.getElementById('formrech').setAttribute('target','');
   }
   else if ( type_recherche == 'YAHOO' ) {
      document.getElementById('tab_1').setAttribute("class", "tabOn_1");
      document.getElementById('query').value = 'Recherche sur le web';    
      document.getElementById('formrech').setAttribute('target','_blank');
   }  
}

function ValideRechercheSimple(leForm) {
   var eAccentGrav = "e";
   var eAccentAigu = "e";
   var eAccentCirc = "e";
   var aAccentGrav = "a";
   
      
   //On teste si le javascript >= 1.2
   if ((String.fromCharCode(232))!=false && (String.fromCharCode(232))!=null) {
      var eAccentGrav = String.fromCharCode(232);
      var eAccentAigu = String.fromCharCode(233);
      var eAccentCirc = String.fromCharCode(234);
      var aAccentGrav = String.fromCharCode(224);
   }

   // Verif de la presence des champs a rechercher
   leForm.query.value = trim (leForm.query.value);
   longueurCle = leForm.query.value.length;
           
   if ( longueurCle == 0 && leForm.periode.options[leForm.periode.selectedIndex].value!=1987) {
      alert ("Veuillez renseigner le champ Mot Cl"+eAccentAigu+"." );
      leForm.query.focus();
      return false;
   }
return true;
}

//fonction de masquage des element en fonction du click sur les bouton radio
function showHide(param) {
   switch (param) {
      
      case 0:
         if ( (form_recherche=document.getElementById('formrech')) )
            form_recherche.setAttribute('target','');
      break;
      case 1:
         if ( (form_recherche=document.getElementById('formrech')) )
            form_recherche.setAttribute('target','_blank');
      break;
   }
}