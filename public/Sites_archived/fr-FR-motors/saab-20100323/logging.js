/* Touch Clarity logging request - http://www.touchclarity.com
 * Copyright (c) Touch Clarity Ltd 2001-2004 All rights reserved. Patent Pending.
 * For GM - Saab Shared Platform 
 * Change the value of tc_logging_active to switch off logging on the site. */

if (typeof tc_logging_active == 'undefined') tc_logging_active = true;

tc_site_id = tc_get_saab_site_id();
tc_server_url = "gm.touchclarity.com";
tc_log_path = '/webdav/files/global/sophus';
document.write("<scr"+"ipt language='JavaScript' type='text/javascript' src='"+tc_log_path+"/logging-code.js'></scr"+"ipt>");

function tc_get_saab_site_id() {
    // get site identifier from URL path
    var p = document.location.pathname;
         if (p.indexOf("/de/de/") > -1) return 358;
    else if (p.indexOf("/gb/en/") > -1) return 357;
	
	// Australia
    else if (p.indexOf("/au/en/") > -1) return 517;
    
	// Sweden
	else if (p.indexOf("/se/sv/") > -1) {tc_logging_active = false; return 589;}
	
    else if (p.indexOf("/fr/fr/") > -1) return 590;
    else if (p.indexOf("/it/it/") > -1) return 591;
	
	// Switzerland
    else if (p.indexOf("/ch/de/") > -1) {tc_logging_active = false; return 592;}
    else if (p.indexOf("/ch/fr/") > -1) {tc_logging_active = false; return 592;}
	
	// Belgium
    else if (p.indexOf("/be/du/") > -1) {tc_logging_active = false; return 593;}
    else if (p.indexOf("/be/fr/") > -1) {tc_logging_active = false; return 593;}
	
	// Ireland
    else if (p.indexOf("/ie/en/") > -1) {tc_logging_active = false; return 594;}

	// Austria
    else if (p.indexOf("/at/de/") > -1) {tc_logging_active = false; return 595;}
	
	// South Africa
    else if (p.indexOf("/za/en/") > -1) {tc_logging_active = false; return 596;}
    
	else if (p.indexOf("/es/es/") > -1) return 597;
	
	// New Zeeland
    else if (p.indexOf("/nz/en/") > -1) {tc_logging_active = false; return 598;}
	
    else if (p.indexOf("/nl/nl/") > -1) return 599;
    else if (p.indexOf("/ru/ru/") > -1) return 600;
	
	// Denmark
    else if (p.indexOf("/dk/dk/") > -1) {tc_logging_active = false; return 601;}
    
	// Norway
	else if (p.indexOf("/no/no/") > -1) {tc_logging_active = false; return 602;}
	
	// Finland
    else if (p.indexOf("/fi/fi/") > -1) {tc_logging_active = false; return 603;}
    
	// Global
	else if (p.indexOf("/global/en/") > -1) {tc_logging_active = false; return 606;}
    
	// otherwise from domain
    var h = document.location.hostname;
         if (h.indexOf(".de") > -1) return 358;
    else if (h.indexOf(".co.uk") > -1) return 357;
	
	//Australia
    else if (h.indexOf(".com.au") > -1) return 517;
    
	//else if (h.indexOf(".se") > -1) return 589; SE
    else if (h.indexOf(".fr") > -1) return 590;
    else if (h.indexOf(".it") > -1) return 591;
	
	// Switzerland	
    else if (h.indexOf(".ch") > -1) {tc_logging_active = false; return 592;}
	
	// Belgium
    else if (h.indexOf(".be") > -1) {tc_logging_active = false; return 593;}
	
	// Ireland
    else if (h.indexOf(".ie") > -1) {tc_logging_active = false; return 594;}
	
	// Austria
    else if (h.indexOf(".at") > -1) {tc_logging_active = false; return 595;}
	
	// South Africa
    else if (h.indexOf(".co.za") > -1) {tc_logging_active = false; return 596;}
	
    //else if (h.indexOf(".com") > -1) return 597; ES
	
	// New Zeeland
    else if (h.indexOf(".co.nz") > -1) {tc_logging_active = false; return 598;}
    
	else if (h.indexOf(".nl") > -1) return 599;
    //else if (h.indexOf(".ru") > -1) return 600; RU
	
	// Denmark
    else if (h.indexOf(".dk") > -1) {tc_logging_active = false; return 601;}
    
	// Norway
	else if (h.indexOf(".no") > -1) {tc_logging_active = false; return 602;}
	
	// Finland
    else if (h.indexOf(".fi") > -1) {tc_logging_active = false; return 603;}
    //else if (h.indexOf(".com") > -1) return 606;

    else {tc_logging_active = false; return 101;} // unknown site
}
