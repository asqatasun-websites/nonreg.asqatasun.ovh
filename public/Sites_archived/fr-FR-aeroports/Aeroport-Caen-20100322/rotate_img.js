//Fade-in image slideshow- By Dynamic Drive
//For full source code and more DHTML scripts, visit http://www.dynamicdrive.com
//This credit MUST stay intact for use

var slideshow_width='423px' //SET IMAGE WIDTH
var slideshow_height='161px' //SET IMAGE HEIGHT
var pause=3000 //SET PAUSE BETWEEN SLIDE (3000=3 seconds)

var fadeimages=new Array()
//SET IMAGE PATHS. Extend or contract array as needed

fadeimages[0]="/accueil/img/bandeaux/12_03_10.jpg"
fadeimages[1]="/accueil/img/bandeaux/24_02_10.jpg"
fadeimages[2]="/accueil/img/bandeaux/09_02_10.jpg"
fadeimages[3]="/accueil/img/bandeaux/27-10-09.jpg"
fadeimages[4]="/accueil/img/bandeaux/25_01_10_2.jpg"
fadeimages[5]="/accueil/img/bandeaux/25_01_10_3.jpg"
fadeimages[6]="/accueil/img/bandeaux/07_08_09.jpg"
fadeimages[7]="/accueil/img/bandeaux/15_07_09b.jpg"





////NO need to edit beyond here/////////////

var linkimages = new Array();
linkimages[0] = 'http://www.caen.aeroport.fr/accueil/fichiers/12_03_10.pdf';
linkimages[1] = '#';
linkimages[2] = 'http://www.caen.aeroport.fr//web/newsletter/09_02_10.php';
linkimages[3] = 'http://www.caen.aeroport.fr/web/contact/demande-guide.php';
linkimages[4] = 'http://www.caen.aeroport.fr/accueil/fichiers/PubAMERASIA-partenairefev2010.pdf';
linkimages[5] = '#';
linkimages[6] = 'http://www.airfrance.fr/common/image/BANNIERES/fr/AF_eservices_MCI_fr_880.swf?BV_SessionID=ZEv4x9gGO_IrSL203HZS2Vy&BV_EngineID=140ufn0s8';
linkimages[7] = 'http://www.caen.aeroport.fr/web/newsletter/15_07_09b.php';




var preloadedimages=new Array()
for (p=0;p<fadeimages.length;p++){
preloadedimages[p]=new Image()
preloadedimages[p].src=fadeimages[p]
}

var ie4=document.all
var dom=document.getElementById

if (ie4||dom)
document.write('<div style="position:relative;width:'+slideshow_width+';height:'+slideshow_height+';overflow:hidden"><div  id="canvas0" style="position:absolute;width:'+slideshow_width+';height:'+slideshow_height+';top:0;left:0;filter:alpha(opacity=10);-moz-opacity:10"></div><div id="canvas1" style="position:absolute;width:'+slideshow_width+';height:'+slideshow_height+';top:0;left:0;filter:alpha(opacity=10);-moz-opacity:10"></div></div>')
else
document.write('<a href="'+linkimages[0]+'"><img name="defaultslide" src="'+fadeimages[0]+'" style="border:none"></a>')

var curpos=10
var degree=10
var curcanvas="canvas0"
var curimageindex=0
var nextimageindex=1


function fadepic(){
if (curpos<100){
curpos+=10
if (tempobj.filters)
tempobj.filters.alpha.opacity=curpos
else if (tempobj.style.MozOpacity)
tempobj.style.MozOpacity=curpos/100
}
else{
clearInterval(dropslide)
nextcanvas=(curcanvas=="canvas0")? "canvas0" : "canvas1"
tempobj=ie4? eval("document.all."+nextcanvas) : document.getElementById(nextcanvas)
tempobj.innerHTML='<a href="'+linkimages[nextimageindex]+'"><img src="'+fadeimages[nextimageindex]+'" style="border:none"></a>'
nextimageindex=(nextimageindex<fadeimages.length-1)? nextimageindex+1 : 0
setTimeout("rotateimage()",pause)
}
}

function rotateimage(){
if (ie4||dom){
resetit(curcanvas)
var crossobj=tempobj=ie4? eval("document.all."+curcanvas) : document.getElementById(curcanvas)
crossobj.style.zIndex++
var temp='setInterval("fadepic()",50)'
dropslide=eval(temp)
curcanvas=(curcanvas=="canvas0")? "canvas1" : "canvas0"
}
else
document.images.defaultslide.src=fadeimages[curimageindex]
curimageindex=(curimageindex<fadeimages.length-1)? curimageindex+1 : 0
}

function resetit(what){
curpos=10
var crossobj=ie4? eval("document.all."+what) : document.getElementById(what)
if (crossobj.filters)
crossobj.filters.alpha.opacity=curpos
else if (crossobj.style.MozOpacity)
crossobj.style.MozOpacity=curpos/100
}

function startit(){
var crossobj=ie4? eval("document.all."+curcanvas) : document.getElementById(curcanvas)
crossobj.innerHTML='<a href="'+linkimages[curimageindex]+'"><img src="'+fadeimages[curimageindex]+'" style="border:none"></a>'
rotateimage()
}

if (ie4||dom)
window.onload=startit
else
setInterval("rotateimage()",pause)    
