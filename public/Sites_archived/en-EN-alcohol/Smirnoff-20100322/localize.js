/*extern isIE, ieVer, moz, vals, g_bWarnBeforeLeave, smirnoffPopup, init, Cookie, Element, Class, Events, Asset, $, $$, $defined, $E, $ES, Event, Fx */
/* $Revision: 5198 $ */
var AKQA = {
	debug: false,
	checkParam: function (param,type) {
		var paramType = typeof param;
		if ($defined(param)) { throw 'parameter must be defined'; }
		if (type) { if (paramType.toLowerCase() !== type.toLowerCase()) { throw 'parameter is not of expected type: '+type; } }
	},
	log: function (val) {
		if (AKQA.debug) {
			if (window.console && window.console.log) { window.console.log(val); }
				else { alert(val); }
		}
	},
	setDebug: function (pDebug) {
		AKQA.debug = pDebug;
	},
	isSafari: function () {
		if (typeof AKQA.saf === 'undefined') { AKQA.saf = navigator.userAgent.indexOf('Safari')!==-1; }
		return AKQA.saf;
	},
	canonicalize:{
		symbols:['¡','¢','£','¤','¥','¦','§','¨','©','ª','«','¬','_','®','¯','°','±','²','³','´','µ','¶','·','¸','¹','º','»','¼','½','¾','¿','À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','×','Ø','Ù','Ú','Û','Ü','Ý','Þ','ß','à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','ì','í','î','ï','ð','ñ','ò','ó','ô','õ','ö','÷','ø','ù','ú','û','ü','ý','þ','ÿ','‘','’','“','”'],
		codes:['&iexcl;','&cent;','&pound;','&curren;','&yen;','&brvbar;','&sect;','&uml;','&copy;','&ordf;','&laquo;','&not;','&shy;','&reg;','&macr;','&deg;','&plusmn;','&sup2;','&sup3;','&acute;','&micro;','&para;','&middot;','&cedil;','&sup1;','&ordm;','&raquo;','&frac14;','&frac12;','&frac34;','&iquest;','&Agrave;','&Aacute;','&Acirc;','&Atilde;','&Auml;','&Aring;','&AElig;','&Ccedil;','&Egrave;','&Eacute;','&Ecirc;','&Euml;','&Igrave;','&Iacute;','&Icirc;','&Iuml;','&ETH;','&Ntilde;','&Ograve;','&Oacute;','&Ocirc;','&Otilde;','&Ouml;','&times;','&Oslash;','&Ugrave;','&Uacute;','&Ucirc;','&Uuml;','&Yacute;','&THORN;','&szlig;','&agrave;','&aacute;','&acirc;','&atilde;','&auml;','&aring;','&aelig;','&ccedil;','&egrave;','&eacute;','&ecirc;','&euml;','&igrave;','&iacute;','&icirc;','&iuml;','&eth;','&ntilde;','&ograve;','&oacute;','&ocirc;','&otilde;','&ouml;','&divide;','&oslash;','&ugrave;','&uacute;','&ucirc;','&uuml;','&yacute;','&thorn;','&yuml;','&#145;','&#146;','&#147;','&#148;'],
		associate: function () {
			if (AKQA.canonicalize.symbols.length !== AKQA.canonicalize.codes.length) { throw 'The number of symbols and html codes are not the same.'; }
			if (!$defined(AKQA.canonicalize.codesSymbols)) { AKQA.canonicalize.codesSymbols = AKQA.canonicalize.symbols.associate(AKQA.canonicalize.codes); }
			if (!$defined(AKQA.canonicalize.symbolsCodes)) { AKQA.canonicalize.symbolsCodes = AKQA.canonicalize.codes.associate(AKQA.canonicalize.symbols); }
		},
		codeToSymbol: function (code) {
			AKQA.canonicalize.associate();
			var symbol = AKQA.canonicalize.codesSymbols[code];
			return symbol;
		},
		symbolToCode: function (symbol) {
			AKQA.canonicalize.associate();
			var code = AKQA.canonicalize.symbolsCodes[symbol];
			return code;
		},
		convertCodesToSymbols: function (val) {
			var i, code, symbol, str;
			str = val;
			for (i=0;i<AKQA.canonicalize.codes.length;i=i+1) {
				code = AKQA.canonicalize.codes[i];
				while (str.indexOf(code) !== -1) {
					str = str.replace(code,AKQA.canonicalize.codeToSymbol(code));
				}
			}
			return str;
		},
		convertSymbolsToCodes: function (val) {
			var i, code, symbol, str;
			str = val;
			for (i=0;i<AKQA.canonicalize.symbols.length;i=i+1) {
				symbol = AKQA.canonicalize.symbols[i];
				while (str.indexOf(symbol) !== -1) {
					str = str.replace(symbol,AKQA.canonicalize.symbolToCode(code));
				}
			}
			return str;
		}
	}
};

var Caller = new Class({
	initialize: function(params){
		if (params) {
			if (!(params instanceof Object)) {
				throw 'Caller initialization parameters must be an object';
			}
			if (params.onStart && params.onStart instanceof Function) {
				this.addEvent('onStart',params.onStart);
			}
			if (params.onStartDuration && params.onStartDuration instanceof Number && params.onStartDuration > 0) {
				this.startDuration = params.onStartDuration;
			} else { this.startDuration = 0; }
			if (params.onComplete && params.onComplete instanceof Function) {
				this.addEvent('onComplete',params.onComplete);
			}
		}
	},
	start: function() {
		this.fireEvent('onStart');
	},
	main: function(fcnToCall,args,duration) {
		var callable;
		this.start();
		if (fcnToCall && fcnToCall instanceof Function) {
			if (args && (args instanceof Array)) {
				callable = fcnToCall.pass(args,this);
			} else {
				callable = fcnToCall.bind(this);
			}
		}
		this.mainFcn = callable;
		if (duration && $type(duration) === 'number' && duration > 0) { this.mainDuration = duration; }
			else { this.mainDuration = 0; }
		this.callMain.delay(this.startDuration,this);
	},
	callMain: function () {
		if (this.mainFcn && this.mainFcn instanceof Function) { this.mainFcn(); }
		this.finish.delay(this.mainDuration,this);
	},
	finish: function() {
		this.fireEvent('onComplete');
	}
});
Caller.implement(new Events());

var localize = {
	environment:'prod',
	images:function() {
		if (localize.environment==='local') { localize.imagePath='../gallery/images'; }
			else { localize.imagePath='/resources/global/images'; }
		return localize.imagePath;
	},
	getFileLocation:function() {
		var lang = localize.getLanguage();
		var country = localize.getCountry();
		var filename;
		var enROW = '/NR/rdonlyres/FF30FE1B-01B3-4D70-A8E9-704D6014623E/0/en_row.js'; // location of the en_row translation
		if (localize.environment==='local') {
			return '../gallery/localization/'+lang+'_'+country+'.js';
		} else {
			if (lang && country) {
				switch (country.toLowerCase()) {
					case 'ca':
						switch (lang.toLowerCase()) {
							case 'en': // location of en_CA translation
								filename = '/NR/rdonlyres/363E44F0-CB5F-4417-BD7E-46618F99E88E/0/en_ca.js';
								//filename = enROW;
								break;
							case 'fr': // location of fr_CA translation
								filename = '/NR/rdonlyres/282599E8-2A1B-4113-9CEB-E6BF604A1C8A/0/fr_ca.js';
								//filename = enROW;
								break;
						}
						break;
					case 'jp':
						filename = '/NR/rdonlyres/58046BAB-5F49-434B-9458-CCDB371182E7/0/en_jp.js';
						//filename = enROW;
						break;
					case 'br':
						filename = '/NR/rdonlyres/67C591AF-32BC-4281-8FC0-0762A002D53C/0/ptbr.js';
						break;
					case 'bg':
						filename = '/NR/rdonlyres/41475B3A-47A7-43EA-AB2B-34486DA2CBE0/0/bg_bg.js';
						break;
					case 'it':
						filename = '/NR/rdonlyres/7D50FC94-859A-4311-A78C-08B896A5901D/0/itit.js';
						break;
					case 'es':
						filename = '/NR/rdonlyres/3265DE43-0A67-497C-9D2B-58FA7ED7C595/0/es_es.js';
						break;
					case 'rla':
						filename = '/NR/rdonlyres/D30A2C38-469C-4021-BC3A-74197CF3B1A7/0/es_rla.js';
						break;
					case 'ar':
						filename = '/NR/rdonlyres/C5644A2F-036F-492A-9BBC-47FDA7B9E7CB/0/es_ar.js';
						break;
					case 'cl':
						filename = '/NR/rdonlyres/E82FEB4F-4F89-4BAB-A6AE-299C6BCFA0F6/0/es_cl.js';
						break;
					case 'co':
						filename = '/NR/rdonlyres/7E70E0D5-FB33-4A43-99B2-3365302D1C7A/0/es_co.js';
						break;
					case 've':
						filename = '/NR/rdonlyres/97185952-5BBF-4854-879C-4DE4C0095DD5/0/es_ve.js';
						break;
					case 'mx':
						filename = '/NR/rdonlyres/E71C65B3-6967-4900-90FE-9B27220E09E6/0/es_mx.js';
						break;
					case 'do':
						filename = '/NR/rdonlyres/6399DE3E-921D-4FF6-B132-E9028C1D51D0/0/es_do.js';
						break;
					case 'cr':
						filename = '/NR/rdonlyres/DFC0AA75-486A-4274-97B5-84F24D5C87C0/0/es_cr.js';
						break;
					// location of de_DE translation
					case 'de': 
						filename = '/NR/rdonlyres/27CBDD73-8720-44E3-88B0-C2E97069696E/0/de_de.js';
						break;
					// location of de_AT translation
					case 'at': 
						filename = '/NR/rdonlyres/50FCE879-5791-420E-8283-2C6E0A21CC20/0/de_at.js';
						break;
					case 'ch':
						switch (lang.toLowerCase()) {
							case 'de': // location of de_CH translation
								filename = '/NR/rdonlyres/FD057904-9416-4EDF-8105-FC437407A6C6/0/de_ch.js';
								break;
							case 'fr': // location of fr_CH translation
								filename = '/NR/rdonlyres/36A4DB20-9921-4769-B667-7CC4FFE66798/0/fr_ch.js';
								break;
						}
						break;
					default:
						filename = enROW;
						break;
				}
			} else {
				filename = enROW;
			}
		}
		return filename;
	},
	fetchDefault:function(fillFcn) {
		localize.fetch('en','row',fillFcn);
	},
	fetch:function(fillFcn) {
		localize.images();
		if (typeof g_bWarnBeforeLeave !== 'undefined' && g_bWarnBeforeLeave) { localize.showBody(); return; }
		//$(document.body).setStyle('visibility','hidden');
		var url = localize.getFileLocation();
		vals = false;
		var script = new Asset.javascript(url,{});
		setTimeout(function() {localize.createFill(fillFcn);},100);
	},
	createFill:function(fillFcn) {
		var fill;
		if (vals) {
			fill = new Caller();
			fill.addEvent('onStart',localize.loadAssets.bind(fill));
			fill.addEvent('onComplete',localize.showBody.bind(fill));
			try {
				fill.main(fillFcn,[vals],1000);
			} catch (error) {
				fill.finish();
				throw error;
			}
		} else {
			setTimeout(function() {localize.createFill(fillFcn);},50);
		}
	},
	showBody: function () {
		var body;
		if (document.body) {
			body = $(document.body);
			if (AKQA.isSafari()) {
				// assuming that if there is no computed style for the body element it's because we've hidden it
				// this is necessary because Safari does not return a computed style object for elements with display
				// none which causes mootools' Element.getStyle method to throw an error
				if (null === document.defaultView.getComputedStyle(document.body,null)) { body.setStyle('display','block'); }
			} else {
				if (body.getStyle('display') === 'none') { body.setStyle('display','block'); }
			}
			if (body.getStyle('visibility') === 'hidden') { body.setStyle('visibility','visible'); }
			if (body.getStyle('opacity') === 0) { body.setStyle('opacity',1); }
		}
	},
	loadAssets:function() {
		var lang = localize.getLanguage();
		var country = localize.getCountry();
		var filename = false;
		var cssAsset, jsAsset;
		if (localize.environment==='prod') {
			if (lang && country) {
				switch (country.toLowerCase()) {
					case 'row':
						break;
					case 'ca':
						switch (lang.toLowerCase()) {
							case 'en':
							 	// location of en_CA css
								filename = '/NR/rdonlyres/2FF7516B-78F8-4CE8-AB00-39812F49C4AB/0/enca.css';
								break;
							case 'fr':
								// location of fr_CA css
								filename = '/NR/rdonlyres/7610017D-3224-4497-8574-839DE5B3D7B1/0/frca.css';
								break;
						}
						break;
					case 'es':
						filename = '/NR/rdonlyres/C8BC1BF3-21CC-4521-B17F-57EFBE54042B/0/eses.css';
						break;
					case 'bg':
						filename = '/NR/rdonlyres/C336EF82-4A9F-4CDC-9D0B-4B7DBA8C9CD5/0/bgbg.css';
						break;
					default:
						break;
				}
			}
		} else { filename = '/CmsUpdates/bin/gallery/localization/css/fr-ca.css'; }
		if (filename) {
			cssAsset = new Asset.css(filename,{});
		}
		if (localize.jsAsset) {
			jsAsset = new Asset.javascript(localize.jsAsset,{});
		}
	},
	setLanguage:function(language) {
		Cookie.set('language',language,{domain:'.smirnoff.com',path:'/',duration:false,secure:false});
	},
	setCountry:function(pCountry) {
		if (pCountry.toLowerCase() !== 'row' && pCountry.length > 2) {
			localize.setCountry(pCountry.substring(0,2));
			localize.setLanguage(pCountry.substring(2));
		} else {
			Cookie.set('country',pCountry,{domain:'.smirnoff.com',path:'/',duration:false,secure:false});
		}
	},
	getLanguage:function() {
		return Cookie.get('language');
	},
	getCountry:function() {
		return Cookie.get('country');
	},
	updateGeneral:function(values) {
		var span, contact, i, link;
		localize.updateElements($$('div.frameHeader div.leftSide a.noDecoration'),values.pages.general.header.leftSide);
		localize.updateElements($$('div.frameHeader div.rightSide a.noDecoration'),values.pages.general.header.rightSide);
		localize.updateElements($$('div.frameFooter div.leftSide a.noDecoration'),values.pages.general.footer.leftSide);
		localize.updateElements($$('div.frameFooter div.rightSide a.noDecoration'),values.pages.general.footer.rightSide);
		var links = $$('div.frameHeader a.noDecoration, div.frameFooter a.noDecoration');
		for (i=0;i<links.length;i=i+1) {
			link = links[i];
			if (link.getProperty('href') === '#') {
				link.setStyle('cursor','pointer');
				link.removeProperty('href');
			}
		}
		if ($('content')) {
			if (values.pages.general.header.contact) {
				span = new Element('span').setHTML('&nbsp;&middot;&nbsp;').injectInside($E('div.frameHeader div.leftSide'));
				contact = new Element('a').setProperties({title:values.pages.general.header.contact});
				contact.setStyle('cursor','pointer');
				contact.addClass('noDecoration');
				contact.setHTML(values.pages.general.header.contact);
				contact.onclick = smirnoffPopup.pass(['contactus'],this);
				contact.injectInside($E('div.frameHeader div.leftSide'));
			}
			$E('p.copy','popupBlocked').setHTML(values.pages.general.popupBlocked);
		}
	},
	updateElement:function(element,val) {
		if (element) {
			$(element).setHTML(val);
		} else {
			setTimeout(localize.updateElement.pass([element,val],this),50);
		}
	},
	updateElements:function(elements,vals) {
		for (var i=0;i<elements.length;i=i+1) {
			elements[i].setHTML(vals[i]);
			if (elements[i].getProperty('title')) { elements[i].setProperty('title',AKQA.canonicalize.convertCodesToSymbols(vals[i])); }
		}
	},
	updateContainerHeight:function(image,parent) {
		var img = $(image);
		var dImg = new Image();
		dImg.par = parent;
		dImg.src = img.src;
		dImg.onload = function() { $(this.par).setStyle('height',dImg.height+'px'); };
	},
	updatePageTitle: function (pageObject) {
		if (pageObject && pageObject.title) {
			document.title = AKQA.canonicalize.convertCodesToSymbols(pageObject.title);
		}
	},
	handlemouse:function(e,imageObj) {
		var evt = new Event(e);
		var type = evt.type;
		var png = $(this).hasClass('png');
		var extension = imageObj.src.substring(imageObj.src.lastIndexOf('.')+1);
		var imgSrc;
		function srcReplace(img,oldSrc,newSrc) {
			if (png) {
				imgSrc = img.getStyle('filter');
				imgSrc = imgSrc.replace(oldSrc,newSrc);
				img.setStyle('filter',imgSrc);
			} else {
				img.src = newSrc;
			}
		}
		switch (type) {
			case 'mouseover':
				//srcReplace(this,imageObj.src,imageObj.over);
				this.src = imageObj.over;
				break;
			case 'mouseout':
				//srcReplace(this,imageObj.over,imageObj.src);
				this.src = imageObj.src;
				break;
			case 'mousedown':
				//srcReplace(this,imageObj.src,imageObj.down);
				this.src = imageObj.down;
				break;
			case 'mouseup':
				//srcReplace(this,imageObj.down,imageObj.src);
				this.src = imageObj.src;
				break;
		}
	},
	setupImg:function(image,imageObj) {
		var normal, over, down;
		var img = $(image);
		if (!img) { return; }
		var extension;
		if (imageObj.src) {
			img.setProperty('src',imageObj.src);
			normal = new Asset.image(imageObj.src);
			extension = imageObj.src.substring(imageObj.src.lastIndexOf('.')+1);
		}
		if (extension && extension === 'png') { if (!img.hasClass(extension)) {img.addClass(extension);} }
		if (imageObj.alt) { img.setProperty('alt',AKQA.canonicalize.convertCodesToSymbols(imageObj.alt)); }
		if (imageObj.height) { img.setProperty('height',imageObj.height); }
		if (imageObj.width) { img.setProperty('width',imageObj.width); }
		if (imageObj.over) {
			over = new Asset.image(imageObj.over);
			img.onmouseover = img.onmouseout = localize.handlemouse.bindAsEventListener(img,[imageObj]);
		}
		if (imageObj.down) {
			down = new Asset.image(imageObj.down);
			img.onmousedown = img.onmouseup = localize.handlemouse.bindAsEventListener(img,[imageObj]);
		}
	},
	setupBgImg:function(block,imageObj) {
		var img = $(block);
		var extension;
		if (!img) { return; }
		if (imageObj.src) { img.setStyle('backgroundImage','url('+imageObj.src+')'); extension = imageObj.src.substring(imageObj.src.lastIndexOf('.')+1); }
		if (extension && extension === 'png') { if (!img.hasClass(extension)) {img.addClass(extension);} }
		if (imageObj.height) { img.setStyle('height',imageObj.height); }
		if (imageObj.width) { img.setStyle('width',imageObj.width); }
	},
	constructBody:function(bodyObj) {
		var i;
		var body = '';
		for (i=0;i<bodyObj.length;i=i+1) {
			body += bodyObj[i];
		}
		return body;
	},
	accessibility:{
		init:function() {
			var fillFcn = localize.accessibility.fill;
			localize.fetch(fillFcn);
		},
		fill:function(res) {
			var values = res;
			localize.updatePageTitle(values.pages.accessibility);
			localize.setupImg($E('img','privacyStatement'),values.pages.accessibility.headerImg);
			if (values.pages.accessibility.body) { $E('div.body').setHTML(localize.constructBody(values.pages.accessibility.body)); }
		},
		getAccessibilityUrl: function () {
			var lang = localize.getLanguage();
			var country = localize.getCountry();
			var enRow = '/en-row/accessibility.html';
			var fileName = enRow;
			/* This if and switch statement will have to be uncommented.
			 * Please note that the same popupOpen function is called on both the gateway and the home page
			 * so make sure the accessibility statement is in the same location on both the gateway and the
			 * .com site.
			 * The default path is defined in enRow and will be returned unless the specific country
			 * or country/language combination cases are defined in the switch statements below.
			 * The country codes and language codes used should follow the ISO standards.
			 * If a country has only one language change 'country code here' to the code for the country in question and change the fileName
			 * assignment just above the break statement related to the 'country code here' case to the location
			 * of the accessibility statement html file.
			 * If a country has multiple languages change 'country code with multiple languages here' to the
			 * code for the country in question. For the first language change the 'lang 1 here' to the ISO language
			 * code for the language and the fileName assignment directly before the break statement related to the 'lang 1 here'
			 * to the location of the accessibility statement html file.
			 */
			if (lang && country) {
				switch (country.toLowerCase()) {
					case 'de':
						fileName = '/de-de/accessibility.htm';
						break;
						case 'at':
						fileName = '/de-at/accessibility.htm';
						break;
					case 'br':
						fileName = '/pt-br/accessibility.htm';
						break;
					case 'bg':
						fileName = '/bg-bg/accessibility.htm';
						break;
					case 'rla':
						fileName = '/es-rla/accessibility.htm';
						break;
					case 'ar':
						fileName = '/es-ar/accessibility.htm';
						break;
					case 'cl':
						fileName = '/es-cl/accessibility.htm';
						break;
					case 'cr':
						fileName = '/es-cr/accessibility.htm';
						break;
					case 'do':
						fileName = '/es-do/accessibility.htm';
						break;
					case 'mx':
						fileName = '/en-mx/accessibility.htm';
						break;
					case 've':
						fileName = '/es-ve/accessibility.htm';
						break;
					case 'co':
						fileName = '/es-co/accessibility.htm';
						break;
					case 'it':
						fileName = '/it-it/accessibility.htm';
						break;
					case 'ch':
						switch (lang.toLowerCase()) {
							case 'de':
								fileName = '/de-ch/accessibility.htm';
								break;
							case 'fr':
								fileName = '/fr-ch/accessibility.htm';
								break;
						}
						break;
				}
			} else {
				fileName = enRow;
			}
			return fileName;
		}
	},
	contact:{
		init:function() {
			var fillFcn = localize.contact.fill;
			localize.fetch(fillFcn);
		},
		fill:function(res) {
			var values = res;
			localize.updatePageTitle(values.pages.contact);
			localize.setupImg('phTitle_PresentationModeControlsContainer_PresentationImage',values.pages.contact.headerImg);
			$('lblRequiredFields').setHTML(values.pages.contact.requiredFields);
			$('phEmailLbl').setHTML(values.pages.contact.email);
			$('phSubjectLbl').setHTML(values.pages.contact.subject);
			localize.updateElements($ES('option','phSubjectXML_PresentationModeControlsContainer_DDLControl'),values.pages.contact.subjectOptions);
			$('phMessageLbl').setHTML(values.pages.contact.message+' <span id="rightCopy">'+values.pages.contact.charactersRemaining);
			$('phSendCopyLbl').setHTML(values.pages.contact.sendCopy);
			localize.setupImg('btnCancel',values.pages.contact.cancelButton);
			localize.setupImg('btnSubmit',values.pages.contact.submitButton);
			$('hdnInvalidText').setProperties({value:values.pages.contact.errorMsg});
			$('hdnSubjectReq').setProperties({value:values.pages.contact.subjectReq});
		},
		getContactUrl: function () {
			var lang = localize.getLanguage();
			var country = localize.getCountry();
			var filename;
			var enROW = '/en-row/Utilities/ContactUs.htm'; // location of the en_row registration
			if (lang && country) {
				switch (country.toLowerCase()) {
					case 'ca':
						switch (lang.toLowerCase()) {
							case 'en': // location of en_CA registration
								filename = '/en-ca/Utilities/ContactUs.htm';
								//filename = enROW;
								break;
							case 'fr': // location of fr_CA registration
								filename = '/fr-ca/Utilities/ContactUs.htm';
								//filename = enROW;
								break;
							default:
								filename = enROW;
								break;
						}
						break;
					case 'br':
						filename = '/pt-br/Utilities/ContactUs.htm';
						break;
					case 'es':
						filename = '/es-es/Utilities/ContactUs.htm';
						//filename = enROW;
						break;
					case 'it':
						filename = '/it-it/Utilities/ContactUs.html';
						break;
					case 'de':
						fileName = '/de-de/Utilities/ContactUs.htm';
						break;
					case 'at':
						fileName = '/de-at/Utilities/ContactUs.htm';
						break;
					case 'ch':
						switch (lang.toLowerCase()) {
							case 'de':
								fileName = '/de-ch/Utilities/ContactUs.htm';
								break;
							case 'fr':
								fileName = '/fr-ch/Utilities/ContactUs.htm';
								break;
						}
						break;
						
					default:
						filename = enROW;
						break;
				}
			} else {
				filename = enROW;
			}
			return filename;
		}
	},
	contactSuccess:{
		init:function() {
			var fillFcn = localize.contactSuccess.fill;
			localize.fetch(fillFcn);
		},
		fill:function(res) {
			var values = res;
			localize.updatePageTitle(values.pages.contactSuccess);
			localize.setupImg($E('img','contactUs'),values.pages.contactSuccess.headerImg);
			$E('p.sentCopy').setHTML(values.pages.contactSuccess.sentCopy);
			localize.setupImg('finished',values.pages.contactSuccess.finishedButton);
		}
	},
	forgotPassword:{
		init:function() {
			var fillFcn = localize.forgotPassword.fill;
			localize.fetch(fillFcn);
		},
		fill:function(res) {
			var values = res;
			localize.updatePageTitle(values.pages.forgotPassword);
			localize.setupImg($E('img','signMeUp'),values.pages.forgotPassword.headerImg);
			$('instructionCopy').setHTML(values.pages.forgotPassword.instruction);
			$('phEmailbl').setHTML(values.pages.forgotPassword.email);
			localize.setupImg('back',values.pages.forgotPassword.backButton);
			localize.setupImg('sendpassword',values.pages.forgotPassword.sendButton);
			$('hdnInvalidText').setProperties({value:'<p class=\'errorCopy1\'>'+values.pages.forgotPassword.missingFields+'</p>'});
			$('hdnMessage').setProperties({value:'<p id=\'instructionCopy\'>'+values.pages.forgotPassword.instruction+'</p>'});
		},
		getForgotUrl: function () {
			var lang = localize.getLanguage();
			var country = localize.getCountry();
			var filename;
			var enROW = '/en-row/Secure/ForgotPW/forgot_password.htm';
			if (lang && country) {
				switch (country.toLowerCase()) {
					case 'ca':
						switch (lang.toLowerCase()) {
							case 'en': // location of en_CA registration
								filename = '/en-ca/Secure/ForgotPW/forgot_password.htm';
								//filename = enROW;
								break;
							case 'fr': // location of fr_CA registration
								filename = '/fr-ca/Secure/ForgotPW/forgot_password.htm';
								//filename = enROW;
								break;
							default:
								filename = enROW;
								break;
						}
						break;
					case 'it':
						filename = '/it-it/Secure/ForgotPW/forgot_password.htm';
						break;
					case 'bg':
						filename = '/bg-bg/Secure/ForgotPW/forgot_password.htm';
						break;
					case 'ie':
						filename = '/en-ie/Secure/ForgotPW/forgot_password.htm';
						break;
					case 'rla':
						filename = '/es-rla/Secure/ForgotPW/forgot_password.htm';
						break;
					case 'es':
						filename = '/es-es/Secure/ForgotPW/forgot_password.htm';
						//filename = enROW;
						break;
						case 'de':
						filename = '/de-de/Secure/ForgotPW/forgot_password.htm';
						break;
						case 'at':
						filename = '/de-at/Secure/ForgotPW/forgot_password.htm';
						break;
					case 'ch':
						switch (lang.toLowerCase()) {
							case 'de':
								filename = '/de-ch/Secure/ForgotPW/forgot_password.htm';
								break;
							case 'fr':
								filename = '/fr-ch/Secure/ForgotPW/forgot_password.htm';
								break;
						}
						break;
						
					default:
						filename = enROW;
						break;
				}
			} else {
				filename = enROW;
			}
			return filename;
		}
	},
	gateway:{
		init: function () {
			var fillFcn = localize.gateway.fill;
			localize.fetch(fillFcn);
		},
		fill: function (res) {
			var values = res;
			localize.home.fill(res);
			//localize.updatePageTitle(values.pages.gateway);
		}
	}, 
	home:{
		init:function() {
			var fillFcn = localize.home.fill;
			localize.fetch(fillFcn);
		},
		fill:function(res) {
			var flashDetect,getFlash,drDiv,drImage,drImg,homeDiv;
			var values = res;
			localize.updatePageTitle(values.pages.home);
			localize.updateGeneral(values);
			if ($('content')) {
				flashDetect = $E('img','flashDetectHeader');
				localize.setupImg(flashDetect,values.pages.home.flashDetect);
				flashDetect.onload = localize.updateContainerHeight.pass([flashDetect,$('flashDetectHeader')],this);
				getFlash = $E('img',$('flashHtmlButtons'));
				localize.setupImg(getFlash,values.pages.home.flashUpgrade);
				getFlash.onload = localize.updateContainerHeight.pass([getFlash,$('flashHtmlButtons')],this);
				localize.updateElements($$('p.shellCopy'),values.pages.home.shellCopy);
			}
			if (values.pages.home.drinkResponsiblyImg) {
				homeDiv = $E('.home');
				drDiv = new Element('div').injectInside($E('div.wrapper')).setProperties({id:'drinkResponsibly'});
				drImage = new Asset.image(values.pages.home.drinkResponsiblyImg.src);
				drImg = new Element('img').setProperties({src:values.pages.home.drinkResponsiblyImg.src,id:'drinkResponsibilityImage'}).injectInside(drDiv);
				drDiv.setStyles({position:'absolute',width:values.pages.home.drinkResponsiblyImg.width,height:values.pages.home.drinkResponsiblyImg.height});
				localize.home.repositionDrDiv();
				drDiv.setStyles({paddingBottom:'5px',paddingTop:'5px'});
				window.addEvent('resize',localize.home.repositionDrDiv);
				window.oldSetStyleByTag = window.setStyleByTag;
				window.setStyleByTag = function(tag, property, value, global) {
					$ES(tag).setStyle(property,value);
					if (property === 'backgroundColor' && value.toLowerCase() === '#000000') {
						$('drinkResponsibilityImage').setProperty('src',vals.pages.home.drinkResponsiblyImg[value]);
					} else if (property === 'backgroundColor' && value.toLowerCase() === '#ffffff') {
						$('drinkResponsibilityImage').setProperty('src',vals.pages.home.drinkResponsiblyImg[value.toLowerCase()]);
					}
				}
			}
		},
		repositionDrDiv: function () {
			var homeDiv = $('frame');
			var drDiv = $('drinkResponsibly');
			drDiv.setStyles({top:homeDiv.getPosition().y+homeDiv.getSize().size.y+'px',
				left:homeDiv.getPosition().x+homeDiv.getSize().size.x-drDiv.getSize().size.x-15+'px'});
		}
	},
	htmlHome:{
		init:function() {
			var fillFcn = localize.htmlHome.fill;
			localize.fetch(fillFcn);
		},
		fill:function(res) {
			var i;
			var values = res;
			localize.updatePageTitle(values.pages.htmlHome);
			localize.updateGeneral(values);
			var getToKnow = $ES('p.copy','gettoknow');
			var props = values.pages.htmlHome.enterPage.getToKnow;
			for (i=0;i<getToKnow.length;i=i+1) {
				if (getToKnow[i].hasClass('medium')) { getToKnow[i].setHTML('<img class="png" height="4" src="'+localize.imagePath+'/Family/Txt.png" width="24" />'+props[i]+'<img class="png" height="4" src="'+localize.imagePath+'/Family/Txt_flip.png" width="24" />'); }
				else { getToKnow[i].setHTML(props[i]); }
			}
			var imgs = $ES('div.images div img','enterpage');
			for (i=0;i<imgs.length;i=i+1) { localize.setupImg(imgs[i],values.pages.htmlHome.enterPage.images[i]); }
			localize.setupBgImg($E('div.familyRibbon div.background','productpage1'),values.pages.htmlHome.familyRibbon.background);
			localize.setupImg($E('div.familyRibbon div#ribbon img','productpage1'),values.pages.htmlHome.familyRibbon.ribbon);
			localize.setupImg($E('div.familyRibbon div#familyScript img','productpage1'),values.pages.htmlHome.familyRibbon.familyScript);
			localize.setupBgImg($E('div.familyRibbon div.background','productpage2'),values.pages.htmlHome.familyRibbon.background);
			localize.setupImg($E('div.familyRibbon div#ribbon img','productpage2'),values.pages.htmlHome.familyRibbon.ribbon);
			localize.setupImg($E('div.familyRibbon div#familyScript img','productpage2'),values.pages.htmlHome.familyRibbon.familyScript);
			localize.setupBgImg($E('div#centerpiece','familyPhoto'),values.pages.htmlHome.productpage1.familyPhoto.centerpiece);
			localize.setupBgImg($E('div.background','familyPhoto'),values.pages.htmlHome.productpage1.familyPhoto.background);
			localize.setupBgImg($E('div#centerRibbon','familyPhoto'),values.pages.htmlHome.productpage1.familyPhoto.centerRibbon);
			localize.setupImg($E('div.leftArrow img','productpage1'),values.pages.htmlHome.productpage1.leftArrow);
			localize.setupImg($E('div.rightArrow img','productpage1'),values.pages.htmlHome.productpage1.rightArrow);
			localize.setupImg($E('div.leftArrow img','productpage2'),values.pages.htmlHome.productpage2.leftArrow);
			localize.setupImg($E('div.rightArrow img','productpage2'),values.pages.htmlHome.productpage2.rightArrow);
			$$('div.disclaimer').setHTML(values.pages.htmlHome.disclaimer);
			$$('div.closeButton p.closeCopy span.closeText').setHTML(values.pages.htmlHome.close);
			var productBlocks = $$('div.productBlock');
			for (i=0;i<productBlocks.length;i=i+1) { localize.htmlHome.updateProduct(productBlocks[i],values.pages.htmlHome.products[i]); }
			var wallBlocks = $$('div.wallBlock');
			for (i=0;i<wallBlocks.length;i=i+1) { localize.htmlHome.updateWallItem(wallBlocks[i],values.pages.htmlHome.wallItems[i]); }
			var numShelfItems = $$('div.shelfCopy').length;
			for (i=0;i<numShelfItems;i=i+1) { localize.htmlHome.updateShelfItem(i+1,values.pages.htmlHome.shelfItems[i]); }
			var show = new Caller({
				onStart:function(){$(document.body).setStyle('display','block');},
				onComplete:function(){init(window,'frame',40,15);}
			}); show.main();
			var eventRegistration;
			if (isIE && ieVer<6) {
				eventRegistration = new Asset.javascript('/NR/rdonlyres/BDEF8ED6-94D7-4B6C-A209-0BBA87B0889B/0/htmlhome_events_s.js');
			} else {
				eventRegistration = new Asset.javascript('/NR/rdonlyres/1968B06A-080B-4388-9B24-65A1BFF317DA/0/htmlhome_events_refactor.js');
			}
		},
		updateProduct:function(productBlock,productObject) {
			localize.setupImg($E('div.productImage img.bottle',productBlock),productObject.smallBottle);
			localize.setupImg($E('div.productCopy img.bottle',productBlock),productObject.largeBottle);
			$E('div.closeButton p.closeCopy span.closeText',productBlock).setHTML(vals.pages.htmlHome.close);
			$E('div.productCopy div.copy p.header',productBlock).setHTML(productObject.header);
			$E('div.productCopy div.copy p.subheader',productBlock).setHTML(productObject.subheader);
			$E('div.productCopy div.copy p.body',productBlock).setHTML(productObject.copy);
		},
		updateWallItem:function(wallBlock,wallObject) {
			localize.setupImg($E('div.wallImage img.item',wallBlock),wallObject.smallImg);
			localize.setupImg($E('div.wallCopy img.item',wallBlock),wallObject.largeImg);
			$E('div.wallCopy div.copy p.header',wallBlock).setHTML(wallObject.header);
			$E('div.wallCopy div.copy p.body',wallBlock).setHTML(wallObject.body);
		},
		updateShelfItem:function(index,shelfObject) {
			localize.setupImg($E('img','shelfItem'+index+'Image'),shelfObject.smallImg);
			localize.setupImg($E('img.item','shelfItem'+index+'Copy'),shelfObject.largeImg);
			$E('div.copy p.header','shelfItem'+index+'Copy').setHTML(shelfObject.header);
			$E('div.copy p.body','shelfItem'+index+'Copy').setHTML(shelfObject.body);
		}
	},
	login:{
		init:function() {
			var fillFcn = localize.login.fill;
			
			localize.fetch(fillFcn);
		},
		fill:function(res) {
			var values = res;
			localize.updatePageTitle(values.pages.login);
			localize.setupImg('phTitle_PresentationModeControlsContainer_PresentationImage',values.pages.login.headerImg);
			$('phUserNameLbl').setHTML(values.pages.login.username);
			$('phRememberLbl').setHTML(values.pages.login.rememberMe);
			$('phPasswordLbl').setHTML(values.pages.login.password);
			$E('a','phForgotPWLink').setProperties({title:values.pages.login.forgotPassword,href:localize.forgotPassword.getForgotUrl()}).setHTML(values.pages.login.forgotPassword);
			localize.setupImg('btnClear',values.pages.login.clearButton);
			localize.setupImg('btnSubmit',values.pages.login.submitButton);
			$('hdnInvalidText').setProperties({value:'<p class=\'errorCopy1\'>'+values.pages.login.missingFields+'</p>'});
		},
		getLoginUrl: function () {
			var lang = localize.getLanguage();
			var country = localize.getCountry();
			var enROW = '/en-row/secure/login/login.htm'; // location of the en_row login
			var filename = enROW;
			/* This if and switch statement will have to be uncommented.
			 * The default path is defined in enRow and will be returned unless the specific country
			 * or country/language combination cases are defined in the switch statements below.
			 * The country codes and language codes used should follow the ISO standards.
			 * If a country has only one language change 'country code here' to the code for the country in question and change the fileName
			 * assignment just above the break statement related to the 'country code here' case to the location
			 * of the login html file.
			 * If a country has multiple languages change 'country code with multiple languages here' to the
			 * code for the country in question. For the first language change the 'lang one' to the ISO language
			 * code for the language and the fileName assignment directly before the break statement related to the 'lang two'
			 * to the location of the login html file.
			 */
			
			if (lang && country) {
				switch (country.toLowerCase()) {
					case 'it':
						filename = '/it-it/Secure/Login/Login.htm';
						break;
					case 'rla':
						filename = '/es-rla/Secure/Login/Login.htm';
						break;
					case 'bg':
						filename = '/bg-bg/Secure/Login/Login.htm';
						break;
				  default:
				    filename = enROW;
				    break;
				}
			} else {
			  filename = enROW;
			}  
			return filename;
		}
	},
	manageAccount:{
		init:function() {
			var fillFcn = localize.manageAccount.fill;
			localize.fetch(fillFcn);
		},
		fill:function(res) {
			var values = res;
			localize.updatePageTitle(values.pages.manageAccount);
			var body = $(document.body);
			localize.setupImg('phTitle_PresentationModeControlsContainer_PresentationImage',values.pages.manageAccount.header.signMeUp);
			$E('div.body p.requiredText',body).setHTML(values.pages.manageAccount.body.requiredFields);
			$('errMsg').setHTML(values.pages.manageAccount.body.errorCopy);
			$E('div.body div.mainLeftColumn h2.headerText',body).setHTML(values.pages.manageAccount.body.needToHave);
			$('GenderLabel').setHTML(values.pages.manageAccount.body.gender);
			$E('label',$('GenderLabel1')).setHTML(values.pages.manageAccount.body.female);
			$E('label',$('GenderLabel2')).setHTML(values.pages.manageAccount.body.male);
			$('FirstNameLabel').setHTML(values.pages.manageAccount.body.firstName);
			$('LastNameLabel').setHTML(values.pages.manageAccount.body.lastName);
			$E('label',$('DOBLabel')).setHTML(values.pages.manageAccount.body.dateOfBirth);
			$E('label',$('CountryLabel')).setHTML(values.pages.manageAccount.body.country);
			$E('div.loginData h3.headerText',body).setHTML(values.pages.manageAccount.body.logInInfo);
			$E('label',$('EmailLabel')).setHTML(values.pages.manageAccount.body.email);
			$E('label',$('PasswordLabel')).setHTML(values.pages.manageAccount.body.password);
			$('ConfirmLabel').setHTML(values.pages.manageAccount.body.confirmPassword);
			$E('div.optionalPersonalData h2.headerText',body).setHTML(values.pages.manageAccount.body.niceToHave);
			$E('label',$('AddressLabel1')).setHTML(values.pages.manageAccount.body.address1);
			$E('label',$('AddressLabel2')).setHTML(values.pages.manageAccount.body.address2);
			$E('label',$('CityLabel')).setHTML(values.pages.manageAccount.body.county);
			$E('label',$('CountyLabel')).setHTML(values.pages.manageAccount.body.city);
			$E('label',$('PostCodeLabel')).setHTML(values.pages.manageAccount.body.postcode);
			$E('label',$('PhoneNumLabel')).setHTML(values.pages.manageAccount.body.phoneNumber);
			$E('div.communicationsAndServices h3.headerText',body).setHTML(values.pages.manageAccount.body.keepInTouch);
			$E('label',$('SmirnoffOffersAndPromosLabel')).setHTML(values.pages.manageAccount.body.smirnoffOffers);
			$E('label',$('DiageoOffersAndPromosLabel')).setHTML(values.pages.manageAccount.body.diageoOffers);
			$E('label',$('FocusGroupLabel')).setHTML(values.pages.manageAccount.body.smirnoffResearch);
			$('DeliveryMethodLabel').setHTML(values.pages.manageAccount.body.deliveryMethod);
			$('EmailOptIn').setHTML(values.pages.manageAccount.body.emailOptIn);
			$('MailOptIn').setHTML(values.pages.manageAccount.body.mailOptIn);
			$('SMSOptIn').setHTML(values.pages.manageAccount.body.smsOptIn);
			$E('label',$('CellNumLabel')).setHTML(values.pages.manageAccount.body.mobileNumber);
			$E('p.legalText',$('legalCopy')).setHTML(values.pages.manageAccount.footer.legalText);
			localize.setupImg('btnCancel',values.pages.manageAccount.footer.cancel);
			localize.setupImg('btnSubmit',values.pages.manageAccount.footer.submit);
		},
		getManageAccountUrl: function () {
			var lang = localize.getLanguage();
			var country = localize.getCountry();
			var filename;
			var enROW = '/en-row/Secure/ManageAccount/manage_account.htm'; // location of the en_row registration
			if (lang && country) {
				switch (country.toLowerCase()) {
					case 'ca':
						switch (lang.toLowerCase()) {
							case 'en': // location of en_CA registration
								filename = '/en-ca/Secure/ManageAccount/manage_account_ca.htm';
								//filename = enROW;
								break;
							case 'fr': // location of fr_CA registration
								filename = '/fr-ca/Secure/ManageAccount/manage_account_ca.htm';
								//filename = enROW;
								break;
							default:
								filename = enROW;
								break;
						}
						break;
					case 'es':
						filename = '/es-es/Secure/ManageAccount/manage_account.htm';
						//filename = enROW;
						break;
					case 'th':
						filename = '/th-th/Secure/ManageAccount/manage_account.htm';
						break;
					case 'de':
						filename = '/de-de/Secure/ManageAccount/manage_account.htm';
						break;
						case 'at':
						filename = '/de-at/Secure/ManageAccount/manage_account.htm';
						break;
					case 'ch':
						switch (lang.toLowerCase()) {
							case 'de':
								filename = '/de-ch/Secure/ManageAccount/manage_account.htm';
								break;
							case 'fr':
								filename = '/fr-ch/Secure/ManageAccount/manage_account.htm';
								break;
						}
						break;
					default:
						filename = enROW;
						break;
				}
			} else {
				filename = enROW;
			}
			return filename;
		}
	},
	privacy:{
		init:function() {
			var fillFcn = localize.privacy.fill;
			localize.fetch(fillFcn);
		},
		fill:function(res) {
			var values = res;
			localize.updatePageTitle(values.pages.privacy);
			localize.setupImg($E('img','privacyStatement'),values.pages.privacy.headerImg);
			if (values.pages.privacy.body) { $E('div.body').setHTML(localize.constructBody(values.pages.privacy.body)); }
		},
		getPrivacyUrl: function () {
			var lang = localize.getLanguage();
			var country = localize.getCountry();
			var enRow = '/en-row/privacy.html';
			var fileName = enRow;
			/* This if and switch statement will have to be uncommented.
			 * Please note that the same popupOpen function is called on both the gateway and the home page
			 * so make sure the privacy statement is in the same location on both the gateway and the
			 * .com site.
			 * The default path is defined in enRow and will be returned unless the specific country
			 * or country/language combination cases are defined in the switch statements below.
			 * The country codes and language codes used should follow the ISO standards.
			 * If a country has only one language change 'country code here' to the code for the country in question and change the fileName
			 * assignment just above the break statement related to the 'country code here' case to the location
			 * of the privacy statement html file.
			 * If a country has multiple languages change 'country code with multiple languages here' to the
			 * code for the country in question. For the first language change the 'lang 1 here' to the ISO language
			 * code for the language and the fileName assignment directly before the break statement related to the 'lang 1 here'
			 * to the location of the privacy statement html file.
			 */
			if (lang && country) {
				switch (country.toLowerCase()) {
					case 'de':
						fileName = '/de-de/privacy.html';
						break;
							case 'at':
						fileName = '/de-at/privacy.html';
						break;
					case 'br':
						fileName = '/pt-br/privacy.htm';
						break;
					case 'bg':
						fileName = '/bg-bg/privacy.html';
						break;
					case 'ie':
						fileName = '/en-ie/privacy.htm';
						break;
					case 'rla':
						fileName = '/es-rla/privacy.htm';
						break;
					case 'ar':
						fileName = '/es-ar/privacy.htm';
						break;
					case 'cl':
						fileName = '/es-cl/privacy.htm';
						break;
					case 'cr':
						fileName = '/es-cr/privacy.htm';
						break;
					case 'do':
						fileName = '/es-do/privacy.htm';
						break;
					case 'mx':
						fileName = '/es-mx/privacy.htm';
						break;
					case 've':
						fileName = '/es-ve/privacy.htm';
						break;
					case 'co':
						fileName = '/es-co/privacy.htm';
						break;
					case 'it':
						fileName = '/it-it/privacy.htm';
						break;
					case 'ch':
						switch (lang.toLowerCase()) {
							case 'de':
								fileName = '/de-ch/privacy.html';
								break;
							case 'fr':
								fileName = '/fr-ch/privacy.html';
								break;
						}
						break;
				}
			} else {
				fileName = enRow;
			}
			return fileName;
		}
	},
	registration:{
		getRegistrationUrl: function () {
			var lang = localize.getLanguage();
			var country = localize.getCountry();
			var filename;
			var enROW = '/en-row/Secure/Registration/GVRegistration1.htm'; // location of the en_row registration
			if (lang && country) {
				switch (country.toLowerCase()) {
					case 'ca':
						switch (lang.toLowerCase()) {
							case 'en': // location of en_CA registration
								filename = '/en-ca/Secure/Registration/GVRegistration1.htm';
								//filename = enROW;
								break;
							case 'fr': // location of fr_CA registration
								filename = '/fr-ca/Secure/Registration/GVRegistration1.htm';
								//filename = enROW;
								break;
							default:
								filename = enROW;
								break;
						}
						break;
					case 'es':
						filename = '/es-es/Secure/Registration/GVRegistration1.htm';
						//filename = enROW;
						break;
					case 'de':
						filename = '/de-de/Secure/Registration/DERegistration.htm';
						break;
					case 'th':
						// don't know why this page isn't live, but looks good on the staging site
						filename = '/th-th/Secure/Registration/ThailandRegistration.htm';
						break;
					case 'it':
						filename = enROW;
						break;
					case 'at':
						filename = '/de-at/Secure/Registration/DERegistration.htm';
						break;
					case 'ch':
						switch (lang.toLowerCase()) {
							case 'de':
								filename = '/de-ch/Secure/Registration/DERegistration.htm';
								break;
							case 'fr':
								filename = '/fr-ch/Secure/Registration/DERegistration.htm';
								break;
						}
						break;
					default:
						filename = enROW;
						break;
				}
			} else {
				filename = enROW;
			}
			return filename;
		}
	},
	registration1:{
		init:function() {
			var fillFcn = localize.registration1.fill;
			localize.jsAsset = '/NR/rdonlyres/D28F2040-9967-4773-BABB-9FAE1D4749AB/0/reg1.js';
			localize.fetch(fillFcn);
		},
		fill:function(res) {
			var values = res;
			var body = $(document.body);
			localize.updatePageTitle(values.pages.registration1);
			localize.setupImg('phTitle_PresentationModeControlsContainer_PresentationImage',values.pages.registration1.header.signMeUp);
			localize.setupImg($E('img',$('alreadyRegistered')),values.pages.registration1.header.alreadyRegistered);
			$E('div.body p.requiredText',body).setHTML(values.pages.registration1.body.requiredFields);
			$('errMsg').setHTML(values.pages.registration1.body.errorCopy);
			$E('div.body div.mainLeftColumn h2.headerText',body).setHTML(values.pages.registration1.body.needToHave);
			$('GenderLabel').setHTML(values.pages.registration1.body.gender);
			localize.updateElement($E('label',$('GenderLabel1')),values.pages.registration1.body.female);
			$E('label',$('GenderLabel2')).setHTML(values.pages.registration1.body.male);
			$('FirstNameLabel').setHTML(values.pages.registration1.body.firstName);
			$('LastNameLabel').setHTML(values.pages.registration1.body.lastName);
			$E('label',$('DOBLabel')).setHTML(values.pages.registration1.body.dateOfBirth);
			$E('option[value="DD"]').setHTML(values.pages.registration1.body.day);
			localize.updateElements($ES('select.month option'),values.pages.registration1.body.months);
			$E('option[value="YYYY"]').setHTML(values.pages.registration1.body.year);
			$E('label',$('CountryLabel')).setHTML(values.pages.registration1.body.country);
			$E('div.loginData h3.headerText',body).setHTML(values.pages.registration1.body.logInInfo);
			$E('label',$('EmailLabel')).setHTML(values.pages.registration1.body.email);
			$E('label',$('PasswordLabel')).setHTML(values.pages.registration1.body.password);
			$('ConfirmLabel').setHTML(values.pages.registration1.body.confirmPassword);
			$E('div.optionalPersonalData h2.headerText',body).setHTML(values.pages.registration1.body.niceToHave);
			$E('label',$('AddressLabel1')).setHTML(values.pages.registration1.body.address1);
			$E('label',$('AddressLabel2')).setHTML(values.pages.registration1.body.address2);
			$E('label',$('CityLabel')).setHTML(values.pages.registration1.body.county);
			$E('label',$('CountyLabel')).setHTML(values.pages.registration1.body.city);
			$E('label',$('PostCodeLabel')).setHTML(values.pages.registration1.body.postcode);
			$E('label',$('PhoneNumLabel')).setHTML(values.pages.registration1.body.phoneNumber);
			$E('div.communicationsAndServices h3.headerText',body).setHTML(values.pages.registration1.body.keepInTouch);
			$E('label',$('SmirnoffOffersAndPromosLabel')).setHTML(values.pages.registration1.body.smirnoffOffers);
			$E('label',$('DiageoOffersAndPromosLabel')).setHTML(values.pages.registration1.body.diageoOffers);
			$E('label',$('FocusGroupLabel')).setHTML(values.pages.registration1.body.smirnoffResearch);
			$('DeliveryMethodLabel').setHTML(values.pages.registration1.body.deliveryMethod);
			$E('label[for="EmailOptIn"]').setHTML(values.pages.registration1.body.emailOptIn);
			$E('label[for="MailOptIn"]').setHTML(values.pages.registration1.body.mailOptIn);
			$E('label[for="SMSOptIn"]').setHTML(values.pages.registration1.body.smsOptIn);
			$E('label',$('CellNumLabel')).setHTML(values.pages.registration1.body.mobileNumber);
			$E('p.legalText',$('legalCopy')).setHTML(values.pages.registration1.footer.legalText);
			localize.setupImg('btnCancel',values.pages.registration1.footer.cancel);
			localize.setupImg('btnSubmit',values.pages.registration1.footer.submit);
			//localize.registration1.updateCountries(values.pages.general.countries);
		},
		updateCountries:function(countries) {
			/**
			 * @TODO: This function needs to be tested and the countries need to be added to the
			 * translation files.
			 */
			var i,country,option;
			for (i=0;i<countries.length;i=i+1) {
				option = false;
				country = countries[i];
				if (country.value) {
					option = $E('select option[value="'+country.value+'"]','CountryField');
					if (option) {
						option.setHTML(country.display);
					}
				}
			}
		}
	},
	registration2:{
		init:function() {
			var fillFcn = localize.registration2.fill;
			localize.fetch(fillFcn);
		},
		fill:function(res) {
			var i;
			var values = res;
			var body = $(document.body);
			localize.updatePageTitle(values.pages.registration2);
			localize.setupImg('phTitle_PresentationModeControlsContainer_PresentationImage',values.pages.registration2.signMeUp);
			localize.setupImg('phLeftImg_PresentationModeControlsContainer_PresentationImage',values.pages.registration2.deserveDrink);
			localize.updateElements($$('span#phLeftHTML p.boldCopy'),values.pages.registration2.introCopy);
			localize.setupImg($E('img','signatureDrinkImage'),values.pages.registration2.drinkImg);
			$('spacer').setProperties({height:values.pages.registration2.spacerHeight});
			localize.updateElements($$('div#signatureDrinkCopy p.boldCopy'),values.pages.registration2.recipeLines);
			$E('div.mainLeftColumn div.questions h2.headerText',body).setHTML(values.pages.registration2.whenYoureDrinking);
			$('WhatDrinkLabel').setHTML(values.pages.registration2.whatDoYouDrink);
			$E('div.mainLeftColumn div.questions h4.formLabel span.checkLabel',body).setHTML(values.pages.registration2.checkAllApply);
			$E('label span',$('WhatDrinkLabel1')).setHTML(values.pages.registration2.drinks.beer);
			$E('label span',$('WhatDrinkLabel2')).setHTML(values.pages.registration2.drinks.cognac);
			$E('label span',$('WhatDrinkLabel3')).setHTML(values.pages.registration2.drinks.liquor);
			$E('label span',$('WhatDrinkLabel4')).setHTML(values.pages.registration2.drinks.gin);
			$E('label span',$('WhatDrinkLabel5')).setHTML(values.pages.registration2.drinks.rum);
			$E('label span',$('WhatDrinkLabel6')).setHTML(values.pages.registration2.drinks.tequila);
			$E('label span',$('WhatDrinkLabel7')).setHTML(values.pages.registration2.drinks.whiskey);
			$E('label span',$('WhatDrinkLabel8')).setHTML(values.pages.registration2.drinks.vodka);
			$E('label span',$('WhatDrinkLabel9')).setHTML(values.pages.registration2.drinks.scotch);
			$E('label span',$('WhatDrinkLabel10')).setHTML(values.pages.registration2.drinks.wine);
			$E('label span',$('WhatDrinkLabel11')).setHTML(values.pages.registration2.drinks.none);
			localize.updateElement('HowManyLabel',values.pages.registration2.howManyDrinks);
			$('HowManySmirnoffLabel').setHTML(values.pages.registration2.howManySmirnoffDrinks);
			$('vdtZero').setHTML(values.pages.registration2.ranges.zero); $('sdtZero').setHTML(values.pages.registration2.ranges.zero);
			$('vdtOne').setHTML(values.pages.registration2.ranges.oneToThree); $('sdtOne').setHTML(values.pages.registration2.ranges.oneToThree);
			$('vdtFour').setHTML(values.pages.registration2.ranges.fourToSix); $('sdtFour').setHTML(values.pages.registration2.ranges.fourToSix);
			$('vdtSeven').setHTML(values.pages.registration2.ranges.sevenToNine); $('sdtSeven').setHTML(values.pages.registration2.ranges.sevenToNine);
			$('vdtTen').setHTML(values.pages.registration2.ranges.tenToFifteen); $('sdtTen').setHTML(values.pages.registration2.ranges.tenToFifteen);
			$('vdtSixteen').setHTML(values.pages.registration2.ranges.sixteenToThirtySix); $('sdtSixteen').setHTML(values.pages.registration2.ranges.sixteenToThirtySix);
			$('WhichSmirnoffLabel').setHTML(values.pages.registration2.whichSmirnoffProducts);
			$E('label span',$('WhichSmirnoffLabel1')).setHTML(values.pages.registration2.products.vodka);
			$E('label span',$('WhichSmirnoffLabel2')).setHTML(values.pages.registration2.products.ice);
			$E('label span',$('WhichSmirnoffLabel3')).setHTML(values.pages.registration2.products.flavoured);
			$E('label span',$('WhichSmirnoffLabel4')).setHTML(values.pages.registration2.products.other);
			$('RelationshipLabel').setHTML(values.pages.registration2.relationshipStatementSelect);
			$('option00LineOne').setHTML(values.pages.registration2.relationships.select);
			$('RelationshipLabel1').setHTML(values.pages.registration2.relationships.only);
			$('RelationshipLabel2').setHTML(values.pages.registration2.relationships.first);
			$('RelationshipLabel3').setHTML(values.pages.registration2.relationships.favorite);
			$('RelationshipLabel4').setHTML(values.pages.registration2.relationships.drinkable);
			$('RelationshipLabel5').setHTML(values.pages.registration2.relationships.sometimes);
			$('RelationshipLabel6').setHTML(values.pages.registration2.relationships.neverTried);
			$('RelationshipLabel7').setHTML(values.pages.registration2.relationships.unknowledgable);
			$('RelationshipLabel8').setHTML(values.pages.registration2.relationships.neverHeard);
			$('RelationshipLabel9').setHTML(values.pages.registration2.relationships.notInterested);
			$('RelationshipLabel10').setHTML(values.pages.registration2.relationships.notAgain);
			$E('p.legalText',$('legalCopy')).setHTML(values.pages.registration2.legalText);
			localize.setupImg('takemetothesite',values.pages.registration2.takeMeToSiteButton);
			localize.setupImg('finished',values.pages.registration2.finishedButton);
		}
	},
	registration3:{
		init:function() {
			var fillFcn = localize.registration3.fill;
			localize.jsAsset = '/NR/rdonlyres/572184C7-A8F4-439E-AEA6-97A9775379DE/0/reg3.js';
			localize.fetch(fillFcn);
		},
		fill:function(res) {
			var i, img;
			var values = res;
			localize.updatePageTitle(values.pages.registration3);
			localize.setupImg($E('img',$('signMeUp')),values.pages.registration3.signMeUp);
			localize.setupImg($E('img',$('deserveADrinkImage')),values.pages.registration3.deserveDrink);
			$E('div.mainLeftColumn p.boldCopy').setHTML(values.pages.registration3.introCopy[0]);
			localize.setupImg($E('img',$('signatureDrinkImage')),values.pages.registration3.drinkImg);
			localize.updateElements($ES('p.boldCopy','signatureDrinkCopy'),values.pages.registration3.recipeLines);
			localize.updateElements($ES('p.boldCopy','signatureDrinkInstruction'),values.pages.registration3.instructionLines);
			localize.setupImg('print',values.pages.registration3.printButton);
			localize.setupImg('takemetothesite',values.pages.registration3.takeMeToSiteButton);
			window.check_length = function(field,maxchars,remaining) {
				var message = document.getElementById(field);
				var len = message.value.length;
				if (len > maxchars) { message.value = message.value.substr(0, maxchars); len = maxchars; }
				var rem = maxchars - len;
				var remstr = rem + vals.pages.contact.charactersRemaining.substring(vals.pages.contact.charactersRemaining.indexOf(" "));
				document.getElementById(remaining).innerHTML = remstr;
			};
		}
	},
	registrationDuplicate:{
		init:function() {
			var fillFcn = localize.registrationDuplicate.fill;
			localize.fetch(fillFcn);
		},
		fill:function(res) {
			var i, img;
			var values = res;
			localize.updatePageTitle(values.pages.registrationDuplicate);
			localize.setupImg($E('img','signMeUp'),values.pages.registrationDuplicate.headerImg);
			localize.updateElements($ES('div.body p.copy'),values.pages.registrationDuplicate.copy);
			localize.setupImg('cancel',values.pages.registrationDuplicate.backButton);
		}
	},
	sentPassword:{
		init:function() {
			var fillFcn = localize.sentPassword.fill;
			localize.fetch(fillFcn);
		},
		fill:function(res) {
			var values = res;
			localize.updatePageTitle(values.pages.sentPassword);
			localize.setupImg($E('img','signMeUp'),values.pages.sentPassword.headerImg);
			$('instructionCopy').setHTML(values.pages.sentPassword.instructionCopy);
			var login = $('login');
			localize.setupImg(login,values.pages.sentPassword.loginButton);
			login.getParent().setProperties({title:values.pages.sentPassword.loginTitle});
		}
	},
	terms:{
		init:function() {
			var fillFcn = localize.terms.fill;
			localize.fetch(fillFcn);
		},
		fill:function(res) {
			var values = res;
			localize.updatePageTitle(values.pages.terms);
			localize.setupImg($E('img','termsAndConditions'),values.pages.terms.headerImg);
			if (values.pages.terms.body) { $E('div.body').setHTML(localize.constructBody(values.pages.terms.body)); }
		},
		getTermsUrl: function () {
			var lang = localize.getLanguage();
			var country = localize.getCountry();
			var enRow = '/en-row/terms.html';
			var fileName = enRow;
			/* This if and switch statement will have to be uncommented.
			 * Please note that the same popupOpen function is called on both the gateway and the home page
			 * so make sure the terms statement is in the same location on both the gateway and the
			 * .com site.
			 * The default path is defined in enRow and will be returned unless the specific country
			 * or country/language combination cases are defined in the switch statements below.
			 * The country codes and language codes used should follow the ISO standards.
			 * If a country has only one language change 'country code here' to the code for the country in question and change the fileName
			 * assignment just above the break statement related to the 'country code here' case to the location
			 * of the terms statement html file.
			 * If a country has multiple languages change 'country code with multiple languages here' to the
			 * code for the country in question. For the first language change the 'lang 1 here' to the ISO language
			 * code for the language and the fileName assignment directly before the break statement related to the 'lang 1 here'
			 * to the location of the terms statement html file.
			 */
			if (lang && country) {
				switch (country.toLowerCase()) {
					case 'de':
						fileName = '/de-de/terms.html';
						break;
						case 'at':
						fileName = '/de-at/terms.html';
						break;
					case 'br':
						fileName = '/pt-br/terms.htm';
						break;
					case 'bg':
						fileName = '/bg-bg/terms.htm';
						break;
					case 'rla':
						fileName = '/es-rla/termsofuse.htm';
						break;
					case 'ar':
						fileName = '/es-ar/termsofuse.htm';
						break;
					case 'cl':
						fileName = '/es-cl/termsofuse.htm';
						break;
					case 'cr':
						fileName = '/es-cr/termsofuse.htm';
						break;
					case 'do':
						fileName = '/es-do/termsofuse.htm';
						break;
					case 'mx':
						fileName = '/es-mx/termsofuse.htm';
						break;
					case 've':
						fileName = '/es-ve/termsofuse.htm';
						break;
					case 'co':
						fileName = '/es-co/termsofuse.htm';
						break;						
					case 'ie':
						fileName = '/en-ie/terms.htm';
						break;
					case 'it':
						fileName = '/it-it/terms.htm';
						break;
					case 'ch':
						switch (lang.toLowerCase()) {
							case 'de':
								fileName = '/de-ch/terms.html';
								break;
							case 'fr':
								fileName = '/fr-ch/terms.html';
								break;
						}
						break;
				}
			} else {
				fileName = enRow;
			}
			return fileName;
		}
	},
	unsubscribe:{
		init:function() {
			var fillFcn = localize.unsubscribe.fill;
			localize.fetch(fillFcn);
		},
		fill:function(res) {
			var values = res;
			localize.updatePageTitle(values.pages.unsubscribe);
			localize.setupImg('phTitle_PresentationModeControlsContainer_PresentationImage',values.pages.unsubscribe.headerImg);
			$('phMessage').setHTML(values.pages.unsubscribe.message);
			$('phEmailLbl').setHMTL(values.pages.unsubscribe.email);
			$('phLinkText').setHTML(values.pages.unsubscribe.linkText+' <a href="/en-row/Secure/Login/login2.htm?RhCountry=GB&RhYear=1982&RhMonth=1&RhDay=1&RhLanguage=en" title="'+values.pages.unsubscribe.loginMessage+'">'+values.pages.unsubscribe.loginMessage+'</a>.');
			localize.setupImg('btnCancel',values.pages.unsubscribe.cancelButton);
			localize.setupImg('btnUnsubscribe',values.pages.unsubscribe.unsubscribeButton);
			$('hdnInvalidText').setProperties({value:'<p class=\'error\'>'+values.pages.unsubscribe.errorMsg+'</p>'});
		}
	}
};

localize.cm = {
	tag: function (attribute) {
		
	}
};