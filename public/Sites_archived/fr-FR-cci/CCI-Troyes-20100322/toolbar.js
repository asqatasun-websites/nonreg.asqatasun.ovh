/***********************************************/
/* Affiche/masque alternativement des �l�ments */
/***********************************************/
function toolbar_displayElement(id,num,total)
{
    for (i=1; i<=total; i++)
    {
        if (i == num) {document.getElementById(id+i).style.display = "";}
        else {document.getElementById(id+i).style.display = "none";}
    }
}

/*******************************/
/* Afficher/Masquer un �lement */
/*******************************/
function toolbar_showHide(id)
{
 	id = document.getElementById(id);
	if (id.style.display == 'none') {id.style.display = 'block';}
	else {id.style.display = 'none';}
}

/*************************/
/* Param�tres par d�faut */
/*************************/
function toolbar_params_default(type,name)
{
 	// Initialisation
 	var params = new Array('tailleTexte');
 	params['tailleTexte'] = new Array();
 	
 	// Param�tres modifiables
 	params['tailleTexte']['evolution'] = 1;
 	params['tailleTexte']['nb_evolution'] = 5;
 	params['tailleTexte']['balises_tag'] = new Array('*','h1','h2','h3','td','input','textarea','legend');
 	params['tailleTexte']['balises_default'] = new Array(11,14,11,11,11,11,11,11);
 	params['tailleTexte']['unite'] = 'px';
 	
 	return params[type][name];
}

/**************************************/
/* Modification de la taille du texte */
/**************************************/
function toolbar_tailleTexte(id,type)
{
 	function toolbar_tailleTexte_ajuster(taille,evolution,min,max,type)
	{
	 	// Param�tres
	 	var reg = new RegExp('([^0-9])','g');
	 	// Initialisation
	 	var evolution = eval(evolution);
	 	var min = eval(min);
	 	var max = eval(max);
	 	// Initialisation de la taille
		if (!taille) {taille = min;}
		else {taille = taille.replace(reg,'');}
		taille = eval(taille);
		
		// Nouvelle taille
		if (type == 'up') {taille = taille + evolution;}
		else if (type == 'down') {taille = taille - evolution;}
		
		// Ajustement de la nouvelle taille
		if (taille > max) {taille = max;}
		else if (taille < min) {taille = min;}
		
		return taille;
	}
	
 	// Initialisation
 	var params = new Array();
 	// Liste des param�tres � importer Importation des param�tres
 	var listparams = new Array('evolution','nb_evolution','balises_tag','balises_default','unite');
	// V�rification de l'existence de param�tres personnalis�s
 	if (typeof(toolbar_params) == 'function')
 	{
 	 	// Importation des param�tres
 	 	for (var i=0; i<listparams.length; i++)
		{
 	 		if (toolbar_params('tailleTexte',listparams[i]) != undefined) {params[listparams[i]] = toolbar_params('tailleTexte',listparams[i]);}
 	 		else {params[listparams[i]] = toolbar_params_default('tailleTexte',listparams[i]);}
 	 	}
 	}
 	else
 	{
 	 	// Importation des param�tres
 	 	for (var i=0; i<listparams.length; i++)
		{
 	 		params[listparams[i]] = toolbar_params_default('tailleTexte',listparams[i]);
 	 	}
 	}
	
	// Initialisation
 	var max_evolution = params['evolution'] * params['nb_evolution'];
	
	// Ciblage du texte
	var texte = document.getElementById(id);
	
	// Modification des balises
	for (var i1=0; i1<params['balises_tag'].length; i1++)
	{
	 	// Initialisation
	 	var max = eval(params['balises_default'][i1] + max_evolution);
	 	// Toutes les balises
	 	if (params['balises_tag'][i1]=='*')
		{
		 	var taille = toolbar_tailleTexte_ajuster(texte.style.fontSize,params['evolution'],params['balises_default'][i1],max,type);
		 	texte.style.fontSize = taille + params['unite'];
		}
		// Balise sp�cifique
		else
		{
			for (var i2=0; i2<texte.getElementsByTagName(params['balises_tag'][i1]).length; i2++)
			{
			 	var taille = toolbar_tailleTexte_ajuster(texte.getElementsByTagName(params['balises_tag'][i1])[i2].style.fontSize,params['evolution'],params['balises_default'][i1],max,type);
				texte.getElementsByTagName(params['balises_tag'][i1])[i2].style.fontSize = taille + params['unite'];
		 	}
		}
	}
}