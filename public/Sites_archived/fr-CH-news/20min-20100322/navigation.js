




$(document).ready(function(){
	/* override css-stuff to keep sliding ads on the right z-index */
	if (!$.browser.msie) {
		$("#content_header").css( 'z-index', '200' );
		$("#header").css( 'z-index', '300' );
		$("#content").css( 'z-index', '10' );
		$(".ads").css( 'z-index', '1' );
	}

	/* flap management */
	var flap_content = $('#mehr_flap').html();
	$('#mehr_flap').remove();
	var offset_button = $('#mehr_button').offset();
	var offset_wrapper = $('#wrapper').offset();
	offset_button.left = offset_button.left - offset_wrapper.left - 2;
	offset_button.top = offset_button.top - offset_wrapper.top + $('#mehr_button').height() -31;
	$('#wrapper').append("<ul class='navi_flap' id='mehr_flap'>"+flap_content+"</ul>");
	$("#mehr_flap").css( 'left', offset_button.left+'px' );
	$("#mehr_flap").css( 'top', offset_button.top+'px' );
	$("#mehr_button a").fadeIn("slow");
	
	$("#mehr_flap").mouseover(function () {
		$("#mehr_flap").css('display', 'block');
		$("#mehr_button>a").addClass("icon_mehr_active");
	});
	$("#mehr_flap").mouseleave(function () {
		$("#mehr_flap").css('display', 'none');
		$("#mehr_button>a").removeClass("icon_mehr_active");
	});
	$("#mehr_button").mouseover(function () {
		$("#mehr_flap").css('display', 'block');
		$("#mehr_button>a").addClass("icon_mehr_active");
	});
	$("#mehr_button").mouseleave(function () {
		$("#mehr_flap").css('display', 'none');
		$("#mehr_button>a").removeClass("icon_mehr_active");
	});
});





function popupRadio(url) {
 fenster = window.open(url, "20MinRadio", "location=no,menubar=no,resizable=no,status=no,toolbar=no,width=547,height=472,left=0,top=0");
 fenster.focus();
}

