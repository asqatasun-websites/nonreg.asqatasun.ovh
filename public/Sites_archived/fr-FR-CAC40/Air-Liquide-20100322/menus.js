/*www.equallia.com - 2007*/


var queue = false;
var dernier = false;
var repere = false;
function charger(){
	if (document.getElementById && document.getElementsByTagName){
		document.getElementById('menus').style.height = '5.7em';
		var cesMenus = document.getElementById('lesMenus').getElementsByTagName('ul');
		for (i=0;i<cesMenus.length;i++){
	//(position du dernier menu)
			if (i == cesMenus.length - 1) {
				cesMenus[i].className = 'nav_on';
				var position1 = cesMenus[i].offsetWidth;
			}
//1) Cacher tous les menus
			cesMenus[i].className = 'nav_out';
	//(position du dernier menu)
			if (i == cesMenus.length - 1){
				var position2 = cesMenus[i].parentNode.offsetWidth;
				if (position1 > position2) cesMenus[i].style.marginLeft = position2 - position1 + 'px';
				if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) cesMenus[i].style.width = position1 + 'px';
			}
//2) evenements pour la navigation a la souris (correlation a la navigation au clavier)
			cesMenus[i].parentNode.onmouseover = function fonction(event){ //SOURIS SURVOLLE
				if (dernier && dernier != this.getElementsByTagName('ul')[0]){ //(quand fait suite a la navigation au clavier)
					dernier.className = 'nav_out';
					dernier = false;
					document.getElementById('menus').getElementsByTagName('a')[0].focus(); 
					/* ATTENTION a l idee de mettre sytematiquement le focus sur l'en tete du menu survole :
						respecter une navigation clavier qui peut, ailleurs sur la page, ne pas concerner le menu */
				}
				queue = this.getElementsByTagName('ul')[0];
				queue.className = 'nav_on';
				if (navigator.userAgent.toLowerCase().indexOf('safari') > -1) queue.style.top = '21px';
	//double bogue IE : 1) boite de selection par dessus les menus 2) et idem avec un calque de pseudo boite de selection en position absolue et z-index inferieur mais...
				if (navigator.userAgent.toLowerCase().indexOf('msie') > -1 && repereBis) document.getElementById('pselekt' + repereBis).className = etat[0];
			}
			cesMenus[i].parentNode.onmouseout = function fonction(event){ //SOURIS FINIT DE SURVOLER
				this.getElementsByTagName('ul')[0].className = 'nav_out';
				queue = false;
			}
			/* ne pas prevoir pour IE que le survol des 'li' des menus change la couleur de fond comme sur Firefox (qui prend correctement le display du 'a') :
				car l'element 'li' n'en serait pas plus cliquable mais ca en donnerait le signal inverse */
	//pb sur Safari (aucune couleur de fond au survol) et sur IE 6 (extension de la couleur de fond sur toute la surface du 'li')
			if (navigator.userAgent.toLowerCase().indexOf('safari') > -1 || navigator.userAgent.toLowerCase().indexOf('msie 6') > -1){
				for (j=1;j<cesMenus[i].parentNode.getElementsByTagName('a').length;j++){
					cesMenus[i].parentNode.getElementsByTagName('a')[j].onmouseover = function fonction(event){ 
						this.parentNode.className = (this.parentNode.className.length > 0 && this.parentNode.className.indexOf('first') > -1) ? 'first survol' : 'survol';
				}	}
				for (j=1;j<cesMenus[i].parentNode.getElementsByTagName('a').length;j++){
					cesMenus[i].parentNode.getElementsByTagName('a')[j].onmouseout = function fonction(event){ 
						this.parentNode.className = (this.parentNode.className.length > 0 && this.parentNode.className.indexOf('first') > -1) ? 'first hors' : 'hors';
			}	}	}
//3) evenements pour la navigation au clavier (correlation a la navigation a la souris)
			cesMenus[i].parentNode.getElementsByTagName('a')[0].onfocus = function fonction(event){ //CLAVIER POSE SUR LIEN D EN-TETE DE MENU
				if (queue && queue != this.parentNode.getElementsByTagName('ul')[0]){ //(quand fait suite a navigation a la souris)
					queue.className = 'nav_out';
					queue = false;
				}
				if (dernier && dernier != this.parentNode.getElementsByTagName('ul')[0]){ //(quand fait suite a tabulation arriere depuis une autre en-tete)
					repere = false;
					dernier.className = 'nav_out';
				}
				if (dernier && dernier == this.parentNode.getElementsByTagName('ul')[0]) repere = false; //(quand fait suite a tabulation arriere depuis menu - stopper la minuterie de fermeture)
				dernier = this.parentNode.getElementsByTagName('ul')[0];
				dernier.className = 'nav_on';
				if (navigator.userAgent.toLowerCase().indexOf('safari') > -1) dernier.style.top = '21px';
	//double bogue IE : 1) boite de selection par dessus les menus 2) et idem avec un calque de pseudo boite de selection en position absolue et z-index inferieur mais...
				if (navigator.userAgent.toLowerCase().indexOf('msie') > -1 && repereBis) document.getElementById('pselekt' + repereBis).className = etat[0];
			}
			cesMenus[i].parentNode.getElementsByTagName('a')[0].onblur = function fonction(event){ //CLAVIER QUITTE LIEN D EN-TETE DE MENU
				repere = true;
				setTimeout('cacher()',100);
			}
			for (j=1;j<cesMenus[i].parentNode.getElementsByTagName('a').length;j++){
				cesMenus[i].parentNode.getElementsByTagName('a')[j].onfocus = function fonction(event){ //CLAVIER POSE SUR LIEN DE MENU
					this.parentNode.className = (this.parentNode.className.length > 0 && this.parentNode.className.indexOf('first') > -1) ? 'first survol' : 'survol';
					/* la souris et le clavier peuvent modifier la couleur de fond de deux items distincts d'un menu : 
	=> a conserver ? */
					repere = false;
			}	}
			for (j=1;j<cesMenus[i].parentNode.getElementsByTagName('a').length;j++){
				cesMenus[i].parentNode.getElementsByTagName('a')[j].onblur = function fonction(event){ //CLAVIER QUITTE LIEN DE MENU
					this.parentNode.className = (this.parentNode.className.length > 0 && this.parentNode.className.indexOf('first') > -1) ? 'first hors' : 'hors';
					repere = true;
					setTimeout('cacher()',100);
}	}	}	}	}

function cacher(){
	if (repere) dernier.className = 'nav_out';
}

//pb sur Mac (FF car Safari ne semble par permettre la generation dynamique de selecteurs dans les .css)
if (navigator.userAgent.toLowerCase().indexOf('macintosh') > -1){
	try{
		var stylesPourmac = document.styleSheets[0];
		stylesPourmac.insertRule(".nav_top ul { top: 19px; } ", stylesPourmac.cssRules.length);
	}
	catch(err){}
}



//pseudo boite de selection
var etat = new Array('select_option','select_option_vu');
var repere = false;
var repereBis = false;
function selectionner(a){
	repere = true;
	if (repereBis && repereBis != a) document.getElementById('pselekt' + repereBis).className = etat[0];
	document.getElementById('pselekt' + a).className = document.getElementById('pselekt' + a).className == etat[0] ? etat[1] : etat[0];
	repereBis = a;
	setTimeout('repere = false',350);
	return false;
}



var rechercheAvancee = false;
//definir les evenements au chargement
function toutCharger(){
	charger();
	if (document.getElementById('pselekt1')){
		document.body.onclick = function fonction(event){
			if (! repere && repereBis) document.getElementById('pselekt' + repereBis).className = etat[0];
		}	
	}
	try{
		if (rechercheAvancee) predetailler();
	}
	catch(e){}
}
window.onload = toutCharger;



