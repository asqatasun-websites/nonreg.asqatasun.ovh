(function ($){
	$.la = $.la || {};

	$.extend(true, $.la,
	{
		tracking:{
	    	cybermonitor:{
	    		ids:{
	    			CM_SERIAL:'274074197502',
	    			CM_CLIENT:'canalj'
	    		}
	    	},
	    	webo:{
	    		ids:{
	    			WRP_ID:364317
	    		}
	    	},
	    	ga:{
	    		_ua:'UA-10526233-1',
	    		_domain:'.canalj.fr'
	    	}
	    },
	    
	    promo:{
	    	_adDefaultDefer:false,
            _adDefaultZoneDefer:{
	    		'BAS': 'false',
	    		'Inter': 'false',
	    		'Banner': 'ready',
	    		'Pave': 'ready',
	    		'Vignette1': 'ready',
	    		'Vignette2': 'ready',
	    		'Vignette3': 'ready',
	    		'Vignette4': 'ready'
	    	},
            _adDefaultForceDefer:null,
            _adDefaultForceZoneDefer:{},
            
            _testA2d:{
            	'testHome':{'BAS':8754}
            }
	    }
	    
	});

	
})(jQuery);

var sHost = jQuery.la.sHost;
var tb_pathToImage = jQuery.la.tb_pathToImage;