var xhr = null ;

function go(c){
	if (!c.data.replace(/\s/g,''))
		c.parentNode.removeChild(c) ;
}
	
function clean(d){
	var bal = d.getElementsByTagName('*') ;

	for(i= 0 ; i < bal.length ; i++) {
		a = bal[i].previousSibling ; 	// noeud gauche
		if(a && a.nodeType == 3)		// 3 = TEXTNODE
			go(a) ;
		b = bal[i].nextSibling ;		// noeud droit		
		if (b && b.nodeType == 3)		// 3 = TEXTNODE
			go(b) ;
	}
	return d ;
}