function LA_Stop()
{
	Accueil_Fin();
}

function LA_OnDialogID(event)
{
	if(event=="Produits")
	{
		Produits();		
	}
	
	if(event=="Demarches")
	{
		Demarches();		
	}
	
	if(event=="EnLigne")
	{
		EnLigne();		
	}
	
	if(event=="Moments")
	{
		Moments();		
	}
	
	if(event=="Comparateurs")
	{
		LA_SetCookie("Cathy_CA_Comparateurs","yes",true);
		window.document.location.href="/spip.php?rubrique215";		
	}
	
	if(event=="Tarifs")
	{
		Tarifs();		
	}	
	
	if(event=="Econso")
	{		
		LA_SetCookie("Cathy_CA_E-conso","yes",true);
		window.document.location.href="/spip.php?rubrique220";
	}
	
	if(event=="Eimmo")
	{		
		LA_SetCookie("Cathy_CA_E-immo","yes",true);
		window.document.location.href="/spip.php?rubrique219";
	}
	
	if(event=="Securite")
	{
		Securite();		
	}
	
	if(event=="OuvrirCompte")
	{
		OuvrirCompte();		
	}
	
	if(event=="Contacter")
	{
		window.document.location.href="/spip.php?article1239";
	}
	
	if(event=="Perte_Vol")
	{
		LA_SetCookie("Cathy_CA_Perte-Vol","yes",true);
		window.document.location.href="/spip.php?rubrique101";
	}
	
	if(event=="Sinistres_Accueil")
	{
		LA_SetCookie("Cathy_CA_Sinistres-Accueil","yes",true);
		window.document.location.href="/spip.php?rubrique101";	
	}
	
	if(event=="Trouver_Agence")
	{
		window.document.location.href="/spip.php?page=contact_annuaire_agences";	
	}
	
	if(event=="Pieces")
	{		
		LA_SetCookie("Cathy_CA_Pieces","yes",true);
		window.document.location.href="/spip.php?rubrique101";		
	}
	
}

function LA_OnBookmark(abook)
{
	if (abook == "Accueil_02"){
		Accueil_02();
	}
	
	if (abook == "CarteFrance"){
		window.document.location.href="/spip.php?page=contact_carte";
	}
	
	if (abook == "Securite"){
		window.document.location.href="/spip.php?article751";
	}
}


function Main_LivingActor(userdefine)
{
	if(userdefine=="1"){
		
			Accueil_01();
			
	}
	if(userdefine=="2"){
		Accueil_02();
		
	}
}

function LA_OnReady()
{				
	if (LA_GetCookie("Cathy_CA_Home")==null){
		Init_LivingActor(LA_RELATIVE_PATH+LA_ACTORFILE_NAME+".swf",1);
		LA_SetCookie("Cathy_CA_Home","yes",30,true);
	}
}
LA_AllObservableAdded = true;