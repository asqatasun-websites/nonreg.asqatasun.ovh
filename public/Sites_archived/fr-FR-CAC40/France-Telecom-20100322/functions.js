function popup(url, width, height) {
  var params = "";
  if (width != 0 && height != 0) {
    params = "width=" + width + ",height=" + height + ",resizable=yes,scrollbars=yes";
  } else {
    params = "";
  }
  window.open(url, 'popup', params);
}

function popInAjax(url, w, h) {
	var popInAjaxHeight = h ? h : 500;
	var popInAjaxWidth = w ? w : 500;
	pop.openPop({content:url, evaluation:false, height:popInAjaxHeight, width:popInAjaxWidth, mode: 'remote'});
	return false;
}

function popInAjaxParams(_mode, _url, _width, _height){
	switch (_mode) {
	    case 'popin': //Mode ajax
			pop.openPop({content:_url, evaluation:true, height:_height, width:_width, mode:'remote'});
		break;
	    case 'popup': //Mode popup
			window.open(_url, '' , 'width=' + _width + ',height=' + _height + ',left=100,top=100')
		break;
		default : // Mode normal
			window.location.href = _url;
		break;
	}
}

function onWordsFocus(obj, text) {
    if (obj.value == text) {
        obj.value = '';
    }
    obj.focus = true;
}
//add fonction bubble
function change(dest) {
    //window.opener.location=dest;
	if( window.opener ) {
		window.close();
		window.opener.location = dest;
	} else {
		pop.closePop();
		window.location = dest;
	}
}
function linkCG(url) {
	window.open(url, 'popup',"width=200, height=200, scrollbars=yes");
}
function centrerConteneurInterne() {
	var window_w = document.documentElement.clientWidth;
	var conteneur_w = $('conteneur_centrer').getStyle('width').toInt();
	
	if(window_w > conteneur_w) {
		
		var m_left = (window_w-conteneur_w) / 2;
		$('conteneur_centrer').setStyle('margin-left', m_left); 
	}
	else {
		$('conteneur_centrer').setStyle('margin-left', 0); 
	}
}

/**
 * Transfert de m�thodes utilis�es par les classes des outils et autrefois pr�sentes dans common_ajax
 */

function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}

function getDateString_fr(unedate,type)
{
		var letype = type;
	var time = String(unedate.getTime()+(unedate.getTimezoneOffset()*60)+7200);
	
	if(time.length < 13) {
		time += '000';
	}
	unedate.setTime(time);
	var te = new String(unedate);
	var tab=te.split(' ');
	if(tab[3].length != 4 ) {
		tab[3] = tab[5] ;
	}
	var sMois;
	switch(letype)
	{
		case 0 :
		if (sLangSite == "fr") {
			switch (tab[1]) {
				case 'Jan' :
					sMois = 'Jan';
					break;
				case 'Feb' :
					sMois = 'F&eacute;v';
					break;
				case 'Mar' :
					sMois = 'Mars';
					break;
				case 'Apr' :
					sMois = 'Avr';
					break;
				case 'May' :
					sMois = 'Mai';
					break;
				case 'Jun' :
					sMois = 'Juin';
					break;
				case 'Jul' :
					sMois = 'Juil';
					break;
				case 'Aug' :
					sMois = 'Ao&ucirc;t';
					break;
				case 'Sep' :
					sMois = 'Sep';
					break;
				case 'Oct' :
					sMois = 'Oct';
					break;
				case 'Nov' :
					sMois = 'Nov';
					break;
				case 'Dec' :
					sMois = 'Dec';
					break;
			}
			var chaine = tab[2] +"&nbsp;" +sMois +"&nbsp;" +tab[3];
		}
		else {
		
			var chaine = tab[2] +"&nbsp;" +tab[1] +"&nbsp;" +tab[3];
		}
	
		break;
	}
	return chaine;
}

function getDateString(unedate,type)
{
	var letype = type;
	var time = String(unedate.getTime()+(unedate.getTimezoneOffset()*60)+7200);
	
	if(time.length < 13) {
		time += '000';
	}
	unedate.setTime(time);
	
	//var te2=unedate.toGMTString(); //fixe la date pour tt le monde
	var te = new String(unedate);
	
	//alert("unedate.toGMTString() = "+te);
	var tab=te.split(' ');
	// 0 day de la semaine
	// 1 mois
	// 2 jour
	// 3 annee
	
	// lann?e chez IE est la tab[5] donc on inverse pour que sa soit comme sur FF
	if(tab[3].length != 4 ) {
		tab[3] = tab[5] ;
	}
	
	var sMois;
	switch(letype)
	{
		case 0 :
		if (sLangSite == "fr") {
			switch (tab[1]) {
				case 'Jan' :
					sMois = 'Jan';
					break;
				case 'Feb' :
					sMois = 'Feb';
					break;
				case 'Mar' :
					sMois = 'Mar';
					break;
				case 'Apr' :
					sMois = 'Apr';
					break;
				case 'May' :
					sMois = 'May';
					break;
				case 'Jun' :
					sMois = 'Jun';
					break;
				case 'Jul' :
					sMois = 'Jul';
					break;
				case 'Aug' :
					sMois = 'Aug';
					break;
				case 'Sep' :
					sMois = 'Sep';
					break;
				case 'Oct' :
					sMois = 'Oct';
					break;
				case 'Nov' :
					sMois = 'Nov';
					break;
				case 'Dec' :
					sMois = 'Dec';
					break;
			}
			var chaine = tab[2] +"&nbsp;" +sMois +"&nbsp;" +tab[3];
		}
		else {
			var chaine = tab[2] +"&nbsp;" +tab[1] +"&nbsp;" +tab[3];
		}

		break;
		
		case 1:
		var chaine = tab[1]; // mois
		break;
		
		case 2 : 
		var chaine = tab[3]; // ann??e
		break;
		
		case 3 : 
		var chaine = tab[2]; // jour
		break;
		case 4 :
		var chaine = tab[2]+"-"+tab[1]+"-"+tab[3];
		break;
	
		case 5 : //JOUR, mois et ann?e en francais ou anglais
		if (sLangSite == "fr") {
			sMois = tab_mois_fr[tab[1]];	
		} else {
			sMois = tab_mois_en[tab[1]];	
		}
		
		var chaine = tab[2] +"&nbsp;" +sMois +"&nbsp;" +tab[3];	

		break;
		
		case 6 : // MOIS ET ANNEE EN FRANCAIS OU ANGLAIS
		
		if (sLangSite == "fr") {
			sMois = tab_mois_fr[tab[1]];
		} else {
			sMois = tab_mois_en[tab[1]];
		}
		
		var chaine = sMois +"&nbsp;" +tab[3];

		break;

	}

	return chaine;
}


function obtenirMois(num)
{
	var chaine ="";
		switch(num)
	{
		case 1 :
		var chaine ="Jan" 
		break;
		
		case 2:
		var chaine ="Feb" 
		break;
		
		case 3 : 
		var chaine = "Mar"
		break;
		
		case 4 :
		var chaine = "Apr"
		break;
		
		case 5 :
		var chaine ="May" 
		break;
		
		case 6 :
		var chaine = "Jun"
		break;
		
		case 7 :
		var chaine = "Jul"
		break;
		
		case 8 :
		var chaine = "Aug"
		break;
		
		case 9 :
		var chaine = "Sep"
		break;
		
		case 10 :
		var chaine = "Oct"
		break;
		
		case 11 :
		var chaine = "Nov"
		break;
		
		case 12 :
		var chaine = "Dec"
		break;
		
		
	}
	return chaine;
}


function Obenir_mois_fr(mois)
{
	var chaine = '';
	if(sLangSite == 'fr') {
		var tabmois = $A(['','janvier','f&eacute;vrier','mars','avril','mai','juin','juillet','ao&ucirc;t','septembre','octobre','novembre','d&eacute;cembre']);
	}else{
		var tabmois = $A(['','January','February','March','April','May','June','July','August','September','October','November','December']);
	}
	tabmois.each(function(item,index){
						  if(index == mois)
						  {
							chaine = new String(item);
						  }
					  })	
	return chaine;
}

function obtenirMois_int(mois)
{
	var chaine = '';
	var tabmois = $A(['','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']);
	tabmois.each(function(item,index){
						  if(item == mois)
						  {
							chaine = new String(index);
						  }
					  })
	if(chaine.length == '1'){ chaine = 0+chaine}
	return chaine;
}

//cr?ation du cookie de sauvegarde de donn?e
// initialisation ? id@x@y@nb@
// idprofil@posX@posY@nbParam
// si nbParam > 0 => on rajoute les parametre a la suite
function savaData(){
	outils_id.each(function(item,index){
		var outil = getEndString('/', tools[item]);
		if(!Cookie.get(outil)){
			Cookie.set(outil,'0@x@y@nb@0@', {duration:365,path:"/"});
		}
	});
}

// r?cup?ration des donn?es du cookie
// prend en param un cookie et un chiffre 
// 0 = iprofil 
// 1 = posX 
// 2  = posY
// 3 = nbParam
// 4 = les parametre sous forme de tableau

//alert(getValueCookie('b_press.html',4))
function getValueCookie(myCookie,mode){
	//console.log(myCookie, mode)
	var trace_cookies = "";
	if(Cookie.get(myCookie))
	{
		trace_cookies+="get value cookie "+myCookie+" (mode "+mode+") : "+Cookie.get(myCookie)+"\n";
		
		var cookie_data = Cookie.get(myCookie);
		var tab = cookie_data.split("@");
		var chaine ='';
		var tab_chaine = $A([]);
		switch(mode)
		{
			case 0:
				chaine = tab[0];
				return chaine;
			break;
			case 1:
				chaine = tab[1];
				return chaine;
			break;
			case 2:
				chaine = tab[2];
				return chaine;
			break;
			case 3:
				chaine = tab[3];
				return chaine;
			break;
			case 4:
			
				trace_cookies+="valeur cookie :\n";
			
				if(tab[2] > 0)
				{
				
					tab.each(function(item,index){
						if(index > 3 && item!=""){tab_chaine.include(item);trace_cookies+=item+", ";}
					});
					
				
					return tab_chaine;
				}
					else
					{
						//alert('error : Ce Cookie ne possede pas de parametre');
					}
			break;
		} 
		
	}
		else
		{
			//alert("Error : Cookie n'existe pas")
		}
}

// return le cookie sous forme de tableau
function getTab_cookie(mycookie){
	var tab = mycookie.split('@');
	return tab;
}

function setZIndexCookie(aBoxes, aIndex){
	var s = aIndex.join('&');
	var s2 = '';
	aBoxes.each(function(oBox){
		s2 += oBox.id + '@';
	// F6 DEBUG 	s3 += oBox.layerMode + '*';
	})
	var s = s + '#' + s2
	Cookie.remove('boxesAndIndexes');
	Cookie.set('boxesAndIndexes',s, {duration:365,path:"/"});	
}
function getBoxesAndIndexes(){
	if (Cookie.get('boxesAndIndexes')) return Cookie.get('boxesAndIndexes');	
}


//modification de la posX 
//prend en parametre le nom du cookie et la nouvelle position
function setposXCookie(myCookie,newPosX)
{
	/*mini trick*/ if(newPosX ==0 ) newPosX = 1;
	if(Cookie.get(myCookie))
	{
		
		var tab_cookie = getTab_cookie(Cookie.get(myCookie)); // transformation en tableau
		tab_cookie[1] = newPosX;
		var chaine ='';
		tab_cookie.each(function(item2,index2){
								 if(item2 !='')
								 {
									chaine += item2+'@';
								 }
						});
		Cookie.remove(myCookie);
		//console.log(chaine)
		Cookie.set(myCookie,chaine, {duration:365,path:"/"});
	}
		else
		{
			//alert("error : le cookie n'existe pas")
		}
}

//setposYCookie('b_press.html',500)
//modification de la posY
//prend en parametre le nom du cookie et la nouvelle position
function setposYCookie(myCookie,newPosY){
	/*mini trick*/ if(newPosY ==0 ) newPosY = 1;
	if(Cookie.get(myCookie))
	{
		var tab_cookie = getTab_cookie(Cookie.get(myCookie)); // transformation en tableau
		tab_cookie[2] = newPosY;
		var chaine ='';
		tab_cookie.each(function(item2,index2){
								 if(item2 !='')
								 {
									chaine += item2+'@';
								 }
						});
		Cookie.remove(myCookie);
		Cookie.set(myCookie,chaine, {duration:365,path:"/"});
	}
	else
	{
		//alert("error : le cookie n'existe pas")
	}
}

function initParamCookie(myCookie)
{
	myCookie = getEndString('/', myCookie);
	if(Cookie.get(myCookie)){
		var idprofil = (myCookie,0);
		var posX = getValueCookie(myCookie,1);
		var posY = getValueCookie(myCookie,2);
		var mode = getValueCookie(myCookie,4);
		var chaine = idprofil+'@'+posX+'@'+posY+'@nb@'+mode+'@';
		Cookie.remove(myCookie);
		Cookie.set(myCookie,chaine, {duration:365,path:"/"});
	}
}
var resetCookie = function(){
	$('items').style.visibility = 'hidden';
	return false;
	
	/*
	Cookie.set('boxesAndIndexes','', {duration:365,path:"/"});
	for (var i=0; i < tab_item_sur_scene.length; i++) {
		var myCookie = b_outils[outils_id.indexOf(tab_item_sur_scene[i].id)];
		setposYCookie(myCookie, 0);
		setposXCookie(myCookie, 0);
	};
	*/
}
var getBoxesCoord = function(){
	var string = '';
	tab_item_sur_scene.each(function(box){
		name = box.id;
		posx = box.getLeft() - $('drawall').getLeft();
		posy = box.getTop() - $('drawall').getTop();
		string += name + ' --> left : ' + posx.toString() + ' ,top : ' +posy.toString() + '\r\n' 
	})
	if (pop){
		pop.openPop({content:string, height:250, width:300});
	}else{
		alert(string)
	}
	
}


// fonction pour modifier le nombre de parametre et ajout? les parametres dans le cookie
// utiliser lors d'un edit d'un outil 
//tab_test = ['theme1'];
//tab_test1 = ['theme1','theme2','theme3'];
//setParamCookie('b_press.html',tab_test)
function setParamCookie(myCookie,tab_param)
{
	
	if(Cookie.get(myCookie))
	{
		trace_cookies = "get cookie "+myCookie+" : "+Cookie.get(myCookie)+"\n";
		
		var tab_cookie = getTab_cookie(Cookie.get(myCookie));
		var nbparam = tab_param.length;
		tab_cookie[3] = nbparam;
		var tab_copy =$A([]);
		tab_cookie.each(function(item,index){
			//r�cup�ration des quatre premiers items uniquement (num�ro, x, y, nombre de param�tres)
			//pour leur ajouter les nouveaux param�tres � la suite
			if (index <= 4) {
				tab_copy.include(item);
			}
		 });
		tab_cookie = tab_copy;
		var chaine='';
		
		trace_cookies+="tab_cookie new = "+tab_param.join(', ')+"\n";
		
		tab_param.each(function(item,index){
			 tab_cookie.include(item)
		});
		
		
		
		tab_cookie.each(function(item2,index2){
			 if(item2 !='')
			 {
				chaine += item2+'@';
			 }
		});
		Cookie.remove(myCookie);
		
		trace_cookies += "set cookie "+myCookie+" : "+chaine+"\n";
		//console.log(chaine, 'here')
		Cookie.set(myCookie,chaine, {duration:365,path:"/"});
		
		//alert(trace_cookies);
		
	}
		else
		{
			alert("error : le cookie n'existe pas")
		}
}

var getEndString = function(separator, string) {
	return string.split(separator)[string.split(separator).length - 1]
}

/*  ifrlayer : 
	Cette fonction corrige un probleme sous IE6 lorsqu'un layer passe par dessus un select, le select sera toujours au dessus. Pour corriger ce probl&egrave;me.
	ex : 	
		- afficher un bloc : 
			myBlock.style.display='block';
			ifrlayer.make(myBlock); //genere ou affiche l'iframe
		- cacher un bloc :
			myBlock.style.display='none';
			ifrlayer.hide(myBlock); //cache l'iframe associee au bloc
		-deplacer un bloc : 
			myBlock.style.left = "100px";
			ifrlayer.move(myBlock); // deplace l'iframe associee au bloc
*/
var ifrlayer = {
	ie : document.all && window.print && document.getElementById && /MSIE [56]/.test(navigator.userAgent),

	$:function(obj) {
		return (typeof(obj)=="string" ? document.getElementById(obj) : obj)
	},

	make:function(obj) {
		if(!ifrlayer.$(obj)) return;
		obj = ifrlayer.$(obj);

		if(ifrlayer.ie && !obj.iframelayer) {
			var ifr;

			if(obj.parentNode && !obj.iframelayer)
				ifr = obj.parentNode.insertBefore(document.createElement("iframe"), obj);

			if(obj.currentStyle.zIndex != "" && parseInt(obj.currentStyle.zIndex)>1 ) {
				ifr.style.zIndex = parseInt(obj.currentStyle.zIndex) - 1;
			}

			ifr.src = "javascript:false;";

			with(ifr.style) {
				filter = "mask()";
				position = "absolute";
			};

			obj.iframelayer = ifr;
		}

		if (obj.iframelayer) {
			obj.iframelayer.style.visibility = "visible";
			ifrlayer.move(obj,true);
		};
	},

	hide:function(obj) {
		obj = ifrlayer.$(obj);

		if(obj && obj.iframelayer)
			obj.iframelayer.style.visibility="hidden";
	},

	kill:function(obj) {
		obj = ifrlayer.$(obj);
		if(obj && obj.iframelayer) {
		obj.iframelayer.parentNode.removeChild(obj.iframelayer);
		};
	},

	move:function(obj, size) {
		obj = ifrlayer.$(obj);

		if(obj && obj.iframelayer) {
			with(obj.iframelayer.style) {
				top = obj.offsetTop+"px";
				left = obj.offsetLeft+"px";

				if (size) {
					width  =  obj.offsetWidth+"px";
					height =  obj.offsetHeight+"px";
				};
			};
		}
	}
};


// ACH -> Intégration de l'objet "console" pour IE
if(!window.console){
	var console = {
		init:function(){
			console.d = document.createElement('div');
			document.body.appendChild(console.d);
			/*var a=document.createElement('a');
			a.href='javascript:console.hide()';
			a.innerHTML='close';
			console.d.appendChild(a);
			var a=document.createElement('a');
			a.href='javascript:console.clear();';
			a.innerHTML='clear';
			console.d.appendChild(a);*/
			var id='fauxconsole';
			if(!document.getElementById(id)){
				console.d.id=id;
			}
			console.hide();
		},

		hide:function(){
			console.d.style.display='none';
		},

		show:function(){
			//console.d.style.display='block';
		},

		trace:function(){
			//console.d.style.display='block';
		},

		info:function(o){
			console.d.innerHTML+='<br/>'+o;
			console.show();
		},

		log:function(o){
			console.d.innerHTML+='<br/>'+o;
			console.show();
		},

		clear:function(){
			console.d.parentNode.removeChild(console.d);
			console.init();
			console.show();
		},

		/*Simon Willison rules*/
		addLoadEvent:function(func){
			var oldonload = window.onload;
			if(typeof window.onload!='function'){
				window.onload=func;
			}else{
				window.onload = function(){
					if(oldonload){
						oldonload();
					}
					func();
				}
			};
		}
	};

	console.addLoadEvent(console.init);
}

// fonction qui retourne true si la valeur passée en param existe dans le tableau
Array.prototype.in_array = function(p_val) {
    for(var i = 0, l = this.length; i < l; i++) {
        if(this[i] == p_val) {
            rowid = i;
            return true;
        }
    }
    return false;
}

// actions sur les select de search engine
function majListItemSearchEngine(responseDOC,valueSelectType,valueSelectYear){
	var positionCurrent = 0;
	for (var i=0; i<responseDOC.getElementsByTagName('media').length ;i++){
		var id = responseDOC.getElementsByTagName('media')[i].getAttribute('id');
		var year = responseDOC.getElementsByTagName('media')[i].getElementsByTagName('year')[0].firstChild.nodeValue;
		var cat = new Array();
		for(var j=0; j<responseDOC.getElementsByTagName('media')[i].getElementsByTagName('categories')[0].getElementsByTagName('category').length; j++){
			cat.push(responseDOC.getElementsByTagName('media')[i].getElementsByTagName('categories')[0].getElementsByTagName('category')[j].firstChild.nodeValue);
		}
		var display = ( valueSelectType == 'all' || cat.in_array(valueSelectType) ) ? 'block' : 'none';
		if(display == 'block'){
			display = ( valueSelectYear == 'all' || valueSelectYear == year ) ? 'block' : 'none';
			if(display == "block"){
				//console.log(year+" "+cat);
			}
		}
		var oldCurrent = $$('.view_search_engine_docs .current');
		var oldLICurrent = $$('.selector_search_engine li.current');
		
		// Modification du style de la LI
		$$('li#document_se_list_' + id).setStyle('display', display);
		
		if(display == "block" && positionCurrent==0){
			oldCurrent.removeClass('current');
			oldCurrent.addClass('hidden');
			oldLICurrent.removeClass('current');
			oldLICurrent.addClass('hidden');
			$$('#document_se_' + id).removeClass('hidden');
			$$('#document_se_' + id).addClass('current');
			$$('li#document_se_list_' + id).removeClass('hidden');
			$$('li#document_se_list_' + id).addClass('current');
			positionCurrent++;
		}
	}
	$$(".res_search_engine .loadingIcone")[0].style.display = "none";
	$$(".res_search_engine .loadingIcone")[1].style.display = "none";
	$$(".selector_search_engine ul.fetch_search_engine")[0].style.display = "block";
}
//fonction de récupération de l'extension depuis le fichier
function getFileExtension(pathFile){
		if(pathFile.match('/')){
			var newPathFile = pathFile.substr((pathFile.lastIndexOf("/")+1), pathFile.length);
			return getExtFromFile(newPathFile);
		}else if(pathFile.match('.')){
			return getExtFromFile(pathFile);
		}else{
			return "no ext";
		}
}
function getExtFromFile(sourceFile){
		if(sourceFile.match('.')){
			var newSourceFile = sourceFile.substr((sourceFile.lastIndexOf(".")+1), sourceFile.length);
			return newSourceFile;
		}else{
			return "no ext";
		}
}
//fonction de récupération du mimetype depuis le fichier
function getMimetypeFromExt(source){
	var sourceExt = getFileExtension(source);
	if(sourceExt == 'xml'){
		return 'text/xml';
	}else if(sourceExt == 'rtp'){
		return 'application/rtf';
	}else if(sourceExt == 'txt'){
		return 'text/xml';
	}else if(sourceExt == 'pdf'){
		return 'application/pdf';
	}else if(sourceExt == 'doc' || sourceExt == 'docx'){
		return 'application/msword';
	}else if(sourceExt == 'xls' || sourceExt == 'xlsx'){
		return 'application/msexcel';
	}else if(sourceExt == 'ppt' || sourceExt == 'pptx' || sourceExt == 'pot' || sourceExt == 'pps'){
		return 'application/mspowerpoint';
	}else if(sourceExt == 'wma'){
		return 'audio/x-ms-wma';
	}else if(sourceExt == 'mp3' || sourceExt == 'mp2'){
		return 'audio/mpeg';
	}else if(sourceExt == 'wav'){
		return 'audio/x-ms-wave';
	}else if(sourceExt == 'jpg' || sourceExt == 'jpeg'){
		return 'image/jpg';
	}else if(sourceExt == 'gif'){
		return 'image/gif';
	}else if(sourceExt == 'png'){
		return 'image/png';
	}else if(sourceExt == 'bmp'){
		return 'image/bmp';
	}else{
		return 'text/xml';
	}
}
function getPictoFromMimetype(mimetype){
	if(mimetype.match("audio/")){
		return '<img src="/images/picto/picto-audio.png"/>';
	}else if(mimetype.match('image/')){
		return '<img src="/images/picto/picto-image.png"/>';
	}else if(mimetype.match('video/')){
		return '<img src="/images/picto/picto-video.png"/>';
	}else if(mimetype.match('application/pdf')){
		return '<img src="/images/picto/picto-pdf.png"/>';
	}else if(mimetype.match('application/msword')){
		return '<img src="/images/picto/picto-doc.png"/>';
	}else if(mimetype.match('application/mspowerpoint')){
		return '<img src="/images/picto/picto-ppt.png"/>';
	}else if(mimetype.match('application/msexcel')){
		return '<img src="/images/picto/picto-xls.png"/>';
	}else{
		return '<img src="/images/picto/picto-txt.png"/>';
	}
}
function getRealSize(size){
	if((size/1024) < 1){
		return (""+size);
	}else if (((size/1024)/1024) < 1){
		return (""+Math.ceil(size/1024)+"K");
	}else if((((size/1024)/1024)/1024) < 1){
		return (""+Math.ceil((size/1024)/1024)+"M");
	}else if(((((size/1024)/1024)/1024)/1024) < 1){
		return (""+Math.ceil(((size/1024)/1024)/1024)+"G");
	}else{
		return (""+size);
	}
}
function showHideDivDisplay(div,action){
	if(action == "hide"){
		for(var i=0; i<$$(div).length; i++){
			$$(div)[i].style.display="none";
		}
	}else{
		for(var i=0; i<$$(div).length; i++){
			$$(div)[i].style.display="block";
		}
	}
}
/*function initSearchEngine2(tabTypes, tabYear, pathImage, responseXML, responseDOC){
	$$('div.search_engine .bloc_search_engine .res_search_engine #view_default_visual')[0].getParent().style.display="block";
	var defaultVisual = $$('div.search_engine .bloc_search_engine .res_search_engine #view_default_visual')[0].src;
	var defaultVisualWidth = $$('div.search_engine .bloc_search_engine .res_search_engine #view_default_visual')[0].width;
	var defaultVisualHeight = $$('div.search_engine .bloc_search_engine .res_search_engine #view_default_visual')[0].height;
	$$('div.search_engine .bloc_search_engine .res_search_engine #view_default_visual')[0].getParent().style.display="none";
	
	var listeDocumentsSE = $$('div.search_engine .selector_search_engine .fetch_search_engine')[0];
	var descDocumentsSE = $$('div.search_engine .bloc_search_engine .res_search_engine .view_search_engine .view_search_engine_docs')[0];
	var selectYear = $$('div.search_engine .form_search_engine form span#my_select_form_search_year')[0];
	var selectCat =$$('div.search_engine .form_search_engine form span#my_select_form_search_type')[0];
	
	var tableContentListe = new Array();
	var tableContentDesc = new Array();
	
	var blocSelectYearWithproperties = selectYear.innerHTML;
	var blocSelectCatWithproperties = selectCat.innerHTML;
	
	blocSelectYearWithproperties = '<select id="select_form_search_year" name="year" class="select_sites select_year">';
	blocSelectCatWithproperties = '<select id="select_form_search_type" name="type" class="select_sites select_type">';

	var yearLabel = (pathImage == "/fr_FR/") ? "Années" : "Years";
	var cateLabel = (pathImage == "/fr_FR/") ? "Toutes catégories" : "All categories";

	tabYear.push(blocSelectYearWithproperties);
	tabYear.push('<option value="all">' + yearLabel + '</option>');
	tabTypes.push(blocSelectCatWithproperties);
	tabTypes.push('<option value="all">' + cateLabel + '</option>');
	
	// on commence le traitement des données
	for (var i=0; i<responseDOC.getElementsByTagName('media').length ;i++){
		var id = responseDOC.getElementsByTagName('media')[i].getAttribute('id');
		var year = responseDOC.getElementsByTagName('media')[i].getElementsByTagName('year')[0].firstChild.nodeValue;
		// on ajoute l'option de l'année en cours
		var optionYearListe = '<option value="'+year+'">'+year+'</option>';
		if(tabYear.in_array(optionYearListe)){
			// on n'ajoute pas si la cat existe deja dans le tableau de cat 
		}else{
			tabYear.push(optionYearListe);
		}
		// on ajoute les options de catégories
		for(var j=0; j<responseDOC.getElementsByTagName('media')[i].getElementsByTagName('categories')[0].getElementsByTagName('category').length; j++){
			var cat = responseDOC.getElementsByTagName('media')[i].getElementsByTagName('categories')[0].getElementsByTagName('category')[j].firstChild.nodeValue;
			// on ajoute l'option de la catégorie en cours
			if( cat != "documentation finance" && cat != "fr" && cat != "en" ) {
				var optionCatListe = '<option value="'+cat+'">'+cat+'</option>';
				if(tabTypes.in_array(optionCatListe)){
					// on n'ajoute pas si la cat existe deja dans le tableau de cat 
				}else{
					tabTypes.push(optionCatListe);
				}
			}
		}
		// recupération du visuel, si vide on passe le visuel par défaut
		if(responseDOC.getElementsByTagName('media')[i].getElementsByTagName('visual')[0].firstChild != null){
			var visual = responseDOC.getElementsByTagName('media')[i].getElementsByTagName('visual')[0].firstChild.nodeValue;
		}else{
			var visual = defaultVisual;
		}
		// récupération de la date dans le bon format langue
		var leOn = ((pathImage == "/fr_FR/") ? "le " : "on ");
		var date = "";
		if(pathImage == "/fr_FR/"){
			var blocXML = responseDOC.getElementsByTagName('media')[i].getElementsByTagName('datefr')[0].firstChild;
		} else {
			var blocXML = responseDOC.getElementsByTagName('media')[i].getElementsByTagName('dateen')[0].firstChild;
		}
		if(blocXML != null){
			var date = blocXML.nodeValue;
		}else{
			var date = "";
		}
		date = (date == '  ' || date == ' , ' || date == '') ? '' : leOn + date;
		// recupération des données textuelles pour les listes et descriptions
		var title = responseDOC.getElementsByTagName('media')[i].getElementsByTagName('title')[0].firstChild.nodeValue;
		var labellink = (responseDOC.getElementsByTagName('media')[i].getElementsByTagName('label')[0].firstChild != null) ? responseDOC.getElementsByTagName('media')[i].getElementsByTagName('label')[0].firstChild.nodeValue : ((pathImage == "/fr_FR/") ? 'Télécharger' : 'Download');
		var link = responseDOC.getElementsByTagName('media')[i].getElementsByTagName('link')[0].firstChild.nodeValue;
		var linksize = responseDOC.getElementsByTagName('media')[i].getElementsByTagName('filesize')[0].firstChild.nodeValue;
		var mimetypeDocSE = getMimetypeFromExt(link);

		if(pathImage == "/fr_FR/"){
			linksize = getRealSize(linksize)+"o";
		}else{
			linksize = getRealSize(linksize)+"b";
		}
		
		// génération de la liste des documents
		var liclass= "";
		if(i==0){
			liclass= "fetch_title_search_engine current";
		}else{
			liclass= "fetch_title_search_engine hidden";
		}
		tableContentListe.push('<li class="'+liclass+'" id="document_se_list_'+id+'"><a href="'+link+'">'+title+'</a></li>');
		
		// génération de la description du document en cours
		if(i==0){
			var classDivDocumentDesc = "document_search_engine current";
		}else{
			var classDivDocumentDesc = "document_search_engine hidden";
		}
		var newDivDocumentDesc = '<div id="document_se_'+id+'" class="'+classDivDocumentDesc+'">';
		
		//ajout du visuel
		newDivDocumentDesc += '<div class="documentImageLeft">';
		newDivDocumentDesc += '<img src="'+visual+'" width="'+defaultVisualWidth+'" height="'+defaultVisualHeight+'" />';
		newDivDocumentDesc += '</div>';
		
		//ajout du bloc de description
		newDivDocumentDesc += '<div class="documentDescSE">';		
		// récupération de la mimetype pour le picto
		var mimetypeDocSE = getMimetypeFromExt(link);
		var htmlInnerDocumentProperties = getPictoFromMimetype(mimetypeDocSE)+'<a href="'+link+'">'+labellink+'</a> ('+linksize+")";
		//documentPropertiesSE.setHTML(htmlInnerDocumentProperties);

		var htmlTextToDesc = '<div class="documentDateSE">'+date+'</div>';
		htmlTextToDesc += '<div class="documentTitleSE">'+title+'</div>';
		htmlTextToDesc += '<div class="document_properties">'+htmlInnerDocumentProperties+'</div>';
		newDivDocumentDesc += htmlTextToDesc;
		newDivDocumentDesc += '</div>';
		
		//divTXTdescSE.setHTML(visual);
		newDivDocumentDesc += '</div>';
		
		// ajout du bloc de desc au tableau
		tableContentDesc.push(newDivDocumentDesc);
	}
	tabYear.push('</select>');
	tabTypes.push('</select>');
	
	// on remplace la div de descriptions
	var stringTableDesc = ""+tableContentDesc.join(" ");
	descDocumentsSE.setHTML(stringTableDesc);
	// on remplace la liste de documents
	var stringListDocs = ""+tableContentListe.join(" ");
	listeDocumentsSE.setHTML(stringListDocs);
	// on remplace la liste des années
	var stringListYears = ""+tabYear.join(" ");
	selectYear.innerHTML = stringListYears;
	// on remplace la liste des catégories
	var stringListCats = ""+tabTypes.join(" ");
	selectCat.innerHTML = stringListCats;
}*/

// Classe Search Engine, test de développement
var SearchEngine = new Class({
	options:{
		pathImage : false,
		urlXMLSE : false,
		tabTypes : false,
		tabYear : false,
		listeDocumentsSE : false,
		descDocumentsSE : false
	},
	initialize: function(options){
		this.pathImage = this.options.pathImage ? this.options.pathImage : document.location.pathname.substring(0,7);
		this.urlXMLSE = this.options.urlXMLSE ? this.options.urlXMLSE : "finance/documentation/documentation.xml";
		this.urlXMLSE = this.pathImage + this.urlXMLSE;
		this.tabTypes = this.options.tabTypes ? this.options.tabTypes : new Array();
		this.tabYear = this.options.tabYear ? this.options.tabYear : new Array();
		this.listeDocumentsSE = this.options.listeDocumentsSE ? this.options.listeDocumentsSE : $$('div.search_engine .selector_search_engine .fetch_search_engine')[0];
		this.descDocumentsSE = this.options.descDocumentsSE ? this.options.descDocumentsSE : $$('div.search_engine .bloc_search_engine .res_search_engine .view_search_engine .view_search_engine_docs')[0];
	},
	setXMLContent:function(xmlFile){
		this.urlXMLSE = this.pathImage + "" + xmlFile;
	},
	createSearchEngineContent : function(){
		var _this = this;
		$$('div.search_engine .bloc_search_engine .res_search_engine')[0].style.display = "block";
		new Ajax(_this.urlXMLSE, {
			onRequest:function(){
				showHideDivDisplay(".res_search_engine .loadingIcone", "show");
				$$(".selector_search_engine ul.fetch_search_engine")[0].style.display = "none";
			},
			onFailure:function(){
				showHideDivDisplay(".res_search_engine .loadingIcone", "hide");
				$$(".selector_search_engine ul.fetch_search_engine")[0].style.display = "block";
			},
			onComplete:function(responseXML, responseDOC){
				$$('div.search_engine .form_search_engine')[0].getElement("form").style.display = "block";
				$$('div.search_engine .form_search_engine')[0].getElement("p").style.display = "none";

				//initSearchEngine(_this.tabTypes, _this.tabYear, _this.pathImage, responseXML, responseDOC);
				_this.initSearchEngine(responseDOC);

				showHideDivDisplay(".res_search_engine .loadingIcone", "hide");
				$$(".selector_search_engine ul.fetch_search_engine")[0].style.display = "block";
				_this.initiateDocumentList();
				$$('div.search_engine .form_search_engine')[0].getElement("p").style.display = "none";
				$$('div.search_engine .form_search_engine')[0].getElement("form").style.display = "block";

				_this.initiateEventCat();
				_this.initiateEventYear();
			}
		}).request();
	},
	/*createDocumentProperties : function(urlLink, linkLabel, filesize){
		var htmlDocumentProperties = '<img src="'+urlLink+'" />';
		htmlDocumentProperties += '<a href="'+urlLink+'">'+linkLabel+'</a>';
		htmlDocumentProperties += '('+filesize+')';
		alert(htmlDocumentProperties);
	},*/
	initiateEventYear:function(){
		var _this = this;
		// Effets sur le select des années
		$$('div.search_engine .form_search_engine form span#my_select_form_search_year select#select_form_search_year').addEvent('change', function(event){
			(new Event(event)).stop();

			var valueSelectYear = this.value;
			var valueSelectType = $$('div.search_engine .form_search_engine form span#my_select_form_search_type select#select_form_search_type')[0].value;
			new Ajax(_this.urlXMLSE, {
				onRequest:function(){
					showHideDivDisplay(".res_search_engine .loadingIcone", "show");
					$$(".selector_search_engine ul.fetch_search_engine")[0].style.display = "none";
				},
				onFailure:function(){
					showHideDivDisplay(".res_search_engine .loadingIcone", "hide");
					$$(".selector_search_engine ul.fetch_search_engine")[0].style.display = "block";
				},
				onComplete:function(responseXML, responseDOC){
					// ETAPE DE COMPLETION : traitement de la liste
					majListItemSearchEngine(responseDOC,valueSelectType,valueSelectYear);
				}
			}).request();
		});
	},
	initiateEventCat:function(){
		var _this = this;
		// Effets sur le select des catégories
		$$('div.search_engine .form_search_engine form span#my_select_form_search_type select#select_form_search_type').addEvent('change', function(event){
			(new Event(event)).stop();
			
			var valueSelectType = this.value;
			var valueSelectYear = $$('div.search_engine .form_search_engine form span#my_select_form_search_year select#select_form_search_year')[0].value;
			new Ajax(_this.urlXMLSE, {
				onRequest:function(){
					showHideDivDisplay(".res_search_engine .loadingIcone", "show");
					$$(".selector_search_engine ul.fetch_search_engine")[0].style.display = "none";
				},
				onFailure:function(){
					showHideDivDisplay(".res_search_engine .loadingIcone", "hide");
					$$(".selector_search_engine ul.fetch_search_engine")[0].style.display = "block";
				},
				onComplete:function(responseXML, responseDOC){
					// ETAPE DE COMPLETION : traitement de la liste
					majListItemSearchEngine(responseDOC,valueSelectType,valueSelectYear);
				}
			}).request();
		});
	},
	initiateDocumentList:function(){
		var _this = this;
		_this.listeDocumentsSE.getElements('li a').each(function(item){
			item.addEvent('click',function(e){
				(new Event(e)).stop();
				var classLi = this.getParent().className;
				if(classLi.match("current") != "current"){
					var liCible = this.getParent();
					var liCurrent = liCible.getParent().getElement("li.current");
					var a = liCible.id.replace(/[^0-9]/g, "");
					var b = liCurrent.id.replace(/[^0-9]/g, "");
					var divVisuToHide = _this.descDocumentsSE.getElement("div#document_se_"+b);
					var divVisuToChange = _this.descDocumentsSE.getElement("#document_se_"+a);
					liCurrent.removeClass("current");
					liCurrent.addClass("hidden");
					liCible.removeClass("hidden");
					liCible.addClass("current");
					divVisuToHide.removeClass("current");
					divVisuToHide.addClass("hidden");
					divVisuToChange.removeClass("hidden");
					divVisuToChange.addClass("current");
				}
			});
			item.addEvent('mouseup',function(e){
				(new Event(e)).stop();
				this.blur();
			});
		});
	},
	initSearchEngine:function(responseDOC){
//initSearchEngine:function(tabTypes, tabYear, pathImage, responseXML, responseDOC){
//initSearchEngine(_this.tabTypes, _this.tabYear, _this.pathImage, responseXML, responseDOC);
		var _this = this;

		// Visuel par défaut
		$$('div.search_engine .bloc_search_engine .res_search_engine #view_default_visual')[0].getParent().style.display="block";
		var defaultVisual = $$('div.search_engine .bloc_search_engine .res_search_engine #view_default_visual')[0].src;
		var defaultVisualWidth = $$('div.search_engine .bloc_search_engine .res_search_engine #view_default_visual')[0].width;
		var defaultVisualHeight = $$('div.search_engine .bloc_search_engine .res_search_engine #view_default_visual')[0].height;
		$$('div.search_engine .bloc_search_engine .res_search_engine #view_default_visual')[0].getParent().style.display="none";

		// Création des selectboxes
		_this.tabYear.push('<select id="select_form_search_year" name="year" class="select_sites select_year">');
		_this.tabYear.push('<option value="all">' + ((_this.pathImage == "/fr_FR/") ? "Années" : "Years") + '</option>');
		_this.tabTypes.push('<select id="select_form_search_type" name="type" class="select_sites select_type">');
		_this.tabTypes.push('<option value="all">' + ((_this.pathImage == "/fr_FR/") ? "Toutes catégories" : "All categories") + '</option>');

		// Création des tableaux conteneurs
		var tableContentListe = new Array();
		var tableContentDesc = new Array();

		// traitement des données
		for (var i=0; i<responseDOC.getElementsByTagName('media').length ;i++){
			var id = responseDOC.getElementsByTagName('media')[i].getAttribute('id');
			var year = responseDOC.getElementsByTagName('media')[i].getElementsByTagName('year')[0].firstChild.nodeValue;

			// ANNEE
			var optionYearListe = '<option value="' + year + '">' + year + '</option>';
			if(!_this.tabYear.in_array(optionYearListe)) {
				_this.tabYear.push(optionYearListe);
			}

			// CATEGORIE
			for(var j=0; j<responseDOC.getElementsByTagName('media')[i].getElementsByTagName('categories')[0].getElementsByTagName('category').length; j++){
				var cat = responseDOC.getElementsByTagName('media')[i].getElementsByTagName('categories')[0].getElementsByTagName('category')[j].firstChild.nodeValue;
				if(cat != "documentation finance" && cat != "fr" && cat != "en") {
					var optionCatListe = '<option value="' + cat + '">' + cat + '</option>';
					if(!_this.tabTypes.in_array(optionCatListe)) {
						_this.tabTypes.push(optionCatListe);
					}
				}
			}

			// DATE
			var leOn = ((_this.pathImage == "/fr_FR/") ? "le " : "on ");
			var date = "";
			if(_this.pathImage == "/fr_FR/"){
				var blocXML = responseDOC.getElementsByTagName('media')[i].getElementsByTagName('datefr')[0].firstChild;
			} else {
				var blocXML = responseDOC.getElementsByTagName('media')[i].getElementsByTagName('dateen')[0].firstChild;
			}
			date = (blocXML != null) ? blocXML.nodeValue : "";
			date = (date == '  ' || date == ' , ' || date == '') ? '' : leOn + date;

			// TEXTES : TITLE / (img) LABELLINK (size)
			var title = responseDOC.getElementsByTagName('media')[i].getElementsByTagName('title')[0].firstChild.nodeValue;
			var link = responseDOC.getElementsByTagName('media')[i].getElementsByTagName('link')[0].firstChild.nodeValue;
			var linksize = responseDOC.getElementsByTagName('media')[i].getElementsByTagName('filesize')[0].firstChild.nodeValue;
			linksize = getRealSize(linksize) + ((_this.pathImage == "/fr_FR/") ? "o" : "b");

			// LISTE DES DOCS
			var liclass= "";
			liclass = (i == 0) ? "fetch_title_search_engine current" : "fetch_title_search_engine hidden";
			tableContentListe.push('<li class="' + liclass + '" id="document_se_list_' + id + '"><a href="' + link + '">' + title + '</a></li>');

			// DESCRIPTION CURRENT DOC
			var classDivDocumentDesc = (i == 0) ? "document_search_engine current" : "document_search_engine hidden";
			var newDivDocumentDesc = '<div id="document_se_' + id + '" class="' + classDivDocumentDesc + '">';

			// VISUEL
			var visual = defaultVisual;
			if( responseDOC.getElementsByTagName('media')[i].getElementsByTagName('visual')[0].firstChild != null ) {
				visual = responseDOC.getElementsByTagName('media')[i].getElementsByTagName('visual')[0].firstChild.nodeValue;
			}
			newDivDocumentDesc += '	<div class="documentImageLeft">';
			newDivDocumentDesc += '	<img src="' + visual + '" width="' + defaultVisualWidth + '" height="' + defaultVisualHeight + '" />';
			newDivDocumentDesc += '	</div>';

			// LIEN
			newDivDocumentDesc += '	<div class="documentDescSE">';
			var mimetypeDocSE = getMimetypeFromExt(link);
			var labellink = (responseDOC.getElementsByTagName('media')[i].getElementsByTagName('label')[0].firstChild != null) ? responseDOC.getElementsByTagName('media')[i].getElementsByTagName('label')[0].firstChild.nodeValue : ((_this.pathImage == "/fr_FR/") ? 'Télécharger' : 'Download');
			var htmlInnerDocProp = getPictoFromMimetype(mimetypeDocSE) + '<a href="' + link + '">' + labellink + '</a> (' + linksize + ")";
			//documentPropertiesSE.setHTML(htmlInnerDocProp);
			var htmlTextToDesc = '		<div class="documentDateSE">' + date + '</div>';
			htmlTextToDesc += '		<div class="documentTitleSE">' + title + '</div>';
			htmlTextToDesc += '		<div class="document_properties">' + htmlInnerDocProp + '</div>';
			newDivDocumentDesc += htmlTextToDesc;
			newDivDocumentDesc += '	</div>';

			//divTXTdescSE.setHTML(visual);
			newDivDocumentDesc += '</div>';

			// ajout du bloc de desc au tableau
			tableContentDesc.push(newDivDocumentDesc);
		}

		_this.tabYear.push('</select>');
		_this.tabTypes.push('</select>');

		// Remplacement des contenus
		var listeDocumentsSE = $$('div.search_engine .selector_search_engine .fetch_search_engine')[0];
		var descDocumentsSE = $$('div.search_engine .bloc_search_engine .res_search_engine .view_search_engine .view_search_engine_docs')[0];
		var selectYear = $$('div.search_engine .form_search_engine form span#my_select_form_search_year')[0];
		var selectCat =$$('div.search_engine .form_search_engine form span#my_select_form_search_type')[0];

		// on remplace la div de descriptions
		var stringTableDesc = ""+tableContentDesc.join(" ");
		descDocumentsSE.setHTML(stringTableDesc);
		// on remplace la liste de documents
		var stringListDocs = ""+tableContentListe.join(" ");
		listeDocumentsSE.setHTML(stringListDocs);
		// on remplace la liste des années
		var stringListYears = "" + _this.tabYear.join(" ");
		selectYear.innerHTML = stringListYears;
		// on remplace la liste des catégories
		var stringListCats = "" + _this.tabTypes.join(" ");
		selectCat.innerHTML = stringListCats;
	}
});

$(window).addEvent('domready', function(){
	// Correct PNG au lancement
	correct_png_background();
	// lancement du bloc search engine
	if($$('div.search_engine').length != 0){
		$$('div.search_engine .form_search_engine')[0].getElement("p").setHTML("Chargement du module de recherche en cours ...");
		var SEDocumentation = new SearchEngine();
		SEDocumentation.setXMLContent("searchengine.xml");
		SEDocumentation.createSearchEngineContent();

		/*var pathImage = document.location.pathname.substring(0,7);
		var urlXMLSE = pathImage+"finance/documentation/documentation.xml";
		var tabTypes = new Array();
		var tabYear = new Array();
		var listeDocumentsSE = $$('div.search_engine .selector_search_engine .fetch_search_engine')[0];
		var descDocumentsSE = $$('div.search_engine .bloc_search_engine .res_search_engine .view_search_engine .view_search_engine_docs')[0];
		if($$('div.search_engine .bloc_search_engine').length != 0){
			// initialisation des valeurs dans les blocs
			new Ajax(urlXMLSE, {
				onRequest:function(){
					$$('div.search_engine .form_search_engine')[0].getElement("p").setHTML("Chargement du module de recherche en cours ...");
					showHideDivDisplay(".res_search_engine .loadingIcone", "show");
					$$(".selector_search_engine ul.fetch_search_engine")[0].style.display = "none";
				},
				onFailure:function(){
					showHideDivDisplay(".res_search_engine .loadingIcone", "hide");
					$$(".selector_search_engine ul.fetch_search_engine")[0].style.display = "block";
				},
				onComplete:function(responseXML, responseDOC){
					// ETAPE DE COMPLETION : traitement de la liste
					initSearchEngine(tabTypes, tabYear, pathImage, responseXML,responseDOC);
					// une fois qu'on a tout généré, on enleve les doublons
					$$('div.search_engine .form_search_engine form select#select_form_search_year option').each(function(item){
						if(tabYear.in_array(item.value)){
							item.remove();
						}else{
							tabYear.push(item.value);
						}
					});
					$$('div.search_engine .form_search_engine form select#select_form_search_type option').each(function(item){
						if(tabTypes.in_array(item.value)){
							item.remove();
						}else{
							tabTypes.push(item.value);
						}
					});
					showHideDivDisplay(".res_search_engine .loadingIcone", "hide");
					$$(".selector_search_engine ul.fetch_search_engine")[0].style.display = "block";
					// action sur les liens de la liste de documents
					listeDocumentsSE.getElements('li a').each(function(item){
						item.addEvent('click',function(e){
							(new Event(e)).stop();
							var classLi = this.getParent().className;
							if(classLi.match("current") != "current"){
								var liCible = this.getParent();
								var liCurrent = liCible.getParent().getElement("li.current");
								var a = liCible.id.replace(/[^0-9]/g, "");
								var b = liCurrent.id.replace(/[^0-9]/g, "");
								var divVisuToHide = descDocumentsSE.getElement("div#document_se_"+b);
								var divVisuToChange = descDocumentsSE.getElement("#document_se_"+a);
								liCurrent.removeClass("current");
								liCurrent.addClass("hidden");
								liCible.removeClass("hidden");
								liCible.addClass("current");
								divVisuToHide.removeClass("current");
								divVisuToHide.addClass("hidden");
								divVisuToChange.removeClass("hidden");
								divVisuToChange.addClass("current");
							}
						});
						item.addEvent('mouseup',function(e){
							(new Event(e)).stop();
							this.blur();
						});
					});
					$$('div.search_engine .form_search_engine')[0].getElement("p").style.display = "none";
					$$('div.search_engine .form_search_engine')[0].getElement("form").style.display = "block";
				}
			}).request();
		}*/
		/*// Effets sur le select des années
		$$('div.search_engine .form_search_engine form select#select_form_search_year').addEvent('change', function(event){
			(new Event(event)).stop();

			var valueSelectYear = this.value;
			var valueSelectType = $$('div.search_engine .form_search_engine form select#select_form_search_type')[0].value;
			new Ajax(urlXMLSE, {
				onRequest:function(){
					showHideDivDisplay(".res_search_engine .loadingIcone", "show");
					$$(".selector_search_engine ul.fetch_search_engine")[0].style.display = "none";
				},
				onFailure:function(){
					showHideDivDisplay(".res_search_engine .loadingIcone", "hide");
					$$(".selector_search_engine ul.fetch_search_engine")[0].style.display = "block";
				},
				onComplete:function(responseXML, responseDOC){
					// ETAPE DE COMPLETION : traitement de la liste
					majListItemSearchEngine(responseDOC,valueSelectType,valueSelectYear);
				}
			}).request();
		});

		// Effets sur le select des catégories
		$$('div.search_engine .form_search_engine form select#select_form_search_type').addEvent('change', function(event){
			(new Event(event)).stop();

			var valueSelectType = this.value;
			var valueSelectYear = $$('div.search_engine .form_search_engine form select#select_form_search_year')[0].value;
			new Ajax(urlXMLSE, {
				onRequest:function(){
					showHideDivDisplay(".res_search_engine .loadingIcone", "show");
					$$(".selector_search_engine ul.fetch_search_engine")[0].style.display = "none";
				},
				onFailure:function(){
					showHideDivDisplay(".res_search_engine .loadingIcone", "hide");
					$$(".selector_search_engine ul.fetch_search_engine")[0].style.display = "block";
				},
				onComplete:function(responseXML, responseDOC){
					// ETAPE DE COMPLETION : traitement de la liste
					majListItemSearchEngine(responseDOC,valueSelectType,valueSelectYear);
				}
			}).request();
		});*/
		
		/*$$('div.search_engine .form_search_engine form #valid_formSearchEngine').addEvent('click', function(event){
			(new Event(event)).stop();
			var SEDaniel = new SearchEngine();
		});*/
	}
	// remodelage bloc Caroussel
	if($$('div.publi_caroussel_container').length != 0){
		$$('div.publi_caroussel_container .publi_caroussel').removeClass("blocPreloadCaroussel");
	}

	//test de fonctionnalité JS des blocs Reports
	if($$('div.content_insert_reports').length != 0){
		$$('.content_insert_reports_unit').addClass("hidden");
		$$('.content_insert_reports div.current').removeClass("hidden");
		$$('.content_insert_reports')[0].getElements('li a').addEvent("click", function (e){
			(new Event(e)).stop();
		});
		
		$$('.content_insert_reports')[0].getElements('li a img').addEvent("click", function (e){
			(new Event(e)).stop();
			var a = this.getParent().getParent().getParent().id.replace(/[^0-9]/g, "");
			var c = $$('.content_insert_reports .current')[0].removeClass("current").addClass("hidden");
			var b = $('content_insert_reports_unit_'+a);
			b.removeClass("hidden").addClass('current');
			//$$(".content_insert_reports")[0].getElement('.current').removeClass("current").addClass('hidden');
			//b.removeClass("hidden").addClass('current');
		});
	}
});