// JavaScript Document

function fileLinks() {
    var fileLink;
    if (document.getElementsByTagName('a')) {
        for (var i = 0; (fileLink = document.getElementsByTagName('a')[i]); i++) {
            if (fileLink.href.indexOf('.pdf') != -1) {
                fileLink.setAttribute('target', '_blank');
                fileLink.className = 'pdfLink lien-externe';
            }
            if (fileLink.href.indexOf('.doc') != -1) {
                fileLink.setAttribute('target', '_blank');
                fileLink.className = 'docLink';
            }
			if (fileLink.href.indexOf('.xls') != -1) {
                fileLink.setAttribute('target', '_blank');
                fileLink.className = 'excelLink';
            }
            if (fileLink.href.indexOf('.zip') != -1) {
                fileLink.setAttribute('target', '_blank');
                fileLink.className = 'zipLink';
            }
        }
    }
}
window.onload = function() {
    fileLinks();
}