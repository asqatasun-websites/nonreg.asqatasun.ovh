var incBusy = false;
function initIncDec(){
	$("a.incLink").each(function() {
		$(this).attr("href","javascript:void(0);");
		$(this).bind("click", inc);
	});
	$("a.decLink").each(function() {
		$(this).attr("href","javascript:void(0);");
		$(this).bind("click", dec);
	});
   if(document.getElementById("kitchen")){
      var myCheckBox = document.getElementById("emptySelection");
      addEvent(myCheckBox, 'click', emptyAllInput, false);
      var myCheckBox2 = document.getElementById("planType");
      addEvent(myCheckBox2, 'click', planType, false);
   }
}

function initIncDecForPopin(){
   var linksInc = getElementsByClass("incLinkPopin",null,"a");
   var link= "";
   if(linksInc.length>0){
      for(var i=0; i<linksInc.length;i++){
   		link=linksInc[i];
         addEvent(link, 'click', inc, false);
         link.href = "javascript:void(0);";
   	}
   }
   var linksDec = getElementsByClass("decLinkPopin",null,"a");
   if(linksDec.length>0){
      for(var i=0; i<linksDec.length;i++){
   		link=linksDec[i];
         addEvent(link, 'click', dec, false);
         link.href = "javascript:void(0);";
   	}
   }
}

//Fonction incr&eacute;mente quantit&eacute; article panier
function inc(e){
	var navig  = navigator.userAgent;
	/*
	var cartNbrTotal = document.getElementById("cartNbrTotal").value;
	if(document.getElementById("bagLimitMsg")){
		var msgLimitCart = document.getElementById("bagLimitMsg").value;
	}

	if(parseInt(cartNbrTotal)<20){
	*/
	var the_parent;
	the_parent = findTarget(e);
	if(!incBusy){
		incBusy = true;
	   while(the_parent.nodeName.toLowerCase()!="div" && the_parent.className != "qtyBloc"){
	      the_parent = the_parent.parentNode;
	   }
	   var inputQuantite = the_parent.parentNode.getElementsByTagName('INPUT');
	   
	   if(inputQuantite[0].value < 99){
		   
		   inputQuantite[0].value++;
		   
		   //Pour la fiche de cuisine
		   if(document.getElementById("kitchen")){
		      document.getElementById("visuPlan").style.display = "none";
		     //DBUT MODIFICATIONS DU 09/08/07
		      the_parent = the_parent.parentNode;
		      var price = the_parent.getElementsByTagName('INPUT');
			  
		      var compoTotal = document.getElementById("compoTotal");
		      var total = compoTotal.innerHTML.replace(",", ".");
		      the_parent = the_parent.parentNode;
		      inputQuantite = the_parent.getElementsByTagName('INPUT');
		      var addPrice = price[1].value.replace(",", '.');
		
			  var compoOldTotal = document.getElementById("compoOldTotal");
			  var oldTotal = compoOldTotal.innerHTML.replace(",", ".");
			  var addOldPrice = price[3].value.replace(",", '.');
			  
		      //FIN MODIFICATIONS DU 09/08/07
		      //Test si navigateur est safari
		      if(navig.search("AppleWebKit")!= -1){
		     	 var p = total.split("&");
		     	 var o = oldTotal.split("&");
		     	 oldTotal =o[0]
		     	 total = p[0]
		      }
			  total = parseFloat(total) + parseFloat(addPrice);
		      total = Math.round(total*100)/100;

			  total=total.toFixed(2);
		      replacePrice(compoTotal, total);
			  
			  oldTotal = parseFloat(oldTotal) + parseFloat(addOldPrice);
		      oldTotal = Math.round(oldTotal*100)/100;
		
			  oldTotal = oldTotal.toFixed(2);
		      replacePrice(compoOldTotal, oldTotal);
			  afficheOldPrice(total,oldTotal);
			  
			  var compoTotalEconomies = document.getElementById("compoTotalEconomies");
			  if(compoTotalEconomies!=null && compoTotalEconomies!="undefined"){
			  	var resTotlot = oldTotal-total;
		  	 	if(!isNaN(resTotlot) && resTotlot>0){
		  	 		replacePrice(compoTotalEconomies, resTotlot.toFixed(2));
		  	 		document.getElementById("blackLot").style.display = "block";
		  	 	}else{
		  	 		document.getElementById("blackLot").style.display = "none";
		  	 	}
			  }
			  var compoTotalEconomiesPercent = document.getElementById("compoTotalEconomiesPercent");
			  if(compoTotalEconomiesPercent!=null && compoTotalEconomiesPercent!="undefined"){
			  	var resultat = ((oldTotal-total)*100)/oldTotal;
			  	if(!isNaN(resultat) && resultat >0){
			  		replacePercent(compoTotalEconomiesPercent, resultat.toFixed(2));
			  		document.getElementById("blackLot").style.display = "block";
			  	}else{
			  		document.getElementById("blackLot").style.display = "none";
			  	}
			  }
			  var compoTotalEconomiesKitchen = document.getElementById("compoTotalEconomiesKitchen");
			  if(compoTotalEconomiesKitchen!=null && compoTotalEconomiesKitchen!="undefined"){
			  	replacePrice(compoTotalEconomiesKitchen, oldTotal-total);
			  }
			  var oldLotPriceFlyerPromo = document.getElementById("oldLotPriceFlyerPromo");
			  if(oldLotPriceFlyerPromo==null || oldLotPriceFlyerPromo=="undefined" ){
			  	document.getElementById("oldLotPriceFlyer").style.display = "none";
			  }
		      document.getElementById("emptySelection").checked = false;
		      document.getElementById("planType").checked = false;
		   }
	   }
	
	   incBusy = false;
	   // for shipping cart (reload page)
	   if(document.getElementById("ShopCartForm") && UpdateTotal){
		   UpdateTotal(document.ShopCartForm);
		   incBusy = true;
	   }
   }
   /*}else{
   			document.getElementById("bagLimitMsg").style.display="block";
		}*/
}
         
//Fonction d&eacute;cr&eacute;mente quantit&eacute; article panier
function dec(e){
	var navig  = navigator.userAgent;
	var the_parent;
	the_parent = findTarget(e);
	if(!incBusy){
		incBusy = true;
	   while(the_parent.nodeName.toLowerCase()!="div" && the_parent.className != "qtyBloc"){
	      the_parent = the_parent.parentNode;
	   }
	   var inputQuantite = the_parent.parentNode.getElementsByTagName('INPUT');
	   var minVal = 1;
	   if (inputQuantite[0].value > 0 ) {
	   	   
	   	   if(document.getElementById("embededPoductPage") || $("div.OrderByCatalogueCol1").length > 0){
	   	   	  if ( $(".crossSales").length > 0 ) {
				minVal = 0;
			  }
			  if ( inputQuantite[0].value <= minVal){ 
	   				incBusy = false;
			 	 	return;
			  }
		   } 
	       inputQuantite[0].value--;   
	       //Pour la fiche de cuisine
	       if(document.getElementById("kitchen")){
	       
	         document.getElementById("visuPlan").style.display = "none";
	         
	         //DBUT MODIFICATIONS DU 09/08/07
	         the_parent = the_parent.parentNode;
	         var price = the_parent.getElementsByTagName('INPUT');
			         
	         var compoTotal = document.getElementById("compoTotal");
	         var total = compoTotal.innerHTML.replace(",", ".");
	         var minusPrice = price[1].value.replace(",", '.');
			 
			 var compoOldTotal = document.getElementById("compoOldTotal");
			 var oldTotal = compoOldTotal.innerHTML.replace(",", ".");
			 var minusOldPrice = price[3].value.replace(",", '.');
		  
	         //FIN MODIFICATIONS DU 09/08/07
	          //Test si navigateur est safari
		      if(navig.search("AppleWebKit")!= -1){
		     	 var p = total.split("&");
		     	 var o = oldTotal.split("&");
		     	 oldTotal =o[0]
		     	 total = p[0]
		      }
	         total = parseFloat(total) - parseFloat(minusPrice);
	         total = Math.round(total*100)/100;
				total=total.toFixed(2);
	         replacePrice(compoTotal, total);
			 
			 oldTotal = parseFloat(oldTotal) - parseFloat(minusOldPrice);
		     oldTotal = Math.round(oldTotal*100)/100;
		     oldTotal = oldTotal.toFixed(2);
	         replacePrice(compoOldTotal, oldTotal);
			 
			 afficheOldPrice(total,oldTotal);
			 
			 var compoTotalEconomies = document.getElementById("compoTotalEconomies");
		  	 if(compoTotalEconomies!=null && compoTotalEconomies!="undefined"){
		  	 	var resTotlot = oldTotal-total;
		  	 	if(!isNaN(resTotlot) && resTotlot>0){
		  	 		replacePrice(compoTotalEconomies, resTotlot.toFixed(2));
		  	 		document.getElementById("blackLot").style.display = "block";
		  	 	}else{
		  	 		document.getElementById("blackLot").style.display = "none";
		  	 	}
		  	 }
		  	 
			 var compoTotalEconomiesPercent = document.getElementById("compoTotalEconomiesPercent");
			  if(compoTotalEconomiesPercent!=null && compoTotalEconomiesPercent!="undefined"){
			  	var resultat = ((oldTotal-total)*100)/oldTotal;
			  	if(!isNaN(resultat) && resultat >0){
			  		replacePercent(compoTotalEconomiesPercent, resultat.toFixed(2));
			  		document.getElementById("blackLot").style.display = "block";
			  	}else{
			  		document.getElementById("blackLot").style.display = "none";
			  	}
			  }
			 var compoTotalEconomiesKitchen = document.getElementById("compoTotalEconomiesKitchen");
			 if(compoTotalEconomiesKitchen!=null && compoTotalEconomiesKitchen!="undefined"){
			  	replacePrice(compoTotalEconomiesKitchen, oldTotal-total);
			 }
			 
			 var oldLotPriceFlyerPromo = document.getElementById("oldLotPriceFlyerPromo");
			  if(oldLotPriceFlyerPromo==null || oldLotPriceFlyerPromo=="undefined" ){
			  	document.getElementById("oldLotPriceFlyer").style.display = "none";
			  }
	         document.getElementById("planType").checked = false;
	      }
	      
	   	  incBusy = false;
	      // for shipping cart (reload page)
		  if(document.getElementById("ShopCartForm") && UpdateTotal){
			 UpdateTotal(document.ShopCartForm);
			 incBusy = true;
		  }
	   }else{
	  	 incBusy = false;
	   }
   }
   
}

//Calcul du prix avant reduction
function calculOldPrice(total){
   if(document.getElementById("compoOldTotal")){
      var compoOldTotal = document.getElementById("compoOldTotal");
      var oldTotal = 0;
      /*if(document.getElementById("reductionPercent").value!=""){
         alert ('Percent');
         var reductionPercent = parseFloat(document.getElementById("reductionPercent").value);
         oldTotal = (100 * total) / (100 - reductionPercent);
         oldTotal = Math.round(oldTotal*100)/100;
			oldTotal=oldTotal.toFixed(2);
      }
      if(document.getElementById("reductionEuro").value!=""){
         alert ('Euro');
         var reductionEuro = parseFloat(document.getElementById("reductionEuro").value.replace(",", '.'));
         oldTotal = total + reductionEuro;
      }
      compoOldTotal.innerHTML = String(oldTotal).replace(".", ",");*/  
         
      oldTotal =  total.toFixed(2);
      replacePrice(compoOldTotal, oldTotal);
   }
}


function calculateTotals() {
	// cette m�thode est appel�e (listener => load) du moment que incquantity.js
	// est inclus. Elle ne doit �tre utilis�e que dans une page de fiche lot.
	if (document.getElementById("kitchen") == null) {
		return;
	}
	
      inputs = getElementsByClass("defaultQty",null,"input");
      var total = 0;
      var oldTotal = 0;
      if(inputs.length>0){
         for(var i=0; i<inputs.length;i++){
      		inputs2 = inputs[i].parentNode.getElementsByTagName('INPUT');
            inputQty = inputs2[0];
            total += (parseFloat(inputQty.value) * parseFloat(inputs2[1].value.replace(",", '.')));
            oldTotal += (parseFloat(inputQty.value) * parseFloat(inputs2[3].value.replace(",", '.')));
      	}
      }
      document.getElementById("visuPlan").style.display = "block";
      var compoTotal = document.getElementById("compoTotal");
	  total=total.toFixed(2);
      replacePrice(compoTotal, total);
		
      //calculOldPrice(total);
      calculOldPrice(oldTotal);
}

//Selectionner tous les elements
function emptyAllInput(){
   if(document.getElementById("emptySelection").checked){
      var inputs = getElementsByClass("inputQty",null,"input");
      if(inputs.length>0){
         for(var i=0; i<inputs.length;i++){
      		inputs[i].value = 1;
      	}
      }
      
      calculateTotals();
      document.getElementById("planType").checked = false;
	
	  //afficheOldPrice(0,0);
   }
}


//Retrouver le plan type
function planType(){
   if(document.getElementById("planType") != null && document.getElementById("planType").checked){
      var inputs = getElementsByClass("defaultQty",null,"input");
      var total = 0;
      var oldTotal = 0;
      if(inputs.length>0){
         for(var i=0; i<inputs.length;i++){
      		inputs2 = inputs[i].parentNode.getElementsByTagName('INPUT');
            inputQty = inputs2[0];
            inputQty.value = inputs[i].value;
            total += (parseFloat(inputs[i].value) * parseFloat(inputs2[1].value.replace(",", '.')))
            oldTotal += (parseFloat(inputs[i].value) * parseFloat(inputs2[3].value.replace(",", '.')));
      	}
      }
      document.getElementById("visuPlan").style.display = "block";
      var compoTotal = document.getElementById("compoTotal");
		total=total.toFixed(2);
      replacePrice(compoTotal, total);

	  var compoOldTotal = document.getElementById("compoOldTotal");
	  oldTotal=oldTotal.toFixed(2);
      replacePrice(compoOldTotal, oldTotal);
	  
	  afficheOldPrice(total,oldTotal);
	  
      document.getElementById("emptySelection").checked = false;
   }
}

function afficheOldPrice(total,oldTotal){
	if(total == oldTotal) document.getElementById("compoOldTotal").parentNode.style.display = "none";
	else document.getElementById("compoOldTotal").parentNode.style.display = "inline";
}

function replacePrice(element, newPrice) {
	element.innerHTML = String(newPrice).replace(".", ",")+ "&nbsp;&euro;";
}

function replacePercent(element, newPrice) {
	element.innerHTML = String(newPrice).replace(".", ",")+ "&nbsp;%";
}

//initialisation au chargement de la page (uniquement utile pour la page de lot)

addLoadListener(planType);
addLoadListener(calculateTotals);