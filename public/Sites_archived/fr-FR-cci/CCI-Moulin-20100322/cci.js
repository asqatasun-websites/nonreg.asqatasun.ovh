function toggleMenu(id)
{
    var menu = document.getElementById(id);
    if (menu) {
        if (menu.className.indexOf('masked') != -1)
            menu.className = menu.className.replace(' masked', '');
        else
            menu.className = menu.className + ' masked';
	}
}

function resizeCorps()
{
    var menu = document.getElementById('menu');
    var corps = document.getElementById('corps');
    if (menu && corps) {
        // We get the size of the 'menu' div
        hMenu = menu.offsetHeight;
        // And the size of the 'corps' div
        hCorps = corps.offsetHeight;
        // Then we adjsut the second to the first
        if (hCorps < hMenu - 180)
            corps.style.height = hMenu - 180 + 'px';
    }
}

function imageClick(event)
{
	if (!event)
		var event = window.event; 
	var target;
	if (event.target)
		target = event.target;
	else if (event.srcElement)
		target = event.srcElement;
	else
		exit;
	var image = document.getElementById('currentimage');
	if (image) {
		if (target.src)
			var src = target.src.replace(/&w=.*/, '&w=700');
		else if (target.href)
			var src = target.href;
		else
			exit;
		if (image.src == src) 
			imageLoaded(null);
		else {
			// We set the image
			if (image.addEventListener) {
				image.addEventListener('load', imageLoaded, false);
			} else {
				image.attachEvent('onload', imageLoaded);
			}
			image.src = src;
		}
		
		if (event.preventDefault)
			event.preventDefault();
		event.returnValue = false;
	}
}

function imageLoaded(event)
{
	var div = document.getElementById('imagerollover');
	
	if (div) {
		// We retrieve window and div sizes
		var ww = wh = st = 0;
		if (typeof(window.innerWidth) == 'number') {
			ww = window.innerWidth;
			wh = window.innerHeight;
			st = window.pageYOffset;
		} else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
			ww = document.documentElement.clientWidth;
			wh = document.documentElement.clientHeight;
			st = document.documentElement.scrollTop;
		}
		var dw = div.firstChild.width;
		var dh = div.firstChild.height;
		// We place div
		var left = (ww - dw) / 2;
		div.style.left = (left < 0 ? 10 : left) + 'px';
		var top = st + (wh - dh) / 2;
		if (top < 283)
			top = 283;
		div.style.top = (top < 0 ? 10 : top) + 'px';
		div.style.display = 'block';
	}	
}

function imageMouseOut()
{
	var div = document.getElementById('imagerollover');
	if (div)
		div.style.display = 'none';	
}

function setupRollover()
{
	var imgs = document.getElementsByTagName("img");

	for (var i=0; i < imgs.length; i++) {
		var img = imgs[i];
		if (img.className.indexOf('rollover') != -1) {
			if (img.addEventListener) {
				img.addEventListener('click', imageClick, false);
			} else {
				img.attachEvent('onclick', imageClick);
			}
		}
	}
	
	var anchors = document.getElementsByTagName("a");

	for (var i=0; i < anchors.length; i++) {
		var anchor = anchors[i];
		if (anchor.className.indexOf('rollover') != -1) {
			if (anchor.addEventListener) {
				anchor.addEventListener('click', imageClick, false);
			} else {
				anchor.attachEvent('onclick', imageClick);
			}
		}
	}
}

