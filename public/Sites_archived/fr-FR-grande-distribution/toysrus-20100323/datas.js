var DATAS = {};

// r�gles de validation et messages d'erreur
DATAS.rules = {
	required: {
		msg: 'requis',
		test: function(o) {return o.type == 'select-one' ? o.value != -1 : !/^\s*$/.test(o.value);}
	},
	email: {
		msg: 'non valide',
		test: function(o) {return /^\s*[\w-]+(\.[\w-]+)*@([\w-]+\.)+[A-Z]{2,7}\s*$/i.test(o.value);}
	},
	password: {
		msg: 'Le mot de passe doit comporter au moin 5 caract�res',
		test: function(o) {return /^[a-z0-9]{5,}$/i.test(o.value);}
	},
	zip: {
		msg: 'non valide',
		test: function(o) {return /^\s*[0-9]{5}\s*$/i.test(o.value);}
	},
	confirm: {
		msg: 'confirmation erron�e',
		test: function(o, ref) {return o.value == ref.value;}
	},
	carteClub: {
		msg: 'non valide',
		test: function(o) {return /^\s*[\d]{13}\s*$/.test(o.value);}
	}
};

// message d'infos quand un formulaire n'est pas valide
DATAS.inform = {
	msg: 'Le fomulaire contient au moins une erreur (champ omis ou non valide). Toute erreur est indiqu�e en italique et en rouge � c�t� du champ concern�. Merci de corriger.',
	cName: 'errorsInforming',
	disable: 'fLogin, fIdentification'
};

// messages d'erreur sp�cifiques
DATAS.msgSpecs = {
	'EnfantDateJourAnniv1': ['Jour requis'],
	'EnfantDateJourAnniv2': ['Jour requis'],
	'EnfantDateJourAnniv3': ['Jour requis'],
	'EnfantDateJourAnniv4': ['Jour requis'],
	'EnfantDateMoisAnniv1': ['Mois requis'],
	'EnfantDateMoisAnniv2': ['Mois requis'],
	'EnfantDateMoisAnniv3': ['Mois requis'],
	'EnfantDateMoisAnniv4': ['Mois requis'],
	'EnfantDateYearAnniv1': ['Ann�e requise'],
	'EnfantDateYearAnniv2': ['Ann�e requise'],
	'EnfantDateYearAnniv3': ['Ann�e requise'],
	'EnfantDateYearAnniv4': ['Ann�e requise'],
	'EnfantDateJourAnnivFutur1': ['Jour requis'],
	'EnfantDateJourAnnivFutur2': ['Jour requis'],
	'EnfantDateJourAnnivFutur3': ['Jour requis'],
	'EnfantDateJourAnnivFutur4': ['Jour requis'],
	'EnfantDateMoisAnnivFutur1': ['Mois requis'],
	'EnfantDateMoisAnnivFutur2': ['Mois requis'],
	'EnfantDateMoisAnnivFutur3': ['Mois requis'],
	'EnfantDateMoisAnnivFutur4': ['Mois requis'],
	'EnfantDateYearAnnivFutur1': ['Ann�e requise'],
	'EnfantDateYearAnnivFutur2': ['Ann�e requise'],
	'EnfantDateYearAnnivFutur3': ['Ann�e requise'],
	'EnfantDateYearAnnivFutur4': ['Ann�e requise']
}

// champs de formulaire � valider
DATAS.validate = {
	'UsrEmailEspacePerso': ['required', 'email'],
	'UsrMDPEspacePerso': ['required', 'password'],
	'UsrEmailEspacePerso1': ['required', 'email'],
	'UsrMDPEspacePerso1': ['required', 'password'],
	'mlle,mme,mr': ['required'],
	'UsrNom': ['required'],
	'UsrPrenom': ['required'],
	'UsrEmail': ['required', 'email'],
	'UsrEmailNew': ['email'],
	'UsrEmailConfirm': [],
	'UsrPassword': ['required', 'password'],
	'UsrPasswordNew': ['password'],
	'UsrPasswordConfirm': [],
	'UsrAdresse': ['required'],
	'UsrCP': ['required', 'zip'],
	'UsrVille': ['required'],
	'UsrParent,UsrGrandParent,UsrAutre': ['required'],
	'UsrMagasin': ['required'],
	'UsrCarteFamilleOui,UsrCarteFamilleNon': ['required'],
	'UsrCarteClubOui,UsrCarteClubNon': ['required'],
	'UsrClubNm': ['carteClub'],
	'UsrNbEnfant': ['required'],
	'boy1,girl1': [],
	'EnfantDateJourAnniv1': [],
	'EnfantDateMoisAnniv1': [],
	'EnfantDateYearAnniv1': [],
	'boy2,girl2': [],
	'EnfantDateJourAnniv2': [],
	'EnfantDateMoisAnniv2': [],
	'EnfantDateYearAnniv2': [],
	'boy3,girl3': [],
	'EnfantDateJourAnniv3': [],
	'EnfantDateMoisAnniv3': [],
	'EnfantDateYearAnniv3': [],
	'boy4,girl4': [],
	'EnfantDateJourAnniv4': [],
	'EnfantDateMoisAnniv4': [],
	'EnfantDateYearAnniv4': [],
	'UsrNbEnfantFutur': ['required'],
	'boyFutur1,girlFutur1': [],
	'EnfantDateJourAnnivFutur1': [],
	'EnfantDateMoisAnnivFutur1': [],
	'EnfantDateYearAnnivFutur1': [],
	'boyFutur2,girlFutur2': [],
	'EnfantDateJourAnnivFutur2': [],
	'EnfantDateMoisAnnivFutur2': [],
	'EnfantDateYearAnnivFutur2': [],
	'boyFutur3,girlFutur3': [],
	'EnfantDateJourAnnivFutur3': [],
	'EnfantDateMoisAnnivFutur3': [],
	'EnfantDateYearAnnivFutur3': [],
	'boyFutur4,girlFuturFutur4': [],
	'EnfantDateJourAnnivFutur4': [],
	'EnfantDateMoisAnnivFutur4': [],
	'EnfantDateYearAnnivFutur4': [],
	'FromNom': ['required'],
	'FromPrenom': ['required'],
	'FromEmail': ['required', 'email'],
	'ToEmail1': ['required', 'email'],
	'ToEmail2': ['email'],
	'ToEmail3': ['email'],
	'UsrSuppression': ['required']
};

// conditions de validation
DATAS.conditions = {
	'UsrEmailConfirm': {
		field: 'UsrEmailNew',
		verif: 'required',
		checks: ['required', 'confirm']
	},
	'UsrPasswordConfirm': {
		field: 'UsrPasswordNew',
		verif: 'required',
		checks: ['required', 'confirm']
	},
	'boy1,girl1': { // attention, tous les IDs dans le cas d'un groupe radio (s�par�s par une virgule, sans espace)
		field: 'EnfantPrenom1',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateJourAnniv1': {
		field: 'EnfantPrenom1',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateMoisAnniv1': {
		field: 'EnfantPrenom1',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateYearAnniv1': {
		field: 'EnfantPrenom1',
		verif: 'required',
		checks: ['required']
	},
	'boy2,girl2': { // attention, tous les IDs dans le cas d'un groupe radio (s�par�s par une virgule, sans espace)
		field: 'EnfantPrenom2',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateJourAnniv2': {
		field: 'EnfantPrenom2',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateMoisAnniv2': {
		field: 'EnfantPrenom2',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateYearAnniv2': {
		field: 'EnfantPrenom2',
		verif: 'required',
		checks: ['required']
	},
	'boy3,girl3': { // attention, tous les IDs dans le cas d'un groupe radio (s�par�s par une virgule, sans espace)
		field: 'EnfantPrenom3',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateJourAnniv3': {
		field: 'EnfantPrenom3',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateMoisAnniv3': {
		field: 'EnfantPrenom3',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateYearAnniv3': {
		field: 'EnfantPrenom3',
		verif: 'required',
		checks: ['required']
	},
	'boy4,girl4': { // attention, tous les IDs dans le cas d'un groupe radio (s�par�s par une virgule, sans espace)
		field: 'EnfantPrenom4',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateJourAnniv4': {
		field: 'EnfantPrenom4',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateMoisAnniv4': {
		field: 'EnfantPrenom4',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateYearAnniv4': {
		field: 'EnfantPrenom4',
		verif: 'required',
		checks: ['required']
	},
	
	'boyFutur1,girlFutur1': { // attention, tous les IDs dans le cas d'un groupe radio (s�par�s par une virgule, sans espace)
		field: 'EnfantPrenomFutur1',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateJourAnnivFutur1': {
		field: 'EnfantPrenomFutur1',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateMoisAnnivFutur1': {
		field: 'EnfantPrenomFutur1',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateYearAnnivFutur1': {
		field: 'EnfantPrenomFutur1',
		verif: 'required',
		checks: ['required']
	},
	'boyFutur2,girlFutur2': { // attention, tous les IDs dans le cas d'un groupe radio (s�par�s par une virgule, sans espace)
		field: 'EnfantPrenomFutur2',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateJourAnnivFutur2': {
		field: 'EnfantPrenomFutur2',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateMoisAnnivFutur2': {
		field: 'EnfantPrenomFutur2',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateYearAnnivFutur2': {
		field: 'EnfantPrenomFutur2',
		verif: 'required',
		checks: ['required']
	},
	'boyFutur3,girlFutur3': { // attention, tous les IDs dans le cas d'un groupe radio (s�par�s par une virgule, sans espace)
		field: 'EnfantPrenomFutur3',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateJourAnnivFutur3': {
		field: 'EnfantPrenomFutur3',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateMoisAnnivFutur3': {
		field: 'EnfantPrenomFutur3',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateYearAnnivFutur3': {
		field: 'EnfantPrenomFutur3',
		verif: 'required',
		checks: ['required']
	},
	'boyFutur4,girlFutur4': { // attention, tous les IDs dans le cas d'un groupe radio (s�par�s par une virgule, sans espace)
		field: 'EnfantPrenomFutur4',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateJourAnnivFutur4': {
		field: 'EnfantPrenomFutur4',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateMoisAnnivFutur4': {
		field: 'EnfantPrenomFutur4',
		verif: 'required',
		checks: ['required']
	},
	'EnfantDateYearAnnivFutur4': {
		field: 'EnfantPrenomFutur4',
		verif: 'required',
		checks: ['required']
	}
};

// les contr�les pour les champs ajout�s dynamiquement / dont on ne connait pas le nombre
DATAS.addFieldsChecks = {
	'LCNom': ['required'],
	'LCMDP': ['required', 'password'],
	'LNNom': ['required'],
	'LNMDP': ['required', 'password']
}

// ouverture des popups
DATAS.popups = {
	TRU: {
		'default': 'width=600,height=450,top=0,left=100,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes,status=yes',
		'ajouter-a-ma-liste.php': 'width=600,height=380,top=0,left=100,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes,status=yes',
		'rappel-mot-de-passe.php': 'width=600,height=450,top=0,left=100,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes,status=yes',
		'pop_envoyer_ami.php': 'width=600,height=500,top=0,left=100,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes,status=yes',
		'pop_credits.php': 'width=600,height=380,top=0,left=100,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes,status=yes',
		'desinscription.php': 'width=600,height=500,top=0,left=100,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes,status=yes',
		'reglement.php': 'width=600,height=500,top=0,left=100,toolbar=no,menubar=yes,location=no,resizable=yes,scrollbars=yes,status=yes'
	},
	BRU: {
		'default': 'width=650,height=400,top=0,left=100,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes,status=yes',
		'credits.php': 'width=650,height=350,top=0,left=100,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes,status=yes',
		'ajouter-a-ma-liste.php': 'width=650,height=320,top=0,left=100,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes,status=yes',
		'rappel-mot-de-passe.php': 'width=650,height=350,top=0,left=100,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes,status=yes',
		'pop_envoyer_ami.php': 'width=650,height=650,top=0,left=100,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes,status=yes'
	}
};
