function setBulleHeight(h)
{
	$('bulle').style.height = Math.round(h) + "px";
}

function setProchainDossierHeight(h)
{
	$('prochain_dossierswf').style.height = Math.round(h) + "px";
}

function displayTab(t)
{
	
   var tab = 'ajaxcontent_' + t;
	if ($('ajaxcontent_info') &&  ( $('ajaxcontent_info').style.display == 'block' || $('ajaxcontent_info').style.display == '' )  && 'ajaxcontent_info' != tab)
	{
	  Element.removeClassName($('en_ligne_info').parentNode, "selected");
	  new Effect.BlindUp('ajaxcontent_info', { duration: .4 });
	}
	if ($('ajaxcontent_fr') &&  ( $('ajaxcontent_fr').style.display == 'block' || $('ajaxcontent_fr').style.display == '' )  && 'ajaxcontent_fr' != tab)
	{
	  Element.removeClassName($('en_ligne_fr').parentNode, "selected");
	  new Effect.BlindUp('ajaxcontent_fr', { duration: .4 });
	}
	if ($('ajaxcontent_net') &&  ( $('ajaxcontent_net').style.display == 'block' || $('ajaxcontent_net').style.display == '' )  && 'ajaxcontent_net' != tab)
	{
	  Element.removeClassName($('en_ligne_net').parentNode, "selected");
	  new Effect.BlindUp('ajaxcontent_net', { duration: .4 });
	}
	if ($('ajaxcontent_org') &&  ( $('ajaxcontent_org').style.display == 'block' || $('ajaxcontent_org').style.display === '' )  && 'ajaxcontent_org' != tab)
	{
	  Element.removeClassName($('en_ligne_org').parentNode, "selected");
	  new Effect.BlindUp('ajaxcontent_org', { duration: .4 });
	}
	if ($('ajaxcontent_eu') &&  ( $('ajaxcontent_eu').style.display == 'block' || $('ajaxcontent_eu').style.display === '' )  && 'ajaxcontent_eu' != tab)
	{
	  Element.removeClassName($('en_ligne_eu').parentNode, "selected");
	  new Effect.BlindUp('ajaxcontent_eu', { duration: .4 });
	}
	if ($('ajaxcontent_' + t) && $(tab).style.display != '' )
	{		
	  Element.addClassName($('en_ligne_' + t).parentNode, "selected");
	  new Effect.BlindDown(tab, { duration: .8 });
	}
}

function setDivVideoSize (w,h)
{
  //alert( Window.ModalBox + "   " + parent.ModalBox + "  ");
  //window.ModalBox.resize( 500,400 );
  //alert( "SET DIV VIDEO SIZE OK "  +w +"  " + h + "  " +$('MB_wrapper').style.width + "  " + $('MB_wrapper').style.height+ $('MB_wrapper').style.left  ) ;
  var newLeft = parseInt($('MB_wrapper').style.left) +  (parseInt(parseInt($('MB_wrapper').style.width) - parseInt(w))/2) ;
  $('MB_wrapper').style.left =  parseInt(newLeft) +"px";

  scalewidth = 100 * (parseInt(w) + 30) / parseInt($('MB_wrapper').style.width);
  scaleheight = ( 100 * (parseInt(h) + 50) / parseInt($('MB_wrapper').style.height) ) ;

  new Effect.Scale('MB_window', scalewidth,{ duration: .5,scaleY:false, scaleContent:false});
  new Effect.Scale('MB_window', scaleheight,{ duration: .5,scaleX:false, scaleContent:false});

  $('MB_content').style.width = w  +"px";
  $('MB_content').style.height = h +"px";

  /*$('video').style.width = w+"px";*/
  $('video').style.height = h+"px";

  //alert( "SET DIV VIDEO SIZE OK "  +w +"  " + h + "  " +$('MB_wrapper').style.width + "  " + $('MB_wrapper').style.height + $('MB_wrapper').style.left ) ;

  
}

function HideModalbox ()
{
	alert('ok') ;
	Modalbox.hide() ;  
}

