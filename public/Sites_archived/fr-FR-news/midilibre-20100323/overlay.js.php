
/***************************/
//@Author: Adrian "yEnS" Mato Gondelle
//@website: www.yensdesign.com
//@email: yensamg@gmail.com
//@license: Feel free to use it, but keep this credits please!					
/***************************/

//SETTING UP OUR POPUP
//0 means disabled; 1 means enabled;
var popupStatus = 0;

//loading popup with jQuery magic!
function loadPopup(){
	//loads popup only if it is disabled
	if(popupStatus==0){
		$("#backgroundPopup").css({
			"opacity": "0.7"
		});
		$("#backgroundPopup").fadeIn(1000);
		$("#popupOverlay").slideDown(2000);
		popupStatus = 1;
	}
}

//disabling popup with jQuery magic!
function disablePopup(){
	//disables popup only if it is enabled
	if(popupStatus==1){
		$("#backgroundPopup").fadeOut("slow");
		$("#popupOverlay").fadeOut("slow");
		popupStatus = 0;
		//showSpecial();
	}
}

//centering popup
function centerPopup(){
	//request data for centering
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $("#popupOverlay").height();
	var popupWidth = $("#popupOverlay").width();
	
	//centering
	$("#popupOverlay").css({
		"position": "absolute",
		"top": windowHeight/2-popupHeight/2,
		"left": windowWidth/2-popupWidth/2
	});
	//only need force for IE6
	
	$("#backgroundPopup").css({
		"height": windowHeight
	});
	
}

function centerPopupTop(){
	//request data for centering
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	
	var popupWidth = $("#popupOverlay").width();
	
	//centering
	$("#popupOverlay").css({
		"position": "absolute",
		"top": document.documentElement.scrollTop,
		"left": windowWidth/2-popupWidth/2
	});
	//only need force for IE6
	
	$("#backgroundPopup").css({
		"height": windowHeight
	});
	
}


//CONTROLLING EVENTS IN jQuery
$(document).ready(function(){
		
	//CLOSING POPUP
	//Click the x event!
	$("#popupOverlayClose").click(function(){
		disablePopup();
	});
	//Click out event!
//	$("#backgroundPopup").click(function(){
//		disablePopup();
//	});
	//Press Escape event!
	$(document).keypress(function(e){
		if(e.keyCode==27 && popupStatus==1){
			disablePopup();
		}
	});

});

/***********************************/
/**** Ajout pour la gestion Overlay    */
/**** Eric SARDA 2009 / 03 / 19        */

/**** Modification text->xml   */
/**** Frederic PERCHET 2009 / 04 / 15  */
/***********************************/

viewVideoXml = function(sig) 
{
	$.ajax({
			url: '/kewego/infoOverlay.php',
			type: 'POST',
			data: "sig="+sig,
			dataType : 'xml',
			error: function(){
				alert('Une erreur interne est survenue. Merci de retenter ultérieurement');
			},
			success: function(xmlDoc){	
				//alert(xmlDoc);
				$("#dyn_titre").html(xmlDoc.getElementsByTagName("titre")[0].childNodes[0].nodeValue);
				$("#dyn_descriptif").html(xmlDoc.getElementsByTagName("descriptif")[0].childNodes[0].nodeValue);
				$("#dyn_descriptif_full").html(xmlDoc.getElementsByTagName("descriptifFull")[0].childNodes[0].nodeValue);
				$("#player").html(xmlDoc.getElementsByTagName("playerSource")[0].childNodes[0].nodeValue);						
				$("#dyn_keywords").html(xmlDoc.getElementsByTagName("keywords")[0].childNodes[0].nodeValue);			
				xt_rm("video",27,xmlDoc.getElementsByTagName("categorie")[0].childNodes[0].nodeValue,"play",null,null,xmlDoc.getElementsByTagName("duree")[0].childNodes[0].nodeValue,0,1,1,"int","clip",0,1);
			}	
		});
}

testFred = function ()
{
	$.ajax({
			url: 'http://www.journauxdumidi.com/testFred/ajax.php',
			type: 'POST',
			dataType : 'html',
			error: function(){
				alert('Une erreur interne est survenue. Merci de retenter ultérieurement');
			},
			success: function(xmlDoc){	
				//alert(xmlDoc);
				$("#backtest").html(xmlDoc);			
			}	
		});
}

viewThumbnail = function(pageEC,pageMax) {

	for (p=1;p<=pageMax;p++) {
		if (p==pageEC) {
			for (i=(p-1)*4+1;i<=p*4;i++) {
				myVideo = 'video'+i;
				//alert(myVideo);
				document.getElementById(myVideo).style.display='block';
			}
		
		}
		else {
			for (i=(p-1)*4+1;i<=p*4;i++) {
				myVideo = 'video'+i;
				//ale	rt(myVideo);
				document.getElementById(myVideo).style.display='none';
			}
	
		}

	}
}

var pageCourante= 1;
var pageMax 	= 3;

function videosSuivantes()
{
	p = pageCourante;
	
	for (i=(p-1)*4+1;i<=p*4;i++) {
		myVideo = 'video'+i;
		document.getElementById(myVideo).style.display='none';
	}

	if(p==pageMax)
		p=1;
	else
		p++;

	for (i=(p-1)*4+1;i<=p*4;i++) {
		myVideo = 'video'+i;
		document.getElementById(myVideo).style.display='block';
	}

	pageCourante=p;
}

function videosPrecedentes()
{
	p = pageCourante;
	
	for (i=(p-1)*4+1;i<=p*4;i++) {
		myVideo = 'video'+i;
		document.getElementById(myVideo).style.display='none';
	}

	if(p==1)
		p=pageMax;
	else
		p--;

	for (i=(p-1)*4+1;i<=p*4;i++) {
		myVideo = 'video'+i;
		document.getElementById(myVideo).style.display='block';
	}

	pageCourante=p;
}

function hideSpecial() {
	elements = document.getElementsByName("playersimple");
	
	for (i=0; i<elements.length; i++)
	{
		//elements.item(i).style.display = "none";
	}
}

function showSpecial() {

	elements = document.getElementsByName("playersimple");
		
	for (i=0; i<elements.length; i++)
	{
		//elements.item(i).style.display = "block";
	}
		
	$('#descriptif_full').css('display','none');
}

function updateRectangle(banner,href) {
	var bannerIMG = banner;
	var clickThruHref = href;
	
	var bannerLower = bannerIMG.toLowerCase();
	var bannerHTML = bannerIMG;
	
	if ("" == clickThruHref
		|| "DHTML" == clickThruHref
		|| "null" == clickThruHref
		|| "0" <bannerLower.indexOf(".swf"))
	{
		bannerHTML = '<HTML><IFRAME src="'
		+bannerIMG
		+ '" frameborder="0" scrolling="no" width="100%" height="100%"'
		+ ' marginwidth="0" marginheight="0"/></HTML>';
		

	}
	else if (0 == bannerHTML.indexOf("http"))
	{
		bannerHTML = '<HTML><CENTER><A HREF = "http://www.midilibre.com/kewego/js/'		+&#32;clickThruHref		+&#32;'" target="_blank"><IMG SRC="http://www.midilibre.com/kewego/js/'		+&#32;bannerIMG&#32;+&#32;'" border="0"></A></CENTER></HTML>';	
	}
	else 
	{
		bannerHTML = '<HTML>'+bannerIMG+'</HTML>';
	}

	var oDiv = document.getElementById('pubRectangle');
	oDiv.innerHTML = bannerHTML;
} 


function updateBanner(banner,href) {
	var bannerIMG = banner;
	var clickThruHref = href;
	
	var bannerLower = bannerIMG.toLowerCase();
	var bannerHTML = bannerIMG;
	
	if ("" == clickThruHref
		|| "DHTML" == clickThruHref
		|| "null" == clickThruHref
		|| "0" <bannerLower.indexOf(".swf"))
	{
		bannerHTML = '<HTML><IFRAME src="'
		+bannerIMG
		+ '" frameborder="0" scrolling="no" width="100%" height="100%"'
		+ ' marginwidth="0" marginheight="0"/></HTML>';
		

	}
	else if (0 == bannerHTML.indexOf("http"))
	{
		bannerHTML = '<HTML><CENTER><A HREF = "http://www.midilibre.com/kewego/js/'		+&#32;clickThruHref		+&#32;'" target="_blank"><IMG SRC="http://www.midilibre.com/kewego/js/'		+&#32;bannerIMG&#32;+&#32;'" border="0"></A></CENTER></HTML>';	
	}
	else 
	{
		bannerHTML = '<HTML>'+bannerIMG+'</HTML>';
	}

	var oDiv = document.getElementById('pubMegaBanner');
	oDiv.innerHTML = bannerHTML;
} 