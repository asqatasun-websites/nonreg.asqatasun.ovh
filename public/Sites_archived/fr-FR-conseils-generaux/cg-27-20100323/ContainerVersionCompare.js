/*
 * Copyright 2002-2006 Jahia Ltd
 *
 * Licensed under the JAHIA SUSTAINABLE SOFTWARE LICENSE (JSSL), 
 * Version 1.0 (the "License"), or (at your option) any later version; you may 
 * not use this file except in compliance with the License. You should have 
 * received a copy of the License along with this program; if not, you may obtain 
 * a copy of the License at 
 *
 *  http://www.jahia.org/license/
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */
/**
* Constructor
*/
function ContainerVersionCompare(revisions) {
  if ( arguments.length>0 ){
    this._revisions = revisions;
  } else {
    this._revisions = new Array();
  }
  this._count = this._revisions.length;
}

ContainerVersionCompare.prototype._handleRevisionSelect
  = function(theRevision) {
  if ( theRevision.checked ){
    if ( this._count == 0 ){
      this._revisions[0] = theRevision.id;
      this._count++;
    } else if ( this._count == 1 ){
      this._revisions[1] = theRevision.id;
      this._count++;
    } else if ( this._count == 2 ){
      var el = document.getElementById(this._revisions[1]); 
      if ( el ){
        el.checked = false;
      }
      this._revisions[1] = theRevision.id;
    }    
  } else {
    if ( this._count == 1 ){
      this._revisions[0] == null;  
    } else if ( this._count == 2 ){
      if ( this._revisions[0] == theRevision.id ){
        this._revisions[0] = this._revisions[1];
      } else if ( this._revisions[1] == theRevision.id ){
        this._revisions[1] = null;
      }     
    } 
    this._count--;
  }
}

ContainerVersionCompare.prototype._getRevisions
  = function() {  
  return this._revisions;
}