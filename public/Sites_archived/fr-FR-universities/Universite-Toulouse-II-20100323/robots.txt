User-agent: *
Disallow:/24263473/0/fiche___pagelibre/
Disallow:/jsp/fiche_pagelibre.jsp?STNAV=&RUBNAV=&CODE=24263473&LANGUE=0&RH=FauxAccueil
Disallow:/admin/
Disallow:/allemand/
Disallow:/annuaire/
Disallow:/arabe/
Disallow:/arts/
Disallow:/cdp/
Disallow:/ceram/
Disallow:/cerise/
Disallow:/cerpp/test/
Disallow:/cerpp/intranet/
Disallow:/cgi-bin/
Disallow:/cirus/
Disallow:/clle/ltc/stankovic/
Disallow:/competences/
Disallow:/diasporas/
Disallow:/ens.html
Disallow:/ergo98/
Disallow:/erreur.php
Disallow:/esav/gloub/
Disallow:/fi/calexam/
Disallow:/fi/exam/
Disallow:/forum/
Disallow:/forums/
Disallow:/fournisseurs/marches-publics/index.php?mode=s-identifier\*
Disallow:/fournisseurs/marches-publics/index.php?mode=documents_joints\*
Disallow:/HTDIG/
Disallow:/index.html
Disallow:/iut-figeac/index.html
Disallow:/langues-etrangeres/
Disallow:/langues-slaves/
Disallow:/ldcc/
Disallow:/letmod/catalogue-dossiers/
Disallow:/lmd/
Disallow:/lea/actium/
Disallow:/les-pas-de-la-paix/
Disallow:/lla/equipe/interne/
Disallow:/ltc/gestion/
Disallow:/mathinfoLMD/assoc_ismag/
Disallow:/mde/cache/
Disallow:/mde/test/
Disallow:/mosaique/
Disallow:/msh/territoires/activites/
Disallow:/multimedia/
Disallow:/N6,9/
Disallow:/pejm/
Disallow:/php/
Disallow:/planreussitelicence/
Disallow:/pole-fp/
Disallow:/polonais/
Disallow:/portugais/
Disallow:/rech/atelier-informatique/
Disallow:/siama/
Disallow:/soe/
Disallow:/stat/
Disallow:/ufr-psycho/gestpsy/
Disallow:/ums838/carto/
Disallow:/ums838/spip/
Disallow:/univ.html
Disallow:/utah/hautefeuille/
Disallow:/utah/archtest/
Disallow:/utah/rapport/

