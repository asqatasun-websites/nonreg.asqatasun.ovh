ns4 = (navigator.appName.indexOf("Netscape")+1) // test Netscape ou IE
//var nblay = 2;    // nombre total de calques
coords = new Array(nblay+1); // les positions de tous les calques
var lay;          // variable de gestion des calques
var pos;          // variable de position verticale des calques
var haut = 120;    // hauteur d'un calque
var esp = 1;      // espace inter-calque
var nom = "elt";  // pr�fixe des noms de calques
var speed = 30;   // vitesse de d�filement (plus chiffre petit, plus vite)
var pause = 2000; // temps de pause entre les calques (ms)
var cycle = 0;    // pour compter les cycles et pouvoir faire une pause entre chaque calque
var laytop = 1;   // calque le plus haut
var cpterebours;  // le compte � rebours

function init() // remplissage du vecteur contenant les positions y de tous les calques
{
	for( var x = 1; x <= nblay; x++ )
		coords[x] = (x-1)*(haut+esp);
}

function montee() // montee des calques
{	
	if( cycle < (haut + esp))
	{
		for( lay = 1; lay <= nblay; lay++ )
		{
			pos = coords[lay];
			
			if( pos > (haut*-1)) // si calque encore visible on le monte
			{
				deplace(lay, (pos-1));
				coords[lay] = pos-1;
			}
			else // sinon on le remet en bas de la pile
			{
				deplace(lay, ((nblay-1)*(haut+esp)+esp));
				coords[lay] = (nblay-1)*(haut+esp)+esp-1;
			}
		}
		cycle++;
		cpterebours = setTimeout("montee()",speed);
	}
	else // les calques sont tous mont�s � la position des calques pr�c�dents
	{
		cycle = 0;
		if( laytop < nblay ) // on met � jour le num�ro du calque le plus haut
			laytop++;
		else
			laytop = 1;
		cpterebours = setTimeout("montee()",pause);
	}
}

function deplace(numlay, y) // deplace le calque pass� en parametre de y pixels
{
	if( ns4 )
			{
			if (navigator.appVersion.indexOf("5.0")!=-1)
				{
				document.getElementById("elt"+numlay).style.top = y
				}
			else
				{
				eval("document.actualites.document."+nom+numlay+".top = "+y);
				}
			}
	else
		document.getElementById("elt"+numlay).style.top = y;
}

function precedent() // voir le calque pr�c�dent
{
	if( laytop == 1 )
		laytop = nblay;
	else
		laytop--;
	replace();
}

function suivant() // voir le calque suivant
{
	replace();	
	if( laytop == nblay )
		laytop = 1;
	else
		laytop++;

}

function replace() // repositionnement apr�s clic sur le bouton suivant ou pr�c�dent
{
	clearTimeout(cpterebours); // on stop le rappelle automatique de montee()
	var j = 0;
	for( var k = laytop; k <= nblay; k++ )
	{
		deplace(k, (j*(esp+haut)));
		coords[k] = j*(esp+haut);
		j++;
	}
	if( laytop != 1 )
		for( var k = 1; k < laytop; k++ )
		{
			deplace(k, (j*(esp+haut)));
			coords[k] = j*(esp+haut);
			j++;
		}
	cycle = 0;
	cpterebours = setTimeout("repart()",pause);
}

function repart() // permet la pause sur Netscape apr�s un clic sur suivant ou pr�c�dent
{
	montee();
}
