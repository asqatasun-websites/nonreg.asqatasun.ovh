/******************************************************************************
* smeFontSize.asp.asp                                                                
*******************************************************************************

*******************************************************************************
*                                                                             *
* Copyright 2000-2006                                                         *
*                                                                             *
******************************************************************************/

function smeFontSizeChange(strChange) 
{
	// on regarde si on a deja un cookie :
	var fontSizeIncrement;
	if (strChange == "normal")
		fontSizeIncrement = 0;
    else
    {
        fontSizeIncrement = parseInt(smeGetCurrentIncrementCookie(), 10);
        if (strChange == "plus")
		    fontSizeIncrement ++;
	    else if (strChange == "minus")
		    fontSizeIncrement --;
	}
    // on met a jour le cookie
    smeSetCurrentIncrementCookie(fontSizeIncrement);

	// on modifie explicitement la taille de la police
	smeChangeBodyFontSize(fontSizeIncrement);
}
addLoadAction(smeFontSizeChange);


function smeChangeBodyFontSize(fontSizeIncrement)
{
    var normalSize = smeGetNormalSizeCookie();
    
    if (fontSizeIncrement==0)
    {
        document.body.style.fontSize = normalSize;
    }
    else
    {
        var newFontSize;
        
        if (normalSize.indexOf("%") > 0)
        {
            newFontSize = parseFloat(normalSize.substr(0, normalSize.indexOf("%"))) + (12*fontSizeIncrement) ;
            document.body.style.fontSize = newFontSize+"%";
        }
        else if (normalSize.indexOf("em") > 0)
        {
            newFontSize = parseFloat(normalSize.substr(0, normalSize.indexOf("em"))) + (1*fontSizeIncrement) ;
            document.body.style.fontSize = newFontSize+"em";
        }
        else if (normalSize.indexOf("px") > 0)
        {
            newFontSize = parseFloat(normalSize.substr(0, normalSize.indexOf("px"))) + (2*fontSizeIncrement) ;
            document.body.style.fontSize = newFontSize+"px";
        }
        else if (normalSize.indexOf("pt") > 0)
        {
            newFontSize = parseFloat(normalSize.substr(0, normalSize.indexOf("pt"))) + (1*fontSizeIncrement) ;
            document.body.style.fontSize = newFontSize+"pt";
        }
    }
	
    
}

function smeGetCurrentIncrementCookie()
{
	var offset = document.cookie.indexOf("smeCurrentIncrement=");
	if (offset != -1)
	{
		offset += 20; // "smeCurrentIncrement=" length
		var end = document.cookie.indexOf(";", offset);
		if (end == -1)
			end = document.cookie.length;
		return document.cookie.substring(offset, end);
	}
	else
	{
		// initialise le cookie pour conserver la taille "normale"
	    var objBody = new YAHOO.util.Element(document.body);
        var currentBodyFontSize = objBody.getStyle("font-size");
		smeSetNormalSizeCookie(currentBodyFontSize);
		
		return 0;
	}
}

function smeSetCurrentIncrementCookie(fontSizeIncrement)
{
    // on met a jour le cookie
	var expires = new Date();
	expires.setTime(expires.getTime() + (86400 * 1000)); /*24h*/
	document.cookie = "smeCurrentIncrement=" + fontSizeIncrement + "; expires=" + expires.toGMTString() +  "; path=/";
}

function smeGetNormalSizeCookie()
{
    var offset = document.cookie.indexOf("smeNormalSize=");
	if (offset != -1)
	{
		offset += 14; // "smeNormalSize=" length
		var end = document.cookie.indexOf(";", offset);
		if (end == -1)
			end = document.cookie.length;
		return document.cookie.substring(offset, end);
	}
	return;
}

function smeSetNormalSizeCookie(normalSize)
{
    // on met a jour le cookie
	var expires = new Date();
	expires.setTime(expires.getTime() + (86400 * 1000)); /*24h*/
	document.cookie = "smeNormalSize=" + normalSize + "; expires=" + expires.toGMTString() +  "; path=/";
}
