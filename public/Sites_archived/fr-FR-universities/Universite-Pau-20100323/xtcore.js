<!--
//-- Copyright 2008 XiTi, All Rights Reserved.
//-- XiTi Tag 3.1.001
xtdm = ".univ-pau.fr";				//cookie domain (.xxxxxx.com)	
xt_idprior = "0";		//remanence priority - 0:1st campaign;1:last campaign
xtanrm = 3650 ; 		//xtan remanence (number of days)
xttdrm = new Array;;	//remanences (number of days)
xttdrm["sec"]="20";		
xttdrm["rss"]="20";		
xttdrm["epr"]="20";		
xttdrm["erec"]="20";		
xttdrm["adi"]="20";		
xttdrm["adc"]="20";
xttdrm["al"]="20";
xttdrm["es"]="20";
xttdrm["ad"]="20";

//do not modify below
var xtdr = 30;
var xw = window;
var xd = document;
xtdm = (xw.xtdmc!=null&&xw.xtdmc!='')? ";domain="+xw.xtdmc : (xtdm!='') ? ";domain="+xw.xtdm : "" ;
xtnv = (xw.xtnv!=null) ? xw.xtnv : xd ;
xtsd = (xw.xtsd!=null) ? xw.xtsd : "http://log" ;
xtsite = (xw.xtsite!=null) ? xw.xtsite : 0;
xtn2 = (xw.xtn2!=null) ? '&s2='+xw.xtn2 : '';
xtp = (xw.xtpage!=null) ? xw.xtpage : "";
xto_force = ((xw.xto_force!=null)&&(xw.xto_force!="")) ? xw.xto_force : null;
xtrd = (xtsite=="redirect") ? true : false;
xtdi = ((xw.xtdi!=null)&&(xw.xtdi!="")) ? "&di=" + xw.xtdi : "";
xtidp = ((xw.xtidp!=null)&&(xw.xtidp!="")) ? "&idpays=" + xw.xtidp : "";
xtidprov = ((xw.xtidprov!=null)&&(xw.xtidprov!="")) ? "&idprov=" + xw.xtidprov : "";
xtm = (xw.xtparam!=null) ? xw.xtparam : "";	
xtclzone = (xw.scriptOnClickZone!=null) ? xw.scriptOnClickZone : 0;
xtoid = (xw.xt_orderid!=null) ? xw.xt_orderid : "";
xtbask = (xw.xtbask!=null) ? xw.xtbask : "";	
xtcart = (xw.xtcart!=null) ? xw.xtcart : "";

xtrmt = ((xw.roimt!=null)&&(xw.roimt!="")&&(xtm.indexOf("&roimt",0)<0)) ? "&roimt=" + xw.roimt : "";	
xter = ((xw.xterr!=null)&&(xw.xterr!="")&&(xtm.indexOf("&err",0)<0)) ? "&err=" + xw.xterr : "";
xtmc = ((xw.xtmc!=null)&&(xw.xtmc!="")&&(xtm.indexOf("&mc",0)<0)) ? "&mc=" + xw.xtmc : "";
xtac = ((xw.xtac!=null)&&(xw.xtac!="")&&(xtm.indexOf("&ac",0)<0)) ? "&ac=" + xw.xtac : "";
xtan = ((xw.xtan!=null)&&(xw.xtan!="")&&(xtm.indexOf("&an",0)<0)) ? "&an=" + xw.xtan : "";
xtnp = ((xw.xtnp!=null)&&(xw.xtnp!="")&&(xtm.indexOf("&np",0)<0)) ? "&np=" + xw.xtnp : "";
xtprm = ((xw.xtprm!=null)&&(xtm.indexOf("&x",0)<0)) ? xw.xtprm : "";	
xtm += xtrmt+xter+xtmc+xtac+xtan+xtnp+xtprm;	
try {xt_rfr = top.document.referrer;}
catch(e) {xt_rfr = xtnv.referrer; }
xts = screen;	
var xtdate = new Date();
var xtheureh = xtdate.getTime() / (1000*3600);

function xtclURL(ch)
{return ch.replace(/%3C/g,'<').replace(/%3E/g,'>').replace(/[<>]/g,'');}

function Getxtorcookie(nom,xtenc)
{
	xtenc = ((xtenc != null)&&(xtenc!=undefined)) ? xtenc : "0";
	var arg = nom + "=";
	var i = 0 ;
	while (i<xd.cookie.length)
	{
		var j = i + arg.length;
		if (xd.cookie.substring(i,j) == arg) {return valeurxtorcook(j,xtenc);}
		i = xd.cookie.indexOf(" ",i) + 1;
		if (i==0) {break;}
	}
	return null;}

function valeurxtorcook(index,xtenc)
{
		var fin = xd.cookie.indexOf(";",index);
		if (fin==-1) {fin=xd.cookie.length;};
		if (xtenc!="1"){return unescape(xtclURL(xd.cookie.substring(index,fin)));}
		else{return xtclURL(xd.cookie.substring(index,fin));}
}

function wcookie(p1,p2,p3,p4,fmt)
{
	p2 = (fmt==0) ? p2 : escape(p2);
	xd.cookie = p1 + "=" + p2 + ";expires=" + p3.toGMTString() + " ;path=/" + p4;
}	
function recupxtor(param,chba)
{
		if ((chba==null)||(chba==undefined)) 
		{var xturl = xtclURL(xtnv.location.href.toLowerCase().replace(/%3d/g,'='));}
		else
		{var xturl = chba;}	
		xtpos = xturl.indexOf(param+"=");
		if (xtpos > 0)
		{
			chq = xturl.substring(1, xturl.length);
			mq = chq.substring(chq.indexOf(param+"="), chq.length);
			pos3 = mq.indexOf("&");
			if (pos3 == -1) pos3 = mq.indexOf("%26");
			if (pos3 == -1) pos3 = mq.length;
			return mq.substring(mq.indexOf("=")+1, pos3);
		}
		else{  return null; }
}

function xt_med(type,section,page,x1,x2,x3,x4,x5)
{	
	if (xtclzone==0){xt_img = new Image();
	var xtdmed = new Date();
	xt_ajout = (type=='F') ? '' : (type=='M') ? '&a='+x1+'&m1='+x2+'&m2='+x3+'&m3='+x4+'&m4='+x5 : '&clic='+x1;
	Xt_im = xtsd+'.xiti.com/hit.xiti?s='+xtsite+'&s2='+section;
	Xt_im += '&p='+page+xt_ajout+'&hl=' + xtdmed.getHours() + 'x' + xtdmed.getMinutes() + 'x' + xtdmed.getSeconds();
	if(parseFloat(navigator.appVersion)>=4)
	{Xt_im += '&r=' + xts.width + 'x' + xts.height + 'x' + xts.pixelDepth + 'x' + xts.colorDepth;}
	xt_img.src = Xt_im;
	if ((x2 != null)&&(x2!=undefined)&&(type=='C'))
	{ if ((x3=='')||(x3==null)) { document.location = x2} else {xfen = window.open(x2,'xfen',''); xfen.focus();}}
	else
	{return;}	
	}
}
function f_nb(a)
{
	a = a-Math.floor(a/100)*100;
	if (a<10){return "0"+a;}else{return a;}
}
xtnbPts=0;
xtbask="";
function xt_addProduct(rg,pdt,qtt,unp,dsc,dscc) {
	xtnbPts++;
	xtbask+="&pdt"+xtnbPts+"=";
	xtbask+=((rg!=null)&&(rg!="")&&(rg!=undefined)) ? rg+"::" : "";
	xtbask+=((pdt!=null)&&(pdt!="")&&(pdt!=undefined)) ? pdt : "";
	xtbask+=((qtt!=null)&&(qtt!="")&&(qtt!=undefined)) ? "&qte"+xtnbPts+"="+qtt : "";
	xtbask+=((unp!=null)&&(unp!="")&&(unp!=undefined)) ? "&mt"+xtnbPts+"="+unp : "";
	xtbask+=((dsc!=null)&&(dsc!="")&&(dsc!=undefined)) ? "&dsc"+xtnbPts+"="+dsc: "";
	xtbask+=((dscc!=null)&&(dscc!="")&&(dscc!=undefined)) ? "&pcode"+xtnbPts+"="+dscc : "";
}
try {xt_cart();}
catch(e) {xtbask=""; }
function xt_ParseUrl(hit,xtch,xtrefP,thit) {
	var tabUrl=new Array;
	if ((xtch.length>0)&&(xtoid!="")) {
		var xtlg=1600-xtrefP.length;
		var i=0;
		var j=0;
		var xtch_prec="";
		while (xtch.length>xtlg && xtch_prec!=xtch) {
			xtch_prec=xtch;var xsep="&pdt";
			if (xtch.lastIndexOf(xsep,xtlg)<=0 ) {xsep="&";}
			tabUrl[i]=xtch.substring(0,xtch.lastIndexOf(xsep,xtlg));
			xtch=xtch.substring(xtch.lastIndexOf(xsep,xtlg),xtch.length);
			i++;xtlg=1600;
		}
		tabUrl[i]=xtch;
		for (j=0 ; j <= i ; j++){
			if (i>0){tabUrl[j]+="&idhit="+(j+1)+"-"+(i+1)+"-"+xtsite+"-"+xtoid+"-"+xtcart;}
			if (j>0) {tabUrl[j]="s="+xtsite+"&cmd="+xtoid+"&idcart="+xtcart+tabUrl[j];}
			else{tabUrl[j]+=xtrefP;}
			if ((thit=='')||(thit==null)){xd.write('<img width="1" height="1" src="'+hit+tabUrl[j]+'">');}
			else {
				xt_img = new Image();
				xt_img.src = hit+tabUrl[j];}
		}
	}
}

if((xtsite!=0)||(xtrd))
{
	xtpm = "xtor" + xtsite  ;
	xtpmd = "xtdate" + xtsite  ;
	xtpmc = "xtocl" + xtsite  ;
	xtpan = "xtan" + xtsite ;
	xtpant = "xtant" + xtsite ;

	xtourl=recupxtor("xtor");xtdtgo=recupxtor("xtdt");xtourl_rf=recupxtor("xtref");xtanurl=recupxtor("xtan");xtantag=recupxtor("an",xtm);xtactag=recupxtor("ac",xtm);
	xtocl= (Getxtorcookie(xtpmc)!=null)?Getxtorcookie(xtpmc):"$";xtord = (Getxtorcookie("xtgo")=="0") ? Getxtorcookie("xtord") : null;xtgord = (Getxtorcookie("xtgo")!=null) ? Getxtorcookie("xtgo") : "0";
	xtvrn= (Getxtorcookie("xtvrn")!=null)?Getxtorcookie("xtvrn"):"$";xtgmt=xtdate.getTime()/60000;
	xtgo = (xtdtgo!=null) ? (((xtgmt-xtdtgo)<30)&&(xtgmt-xtdtgo)>=0) ? "2" : "1" : xtgord;
	xtpgt = (xtgord=="1") ? "&pgt="+Getxtorcookie("xtord") : ((xtgo=="1")&&(xtourl!=null)) ? "&pgt="+xtourl : "";
	xto = (xto_force!=null) ? xto_force : ((xtourl!=null)&&(xtgo=="0")) ? xtourl : (!xtrd) ? xtord : null;
	xto = ((xtocl.indexOf('$'+escape(xto)+'$')<0)||(xtocl=="$")) ? xto : null;
	xtock = (xtgo=="0") ? xto : (xtgord=="2") ? Getxtorcookie("xtord") : (xtgo=="2") ? xtourl : null;	
	if (xtock!=null){tmpxto=xtock.substring(0,xtock.indexOf("-"));xtdrm=xttdrm[tmpxto];}else{xtdrm="1";}		
	if((xtanurl==null)&&(!xtrd)){xtanurl=Getxtorcookie("xtanrd");}
	xtanc = Getxtorcookie(xtpan);xtanct = Getxtorcookie(xtpant);
	var xtxp = new Date();var xtxpan = new Date();var xtxpcl = new Date();
	if (!xtrd) {xtxp.setTime(xtxp.getTime()+(xtdrm*24*3600*1000));}
	else {xtxp.setTime(xtxp.getTime()+(xtdr*1000));}
	xtxpcl.setTime(xtxpcl.getTime()+1800000);xtxpan.setTime(xtxpan.getTime()+(xtanrm*24*3600*1000));
	xtanpos = (xtanurl!=null) ? xtanurl.indexOf("-") : 0;
	xtan2 = (xtantag!=null) ? "" : ((xtanurl!=null)&&(xtanpos>0)) ? "&ac="+xtanurl.substring(0,xtanpos)+"&ant=0&an="+xtanurl.substring(xtanpos+1,xtanurl.length) : (xtanc!=null) ? "&anc="+xtanc+"&anct="+xtanct : "";
	xplus = (xtvrn.indexOf('$'+xtsite+'$')<0)?"&vrn=1":"";
	if(xplus!=""){wcookie("xtvrn",xtvrn+xtsite+'$',xtxpan,xtdm,0);}
	xplus += (xto==null)?"":"&xto="+xto;
	xplus += xtan2+xtpgt;
	if (xtantag!=null){wcookie(xtpan,xtactag+"-"+xtantag,xtxpan,xtdm,1);wcookie(xtpant,"1",xtxpan,xtdm,1);}
	else{if ((xtanurl!=null)&&(xtanct!="1")){wcookie(xtpan,xtanurl,xtxpan,xtdm,1);wcookie(xtpant,"0",xtxpan,xtdm,1);}}
	xtor = Getxtorcookie(xtpm);
	xtor_duree = Getxtorcookie(xtpmd);
	xtdate2 = (xtor_duree!=null) ? new Date(xtor_duree) : new Date();
	xtheureavant = xtdate2.getTime() / (1000*3600);
	xtecart = (Math.floor(xtheureh - xtheureavant)>=0) ? Math.floor(xtheureh - xtheureavant) : 0;
	xplus += (xtor==null) ? "" : "&xtor="+xtor+"&roinbh="+xtecart;
	xplus2 = "";
    Xt_r = (xtourl_rf!=null) ? xtourl_rf.replace(/[<>]/g, '') : Getxtorcookie('xtref') ;
	if(Xt_r==null)	{Xt_r = xt_rfr.replace(/[<>]/g, '');}
	if (!xtrd)
	{
		if ((xtock!=null)&&((xtocl.indexOf('$'+escape(xtock)+'$')<0)||(xtocl=="$"))){wcookie(xtpmc,xtocl+xtock+'$',xtxpcl,xtdm,0)}
		xplus2 +=navigator.javaEnabled()?"&jv=1":"&jv=0";
		var xtnav = navigator.appName+" "+navigator.appVersion;
		var xtIE = (xtnav.indexOf('MSIE'));
		if (xtIE>=0) {xtvers = parseInt(xtnav.substr(xtIE+5));xtIE=true;}
		else {xtvers = parseFloat(navigator.appVersion);xtIE=false;}
		var xtnet=(xtnav.indexOf('Netscape') >=0);
		var xtmac=(xtnav.indexOf('Mac') >=0);
		var xtOP=(navigator.userAgent.indexOf('Opera') >=0);
		if((xtIE)&&(xtvers >=5)&&(!xtmac)&&(!xtOP)&&(!xtrd))
 		{
   	 		xd.body.addBehavior("#default#clientCaps");xtconn = '&cn=' + xd.body.connectionType;
    		xtconn += '&ul=' + xd.body.UserLanguage;xd.body.addBehavior("#default#homePage");
    		xthome = (xd.body.isHomePage(location.href))? '&hm=1': '&hm=0';
    		xtresr = '&re='+xd.body.offsetWidth+'x'+xd.body.offsetHeight;
 		}
		else
 		{xtconn = ''; xthome='';if(xtvers >=5){xtresr = '&re='+xw.innerWidth+'x'+xw.innerHeight;}else{xtresr =''};}
		if((xtnet)&&(xtvers >=4)||(xtOP)){var xtlang = '&lng=' + navigator.language;}
		else {if((xtIE)&&(xtvers >=4)&&(!xtOP)){var xtlang = '&lng=' +navigator.userLanguage;} else {xtlang = '';}}

		wcookie("xtord","",xtdate,xtdm,1);
		if (xtock!=null){	
			if (((xtor==null)&&(xt_idprior!="1"))||(xt_idprior=="1")){
				wcookie(xtpm,xtock,xtxp,xtdm,1);wcookie(xtpmd,xtdate,xtxp,xtdm,0);}}
		Xt_param = 's='+xtsite+xtn2+'&p='+xtp+'&hl='+xtdate.getHours() + 'x' + xtdate.getMinutes() + 'x' + xtdate.getSeconds();
		Xt_param += xtdi+xtidp+xtidprov+xplus+xtm+xtconn+xthome+xtlang;
		if (xtclzone>0) {
			xtidpg = f_nb(xtdate.getHours())+''+f_nb(xtdate.getMinutes())+''+f_nb(xtdate.getSeconds())+''+Math.floor(Math.random()*9999999);
			Xt_param+="&idp="+xtidpg;
		}
		xtvalCZ=Getxtorcookie('xtvalCZ',1);
		if (xtvalCZ!=null){Xt_param+=xtvalCZ;var xtdateo=new Date();xtdateo.setTime(xtdateo.getTime()-3600000);wcookie("xtvalCZ",xtvalCZ,xtdateo,xtdm,1);}
		Xt_id = xtsd+'.xiti.com/hit.xiti?';	
		if(xtvers >=4)
		{xplus2+='&r='+xts.width+'x'+xts.height+'x'+xts.pixelDepth+'x'+xts.colorDepth;}	
		Xt_param+=xplus2+xtresr+xtbask;
		Xt_i = Xt_id+Xt_param+'&ref='+Xt_r.replace(/&/g, '$');
		if (xw.xtbask==""){xd.write('<img width="1" height="1" src="'+Xt_i+'">');}
		else{xt_ParseUrl(Xt_id,Xt_param,'&ref='+Xt_r.replace(/&/g, '$'));}		
	}
	else
	{
		wcookie("xtgo",xtgo,xtxp,xtdm,1);
		if (xtourl!=null) {wcookie("xtord",xtourl,xtxp,xtdm,1);}
		if (xtanurl!=null) {wcookie("xtanrd",xtanurl,xtxp,xtdm,1);}
     	if (Xt_r!="")	{wcookie("xtref",Xt_r.replace(/&/g, '$'),xtxp,xtdm,0);}
		if(xw.xtloc!=null)	{xtnv.location=xw.xtloc;}
	}
}
//-->