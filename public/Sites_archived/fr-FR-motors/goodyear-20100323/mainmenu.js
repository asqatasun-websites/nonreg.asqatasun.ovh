MainMenu = {
	menus : {},
	bg : {},
	
	Init : function(){
		var nodes_top = document.getElementById("topmenu").getElementsByTagName("ul");
		var nodes_main = document.getElementById("mainmenu").getElementsByTagName("ul");
		
		var nodes = new Array()
		for(var i=0;i<nodes_main.length;i++) nodes.push(nodes_main[i]);
		for(var i=0;i<nodes_top.length;i++) nodes.push(nodes_top[i]);

		for(var i=0;i<nodes.length;i++){
			var li = nodes[i].parentNode;
			if(!li.getAttribute("id")) continue;
			this.menus[li.getAttribute("id")] = nodes[i];
			
			li.firstChild.onmouseover = function(){
				this.className = "active";
				MainMenu.showSubMenu(this.parentNode.getAttribute("id"));
			}
			li.firstChild.onmouseout = function(){MainMenu.startHiding()}
			
			//Init Submenu Links
			var links = nodes[i].getElementsByTagName("a");
			for(var j=0;j<links.length;j++){
				if(!links[j].innerHTML) links[j].style.visibility = "hidden";
				if(links[j].className != "noanim") links[j].onmouseover = function(){new Anim(this, "backgroundLeft", this.offsetWidth - 30, this.offsetWidth-15, 1, 7, 40);}
				else{
					var ul = links[j].parentNode.getElementsByTagName("ul")[0];
					if(ul)
					{
						var lists = ul.getElementsByTagName("li");

						for(var z=0;z<lists.length;z++){
							lists[z].onmouseout = function(){
								MainMenu.subsubmenu = this.parentNode;
								MainMenu.subsubtimer = setTimeout("MainMenu.hideSubSubMenu()", 100);
							}
							lists[z].onmouseover = function(){
								if(MainMenu.subsubmenu == this.parentNode) clearTimeout(MainMenu.subsubtimer);
							}
						}

						links[j].onmouseover = function(){
							this.style.display = "none";
							this.parentNode.getElementsByTagName("ul")[0].style.display = "block";
						}
					}
				}
			}
			
			//Set Interaction on menu
			nodes[i].onmouseover = function(){clearTimeout(MainMenu.timer);}
			nodes[i].onmouseout = function(){MainMenu.startHiding();}
			
			//Fix support for IE not having normal PNG support and the Link + AlphaImageLoader Bug
			if(document.all) this.bg[li.getAttribute("id")] = this.ieFixUL(nodes[i]);
		}
	},
	
	//IE Bug fix
	ieFixUL : function(node){
		var ImageFileName = node.currentStyle.backgroundImage.replace(/^url\(\"(.*)\"\)$/i, "$1");
		
		if(isIE50){
			node.style.background = "url(" + ImageFileName.replace(/\.png/, ".gif") + ")";
			return;
		}
		
		node.style.background = "none";
		var div = node.parentNode.appendChild(document.createElement("DIV"));
		div.style.filter = div.extrafilter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + ImageFileName.replace(/^.*(static.*)$/, "$1") + "', sizingMethod='image');";
		div.style.height = node.currentStyle.height;
		div.style.width = node.currentStyle.width;
		div.style.position = "absolute";
		div.style.margin = node.currentStyle.margin;
		div.style.zIndex = 1;
		div.style.display = "none";
		
		return div;
	},
	
	showSubMenu : function(id){
		clearTimeout(this.timer);
		if(this.current == id) return;
		if(this.current) this.hideSubMenu();
		
		Animate.fade(this.menus[id], 0);
		this.menus[id].style.display = "block";
		new Anim(this.menus[id], "fade", 0, 1, 0, 7, 10);
		
		//IE Bug fix
		if(document.all && this.bg[id]){
			Animate.fade(this.bg[id], 0);
			this.bg[id].style.display = "block";
			new Anim(this.bg[id], "fade", 0, this.bg[id].parentNode.parentNode.id == "topmenu" ? 0.85 : 0.85, 0, 7, 10);//, function(oHTML){oHTML.style.filter = oHTML.extrafilter});
		}
		
		this.current = id;
	},

	hideSubMenu : function(id){
		if(!id) id = this.current;
		clearTimeout(this.timer);
		
		new Anim(this.menus[id], "fade", 1, 0, 0, 5, 10, function(oHTML){oHTML.style.display = "none";});
		//IE Bug fix
		if(document.all) new Anim(this.bg[id], "fade", 0.48, 0, 0, 5, 10, function(oHTML){oHTML.style.display = "none";});
		
		if(document.getElementById(this.current)) document.getElementById(this.current).firstChild.className = "last";
		this.current = null;
	},
	
	hideSubSubMenu : function(){
		this.subsubmenu.style.display = "none";
		this.subsubmenu.parentNode.firstChild.style.display = "inline";
	},
	
	startHiding : function(){
		this.timer = setTimeout("MainMenu.hideSubMenu()", 500);
	}
}