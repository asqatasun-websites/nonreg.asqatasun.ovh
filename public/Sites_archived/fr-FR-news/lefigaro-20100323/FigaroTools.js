/**************************************************************************************************
 *
 * FONCTIONS DIVERSES
 *
 **************************************************************************************************/
/*
	AFOpen : Ouvre une Fen�tre
*/
function AFOpen(url,name,width,height) 
{
  var opt = "top=0,left=0,resizable=yes,toolbar=yes,scrollbars=yes,menubar=no,location=yes,statusbar=yes";
  opt += ",width=" + width + ",height=" + height;
  var win = window.open(url,name,opt).focus();
}
/*
	AFOpenFS : Ouvre une Fen�tre en Full Screen
*/
function AFOpenFS(url,name) 
{
  var opt = "top=0,left=0,resizable=yes,toolbar=no,scrollbars=auto,menubar=no,location=no,statusbar=yes";
  opt += ",width=" + screen.width + ",height=" + screen.height;
  name = new String('openfs');
  var win = window.open(url,name,opt).focus();
}

function FigaroPrint()
{
	url = document.location.href + '?mode=imprimer';
  	opt = "top=0,left=0,resizable=yes,toolbar=no,scrollbars=yes,menubar=yes,location=no,statusbar=no,width=600,height=700";
	window.open(url,'imprimer',opt).focus();
}


/**************************************************************************************************
 *
 * COOKIES
 *
 **************************************************************************************************/

function getCookieVal(offset) {
	var endstr=document.cookie.indexOf (";", offset);
	if (endstr==-1)
      		endstr=document.cookie.length;
	return unescape(document.cookie.substring(offset, endstr));
}

function SetCookie (name, value) {
	var argv=SetCookie.arguments;
	var argc=SetCookie.arguments.length;
	var path=(argc > 2) ? argv[2] : null;
	var expires=(argc > 3) ? argv[3] : null;
	var domain=(argc > 4) ? argv[4] : null;
	var secure=(argc > 5) ? argv[5] : false;
	document.cookie=name+"="+escape(value)+
		((expires==null) ? "" : ("; expires="+expires.toGMTString()))+
		((path==null) ? "" : ("; path="+path))+
		((domain==null) ? "" : ("; domain="+domain))+
		((secure==true) ? "; secure" : "");
}

function GetCookie (name) {
	var arg=name+"=";
	var alen=arg.length;
	var clen=document.cookie.length;
	var i=0;
	while (i<clen) {
		var j=i+alen;
		if (document.cookie.substring(i, j)==arg)
                        return getCookieVal (j);
                i=document.cookie.indexOf(" ",i)+1;
                        if (i==0) break;}
	return null;
}


/**************************************************************************************************
 *
 * ELEMENTS PAR ID (show/hide)
 *
 **************************************************************************************************/

function getByID(id)
{
	if (document.all)
	{
		return document.all(id) ;
	}
	if(document.getElementById)
	{
		return document.getElementById(id) ;
	}
}
function hide(id)
{
	var o = getByID(id) ;
	if (o)
	{
		if (document.all)
		{
			o.display = "none" ;
		}
		if(document.getElementById)
		{
			o.style.display = "none" ;
		}
	}
}
function show(id)
{
	var o = getByID(id) ;
	if (o)
	{
		if (document.all)
		{
			o.display = "visible" ;
		}
		if(document.getElementById)
		{
			o.style.display = "block" ;
		}
	}
}


/**************************************************************************************************
 *
 * DIAPORAMAS / MEDIACENTER
 *
 **************************************************************************************************/

function FigDiapo(bigdst,bigmax,mindst,minnbr) 
{
        this.Idx = 1;				/* Element en cours */
        this.Max = bigmax;			/* Nombre total d'Affiches */
	this.MinNbr = minnbr;			/* Nombre de "Miniatures" par Objet */

	this.BigDst = bigdst;			/* Pr�fix des Affiches */
	this.MinDst = mindst;			/* Pr�fix des Miniatures */
	this.OBigDst = getByID(bigdst);		/* Objet "Affiche" � remplir */
	this.OMinDst = getByID(mindst);		/* Objet "Miniature" � remplir */

	for(i=1;i<=this.Max;i++)
	{	
		hide(this.BigDst + '_' + i);
		hide(this.MinDst + '_' + i);
	}

        this.next = function() {
                this.Idx++;
		if(this.Idx > this.Max)
			this.Idx = 1;
		this.affiche();
        }

        this.prev = function() {
                this.Idx--;
		if(this.Idx < 1)
			this.Idx = this.Max
		this.affiche();
        }

        this.iset = function(i) {
                this.Idx = i;
		this.affiche();
        }

	this.affiche = function() {

		/* On Remplace le contenu de l'Affiche */
		if(this.OBigDst)
		{	OBigSrc = getByID(this.BigDst + '_' + this.Idx);
			this.OBigDst.innerHTML = OBigSrc.innerHTML;
		}

		/* On Remplace le contenu de la liste des Miniatures */
		if(this.OMinDst)
		{
			nb=0;
			this.OMinDst.innerHTML = "";
			for(i=this.Idx;nb<=this.MinNbr;i++)
			{	if(i<=this.Max)
				{	nb++;
					OMinScr = getByID(this.MinDst + '_' + i);
					this.OMinDst.innerHTML += OMinScr.innerHTML;
				}
				else
				{	i=0;
				}
			}
		}

		
	} 
	this.affiche();
}

/**************************************************************************************************
 *
 * ZOOM                          
 *
 **************************************************************************************************/

function FigZoom(bigmax,mindst,minnbr) 
{
        this.Idx = 1;				/* Element en cours */
        this.Max = bigmax;			/* Nombre total d'Affiches */
	this.MinNbr = minnbr;			/* Nombre de "Miniatures" par Objet */

	this.MinDst = mindst;			/* Pr�fix des Miniatures */
	this.OMinDst = getByID(mindst);		/* Objet "Miniature" � remplir */

	for(i=1;i<=this.Max;i++)
	{	
		hide(this.MinDst + '_' + i);
	}

        this.next = function() {
                this.Idx++;
		if(this.Idx > this.Max)
			this.Idx = 1;
		this.affiche();
        }

        this.prev = function() {
                this.Idx--;
		if(this.Idx < 1)
			this.Idx = this.Max
		this.affiche();
        }

        this.iset = function(i) {
                this.Idx = i;
		this.affiche();
        }

	this.affiche = function() {

		/* On Remplace le contenu de la liste des Miniatures */
		if(this.OMinDst)
		{
			nb=0;
			this.OMinDst.innerHTML = "";
			for(i=this.Idx;nb<=this.MinNbr;i++)
			{	if(i<=this.Max)
				{	nb++;
					OMinScr = getByID(this.MinDst + '_' + i);
					this.OMinDst.innerHTML += OMinScr.innerHTML;
				}
				else
				{	i=0;
				}
			}
		}

		
	} 
	this.affiche();
}

function opensenat(){
window.open('http://www.lefigaro.fr/live_streaming/player_whigh.html','','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=285,height=335');
}

/**************************************************************************************************
 *
 * Favoris   & verif pour page de demarrage                       
 *
 **************************************************************************************************/

function favoris() {
	if ( navigator.appName == 'Microsoft Internet Explorer' )
	{	window.external.AddFavorite("http://www.lefigaro.fr/","Le Figaro.fr"); }
	else if (navigator.appName=='Netscape') {window.sidebar.addPanel("Le Figaro.fr","http://www.lefigaro.fr","");}
	else if (navigator.appName=='Opera'){ alert('Vous utilisez Op�ra. Pour ajouter � vos Favoris le site du FIGARO.fr faites la combinai son de touches [CLTR] + D'); }
	else { alert('Pour ajouter � vos Favoris le site du FIGARO.fr faites la combinaison de touches [CMD] + D'); }
}
<!--
var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();
// -->
function verif () {

switch(BrowserDetect.browser) {

case "Firefox":
alert ('Utilisateur de Firefox, glissez l\'icon � gauche de l\'adresse url dans la petite maison de d�marrage' );
break;

case "Opera":
alert ('Utilisateur d\'Op�ra, tapez [CLTR] F12 puis tapez l\'adresse                "http://www.lefigaro.fr" dans le champs pr�vu � cet effet' );
break;

case "Safari":
alert ('Utilisateur de Safari, glissez l\'icon � gauche de l\'adresse url dans la petite maison de d�marrage' );
break;

}
}

/**************************************************************************************************
 *
 * QCM
 *
 **************************************************************************************************/

var qcmdate=new Date(2015, 12, 31);
var qcmhost='lefigaro.fr';

function FigQCM(obj,qcm,hvote,hresu,statut,votants) 
{
        this.obj = getByID(obj);		/* Identifiant de l'Objet recevant le code HTML */
        this.qcm = 'Q' + qcm;			/* Id QCM */
        this.hvote = hvote;			/* HTML Formulaire de vote */
	this.hresu = hresu;			/* HTML Affichage des r�sultats */
	this.statut = statut;			/* Statut du Sondage (A|O|F) */
	this.votants = votants;			/* Nb Votants du Sondage */

	this.idcount = 'QCM' + qcm + '-votants';
        this.objcount = getByID(this.idcount);


	this.cookie = GetCookie(this.qcm);

	if(this.cookie)
	{
		this.tab=this.cookie.split("-");
		this.cvote=this.tab[0];
		this.cmode=this.tab[1];
		this.clast=this.tab[2];
	}
	else
	{
		this.cvote=this.qcm;
		this.cmode='V';
		this.clast='';
		this.cookie = this.cvote + '-' + this.cmode + '-' + this.clast
		SetCookie(this.qcm,this.cookie,"/",null,qcmhost);
	}

	this.affiche = function() {
		if(this.statut == 'F')
			this.obj.innerHTML += this.hresu;
		else
		{
			if(this.cmode == 'R' || this.cmode == 'X' || this.cmode == 'A')
				this.obj.innerHTML += this.hresu;
			else
				this.obj.innerHTML += this.hvote;
		}

		if(this.objcount)
			this.objcount.innerHTML = this.votants;
		
	} 
	this.affiche();
}
function FigQCMVote(qcm,vote,url) {
	var mod;
	ncookie = 'Q' + qcm;	
	c = GetCookie(ncookie);
	if(c)
	{
		tab = c.split("-");
		mod = tab[1];
	}

	if(mod=='R'||mod=='X')
		mod='X';
	else
		mod='R';

	cookie = vote + '-' + mod + '-' + qcm
	SetCookie(ncookie,cookie,"/",qcmdate,qcmhost);
	document.location=url;
}
function FigQCMVoir(qcm,url) {
	var mod;
	ncook = 'Q' + qcm;	
	c = GetCookie(ncook);
	if(c)
	{
		tab = c.split("-");
		mod = tab[1];
	}

	if(mod=='R'||mod=='X')
		mod='X';
	else
		mod='A';
	cookie = vote + '-' + mod + '-' + qcm
	ncookie = 'Q' + qcm;
	SetCookie(ncookie,cookie,"/",qcmdate,qcmhost);
	document.location=document.location;
}


/**************************************************************************************************
 *
 * VIDEO PLAYER
 *
 **************************************************************************************************/


function FigVideo(idobj,bigdst,mindst,liste) 
{
        this.Idx = 0;				/* Element en cours */
	this.IdObj = idobj;			/* IDENTIFIANT DE L'OBJET */
	this.Liste = liste;			/* Tableau des donn�es */
	this.BigDst = bigdst;			/* Id Bloc Video */
	this.MinDst = mindst;			/* Id Bloc des Miniatures */
	this.OBigDst = getByID(bigdst);		/* Objet "Video" � remplir */
	this.OMinDst = getByID(mindst);		/* Objet "Miniature" � remplir */


	this.MinBuf = '';
	for (i=0;i<this.Liste.length;i++)
	{
		this.MinBuf += '<a href="JavaScript:' + this.IdObj + '.iset('+ i + ');"><img class="vignette" src="' + this.Liste[i][4] + '" border="0" /></a>';
		this.MinBuf += '<div class="tit"><a href="JavaScript:' + this.IdObj + '.iset('+ i + ');">' + this.Liste[i][1] + '</a></div>';
		this.MinBuf += '<div class="separe">&nbsp;</div>';
	}
	if(this.OMinDst)
	{
		this.OMinDst.innerHTML = this.MinBuf;
	}
	
        this.iset = function(i) {
                this.Idx = i;
		this.affiche();
        }

	this.affiche = function() {
		this.BigBuf = '';	

		this.BigBuf += '<object type="application/x-shockwave-flash" data="http://sa.kewego.com/swf/p3/epix.swf" width="240" height="182">';
		this.BigBuf += '<param name="flashVars" value="language_code=fr&playerKey=f5c5b9689b58&skinKey=0e1ec0f6f089&sig='+ this.Liste[this.Idx][5] + '&autostart=false" />';
		this.BigBuf += '<param name="movie" value="http://sa.kewego.com/swf/p3/epix.swf" />';
		this.BigBuf += '<param name="allowFullScreen" value="true" />';
		this.BigBuf += '<param name="allowscriptaccess" value="always" />';
		this.BigBuf += '</object>';
		this.BigBuf += '<br/>';



		this.BigBuf += '<span class="titre">'+ this.Liste[this.Idx][1] + '<br /></span>';
		this.BigBuf += '<span class="orange">Dur�e : ' + this.Liste[this.Idx][2] + ' le ' + this.Liste[this.Idx][6] +' </span>';
		this.BigBuf += '<div class="clear">&nbsp;</div>';

		/* On Remplace le contenu de l'Affiche */
		if(this.OBigDst)
		{
			this.OBigDst.innerHTML = this.BigBuf;
		}
	} 
	this.affiche();
}



/**************************************************************************************************
 *
 * Pour Commentaires
 *
 **************************************************************************************************/
var figcom_sep_bulle=false;



function fig_fbs_click() {
	var u=location.href;
	var t=document.title;
	window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');
	return false;
}

/**************************************************************************************************
 *
 * FONCTIONS ADTECH / YAHOO
 *
 **************************************************************************************************/

function adTech(pageId, formatId) {
  if (window.adgroupid == undefined)
    window.adgroupid = Math.round(Math.random() * 1000);

  if(window.location.hash === "#xtor=AL-5")
    SetCookie("_yahoo", "1", "/");

  if(GetCookie("_yahoo") &&
    /^(151|582|138|123|136|1310|126|1415|1416|131|124|574|2797)$/.
    exec(formatId))
    pageId = "yahoo";

  document.write('<scr' + 'ipt language="javascript1.1" ' + 'src="http://adserver.adtech.de/addyn|3.0|' +
    '906.1|1909408|0|-1|ADTECH;loc=100;alias=' + pageId + '' + formatId +
    ';target=_blank;key=key1+key2+key3+key4;grp=' + window.adgroupid +
    ';misc='+new Date().getTime()+'"></scri' + 'pt>'
  );
}

function open_m(url){var screen_size=get_screen_size();var x=0;var y=0;if(screen){x=(screen.availWidth-screen_size[0])/2};if(screen){y=(screen.availHeight-screen_size[1])/2};var popped=window.open(url,'m_for_more','width='+screen_size[0]+',height='+(screen_size[1])+',status=no,menubar=no,scrollbars=no,resizable=no,screenX='+x+',screenY='+y+',left='+x+',top='+y);if(!popped.opener){popped.opener=window}popped.focus()}function get_screen_size(){var screen_width=screen.availWidth;var screen_height=screen.availHeight;BrowserDetect.init();if(BrowserDetect.browser=='Explorer'&&BrowserDetect.version>=7){screen_height-=84}if(BrowserDetect.browser=='Firefox'&&BrowserDetect.version>=3){screen_height-=86}var site_min_width=1010;var site_min_height=670;if(screen_width<site_min_width||screen_height<site_min_height){return[site_min_width,site_min_height]}var site_max_width=1500;if(screen_width>site_max_width){window_width=site_max_width}else{window_width=screen_width-10}var window_height=Math.ceil((window_width*site_min_height)/site_min_width);if(window_height>screen_height){window_width=Math.ceil(((screen_height-10)*site_min_width)/site_min_height);window_height=screen_height-10}return[window_width,window_height]}var BrowserDetect={init:function(){this.browser=this.searchString(this.dataBrowser)||"An unknown browser";this.version=this.searchVersion(navigator.userAgent)||this.searchVersion(navigator.appVersion)||"an unknown version";this.OS=this.searchString(this.dataOS)||"an unknown OS"},searchString:function(data){for(var i=0;i<data.length;i++){var dataString=data[i].string;var dataProp=data[i].prop;this.versionSearchString=data[i].versionSearch||data[i].identity;if(dataString){if(dataString.indexOf(data[i].subString)!=-1)return data[i].identity}else if(dataProp)return data[i].identity}},searchVersion:function(dataString){var index=dataString.indexOf(this.versionSearchString);if(index==-1)return;return parseFloat(dataString.substring(index+this.versionSearchString.length+1))},dataBrowser:[{string:navigator.userAgent,subString:"Chrome",identity:"Chrome"},{string:navigator.userAgent,subString:"OmniWeb",versionSearch:"OmniWeb/",identity:"OmniWeb"},{string:navigator.vendor,subString:"Apple",identity:"Safari",versionSearch:"Version"},{prop:window.opera,identity:"Opera"},{string:navigator.vendor,subString:"iCab",identity:"iCab"},{string:navigator.vendor,subString:"KDE",identity:"Konqueror"},{string:navigator.userAgent,subString:"Firefox",identity:"Firefox"},{string:navigator.vendor,subString:"Camino",identity:"Camino"},{string:navigator.userAgent,subString:"Netscape",identity:"Netscape"},{string:navigator.userAgent,subString:"MSIE",identity:"Explorer",versionSearch:"MSIE"},{string:navigator.userAgent,subString:"Gecko",identity:"Mozilla",versionSearch:"rv"},{string:navigator.userAgent,subString:"Mozilla",identity:"Netscape",versionSearch:"Mozilla"}],dataOS:[{string:navigator.platform,subString:"Win",identity:"Windows"},{string:navigator.platform,subString:"Mac",identity:"Mac"},{string:navigator.userAgent,subString:"iPhone",identity:"iPhone/iPod"},{string:navigator.platform,subString:"Linux",identity:"Linux"}]};

/**************************************************************************************************
 *
 * FONCTIONS SCROLL CARRROUSEL AJOUTE LE 10/12/2009
 *
 **************************************************************************************************/

/**
 * @author mhamadi
 * @content Pseudo class objet de gestion des diapos mDossier    
 * 
 */

var mDossierObj = function () {
	this.mDossierListDiapo = new Array;
	this.mDossierDiapoElements = new Array;
	this.mDossierDiaposizeBySlide;
	this.mDossierDiapoClassName = 'm-dossier-diapo-effect-box';
	this.mDossierDiapoAnimationStatut = false;
	this.mDossierDiapoSizeBySlide = 460;
	this.mDossierDiapoNbrItemBySlide = 4;
	this.mDossierDiapoItemLength = 115;
	this.mDossierDiapoEffectGraduate = 46;
	this.mDossierMicroMooveSize = this.mDossierDiapoSizeBySlide/this.mDossierDiapoEffectGraduate;
	this.mDossierCurrentSlide = 0;
}

mDossierObj.prototype = {
	loadMDossierListDiapo:function () {
		var mDossierObj = this;
		var e = 0;
		var listDiapoMDossier = new Array;
		var listeDivToCheck = document.getElementsByTagName('div');
		for (var a=0;a<listeDivToCheck.length;a++) {
			if (listeDivToCheck[a].className == mDossierObj.mDossierDiapoClassName) {
				mDossierObj.mDossierListDiapo[e] = listeDivToCheck[a];
				mDossierObj.mDossierDiapoElements[e] = {};
				e++;
			}
		}
		for (var b=0;b<mDossierObj.mDossierListDiapo.length;b++) {
			mDossierObj.initMDossierDiapo(b);	
		}
	},
	initMDossierDiapo:function (b) {
		
		var mDossierObj = this;
		mDossierObj.mDossierDiapoElements[b] = {};
		
		mDossierObj.mDossierDiapoElements[b].effectBox = mDossierObj.mDossierListDiapo[b]; 
		mDossierObj.mDossierDiapoElements[b].parent = mDossierObj.mDossierListDiapo[b].parentNode;
		mDossierObj.mDossierDiapoElements[b].prevButton = mDossierObj.mDossierDiapoElements[b].parent.getElementsByTagName('a')[0];
		mDossierObj.mDossierDiapoElements[b].nextButton = mDossierObj.mDossierDiapoElements[b].parent.getElementsByTagName('a')[1];
		mDossierObj.mDossierDiapoElements[b].listItemsContainer = mDossierObj.mDossierListDiapo[b].getElementsByTagName('ul')[0];
		mDossierObj.mDossierDiapoElements[b].listeItemsArray = mDossierObj.mDossierDiapoElements[b].listItemsContainer.getElementsByTagName('li');
		mDossierObj.mDossierDiapoElements[b].listeDescriptionArray = mDossierObj.mDossierDiapoElements[b].parent.getElementsByTagName('ul')[1].getElementsByTagName('li');
		mDossierObj.mDossierDiapoElements[b].listItemsContainer.style.width = mDossierObj.mDossierDiapoElements[b].listeItemsArray.length * mDossierObj.mDossierDiapoItemLength +'px'; 
		mDossierObj.mDossierDiapoElements[b].mDossierCurrentSlide = 1;
		
		if (parseInt(mDossierObj.mDossierDiapoElements[b].listeItemsArray.length / mDossierObj.mDossierDiapoNbrItemBySlide) == mDossierObj.mDossierDiapoElements[b].listeItemsArray.length / mDossierObj.mDossierDiapoNbrItemBySlide) {
			mDossierObj.mDossierDiapoElements[b].maxSlides = mDossierObj.mDossierDiapoElements[b].listeItemsArray.length / mDossierObj.mDossierDiapoNbrItemBySlide;
			mDossierObj.mDossierDiapoElements[b].lastSlideNbrItems = mDossierObj.mDossierDiapoNbrItemBySlide; 
		}
		else {
			mDossierObj.mDossierDiapoElements[b].maxSlides = parseInt(mDossierObj.mDossierDiapoElements[b].listeItemsArray.length / mDossierObj.mDossierDiapoNbrItemBySlide) + 1;
			mDossierObj.mDossierDiapoElements[b].lastSlideNbrItems = (mDossierObj.mDossierDiapoElements[b].listeItemsArray.length) - (parseInt(mDossierObj.mDossierDiapoElements[b].listeItemsArray.length / mDossierObj.mDossierDiapoNbrItemBySlide)*mDossierObj.mDossierDiapoNbrItemBySlide) ;
		}
		mDossierObj.attributeActionOnClick(b);
		mDossierObj.attributeActionOnMouseOver(b);
	},
	attributeActionOnClick:function(b) {
		
		mDossierObj.mDossierDiapoElements[b].prevButton.onclick = function () {
			if (mDossierObj.mDossierDiapoAnimationStatut == false) {
				mDossierObj.mDossierDiapoAnimationStatut = true;
				mDossierObj.startMDossierDiapoMoove(mDossierObj.mDossierDiapoElements[b],'previous', b);
				return false;	
			}else return false;
		}
		mDossierObj.mDossierDiapoElements[b].nextButton.onclick = function () {
			if (mDossierObj.mDossierDiapoAnimationStatut == false) {
				mDossierObj.mDossierDiapoAnimationStatut = true;
				mDossierObj.startMDossierDiapoMoove(mDossierObj.mDossierDiapoElements[b],'next', b);
				return false;	
			}else return false;
		}
	},
	attributeActionOnMouseOver:function(b) {
		alert('coucou');
		var mDossierObj = this;
		var slidePosition = 0;
		for (var c=0;c<mDossierObj.mDossierDiapoElements[b].listeItemsArray.length;c++) {
			if (slidePosition == mDossierObj.mDossierDiapoNbrItemBySlide) slidePosition = 0;
			mDossierObj.mDossierDiapoElements[b].listeDescriptionArray[c].getElementsByTagName('input')[0].value = slidePosition;
			mDossierObj.showDescriptionText(b, mDossierObj.mDossierDiapoElements[b].listeItemsArray[c], c, slidePosition);
			slidePosition++;
		}
	},
	reatributePositionValue:function(currentDiapo, b, nextSlide, state){
		var mDossierObj = this;
		slidePosition = 0;
		if (state == 'special') {
			var counterStart = (((nextSlide-1)*mDossierObj.mDossierDiapoNbrItemBySlide)+currentDiapo.lastSlideNbrItems)-4;
			var counterStop = ((nextSlide-1)*mDossierObj.mDossierDiapoNbrItemBySlide)+currentDiapo.lastSlideNbrItems;
			
			for (var c=counterStart;c<counterStop;c++) {
				currentDiapo.listeDescriptionArray[c].getElementsByTagName('input')[0].value = slidePosition;
				slidePosition++;
			}
		}else {
			for (var c=0;c<mDossierObj.mDossierDiapoElements[b].listeItemsArray.length;c++) {
				if (slidePosition == mDossierObj.mDossierDiapoNbrItemBySlide) slidePosition = 0;
				mDossierObj.mDossierDiapoElements[b].listeDescriptionArray[c].getElementsByTagName('input')[0].value = slidePosition;
				slidePosition++;
			}
		}
	},
	showDescriptionText:function (diapoKey, elem, key) {
		var mDossierObj = this;
		var bgPositionArray = {0:'55px top',1:'173px top',2:'286px top',3:'404px top'}
		elem.getElementsByTagName('a')[0].onclick = function(){return false;}
		elem.getElementsByTagName('a')[0].onmouseover = function () {
			mDossierObj.mDossierDiapoElements[diapoKey].listeDescriptionArray[key].style.display='block';
			mDossierObj.mDossierDiapoElements[diapoKey].listeDescriptionArray[key].style.backgroundPosition=bgPositionArray[mDossierObj.mDossierDiapoElements[diapoKey].listeDescriptionArray[key].getElementsByTagName('input')[0].value];
			for (var d = 0; d < mDossierObj.mDossierDiapoElements[diapoKey].listeDescriptionArray.length; d++) {
				if (key != d && mDossierObj.mDossierDiapoElements[diapoKey].listeDescriptionArray[d].style.display == 'block') {
					mDossierObj.mDossierDiapoElements[diapoKey].listeDescriptionArray[d].style.display='none';
				}	
			}
		}
	},
	fadeInOutEffect:function (elem, way) {
		var mDossierObj = this;
		var fadeInOutFunction = function(){mDossierObj.fadeInOutEffect(elem, way);}
		if (way=='out') {
			if (elem.style.opacity != 0) {
				elem.style.opacity = elem.style.opacity-0.1;
				setTimeout(fadeInOutFunction, 10);
			}else elem.style.display = 'none';
		} else {
			if (elem.style.opacity != 1) {
				elem.style.opacity = elem.style.opacity+0.1;
				setTimeout(fadeInOutFunction, 10);
			} 
		}
	},
	startMDossierDiapoMoove:function (currentDiapoObject, direction, b) {
		if (direction == 'previous' ) {
			if (currentDiapoObject.mDossierCurrentSlide != 1) {
				if (currentDiapoObject.mDossierCurrentSlide == currentDiapoObject.maxSlides && currentDiapoObject.lastSlideNbrItems != this.mDossierDiapoNbrItemBySlide) {
					var marginWanted = parseInt(currentDiapoObject.listItemsContainer.style.marginLeft)+(currentDiapoObject.lastSlideNbrItems*this.mDossierDiapoItemLength);	
					this.reatributePositionValue(currentDiapoObject, b, currentDiapoObject.mDossierCurrentSlide-1,  'normal');
				}
				else var marginWanted = parseInt(currentDiapoObject.listItemsContainer.style.marginLeft)+this.mDossierDiapoSizeBySlide;	
				this.animateMDossierDiapo(currentDiapoObject, direction, marginWanted);
				currentDiapoObject.mDossierCurrentSlide --;
			} else this.mDossierDiapoAnimationStatut = false;
		} else {
			if (currentDiapoObject.mDossierCurrentSlide != currentDiapoObject.maxSlides) {
				if (currentDiapoObject.mDossierCurrentSlide == (currentDiapoObject.maxSlides-1) && currentDiapoObject.lastSlideNbrItems != this.mDossierDiapoNbrItemBySlide ) {
					this.reatributePositionValue(currentDiapoObject, b, currentDiapoObject.mDossierCurrentSlide+1, 'special');	
					var marginWanted = parseInt(currentDiapoObject.listItemsContainer.style.marginLeft)-(currentDiapoObject.lastSlideNbrItems*this.mDossierDiapoItemLength);
				} else var marginWanted = parseInt(currentDiapoObject.listItemsContainer.style.marginLeft)-this.mDossierDiapoSizeBySlide;
				this.animateMDossierDiapo(currentDiapoObject, direction, marginWanted);
				currentDiapoObject.mDossierCurrentSlide ++;	
			}else this.mDossierDiapoAnimationStatut = false; 			
		}
	},
	animateMDossierDiapo:function (currentDiapoObject, direction, marginWanted) {
		var mDossierObj = this;
		var moovingFunction = function(){
			mDossierObj.animateMDossierDiapo(currentDiapoObject, direction, marginWanted);
		}
		if (marginWanted != parseInt(currentDiapoObject.listItemsContainer.style.marginLeft)) {
			if (direction == 'next') {
				currentDiapoObject.listItemsContainer.style.marginLeft = parseInt(currentDiapoObject.listItemsContainer.style.marginLeft)-mDossierObj.mDossierMicroMooveSize+'px';	
			}else {
				currentDiapoObject.listItemsContainer.style.marginLeft = parseInt(currentDiapoObject.listItemsContainer.style.marginLeft)+mDossierObj.mDossierMicroMooveSize+'px'; 
			}
			setTimeout(moovingFunction, 10);		
		}else {
			mDossierObj.mDossierDiapoAnimationStatut = false;	
		}
	}
}
var mDossierObj = new mDossierObj();
mDossierObj.loadMDossierListDiapo();

