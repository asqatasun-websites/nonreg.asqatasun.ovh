/*
 * Opodo Autocomplete Module
 *
 * REQUIRED LIBRAIRIES : 
 *	- jquery.js
 *	- jquery.autocomplete.js plugin
 *	- jquery.bgiframe.js plugin
 *
 * Author : Mathieu Bourgeois
 */


/*
 * Function applying opodo autocomplete to specified fields
 *
 * @param fields - string selecting the fields we want the autocomplete to apply (in jquery selector format)
 * @param product - name of the product which we are on (flight or dp)
 * @param width - integer value of the autocomplete results list, in pixels [optional]
 */

function applyOpodoAutocomplete(fields, product, width) {
	
	// Case when width parameter is not specified
	if (typeof width == "undefined") {
		if (product == "flight") width = 370;		// flight default width value
		else if (product == "dp") width = 320;		// dp default width value
		else width = 370;
	}


	jQuery(fields).autocomplete("/php/autocomplete/autocomplete.php?product="+product, {
		minChars: 3,
		delay: 400,
		matchContains: true,
		selectFirst: true,
		width: width,
		max: 10,
		scroll: false,
		formatResult: function(data, value) {
			return data[1];
		}
	});
}