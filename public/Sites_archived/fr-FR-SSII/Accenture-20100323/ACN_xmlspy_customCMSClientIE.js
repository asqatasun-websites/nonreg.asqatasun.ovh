//***************************************************************************************
//
// JavaScript File
//
// Name:         ACN_xmlspy_customCMSClientIE.js
// Description:  Public Java script functions used for return values of CMS Dialogs used 
//               by ACNAuthenticPlaceholder.
//
//***************************************************************************************
//
// FUNCTIONS
//
// Name:         WBC_setThinEditIEImage 
// Description:  Gets return values of CMS Dialogs window and sends to insert image 
//               function of ACNAuthenticPlaceholder.
// Accepts:      strPhName - Placeholder Name
//               strURL - URL of Image
//				 strHRef - hyperlink of image
//               strAltText - Alternate Text of Image
// Returns:      None
//
// Name:         WBC_setThinEditIEAttachment
// Description:  Gets return values of CMS Dialogs window and sends to create 
//               attachment function of ACNAuthenticPlaceholder.
// Accepts:      strPhName - Placeholder Name
//               strURL - URL of Attachment
//               strDispText - Text of Attachment
// Returns:      None
//
// Name:         WBC_InsertImage_AltovaPlaceholder
// Description:  Insert Image into Altova Placeholder and phImg Placeholder 
// Accepts:      strPhName - Placeholder Name
//               strURL - URL of Image
//				 strHRef - hyperlink of image
//               strAltText - Alternate Text of Image
// Returns:      None
//
// Name:         WBC_InsertImage_HTMLPlaceholder
// Description:  Insert images into HTML placeholder
// Accepts:      strPhName - Placeholder Name
//               strURL - URL of Image
//				 strHRef - hyperlink of image
//               strAltText - Alternate Text of Image
// Returns:      None
//
// Name:         WBC_InsertAttachment_AltovaPlaceholder
// Description:  Insert attachment for altova placeholder control
// Accepts:      strPhName - Placeholder Name
//               strURL - URL of Attachment
//               strDispText - Text of Attachment
// Returns:      None
//
// Name:         WBC_InsertAttachment_ResourceGalleryPicker
// Description:  Insert attachment url within the resource gallery picker
// Accepts:      strPhName - Placeholder Name
//               strURL - URL of Attachment
//               strDispText - Text of Attachment
// Returns:      None
//
// Name:         WBC_CreateLink
// Description:  [Public] Response to user clicking on Hyperlink button
// Accepts:      strType - type of anchor tag to create 
//               strURL - URL of hyperlink
//               strAnchorName - name of anchor tag
//				 strTitle - Alternate text of hyperlink
// Returns:      None
//
//Declare Global Variable for setting which javasctipt to point to 
//***************************************************************************
//  DIALOG BOXES
//***************************************************************************
//
// REVISION HISTORY
//
// Author:       NND
// 
// Revision Date      Author         Description
// -------------      ------         -----------
// 04/25/2006		  NND			 Added global variables for Resource 
//									 Gallery Picker, and added condition to
//									 insert url into Resource Gallery Picker
//									 textbox dependent on global variable.
// 05/06/2006		  NND			 Added functions for inserting images for
//									 Altova Control and for HTML Placeholder
// 05/31/2006		  NND			 Updated code to insert HTML tags into
//									 phImg Placeholder, used HTML property
//									 instead of insertHMTL to ensure tags
//									 are appended not inserted. ref. SIR 266
//
//***************************************************************************

//Declare Global Variables
var ObjPlugInName = "";
var referenceObject = "";
var referenceTextboxID = "";
var c_Altova = "Altova";
var c_ResourceGalleryPicker = "ResourceGalleryPicker";

//Hide CMS Html placeholder
try
{
	if(document.all["ImagePH"] != null)
	{
		document.all["ImagePH"].style.display="none"
	}
}
catch(err)
{
}


// ******************************************************
// ---- [Public] Response to user clicking on 
//      Image button
function WBC_setThinEditIEImage(strPhName, strURL, strHRef, strAltText) 
{
	//Declare variables
	var strMultiPurposeTag;

	if(referenceObject != null)
	{
	
		if(referenceObject == c_Altova)
		{
			
			//Call Altova Placeholder function for inserting images
			WBC_InsertImage_AltovaPlaceholder(strPhName, strURL, strHRef, strAltText)
			
		}
		else
		{
		
			//Call HTML Placeholder function for inserting images
			WBC_InsertImage_HTMLPlaceholder(strPhName, strURL, strHRef, strAltText)
			
		}
			
		//Reset Global Variables
		referenceObject = "";
		
	}
	
}

//function to insert image into Altova Placeholder and phImg Placeholder
function WBC_InsertImage_AltovaPlaceholder(strPhName, strURL, strHRef, strAltText)
{
	//Declare Variables
	var phImage  = document.all["NCPHRICH_" + strPhName].HTML;
	var matchUrl = "";
	
	//Add alternate text to image

	strMultiPurposeTag = "<img src=\"" + strURL + "\" border=\"0\" alt=\"" + strAltText + "\" height=\"0\" width=\"0\" style=\"display:none\">";
		
	
	//Add href to image
	if ((strHRef != "") && (strHRef != IDS_HTTP_PROTOCOL)) 
	{
	
		strMultiPurposeTag = "<a href=\"" + strHRef + "\">" + strMultiPurposeTag + "</a>";
		
	}
	 
	//Set matching expression to Url of Image
	matchUrl = strURL; 

	//Insert Image into Altova HTML Placeholder
	if(phImage.indexOf(matchUrl) == -1)
	{		
	
		//Append Image to HTML of phImg Placeholder
		phImage += strMultiPurposeTag;
		
		//Set HTML of phImg Placeholder
		document.all["NCPHRICH_" + strPhName].HTML = phImage;
		
	}	
	 	 	 
	//Call Insert Image function based on calling altova control
	switch(ObjPlugInName) 
		{
		case "0":
			InsertImage(strURL, strHRef, strAltText);
			break;
		case "2":
			InsertImage2(strURL, strHRef, strAltText);
			break;
		case "3":
			InsertImage3(strURL, strHRef, strAltText);
			break;
		case "4":
			InsertImage4(strURL, strHRef, strAltText);
			break;
		}
}

//function to insert images into HTML placeholder
function WBC_InsertImage_HTMLPlaceholder(strPhName, strURL, strHRef, strAltText)
{

	//Add alternate text to image
	strMultiPurposeTag = "<img src=\"" + strURL + "\" border=\"0\" alt=\"" + strAltText + "\" >";
	
	
	//Add href to image		
	if ((strHRef != "") && (strHRef != IDS_HTTP_PROTOCOL)) 
	{
		strMultiPurposeTag = "<a href=\"" + strHRef + "\">" + strMultiPurposeTag + "</a>";
	}
	
	//insert image HTML into HTML Placeholder
	document.all["NCPHRICH_" + strPhName].insertHtml(strMultiPurposeTag);

}

// ******************************************************
// ---- [Public] Response to user clicking on 
//      Attachment button
function WBC_setThinEditIEAttachment(strPhName, strURL, strDispText) 
{
		
	if(referenceObject != null)
	{
		
		if(referenceObject == c_Altova)
		{
		
			//Call Altova Placeholder function for inserting attachments
			WBC_InsertAttachment_AltovaPlaceholder(strPhName, strURL, strDispText)
					
		}
		else
		{
			
			//Call Resource Gallery Picker function for inserting attachments
			WBC_InsertAttachment_ResourceGalleryPicker(strPhName, strURL, strDispText)	
			
		}
		
		//Reset Global Variables
		referenceObject = "";
		referenceTextboxID = "";
	}
}

//function to insert attachment for altova placeholder control
function WBC_InsertAttachment_AltovaPlaceholder(strPhName, strURL, strDispText)
{

	//Declare Variables
	var strMyDispText = strDispText;
	var strDSP;
	var int1;
	var int2;
	var strDSPFinal;
	var indexofDisplayText; 
	var DisplayText;
	var phImage  = document.all["NCPHRICH_" + strPhName].HTML;
	var matchUrl = ""; 
	
	//set display text to URL of attachment
	if (strMyDispText == "") 
	{
		strMyDispText = strURL;
	}
	
	int1 = strMyDispText.indexOf('<img', 0);
	int2 = strMyDispText.indexOf('border=', 0);
	strDSP = strMyDispText.slice(int1, int2 + 10);

	if(strDSP.search('alt'))
	{
		indexofDisplayText = strDSP.indexOf('"',0);
		DisplayText = strDSP.substring(indexofDisplayText+1,strDSP.length);
		indexofDisplayText = DisplayText.indexOf('"',0);
		DisplayText = DisplayText.substring(0,indexofDisplayText);
		strDSPFinal = strDSP + " height=\"0\" width=\"0\" style=\"display:none\">";
	}

	//Set matching expression to Url of Attachment
	matchUrl = strURL; 

	//Insert Attachment into HTML placeholder "phImg" of Altova control
	if(phImage.indexOf(matchUrl) == -1)
	{		
	
		//Append Image to HTML of phImg Placeholder
		phImage += "<a href=\"" + strURL + "\">" + strDSPFinal + "</a>";
		
		//Set HTML of phImg Placeholder
		document.all["NCPHRICH_" + strPhName].HTML = phImage;
		
	}	
			
	//Call altova Javascript to create XML for attachment elements		
	if (strURL != "")
	{	
		CreateAttachment("Attachment", strURL, DisplayText);
	}
	else
	{
		DetachText();
	}	
}

//function to insert attachment url within the resource gallery picker
function WBC_InsertAttachment_ResourceGalleryPicker(strPhName, strURL, strDispText)
{

	//Declare Variables
	var strMyDispText = strDispText;
	var strDSP;
	var int1;
	var int2;
	var strDSPFinal;
	var indexofDisplayText; 
	var DisplayText; 
	var phImage  = document.all["NCPHRICH_" + strPhName].HTML;
	var matchUrl = ""; 
	
	//set display text to URL of attachment
	if (strMyDispText == "") 
	{
		strMyDispText = strURL;
	}
		
	int1 = strMyDispText.indexOf('<img', 0);
	int2 = strMyDispText.indexOf('border=', 0);
	strDSP = strMyDispText.slice(int1, int2 + 10);
	
	if(strDSP.search('alt'))
	{
		indexofDisplayText = strDSP.indexOf('"',0);
		DisplayText = strDSP.substring(indexofDisplayText+1,strDSP.length);
		indexofDisplayText = DisplayText.indexOf('"',0);
		DisplayText = DisplayText.substring(0,indexofDisplayText);
		strDSPFinal = strDSP + " height=\"0\" width=\"0\" style=\"display:none\">";
	}

	//Set matching expression to Url of Attachment
	matchUrl = strURL; 

	//function to insert attachment url within the resource gallery picker
	if(phImage.indexOf(matchUrl) == -1)
	{		
	
		//Append Image to HTML of phImg Placeholder
		phImage += "<a href=\"" + strURL + "\">" + strDSPFinal + "</a>";
		
		//Set HTML of phImg Placeholder
		document.all["NCPHRICH_" + strPhName].HTML = phImage;
		
	}	
	
	//Set resource gallery textbox with attachment URL
	document.all[referenceTextboxID].value = strURL;
}

// [Public] Response to user clicking on Hyperlink button
function WBC_CreateLink(strHref, strType, strAnchorName, strTitle) 
{
	try
	{
		//  Originally ";" was used to separate the parameters 
		//  but it had an affect when "&" were replaced by "&amp;". 
		//  So it had to be changed to something else, in this case "@".
		strHref = strHref.replace(/&amp;/g, "&");
		
		var args = strHref + "!" + strTitle + "!" + strType + "!" + strAnchorName;		
		var strPath = "/Accenture/Altova/ASPX/Hlink/hlink.aspx";
		var strDlgRet = window.showModalDialog( strPath, args, "dialogWidth:450px;dialogHeight:265px;status:no;help:no");
			
		if (strDlgRet == "Cancel")
			return;	
	
		if ( strDlgRet != "")
		{		
			// split returned string "HRef;Title;Target;Name"
			var aAttributes = strDlgRet.split("!");
			var strHref = aAttributes[0];
			var strTitle = aAttributes[1];
			var strTarget = aAttributes[2];
			var strName = aAttributes[3];	
			if(strHref != "" || strName != "")
			{
				strHref = strHref.replace(/&amp;/g, "&");
				strHref = strHref.replace(/&/g, "&amp;");
				switch(ObjPlugInName) 
				{
				case "0":
					CreateLink('Link', strHref, strTarget, strName, strTitle);
					break;
				case "2":
					CreateLink2('Link', strHref, strTarget, strName, strTitle);
					break;
				}							
			}
			else
			{
				switch(ObjPlugInName) 
				{
				case "0":
					UnlinkText();
					break;
				case "2":
					UnlinkText2();
					break;
				}
			}			
		}
	}
	catch(Err)
	{
		return;
	
	}
}