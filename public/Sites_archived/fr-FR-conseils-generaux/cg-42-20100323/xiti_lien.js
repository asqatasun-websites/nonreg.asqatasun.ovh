//*****************************************************************
// Ce javascript permet de traiter le marquage des liens XITI
//*****************************************************************

// Fonction permettant l'ajout de Listener
function xiti_addEventLst(EventTarget,type,listener,useCapture) {
	useCapture = typeof(useCapture)=="boolean"?useCapture:false;
	if (EventTarget.addEventListener) {
		EventTarget.addEventListener(type, listener, useCapture);
	} else if ((EventTarget==window) && document.addEventListener) {
		document.addEventListener(type, listener, useCapture);
	} else if (EventTarget.attachEvent) {
		EventTarget["e"+type+listener] = listener;
		EventTarget[type+listener] = function() {EventTarget["e"+type+listener]( window.event );}
		EventTarget.attachEvent("on"+type, EventTarget[type+listener]);
	} else {
		EventTarget["on"+type] = listener;
	}
}

// Ajout d'un listener au chargement de la fen�tre
// La m�thode "xiti_initialisation" est ainsi appel�e au chargement
xiti_addEventLst(window,'load',xiti_initialisation);

// Cette m�thode ajoute un listener sur chaque Tag html "A"
// Ainsi, chaque action sur un tag "A" appelle la m�thode "xiti_clic"
function xiti_initialisation() {
  try {
    //V�rification que les refs sont remplies
    if (xiti_chemin_base == '') {
      window.status = "Les chemins (urls de base) pour le suivi des liens xiti ne sont pas corrects.";
    } else {
      //Parcours des tags A et ajout du listener sur chacun
    	var tagsA = document.getElementsByTagName("A");
    	for (var i = 0; i < tagsA.length; i++){
        xiti_addEventLst(tagsA[i], 'click', xiti_clic);
      } 
    }
  } catch(e) {}
}

// Cette Fonction envoie l'information � xiti si toutes les conditions sont ok 
function xiti_clic() {
  try {
    //R�cup�ration du lien
    ahref = this.href;
    //Si le lien est un javascript ou un mailto, on ne traite pas
    if (ahref.substr(0,11)!= 'javascript:' && ahref.substr(0,7)!= 'mailto:') {

      //Si le lien commence par l'url de base des documents, on envoie l'information � xiti
      //Si le lien ne commence pas par l'url de base, c'est un lien externe et on envoit l'information � xiti
      if (ahref.indexOf(xiti_chemin_base+'upload/') == 0) {
        var extension = '';
        //R�cup�ration de l'extension
        if(ahref.lastIndexOf('.')!=-1){
          extension = ahref.substring(ahref.lastIndexOf('.')+1,ahref.length);
        }
        //Si l'extension vaut "htm", on ne consid�re pas que c'est un fichier � t�l�charger (exemples : archives/accfond.htm, data/index.htm, etc...)
        if(extension.indexOf('htm')==-1){
          var nomFichier = '';
          if(ahref.lastIndexOf('/')!=-1){
            nomFichier = ahref.substring(ahref.lastIndexOf('/')+1,ahref.length);
          }
          //nomFichier = xiti_trim(nomFichier).replace(/[^A-Za-z0-9_~\\\/\-]+/g, '_');
          xt_med('C', siteNiveau2, nomFichier, 'T');
        }
      } else if (ahref.indexOf(xiti_chemin_base) != 0) {
        xt_med('C', siteNiveau2, ahref, 'S');
      }
      return false;
    }
  } catch (e) {}
}

// NON UTILISE 
// Fonction de r�cup�ration du nom du lien
function xiti_getInnerText(oNode) {
	var _innerText = "";
	if (!(oNode=oNode.firstChild)) {return "";}
	try {
		while (oNode) {
			if (oNode.tagName == 'IMG') _innerText += oNode.getAttributeNode("ALT").value;
			else _innerText += getOuterText(oNode);
			oNode = oNode.nextSibling;
		}
	} catch (e) {}
	return _innerText;
}

// NON UTILISE
// Fonction de nettoyage de cha�ne
function xiti_trim(str) {
	try {
		str = str.replace(/(^[\s:*]+)|([\s:*]+$)/g,'');
	} catch (e) {
		str = '';
	}
	return str;
}
