var busy = false;
var interval = 0;
function qubeFx(fx)
{

    if (isWAI)
    {

        if (fx == 'showFDContent' && !busy) {
            setTimeout('showFDContent()', interval);
            busy = true;
        }
        else if (fx == 'hideFDContent' && busy) {
            setTimeout('hideFDContent()', interval);
            busy = false;
        }
        else if (fx == 'showDEUContent' && !busy) {
                setTimeout('showDEUContent()', interval);
                busy = true;
            }
            else if (fx == 'hideDEUContent' && busy) {
                    setTimeout('hideDEUContent()', interval);
                    busy = false;
                }
                else if (fx == 'showActContent' && !busy) {
                        setTimeout('showActContent()', interval);
                        busy = true;
                    }
                    else if (fx == 'hideActContent' && busy) {
                            setTimeout('hideActContent()', interval);
                            busy = false;
                        }
    }
}

function showFDContent()
{
    setTimeout('hideElement("cube2")', interval);
    setTimeout('hideElement("cube3")', interval);
    setTimeout('showElement("cube23_hidden")', interval);
}

function hideFDContent()
{
    setTimeout('showElement("cube2")', interval);
    setTimeout('showElement("cube3")', interval);
    setTimeout('hideElement("cube23_hidden")', interval);
}

function showDEUContent()
{
    setTimeout('hideElement("cube5")', interval);
    setTimeout('hideElement("cube6")', interval);
    setTimeout('showElement("cube56_hidden")', interval);
}

function hideDEUContent()
{
    setTimeout('showElement("cube5")', interval);
    setTimeout('showElement("cube6")', interval);
    setTimeout('hideElement("cube56_hidden")', interval);
}

function showActContent()
{
    setTimeout('hideElement("cube4")', interval);
    setTimeout('hideElement("cube5")', interval);
    setTimeout('showElement("cube45_hidden")', interval);
}

function hideActContent()
{
    setTimeout('showElement("cube4")', interval);
    setTimeout('showElement("cube5")', interval);
    setTimeout('hideElement("cube45_hidden")', interval);
}

function showElement(elementName)
{
    var docelements = document.getElementById(elementName);
    docelements.style.display = 'block';
}

function hideElement(elementName)
{
    var docelementh = document.getElementById(elementName);
    docelementh.style.display = 'none';
}