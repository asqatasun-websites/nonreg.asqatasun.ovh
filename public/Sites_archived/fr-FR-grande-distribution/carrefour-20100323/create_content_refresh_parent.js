
var url = window.location.href;

var parentUrl = "";
var reqToken = url.substr(0, url.indexOf("?")+1);
var optToken = url.substr(url.indexOf("?")+1);
var delimiter="&";

var appendString = "&vgnextrefresh=1";

//Script to define the callback handler for custom refresh functionality.
function doPostSubmit(JSName, JSCall) {
	if(eval(JSName)) eval(JSCall);
}

//Script to refresh the parent with new url for content create from detail page.
function refreshWithNewUrl(objectId, ObjectXML, isNewObject) {
	tokenize(optToken, delimiter, objectId);
	appendVgnExtRefresh();
	window.location.href = parentUrl;
}

// Script to build the new url.
function tokenize(optToken, delimiter, objectId) {
	keyArray=new Array();
	valueArray=new Array();
	var Count=0;
	while (optToken.length>0) {
		if(optToken.indexOf(delimiter)!=-1){
			pair = optToken.substr(0,optToken.indexOf(delimiter));
		}else{
			pair = optToken;
		}
		var key = pair.substr(0, pair.indexOf("="));
		var value = pair.substr(pair.indexOf("=")+1);
		if (key == "vgnextoid") {
			value = objectId;
		}
		if (key != "vgnextchannel") {
			keyArray[Count] = key;
			valueArray[Count] = value;
			Count=Count+1;
		}
		optToken=optToken.substr(optToken.indexOf(delimiter)+1,optToken.lenth-optToken.indexOf(delimiter)+1);
	}
	parentUrl = reqToken;
	for(var i=0;i<keyArray.length;i++) {
		parentUrl = parentUrl+keyArray[i]+"="+valueArray[i];
		if (i<keyArray.length-1) {
			parentUrl=parentUrl+delimiter;
		}
	}
}

//Script to refresh the parent for content create from query.
function refreshParent(objectId, ObjectXML, isNewObject) {
	parentUrl=url;
	appendVgnExtRefresh();
	window.location.href = parentUrl;
}

//Script to append the "&vgnextrefresh=1" to the url.
function appendVgnExtRefresh() {

	if (parentUrl.indexOf(appendString)==-1) {
		parentUrl=parentUrl+appendString;
	}
}