// JavaScript Document
$(document).ready(function() {
	// Menu d�roulant (Superfish)
	$('#menu').superfish({
		hoverClass:'on',
		delay:300,
		autoArrows:false
	});
		// Masque les sous-menus proprement en cas de d�sactivation du js
		$("#menu li").mouseover(function(){
			$(this).children('ul').addClass('actif');
		});
		$("#menu li a").focus(function(){
			$(this).parent().children('ul').addClass('actif');
		});
	// Onglets
	$('ul.onglets').tabs({selected:0, fx:{opacity:"toggle"}});
	// Lightbox
	$('a.lightbox').lightBox();
	// Identifie les liens pointant vers des sites externes (ajout d'un pictogramme � droite du lien)
	$("a[href^=\"http\"]").addClass("lien_externe");
	$("a.lightbox").removeClass("lien_externe");
	$("#pied_page a").removeClass("lien_externe");
	// Aspect des lignes (<tr>) des tableaux de donn�es au survol
	$('table.tableau_donnees tr').mouseover(function(){$(this).addClass('survol');}).mouseout(function(){$(this).removeClass('survol');});
	// Chiffres cl�s
	$('#chiffres_cles').innerfade({
		//animationtype: 'slide',
		speed: 1500,
		timeout: 5000,
		type: 'random',
		containerheight: '1em'
	});
});