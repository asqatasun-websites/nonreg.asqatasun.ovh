// The Pukkelpop Core Javascript File
// Created by David Candreva <david@inventis.be>

window.addEvent('domready', function(){	

	/*
	 * Check if Cufon is loaded? Quite useless but who cares..
	 */
	if(Cufon){
		
		/*
		 * Add if's for speeding things up. MooTools is much faster than Cufon
		 */
		if($$('h1')){
			Cufon.replace('h1', {fontFamily:'bold'});
		}
		
		if($$('h2')){
			Cufon.replace('h2', {fontFamily:'bold'});
		}
		
		if($$('h3')){
			Cufon.replace('h3', {fontFamily:'bold'});
		}
				
		
		if($$('div.rows div.col div.content h3')){
			Cufon.replace('div.rows div.col div.content h3', {fontFamily:'light'});
		}
		
		if($$('ul.arrowList li')){
			Cufon.replace('ul.arrowList li', {fontFamily:'light', hover:true});
		}
		
		if($$('h4')){
			Cufon.replace('h4', {fontFamily:'bold'});
		}
		
		if($$('#sitemap ul li span')){
			Cufon.replace('#sitemap ul li span', {fontFamily:'bold'});	
		}
		
		if($$('ul.actions li.button')){
			Cufon.replace('ul.actions li.button', { fontFamily:'light', hover:true});
		}				
		
		if($$('h5')){
			Cufon.replace('h5', { fontFamily:'light', hover:true});
		}
		
		if($$('div#breadcrumbs ol li')){
			Cufon.replace('div#breadcrumbs ol li', { fontFamily:'light', hover:true});
		}
		
		if($$('form.default div.buttons button')){
			Cufon.replace('form.default div.buttons button', {fontFamily:'light', hoverables:{a:false, button:true}, hover:true});
		}
		
		if($$('span.yeartitle')){
			Cufon.replace('span.yeartitle', {fontFamily:'bold'});
		}
		
		if($$('ul.sociallinks li')){
			Cufon.replace('ul.sociallinks li', { fontFamily:'light', hover:true});
		}
		
		if($$('ul.niceList li')){
			Cufon.replace('ul.niceList li', { fontFamily:'bold'});
		}
		
		if($$('div#topbar div.content div.options ul.option')){
			Cufon.replace('div#topbar div.content div.options ul.languages li', {fontFamily:'bold', hover:true});
			Cufon.replace('div#topbar div.content div.options ul.option li.register', {fontFamily:'light', hover:true});
			Cufon.replace('div#topbar div.content div.options ul.option li.welcome', {fontFamily:'light', hover:true});
			Cufon.replace('div#topbar div.content div.options ul.option li.logout', {fontFamily:'light', hover:true});
			Cufon.replace('div#topbar div.content div.options ul.option li.login', {fontFamily:'bold', hover:true});
		}
		
		if($$('#lbBottom a')){
			Cufon.replace('#lbBottom a', { fontFamily:'light', hover:true});
		}
		
		if($$('#lbCaption span')){
			Cufon.replace('#lbCaption span', {fontFamily:'bold'});
		}
		
		if($$('#lbCaption a')){
			Cufon.replace('#lbCaption a', {fontFamily:'bold'});
		}
	}
	
	if($$('ul.showActionOnfocus li')){
		$$('ul.showActionOnfocus li').addEvents({
			'mouseenter': function(){
				if(this.getElement('ul.actions')){
					this.getElement('ul.actions').setStyle('display','block');
				}
			},
			'mouseleave': function(){
				if(this.getElement('ul.actions')){
					this.getElement('ul.actions').setStyle('display','none');
				}
			}
		});
	}
	
	$$('a[href^=http]').each(function(a) {
		if (window.location.hostname) {
			var hostname = window.location.hostname.replace("www.", "").toLowerCase();
			if (!a.get('href').contains(hostname)) {
				a.set({
					'target': '_blank'
				});
			}
		}
	});
	
	/*
	 * If alternate text is set we use it as default value
	 */
	$$('input, textarea').each(function(el){
		el.addEvents({
			'click': function(){
				if(el.get('value') == el.get('alt')){
					el.set('value', '');
				}
				el.addClass('focus');
			},
			'blur': function(){
				el.removeClass('focus');
				if(el.get('value') == '' && el.get('alt')){
					el.set('value', el.get('alt'));
				}
			},
			'focus': function(){
				if(el.get('value') == el.get('alt')){
					el.set('value', '');
				}
				el.addClass('focus');
			}
		});
	});
	

	//fetch lang
	var urlParts = window.location.href.split("//").pop().split('/');
	var DOMAIN    = urlParts.shift();
	var LANG    = urlParts.shift();
	if(!LANG){
		LANG = 'nl';
	}		
	
	// End Search Form Search Events
	
	
	// Start Submenu effect
	var currentActive = document.getElement('div#navigation > ul > li > a.active');
	var menu = document.getElements('div#navigation > ul > li');
	
	menu.each(function(el){
		
		var subMenu = el.getElement('ul');
		var width = 0;
		
		if(subMenu){
			var listItems = subMenu.getElements('li');
			listItems.each(function(li){
				width += li.getSize().x;
			});
			
			width = width + (subMenu.getElements('li').length * 6) 
			
			
			if(listItems.length >= 6){
				subMenu.setStyle('right', (el.getCoordinates().right - $('navigation').getCoordinates().right - 6));
				
				if(width > 621){
					subMenu.setStyle('width', 621);
				} else {
					subMenu.setStyle('width', width);
				}
			} else if(width > 550){
				
				subMenu.setStyle('left', - (el.getCoordinates().left - $('navigation').getCoordinates().left));
				
				if(width > 621){
					subMenu.setStyle('width', 621);
				} else {
					subMenu.setStyle('width', width);
				}
			} else {
				
				subMenu.setStyle('width', width + 1);
				subMenu.setStyle('marginLeft', -(width/2));
				subMenu.setStyle('left', '50%');
			}
			
		}
		
		
		var linkEl = el.getElement('a');
		
		el.addEvents({
			'mouseover': function(el){
				hideAllSubmenus();
				linkEl.addClass('active');
				if(subMenu && !subMenu.hasClass('submenu'))subMenu.addClass('submenu');
			},
			'mouseleave': function(el){
				hideAllSubmenus();
				if(currentActive){	
					var currentSubMenu = currentActive.getParent().getElement('ul');
				 	currentActive.addClass('active');
				}
				if(currentSubMenu)currentSubMenu.addClass('submenu');
			}
		});
		
	});
	
	function hideAllSubmenus(){
		var btn = $$('div#navigation ul li a.active');
		btn.removeClass('active');

		var menus = $$('div#navigation ul li ul.submenu');
		menus.removeClass('submenu');
	}
	
	// End Search Form Search Events
	
	// Start Newsletter subscription
	if($('form_newsletter')){		
		
		var element = $('form_newsletter').getElement('div');
		var label	= $('form_newsletter').getElement('label');
		var clickEl = $$('#form_newsletter button[name=newsletter_submit]');
		var span = new Element('span', {text:''}).injectAfter(label);
		
		clickEl.addEvent('click', function(el){
			el = new Event(el).stop();
			el = el.target;
			
			var emailValue = $('form_newsletter').getElement('input[name=frm_email]').get('value');
			
			var jsonRequest = new Request.JSON({
				method: 'post',
				url: '/'+LANG+'/newsletter/home/'+LANG,
				data: {'email' : emailValue, 'language': LANG},
				onComplete: function(data){
					if(data.validation){
						element.addClass('error');
						span.addClass('error')
						span.set('text', data.validation.errors.email.message);
					} else {
						var div = new Element('div', {text:data.message}).addClass('success');
						div.inject($('form_newsletter').getParent(), 'inside');
						$('form_newsletter').dispose();
					}
				}
			}).send();
			
		});
		
	}
	// End Newsletter subscription
});