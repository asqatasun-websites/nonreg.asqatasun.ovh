//------------------------------------------------------------------------------
// FICHIER : $RCSfile: gsite.js,v $
// AUTEUR  : $Author: bouba $
// VERSION : $Revision: 1.19 $
// DATE    : $Date: 2007-10-24 14:54:16 $
//------------------------------------------------------------------------------
// GSite - Web site management in PHP - main js file
//
// Copyright (C) 2002 COUDOUNEAU Laurent (lc@gsite.org)
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//------------------------------------------------------------------------------

//==============================================================================
/**
* Function for change style when mouse over a ligne, and put old style when out
*/
//==============================================================================
var sSavedColor1 = "";
var sSavedColor2 = "";
function gsRowOver (oRow, sCol) {
  sSavedColor1 = oRow.cells[0].style.backgroundColor;
  sSavedColor2 = oRow.cells[oRow.cells.length - 1].style.backgroundColor;
  for (i = 0; i < oRow.cells.length; i++) {
    oRow.cells[i].style.backgroundColor = sCol;
  }
}

function gsRowOut (oRow) {
  for (i = 0; i < 2; i++) {
    oRow.cells[i].style.backgroundColor = sSavedColor1;
  }
  for (i = 2; i < oRow.cells.length; i++) {
    oRow.cells[i].style.backgroundColor = sSavedColor2;
  }
}

//==============================================================================
//==============================================================================

function gs_getElementLeft (eElement) {
  var nLeftPos    = eElement.offsetLeft;    // initialize var to store calculations
  var eParElement = eElement.offsetParent;  // identify first offset parent element
    
  while (eParElement != null) {             // move up through element hierarchy
    nLeftPos   += eParElement.offsetLeft;   // appending left offset of each parent
    eParElement = eParElement.offsetParent; // until no more offset parents exist
  }
  
  return nLeftPos;                          // return the number calculated
}

//==============================================================================
//==============================================================================

function gs_getElementTop (eElement) {
  var nTopPos     = eElement.offsetTop;     // initialize var to store calculations
  var eParElement = eElement.offsetParent;  // identify first offset parent element  
  
  while (eParElement != null) {             // move up through element hierarchy
    nTopPos    += eParElement.offsetTop;    // appending top offset of each parent
    eParElement = eParElement.offsetParent; // until no more offset parents exist
  }
  
  return nTopPos;                           // return the number calculated
}

//==============================================================================
//==============================================================================

function gsGetDivByName (sDiv) {
  var namedList = document.getElementsByName (sDiv);
  return namedList.item (0);
}

//==============================================================================
//==============================================================================

function gsSwitchDivVisibility (sDiv) {
  var oDiv = gsGetDivByName (sDiv);
  if (oDiv == null) return;
  oDiv.style.display = (oDiv.style.display == "none" ? "block" : "none");
}

//==============================================================================
//==============================================================================

var gsCurrentDiv = null;

function gsShowHideDiv (sDiv, sDivDft) {
  if (gsCurrentDiv == null) gsCurrentDiv = sDivDft;
  
  gsSwitchDivVisibility (gsCurrentDiv);
  gsSwitchDivVisibility (sDiv);
  
  gsCurrentDiv = sDiv;
}

//==============================================================================
//==============================================================================

function openBasicPopup (sURL, iWidth, iHeight, sName) {
  if (arguments.length == 3) {
    sName = 'popup';
  }
  
  if (arguments.length == 2) {
    sName   = 'popup';
    iHeight = 400;
  }
  
  if (arguments.length == 1) {
    sName   = 'popup';
    iWidth  = 660;
    iHeight = 400;
  }
  
  var sArgs = 'toolbar=0'+
              ',hotkeys=0'+
              ',location=0'+
              ',directories=0'+
              ',menubar=0'+
              ',personalbar=0'+
              ',status=0'+
              ',scrollbars=1'+
              ',resizable=1'+
              ',width='+iWidth+
              ',height='+iHeight;
  
  var wWindow = open (sURL, sName, sArgs);
  wWindow.window.focus ();
}

//==============================================================================
//==============================================================================

function openPopup (sModule, sParam, iWidth, iHeight, sName, iMenubar) {
  var sURL = 'popup.php?module='+escape (sModule);

  if (sParam != '') {
    sURL += sParam;
  }
  
  if (arguments.length == 5) {
    iMenubar = '0';
  }
  
  if (arguments.length == 4) {
    iMenubar = '0';
    sName    = 'popup';
  }
  
  if (arguments.length == 3) {
    iMenubar = '0';
    sName    = 'popup';
    iHeight  = 400;
  }
  
  if (arguments.length == 2) {
    iMenubar = '0';
    sName    = 'popup';
    iWidth   = 660;
    iHeight  = 400;
  }
  
  var sArgs = 'toolbar=0'+
              ',hotkeys=0'+
              ',location=0'+
              ',directories=0'+
              ',menubar='+iMenubar+
              ',personalbar=0'+
              ',status=0'+
              ',scrollbars=1'+
              ',resizable=1'+
              ',width='+iWidth+
              ',height='+iHeight;
  
  var wWindow = open (sURL, sName, sArgs);
  wWindow.window.focus ();
}

//==============================================================================
//==============================================================================

function openUser (sProject, sAnnu, sLocale, iUser, bMessenger) {
  var sURL = 'popup.php'+
             '?project='+escape (sProject)+
             '&locale='+escape (sLocale)+
             '&annu='+escape (sAnnu)+
             '&module=annu'+
             '&fct=showUser'+
             '&userid='+iUser+
             '&with_msg='+bMessenger;
  
  var sArgs = 'toolbar=0'+
              ',hotkeys=0'+
              ',location=1'+
              ',directories=0'+
              ',menubar=0'+
              ',personalbar=0'+
              ',status=0'+
              ',scrollbars=1'+
              ',resizable=1'+
              ',width=650'+
              ',height=400';
  
  var wWindow = open (sURL, 'user', sArgs);
  wWindow.window.focus ();
}

//==============================================================================
//==============================================================================

function openMessenger (sProject, sLocale, iTo) {
  var sURL = 'popup.php'+
             '?project='+escape (sProject)+
             '&locale='+escape (sLocale)+
             '&module=messenger'+
             '&fct=userMessages'+
             '&engine_cmd=newmsg'+
             '&msg_to='+iTo;
  
  var sArgs = 'toolbar=0'+
              ',hotkeys=0'+
              ',location=0'+
              ',directories=0'+
              ',menubar=0'+
              ',personalbar=0'+
              ',status=0'+
              ',scrollbars=1'+
              ',resizable=1'+
              ',width=650'+
              ',height=400';
  
  var wWindow = open (sURL, 'messenger', sArgs);
  wWindow.window.focus ();
}

//==============================================================================
//==============================================================================

function openBrowser (sProject, sAnnu, sProjectadm, sLocale, sForm, sFile, sPath, sWithPrettyEditor, sPrettyFunc, sPrettyTextarea, sPrettyTagOpen, sPrettyTagClose, sPrettySample ) {
  var sURL = 'popup.php'+
             '?project='+escape (sProject)+
             '&locale='+escape (sLocale)+
             '&annu='+escape (sAnnu)+
             '&projectadm='+escape (sProjectadm)+
             '&module=dir'+
             '&fct=browser'+
             '&browser_form='+escape (sForm)+
             '&browser_file='+escape (sFile)+
             '&browser_path='+escape (sPath)+
             '&with_pretty='+escape (sWithPrettyEditor)+
             '&pretty_func='+escape (sPrettyFunc)+
             '&pretty_area='+escape (sPrettyTextarea)+
             '&pretty_open='+escape (sPrettyTagOpen)+
             '&pretty_close='+escape (sPrettyTagClose)+
             '&pretty_text='+escape (sPrettySample);
  
  var sArgs = 'toolbar=0'+
              ',hotkeys=0'+
              ',location=0'+
              ',directories=0'+
              ',menubar=0'+
              ',personalbar=0'+
              ',status=0'+
              ',scrollbars=1'+
              ',resizable=1'+
              ',width=600'+
              ',height=400';
  
  var wWindow = open (sURL, 'browser', sArgs);
  wWindow.window.focus ();
}

//==============================================================================
//==============================================================================

function eraseBrowserFields (oField1, oField2) {
  oField1.value = '';
  oField2.value = '';
}

//==============================================================================
//==============================================================================

function openDocumentPopup (sProject, sLocale, sDoc, sSet, sParam, iWidth, iHeight) {
  var sURL = 'document.php'+
             '?project='+escape (sProject)+
             '&locale='+escape (sLocale)+
             '&doc='+escape (sDoc)+
             '&set='+escape (sSet);
  
  if (sParam != '') {
    if (sParam[0] != '&') sURL += '&';
    
    sURL += sParam;
  }
  
  if (arguments.length == 6) {
    iHeight = 400;
  }
  
  if (arguments.length == 5) {
    iWidth  = 660;
    iHeight = 400;
  }
  
  var sArgs = 'toolbar=0'+
              ',hotkeys=0'+
              ',location=0'+
              ',directories=0'+
              ',menubar=0'+
              ',personalbar=0'+
              ',status=0'+
              ',scrollbars=1'+
              ',resizable=1'+
              ',width='+iWidth+
              ',height='+iHeight;
  
  var wWindow = open (sURL, 'popupdoc', sArgs);
  wWindow.window.focus ();
}

//==============================================================================
//==============================================================================

function checkFrenchZipCode (oZip, oDpt, sMsg) {
  var sZip = oZip.value;
  
  if (sZip.length == 0) {
    oDpt.value = 900;
    
  } else {
    if (sZip.length != 5) {
      window.alert (sMsg+' ('+sZip+')');
      return false;
    }
    
    var sCode = '';
    if ((sZip.charAt (0) == '9') && ((sZip.charAt (1) == '7') || (sZip.charAt (1) == '8'))) {
      sCode = sZip.charAt (0) + sZip.charAt (1) + sZip.charAt (2);
      
    } else {
      sCode = sZip.charAt (0) + sZip.charAt (1);
    }
    
    var iCode = Number (sCode);
    if (! (((iCode >    0) && (iCode <=  95)) ||
           ((iCode >= 971) && (iCode <= 976)) ||
            (iCode == 980) ||
           ((iCode >= 986) && (iCode <= 988)))) {
      window.alert (sMsg+' ('+sZip+')');
      return false;
    }
    
    oDpt.value = iCode;
  }
  
  return true;
}

//==============================================================================
//==============================================================================

function isYear (sYear) {
  if (sYear.length > 0) {
    if (! isNaN (Number (sYear))) {
      if (sYear >= 1752) return true;
    }
  }
  
  return false;
}

//==============================================================================
//==============================================================================

function isLeapYear (y) {
  return (y % 4) == 0 && ((y % 100) != 0 || (y % 400) == 0);
}

//==============================================================================
//==============================================================================

function daysInMonth (m, y) {
  if ((m >= 1) && (m <= 12)) {
    if (m ==  1) return 31;
    if (m ==  3) return 31;
    if (m ==  4) return 30;
    if (m ==  5) return 31;
    if (m ==  6) return 30;
    if (m ==  7) return 31;
    if (m ==  8) return 31;
    if (m ==  9) return 30;
    if (m == 10) return 31;
    if (m == 11) return 30;
    if (m == 12) return 31;
    
    if (isLeapYear (y)) {
      return 29;
      
    } else {
      return 28;
    }
    
  } else {
   return 0;
  }
}

//==============================================================================
//==============================================================================

function checkDate_fr (sDate) {
  if (sDate.match (/\d{2}\/\d{2}\/\d{4}/) == null) {
    return false;
  }
  
  var daystr   = sDate.substr (0, 2);
  var monthstr = sDate.substr (3, 2);
  var yearstr  = sDate.substr (6, 4);
  
  return ((daystr >= 1) && 
          (daystr <= daysInMonth (monthstr, yearstr)) &&
          (monthstr >= 1) && (monthstr <= 12) &&
          (yearstr >= 1752))
}

//==============================================================================
//==============================================================================

function checkDateNull_fr (sDate) {
  if (sDate.match (/\d{2}\/\d{2}\/\d{4}/) == null) {
    return false;
  }
  
  var daystr   = sDate.substr (0, 2);
  var monthstr = sDate.substr (3, 2);
  var yearstr  = sDate.substr (6, 4);
  
  return ( ((daystr >= 1) && 
           (daystr <= daysInMonth (monthstr, yearstr)) &&
           (monthstr >= 1) && (monthstr <= 12) &&
           (yearstr >= 1752)) || 
           ((daystr=='00') && (monthstr=='00') && (yearstr =='0000')) )
}

//==============================================================================
//==============================================================================

function checkDateOrder_fr (sDate1, sDate2) {
  var daystr1   = sDate1.substr (0, 2);
  var monthstr1 = sDate1.substr (3, 2);
  var yearstr1  = sDate1.substr (6, 4);
  
  var daystr2   = sDate2.substr (0, 2);
  var monthstr2 = sDate2.substr (3, 2);
  var yearstr2  = sDate2.substr (6, 4);
  
  if ((daystr1=='00') && (monthstr1=='00') && (yearstr1=='0000')) return true;
  if ((daystr2=='00') && (monthstr2=='00') && (yearstr2=='0000')) return true;
  
  var date1 = new Date (yearstr1, monthstr1 - 1, daystr1)
  var date2 = new Date (yearstr2, monthstr2 - 1, daystr2)
  
  return (date1 < date2)
}

//==============================================================================
//==============================================================================

function checkDate_en (sDate) {
  if (sDate.match (/\d{4}\/\d{2}\/\d{2}/) == null) {
    return false;
  }
  
  var yearstr  = sDate.substr (0, 4);
  var monthstr = sDate.substr (5, 2);
  var daystr   = sDate.substr (8, 2);
  
  return ((daystr >= 1) && 
          (daystr <= daysInMonth (monthstr, yearstr)) &&
          (monthstr >= 1) && (monthstr <= 12) &&
          (yearstr >= 1752))
}

//==============================================================================
//==============================================================================

function checkDateNull_en (sDate) {
  if (sDate.match (/\d{4}\/\d{2}\/\d{2}/) == null) {
    return false;
  }
  
  var yearstr  = sDate.substr (0, 4);
  var monthstr = sDate.substr (5, 2);
  var daystr   = sDate.substr (8, 2);
  
  return ( ((daystr >= 1) && 
           (daystr <= daysInMonth (monthstr, yearstr)) &&
           (monthstr >= 1) && (monthstr <= 12) &&
           (yearstr >= 1752)) || 
           ((daystr=='00') && (monthstr=='00') && (yearstr=='0000')) )
}

//==============================================================================
//==============================================================================

function checkDateOrder_en (sDate1, sDate2) {
  var yearstr1  = sDate1.substr (0, 4);
  var monthstr1 = sDate1.substr (5, 2);
  var daystr1   = sDate1.substr (8, 2);
  
  var yearstr2  = sDate2.substr (0, 4);
  var monthstr2 = sDate2.substr (5, 2);
  var daystr2   = sDate2.substr (8, 2);
  
  if ((daystr1=='00') && (monthstr1=='00') && (yearstr1=='0000')) return true;
  if ((daystr2=='00') && (monthstr2=='00') && (yearstr2=='0000')) return true;
  
  var date1 = new Date (yearstr1, monthstr1 - 1, daystr1)
  var date2 = new Date (yearstr2, monthstr2 - 1, daystr2)
  
  return (date1 < date2)
}

//==============================================================================
//==============================================================================
function gs_isValidEmail (sEmail) {
  if (sEmail == "") { return false; }
  var sEmailLower =  sEmail.toLowerCase();
  var pos = sEmailLower.search(/^[0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\.[a-z]{2,3}$/);
  if (pos == '-1'){
    return false;
  }
  return true;
}

//==============================================================================
// Launch managePrint to print a gsite <document> page. (cf. managePrint).
//==============================================================================
function printThisPage() 
{ 
   if (managePrint ()) {
      //-- Launch print.
      window.print();
   }
}
//==============================================================================
// Print a gsite <document> page :
//
// Several <div> must be appears in the gsite template page :
//  - id=<doc_to_print> : Container of the GSITE TAG DOCUMENT table.
//  - id=<data_print>   : Empty div container : to put just one line 
//                        before the </body>, which store temporaly the infos 
//                        to print.
//
//==============================================================================
function managePrint () {
   var oDocToPrintDiv, oDataPrintDiv;
   
   if (    (oDocToPrintDiv       = document.getElementById ('doc_to_print'))
        && (oDataPrintDiv        = document.getElementById ('data_print')) ) {
      
      //-- Temporary copy of the inner to print.
      sTextToPrint = ""+oDocToPrintDiv.innerHTML;
      
      //------------------------------------------------------------------------ 
      // Replace name and id tag by gsprintname and gsprintid tag to
      // avoid have dom doublons by the innerHtml copy value.
      // NOTE: if {name = } or {id = }
      //------------------------------------------------------------------------
      myRegExp = /(<[^>]*)name( *=[^>]*>)/gi;
      txtFormated=sTextToPrint.replace(myRegExp, "$1gsprintname$2");
      
      myRegExp = /(<[^>]*)id( *=[^>]*>)/gi;
      txtFormated2=txtFormated.replace(myRegExp, "$1gsprintid$2");
      
      //-- Copy info to print
      oDataPrintDiv.innerHTML  = txtFormated2;
      return true;
   
   } else {
    if ( oDataPrintDiv && oDataPrintDiv.innerHTML) oDataPrintDiv.innerHTML  = '';
    return false;
   }
}

//------------------------------------------------------------------------------
// $Log: gsite.js,v $
// Revision 1.19  2007-10-24 14:54:16  bouba
// add in standart  gsRowOver and gsRowOut for change color on ligne's
// mouse over.
//
// Revision 1.18  2005-05-25 12:00:45  autran
// Modifiy < managePrint > function.
//
// Revision 1.17  2005/04/18 17:37:05  autran
// Add two function to manage the print of tag document gsite page :
// - printThisPage () : launch print.
// - managePrint ()   : plan the print.
//
// Revision 1.16  2005/02/23 17:22:36  autran
// No change.
//
// Revision 1.13  2004/09/15 22:41:19  bouba
// big modification on openBrowser for pretty editor.
//
// Revision 1.12  2003/10/28 11:56:13  lc
// Add menubar argument to openPopup
//
// Revision 1.11  2003/10/28 09:33:48  lc
// Add gs_getElementLeft and gs_getElementTop
//
// Revision 1.10  2003/09/18 20:13:32  lc
// openPopup : no location, no menubar
//
// Revision 1.9  2003/04/22 15:03:07  lc
// Div management.
//
// Revision 1.8  2003/04/01 14:43:45  lc
// Add gsGetDivByName
//
// Revision 1.7  2002/11/21 11:49:49  lc
// Bug on month nullity check.
// Add nullity check into date comparison.
//
// Revision 1.6  2002/11/08 11:25:21  lc
// Add function openBasicPopup
// Bug in openPopup for arguments count ?
//
// Revision 1.5  2002/09/13 07:51:08  lc
// LC0043 : add a popup to show infos about an user
//
// Revision 1.4  2002/09/12 10:49:56  lc
// Add popup name in openPopup function.
//
// Revision 1.3  2002/06/19 11:22:06  lc
// Bug LC0024.
// Add default values for height and width.
//
// Revision 1.2  2002/06/18 13:20:52  lc
// Add openMessenger.
//
// Revision 1.1  2002/06/14 07:39:34  lc
// Initial revision
//
//-- End of source  ------------------------------------------------------------
