var divNews = document.getElementById('news-accueil');

var divAgenda = document.getElementById('agenda');
var divActus = document.getElementById('actus');
var divCulture = document.getElementById('culture');

var divContenuAgenda = document.getElementById('contenuAgenda');
var divContenuActus = document.getElementById('contenuActus');
var divContenuCulture = document.getElementById('contenuCulture');

function replierNews() {
    //divAgenda.style.position = 'static';
    // divAgenda.style.zIndex = '1';

	//divActus.style.position = 'absolute';
	// divActus.style.zIndex = '1';
	//divActus.style.top = '0px';
	divActus.style.background = 'transparent';
	//divActus.style.height = '1px';
	divContenuActus.style.display = 'none';
	
	//divVieEtudiante.style.position = 'absolute';
	// divVieEtudiante.style.zIndex = '1';
	//divVieEtudiante.style.top = '0px';
	divCulture.style.background = 'transparent';
	//divVieEtudiante.style.height = '1px';
	divContenuCulture.style.display = 'none';
}

function afficherAgenda() {
    divContenuAgenda.style.display = 'block';
    divContenuActus.style.display = 'none';
    divContenuCulture.style.display = 'none';
	
	divNews.getElementsByTagName('a')[0].style.backgroundImage = 'url(/fileadmin/templates/v1/images/flexibles/onglet-actif-first.gif)';
	divNews.getElementsByTagName('a')[1].style.backgroundImage = 'url(/fileadmin/templates/v1/images/flexibles/onglet-inactif.gif)';
	divNews.getElementsByTagName('a')[2].style.backgroundImage = 'url(/fileadmin/templates/v1/images/flexibles/onglet-inactif-last.gif)';
	
	return false;
}

function afficherActus() {
    divContenuAgenda.style.display = 'none';
    divContenuActus.style.display = 'block';
    divContenuCulture.style.display = 'none';
	
	divNews.getElementsByTagName('a')[0].style.backgroundImage = 'url(/fileadmin/templates/v1/images/flexibles/onglet-inactif-first.gif)';
	divNews.getElementsByTagName('a')[1].style.backgroundImage = 'url(/fileadmin/templates/v1/images/flexibles/onglet-actif.gif)';
	divNews.getElementsByTagName('a')[2].style.backgroundImage = 'url(/fileadmin/templates/v1/images/flexibles/onglet-inactif-last.gif)';
	
	return false;
}

function afficherCulture() {
    divContenuAgenda.style.display = 'none';
    divContenuActus.style.display = 'none';
    divContenuCulture.style.display = 'block';
	
	divNews.getElementsByTagName('a')[0].style.backgroundImage = 'url(/fileadmin/templates/v1/images/flexibles/onglet-inactif-first.gif)';
	divNews.getElementsByTagName('a')[1].style.backgroundImage = 'url(/fileadmin/templates/v1/images/flexibles/onglet-inactif.gif)';
	divNews.getElementsByTagName('a')[2].style.backgroundImage = 'url(/fileadmin/templates/v1/images/flexibles/onglet-actif-last.gif)';
	
	return false;
}