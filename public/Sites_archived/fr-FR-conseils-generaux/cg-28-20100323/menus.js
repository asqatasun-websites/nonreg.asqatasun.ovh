var nua, d, dom, OP4; 
nua = navigator.userAgent;
d = document;
dom = document.getElementById;
OP4 = ((nua.indexOf('Opera\/4')!=-1) || (nua.indexOf('Opera 4')!=-1));

 /* v3.0 for MS IE 4.0 for Windows and corresponding MS IE for Mac browsers */
function findMenuObjectFromId(n, d) {
  var p,i,x;
  if(!d) d=document;
  if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);
  }
  if(!(x=d[n])&&d.all)
    x=d.all[n];
  for (i=0;!x&&i<d.forms.length;i++)
    x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++)
    x=findMenuObjectFromId(n,d.layers[i].document);
  return x;
}

function hideMenu() {/* v3.0 */
  if (!d.layers) {
    /* for all browsers except Netscape 4.x. */
    var i,v,obj,args=hideMenu.arguments;
    if (dom && !OP4) {
      /* for all DOM1 supporting browsers except for Opera 4.x */
      for (i=0; i<(args.length); i++) {
        var Cmenu = document.getElementById(args[i]);
        Cmenu.style.visibility = 'hidden';
      }
    }
    else if (d.all) {
      /* for MS IE 4.0 for Windows and corresponding MS IE for Mac browsers,
      /* which support the proprietary DHTML of MS IE */
      for (i=0; i<(args.length); i++) {
        if ((obj=findMenuObjectFromId(args[i]))!=null) { 
          if (obj.style) {
            obj=obj.style; 
          }
          obj.visibility='hidden'; 
        }
      }
    }
  }
  else {
    /* for Netscape 4.x and browsers, which don't support either DOM1
       or proprietary DHTML of MS IE don't do anything */
  }
}
function showMenu() {/* v3.0 */
  if (!d.layers) {
    /* for all browsers except Netscape 4.x. */
    var i,v,obj,args=showMenu.arguments;
    if (dom && !OP4) {
      /* for all DOM1 supporting browsers except for Opera 4.x */
      for (i=0; i<(args.length); i++) {
        var Cmenu = document.getElementById(args[i]);
        Cmenu.style.visibility = 'visible';
      }
    }
    else if (d.all) {
      /* for MS IE 4.0 for Windows and corresponding MS IE for Mac browsers,
      /* which support the proprietary DHTML of MS IE */
      for (i=0; i<(args.length); i++) {
        if ((obj=findMenuObjectFromId(args[i]))!=null) { 
          if (obj.style) {
            obj=obj.style; 
          }
          obj.visibility='visible'; 
        }
      }
    }
  }
  else {
    /* for Netscape 4.x and browsers, which don't support either DOM1
       or proprietary DHTML of MS IE don't do anything */
  }
}


function showInlineVMenu(menuId, currentId) {
  var d = document.getElementById(currentId);
  var menu=document.getElementById(menuId);
  var found=0;
  var foundInCurrentSubmenu=0;
  var menu_items=menu.getElementsByTagName('dd');
  for (var i = 0; i<menu_items.length; i++) {
    foundInCurrentSubmenu=0;
    submenu=menu_items[i];
    submenuItems=submenu.getElementsByTagName('li');
    for(var j=0; j<submenuItems.length; j++) {
      anchor=submenuItems[j].getElementsByTagName('a')[0];
      if (!found && (!d || d == submenu) && anchor.href == location) {
        d=submenu;
        found=1;
        foundInCurrentSubmenu=1;
        anchor.className='inline_v_submenu_current';
      } else {
        anchor.className='dummy';
      }
    }
    submenu.style.display='none';
  } 
  if (d) {d.style.display='block';}
}
