Fx.Scroll2 = new Class({
 
    'Extends': Fx.Scroll,
    'styleString': Element.getComputedStyle,
    'styleNumber': function(element, style) {
        return this.styleString(element, style).toInt() || 0;
    },
    'borderBox': function(element) {
        return this.styleString(element, '-moz-box-sizing') == 'border-box';
    },
    'topBorder': function(element) {
        return this.styleNumber(element, 'border-top-width');
    },
    'leftBorder': function(element) {
        return this.styleNumber(element, 'border-left-width');
    },
    'isBody': function(element) {
        return (/^(?:body|html)$/i).test(element.tagName);
    }, 
    'toElement': function(el) {
        var offset   = {x: 0, y: 0};
        var element  = $(el);
       
        if (this.isBody(element)) {
            return offset;
        }
        var scroll = element.getScrolls();
               
        while (element && !this.isBody(element)){
            offset.x += element.offsetLeft;
            offset.y += element.offsetTop;
           
            if (Browser.Engine.gecko){
                if (!this.borderBox(element)){
                    offset.x += this.leftBorder(element);
                    offset.y += this.topBorder(element);
                }
                var parent = element.parentNode;
                if (parent && this.styleString(parent, 'overflow') != 'visible'){
                    offset.x += this.leftBorder(parent);
                    offset.y += this.topBorder(parent);
                }
            } else if (Browser.Engine.trident || Browser.Engine.webkit){
                offset.x += this.leftBorder(element);
                offset.y += this.topBorder(element);
            }
 
            element = element.offsetParent;
            if (Browser.Engine.trident) {
                while (element && !element.currentStyle.hasLayout) {
                    element = element.offsetParent;
                }
            }
        }
        if (Browser.Engine.gecko && !this.borderBox(element)){
            offset.x -= this.leftBorder(element);
            offset.y -= this.topBorder(element);
        }
       
        var relative = this.element;
        var relativePosition = (relative && (relative = $(relative))) ? relative.getPosition() : {x: 0, y: 0};
        var position = {x: offset.x - scroll.x, y: offset.y - scroll.y};
       
        return this.start(position.x - relativePosition.x, position.y - relativePosition.y);
    }
});

var fmcScrollTo = new Class({
	Implements: Options,
	options: {
		container: document.body,
		slides: [],
		startIndex: 0,
		wrap: true,
		duration:1500,
		onShow: Class.empty //Mootools 1.2: $empty
	},
	initialize: function(options){
		//this.options = 
		this.setOptions(options);
		this.startIndex = this.options.startIndex;
		this.scroll = new Fx.Scroll2(this.options.container, {
			wait: false,
			duration: this.options.duration,
			offset: {'x': 0, 'y': 0}
			//transition: Fx.Transitions.Expo.easeOut
		});
		if(options.onShow) this.onShow = options.onShow;
	},
	scrollToEl: function(itemToScrollTo)
	{
		this.startIndex = itemToScrollTo;
		this.scroll.toElement(this.options.slides[itemToScrollTo]);
		if(this.onShow){
			this.onShow();
		}
	}
});

var fadeImages = {
	actual:0,
	prev:false,
	on:false,
	init:function(options)
	{
		this.on=true;
		this.options = options;
		this.elms = options.elements;
		this.container = options.container;
		this.elms.each(function(el, index)
		{
			el.setStyles({'position':'absolute', opacity:0});
		});
		this.newImage = this.elms[this.actual]; 
		this.oldImage = this.elms[this.prev];
		this.fadeInOut();
		this.timer = this.fadeInOut.periodical(6000, this);
	}, fadeInOut:function()
	{
		this.newImage.set('tween', {duration: 'long'});
		this.newImage.tween('opacity', 1);
		if(this.oldImage)
		{
			this.oldImage.set('tween', {duration: 'long'});
			this.oldImage.fade(0);
		}
		this.prev = this.actual;
		this.actual++;
		if(this.actual ==this.elms.length)
		{
			this.actual = 0;
		}
		this.newImage = this.elms[this.actual];
		this.oldImage = this.elms[this.prev];
	}
};

function checkBTNS(startIndex, steps, stepsLength, el)
{
	var minus = false;
	var plus = false;
	//alert(Math.ceil((startIndex+stepsLength) / stepsLength));
	if(Math.ceil((startIndex+stepsLength) / stepsLength) == steps)
	{
		plus = false;
		el.next.addClass('disabled');
	}else{
		plus = true;
		el.next.removeClass('disabled');
	}
	if(startIndex == 0)
	{
		el.prev.addClass('disabled');
	}else{
		el.prev.removeClass('disabled');
	}
}
