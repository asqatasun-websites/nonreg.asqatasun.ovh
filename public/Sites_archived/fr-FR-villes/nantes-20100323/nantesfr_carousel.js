(function($){
	$(".stepcarousel")
		.each(function(){
			if (this.id == ""){
				var id = "carousel" + (new Date()).getTime();
				this.id = id;
			}
			stepcarousel.setup({
				galleryid: this.id, //id of carousel DIV
				beltclass: 'belt', //class of inner "belt" DIV containing all the panel DIVs
				panelclass: 'panel', //class of panel DIVs each holding content
				autostep: {enable:true, moveby:1, pause:9000},
				panelbehavior: {speed:1000, wraparound:false, persist:true},
				defaultbuttons: {enable: true, moveby: 1, leftnav: [pathToStatic + '/images/carousel/biais_ok.png', 0, 0], rightnav: [pathToStatic + '/images/carousel/flechedroite.png', -22, 356]},
				statusvars: ['statusA', 'statusB', 'statusC'], //register 3 variables that contain current panel (start), current panel (last), and total panels
				contenttype: ['inline'] //content setting ['inline'] or ['external', 'path_to_external_file']
			});
		})
	;
})(jQuery);
