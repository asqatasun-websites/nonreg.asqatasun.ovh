/* ========== OFFRES D'EMPLOI ========== */
function updateOffreEmploiAction(emploiActionForm, offreActionInRequest)
{
	var tagsBouton=emploiActionForm.childNodes;
	  for (var i=0;i<tagsBouton.length;i++)
	  {
		curElement = tagsBouton.item(i);
		if (curElement.nodeName == "INPUT")
		{
			if (curElement.name == offreActionInRequest)
			{	
				curElement.className ="boutonsEmploiActionActif";
			}
			else
			{	
				curElement.className ="boutonsEmploiActionInactif";
			}
		}	
	}
}

/* ========== PUBLICATIONS PAR CATEGORIE ========== */
function filtrePublicationsParCategorie(selectedAnchorId)
{
	var formPublications = document.forms["CategoriePublicationsForm"];
	var conteneurCategoriesPublications=document.getElementById("categoriesPublications");
	var filtreLabelList=conteneurCategoriesPublications.childNodes;
	for (var i=0;i<filtreLabelList.length;i++)
	{
		var curCategoriePublications = filtreLabelList.item(i);
		if (curCategoriePublications.nodeName == "A")
		{
			var categorieId = curCategoriePublications.id;
			if (categorieId == selectedAnchorId)
			{	
				formPublications.elements["categoriePublicationsSelectionnee"].value=categorieId;
			}		
		}
	}
	formPublications.submit();
}	

function afficherToutesLesPublications()
{
	var formPublications = document.forms["CategoriePublicationsForm"];
	formPublications.elements["categoriePublicationsSelectionnee"].value="";
	formPublications.submit();	
}

/* ========== PRESSE PAR CATEGORIE ========== */
function filtrePresseParCategorie(selectedAnchorId)
{
	var formPresse = document.forms["CategoriePresseForm"];
	var conteneurCategoriesPresse=document.getElementById("categoriesPresse");
	var filtreLabelList=conteneurCategoriesPresse.childNodes;
	for (var i=0;i<filtreLabelList.length;i++)
	{
		var curCategoriePresse = filtreLabelList.item(i);
		if (curCategoriePresse.nodeName == "A")
		{
			var categorieId = curCategoriePresse.id;
			if (categorieId == selectedAnchorId)
			{	
				formPresse.elements["categoriePresseSelectionnee"].value=categorieId;
			}		
		}
	}
	formPresse.submit();
}	

function afficherTousLesCommuniques()
{
	var formPresse = document.forms["CategoriePresseForm"];
	formPresse.elements["categoriePresseSelectionnee"].value="";
	formPresse.submit();	
}

/* ========== BOITE A LIENS PAR CATEGORIE ========== */
function filtreBoiteLiensParCategorie(selectedAnchorId)
{
	var formBoiteLiens = document.forms["CategorieBoiteLiensForm"];
	var conteneurCategoriesBoiteLiens=document.getElementById("categoriesBoiteLiens");
	var filtreLabelList=conteneurCategoriesBoiteLiens.childNodes;
	for (var i=0;i<filtreLabelList.length;i++)
	{
		var curCategorieBoiteLiens = filtreLabelList.item(i);
		if (curCategorieBoiteLiens.nodeName == "A")
		{
			var categorieId = curCategorieBoiteLiens.id;
			if (categorieId == selectedAnchorId)
			{	
				formBoiteLiens.elements["categorieBoiteLiensSelectionnee"].value=categorieId;
			}		
		}
	}
	formBoiteLiens.submit();
}	

function afficherTousLesLiens()
{
	var formBoiteLiens = document.forms["CategorieBoiteLiensForm"];
	formBoiteLiens.elements["categorieBoiteLiensSelectionnee"].value="";
	formBoiteLiens.submit();	
}

/* ========== ACTUALITES PORTAIL ========== */
function updateFiltreActualitePortailInputColor(selectedAnchorId)
{
	var formActualite = document.forms["filtreCategorieActualitePortailForm"];
	var conteneurCategoriesActualite=document.getElementById("categoriesActualite");
	var filtreLabelList=conteneurCategoriesActualite.childNodes;
	for (var i=0;i<filtreLabelList.length;i++)
	{
		var curCategorieActualite = filtreLabelList.item(i);
		if (curCategorieActualite.nodeName == "A")
		{
			var categorieId = curCategorieActualite.id;
			if (categorieId == selectedAnchorId)
			{	
				formActualite.elements["categorieActualitePortailSelectionnee"].value=categorieId;
			}		
		}
	}
	formActualite.submit();
}	

function afficherToutesLesActualites()
{
	var formActualite = document.forms["filtreCategorieActualitePortailForm"];
	formActualite.elements["categorieActualitePortailSelectionnee"].value="";
	formActualite.submit();	
}
