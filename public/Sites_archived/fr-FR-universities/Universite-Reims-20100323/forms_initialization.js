/*************************************************************************
	F O N C T I O N S   D E   S E L E C T I O N   A U T O M A T I Q U E 
					(select, radio, checkbox)
**************************************************************************/	
function selectByValue(obj,val)
{
	for (i=0; i < obj.options.length; i++)
	{
		if(obj.options[i].value==val)
		{
			obj.selectedIndex=i;
			return;
		}
	}
	//Not Found
	obj.selectedIndex=0;
}

function checkSingle(obj,val)
{
	if(val > 0)
	{
		obj.checked=true;
	}else
	{
		obj.checked=false;	
	}
}

function putCheckValue(check,hidden,state)
{
	if(hidden.value=="")
	{
		check.checked=state;
		getCheckValue(check,hidden);
	}else if(hidden.value==0)
	{ 
		check.checked=false;
	}else
	{ 
		check.checked=true;
	}		
}

function getCheckValue(check,hidden)
{
	if(check.checked==true)
	{
		hidden.value=check.value;
	}else
	{ 
		hidden.value=0;
	}
}

function checkByValue(obj,val) {
	if (obj != null && obj.length > 0) {
		for (i=0; i < obj.length; i++) {
			if(obj[i].value==val) {
				obj[i].checked=true;
				return;
			}
		}
	}
	else {
		if (obj != null) {
			if(obj.value==val) {
				obj.checked=true;
			}
		}
	}
}