// Groupama namespace
if(GA === undefined) {
	var GA = {};
}
/*
(function() {
	
	// cookie
	var setCookie, getCookie;
	
	setCookie = function(name, value, days) {
		if(days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = '; expires='+date.toGMTString();
		} else  {
			expires = '';
		}
		document.cookie = name+'='+value+expires+'; path=/';
	};
	GA.setCookie = setCookie;
	
	getCookie = function(name) {
		var cookie = document.cookie.split(';');
		for(var i = 0; i < cookie.length; i++) {
			if(cookie[i].indexOf(name) != -1) {
				var str = cookie[i];
				break;
			}
		}
		if(!str) {return null;}
		var cut = str.indexOf('=')+1;
		return str.substring(cut);
	};
	GA.getCookie = getCookie;
	
})();
*/
$(document).ready(function() {
  // inform js dispo for css
  $(document.body).addClass('js');
  
  // change typo size
  /*var lastTypoSize;
  
  var changeSize = function(size) {
    if(lastTypoSize) {
     $('a.' + lastTypoSize + ' img', changeSizeRef).remove();
     $(document.body).removeClass(lastTypoSize);
    }
    $('a.' + size, changeSizeRef).append(changeSizeImg);
    $(document.body).addClass(size);
    lastTypoSize = size;
    GA.setCookie('GAtypo', size);
  };
  
  var changeSizeClick = function(e) {
    var t = e.target, cls;
    if(t.nodeName == 'A' && (cls = t.className)) {
      switch(cls) {
        case 'petite-taille':
          changeSize('petite-taille');
          break;
        case 'taille-moyenne':
          changeSize('taille-moyenne');
          break;
        case 'grande-taille':
          changeSize('grande-taille');
      }
      e.preventDefault();
    }
  };
  
  var changeSizeHTML = '<li id="taille-texte">'+
    '<span>'+i18n._('changeSize', 'title')+'</span>'+
    ' <a class="petite-taille" href="#haut" title="'+i18n._('changeSize', 'small')+'">A</a>'+
    ' <a class="taille-moyenne" href="#haut" title="'+i18n._('changeSize', 'middle')+'">A</a>'+
    ' <a class="grande-taille" href="#haut" title="'+i18n._('changeSize', 'large')+'">A</a>'+
  '</li>';
  
  var changeSizeImg = '<img src="../images/fleche-taille-texte.gif" alt="" width="9" height="5" />';
  
  var changeSizeRef = $(changeSizeHTML).appendTo('#espace-presse-english').click(changeSizeClick);
  
  var cookieTypo = GA.getCookie('GAtypo');
  if(cookieTypo) {
    lastTypoSize = cookieTypo;
    changeSize(cookieTypo);
  } else {
    changeSize('taille-moyenne');
  }*/
  
  // changement produits-services-pays
  var produitsServices = $('#produits-services-pays');
  var imageProduitsServicesContinentsHolder = produitsServices.next().next();
  
  // changement image-produits-services-continents
  
  var manageImageProduitsServicesContinents = function(e) {
    var t = e.target;
    while(t.nodeName != 'A' && t != this) {t = t.parentNode;}
    if(t.nodeName == 'A' && t.rel) {
      $.get(t.href, function(datas) {
        imageProduitsServicesContinentsHolder.attr('tabindex', '-1').focus().get(0).innerHTML = datas;
        imageProduitsServicesContinentsHolder.attr('class', t.rel);
      });
      e.preventDefault();
    }
  };
  
  produitsServices.next().click(manageImageProduitsServicesContinents);
  
  // changement produits-services-pays continents
  if (produitsServices.hasClass('continents')) {
    var produitsServicesContinentsHolder = produitsServices.next();
    var lastProduitServiceContinents;
    
    var manageProduitsServicesContinents = function(e) {
      var t = e.target;
      while(t.nodeName != 'A' && t != this) {t = t.parentNode;}
      if(t.nodeName == 'A' && t.rel) {
        $.get(t.href, function(datas) {
          produitsServicesContinentsHolder.attr('tabindex', '-1').focus().get(0).innerHTML = datas;
          if(lastProduitServiceContinents) {
            $(lastProduitServiceContinents).removeClass('produit-service-actif');
          }
          lastProduitServiceContinents = t.parentNode;
          $(lastProduitServiceContinents).addClass('produit-service-actif');
          produitsServicesContinentsHolder.attr('class', 'continents '+t.rel);
          
          // image-produits-services-continents : r�cup�ration du 1er rel pour la classe
          var produitsServicesContinentsChildren = produitsServicesContinentsHolder.children('UL');
          var firstChild = $(':first-child', produitsServicesContinentsChildren[0]);
          var firstA = firstChild.children('A')[0]
          var firstRel = firstA.rel;
          var ajaxUrl = (firstRel)? firstA.href: 'produits-services-continents-pays.php?pays=ca';
          $.get(ajaxUrl, function(datas) {
            imageProduitsServicesContinentsHolder.get(0).innerHTML = datas;
            imageProduitsServicesContinentsHolder.attr('class', firstRel);
          });
        });
        e.preventDefault();
      }
    };
    
    produitsServices.click(manageProduitsServicesContinents);
    lastProduitServiceContinents = $('li.produit-service-actif', produitsServices).get(0);
  }
  
  // changement produits-services-pays
  else {
    var produitsServicesHolder = $('#produits-services-pays').next();
    var lastProduitService;
    
    var manageProduitsServices = function(e) {
      var t = e.target;
      while(t.nodeName != 'A' && t != this) {t = t.parentNode;}
      if(t.nodeName == 'A' && t.lang) {
        $.get(t.href, function(datas) {
          produitsServicesHolder.attr('tabindex', '-1').focus().get(0).innerHTML = datas;
          if(lastProduitService) {
            $(lastProduitService).removeClass('produit-service-actif');
          }
          lastProduitService = t.parentNode;
          $(lastProduitService).addClass('produit-service-actif');
          produitsServicesHolder.attr({
            'xml:lang': t.lang,
            'lang': t.lang
          });
        });
        e.preventDefault();
      }
    };
    
    produitsServices.click(manageProduitsServices);
    lastProduitService = $('li.produit-service-actif', produitsServices).get(0);
  }
  
  // change ul li in drop down
  var manageDropDownClick = function(e) {
    var t = e.target;
	if(t.parentNode.parentNode.parentNode.id == "faq-selecteur" || t.parentNode.parentNode.parentNode.id == "resultats-rapports-selecteur"){
	  if(t.nodeName == 'A' && t.parentNode.nodeName == 'H4') {
        $(this).hasClass('open') ? closeDropDown(this) : openDropDown(this);
        e.preventDefault();
      }
	}
	else{
      if(t.nodeName == 'A' && t.parentNode.nodeName == 'H3') {
        $(this).hasClass('open') ? closeDropDown(this) : openDropDown(this);
        e.preventDefault();
      }
	}
  };
  
  var openDropDown = function(obj) {
    var classFauxSelect = $(obj).attr('class').match(/\bfaux-select(-int)?\b/)[0];
    if (!classFauxSelect) {return;}
    
    $(obj).addClass('open');
    $(document).bind('keyup.dropDown', function(e) {
			if(e.keyCode == 27) {closeDropDown(obj);}
		});
    $(document).bind('mousedown.dropDown', function(e) {
			var t = e.target;
			while(t.parentNode && !$(t).hasClass(classFauxSelect)) {
				t = t.parentNode;
			}
			if(!t || (t && t != obj)) {closeDropDown(obj);}
		});
  };
  
  var closeDropDown = function(obj) {
    $(obj).removeClass('open');
    $(document).unbind('keyup.dropDown');
    $(document).unbind('mousedown.dropDown');
  };
  
  /** Gestion du scroll dans le menu du footer */
  $('div.faux-select').click(manageDropDownClick).each(function(index) {  	           
    var ul = $('h3', this).wrapInner('<a href="#"></a>').next();      
    if(ul[0].offsetHeight > 100) {
      ul.addClass('scroll');
    }    
    var ul = $('ul', this);
    if(ul[0].offsetHeight > 100) {
      ul.addClass('scroll');
    }    
  });
  
  $('div.faux-select-int').click(manageDropDownClick).each(function(index) {   	
    if(this.parentNode.id == "faq-selecteur" || this.parentNode.id == "resultats-rapports-selecteur"){
	  var ul = $('h4', this).wrapInner('<a href="#"></a>').next();
	}
	else{
	  var ul = $('h3', this).wrapInner('<a href="#"></a>').next();
	}
    if(ul[0].offsetHeight > 200) {
      ul.addClass('scroll');
    }
  });
  
  // angles arrondis
  var displayCorners = function(selector, cls, pos){
    var firstLi = $($(selector).get(0));
    if (firstLi.hasClass(cls)) {
      var lk = firstLi.children().append('<span class="u' + pos + 'c"></span><span class="b' + pos + 'c"></span>');
      if (window.ie6 !== undefined) {
        var blc = lk[0].lastChild;
        blc.style.top = lk[0].offsetHeight - blc.offsetHeight + 'px';
      }
    }
  };
  //displayCorners('#rubriques li', 'rubrique-actuelle', 'l');
  //displayCorners('#rubriques li:last', 'rubrique-actuelle', 'r');
  displayCorners('#rubriques li', 'premier', 'l');
  displayCorners('#rubriques li:last', 'dernier', 'r');
  
  
  // onglets article
  var lastActiveLi = $('#onglets-span li.onglet-actuel'),
      lastActiveId = $('a', lastActiveLi).attr('href'),
      lastActiveTab = $(lastActiveId);
  
  var displayTab = function(t) {
    if(lastActiveTab.length) {
      hideTab();
    }
    var id = $(t).attr('href');
    lastActiveTab = $(id).removeClass('onglet-masque').addClass('onglet-affiche').attr('tabindex', '-1');
    if( displayTab.arguments.length==1 ){ // Le focus est positionn� que si l'appel est fait depuis l'evt. click()
    	$(id).focus();
    }
    lastActiveLi = $(t.parentNode).addClass('onglet-actuel');
  };
  
  var hideTab = function() {
    lastActiveLi.removeClass('onglet-actuel');
    lastActiveTab.addClass('onglet-masque').removeClass('onglet-affiche');
    lastActiveTab.removeAttr('tabindex');
  };
  
  $('#onglets-span ul').click(function(e) {
    var t = e.target;
    var l = t.parentNode;
    if(t.nodeName == 'SPAN' && $(l).attr('href').indexOf('#') === 0) {
      displayTab(l);
      e.preventDefault();
    }
  });
  
  // premier onglet actif par d�faut
  var firstLi = $('#onglets-span li:first-child');
  var firstId = $('a', firstLi);
  var firstTab = $(firstId);
  firstLi.addClass('onglet-actuel');
  displayTab(firstId,false); // sans positionnement de focus
  lastActiveLi = firstLi;
  
  displayCorners('#onglets-span li', 'premier', 'l');
  displayCorners('#onglets-span li:last', 'dernier', 'r');
  
  // visu interactive affichage bloc implantation pays
  var lastActiveArea = $('a.zone-active', '#zones-implantations'),
      lastActiveLink = $(lastActiveArea).attr('href'),
      lastActiveBlock = $(lastActiveLink);
  
  var displayBlock = function(t) {
    if(lastActiveBlock.length) {
      hideBlock();
    }
    var id = $(t).attr('href');
    lastActiveBlock = $(id).removeClass('bloc-masque');
    lastActiveArea = $(t).addClass('zone-active');
  };
  
  var hideBlock = function() {
    lastActiveArea.removeClass('zone-active');
    lastActiveBlock.addClass('bloc-masque');
    lastActiveBlock.removeAttr('tabindex');
  };
  
  $('#zones-implantations').click(function(e) {	
    var link = e.target;    
    if(link.nodeName == 'IMG'){ // On a cliqu� sur l'image donc ce n'est pas un clique g�r� depuis la tabulation
    	link = link.parentNode;
	}
    if(link.nodeName == 'A' && $(link).attr('href').indexOf('#') === 0) {    	  
      displayBlock(link);    
      var id = $(link).attr('href');
  	  $(id).attr('tabindex', '-1').focus();  	
      e.preventDefault();
    }
  });

  $('#zones-implantations').keydown(function(e) {	  	  
	  var link = e.target;
	  if (e.which == 13) { // bouton entrer  
		  if(link.nodeName == 'A' && $(link).attr('href').indexOf('#') === 0) { // implantation-lien : ancre - lien sur l'image puce			 
			  var id = $(link).attr('href');			 
			 $(id).attr('tabindex', '-1').focus(); // Positionnement du focus sur le d�tails de la zone d'implantation			  
		  }
	  } 
  });
  
  $('#zones-implantations').keyup(function(e){
	  var link = e.target;
	  if (e.which == 9) { // bouton tabulation				  
		  if(link.nodeName == 'A' && $(link).attr('href').indexOf('#') === 0) { // implantation-lien : ancre - lien sur l'image puce			  			  			 
			  lastActiveArea.removeAttr('tabindex');			 						
			 displayBlock(link);		     
		  }		 
	  }
  });

  $('#zones-implantations-details a').blur(function(e){ 	  
	  var link = e.target;
	  if(lastActiveBlock.length) { // Il y a un bloc details de s�lectionner		  
		  var lastLink = $('a:last', lastActiveBlock); // R�cup�ration du dernier lien du bloc d�tails de la zone d'implantation
		  if(lastLink.length){ // Il existe un dernier lien dans le bloc d�tails de la zone d'implantation
			  if( link == $(lastLink).get(0) ){ // Le lien en cours est le dernier lien du bloc d�tails de la zone d'implantation			   					
				   lastActiveBlock.removeAttr('tabindex');					   
				   $(lastActiveArea).attr('tabindex', '-1').focus();				   
			  }
		  }	  
	  }
	  
  });

  //$(document.body).append('<div id="debugWindow" style="position: absolute; background-color: red; top:0px; left:900px;"></div>');
  var debugWindow = function(text){
	  $('#debugWindow').append(text+'<br>');
  };
  
  /* Evolution Accessibilit� : On n'alimente plus le alt de l'image pour la rubrique groupama dans le monde.
  if(document.getElementById('zones-implantations')){
    var maplinks = document.getElementById('zones-implantations').getElementsByTagName("img");
	for(var i=0, maplink; maplink = maplinks[i]; i++){
	  maplink.alt = maplink.parentNode.getElementsByTagName("span")[0].innerHTML;
	}
  }
  */
  
  // FAQ
  var lastActiveDlFaq = $('#faq-questions h3.question-actuel'),
      lastActiveIdFaq = $('a', lastActiveDlFaq).attr('href'),
      lastActiveListFaq = $(lastActiveIdFaq);
  
  var displayFaq = function(t) {
    if(lastActiveListFaq.length) {
      hideFaq();
    }
    var Faqid = $(t).attr('href');
    lastActiveListFaq = $(Faqid).removeClass('question-masque').attr('tabindex', '-1').focus();
    lastActiveDlFaq = $(t.parentNode).addClass('question-actuel');
  };
  
  var hideFaq = function() {
    lastActiveDlFaq.removeClass('question-actuel');
    lastActiveListFaq.addClass('question-masque');
    lastActiveListFaq.removeAttr('tabindex');
  };
  
  $('#faq-questions div').click(function(e) {
    var t = e.target;
    if(t.nodeName == 'A' && $(t).attr('href').indexOf('#') === 0) {
      displayFaq(t);
      e.preventDefault();
    }
  });
  
  // Page agenda
  var lastActiveTdAgenda = $('#agenda tr.evenement-actuel'),
      lastActiveIdAgenda = $('a', lastActiveTdAgenda).attr('href'),
      lastActiveTabAgenda = $(lastActiveIdAgenda);
  
  var displayAgenda = function(t) {
    if(lastActiveTabAgenda.length) {
      hideAgenda();
    }
    var id = $(t).attr('href');
    lastActiveTabAgenda = $(id).removeClass('evenement-masque').attr('tabindex', '-1').focus();
    lastActiveTdAgenda = $(t.parentNode.parentNode).addClass('evenement-actuel');
  };
  
  var hideAgenda = function() {
    lastActiveTdAgenda.removeClass('evenement-actuel');
    lastActiveTabAgenda.addClass('evenement-masque');
    lastActiveTabAgenda.removeAttr('tabindex');
  };
  
  $('#agenda td').click(function(e) {
    var t = e.target;
    if(t.nodeName == 'A' && $(t).attr('href').indexOf('#') === 0) {
      displayAgenda(t);
      e.preventDefault();
    }
  });
  
  // Formulaire de recherche
  var inputRecherche = $('#recherche');
  var valueRecherche = inputRecherche.attr('value');
  
  inputRecherche.click(function() {
    // if (valueRecherche == 'Recherche') inputRecherche.attr('value', '');
    inputRecherche.attr('value', '');
  });
  
  // champs textes
  $('#contenu input').focus(function(e) {
	var v = this.value;
	if(!(document.getElementById("RSS-conteneur"))){
		if(this.type == "text"){
			if(v == this.defaultValue){
				this.value = "";
			}
		}
	}
  });
  
  // Archives
  GA.FauxFields.Factory({
    context: 'archives-selection',
	field: 'select',
	maxHeight: 250
  });
  
  // Page Recherche
  GA.FauxFields.Factory({
    context: 'recherche-selection',
	field: 'select',
	maxHeight: 250
  });
  
  // Contact
  GA.FauxFields.Factory({
    context: 'contact-formulaire',
	field: 'select',
	maxHeight: 250
  });
  
  // Envoyer
  GA.FauxFields.Factory({
    context: 'envoyer-formulaire',
	field: 'select',
	maxHeight: 250
  });
  
  // Panier
  GA.FauxFields.Factory({
    context: 'panier-compte-inscription',
	field: 'select',
	maxHeight: 250
  });
  GA.FauxFields.Factory({
    context: 'panier-telechargement-hd',
	field: 'select',
	maxHeight: 250
  });
  
  // Mediatheque
  GA.FauxFields.Factory({
    context: 'mediatheque-recherche',
	field: 'select',
	maxHeight: 250
  });
  
  GA.FauxFields.Factory({
    context: 'mediatheque-telechargement-hd',
	field: 'select',
	maxHeight: 250
  });
  
  // Carto
  GA.FauxFields.Factory({
    context: 'carto-recherche',
	field: 'select',
	maxHeight: 250
  });
  
   // Postuler offres d'emploi
  GA.FauxFields.Factory({
    context: 'rh-conteneur',
	field: 'select',
	maxHeight: 250
  });
  
/* inutile puisque nous rechargeons toute la page ! g�r� dans mediatheque_onglets.jsp  
  var lastActiveBlocMediatheque = $('#mediatheque-onglets li.mediatheque-actuel'),
      lastActiveIdMediatheque = $('a', lastActiveBlocMediatheque).attr('href'),
      lastActiveTabMediatheque = $(lastActiveIdMediatheque);
  
  var displayMediatheque = function(t) {
    if(lastActiveTabMediatheque.length) {
      hideMediatheque();
    }
    var id = $(t).attr('href');
    lastActiveTabMediatheque = $(id).removeClass('mediatheque-masque').attr('tabindex', '-1').focus();
    lastActiveBlocMediatheque = $(t.parentNode).addClass('mediatheque-actuel');
  };
  
  var hideMediatheque = function() {
    lastActiveBlocMediatheque.removeClass('mediatheque-actuel');
    lastActiveTabMediatheque.addClass('mediatheque-masque');
    lastActiveTabMediatheque.removeAttr('tabindex');
  };
  
  $('#mediatheque-onglets ul').click(function(e) {
    var t = e.target;
    if(t.nodeName == 'A' && $(t).attr('href').indexOf('#') === 0) {
      displayMediatheque(t);
      e.preventDefault();
    }
  });
  
  // mediatheque : premier onglet actif par d�faut
  var MediathequefirstLi = $('#mediatheque-onglets li:first-child');
  var MediathequefirstId = $('a', MediathequefirstLi);
  var MediathequefirstTab = $(MediathequefirstId);
  MediathequefirstLi.addClass('mediatheque-actuel');
  displayMediatheque(MediathequefirstId);
  lastActiveBlocMediatheque = MediathequefirstLi;
  */
 
  $('#RSS-conteneur .rss-section-lien').click(function(e) {
    var t = e.target;
	var l = t.parentNode;
    if(l.nodeName == 'A' && $(l).attr('href').indexOf('#') === 0) {
      e.preventDefault();
    }
  });

  // Abonnement
  GA.FauxFields.Factory({
    context: 'abonnement-compte-inscription',
	field: 'select',
	maxHeight: 200
  });
  
    if( $.browser.msie ) {
        $('input.ajout-panier').mouseover(function(){
            $(this).css({"text-decoration":"underline","cursor":"hand"})
        });
        $('input.ajout-panier').mouseout(function(){
            $(this).css({"text-decoration":"none","cursor":"hand"})
        });
    }

  /* Ensemble des liens pour les popups -
   *  
   * L'�v�nement click est positionn� sur une balise A.xxx or il peut �tre d�clench� depuis tout autre balise (ex: SPAN, ABBR, ...) 
   * contenu dans la balise A.xxx donc pour �viter tout erreur d'URL lors d'un click sur ce lien il faut passer en param�tre
   *  l'objet lien (A.xxx) seul d�tenteur de l'attribut href (chemin ou URL) et non l'objet d�clencheur de l'�v�nement  
   * 
   **/
  $('a.liens-popuptext').click(function(e) {
    var t = e.target;
    while(t.nodeName!='A'){t=t.parentNode;}	
	window.open(t, "groupamapopuptexte", "width=578, height=550")
	e.preventDefault();
  });
  $('a.liens-popupvideo').click(function(e) {
    var t = e.target;
    while(t.nodeName!='A'){t=t.parentNode;}	
	window.open(t, "groupamapopupvideo", "width=578, height=440")	
	e.preventDefault();
  });
  $('a.liens-popupaudio').click(function(e) {
    var t = e.target;
    while(t.nodeName!='A'){t=t.parentNode;}
	window.open(t, "groupamapopupaudio", "width=578, height=360")
	e.preventDefault();
  });
  $('a.liens-popuptranscript').click(function(e) {
    var t = e.target;
    while(t.nodeName!='A'){t=t.parentNode;}
	window.open(t, "groupamapopuptranscript",  "width=578, height=440, scrollbars=1")
	e.preventDefault();
  });
  $('#popup-fermer a').click(function(e) {
    window.close();
  });
  
  // sIFR
  /*if (typeof sIFR == 'function') {
    sIFR.replaceElement('#dernieres-publications h2', named({sFlashSrc: 'js/fontname.swf', sColor: '#555555', sWmode: 'transparent'}));
    sIFR.replaceElement('#documents-consultes h2', named({sFlashSrc: 'js/fontname.swf', sColor: '#555555', sWmode: 'transparent'}));
    sIFR.replaceElement('#bloc-complement-home h2', named({sFlashSrc: 'js/fontname.swf', sColor: '#FFFFFF', sWmode: 'transparent'}));
    sIFR.replaceElement('#bloc-complement h2', named({sFlashSrc: 'js/fontname.swf', sColor: '#FFFFFF', sWmode: 'transparent'}));
    sIFR.replaceElement('#bloc-liens-externes h2', named({sFlashSrc: 'js/fontname.swf', sColor: '#FFFFFF', sWmode: 'transparent'}));
    sIFR.replaceElement('#navigation-rubrique h2', named({sFlashSrc: 'js/fontname.swf', sColor: '#FFFFFF', sWmode: 'transparent'}));
  }*/
  
  /* On Positionne la taille des blocs th�matique */
  var maxHblocThematique = 0;
  var blocsThematique = $('#underfooter div.bloc-thematique');  
  for(var i=0; (i<blocsThematique.length && blocsThematique.length>1); i++){
	  var h = blocsThematique[i].clientHeight; if( h>maxHblocThematique) maxHblocThematique=h;
  }
  for(var i=0; (i<blocsThematique.length && blocsThematique.length>1); i++){ 
	  blocsThematique[i].style.height = maxHblocThematique+'px';
  }
  
});

/*
 * changeTitleOnglet() : Changement de l'attribut title lorsque l'onglet est s�lectionn�.
 * titlestart : chaine de caractere a rajouter dans le title
 **/
function changeTitleOnglet(elt, titlestart) {
	/** R�initialisation de l'ensemble des title des onglets : On enleve titlestart s'il est pr�sent */
	var liList = $('#onglets-span li');	
	for(var i=0; i<liList.length; i++){
		var li = $('a', liList[i]);
		var aTitle = $(li).attr('title') 	
		if( aTitle.indexOf(titlestart)!=-1 ){
			var aNewTitle = aTitle.substring(titlestart.length, aTitle.length);			
			$(li).attr('title', aNewTitle)
		}
	}
	/** Positionnement du titlestart dans l'onglet s�lectionner (courant) */ 
	var eltTitle = $(elt).attr('title');
	if( eltTitle.indexOf(titlestart)==-1 ){
		$(elt).attr('title', titlestart+eltTitle);
	}
}

/*
 * changeTitleCarte() : Changement de l'attribut title lorsque la zone d'implantation est s�lectionn� sur la carte
 * titlestart : chaine de caractere a rajouter dans le title
 **/
function changeTitleCarte(elt, titlestart) {
	/** R�initialisation de l'ensemble des title des zone implantation : On enleve titlestart s'il est pr�sent */
	var liList = $('#zones-implantations li');	
	for(var i=0; i<liList.length; i++){
		var li = $('a', liList[i]);
		var aTitle = $(li).attr('title') 	
		if( aTitle.indexOf(titlestart)!=-1 ){
			var aNewTitle = aTitle.substring(titlestart.length+1, aTitle.length);	// +1 car prise en compte de l'espace		
			$(li).attr('title', aNewTitle)
		}
	}
	/** Positionnement du titlestart dans l'onglet s�lectionner (courant) */ 
	var eltTitle = $(elt).attr('title');
	if( eltTitle.indexOf(titlestart)==-1 ){
		$(elt).attr('title', titlestart+' '+eltTitle);
	}
}

/*
 * changeTitleFAQ(): Changement de l'attribut title - Si on clique sur le titre du FAQ on supprime du titre le titlestart 
 * car on ouvre le calque du FAQ.
 * titlestart :chaine de caractere a rajouter dans le title
 */
function changeTitleFAQ(elt, titlestart) {
	/** R�initialisation de l'ensemble des title des zone implantation : On rajoute titlestart s'il n'est pas pr�sent */
	var hList = $('#faq-questions h4');
	for(var i=0; i<hList.length; i++){
		var h = $('a', hList[i]);
		var aTitle = $(h).attr('title') 	
		if( aTitle.indexOf(titlestart)==-1 ){	
			$(h).attr('title', titlestart+' '+aTitle)
		}
	}	
	var eltTitle = $(elt).attr('title');
	if( eltTitle.indexOf(titlestart)!=-1 ){
		var eltNewTitle = eltTitle.substring(titlestart.length+1, eltTitle.length); // +1 car prise en compte de l'espace			
		$(elt).attr('title',eltNewTitle)
	}
}

/*
 * EVOLUTION XITI:
 * La version actuelle du fichier xtcore.js (3.3.002) ne permet pas de d�finir des propri�t�s � la nouvelle fen�tre
 * cr�er par l'appel de la m�thode xt_med() sur les liens sortant avec nouvelle fen�tre.
 * Il faut donc appel� cette m�thode afin d'ouvrir la fen�tre pour qu'il y est prise en comtpe des propri�t�s de la fen�tre
 * exemple : 
 * 		onclick="xt_med(\'C\',\'7\',\'informations_financieres::resultats_et_rapports_financiers::webcast\',\'S\',\'http://www.dbee.com\', 1);" 
 * 		href="javascript:void(window.open(\'http://www.dbee.com\',\'ouverture_Popup\',\'height=300,width=300,resizable=no,toolbar=no,menubar=no\'));"   
 */
function xt_med_popup(elt){
	if(xfen!=null && !xfen.closed){
		var href=elt.href;
		if((href!=null) && (href.indexOf('javascript:void(window.open')!=-1)){
			xfen.close(); eval(elt.href);
		}
	}
}