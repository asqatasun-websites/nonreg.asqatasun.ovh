//
//
//		LIVINGACTOR FLASH JAVASCRIPT LIBRARY VERSION 1.0.0.12
//
//				copyrights La Cantoche Production
//					all rights reserved
//
//
//


/////////////////////////////////////////////////////////////////////////////////////////
// Platform Detection
/////////////////////////////////////////////////////////////////////////////////////////
var AppName = navigator.appName.toLowerCase();
var AppVer = parseFloat(navigator.appVersion);
if (AppName.indexOf("microsoft internet") != -1)
	AppVer = parseFloat(navigator.appVersion.substring((navigator.appVersion.indexOf("MSIE") + 4)));
var WIN32 = (window.navigator.platform.indexOf("Win32")!= -1);
var WIN95 = navigator.appVersion.indexOf("95")!= -1;
var NAV4  = (AppName.indexOf("netscape") != -1 && AppVer >= 4.0 && AppVer <= 4.99);
var NAV6  = (AppName.indexOf("netscape") != -1 && AppVer >= 5.0 );
var IE4   = (AppName.indexOf("microsoft internet") != -1 && AppVer >= 4.0 && AppVer < 5.0);
var IE5   = (AppName.indexOf("microsoft internet") != -1 && AppVer >= 5.0);
var IE   = (AppName.indexOf("microsoft internet") != -1 && AppVer >= 4.0 );
var SP2 = (window.navigator.userAgent.indexOf("SV1") != -1);
var DomBody=(document.documentElement && document.documentElement.clientHeight)?document.documentElement:document.body;

/////////////////////////////////////////////////////////////////////////////////////////
// Event
/////////////////////////////////////////////////////////////////////////////////////////
var LA_OnDragStart = null;
var LA_OnDragEnd   = null;
var LA_OnBookmark  = null;
var LA_OnDialogID  = null;
var LA_OnSelect    = null;
var LA_OnHide	   = null;
var LA_OnShow	   = null;
var LA_OnQuit	   = null;
var LA_OnReady     = null;
/////////////////////////////////////////////////////////////////////////////////////////
// LivingActor Status
/////////////////////////////////////////////////////////////////////////////////////////
var E_UNINITIALIZED = 0;
var E_INITIALIZED = 1;
var E_INSTALLING = 2;
var E_LOADING_ACTOR = 3;
var E_STARTED = 4;
/////////////////////////////////////////////////////////////////////////////////////////
// Global Variable
/////////////////////////////////////////////////////////////////////////////////////////
var LA_CurrentStatus = E_UNINITIALIZED;
var ACTOR = null;
var GENERIC_FLASH_PLAYER = null;
var LA_ACTOR_URL = ""; //internal use
var LA_UserDefined = ""; //internal use
var LA_DRAG_THREAD_ID = 0;
var LA_DIALOG_VISIBILITY = true;
var LA_FISRTPLAYERUPDATE = true;
var LA_UID = new Date();
LA_UID = LA_UID.getTime();
/////////////////////////////////////////////////////////////////////////////////////////
// Error
/////////////////////////////////////////////////////////////////////////////////////////
var E_NO_ERROR = 0;
var E_ACTOR_FILE_HAS_EXPIRED = 1;
var E_ACTOR_FILE_NOT_FOUND = 2;
var E_ACTOR_FILE_BAD_FORMAT = 3;
var E_NO_WIN32_PLATFORM_DETECTED = 4;
var E_NO_COMPATIBLE_BROWSER_DETECTED = 5;
var E_WIN95_DETECTED = 6;
var E_CANNOT_FIND_PLUGIN_VERSION_REQUESTED = 7;
var E_HIGH_SECURITY_OR_USER_SAY_NO = 8;
var E_INSTALL_ABORTED = 9;
var	E_INTERNET_INITIALIZATION_ERROR = 16;
var	E_CANT_CREATE_HTTP_CONNECTION_ERROR = 17;
var	E_HTTP_TRANSMISSION_ERROR = 18;
var	E_CANT_CREATE_TEMP_FILE_ERROR = 19;
var	E_DOWNLOAD_FAILED = 20;
var	E_CANT_WRITE_LIVINGACTOR_ERROR = 21;
var	E_REGISTRATION_ERROR = 22;
var E_JAVA_IS_DISABLED = 23;
var E_NETSCAPE_TRIGGER_IS_DISABLED = 24;
var CANNOT_START_PLUGIN_NETSCAPE = 25;
var CANNOT_INSTALL_XPI = 26;
/////////////////////////////////////////////////////////////////////////////////////////
// Toggle Enable / Disable
/////////////////////////////////////////////////////////////////////////////////////////
var TOGGLE_DOWNLOADING_DIALOGBOX = "1";
var TOGGLE_LOADING_DIALOG_DIALOGBOX = "2";
var TOGGLE_ACTOR_DIALOG = "3";
var TOGGLE_ANTIALIASING = "4";
var TOGGLE_SOUND = "5";
var TOGGLE_PAINTER = "7";
var TOGGLE_SYSTEM_ICON = "6";

/////////////////////////////////////////////////////////////////////////////////////////
// Mouse Inactivity configurations
/////////////////////////////////////////////////////////////////////////////////////////
//Start Detection
var MI_ON_OFF = false;//ON OFF
var MI_fonctionToPlay = "PlayRandomFromBaseName";
var MI_functionToPlayParameters = "\"Timer_\"";
var MI_mouseposx, MI_mouseposy;
var MI_oldmouseposx = 0;
var MI_oldmouseposy = 0;
var MI_counter = 0;

var MI_timeOut = 60;//Temps d'attente par defaut 60s
var MI_initTime = MI_timeOut;
var MI_SETTIMEOUT = null;
var MI_COUNT_nbShowActor = 0;
var MI_nbShowActor = 0;
var MI_nbScenar = 0;
var MI_paddingTime = 0;

/////////////////////////////////////////////////////////////////////////////////////////
// Load Actor
////////////////////////////////////////////////////////////////////////////////////////

function ActorLoading()
{
	if (GENERIC_FLASH_PLAYER != null)
	{
		if (ACTOR==null)
		{
			ManageStatus(E_LOADING_ACTOR);
			ACTOR = new Actor(GENERIC_FLASH_PLAYER);
			ACTOR.LoadActor(LA_ACTOR_URL);
		}
		if(Main_LivingActor)
		{
			Main_LivingActor(LA_UserDefined);
		}
		
	}
}
/////////////////////////////////////////////////////////////////////////////////////////
// LivingActor Initialisation (platform & browser dependant function)
/////////////////////////////////////////////////////////////////////////////////////////

function Init_LivingActor(anActorPath,userValue) {
	LA_ACTOR_URL = anActorPath;
	LA_UserDefined = userValue;
	
	if (!WIN32){
		ManageError(E_NO_WIN32_PLATFORM_DETECTED);
		return;
	}
	else if(NAV4)	{
			ManageError(E_NO_COMPATIBLE_BROWSER_DETECTED);
	}
	else if (NAV6 || IE)
	{
		if (GENERIC_FLASH_PLAYER==null)
		{
			GENERIC_FLASH_PLAYER = document.FLASH_PLAYER_ID;
		}
		ActorLoading();

	}
	else{
			ManageError(E_NO_COMPATIBLE_BROWSER_DETECTED);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
// Stop & Quit LivingActor
/////////////////////////////////////////////////////////////////////////////////////////
function TerminateLivingActor()
{
	ACTOR = null;
	GENERIC_FLASH_PLAYER = null;
}
/////////////////////////////////////////////////////////////////////////////////////////
// Mouse Inactivity Management
/////////////////////////////////////////////////////////////////////////////////////////

//MOTOR
function MoveHandler(evnt)
{
	if(IE)
	{
	MI_mouseposx = event.x;
	MI_mouseposy = event.y;
	}
	if(NAV6)
	{
	MI_mouseposx = evnt.pageX;
	MI_mouseposy = evnt.pageY;
	}
}

function checkMousePos()
{
	//alert("checkMousePos() : ENTER "+MI_COUNT_nbShowActor);
	if(MI_mouseposx!=MI_oldmouseposx || MI_mouseposy!=MI_oldmouseposy)
	{
	MI_counter=0;
	MI_oldmouseposx=MI_mouseposx;
	MI_oldmouseposy=MI_mouseposy;
	}
	
	(ACTOR.f_isSpeaking)?MI_counter=0:++MI_counter;
	
	//If i have seen my actor nbShowActor times...
	testCount=(MI_COUNT_nbShowActor==MI_nbShowActor)?StopIntervalPlayRandom():false;//STOP! or continue!
	
	//Update of my timeout!
	MI_timeOut = MI_initTime + (MI_COUNT_nbShowActor*MI_paddingTime);
	//alert(MI_timeOut);
	//If my actor is not speaking...
	if(MI_counter>=MI_timeOut && !ACTOR.f_isSpeaking && testCount===false)
	{
	MI_counter=0;
	++MI_COUNT_nbShowActor;
	//alert("eval");
	eval(MI_fonctionToPlay + "(" + MI_functionToPlayParameters + "," + MI_nbScenar + ");");
	}
	
	//If i haven't shown nbShowActor times my Actor...
	if(testCount===false) MI_SETTIMEOUT=setTimeout("checkMousePos()",1000);//... I check!

}
(IE || NAV6)?document.onmousemove = MoveHandler:false;
////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//Size of current Window & Body
///////////////////////////////////////////////////////////////////////////

function getBodySize()
{
	this.Width=DomBody.scrollWidth;
	this.Height=DomBody.scrollHeight;
}

function getWindowSize()
{
	if(NAV6)
	{
	this.Height = window.innerHeight;
	this.Width = window.innerWidth;
	this.ScrollTop=window.pageYOffset;
	this.ScrollLeft=window.pageXOffset;
	}
	else if(IE)
	{
	this.Height = DomBody.clientHeight;
	this.Width = DomBody.clientWidth;
	this.ScrollTop=DomBody.scrollTop;
	this.ScrollLeft=DomBody.scrollLeft;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
// Event Management
/////////////////////////////////////////////////////////////////////////////////////////
function Drag_Thread()
{
	//theWindowSize();
	sizeOfWindow =new getWindowSize;
	
	//Detect Window Client Size
	var actorX = parseInt(document.getElementById('LA_ActorLayer').style.left) + ACTOR.f_DRAG_X;
	var actorY = parseInt(document.getElementById('LA_ActorLayer').style.top) + ACTOR.f_DRAG_Y;
	var actorWidth = parseInt(document.getElementById('LA_ActorLayer').style.width);
	var actorHeight = parseInt(document.getElementById('LA_ActorLayer').style.height);
	var windowWidth = sizeOfWindow.Width;
	var windowHeight = 2*sizeOfWindow.Height + 2000;
	
	actorX = (actorX+actorWidth/2<0)?-actorWidth/2:actorX;
	actorX = (actorX+actorWidth/2>windowWidth)?windowWidth-actorWidth/2:actorX;
	actorY = (actorY+actorHeight/2<0)?-actorHeight/2:actorY;
	actorY = (actorY+actorHeight/2>windowHeight)?windowHeight-actorHeight/2:actorY;
	//alert(actorX+" "+actorY);
	document.getElementById('LA_ActorLayer').style.left = actorX+"px";	
	document.getElementById('LA_ActorLayer').style.top = actorY+"px";
//	window.status = "dialogH="+ACTOR.f_DialogHeight+" minX="+ACTOR.f_BBoxMinX+" minY="+ACTOR.f_BBoxMinY+" maxX="+ACTOR.f_BBoxMaxX+" maxY="+ACTOR.f_BBoxMaxY;
	
	
	if (ACTOR.f_isSpeaking && LA_DIALOG_VISIBILITY)
	{
		var dialogX = - parseInt(document.getElementById('LA_DialogLayer').style.width) / 2 + parseInt(document.getElementById('LA_ActorLayer').style.left) + parseInt(document.getElementById('LA_ActorLayer').style.width) / 2;
		var dialogY = actorY+ACTOR.f_BBoxMinY - parseInt(document.getElementById('LA_DialogLayer').style.height);
		var dialogWidth = parseInt(document.getElementById('LA_DialogLayer').style.width);
		var dialogHeight = parseInt(document.getElementById('LA_DialogLayer').style.height),
		dialogX = dialogX <0?0:dialogX;
		dialogY = dialogY+dialogHeight < ACTOR.f_DialogHeight?ACTOR.f_DialogHeight-dialogHeight:dialogY;
		dialogX = dialogX+dialogWidth > windowWidth?windowWidth-dialogWidth:dialogX;
		dialogY = dialogY+dialogHeight > windowHeight?windowHeight-dialogHeight:dialogY;
		
		if (dialogY+ACTOR.f_DialogHeight > actorY+ACTOR.f_BBoxMinY)
		{
			if (actorX + ACTOR.f_BBoxMaxX +dialogWidth < windowWidth)
			{
				dialogX = actorX + ACTOR.f_BBoxMaxX;
			}
			else
			{
				dialogX = actorX + ACTOR.f_BBoxMinX - dialogWidth;
			}
		}
		document.getElementById('LA_DialogLayer').style.left = dialogX+"px";	
		document.getElementById('LA_DialogLayer').style.top = dialogY+"px";		
	}	
}


function FLASH_DIALOG_ID_DoFSCommand(eventType,eventValue)
{
	if(eventType=="check_ready"){
//alert("dialog check ready");
		document.FLASH_DIALOG_ID.SetVariable("JS_READY","true");	
	}
	if(eventType=="get_local_connection_id"){
//alert("dialog get_local_connection_id");
		document.FLASH_DIALOG_ID.SetVariable("LOCAL_CONNECTION_ID",LA_UID);
	}	
}


function FLASH_PROGRESSBAR_ID_DoFSCommand(eventType,eventValue)
{
	if(eventType=="check_ready"){
		document.FLASH_PROGRESSBAR_ID.SetVariable("JS_READY","true");
	}
	if(eventType=="get_local_connection_id"){
		document.FLASH_PROGRESSBAR_ID.SetVariable("LOCAL_CONNECTION_ID",LA_UID);
	}	
	
	
}


function FLASH_PLAYER_ID_DoFSCommand(eventType,eventValue)
{
	switch (eventType) 
	{
		case "flash_alert" :
			if (eventValue!="")	
			{
				window.status = eventValue;
				alert("alert : "+eventValue);
			}
			break;
		case "check_ready" :
//alert("player check ready");
			document.FLASH_PLAYER_ID.SetVariable("JS_READY","true");
			break;
		case "get_local_connection_id" :
//alert("player get local connection");
			document.FLASH_PLAYER_ID.SetVariable("LOCAL_CONNECTION_ID",LA_UID);
			break;			
		case "flash_update":
			if(LA_FISRTPLAYERUPDATE){
				if(LA_OnReady){
					LA_OnReady();
				}
				LA_FISRTPLAYERUPDATE=false;
			}
			
			if (GENERIC_FLASH_PLAYER!=null)
			{
				GENERIC_FLASH_PLAYER.SetVariable("FromJavaScript",ACTOR.scenario.join("<&#&>"));
				ACTOR.scenario = new Array;
			}
			break;
		case "ONPROGRESSBARSHOW":
			//document.getElementById('LA_ProgressBarLayer').style.left = 500;	
			{
			sizeOfWindow =new getWindowSize;
			document.getElementById('LA_ProgressBarLayer').style.visibility = 'hidden';
			var PB_centerX = (sizeOfWindow.Width/2) -100; //100 is half progressbar width
			var PB_centerY = (sizeOfWindow.Height/2) -20; //20 is half progressbar height
			document.getElementById('LA_ProgressBarLayer').style.left = PB_centerX+"px";
			document.getElementById('LA_ProgressBarLayer').style.top = PB_centerY+"px";
			}
			break;
		case "ONPROGRESSBARHIDE":
			document.getElementById('LA_ProgressBarLayer').style.left = -199+"px";			
			break;
		
			
		case "ONDRAG":
			ind = eventValue.lastIndexOf(',');
			ACTOR.f_DRAG_X = eventValue.substr(0,ind) * 1;
			ACTOR.f_DRAG_Y = eventValue.substr(ind+1,eventValue.length) * 1;
			clearTimeout(LA_DRAG_THREAD_ID);			
			LA_DRAG_THREAD_ID = setTimeout('Drag_Thread()',10);
			break;
		case "ONSTARTSPEAK":
			ACTOR.f_isSpeaking = true;
			var Left_ind=0;
			var right_ind=eventValue.indexOf(',',Left_ind);
			ACTOR.f_DialogHeight = parseInt(eventValue.substring(Left_ind,right_ind));
			Left_ind = ++right_ind;
			right_ind=eventValue.indexOf(',',Left_ind);			
			ACTOR.f_BBoxMinX = parseInt(eventValue.substring(Left_ind,right_ind));
			Left_ind = ++right_ind;
			right_ind=eventValue.indexOf(',',Left_ind);			
			ACTOR.f_BBoxMinY = parseInt(eventValue.substring(Left_ind,right_ind));
			Left_ind = ++right_ind;
			right_ind=eventValue.indexOf(',',Left_ind);
			ACTOR.f_BBoxMaxX = parseInt(eventValue.substring(Left_ind,right_ind));
			ACTOR.f_BBoxMaxY = parseInt(eventValue.substring(++right_ind,eventValue.length));
			Drag_Thread();
			break;
		case "ONSTOPSPEAK":
			ACTOR.f_isSpeaking = false;
			document.getElementById("LA_DialogLayer").style.left = (parseInt(document.getElementById('LA_DialogLayer').style.left) - 2000)+"px";
			break;			
		
		case "ONPLACEATPIXEL":
				ACTOR.f_DRAG_X = 0;
				ACTOR.f_DRAG_Y = 0;						
				ind = eventValue.lastIndexOf(',');
				var x = eventValue.substr(0,ind) * 1;
				var y = eventValue.substr(ind+1,eventValue.length) * 1;
				document.getElementById('LA_ActorLayer').style.left = x+"px";
				document.getElementById('LA_ActorLayer').style.top = y+"px";
				break;
		case "ONPLACEATSCREEN":
				ACTOR.f_DRAG_X = 0;
				ACTOR.f_DRAG_Y = 0;				
				ind = eventValue.lastIndexOf(',');
				var x = eventValue.substr(0,ind) * 1;
				var y = eventValue.substr(ind+1,eventValue.length) * 1;
				//alert("PlaceAtScreen : X="+x+" Y="+y+" " + document.body.clientWidth+" " + document.body.clientHeight);
				//document.getElementById('LA_ActorLayer').style.left = Math.floor(x*document.body.clientWidth);
				//document.getElementById('LA_ActorLayer').style.top = Math.floor(y*document.body.offsetHeight);				
				
				break;
		case "ONSHOW" :
			{
				Drag_Thread();
				ACTOR.f_DRAG_X = 0;
				ACTOR.f_DRAG_Y = 0;
				if(LA_OnShow)
				{
					LA_OnShow();
				}
			}
			break; 
			
		case "ONQUIT" :
			if(LA_OnQuit)
			{
				LA_OnQuit();
			
			}
			break;
		case "ONHIDE" :
			{
				document.getElementById("LA_ActorLayer").style.left = (parseInt(document.getElementById('LA_ActorLayer').style.left) - 2000)+"px";
				document.getElementById("LA_DialogLayer").style.left = (parseInt(document.getElementById('LA_DialogLayer').style.left) - 2000)+"px";
				ACTOR.f_DRAG_X = 2000;
				if(LA_OnHide)
				{
					LA_OnHide();
				}
			}
			break; 				
		case "ONDRAGSTART" :
			if(LA_OnDragStart)
			{
				LA_OnDragStart();
			}
			break; 
		case "ONDRAGEND" : 
			clearTimeout(LA_DRAG_THREAD_ID);
			ACTOR.f_DRAG_X = 0;
			ACTOR.f_DRAG_Y = 0;
			Drag_Thread()
			if(LA_OnDragEnd)
			{
				LA_OnDragEnd(document.getElementById('LA_ActorLayer').style.left,document.getElementById('LA_ActorLayer').style.top);
			}
			break; 
		case "TEXTINPUTCHANGE":
			ind = eventValue.indexOf(',');
			var TextInputName = eventValue.substr(0,ind);
			var TextInputValue = eventValue.substr(ind+1,eventValue.length);
			for (var i=0;i<ACTOR.LA_TEXTINPUTS.length;i++)
			{
				if (ACTOR.LA_TEXTINPUTS[i].f_Name == TextInputName)
				{
					ACTOR.LA_TEXTINPUTS[i].f_Value = TextInputValue;
					TextInputValue = "";
				}
			}
			if (TextInputValue != "")
			{
				anEdit = new LA_TEXTINPUT(TextInputName,TextInputValue)
				ACTOR.LA_TEXTINPUTS.push(anEdit);
			}
			break;
		
		case "ONBOOKMARK" : 
			if(LA_OnBookmark)
			{
				LA_OnBookmark(eventValue);
			}
			break; 
		case "ONDIALOGID" : 
			if(LA_OnDialogID)
			{
				LA_OnDialogID(eventValue);
			}
			break; 
		case "ONSELECT" : 
			if(LA_OnSelect)
			{
				ind = eventValue.lastIndexOf(',');
				var material = eventValue.substr(0,ind);
				var bone = eventValue.substr(ind+1,eventValue.length);
				LA_OnSelect(material,bone);
			}
			break; 
		default : 
	} 
	
}
/////////////////////////////////////////////////////////////////////////////////////////
// Error Management
/////////////////////////////////////////////////////////////////////////////////////////
function ManageError(errorCode)
{
	if((errorCode!=0)&& (errorCode!=1)&& (errorCode!=4)&& (errorCode!=5)&& (errorCode!=6)&& (errorCode!=22)&& (errorCode!=23)&& (errorCode!=24)&& (errorCode!=25) )
	{
	if(LA_OnSetupExeLaunchable)
		LA_OnSetupExeLaunchable();
	}
	alert(eval("LA_ERROR_" + errorCode));
}
/////////////////////////////////////////////////////////////////////////////////////////
// Error Management
/////////////////////////////////////////////////////////////////////////////////////////
function ManageStatus(statusCode)
{
	LA_CurrentStatus = statusCode;
	//window.status = "Status Code = " + statusCode;
}

/////////////////////////////////////////////////////////////////////////////////////////
// Tools
/////////////////////////////////////////////////////////////////////////////////////////
function PathFor(anActorName)
{
	var path;
	var completePath = document.location.href;
	completePath = unescape(completePath);
	var LastSlashIndex = completePath.lastIndexOf("/");
	path = completePath.substr(0,LastSlashIndex+1);
	path += anActorName;
	return path;
}
function GetCookie(NameOfCookie)
{  
	if (document.cookie.length > 0) 
	{     
		begin = document.cookie.indexOf(NameOfCookie+"="); 
		if (begin != -1)   
		{
			begin += NameOfCookie.length+1; 
			end = document.cookie.indexOf(";", begin);
			if (end == -1)
			{
				end = document.cookie.length;
			}
			return unescape(document.cookie.substring(begin, end)); 
		}
	}
	return null; 
}
function SetCookie(NameOfCookie, value, expiredays,path) 
{
	var ExpireDate = new Date ();
	ExpireDate.setTime(ExpireDate.getTime() + (expiredays * 24 * 3600 * 1000));
	document.cookie = NameOfCookie + "=" + escape(value) + ((expiredays == null) ? "" : ";expires=" + ExpireDate.toGMTString()) + ((path==true) ? ";path=/" : "");
}
function DelCookie(NameOfCookie) 
{
	if (GetCookie(NameOfCookie))
	{
		document.cookie = NameOfCookie + "=" +"; expires=Thu, 01-Jan-70 00:00:01 GMT";
	}
}


function SetLACookie(NameOfCookie, value)
{
	var ExpireDate = new Date ();
	ExpireDate.setTime(ExpireDate.getTime() + (30 * 1000)); //2 hours = 2 * 3600 * 1000
	document.cookie = NameOfCookie + "=" + escape(value) + ";expires=" + ExpireDate.toGMTString() + ";path=/" ;
}


function GetLACookie(NameOfCookie)
{
	return GetCookie(NameOfCookie);		
}

function PlayRandomFromBaseName(basename,nummax)
{
	//alert("nummax "+nummax);
	lastplayedNum = 1;
	if(GetCookie(basename)==null)
	{
		SetCookie(basename,"1", 30, true);
	}
	else
	{
 		lastplayedNum = GetCookie(basename);
	}
	
	(nummax<=1)?eval(basename + "1();"):false;
	
	if(nummax==2)
	{
	choosedNum = (lastplayedNum==2)?1:2;
	eval(basename + choosedNum + "();")
	SetCookie(basename,choosedNum, 30, true);
	}
	
	while(nummax>2)
	{
		choosedNum =Math.floor(Math.random()* nummax);
		if((choosedNum!=lastplayedNum)&&(choosedNum!=0))
		{
			//alert("choosedNum : "+choosedNum);
			SetCookie(basename,choosedNum, 30, true);
			eval(basename + choosedNum + "();");
			return true;
		}
	}
}

function SetIntervalPlayRandom(nbScenar,nbShowActor,initTime,paddingTime)
{
	//alert(nbScenar+","+nbShowActor+","+initTime+","+paddingTime);
	//Initialise for global val
	MI_timeOut = initTime;
	MI_initTime = initTime;
	MI_nbScenar = nbScenar;
	MI_nbShowActor = nbShowActor;
	MI_paddingTime = paddingTime;
	MI_ON_OFF = true;
	MI_COUNT_nbShowActor = 0;
	clearTimeout(MI_SETTIMEOUT);
	(MI_ON_OFF)?checkMousePos():false;
}

function StopIntervalPlayRandom()
{
	//alert("Stop");
	MI_ON_OFF = false;
	clearTimeout(MI_SETTIMEOUT);
	(MI_ON_OFF)?checkMousePos():false;
}
/////////////////////////////////////////////////////////////////////////////////////////
// Javascript - TextInput Class
/////////////////////////////////////////////////////////////////////////////////////////
function LA_TEXTINPUT(_TextInputName,_TextInputValue)
{
	this.f_Name = _TextInputName;
	this.f_Value = _TextInputValue;
}
/////////////////////////////////////////////////////////////////////////////////////////
// Javascript - LivingActor Mapping
/////////////////////////////////////////////////////////////////////////////////////////
function Actor(_FlashPlugin)
{
	this.FlashPlugin = _FlashPlugin;
	this.f_BBoxMinX = 0;
	this.f_BBoxMinY = 0;
	this.f_BBoxMaxX = 0;
	this.f_BBoxMaxY = 0;
	this.f_DialogHeight = 0;
	this.f_isSpeaking = false;
	this.f_DRAG_X = 0;
	this.f_DRAG_Y = 0;
	this.GetLoadStatus = GenericGetLoadStatus;
	this.scenario = new Array();
	this.LA_TEXTINPUTS = new Array;
	//BASIC COMMANDS
	this.Disable = GenericDisable;
	this.Enable = GenericEnable;
	this.GestureAtPixel = GenericGestureAtPixel;
	this.GestureAtScreen = GenericGestureAtScreen;
	this.GetBookmark = GenericGetBookmark;
	this.GetDialogEditText = GenericGetDialogEditText;	
	this.GetDialogEvent = GenericGetDialogEvent;
	this.GetStatus = GenericGetStatus;
	this.Hide = GenericHide;      	 
	this.LoadActor	= GenericLoadActor;
	this.LookAtPixel = GenericLookAtPixel;
	this.LookAtScreen = GenericLookAtScreen;
	this.MenuAddItem = GenericMenuAddItem;
	this.MenuClear = GenericMenuClear;	 
	this.MenuRemoveItem = GenericMenuRemoveItem;	 
	this.MoveToPixel = GenericMoveToPixel;	 
	this.MoveToScreen = GenericMoveToScreen;
	this.PlaceAtFrame = GenericPlaceAtFrame;
	this.PlaceAtVisibleFrame = GenericPlaceAtVisibleFrame;
	this.PlaceAtPixel = GenericPlaceAtPixel;
	this.PlaceAtScreen = GenericPlaceAtScreen;
	this.Play = GenericPlay;	 
	this.PlayScenario = GenericPlayScenario;
	this.PlaySound = GenericPlaySound;     	 
	this.Resume = GenericResume;
	this.SetBookmark = GenericSetBookmark;
	this.SetDialogWidth = GenericSetDialogWidth;
	this.SetDialogSkin = GenericSetDialogSkin;
	this.SetExpression = GenericSetExpression;
	this.SetScale = GenericSetScale;	 
	this.SetTTSID = GenericSetTTSID;	 
	this.Show = GenericShow;
	this.Speak = GenericSpeak;
	this.Stop = GenericStop; 
	this.Suspend = GenericSuspend;
	this.GetStatus = GenericGetStatus;
	
	//ADVANCED COMMANDS
	this.DeleteActorPreferences = GenericDeleteActorPreferences;
	this.GetActorHeight = GenericGetActorHeight;	 
	this.GetActorPositionX = GenericGetActorPositionX;
	this.GetActorPositionY = GenericGetActorPositionY;
	this.GetActorPreference = GenericGetActorPreference;
	this.GetActorWidth = GenericGetActorWidth;	 
	this.GetAnimation = GenericGetAnimation;
	this.GetAnimationCount = GenericGetAnimationCount;
	this.GetError = GenericGetError;	 
	this.GetErrorCount = GenericGetErrorCount;
	this.GetExpression = GenericGetExpression;
	this.GetExpressionCount = GenericGetExpressionCount;
	
	this.GetMaterialCount = GenericGetMaterialCount;
	this.GetMaterialDiffuseBlue = GenericGetMaterialDiffuseBlue;
	this.GetMaterialDiffuseGreen = GenericGetMaterialDiffuseGreen;
	this.GetMaterialDiffuseRed = GenericGetMaterialDiffuseRed;
	this.GetMaterial = GenericGetMaterial;
	
	this.SaveActorPreferences = GenericSaveActorPreferences;
	this.SetActorPreference = GenericSetActorPreference;	 
	this.SetCameraPosition = GenericSetCameraPosition;
	this.SetDialogFont = GenericSetDialogFont;
	this.SetImmediateScale = GenericSetImmediateScale;
	this.SetLightColor = GenericSetLightColor;	 
	this.SetLightPosition = GenericSetLightPosition;
	this.SetTTSInfos = GenericSetTTSInfos;
	this.SetTTSPitch = GenericSetTTSPitch;	 
	this.SetTTSSpeed = GenericSetTTSSpeed;	 
	
}

/////////////////////////////////////////////////////////////////////////////////////////
// Generic Functions
/////////////////////////////////////////////////////////////////////////////////////////

//BASIC COMMANDS

function GenericGetLoadStatus() 
{
	return "";//this.LA_Plugin.Execute("GetLoaded()");
}

function GenericDisable(Toggle)
{
	if(Toggle==TOGGLE_ACTOR_DIALOG){
		LA_DIALOG_VISIBILITY = false;
	}
	return "";//this.LA_Plugin.Execute("Disable(" + Toggle + ")");
}
function GenericEnable(Toggle)
{
	// manage at least actordialogvisible
	if(Toggle==TOGGLE_ACTOR_DIALOG){
		LA_DIALOG_VISIBILITY = true;
	}
	return "";//this.LA_Plugin.Execute("Enable(" + Toggle + ")");
}
function GenericGestureAtPixel(x,y)
{
	return this.scenario.push(("GestureAtPixel(" + x + "," + y + ")"));
}
function GenericGestureAtScreen(x,y)
{
	return this.scenario.push(("GestureAtScreen(" + x + "," + y + ")"));
}
function GenericGetBookmark() 
{
	return "";//this.LA_Plugin.Execute("GetBookmark()");
}
function GenericGetDialogEditText(name)
{
	for (var i=0;i<ACTOR.LA_TEXTINPUTS.length;i++)
	{
		if (ACTOR.LA_TEXTINPUTS[i].f_Name == name)
		{
			return ACTOR.LA_TEXTINPUTS[i].f_Value;	
		}
	}
	return "";	
}
function GenericGetDialogEvent() 
{
	return "";//this.LA_Plugin.Execute("GetDialogEvent()");
}
function GenericGetStatus()
{
	return "";//this.LA_Plugin.Execute("GetStatus()");
}
function GenericHide(seqname)
{
	if(GenericHide.arguments.length == 1)
	{
		return this.scenario.push("Hide2(" +seqname +" )");
	}
	else
	{
		return this.scenario.push("Hide()");
	}
}
function GenericLoadActor(ActorUrl)
{
	return this.scenario.push("LoadActor("+ActorUrl+")");
}
function GenericLookAtPixel(x,y)
{
	return this.scenario.push(("LookAtPixel(" + x +"," + y +")"));
}
function GenericLookAtScreen(x,y)
{
	return this.scenario.push(("LookAtScreen(" + x +"," + y +")"));
}
function GenericMenuAddItem(item,id)
{
	return this.scenario.push(("MenuAddItem(" + (item) + "," + (id) + ")"));
}
function GenericMenuClear()
{
	return this.scenario.push(("MenuClear()"));
}
function GenericMenuRemoveItem(id)
{
	return this.scenario.push(("MenuRemoveItem("+ (id) + ")"));
}
function GenericMoveToPixel(x,y,speed)
{				
	if(GenericMoveToPixel.arguments.length == 3)
	{
		return this.scenario.push(("MoveToPixel(" + x + "," + y + "," + speed +")"));
	}
	else
	{
		return this.scenario.push(("MoveToPixel(" + x + "," + y +")"));
	}
}
function GenericMoveToScreen(x,y,speed)
{
	if(GenericMoveToScreen.arguments.length == 2)
	{
		return this.scenario.push(("MoveToScreen(" + x + "," + y +")"));
	}
	else if(GenericMoveToScreen.arguments.length == 3)
	{
		return this.scenario.push(("MoveToScreen(" + x + "," + y + "," + speed +")"));
	}
}

function GenericPlaceAtFrame(x,y)
{
	sizeOfBody =new getBodySize;
	return this.scenario.push(("PlaceAtPixel(" + Math.floor(x*sizeOfBody.Width) + "," + Math.floor(y*sizeOfBody.Height) + ")"));
	//return this.scenario.push(/*(*/"PlaceAtScreen(" + x + "," + y + ")")/*)*/;
}

function GenericPlaceAtVisibleFrame(x,y)
{
	sizeOfWindow =new getWindowSize;
	//theWindowSize();
	return this.scenario.push(("PlaceAtPixel(" + Math.floor((x*sizeOfWindow.Width)+sizeOfWindow.ScrollLeft) + "," + Math.floor((y*sizeOfWindow.Height)+sizeOfWindow.ScrollTop) + ")"));
}
	


function GenericPlaceAtPixel(x,y)
{
	return this.scenario.push(("PlaceAtPixel(" + x + "," + y + ")"));
}
function GenericPlaceAtScreen(x,y)
{
	
	//return this.scenario.push(("PlaceAtPixel(" + Math.floor(x*document.body.clientWidth) + "," + Math.floor(y*document.body.clientHeight) + ")"));
	return this.scenario.push(/*(*/"PlaceAtScreen(" + x + "," + y + ")")/*)*/;
}
function GenericPlay(animation,seqname)
{	
	if(GenericPlay.arguments.length == 2)
	{
		return this.scenario.push("Play2("  + animation + "," + seqname+ ")");
	}
	else
	{
		return this.scenario.push("Play("  + animation + ")");
	}
}
function GenericPlayScenario(TextToScript)
{
	return this.scenario.push("PlayScenario(" + TextToScript + ")");
}
function GenericPlaySound(awave)
{
	return this.scenario.push("PlaySound(" + awave + ")");
}
function GenericResume()
{
	return this.scenario.push("Resume()");	
}
function GenericSetBookmark(x)
{
	return this.scenario.push(("SetBookmark(" + x + ")"));
}
function GenericSetDialogWidth(lineWidth)
{
	return this.scenario.push("SetDialogWidth(" + lineWidth + ")");
}
function GenericSetDialogSkin(skinName)
{
	return this.scenario.push("SetDialogSkin(" + skinName + ")");
}
function GenericSetExpression(exp,t)
{
	if(GenericSetExpression.arguments.length == 2)
	{
		return this.scenario.push("SetExpression(" + exp + "," +GenericSetExpression.arguments[1]+ ")");
	}
	else
	{
		return this.scenario.push("SetExpression(" + exp + ")");
	}
}
function GenericSetScale(x,t)
{
	if(GenericSetScale.arguments.length == 1)
	{
		return this.scenario.push("SetScale(" + x + ")");	
	}
	else
	if(GenericSetScale.arguments.length == 2)	
	{
		return this.scenario.push("SetScale(" + x + "," + t +")");
	}
}
function GenericSetTTSID(anID)
{
	return "";//this.LA_Plugin.Execute("SetTTSID(" +anID+")");
}
function GenericShow(seqname)
{
	if(GenericShow.arguments.length == 1)
	{
		return this.scenario.push("Show2(" +seqname + ")");
	}
	else
	{
		return this.scenario.push("Show()");
	}
}
function GenericSpeak(sentence)
{
	this.LA_TEXTINPUTS = new Array;
	return this.scenario.push('Speak("' + sentence  + '")');
}
function GenericStop()
{
	return this.scenario.push("Stop()");
}
function GenericSuspend(second)
{
	return this.scenario.push("Suspend(" + second + ")");	
}

//ADVANCED COMMANDS
function GenericDeleteActorPreferences()
{
	return "";//this.scenario.push("DeleteActorPreferences()");
}
function GenericGetActorHeight()
{
	return "";//this.LA_Plugin.Execute("GetActorHeight()");
}
function GenericGetActorPositionX()
{
	return "";//this.LA_Plugin.Execute("GetActorPositionX()");
}
function GenericGetActorPositionY()
{
	return "";//this.LA_Plugin.Execute("GetActorPositionY()");
}
function GenericGetActorPreference(type,name)
{
	return "";//this.LA_Plugin.Execute("GetActorPreference(" + type+" ," +name+" ," + ")");
}
function GenericGetActorWidth()
{
	return "";//this.LA_Plugin.Execute("GetActorWidth()");
}
function GenericGetAnimation(ind)
{
	return this.scenario.push("GetAnimation(" + ind + ")");
}
function GenericGetAnimationCount()
{
	return this.scenario.push("GetAnimationCount()");
}
function GenericGetError(num)
{
	return this.scenario.push("GetError(num)");
}
function GenericGetErrorCount()
{
	return this.scenario.push("GetErrorCount()");
}
function GenericGetExpression(ind)
{
	return this.scenario.push("GetExpression(" + ind + ")");
}
function GenericGetExpressionCount()
{
	return this.scenario.push("GetExpressionCount()");
}
function GenericGetMaterialCount()
{
	return "";//this.LA_Plugin.Execute("GetMaterialCount()");
}
function GenericGetMaterialDiffuseBlue(name)
{
	return "";//this.LA_Plugin.Execute("GetMaterialDiffuseBlue(" + name + ")");
}
function GenericGetMaterialDiffuseGreen(name)
{
	return "";//this.LA_Plugin.Execute("GetMaterialDiffuseGreen(" + name + ")");
}
function GenericGetMaterialDiffuseRed(name)
{
	return "";//this.LA_Plugin.Execute("GetMaterialDiffuseRed(" + name + ")");
}
function GenericGetMaterial(num)
{
	return "";//this.LA_Plugin.Execute("GetMaterial(num)");
}
function GenericSaveActorPreferences()
{
	return "";//this.LA_Plugin.Execute("SaveActorPreferences()");
}
function GenericSetActorPreference(type,name,value) 
{	       
	var tosend = "SetActorPreference(" +  (type) +"," + (name)+"," + "\"" +(value)+"\"" +")" ;
	return "";//this.LA_Plugin.Execute(tosend);
}
function GenericSetCameraPosition(x,y,z)
{
	return "";//this.LA_Plugin.Execute("SetCameraPosition(" + x+"," +y+","+z+")");
}
function GenericSetDialogFont(fontName)
{
	return "";//this.LA_Plugin.Execute("SetDialogFont(" + fontName + ")");
}
function GenericSetImmediateScale(x)
{
	return "";//this.LA_Plugin.Execute(("SetImmediateScale(" + x + ")"));
}
function GenericSetLightColor(x,y,z)
{
	return "";//this.LA_Plugin.Execute("SetLightColor("+x+","+ y+","+ z+")");
}
function GenericSetLightPosition(x,y,z)
{
	return "";//this.LA_Plugin.Execute("SetLightPosition("+x+","+ y+","+ z+")");
}
function GenericSetTTSInfos(gender,language)
{
	return "";//this.LA_Plugin.Execute("SetTTSInfos("+gender+","+language+")");
}
function GenericSetTTSPitch(val)
{
//	return this.LA_Plugin.Execute("SetTTSPitch(" + val + ")");
	return "";//this.LA_Plugin.Execute("SET_TTS_PITCH(" + val + ")");
}
function GenericSetTTSSpeed(val)
{
//	return this.LA_Plugin.Execute("SetTTSSpeed(" + val + ")");
	return "";//this.LA_Plugin.Execute("SET_TTS_SPEED(" + val + ")");
}
function GenericGetStatus()
{
	return "";//this.LA_Plugin.Execute("GetStatus()");
}
/////////////////////////////////////////////////////////////////////////////////////////
