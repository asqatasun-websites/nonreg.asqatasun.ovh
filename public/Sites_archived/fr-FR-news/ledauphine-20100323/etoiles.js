StarPathg = '/images/etoiles/'+notDiapoPath;
StarOutUrlg ='icone_star_empty.gif'; //image par d�faut
StarOverUrlg ='icone_star_full.gif'; //image d'une �toile s�lectionn�e
StarBaseId = 'Star'; //id de base des �toiles
NbStar = 5; //nombre d'�toiles
LgtStarBaseId=StarBaseId.lastIndexOf('');

function NotationSystem() {
	StarOutUrl = contextPath+StarPathg + StarOutUrlg;
	StarOverUrl = contextPath+StarPathg + StarOverUrlg; 
	for (i=1;i<NbStar+1;i++) {
		var img			=document.getElementById('Star'+i);
			
		//img.onclick		=function() {alert('Vous avez donn� la note de '+Name2Nb(this.id)+'.');};
		img.onclick		=function() {noteObjet(Name2Nb(this.id));};
		//R�action lors du clic sur une �toile
		//Evidemment, il faudrait compl�ter cette fonction pour la rendre vraiment utile.
		//Par exemple, envoyer la note dans une base de donn�e via un XMLHttpRequest.
		
		img.alt			='Donner la note de '+i;
		//Texte au survol
		
		img.src			=StarOutUrl;
		img.onmouseover	=function() {StarOver(this.id);};
		img.onmouseout	=function() {StarOut(this.id);};
	}
}

function StarOver(Star) {
	StarOverUrl = contextPath+StarPathg + StarOverUrlg;
	StarNb=Name2Nb(Star);
	for (i=1;i<(StarNb*1)+1;i++) {
		document.getElementById('Star'+i).src=StarOverUrl;
	}
}

function StarOut(Star) {
	StarOutUrl = contextPath+StarPathg + StarOutUrlg;
	StarNb=Name2Nb(Star);
	for (i=1;i<(StarNb*1)+1;i++) {
		document.getElementById('Star'+i).src=StarOutUrl;
	}
}

function Name2Nb(Star) {
	//Le survol d'une �toile ne nous permet pas de conna�tre directement son num�ro
	//Cette fonction extrait donc ce num�ro � partir de l'Id
	StarNb=Star.slice(LgtStarBaseId);
	return(StarNb);
} 