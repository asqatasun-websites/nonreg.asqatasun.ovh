
var jsTabsRanking=new Class({Implements:[Options,Events],options:{'classOn':'On','show':0,'tween':{'duration':800,'transition':'sine:in:out'},'resize':true,'effects':true},handlers:[],contents:[],clicked:null,last:-1,initialize:function(handlers,contents,options){this.setOptions(options);if(handlers.length!=contents.length){var min=Math.min(handlers.length,contents.length);this.handlers=handlers.splice(0,min);this.contents=contents.splice(0,min);}
else{this.handlers=handlers;this.contents=contents;}
this.handlers.each(function(el,i){el.addEvent('click',this.show.bindWithEvent(this,[i]));},this);this.contents.each(function(el){el.set('tween',this.options.tween);},this);this.hideAll();this.removeAccesorio(this.contents);if(this.options.show>=0){this.show(null,this.options.show);}
return this;},hideAll:function(){this.contents.each(function(el){if(this.options.effects){el.setStyles({'visibility':'hidden','opacity':0});}
else{el.setStyles({'display':'none'});}},this);},removeAccesorio:function(els){els.each(function(el){if(el.hasClass('Accesorio')){el.removeClass('Accesorio');}});},show:function(e,index){if(e){new Event(e).stop();}
this.clicked=index;if(this.clicked==this.last)
return;if(this.clicked.toString().match(/^\d+$/)){if(!$defined(this.contents[this.clicked]))
this.clicked=-1;}
else{this.choosed=-1;$each(this.handlers,function(el,i){if(el.getProperty('id')==this.clicked){this.choosed=i;}},this);if(this.choosed!=-1){this.clicked=this.choosed;}}
if(this.clicked!=-1){this.activateTab();this.activateContent();}
this.last=this.clicked;this.fireEvent('changeTab');},activateTab:function(){this.handlers[this.clicked].getParent().addClass(this.options.classOn).blur();if(this.last!=-1){this.handlers[this.last].getParent().removeClass(this.options.classOn);}},activateContent:function(){if(!this.options.effects){this.contents[this.clicked].setStyle('display','block');if(this.last!=-1){this.contents[this.last].setStyle('display','none');}}
else{this.contents[this.clicked].setStyle('visibility','visible').tween('opacity',0,1);var origen=0;if(this.last!=-1){this.contents[this.last].setStyle('visibility','visible').tween('opacity',1,0);origen=this.contents[this.last].getSize().y;}
var dest=this.contents[this.clicked].getSize().y;if(this.options.resize){this.contents[this.clicked].getParent().tween('height',origen,dest);}}}});
var ListadosLoMas=new Class({initialize:function(els){els.each(this.process,this);},process:function(bloque){var h=bloque.getElements('.Pestanas ul li a');var c=bloque.getElements('.BloquePestanas');$each(h,function(el,index){this.addAjaxQuery(el,c[index]);},this);new jsTabsRanking(h,c,{'resize':false,'effects':false});this.sendQuery(null,h[0],c[0]);},addAjaxQuery:function(el,target){el.addEvent('click',this.sendQuery.bindWithEvent(this,[el,target]));},sendQuery:function(e,el,target){if(e){new Event(e).stop();}
if(!$defined(el)||el.get('finish')=='true'){return;}
var url=el.get('href').split('?')[1];target.empty();new Element('img',{'src':frontGlob['rel_path']+'/common/imgs/ico.cargando-ranking.gif'}).injectInside(new Element('p').setStyles({'text-align':'center'}).injectInside(target))
new Request.HTML({'url':frontGlob['rel_path']+'/index.php/services/lo-mas','method':'get','update':target}).send(url);el.set('finish','true');}});window.addEvent('domready',function(e){new ListadosLoMas($$('.NoticiaPestanasRanking'));});
function date(){var months=new Array();months[0]="Enero";months[1]="Febrero";months[2]="Marzo";months[3]="Abril";months[4]="Mayo";months[5]="Junio";months[6]="Julio";months[7]="Agosto";months[8]="Septiembre";months[9]="Octubre";months[10]="Noviembre";months[11]="Diciembre";var days=new Array();days[1]="Lunes";days[2]="Martes";days[3]="Miércoles";days[4]="Jueves";days[5]="Viernes";days[6]="Sábado";days[0]="Domingo";var current_date=new Date();var current_day=current_date.getDate();var current_month=current_date.getMonth();var current_year=current_date.getFullYear();var current_dayWeek=current_date.getDay();document.write(days[current_dayWeek]+", "+current_day+" de "+months[current_month]+" de "+current_year);}
var Herramientas=new Class({actualSize:1,styles:[{'font-size':'49%','line-height':'85%'},{'font-size':'62.5%','line-height':'110%'},{'font-size':'78%','line-height':'90%'},{'font-size':'92%','line-height':'90%'}],initialize:function(){window.addEvent('domready',this.init.bindWithEvent(this));},init:function(e){this.resizePageFromStyles();this.createButtons();this.imprimir();},resizePageFromStyles:function(){var style=this.readCookie('style');if(!style||!style.match(/^\d+$/)){style=null;}
this._setSize(style);},setPageSize:function(){$$('body')[0].setStyles(this.styles[this.actualSize]);},setBiggerStyle:function(){this._setSize(this.actualSize+1);},setSmallerStyle:function(){this._setSize(this.actualSize-1);},_setSize:function(size){if(size!=null){if(size>=this.styles.length){this.actualSize=this.styles.length-1;}
else if(size<0){this.actualSize=0;}
else{this.actualSize=size;}}
this.writeCookie('style',this.actualSize);this.setPageSize();},imprimir:function(){$$('.imprimirPagina').each(function(el){el.addEvent('click',function(e){new Event(e).stop();window.print();});},this);},createButtons:function(){$$('.Imprimir').each(function(el){var t=new Element('span',{'html':'imprimir '}).injectInside(el);new Element('img',{'src':frontGlob['rel_path']+'/common/imgs/ico.impresora.gif','alt':'Imprimir'}).injectInside(new Element('a',{'href':'#','rel':'nofollow','class':'imprimirPagina','title':'imprimir esta página'}).injectInside(el));new Element('span',{'html':'<span>|</span>tamaño'}).injectInside(el);new Element('img',{'src':frontGlob['rel_path']+'/common/imgs/ico.disminuirTexto.gif','alt':'Disminuir Texto'}).injectInside(new Element('a',{'href':'#','rel':'nofollow','class':'link_disminuye'}).addEvent('click',this.setSmallerStyle.bindWithEvent(this)).injectInside(el));new Element('img',{'src':frontGlob['rel_path']+'/common/imgs/ico.aumentarTexto.gif','alt':'Aumentar Texto'}).injectInside(new Element('a',{'href':'#','rel':'nofollow','class':'link_aumenta'}).addEvent('click',this.setBiggerStyle.bindWithEvent(this)).injectInside(el));},this);},writeCookie:function(name,value){Cookie.write(name,value,{'path':frontGlob['rel_path'],'duration':365});},readCookie:function(name){return(Cookie.read(name)||null);}});var herramientas=new Herramientas();
function addTextSearchBox(){if($('buscar-texto')){$('buscar-texto').addEvent('blur',blurSearchBox.bindWithEvent($('buscar-texto')));$('buscar-texto').addEvent('focus',focusSearchBox.bindWithEvent($('buscar-texto')));}
if($('InputBuscador')){$('InputBuscador').addEvent('blur',blurSearchBox404.bindWithEvent($('InputBuscador')));$('InputBuscador').addEvent('focus',focusSearchBox404.bindWithEvent($('InputBuscador')));}
if($('buscar-texto-clasificados')){$('buscar-texto-clasificados').addEvent('blur',blurSearchBox.bindWithEvent($('buscar-texto-clasificados')));$('buscar-texto-clasificados').addEvent('focus',focusSearchBox.bindWithEvent($('buscar-texto-clasificados')));}
if($('buscar-texto-avanzado')){$('buscar-texto-avanzado').addEvent('blur',blurSearchBoxAv.bindWithEvent($('buscar-texto-avanzado')));$('buscar-texto-avanzado').addEvent('focus',focusSearchBoxAv.bindWithEvent($('buscar-texto-avanzado')));}
if($('frmTextoLocalidad')){$('frmTextoLocalidad').addEvent('blur',blurSearchBox2.bindWithEvent($('frmTextoLocalidad')));$('frmTextoLocalidad').addEvent('focus',focusSearchBox2.bindWithEvent($('frmTextoLocalidad')));}
if($('frmPrincipal')){$('frmPrincipal').addEvent('blur',blurSearchBoxRec.bindWithEvent($('frmPrincipal')));$('frmPrincipal').addEvent('focus',focusSearchBoxRec.bindWithEvent($('frmPrincipal')));}
if($('frmEspecial')){$('frmEspecial').addEvent('blur',blurSearchBoxRec2.bindWithEvent($('frmEspecial')));$('frmEspecial').addEvent('focus',focusSearchBoxRec2.bindWithEvent($('frmEspecial')));}}
function blurSearchBox(e){var e=new Event(e);if(this.value=='')
this.value=frontGlob['textobuscar'];}
function focusSearchBox(e){var e=new Event(e);if(this.value==frontGlob['textobuscar'])
this.value='';else
this.select();}
function blurSearchBoxAv(e){var e=new Event(e);if(this.value=='')
this.value=frontGlob['textobuscar'];}
function focusSearchBoxAv(e){var e=new Event(e);if(this.value==frontGlob['textobuscar']||this.value=='todo el site')
this.value='';else
this.select();}
function blurSearchBoxRec(e){var e=new Event(e);if(this.value=='')
this.value='ingrediente principal';}
function focusSearchBoxRec(e){var e=new Event(e);if(this.value=='ingrediente principal')
this.value='';}
function blurSearchBoxRec2(e){var e=new Event(e);if(this.value=='')
this.value='ingrediente secundario';}
function focusSearchBoxRec2(e){var e=new Event(e);if(this.value=='ingrediente secundario')
this.value='';}
function blurSearchBox2(e){var e=new Event(e);if(this.value=='')
this.value='todo el site';}
function focusSearchBox2(e){var e=new Event(e);if(this.value=='todo el site')
this.value='';}
function blurSearchBox404(e){var e=new Event(e);if(this.value=='')
this.value='introduce texto';}
function focusSearchBox404(e){var e=new Event(e);if(this.value=='introduce texto')
this.value='';else
this.select();}
window.addEvent('domready',addTextSearchBox);
var functionsFooter=new Class({initialize:function(){this.pintarEnlaces();if(!window.sidebar){this.mostrar();}
this.addEvents();},pintarEnlaces:function(){var HomePage=new Element('li');var enlaceHomePage=new Element('a',{'html':'Haznos tu página de Inicio','id':'addHomePage','href':'#'}).injectInside(HomePage);var Favoritos=new Element('li');var enlaceFavoritos=new Element('a',{'html':'Añadir a favoritos','id':'anadirPaginaMarcadores','href':'#'}).injectInside(Favoritos);if(!window.sidebar){HomePage.injectInside($$('.OjdInicioTitulares')[0]);}
Favoritos.injectInside($$('.OjdInicioTitulares')[0]);},addEvents:function(){if($('anadirPaginaMarcadores')){$('anadirPaginaMarcadores').addEvent('click',this.addBookmark.bindWithEvent(this));}
if($('addHomePage')){$('addHomePage').addEvent('click',this.addHomePage.bindWithEvent(this));}},mostrar:function(){if($('addHomePage')){$('addHomePage').set('class','');}},addBookmark:function(e){new Event(e).stop();titulo=document.title;url=document.location;if(window.sidebar){window.sidebar.addPanel(titulo,url,"");}else if(window.external){window.external.AddFavorite(url,titulo);}
else if(window.opera&&window.print){window.external.AddFavorite(url,titulo);}},addHomePage:function(e){new Event(e).stop();url=document.location;if(document.all){document.body.style.behavior='url(#default#homepage)';document.body.setHomePage(url);}
else if(window.sidebar){if(window.netscape){try{netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");}
catch(e){alert("Esta acción está deshabilitada en tu navegador, si desea habilitarla, por favor, escriba about:config en la l�nea de direcci�n, y cambiar el valor de signed.applets.codebase_principal_support de false a true");}}
var prefs=Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);prefs.setCharPref('browser.startup.homepage',url);}}});window.addEvent('domready',function(){new functionsFooter();});