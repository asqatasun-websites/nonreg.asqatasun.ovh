//Javascript pour la fonction du moteur DP 

// DP departure cities management
function DepartureCity(code, city) {
	this.code = code;
	this.city = city;
}		

var	dpDepartureCities2 = new Array;
var i = 0;
dpDepartureCities2[i++] = new DepartureCity("PAR", "Paris");
dpDepartureCities2[i++] = new DepartureCity("AJA", "Ajaccio");
dpDepartureCities2[i++] = new DepartureCity("MLH", "B&acirc;le-Mulhouse");
dpDepartureCities2[i++] = new DepartureCity("BIA", "Bastia");
dpDepartureCities2[i++] = new DepartureCity("BIQ", "Biarritz");
dpDepartureCities2[i++] = new DepartureCity("BOD", "Bordeaux");
dpDepartureCities2[i++] = new DepartureCity("BES", "Brest");
dpDepartureCities2[i++] = new DepartureCity("CFE", "Clemont-Ferrand");
dpDepartureCities2[i++] = new DepartureCity("FDF", "Fort-de-France");
dpDepartureCities2[i++] = new DepartureCity("LIL", "Lille");
dpDepartureCities2[i++] = new DepartureCity("LYS", "Lyon");
dpDepartureCities2[i++] = new DepartureCity("MRS", "Marseille");
dpDepartureCities2[i++] = new DepartureCity("MPL", "Montpellier");
dpDepartureCities2[i++] = new DepartureCity("NTE", "Nantes");
dpDepartureCities2[i++] = new DepartureCity("NCE", "Nice");
dpDepartureCities2[i++] = new DepartureCity("PTP", "Pointe-&agrave;-Pitre");
dpDepartureCities2[i++] = new DepartureCity("RNS", "Rennes");
dpDepartureCities2[i++] = new DepartureCity("SXB", "Strasbourg");
dpDepartureCities2[i++] = new DepartureCity("TLN", "Toulon");
dpDepartureCities2[i++] = new DepartureCity("TLS", "Toulouse");
dpDepartureCities2[i++] = new DepartureCity("", "-----------------");
dpDepartureCities2[i++] = new DepartureCity("AMS", "Amsterdam");
dpDepartureCities2[i++] = new DepartureCity("BCN", "Barcelone");
dpDepartureCities2[i++] = new DepartureCity("BRU", "Bruxelles");
dpDepartureCities2[i++] = new DepartureCity("CGN", "Cologne");
dpDepartureCities2[i++] = new DepartureCity("FRA", "Francfort");
dpDepartureCities2[i++] = new DepartureCity("GVA", "Gen&egrave;ve");
dpDepartureCities2[i++] = new DepartureCity("LIS", "Lisbonne");
dpDepartureCities2[i++] = new DepartureCity("LON", "Londres");
dpDepartureCities2[i++] = new DepartureCity("LUX", "Luxembourg");
dpDepartureCities2[i++] = new DepartureCity("MAD", "Madrid");
dpDepartureCities2[i++] = new DepartureCity("MIL", "Milan");
dpDepartureCities2[i++] = new DepartureCity("YMQ", "Montr&eacute;al");
dpDepartureCities2[i++] = new DepartureCity("OPO", "Porto");
dpDepartureCities2[i++] = new DepartureCity("ROM", "Rome");
dpDepartureCities2[i++] = new DepartureCity("VIE", "Vienne ");
dpDepartureCities2[i++] = new DepartureCity("ZRH", "Zurich");



function dpDepartureCitiesOptions(){
	var departureOptions ="";
	for (var i = 0; i < dpDepartureCities2.length; i++)
		departureOptions+='<option value="'+dpDepartureCities2[i].code+'">'+dpDepartureCities2[i].city+'</option>';
		
	return departureOptions;
}

//Departure and return Hour2
	function Hour2(code, hour) {
      this.code = code;
      this.hour = hour;
   }

	var dpHour2 = new Array;
	dpHour2[1] = new Hour2("MORNING", "Matin (9:00-12:00)");
	dpHour2[2] = new Hour2("AFTERNOON", "Après-Midi (12:00-18:00)");
	dpHour2[3] = new Hour2("EVENING", "Soirée (18:00-00:00)");
	dpHour2[4] = new Hour2("00:00", "00:00");
	dpHour2[5] = new Hour2("0100", "01:00");
	dpHour2[6] = new Hour2("0200", "02:00");
	dpHour2[7] = new Hour2("0300", "03:00");
	dpHour2[8] = new Hour2("0400", "04:00");
	dpHour2[9] = new Hour2("0500", "05:00");
	dpHour2[10] =new Hour2("0600", "06:00");
	dpHour2[11] = new Hour2("0700", "07:00");
	dpHour2[12] = new Hour2("0800", "08:00");
	dpHour2[13] = new Hour2("0900", "09:00");
	dpHour2[14] = new Hour2("1000", "10:00");
	dpHour2[15] = new Hour2("1100", "11:00");
	dpHour2[16] = new Hour2("1200", "12:00");
	dpHour2[17] = new Hour2("1300", "13:00");
	dpHour2[18] = new Hour2("1400", "14:00");
	dpHour2[19] = new Hour2("1500", "15:00");
	dpHour2[20] = new Hour2("1600", "16:00");
	dpHour2[21] =  new Hour2("1700", "17:00");
	dpHour2[22] = new Hour2("1800", "18:00");
	dpHour2[23] = new Hour2("1900", "19:00");
	dpHour2[24] = new Hour2("2000", "20:00");
	dpHour2[25] = new Hour2("2100", "21:00");
	dpHour2[26] = new Hour2("2200", "22:00");
	dpHour2[27] = new Hour2("2300", "23:00");

	function HourOptions() {
		var hourOptions=""
		for (var i = 1; i < dpHour.length; i++)
			 hourOptions+='<option value="'+	dpHour[i].code+'">'+	dpHour[i].hour+'</option>';
			
			return  hourOptions; 
	}
	

