function cocherToutOui(typeCase) //script cocher/d�cocher
{
   
   var cases = document.getElementsByTagName('input');   
  
   for(var i=0; i<cases.length; i++)   {
   	  var iden=cases[i].id+"";
   	 
   	  if(cases[i].type == 'radio' && cases[i].value == 'oui' && iden.indexOf(typeCase,0)!=-1){
        cases[i].checked = true;  
	  }
	}   
}

function cocherToutNon(typeCase)
{
   var cases = document.getElementsByTagName('input');  
   for(var i=0; i<cases.length; i++)  {
   		var iden=cases[i].id+"";  
      if(cases[i].type == 'radio' && cases[i].value == 'non' && iden.indexOf(typeCase,0)!=-1) 		
		 cases[i].checked = true;   
	} 
}


function afficheBlock(response,myField)
	{
		
		if(document.getElementById(myField)!=null){
			if(response)
			{
				
				document.getElementById(myField).style.visibility='visible';
				document.getElementById(myField).style.display='block';
			}
			else
			{
				
				document.getElementById(myField).style.visibility='hidden';
				document.getElementById(myField).style.display='none';
			}
		}
	}
String.prototype.trim = function () {
   return this.replace(/(^\s*)|(\s*$)/g, "");
}
function Verif(mail)
{
	mail=mail.toLowerCase();
	
	
	
	var reg = /^[a-zA-Z0-9._=+-]+@[a-zA-Z0-9-.]+\.[a-zA-Z0-9]+$/;
	
    if (!(reg.test(mail.trim())))
	{
		afficheBlock(true,'messageErreurMail');
		return false;
	}else{
		
		if(document.getElementById("emailNewsletter2")!=null){
			mail2=document.getElementById("emailNewsletter2").value.toLowerCase();
			if(mail2.trim()!=mail.trim()){
				afficheBlock(true,'messageErreur');
				return false;
			}
		}
	}
	return true;
} 
function submitformInscriptionNewsletter(form)
{
	afficheBlock(false,'messageErreur');
	afficheBlock(false,'messageErreurMail');

	
	if(Verif(document.getElementById("emailNewsletter1").value)){
  		document.forms["Form_inscriptionNewsletter"].submit();
  	}

}
function OpenCenterPopUp(Image){
	
	
	window.open(Image,'exempledenewsletter','');

} 
	
	
function showLayerCard(linkDisplay,blocDisplay){
	$(blocDisplay).hide();
	$(linkDisplay).click(function(){
		if(this.checked == true ){
			$(blocDisplay).show();
		}
		else{
			$(blocDisplay).hide();	
		}
	});
}

function showLayerCardRadio(linkDisplayYes,linkDisplayNo,blocDisplay){
	$(blocDisplay).hide();
	$(linkDisplayYes).click(function(){
		$(blocDisplay).show();
	});
	$(linkDisplayNo).click(function(){
		$(blocDisplay).hide();
	});
}

function showStore(){
	if($("#driveStoreInputYes")[0].checked == true ){
		$("#driveStoreBlock").show();
	}
	else{
		$("#driveStoreBlock").hide();	
	}
}

$(document).ready(function(){
	showLayerCard("#fidelityCardInput","#fidelityCardBlock"); 
	showLayerCardRadio("#driveStoreInputYes","#driveStoreInputNo","#driveStoreBlock"); 
	showLayerCardRadio("#driveStoreInputYes2","#driveStoreInputNo2","#driveStoreBlock"); 
	showLayerCardRadio("#driveStoreInputYes3","#driveStoreInputNo3","#driveStoreBlock"); 
	showLayerCardRadio("#driveStoreInputYes4","#driveStoreInputNo4","#driveStoreBlock"); 
});
 