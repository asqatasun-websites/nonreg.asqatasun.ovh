function initImages() {    
    //
    document.imageOut = new Object();
    document.imageOver = new Object();
    // PNG Fix for IE<7
    var png_fix = "images/png_fix.gif";
    var pngRegExp = new RegExp("\\.png$", "i")
    var f = "DXImageTransform.Microsoft.AlphaImageLoader";
    //
    var imageArray = $$("img", "input");
    imageArray.each(function(item){
        var image = item.src.substr(item.src.lastIndexOf("/")+1);
        var id = item.id || image.replace("_n.", "").replace("_N.", "");
        var hover = (image.toLowerCase().lastIndexOf("_n.") !=-1);
        //
        if (hover) {
            document.imageOut[id] = new Image();
            document.imageOut[id].src = item.src;
            document.imageOver[id] = new Image();
            document.imageOver[id].src = item.src.substr(0, item.src.lastIndexOf("/")+1)+image.replace("_n.", "_o.").replace("_N.", "_O.");
        }
        // PNG Fix for IE<7
        if (window.ie && !window.ie7 && image.test(pngRegExp)) {
            item.style.width = item.offsetWidth+"px";
            item.style.height = item.offsetHeight+"px";
            item.style.filter = "progid:"+f+"(src='"+item.src+"', sizingMethod='scale');";
            item.src = png_fix;
        }
        //
        if (hover) {
            item.onmouseover = function(){
                setImage(this, document.imageOver[this.id].src);
            }
            item.onmouseout = function(){
                setImage(this, document.imageOut[this.id].src);
            }
            item.id = id;
            //
            function setImage(imageObject, src) {
                if (window.ie && !window.ie7) {
                    if (imageObject.filters[f] && imageObject.filters[f].src.test(pngRegExp)) {
                        imageObject.filters[f].src = src;
                    } else {
                        imageObject.src = src;
                    }
                } else {
                    imageObject.src = src;
                }
            }
        }
    });
}


//
function showPop() {
	if ($('news')){
		$('news').setStyles("display: none; opacity:0; position:absolute; left:0; top:0; width:425px;z-index:999;");
		$('closeIt').setStyles("display: block;");
    }
}
//
function opaceIt(thisObj, opaceTo) {
    if (!thisObj) return;
	//
	var opaceFrom = 0;
    if (opaceTo == 1) {
		opaceFrom = 0;
		thisObj.setStyles({display:'block'}).setOpacity(0);
	} else {
		opaceFrom = 1;
	}
	//
	if (window.ie) ieFixPopup(thisObj, opaceTo);
	//
    var myEffects = new Fx.Style(thisObj, "opacity", {duration:400, transition:Fx.Transitions.quadInOut});
    myEffects.start(opaceFrom, opaceTo);
}
//
function ieFixPopup(obj, opaceTo){
	if (!window.ie) return;
	//
	var iframe = $(obj.id+"Iframe");
	if (iframe == false || iframe == null) {
		iframe = new Element("iframe").setProperties({
			id: obj.id+"Iframe",
			scrolling: "no",
			frameBorder: "no"
		}).setStyles({
			position: "absolute",
			filter: "progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)"
		}).setOpacity(0).injectBefore(obj);
	}
	//
	iframe.setStyles({
		top: 0,
		left: 0,
		width: $(obj).getCoordinates().width,
		height: $(obj).getCoordinates().height
	}).setOpacity(opaceTo);
	//
}

// count file download
function downloadCounter(){
	var downloadable = $$('a[href$=pdf]');
	if(downloadable.length == 0){
		return;
	}
	
	downloadable.each(function(item, index){
		item.addEvent('click', function(e){
			//new Event(e).stop();
			var link = item.getProperty('href');
			wreport_ok = 1;
			var nav=location.href;
			var arrNav=nav.split("&");
			var navArr1=arrNav[0].split("=");
			nav1=navArr1[1];
			var navArr2=arrNav[1].split("=");
			nav2=navArr2[1];
			switch(nav1){
				case 'entreprises':
					switch(nav2){
						case 'toutes_dossier':
							WRP_CONTENT1 = 'Espace entreprises';
							break;										
					}
					break;
				case 'handicapes':
					switch(nav2){						
						case 'dossier':
							WRP_CONTENT1='Espace personnes handicapes';
							break;						
					}
					break;
			}
			WRP_CONTENT2 = link.substr(link.lastIndexOf('/') + 1); // pdf file name
			if(wreport_ok==1){ 
			    var w_counter = new wreport_counter(WRP_SECTION, WRP_SUBSECTION, WRP_ID, WRP_ACC, WRP_CHANNEL);
			    w_counter.delete_profiles();
			    w_counter.add_content(WRP_CONTENT1);
			    w_counter.add_content(WRP_CONTENT2);
			    w_counter.add_content(WRP_CONTENT3);
			    w_counter.add_content(WRP_CONTENT4);
			    w_counter.add_content(WRP_CONTENT5);
			    w_counter.count();											
				// downloading
			}			
		});
	});
}

function initListTypeEmploi() {
	
	var curEmploi =  0;
	var listTypeEmploi = $$('div.listTypeEmploi');
	var listEmploiDetail = $$('div.listEmploiDetail');
		
	if (!listTypeEmploi || listTypeEmploi.length == 0 || !listEmploiDetail || listEmploiDetail.length == 0)
		return;
	
	listEmploiDetail.each(function(el, index) {		
		if (index == 0) {
			el.setStyle('display', "block");			
		} else {
			el.setStyle('display', "none");
		}
	});
	
	var div = listTypeEmploi[0].getChildren();	
	for (var i = 0; i < div.length; i ++) {
		if (i == curEmploi) {
			div[i].addClass('current');
		} 
		
		var aTag = div[i].getFirst().getFirst();
		aTag.index = i;
		
		aTag.addEvent('click', function(evt) {			
			new Event(evt).stop();			
			
			try {
				var player = listEmploiDetail[curEmploi].getLast().getLast().getLast();			
				player.stopSound();
			} catch (e) {
			}
			
			div[curEmploi].removeClass('current');			
			listEmploiDetail[curEmploi].setStyle('display', 'none');
			
			curEmploi = this.index;
			
			div[curEmploi].addClass('current');
			listEmploiDetail[curEmploi].setStyle('display', 'block');			
		});	
	}	
}

//
window.addEvent('load', function(){
    
	downloadCounter();
	
	initListTypeEmploi();
	
	
	/*notice of parameters*/
    var scrollIndex = 0;
    //var maxScroll = $$("#group1 a").length;
	var maxScroll = $$("#group1 a").length+$$("#group2 a").length+$$("#group3 a").length+$$("#group4 a").length+$$("#group5 a").length+$$("#group6 a").length;
    var slideScrollIndex = 0;
    var maxSlideScroll = Math.floor(maxScroll/7)+(maxScroll%7!=0?1:0);
    var scrollGalery = new Fx.Scroll('gallerieImages', {
        wait: false,
        duration: 500,
        offset: {'x': 0, 'y': 0},
        transition: Fx.Transitions.Quad.easeInOut
    });
    var scrollImages = new Fx.Scroll('galleriePhotoSliderCont', {
        wait: false,
        duration: 500,
        offset: {'x': 0, 'y': 0},
        transition: Fx.Transitions.Quad.easeInOut
    });
    //
    $$("#galleriePhotoSliderCont a").each(function(link, i){
        link.scrollIndex = i;
        link.addEvent("click", function(event){
            event = new Event(event).stop();
			//
			if (document.oLink) document.oLink.removeClass('selected');
			$(this).addClass('selected');
			document.oLink = $(this);
			//
            var id = this.href.split("#")[1];
            scrollIndex = this.scrollIndex;
            scrollGalery.toElement(id);
        });
    });
    // 
    if($("scrollPre")){
		$$('div.section a').setStyles('border:6px solid #ccaccc;');
        $("scrollPre").addEvent("click", function(){
            scrollIndex--;
            if (scrollIndex<0) scrollIndex = maxScroll;
            var id = "section"+(scrollIndex+1);
            scrollGalery.toElement(id);
        });
        $("scrollNext").addEvent("click", function(){
            scrollIndex++;
            if (scrollIndex>=maxScroll) scrollIndex = 0;
            var id = "section"+(scrollIndex+1);
            scrollGalery.toElement(id);
        });
        
        if($("leftSlider")){
            $("leftSlider").addEvent("click", function(){
                slideScrollIndex--;
                if (slideScrollIndex<0) slideScrollIndex = maxSlideScroll;
                var id = "group"+(slideScrollIndex+1);
                scrollImages.toElement(id);
            });
        }
        if($("rightSlider")){
            $("rightSlider").addEvent("click", function(){
                slideScrollIndex++;
                if (slideScrollIndex>maxSlideScroll) slideScrollIndex = 0;
                var id = "group"+(slideScrollIndex+1);
                scrollImages.toElement(id);
            });
        }
    }
});




var callAjax = function(){
    var url = "index.php?nav1=entreprises&nav2=ajax"; 
    //* The simple way for an Ajax request, use onRequest/onComplete/onFailure
    //* to do add your own Ajax depended code.
    var theme_id = $("cboThematique").options[$("cboThematique").selectedIndex].value;
    var secteur = $("cboSecteur").options[$("cboSecteur").selectedIndex].value;
    var region_id = $("cboRegion").options[$("cboRegion").selectedIndex].value;
    var taille_id = $("cboTaille").options[$("cboTaille").selectedIndex].value;
    var postBody="cboThematique=" + theme_id; 
    postBody += "&" + "cboSecteur=" + secteur; 
    postBody += "&" + "cboRegion=" + region_id; 
    postBody += "&" + "cboTaille=" + taille_id; 
     var obj = new Ajax(url, {
        method: "post",
        postBody: postBody,
        update: $("divFormRechecher"),
        onComplete:function(){
            //alert(obj.response.text);
            if (searchInit) searchInit();
        }                
    }).request();                               
}
var searchInit = function() {
    if ($("cboThematique")) {
        $("cboThematique").addEvent("change", function(e) {
            new Event(e).stop(); 
            callAjax();
        });
        $("cboSecteur").addEvent("change", function(e) {
            new Event(e).stop(); 
            callAjax();
        });
        $("cboRegion").addEvent("change", function(e) {
            new Event(e).stop(); 
            callAjax();
        });
        $("cboTaille").addEvent("change", function(e) {
            new Event(e).stop(); 
            callAjax();
        });
    }
}
function showpartenaires(num){
	var aInfo = $$("dl.interlocuteurs");	
	var i = 0;
	aInfo.each(function(oTemp){
		oTemp.setStyles({'display':'none'});
		i++;
	});
	if(num && i>0){
		aInfo[num-1].setStyles({'display':'block'});
	}
}
function showinfo(num){
	
	$("infoMap").setStyles({'width':'222px','height':'580px','float':'right','position':'relative'});
	$$("div.address1").setStyles({'float':'left'});
	var aInfo = $$("div.address2");
	var i=0;
	$("flashContent").setStyles({'position':'absolute','top':'270px'});
	aInfo.each(function(oTemp){
					oTemp.setOpacity(0);
					oTemp.setStyles({'position':'absolute'});
					i++;
			});
	if(num && i>0){
		aInfo[num-1].setOpacity(1);
	}
}
function showinfoPrint(){
	$("infoMap").setStyles({'width':'auto','height':'auto','float':'none','position':'static'});
	$$("div.address1").setStyles({'float':'none'});
	aInfo = $$("div.address2");
	aInfo.each(function(oTemp){
					oTemp.setOpacity(1);
					oTemp.setStyles({'position':'static'});
			});
	$("flashContent").setStyles({'display':'none'});
	window.print();
	setTimeout(
		function(){
			$("flashContent").setStyles({'display':'block'});
			aInfo = $$("div.address2");
			aInfo.each(function(oTemp){
					oTemp.setOpacity(0);
			});
		},1000
	);

	/*
	$("infoMap").setStyles({'width':'182px','height':'580px','float':'right','position':'relative'});
	$$("div.address1").setStyles({'float':'left'});
	aInfo = $$("div.address2");
	$("flashContent").setStyles({'position':'absolute','top':'270px','display':'block'});
	aInfo.each(function(oTemp){
					oTemp.setOpacity(0);
					oTemp.setStyles({'position':'absolute'});
			});*/
}

function hidePopup() {
	document.getElementById("popup").style.display = "none";
}
function panel_group(obj) {
	var element = document.getElementById("panel_group");
		if ((status == "true") || (status == "")) {
				element.style.display="none";
				obj.innerHTML = "Deplier vos groupes de formation";
				obj.style.backgroundImage = "url(interface/icon_squaredown.gif)";
				status = "false";
			}else if (status == "false") {
				element.style.display="";
				obj.innerHTML = "Replier vos groupes de formation";
				obj.style.backgroundImage = "url(interface/icon_squareup.gif)";
				status = "true";
			}
	return status;
}
function closePopup (id) {
	var popup =	document.getElementById(id);
	popup.style.display = "none";
	selects = document.getElementsByTagName("select");
	for (i = 0; i != selects.length; i++) {
		selects[i].style.visibility = "visible";
	}
}
function setTextLength(){
	var oTextLength = $('comment');
	var oTelLength = $('tel');
	
	if(!oTextLength || !oTelLength) return;
	oTextLength.addEvent('keypress',function(e) {
			if(oTextLength.value.length>501) {
				oTextLength.value = oTextLength.value.substring(0, 501);
				return false;
			}
		}
	);
}
function restrict(e, validchars) {
var key = getKeyCode(e);
if (key == null) return true;

var keychar = String.fromCharCode(key);
keychar = keychar.toLowerCase();

validchars = validchars.toLowerCase();

if (validchars.indexOf(keychar) != -1) return true;
if ( key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27) return true;
return false;
}
function getKeyCode(e)
{
	if (window.event)
		return window.event.keyCode;
	else if (e)
		return e.which;
	else
		return null;
}

//
//
//
function fInitListQuestion() {
	var listQuestion = $('listQuestion');
	
	if (!listQuestion) {
		return;
	}
	
	var arrHelpInfo = $$('div.helpInfo');
	arrHelpInfo.each(function(el, i) {
		var h4 = el.getPrevious();
		h4.showed = false;
		
		h4.addEvent('click', function(e){
			new Event(e).stop();
			this.showed = !this.showed;
			
			if (this.showed) {
				el.setStyle('height', '');
			} else {
				el.setStyle('height', 0);
			}
		});
		
		el.setStyles({
			overflow: 'hidden',
			height: 0
		});
	});
}

function fInitNavQuestionList() {
	var arrQuestion = $$('div.listTypeResulats');
	var nav = $$('div.validateQuestion')[0];
	
	var finish = $E('a.suivante1', nav);
	var back = $E('a.precedente', nav);
	var next = $E('a.suivante', nav);
	
	if (arrQuestion.length == 0 || !nav || !finish || !back || !next) {
		return;
	}
	
	arrQuestion[0].setStyle('display', 'block');
	back.setStyle('display', 'none');
	
	var message = $('showMessage');
	$E('a', message).addEvent('click', function(e) {
		new Event(e).stop();
		message.setStyle('display', 'none');
	});
	
	var curQuestion = 0;
	
	
	back.addEvent('click', function(e) {
		new Event(e).stop();
		
		curQuestion--;
		
		arrQuestion[curQuestion + 1].setStyle('display', 'none');
		arrQuestion[curQuestion].setStyle('display', 'block');
		
		if (curQuestion == 0) {
			back.setStyle('display', 'none');
		}
		
		finish.setStyle('display', 'none');
		next.setStyle('display', 'block');
	});
		
	next.addEvent('click', function(e) {
		new Event(e).stop();
		
		var inputs = $ES('input', arrQuestion[curQuestion]);
		if (inputs[0].checked || inputs[1].checked) {
		
			curQuestion++;
			
			arrQuestion[curQuestion - 1].setStyle('display', 'none');
			arrQuestion[curQuestion].setStyle('display', 'block');
			
			if (curQuestion == arrQuestion.length - 1) {
				next.setStyle('display', 'none');
				finish.setStyle('display', 'block');
			}
			
			back.setStyle('display', 'block');
		} else {
			message.setStyle('display', 'block');
		}
	});
	
	finish.handleClickEvent = finish.onclick;
	finish.onclick = null;

	finish.addEvent('click', function(e) {
		new Event(e).stop();
		
		var inputs = $ES('input', arrQuestion[curQuestion]);
		if (inputs[0].checked || inputs[1].checked) {
			finish.handleClickEvent();
		} else {
			message.setStyle('display', 'block');
		}
	});
}

window.addEvent("domready", function(){
    initImages();
    showPop();
	setTextLength();
	fInitListQuestion()
	fInitNavQuestionList();
	
    var infoMap = $("infoMap");
	if (infoMap) {
		showinfo();
	}	
	///
	closeHelp("aide01");
	closeHelp("aide02");
	closeHelp("aide03");
	closeHelp("aide04");
	closeHelp("aide05");
	closeHelp("aide06");
	closeHelp("aide07");
	closeHelp("aide08");
	closeHelp("aide09");
	closeHelp("aide10");
	closeHelp("aide11");
	closeHelp("aide12");
	closeHelp("aide13");
	closeHelp("aide14");
	closeHelp("aide15");
	closeHelp("aide16");	
	
	playSoundFile();
	playVideoFile();
	
	
	if($('nosPartenaires')){
		showpartenaires($('regn').selectedIndex);
		//console.log($('regn').selectedIndex);
		//console.log($('regn').getValue());
	}
	
});  

// Play sound file
function playSoundFile(){
	var plays = $$('ul.lstRecrutement');
	if(!plays.length){
		return;
	}
	var lis = plays[0].getElements('li');
	var lastItem = null;
	lis.each(function(_li, _index){	
		_li.addEvent('click', function(e){
			new Event(e).stop();
			if(lastItem != null && lastItem != _li){
				var _span = lastItem.getElement('span');
				_span.removeClass('active');
			}
			
			var _span = _li.getElement('span');
			_span.addClass('active');
			lastItem = _li;	
			var soundFile = _li.getElement('a');
			callExternalInterface2(soundFile.getProperty('href'));
		});
	});	
}

function playVideoFile(){
	var plays = $$('ul.playVideoList');
	if(!plays.length){
		return;
	}
	var lis = plays[0].getElements('li');
	lis.each(function(_li, _index){	
		_li.addEvent('click', function(e){
			new Event(e).stop();
			var soundFile = _li.getElement('a');
			callExternalInterface(soundFile.getProperty('href'));
		});
	});	
}


function callExternalInterface2(soundLink) {
	$("soundPlayer").playSound(soundLink);
}

/*---------checkbox form---------*/
function checkAll(field, chk)
{
	var checks = document.getElementsByName(field + '[]');
	if (chk.checked == true){
		for (i = 0; i < checks.length; i++)
		{
			checks[i].checked = true;
		}
	} else {
		for (i = 0; i < checks.length; i++)
		{
			checks[i].checked = false;
		}
	}

}

function inputNumber(event){
	if (window.event) {
		var event = window.event;
	}
	
	if ((event.keyCode >= 96 && event.keyCode <= 105) || (event.keyCode >= 48 && event.keyCode <= 57) || event.keyCode == 110 || event.keyCode == 190 || event.keyCode == 46 || event.keyCode == 8) {
		return true;
	}
	
	return false;
}
/*function calculator()
{
	
	var input1 = parseFloat($("montant1").value);
	
	$("montant2").value = "";
	$("montant3").value = "";
	$("montant4").value = "";
			
	if (input1 < 20 || input1=="")
	{
			alert("Please input the digit greater than or equal 20");
			$("montant1").focus();
				$("montant2").value = "";
			$("montant3").value = "";
			$("montant4").value = "";
			return;
	}
	
	var input2 = input1 * 0.06;
	$("montant2").value = input1 * 0.06;
	
	if(input1 >= 20 && input1 <= 199 )
	{
		$("montant3").value = formatNumber(input2 * (400 * 8.44), true);
	}
	else if(input1 >= 200 && input1 <= 749 )
	{
		$("montant3").value = formatNumber(input2 * (500 * 8.44), true);
	}
	else if(input1 >= 750 )
	{
		$("montant3").value = formatNumber(input2 * (600 * 8.44), true);
	}
	
	//var input3 = $("montant3").value;
	$("montant4").value = formatNumber(input2 * (1500 * 8.44), true);
}*/

function calculator()
{
	var input1 = parseFloat($("montant1").value);
	var inputMere = parseFloat($("montantMere").value);
	$("montant2").value = "";
	$("montant3").value = "";
	$("montant4").value = "";
		
	if(document.getElementById("montant1").value =="" || document.getElementById("montantMere").value ==""){
		alert('Merci de saisir les deux premiers champs');
	}else{
		
		if (input1 < 20 || input1==""){
		
			alert("Votre entreprise n'est pas assujettie.");
			$("montant1").focus();
			$("montantMere").value = "";
			$("montant2").value = "";
			$("montant3").value = "";
			$("montant4").value = "";
			return;
			
		}else{
		
			var input2 = Math.floor(input1 * 0.06);
			$("montant2").value = Math.floor(input1 * 0.06);
			
			if(inputMere >= 20 && inputMere <= 199 )
			{
				$("montant3").value = formatNumber(input2 * (400 * 8.71), true);
			}
			else if(inputMere >= 200 && inputMere <= 749 )
			{
				$("montant3").value = formatNumber(input2 * (500 * 8.71), true);
			}
			else if(inputMere >= 750 )
			{
				$("montant3").value = formatNumber(input2 * (600 * 8.71), true);
			}
			
			$("montant4").value = formatNumber(input2 * (1500 * 8.71), true);
		}
	}
}

function formatNumber(param, isRounded) {
	if (isRounded) {
		param = Math.round(param);
	}
	
	var p = param;
	var e = 1;
	while (p - Math.floor(p) > 0) {
		p = p*10;
		e = e * 10;
	}
	var fraction = (p % e);
	var aNumber = Math.floor(param);
	
	var str = '';
	var temp;
	while (aNumber > 1000) {
		temp = aNumber % 1000;
		if (temp < 100) {
			str = ' 0' + temp + str;
		} else if (aNumber < 10) {
			str = ' 00' + temp + str;
		} else {
			str = ' ' + temp + str;
		}
		
		aNumber = Math.floor(aNumber / 1000);
	}
	
	str = aNumber + str;	
	if (fraction != 0) {
		str += '.' + fraction;
	}
	
	str += ' ';
	return str;
}
/////////
function showHideTab(index)
{
	var tabs = $$("div.contTabRegles");
	for(i=0;i<tabs.length;i++){
		//tabs.setStyles({position: "absolute", top:"-10000px", left:"-5000px"});
		tabs.setStyles({display: "none"});
	}
	tabs[index].setStyles({display: "block"});
}
/////////


function thisMovie(movieName) {
    if (navigator.appName.indexOf("Microsoft") != -1) {
        return window[movieName]
    }
    else {
        return document[movieName]
    }
}
//
function callExternalInterface(fileLink) {
	thisMovie("videoPlayer").changeVideo(fileLink);
}

function showContent()
{
	if ($$(".laJourneeDesDefis").length == 0) return;
	var btns = $$(".laJourneeDesDefis")[0].getElements("li");
	if (!btns && btns.length == 0) return;
	btns[0].setOpacity(1);
	btns[1].setOpacity(0.5);
	btns[2].setOpacity(0.5);
	
	var activeIndex = 0;
	var sliders = $$(".hiddenCont");
	
	var oPlace = $("detail");
	//
	btns.each(function(el, i){
		el.index = i;
		el.addEvents({
			"click": function(e){
				new Event(e).stop();
				var oThis = $(this);
				oThis.setOpacity(1);
				if (activeIndex == oThis.index) return;
				// swap class
				btns[activeIndex].setOpacity(0.5);
				oThis.setOpacity(1);
				activeIndex = oThis.index;
				// show new image
				oPlace.empty().setOpacity(0);
				var oTempImg = sliders[activeIndex].clone();
				oTempImg.removeClass("hiddenCont").injectInside(oPlace);
				new Fx.Style(oPlace, "opacity").start(0, 1);
			},
			"mouseover": function(e){
				new Event(e).stop();
				var oThis = $(this);
				oThis.setOpacity(1);
			},
			"mouseout": function(e){
				new Event(e).stop();
				var oThis = $(this);
				if (activeIndex == oThis.index) return;
				//
				oThis.setOpacity(0.5);
			}
		});
	});
}
////////////////////////////////////////
function accordionPlanSoutien () {
	var faqContents = $$('div.accordionContent');
	var togglers = $$('div.accordionTop');
	
	if (faqContents.length == 0 || togglers.length == 0) {
		return;
	}
	//
	
	var activeElement;
	for (var i = 0; i < togglers.length; i++) {
		new Fx.Accordion([togglers[i]], [faqContents[i]], { 
			opacity: false, 
			transition: Fx.Transitions.quadOut,
			display: false, // load page, close first dl
			alwaysHide: true, // open, close
			
			onActive: function(toggler, i){
				toggler.getElements('img.accordion')[0].src = 'images/ico_accor_plan.gif';			
				activeElement = toggler;
			},		
			
			onBackground: function(toggler, i){
				toggler.getElements('img.accordion')[0].src = 'images/ico_accor_plan_a.gif';
			}
		});
	}
}
//
//
window.addEvent("load", function(){	
	accordionPlanSoutien();
});