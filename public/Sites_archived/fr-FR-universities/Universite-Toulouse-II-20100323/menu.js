
/*
	ED - 12.07.2006
	
	Script pour gerer les menus deroulants structures comme des listes <ul> imbriquees.
	Les listes <ul> parentes (ou le menu de plus haut niveau) doient avoir une classe dont
	la valeur est definie ici par la variable : classeMenuDeroulant
	
	initMenus() parcour le document a la recherche des listes <ul> possedant cette classe
	et attache des gestionnaires d evenements aux <li> qui contiennent des sous menus
	pour les faire apparaitre et disparaitre au survol
	
	Attention: la fonction addEvent() definie dans defaut.js est requise !

*/
var classeMenuDeroulant = /menu_deroulant/;

/*
	Attache les gestionnaires d evenements aux lis qui ont des sous menus
*/
function initMenus() {
	var menu, menus, lis, liSousMenus;
	if(document.getElementById && document.getElementsByTagName){
			menus = document.getElementsByTagName("ul");
			if(menus.length > 0 ){
				for(var j=0; j<menus.length; j++){
					if(menus[j].className.match(classeMenuDeroulant)){
						menu = menus[j];
						lis = menu.getElementsByTagName("li");
						for(var i=0; i<lis.length; i++){
							liSousMenus = lis[i].getElementsByTagName("ul");
							if(liSousMenus.length > 0){
								// a un sous-menu
								addEvent(lis[i],"mouseover", montrePremierSousMenu);
								addEvent(lis[i],"focus", montrePremierSousMenu);
								addEvent(lis[i],"mouseout", cachePremierSousMenu);	
								addEvent(lis[i],"blur", cachePremierSousMenu);	
							}
						}
					} 
				}
			}
	}
}

/*
	Montre et cache les sous menus
*/
function montrePremierSousMenu(){
	this.getElementsByTagName("ul")[0].style.display = "block";
	/*this.style.backgroundColor = "#EDF4F9";*/
}

function cachePremierSousMenu(){
	this.getElementsByTagName("ul")[0].style.display = "none";
	/*this.style.backgroundColor = "#fff";*/
}

/*
	Appelle initMenus() au chargement de la page
*/


/*Apel automatique de la fonction initMenus d�port� dans le js propre au sous site afin de pouvoir d�sactiver le menu d�roulant
addEvent(window,"load", initMenus);
*/


//addEvent(window,"load", load_hide);
function showNavi(val){

	var sous_menu = document.getElementById("sous_menu");
	var enc_rub = document.getElementById("encadre_rubrique");
	var enc_auto_fiche = document.getElementById("encadre_auto_fiche");
	var fleche = document.getElementById("fleche");
	if(sous_menu.style.display=="" || sous_menu.style.display=="block"){
		var contenu = document.getElementById("contenu_avec_encadres");
		fleche.firstChild.nodeValue = "afficher";
		sous_menu.style.display = "none";
		enc_rub.style.display = "none";
		/*contenu.id="contenu_sans_encadres";*/
	}else if(sous_menu.style.display=="none"){
	var contenu = document.getElementById("contenu_sans_encadres");
		fleche.firstChild.nodeValue = "cacher";
		sous_menu.style.display = "block";
		/*contenu.id="contenu_avec_encadres";*/
	}

	//document.getElementById(navigation).style.display = "none";
}
/*Cette fonction permet de faire une liste d'element � ne pas afficher pour des objets ksup donn�s. !!Ce comportment est largement simplif� en css*/
function load_hide(){
	var masque = new Array();
	masque['formation']=new Array();
	masque['formation'][0]= "sous_menu";
	masque['formation'][1]= "encadre_rubrique";
	masque['ueup']=new Array();
	masque['ueup'][0]= "sous_menu";
	masque['ueup'][1]= "encadre_rubrique";
	//var classcss = document.getElementsByTagName("body")[0].getAttribute("class","false"); // Marche pas avec ie
	var classcss = document.getElementsByTagName("body")[0].className; // className c'est pour ie
	for(var objetSt in masque){
		var pos =classcss.indexOf(objetSt);
		if(pos == 0){
			for(var i=0; i<masque[objetSt].length;i++){
				document.getElementById(masque[objetSt][i]).style.display="none";
			}
		}
	}
}