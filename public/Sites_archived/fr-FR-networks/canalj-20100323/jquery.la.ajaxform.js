/**
 * AJAXFORM
 * $.la.ajaxForm.commonSettings
 * $.la.ajaxForm.settings
 * $.la.ajaxForm.validateStep(id_form,step)
 * $.la.ajaxForm.validateOnlyStep(id_form, step)
 * $.la.ajaxForm.validateToStep(id_form,step)
 * $.la.ajaxForm.validateForm(id_form)
 * $.la.ajaxForm.validate(id_form)
 * $.la.ajaxForm.alertForm(id_form,error)
 * $.la.ajaxForm.alertInput(id_form,id,html)
 * $.la.ajaxForm.getAlertForm(id_form,id,error)
 * $.la.ajaxForm.gotoStep()
 * $.la.ajaxForm.init(array)
 *
 */

(function($){
    $.la = $.la||{};
    $.la.prefixAjaxReturnUrl = '';
    $.extend(true, $.la,
    {
       
        /* AJAX FORM
         * Requirement:
         *  jquery.validate.js
         *  jquery.maskedinput.js

         * */
        ajaxForm: {
            reset_edit_password_fields:function(){
                jQuery('#password').attr('value', '_ezpassword');
                jQuery('#confirmmotdepasse').attr('value', '_ezpassword');
            },
            
            redirect:function(u){
                //var url = u;
                document.location.href = u;
            },
            
            redirectTimeout:function(u,t){
                setTimeout(function(){jQuery.la.ajaxForm.redirect(u)},t);
            },

            commonSettings:{
                modalBox:{/* $.la.ajaxForm.commonSettings.modalBox.register.width */
                    register: {
                        width:505,
                        height:560
                    },
                    succes:   {
                        width:630,
                        height:530
                    }
                },
                prefixAjaxReturnUrl:'',
                errorMessageClass:'error_message', // class du bloc message pour le formulaire
                errorMessageInputClass:'error_message',// class des blocs message pour les inputs sp�cifiques
                errorClass: 'error',
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    if(element.attr('type') == 'checkbox' || element.attr('type') == 'radio'){
                        // pour les radio buttons newsletter et pour le check cgu
                        element.parent().find('div.alert_form').html('');
                        error.appendTo(element.parent().find('div.alert_form'));
                    }
                    else{
                        element.next().html('');
                        error.appendTo(element.next());
                    }
                },
                success: function(span){
                    span.attr('class','success');
                },
                submitHandler:function(){
                    return false;
                },
                submit:{
                    'method':'post',
                    'dataType':'json',
                    'async' : false 
                },
                ignore: ".ignore",
                debug:true,
                highlight: function(element, errorClass) {
                    jQuery(element).addClass('errorElement');
                },
                unhighlight: function(element, errorClass) {
                    jQuery(element).removeClass('errorElement');
                },
                ignoreTitle: true

            },

            settings: {
                /*****************************************/
                /** Setting for register/edit formulars **/
                /*****************************************/                
                'modal_register_form':{
                    id_loading:'', // has to be overloaded in local settings
                    rules: {
                        sexe: "required",
                        first_name: {
                            'validateFirstName' : true,
                            required: true,
                            minlength: 3
                        },
                        last_name: {
                            'validateLastName' : true,
                            required: true,
                            minlength: 2 // MOD by Young Sun: 2 au lieu de 3; exemple: Noel LY
                        },
                        date_de_naissance: {
                            'validateDateNaissance' : true,
                            required: true
                        },
                        email: {
                            required:true,
                            email: true,
                            'emailExistAjax' :true
                        },
                        code_postal: {
                        	'required':true,
                        	'validateZipCode': true,
                            'number':true,
                            'minlength': 5,
                            'maxlength': 5
                        },
                        login:{
                            'required':true,
                            'validatePseudoFirstChar' :true,
                            'validatePseudo' :true,
                            minlength: 5,
                            maxlength:20,
                            'pseudoExist':true
                        },
                        password:{
                            required:true,
                            minlength: 5
                        },
                        confirmmotdepasse:{
                            required:true,
                            minlength: 5,
                            equalTo: "#password"
                        },
                        /*newsletter_site:{
                            required:true
                        },*/
                        cgu:'required'
                    },

                    messages: {
                        sexe:"Homme ou femme ?",
                        first_name: {
                            'validateFirstName' : "Pr&eacute;nom invalide",
                            required: "Pr&eacute;nom obligatoire",
                            minlength: "3 caract&egrave;res mini"
                        },
                        last_name: {
                            'validateLastName' : "Nom invalide",
                            required: "Nom obligatoire",
                            minlength: "2 caract&egrave;res mini" // MOD by Young Sun: 2 au lieu de 3 cf line 111
                        },
                        date_de_naissance: {
                            'validateDateNaissance' : "Date invalide",
                            required: "Date requise"
                        },
                        email: {
                            required: "email obligatoire",
                            email: "email incorrect",
                            'emailExistAjax': "email d&eacute;j&agrave; utilis&eacute;"
                        },
                        adresse: {
                            'validateAddress': 'Adresse invalide',
                            required: ''
                        },
                        code_postal: {
                            'validateZipCode': 'Code postal invalide',
                            'number': "Que des chiffres !",
                            'required': "Code postal obligatoire",
                            'minlength': "Code postal trop court !",
                            'maxlength': "Code postal trop long !"
                        },
                        ville: {
                            'validateCity': 'Ville invalide',
                            required: ''
                        },
                        telephone: {
                            'validatePhone': 'T&eacute;l&eacute;phone invalide'
                        },
                        login:{
                            required: "Pseudo obligatoire",
                            minlength: "5 caract&egrave;res mini",
                            maxlength: "20 caract&egrave;res max"
                        },
                        password:{
                            required:"Mot de passe requis",
                            minlength: "5 caract&egrave;res mini"
                        },
                        confirmmotdepasse:{
                            required:"Champ requis",
                            minlength: "5 caract&egrave;res mini",
                            equalTo: "Mots de passe diff&eacute;rents"
                        },
                        /*newsletter_site:{
                            required:"choix requis"
                        },*/
                        cgu: "Acceptation obligatoire !"
                    },
                    
                    submit:{
                        'url': jQuery.la.prefixAjaxReturnUrl + '/action/register/Modal',
                        'handler': function(json){
                            if(json.res != '0'){
                                if(json.return_url != ''){
                                    if(window.location.search.indexOf('iframe=1') != -1){
                                        parent.location.href=json.return_url;
                                    }
                                    else{
                                        //$.la.modalbox.call(json.return_url, {title: '', height: 505, width: 505});
                                        window.location.href=json.return_url;
                                    }
                                }
                            }else{
                                jQuery.la.ajaxForm.hide_loader_picture(jQuery.la.ajaxForm.settings['modal_register_form'].id_loading);
                                jQuery.la.ajaxForm.alertForm('modal_register_form',1);
                            }
                        }
                    },
                    
                    dimensions: {
                        height: 505,
                        width: 505
                    }
                },
                'full_register_form':{
                    id_loading:'', // has to be overloaded in local settings
                    rules: {
                        sexe: "required",
                        first_name: {
                            'validateFirstName' : true,
                            required: true,
                            minlength: 3
                        },
                        last_name: {
                            'validateLastName' : true,
                            required: true,
                            minlength: 2 // MOD BY Young Sun: 2 au lieu de 3, cf line 111
                        },
                        date_de_naissance: {
                            required: true,
                            'validateDateNaissance' : true
                        },
                        email: {
                            required:true,
                            email: true,
                            'emailExistAjax' :true
                        },
                        adresse: {
                            'validateAddress': true,
                            required: false
                        },
                        code_postal: {
                        	'required':true,
                        	'validateZipCode': true,
                            'number':true,
                            'minlength': 5,
                            'maxlength': 5
                        },
                        ville: {
                            'validateCity': true,
                            required: false
                        },
                        telephone: {
                            'validatePhone': true,
                            required: false
                        },
                        login:{
                            'required': true,
                            'validatePseudoFirstChar': true,
                            'validatePseudo': true,
                            minlength: 5,
                            maxlength: 20,
                            'pseudoExist': true
                        },
                        password:{
                            required:true,
                            minlength: 5
                        },
                        confirmmotdepasse:{
                            required:true,
                            minlength: 5,
                            equalTo: "#password"
                        },
                        /*newsletter_site:{
                            required:true
                        },*/
                        cgu:'required'
                    },

                    messages: {
                        sexe:"Homme ou femme ?",
                        first_name: {
                            'validateFirstName' : "Pr&eacute;nom invalide",
                            required: "Pr&eacute;nom obligatoire",
                            minlength: "3 caract&egrave;res mini"
                        },
                        last_name: {
                            'validateLastName' : "Nom invalide",
                            required: "Nom obligatoire",
                            minlength: "2 caract&egrave;res mini" // MOD by Young Sun, cf line 111
                        },
                        date_de_naissance: {
                            required: "Date requise",
                            'validateDateNaissance' : "Date invalide"
                        },
                        email: {
                            required: "email obligatoire",
                            email: "email incorrect",
                            'emailExistAjax': "email d&eacute;j&agrave; utilis&eacute;"
                        },
                        adresse: {
                            'validateAddress': 'Adresse invalide',
                            required: ''
                        },
                        code_postal: {
                        	'required': "code postal obligatoire",
                        	'validateZipCode': "code postal invalide",
                            'number': "que des chiffres",
                            'minlength': "code postal trop court",
                            'maxlength': "code postal trop long"
                        },
                        ville: {
                            'validateCity': 'Ville invalide',
                            required: ''
                        },
                        telephone: {
                            'validatePhone': 'T&eacute;l&eacute;phone invalide'
                        },
                        login:{
                            required: "Pseudo obligatoire",
                            minlength: "5 caract&egrave;res mini",
                            maxlength: "20 caract&egrave;res max"
                        },
                        password:{
                            required:"Mot de passe requis",
                            minlength: "5 caract&egrave;res mini"
                        },
                        confirmmotdepasse:{
                            required:"Champ requis",
                            minlength: "5 caract&egrave;res mini",
                            equalTo: "Mots de passe diff&eacute;rents"
                        },
                        /*newsletter_site:{
                            required:"choix requis"
                        },*/
                        cgu: "Acceptation obligatoire !"
                    },

                    submit:{
                        'url': jQuery.la.prefixAjaxReturnUrl + '/action/register',
                        'handler': function(json){
                            if(json.res != '0'){
                                if(window.location.search.indexOf('iframe=1') != -1){
                                    if(json.return_url == ''){
                                           parent.location.href='/';
                                    }else{
                                        parent.location.href=json.return_url;
                                    }
                                }
                                else{
                                    if(json.return_url == ''){
                                           document.location.href='/';
                                    }else{
                                       jQuery.la.ajaxForm.redirectTimeout(jQuery.la.prefixAjaxReturnUrl + json.return_url,100);

                                    }
                                }
                            }else{
                                jQuery.la.ajaxForm.hide_loader_picture(jQuery.la.ajaxForm.settings['full_register_form'].id_loading);
                                //TODO : if is_array(json.errors)
                                jQuery.la.ajaxForm.alertForm('full_register_form', json.errors);
                            }
                        }
                    }
                },
                
                'full_edit_form':{
                    id_loading: '',
                    rules: {
                        sexe: "required",
                        first_name: {
                            'validateFirstName' : true,
                            required: true,
                            minlength: 3
                        },
                        last_name: {
                            'validateLastName' : true,
                            required: true,
                            minlength: 2 // MOD by Young Sun, cf line 111
                        },
                        date_de_naissance: {
                            required: true,
                            'validateDateNaissance' : true
                        },
                        email: {
                            required:true,
                            email: true
                        },
                        adresse: {
                            'validateAddress': true,
                            required: false
                        },
                        code_postal: {
                            'validateZipCode': true,
                            'required':true,
                            'number':true,
                            'minlength': 5,
                            'maxlength': 5
                        },
                        ville: {
                            'validateCity': true,
                            required: false
                        },
                        telephone: {
                            'validatePhone': true,
                            required: false
                        },
                        password:{
                            required:true,
                            minlength: 5
                        },
                        confirmmotdepasse:{
                            required:true,
                            minlength: 5,
                            equalTo: "#password"
                        },
                        cgu:'required'
                     
                    },

                    messages: {
                        sexe:"Homme ou femme ?",
                        first_name: {
                            'validateFirstName' : "Pr&eacute;nom invalide",
                            required: "Pr&eacute;nom obligatoire",
                            minlength: "3 caract&egrave;res mini"
                        },
                        last_name: {
                            'validateLastName' : "Nom invalide",
                            required: "Nom obligatoire",
                            minlength: "2 caract&egrave;res mini" // MOD by Young Sun, cf line 111
                        },
                        date_de_naissance: {
                            'validateDateNaissance' : "Date invalide",
                            required: "Date requise"
                        },
                        email: {
                            required: "email obligatoire",
                            email: "email incorrect"
                        },
                        adresse: {
                            'validateAddress': 'Adresse invalide',
                            required: ''
                        },
                        code_postal: {
                            'validateZipCode': 'Code postal invalide',
                            'number': "Que des chiffres !",
                            'required': "Code postal obligatoire",
                            'minlength': "Code postal trop court !",
                            'maxlength': "Code postal trop long !"
                        },
                        ville: {
                            'validateCity': 'Ville invalide',
                            required: false
                        },
                        telephone: {
                            'validatePhone': 'T&eacute;l&eacute;phone invalide'
                        },
                        password:{
                            required:"Mot de passe requis",
                            minlength: "5 caract&egrave;res mini"
                        },
                        confirmmotdepasse:{
                            required:"Champ requis",
                            minlength: "5 caract&egrave;res mini",
                            equalTo: "Mots de passe diff&eacute;rents"
                        },
                        cgu: "Acceptation obligatoire !"
                    },

                    submit:{
                        'url': jQuery.la.prefixAjaxReturnUrl + '/action/edit',
                        'handler': function(json){
                            if(json.res != '0'){
                                if(json.return_url != ''){
                                    if(window.location.search.indexOf('iframe=1') != -1){
                                        parent.location.href=json.return_url;
                                    }
                                    else{
                                        window.location.href=json.return_url;
                                    }
                                }
                                else{
                                    jQuery('.information_update_success').hide('slow');
                                    jQuery('.information_update_error').show('slow');
                                    jQuery.la.ajaxForm.hide_loader_picture(jQuery.la.ajaxForm.settings['full_edit_form'].id_loading);
                                }
                                window.location.href="#info_update";
                            }else{
                                jQuery.la.ajaxForm.hide_loader_picture(jQuery.la.ajaxForm.settings['full_edit_form'].id_loading);
                                jQuery('.information_update_error').hide('slow');
                                jQuery('.information_update_success').show('slow');
                                window.location.href="#info_update";
                                jQuery.la.ajaxForm.alertForm('full_edit_form',1);
                                /*****************
                                if(jQuery('#avatar').attr('value') != "" && jQuery('#avatar').attr('value') != undefined)
                                {
                                    jQuery('#ajax').attr('value', '0');
                                    document.full_edit_form.submit();
                                }
                                else
                                {
                                    document.location.href="#info_update";
                                }
                                ********************/
                            }
                        }
                    }

                },                
                /*********************************/
                /** Setting for login formulars **/
                /*********************************/
                'modal_login_form':{
                    id_loading:'', // has to be overloaded in local settings
                    rules: {
                        UserLogin: "required",
                        UserPassword: "required"
                    },
                    messages: {
                        UserLogin: "Champ requis",
                        UserPassword: "Champ requis"
                    },
                    submit:{
                        'url':jQuery.la.prefixAjaxReturnUrl + '/action/login',
                        'handler': function(json){
                            if(json.res == 1){
                                if(window.location.search.indexOf('iframe=1') != -1){
                                    parent.location.href=json.return_url;
                                }
                                else{
                                    if(json.return_url != ''){
										window.location.href=json.return_url;
									} else {
										window.location.reload(true);
									}
                                }
                            }else{
                                jQuery.la.ajaxForm.hide_loader_picture(jQuery.la.ajaxForm.settings['modal_login_form'].id_loading);
                                jQuery.la.ajaxForm.alertForm('modal_login_form',1);
                            }
                        }
                    }
                },
                'full_login_form':{
                    id_loading: '',
                    rules: {
                        UserLogin: "required",
                        UserPassword: "required"
                    },
                    messages: {
                        UserLogin: "Champ requis",
                        UserPassword: "Champ requis"
                    },
                    submit:{
                        'url':jQuery.la.prefixAjaxReturnUrl + '/action/login',
                        'handler': function(json){
                            if(json.res != 0){
                                if(json.return_url != ""){
                                    if(window.location.search.indexOf('iframe=1') != -1){
                                        parent.location.href=json.return_url;
                                    }
                                    else{
                                        document.location.href=json.return_url;
                                    }
                                }else{
                                    if(window.location.search.indexOf('iframe=1') != -1){
                                        parent.location.href='/';
                                    }
                                    else{
                                        document.location.href='/';
                                    }
                                }
                            }else{
                                jQuery.la.ajaxForm.hide_loader_picture(jQuery.la.ajaxForm.settings['full_login_form'].id_loading);
                                jQuery.la.ajaxForm.alertForm('full_login_form',1);
                            }
                        }
                    }
                },
					
                /***************************************/
                /** Setting for first visit formulars **/
                /***************************************/
                'modal_first_visit_form':{
                    id_loading:'', // has to be overloaded in local settings
                    rules: {
                        UserEmail: {
                            'required':true,
                            'email': true
                        }
                    },
                    messages: {
                    	UserEmail:{
	                		'required': "email obligatoire",
	                		'email': "email incorrect"
                		}
                    },
                    submit:{
                        'url':jQuery.la.prefixAjaxReturnUrl + '/action/register/Modal',
                        'handler': function(json){
                            if(json.res !=0 ){
                                if(json.return_url != ''){
                                    jQuery.la.modalbox.call( jQuery.la.prefixAjaxReturnUrl + json.return_url, {
                                        title: '',
                                        height: jQuery.la.ajaxForm.commonSettings.modalBox.register.height,
                                        width: jQuery.la.ajaxForm.commonSettings.modalBox.register.width
                                        });
                                }
                            }else{
                                jQuery.la.ajaxForm.hide_loader_picture(jQuery.la.ajaxForm.settings['modal_first_visit_form'].id_loading);
                                jQuery('#UserEmail').val(jQuery('#NewUserEmail').attr('value'));
                                jQuery.la.ajaxForm.alertForm('modal_first_visit_form',json.errors);
                            }
                        }
                    }
                },
                'full_first_visit_form':{
                    id_loading:'', // has to be overloaded in local settings
                    rules: {
                        UserEmail: {
                            'required':true,
                            'email': true
                        }
                    },
                    messages: {
                        UserEmail:{
                    		'required': "email obligatoire",
                    		'email': "email incorrect"
                    	}
                    },
                    submit:{
                        'url':jQuery.la.prefixAjaxReturnUrl + '/action/register',
                        'handler':function(json){
                            if(json.res !=0 ){
                                if(json.return_url != ''){
                                    if(window.location.search.indexOf('iframe=1') != -1){
                                        parent.location.href=jQuery.la.prefixAjaxReturnUrl + json.return_url;
                                    }
                                    else{
                                        window.location.href=jQuery.la.prefixAjaxReturnUrl + json.return_url;
                                    }
                                }
                            }else{
                                jQuery.la.ajaxForm.hide_loader_picture(jQuery.la.ajaxForm.settings['full_first_visit_form'].id_loading);
                                jQuery('#UserEmail').val(jQuery('#NewUserEmail').attr('value'));
                                jQuery.la.ajaxForm.alertForm('full_first_visit_form',json.errors);
                            }
                        }
                    }
                },

                /*******************************************/
                /** Setting for forget password formulars **/
                /*******************************************/
                'modal_forget_password_form':{
                    id_loading:'', // has to be overloaded in local settings
                    rules: {
                        UserEmail: {
                            'required':true,
                            'email': true
                        }
                    },
                    messages: {
                        UserEmail: "email incorrect"
                    },
                    submit:{
                        'url':jQuery.la.prefixAjaxReturnUrl + '/action/forgotpassword',
                        'handler':
                        function(json){
                            if(json.res == '1'){
                                // on laisse le spinner pour nr pas renvoyer le mail.
                                jQuery.la.ajaxForm.alertForm('modal_forget_password_form',1);
                            }else{
                                jQuery.la.ajaxForm.hide_loader_picture(jQuery.la.ajaxForm.settings['modal_forget_password_form'].id_loading);
                                jQuery('#NewUserEmail').val(jQuery('#UserEmail').attr('value'));
                                jQuery.la.ajaxForm.alertForm('modal_forget_password_form',2);
                            }

                        }
                    }
                },
                'full_forget_password_form':{
                    id_loading:'', // has to be overloaded in local settings
                    rules: {
                        UserEmail: {
                            'required':true,
                            'email': true
                        }
                    },
                    messages: {
                        UserEmail: {
                    		'required': "email obligatoire",
                    		'email': "email incorrect"
                    	}
                    },
                    submit:{
                        'url':jQuery.la.prefixAjaxReturnUrl + '/action/forgotpassword',
                        'handler':
                        function(json){
                            if(json.res == '1'){
                                jQuery.la.ajaxForm.alertForm('full_forget_password_form',1);
                            }else{
                                jQuery.la.ajaxForm.alertForm('full_forget_password_form',2);
                                jQuery('#NewUserEmail').val(jQuery('#UserEmail').attr('value'));
                                jQuery.la.ajaxForm.hide_loader_picture(jQuery.la.ajaxForm.settings['full_forget_password_form'].id_loading);
                            }
                        }
                    }
                },
                
                
                /**************************************/
                /** Setting for activation formulars **/
                /**************************************/
                'modal_activation_login_form':{
                    id_loading:'', // has to be overloaded in local settings
                    rules: {
                        UserLogin: "required",
                        UserPassword: "required"
                    },
                    messages: {
                        UserLogin: "Champ requis",
                        UserPassword: "Champ requis"
                    },
                    
                    submit:{
                        'url':jQuery.la.prefixAjaxReturnUrl + '/action/login/Modal',
                        'handler': function(json) {
                            if(json.res != 0){
                                if (json.res == 1) {
                                    $.ajax({
                                        data: {
                                            ajax:1,
                                            ActivateButton: "Yes"
                                        },
                                        type: 'post',
                                        url: '/centraluser/activate',
                                        dataType: 'json',
                                        async:settings.submit.async,
                                        success: function (json1) {
                                            if (json1.res == 1) {
                                                if(json1.return_url != ""){
                                                    if(window.location.search.indexOf('iframe=1') != -1){
                                                        parent.location.href=json.return_url;
                                                    }
                                                    else{
                                                        window.location.href=json.return_url;
                                                    }
                                                }else{
                                                    jQuery.la.modalbox.call(jQuery.la.prefixAjaxReturnUrl + '/action/success/Modal', {
                                                        height:jQuery.la.ajaxForm.commonSettings.modalBox.success.height
                                                        });
                                                }
                                            }
                                        }
                                    });
                                }
                            }else{
                                jQuery.la.ajaxForm.hide_loader_picture(jQuery.la.ajaxForm.settings['modal_activation_login_form'].id_loading);
                                jQuery.la.ajaxForm.alertForm('modal_activation_login_form',1);
                            }
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown){
                        jQuery.la.modalbox.call(jQuery.la.prefixAjaxReturnUrl + '/action/success/Modal', {
                            width: jQuery.la.ajaxForm.commonSettings.modalBox.success.width
                            });
                    }
                },
                'full_activation_login_form':{
                    id_loading: '',
                    rules: {
                        UserLogin: "required",
                        UserPassword: "required"
                    },
                    messages: {
                        UserLogin: "Champ requis",
                        UserPassword: "Champ requis"
                    },
                    submit:{
                        'url':jQuery.la.prefixAjaxReturnUrl + '/action/login',
                        'handler': function(json) {
                            if(json.res != 0){
                                if (json.res == 1) {
                                    $.ajax({
                                        data: {
                                            ajax:1,
                                            ActivateButton: "Yes"
                                        },
                                        type: 'post',
                                        url: '/centraluser/activate',
                                        dataType: 'json',
                                        async:settings.submit.async,
                                        success: function (json1) {
                                            if (json1.res == 1) {
                                                if(json1.return_url != ""){
                                                    if(window.location.search.indexOf('iframe=1') != -1){
                                                        parent.location.href=json.return_url;
                                                    }
                                                    else{
                                                        window.location.href=json.return_url;
                                                    }
                                                }else{
                                                    jQuery.la.modalbox.call(jQuery.la.prefixAjaxReturnUrl + '/action/success/Modal', {
                                                        height:jQuery.la.ajaxForm.commonSettings.modalBox.success.height
                                                        });
                                                }
                                            }
                                        }
                                    });
                                }
                            }else{
                                jQuery.la.ajaxForm.hide_loader_picture(jQuery.la.ajaxForm.settings['full_activation_login_form'].id_loading);
                                jQuery.la.ajaxForm.alertForm('full_activation_login_form',1);
                            }
                        }
                    },
                    error:function(XMLHttpRequest, textStatus, errorThrown){
                        window.location.href=jQuery.la.prefixAjaxReturnUrl + '/action/edit?account=activated';
                    }
                }
            },

            validateStep:function(id_form,step){
                if(this.validateOnlyStep(id_form,step)){
                    this.settings[id_form].step[step].success();
                }
            },

            validateOnlyStep:function(id_form, step){
                for (var i=0; i < this.settings[id_form].step.length; i++){
                    for (var j=0; j <= this.settings[id_form].step[i].fields.length; j++){
                        jQuery('[name=' + this.settings[id_form].step[i].fields[j]+ ']').attr('class',(i == step )? '':'ignore');
                    }
                }
                return jQuery('#' + id_form).validate().form();
            },

            validateToStep:function(id_form,step){
                for(var i=0; i<=step ;i++){
                    if(!this.validateOnlyStep(id_form,i)){
                        return false;
                    }
                }
                return true;
            },

            validateForm:function(id_form){
                var settings = this.settings[id_form];
                var ok = jQuery('#' + id_form).validate().form();
                /* Pour afficher le message d'attente des le click sur Valider et personnaliser le div en fonction du 
                formulaire si plusieurs formulaires sur la meme page */
                jQuery.la.ajaxForm.show_loader_picture(settings.id_loading);
                if(ok){
                    //$.la.ajaxForm.show_loader_picture($.la.ajaxForm.div_du_loader_form_valider);
                    $.ajax({
                        data: jQuery('#' + id_form).serialize() ,
                        type: settings.submit.method,
                        url:settings.submit.url,
                        dataType:  settings.submit.dataType,
                        async:settings.submit.async,
                        success:settings.submit.handler,
                        error:settings.error
                    });
                }else{
                    jQuery.la.ajaxForm.hide_loader_picture(settings.id_loading);
                }
                return ok;
            },

            validate:function(id_form){
                $.extend(true, this.settings[id_form], this.commonSettings);
                jQuery("#" + id_form).validate(this.settings[id_form]);
            },

            alertForm:function(id_form,error){
                // message d'erreur du form
                jQuery('#' + id_form + ' .' + this.settings[id_form].errorMessageClass).show().html(this.getAlertForm(id_form,error));
            },
            
            alertInput:function(id_form,id,html){
                if(html==''){
                    jQuery('#' + id_form + ' #' + id).siblings(' .' + this.settings[id_form].errorMessageInputClass).hide().html(html);
                }
                else{
                    jQuery('#' + id_form + ' #' + id).siblings(' .' + this.settings[id_form].errorMessageInputClass).show().html(html);
                }
                
            },
            
            getAlertForm:function(id_form,id,error){
            // voir siteaccess jquery.la.ajaxform.siteaccess.js
            },
            gotoSetp:function(){
            // voir siteaccess jquery.la.ajaxform.siteaccess.js
            // ERROR IN SPELLING
            // Duplicated function
            },
            gotoStep:function(){
            // voir siteaccess jquery.la.ajaxform.siteaccess.js
            },
            useLocalInit:function(){
            },
            init:function(array){
            	
                //alert(this.settings.register_form);
                jQuery('#TB_ajaxContent').css('overflow','hidden');
                
                $.validator.addMethod("validateFirstName", function(value) {
                    var RegExPattern = /[0-9\t\n\r\.\(\)\[\]\"\#&%@\}\{\*\+,;:\/!\?²]+/i;
                    return (!value.match(RegExPattern) || (value == ''));
                }, 'Pr&eacute;nom invalide');
                
                $.validator.addMethod("validateLastName", function(value) {
                    var RegExPattern = /[0-9\t\n\r\.\(\)\[\]\"\#&%@\}\{\*\+,;:\/!\?²]+/i;
                    return (!value.match(RegExPattern) || (value == ''));
                }, 'Nom invalide');
                
                $.validator.addMethod("validateDateNaissance", function(value) {
                    // TODO : date < now         
                    var d = value.split('/');
                    d = d[1]+'/'+d[0] + '/' + d[2] + '';
                    var date = new Date(d);
                    var now = new Date();
                    // 130 ans max :)
                    var maxAge = (date.getFullYear() > now.getFullYear() - 130);
                    var RegExPattern = /^((((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00)))|(((0[1-9]|[12]\d|3[01])(0[13578]|1[02])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|[12]\d|30)(0[13456789]|1[012])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|1\d|2[0-8])02((1[6-9]|[2-9]\d)?\d{2}))|(2902((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00))))$/;
                    return maxAge && now.getTime() > date.getTime() && (value.match(RegExPattern)) && (value!='');
                }, 'Date de naissance invalide');
                
                
                
                $.validator.addMethod("validateAddress", function(value) {
                    var RegExPattern = /[\t\n\r\(\)\[\]\"\#%@\}\{\*\+;:\/!\?²]+/i;
                    return (!value.match(RegExPattern));
                }, 'Adresse invalide');
                
                $.validator.addMethod("validateZipCode", function(value) {
                    var RegExPattern = /^[0-9a-z]+$/;
                    return (value.match(RegExPattern) || (value == ''));
                }, 'Code postal invalide');
                
                $.validator.addMethod("validateCity", function(value) {
                    var RegExPattern = /[0-9\t\n\r\.\(\)\[\]\"\#&%@\}\{\*\+,;:\/!\?²]+/i;
                    return (!value.match(RegExPattern) || (value == ''));
                }, 'Ville invalide');
                
                $.validator.addMethod("validatePhone", function(value) {
                    var RegExPattern = /^[0123456789()+ ]+$/;
                    return (value.match(RegExPattern) || (value == ''));
                }, 'T&eacute;l&eacute;phone invalide');
             
                $.validator.addMethod("validatePseudoFirstChar", function(value) {
                    var RegExPattern = /^[a-zA-Z]/;
                    return (value.match(RegExPattern) || (value == ''));
                }, '1er caract&egrave;re incorrect');

                $.validator.addMethod("validatePseudo", function(value) {
                    var RegExPattern = /^([a-zA-Z0-9])+$/;
                    return (value.match(RegExPattern) );
                }, 'Caractère invalide dans Pseudo ');

                
                
                $.validator.addMethod("pseudoExist", function(value,element) {
                    jQuery.la.ajaxForm.show_loader_picture(jQuery.la.ajaxForm.div_du_loader_input_pseudo);
                    var res;
                    $.ajax({
                        data: 'login=' + escape(value) + '&outputFormat=json' ,
                        type: 'get',
                        url: '/action/checkLoginAjax',
                        dataType: 'json',
                        async: false,
                        success:function(data){
                            jQuery.la.ajaxForm.hide_loader_picture(jQuery.la.ajaxForm.div_du_loader_input_pseudo);
                            if(data.res){
                                jQuery.la.ajaxForm.alertInput(jQuery('#pseudoLDAP').parents('form:eq(0)').attr('id'),'pseudoLDAP','');
                                res = true;
                            }else{
                                //
                                var html = '<strong>Ce pseudo est d&egrave;j&agrave; pris, nous te proposons :</strong><ul>';
                                for(var i = 0; i < 3; i++){
                                    var pseudo = jQuery('#pseudoLDAP').val().toString().substr(0, 17) + Math.floor(Math.random()*999).toString() ;
                                    html += '<li><a href="javascript:void(0);" onclick="jQuery(\'#pseudoLDAP\').val(\'' + pseudo
                                    + '\');">' + pseudo + '</a></li>';
                                }
                                html += '</ul>';
                                // This selector isn't right for gulli because #pseudo already exists in header
                                jQuery.la.ajaxForm.alertInput(jQuery('#pseudoLDAP').parents('form:eq(0)').attr('id'),'pseudoLDAP',html);
                                res = false;
                            }
                            return (res);
                        }
                    });
                    return (res);
                }, 'Pseudo existant');
                
                $.validator.addMethod("pseudoExistAjax", function(value) {
                    return value;
                }, 'Pseudo existant 2');

                
                $.validator.addMethod("emailExistAjax", function(value) {
                    //$.la.ajaxForm.show_loader_picture($.la.ajaxForm.div_du_loader_input_email);
                    var res	;
                	
                    $.ajax({
                        url: '/action/checkEmailAjax',
                        type: 'get',
                        data: {
                            email:jQuery('#email').val(),
                            outputFormat:'json'
                        },
                        async: false,
                        success: function(data) {
                            //$.la.ajaxForm.hide_loader_picture($.la.ajaxForm.div_du_loader_input_email);
                            result = data.split('|');
                            if(result[0]=='0'){
                                res = false;
                            }else{
                                res = true;
                            }
                        }
                    });
           		
                    return res;
                }, 'Email invalide');
                
                jQuery("#date_de_naissance").mask("99/99/9999");
                jQuery('#code_postal').mask("99999");
                
                for(var i =0; i <array.length;i++){
                    this.validate(array[i]);
                }
                
                jQuery.la.ajaxForm.useLocalInit();
            },
			
            show_loader_picture:function(id_spinner,callback){
                if(typeof id_spinner != 'undefined' && id_spinner != ''){
                    jQuery('#' + id_spinner).show();
                }
                if(typeof callback == 'function'){
                    window.setTimeout(callback,100);
                }
            },
            hide_loader_picture:function(id_spinner){
                if(typeof id_spinner != 'undefined' && id_spinner != ''){
                    jQuery('#' + id_spinner).hide();
                }
            },
            div_du_loader_input_email: "loading_pic_email",
            div_du_loader_input_pseudo: "loading_pic_pseudo",
            div_du_loader_form_valider: "loading_pic_valider",
            div_du_loader_form_first: "loading_pic_first",
            div_du_loader_form_password: "loading_pic_password",
            div_du_loader_form_login: "loading_pic_login",
			
            getModalWidth: function(id_form) {
                var settings = this.settings[id_form];
                return settings.dimensions.width;
            },
            
            getModalHeight: function(id_form) {
                var settings = this.settings[id_form];
                return settings.dimensions.height;
            }
        }

    });
})(jQuery);