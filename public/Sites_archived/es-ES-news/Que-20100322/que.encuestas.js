var ENCUESTAS = {};

ENCUESTAS.votarEncuesta = function(frm, dominio) {
	var id_encuesta = frm.id_encuesta;
	var opciones = frm.id_opcion;
	var id_opcion = 0;
	if((frm.id_encuesta != null) && (opciones != null)) {
		var opcionCheck = false;
		for(j=0;j<opciones.length;j++) {
			if(opciones[j].checked) {
				opcionCheck = true;
				id_opcion = opciones[j].value;
				break;
			}
		}
		if(opcionCheck) {
			if(dominio == null) {
				$.getJSON(
					"/backend/ENCUESTAS.votacion.php",
					{id_encuesta: id_encuesta.value, id_opcion: id_opcion},
					function(respuesta) {
						//alert(respuesta.msg);
						if(respuesta.url != '') {
							window.location = respuesta.url;
						}
					}
				);
			} else { // Encuestas en ozu
				frm.method = 'get';
				frm.target = 'blank';
				frm.action = 'http://www.que.es/backend/votarEncuesta.php?dominio=1&id_encuesta=' + id_encuesta.value + '&id_opcion=' + id_opcion;
				frm.submit();
			}
		}
		else {
			alert('No has seleccionado ninguna opci�n de la encuesta');
			return false;
		}
	}
	else {
		alert('Faltan datos en la encuesta');
	}
}

$(document).ready(function() {
	if($('#resultados #respuesta-encuesta').html() != null) {
		$('#votos .votar .btn').hide();
	}
});