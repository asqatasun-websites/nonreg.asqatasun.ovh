/* Sondage
------------------------------------------------*/
$('.b-poll').each(function(){
	var _this = $(this),
		poll = {
			id: parseInt( $( 'input[name=question]', _this ).attr( 'value' ) ) || false,
			loader: $(_this).find( '.poll-loader' )
		};
	// Function
	var _verif = function(){
		if( $('input:checked', _this ).val() === undefined ) return false;
		else return true;
	};
	// Event
	$( 'form', _this ).submit(
		function(){
			var pollform = _this.find( 'form' );
			var datas = pollform.serialize();
			if( poll.id ){
				$.getScript( 'http://ezteam.europe1.fr/poll/vote?' + datas , function(){
					$('.poll-loader',_this).html('<p style="text-align:right"><strong>Merci de votre participation.</strong></p>')
					location.reload(true);
				});
			}
			return false;
		}
	);
});

var pollCallBack = function( datas ){
	if( datas.url != '' ){ window.location.replace(datas.url); }
	var output = '<ul class="poll-results">';
	for( i in datas.answers ){
		output += '\
			<li>\
				<div class="bar">\
					<div style="width:' + datas.answers[i] + '%">\
						<span class="cursor"><img class="ico i-cursor" src="/blank.gif" />' + datas.answers[i] + '%</span>\
					</div>\
				</div>\
				<span>' + i + '</span>\
			</li>\
		';
	};
	output += '</ul><p><strong>' + datas.count + ' votants</strong></p>';
	$( '#poll' + datas.id ).find( '.poll-loader' ).html( output );
};