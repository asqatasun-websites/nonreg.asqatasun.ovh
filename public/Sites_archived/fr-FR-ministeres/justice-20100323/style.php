 /****************************************************************************
* HAUT DE PAGE COMMUN
****************************************************************************/
/* Page */
#container {
	width:870px;/* largeur page*/
	float: left;
}
p {
	margin: 0px;
}
#article p {
	margin: 7px 0px;
	line-height:1.2em;
	color:#666666;
	font-size:1em;
}
img {
	border: 0; 
	padding: 0;
}
table {
	font-size:1em;
}
/********************************
* ARTICLE OUVERT
*********************************/

.orange {
	color: #FF8A00;
}
.rouge {
	color: #FF0000;
}
.bleu {
	color: #336699;
}
.bleuMarine {
	color: #003366;
}
.noir {
	color: #000000;
}
.marron {
	color: #800000;
}
.rougeSoutenu {
	color: #CC0000;
}
.gris{
	color: #808080;
}
.vert {
	color: #006666;
}
ul {
	font-size:1em;
	margin: 1em 0em 1em 3em;
	padding: 0px;
	list-style: disc inside;
}
ol {
	font-size:1em;
	margin: 1em 0em 1em 0em;
}
li {
	margin: 0 0 0.5em;
}
h1 {
	margin: 13px 0 5px 0;
	font-size: 28px;
	font-weight: bold;
}
h2 {
	margin: 13px 0 5px 0;
	font-size: 2.4em;
}
h3 {
	font-family: Arial, Helvetica, sans-serif;
	margin: 13px 0 5px 0;
	font-size: 20px;
	font-weight: bold;
}
h4 {
	font-family: Arial, Helvetica, sans-serif;
	margin: 13px 0 5px 0;
	font-size: 18px;
	font-weight: bold;
}
h5 {
	margin: 13px 0 5px 0;
	font-size: 14px;
}
h6 {
	margin: 0.5em 0em 1em;
	font-size: 1.1em;
	font-weight: normal;
}
.center {
	text-align:center;
}
.droite {
	text-align:right;
}
.justify {
	text-align:justify;
}
.indent {
	text-indent:40px;
}
dfn {
	font-style:normal;
	border-bottom:1px dashed;
	cursor:help;
}
hr {
	width:90%;
	height:1px;
	color:black;
}
cite {
	font-style:italic;
}

/****************************************************************************
* REDEFINITION DES TAGS ET CLASSES GENERIQUES
****************************************************************************/
body {
	font: 62.5% Verdana, Arial, Helvetica, sans-serif;
	color: #000000;
	background: #FFFFFF url(../pix/images_sin/bg_body.jpg) repeat-x 0px 0px;
	margin: 10px;
	height: 1%;
}
a {
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
}
form {
	margin: 0px;
	padding: 0px;
}
input {
	font-size: 1em;
}
h2 {
	font-size: 1.1em;
	margin: 0px;
	padding: 0;
	color: #cfdcdd;
}
td {
	font-size: 1em;
}

.hidden {
	display: none;
}
.i_style {
	background: #FFFFFF;
	border: 1px solid #B9CBCD;
}
.clearer {
	font-size: 0;
	line-height: 0;
	height: 0;
	clear: both;
}

/********************************
* Bandeau de la page
*********************************/
#bandeau {
	margin:0;
	height:148px;
	background: #FFFFFF;
}
#bandeau  a {
	border:none;
	text-decoration:none;
	cursor:pointer;
}
#bandeau a:hover {
	cursor:pointer;
}
#container #bandeau #tribunal {
	position: absolute;
	left: 183px;
	top: 10px;
	overflow: hidden;
	width: 697px;/*571px;*/
}

/********************************
* Profils
*********************************/

#profils {
	background: url(../pix/images_sin/bg_profils.gif) no-repeat 0% 100%;
	height: 137px;
	width: 279px;
	position: absolute;
	left: 600px;/*475px;*/
	top: 21px;
}
#profils ul {
	line-height: 11px;
	margin: 0px;
	padding: 0px;
	list-style: none;
}
#profils li {
	float: left;
	width: 90px;
	margin-right: 3px;
	text-align: center;
	display: inline;
}
#profils img {
	margin: 0px 0px 19px;
	border-style: none;
}
#profils a {
	font-weight: bold;
	color: #cebcc8;
}
#profils .activ a {
	color: #FFFFFF;
}

/********************************
* Moteur et outils
*********************************/

#moteurEToutils .debutBarreOutils {
	float:left;
	width:0px;
	height:45px;
}

/* Moteur de recherche */
#moteurEToutils .rechercheOutils {
	float:left;
	height:45px;
	width:auto;
	padding-top: 8px;
	padding-left: 12px;
	background: url(../pix/images_sin/bg_recherche.jpg) no-repeat 3px 0px;
}
#rechercheOutils .left {
	float:left;
}
#moteurEToutils .boutonRecherche {
	float:left;
	width:14px;
	height:18px;
	background: url(../pix/images_sin/bt_recherche.jpg) no-repeat 0px 2px;
}
#moteurEToutils a.btRecherche {
	border:none;
	text-decoration:none;
	cursor:pointer;
	vertical-align:middle;
}
#moteurEToutils a.btRecherche:hover {
	cursor:pointer;
}
#moteurEToutils .rechercheAvancee {
	float:left;
	margin-top:2px;
	height:18px;
	vertical-align:middle;
}
#moteurEToutils a.rechercheAvancee:hover {
	text-decoration:underline;
}
#moteurEToutils a {
	color: #FFFFFF;
}
#moteurEToutils input{
	width:147px;
	font-size:10px;
	border:1px solid #B9CBCD;
}
#moteurEToutils .btRecherche{
	width:18px;
	height:18px;
	border:0px;
	vertical-align:middle;
}

/* Menu sous bandeau */
#moteurEToutils .menuBarreOutils {
	float:right;
	width:auto;
	text-align:right;
	padding: 30px 10px 0px 0px;
	line-height: 1.2em;
	font-weight: bold;
}
#moteurEToutils .menuBarreOutils span{
	font-family:Arial, Helvetica, sans-serif;
	font-size:1em;
	color:#fff;
	margin-right: 3px;
	margin-left: 3px;
}
#moteurEToutils .menuBarreOutils a{
	font-size:1em;
	color:#fff;
	margin: 0px 15px;
	padding: 0;
}
#moteurEToutils .menuBarreOutils a:hover{
	text-decoration:underline;
}
#esp_dedies ul {
	line-height: 1.2em;
	margin: 0px;
	padding: 0px;
	list-style: none;
}
#esp_dedies li {
	float: left;
	width: 85px;
	padding: 11px 0px 0px;
	margin: 0px 25px 0 50px;
	text-align: center;
}
#esp_dedies a {
	font-weight: bold;
	color: #FFFFFF;
}

/****************************************************************************
* PAGE ACCUEIL
****************************************************************************/

/********************************
* Colonne de gauche
*********************************/
#leftnav {
	width: 174px;
	float: left;
	clear: left;
}
#leftnav h2 {
	background: #610946;
	padding: 0px 5px 0px 10px;
}

/* rubrique */
.boxGauche {
	margin-bottom:10px;
}

/* rubrique Services pratiques */

h1.servicesPratiques span{
	display:block;
}
ul.servicesPratiques {
	list-style: none;
	margin: 0 0 0 20px;
	padding:0;
}
li.servicesPratiques a {
	color:#FFFFFF;
	font-weight: bold;
	font-size: 1em;
}
a.btservicesPratiques  {
	text-decoration:none;
	cursor:pointer;
}
a.btservicesPratiques:hover {
	text-decoration:none;
}
#servicePublic {
	background: #FFFFFF;
	margin: 8px 0px;
	padding: 8px 0px;
}
#servicePublic  .btServicePublic {
	background:url(../pix/images_sin/logo_service-public.gif) no-repeat 0px 0px;
	display: block;
	height: 24px;
	text-indent: -1000px;
}
#servicePublic.altServicePublic {
	display:none;
}
#servicePublica.lienServicePublic {
	border:none;
	text-decoration:none;
	cursor:pointer;
}
#servicePublic a.lienServicePublic:hover {
	cursor:pointer;
}

/* menu */
#navcontainer {
}
#navlist {
	margin:0px;
	padding: 3px;
	padding-left:0px;
	font-size:1em;
}
#subnavlist {
	padding:0px;
	margin:0 0 0 10px;
	list-style: url(none) none outside;
}
#subnavlist ul {
	list-style: none;
}
li#active a#current {
	color: #a4b8b9;
}
#subnavlist li a {
	border: 0;
	font-weight: normal;
}
#navlist #active #subnavlist #subcurrent {
	font-weight: bold;
	color: #a4b8b9;
}

/* FIN CODE ORIGINAL */

#leftnav #navlist {
	background: #FFFFFF;
	margin: 0px 0px 16px;
	padding: 0px;
	list-style: url(none) none outside;
	font-size: 1em;
	font-weight: bold;
}
#leftnav #navlist li {
	border-left: none;
	border-right: none;
	border-top: none;
	border-bottom: 1px solid #beb4a6;
	padding: 10px 0px 8px 7px;
}
#leftnav #navlist a {
	color: #000066;
	border: none;
	padding: 0;
	display: inline;
}
#leftnav #navlist #subnavlist li {
	border-left: none;
	border-right: none;
	border-top: 1px solid #beb4a6;
	border-bottom: none;
	padding: 8px 0px 0px 0px;
	margin: 13px 10px 0 0;
}
#leftnav #thematiques .site {
	background: #610946;
	margin: 3px 0px 0px;
	color: #FFFFFF;
}
#leftnav #thematiques .site p {
	margin: 0px;
	padding: 2px 5px 0px 15px;
}
#leftnav #thematiques .site a {
	font-weight: bold;
	color: #FFFFFF;
}
#leftnav #thematiques #servicePublic {
	margin: 9px 0px 5px;
}

/********************************
* Coeur de page
*********************************/
/* page accueil */
#content_pa {
	width:505px;
	padding: 0 0 20px;
	float: left;
	height: 1%;
	margin: 0px 0px 0px 5px;
}

/* pour article */
#content {
	width:380px;
	padding: 0 0 20px;
	float: left;
	height: 1%;
	margin: 0px 0px 0px 5px;
}

/* Photo et légende */
.photo {
	border:0;
	margin: 0px 10px 10px 0px;
	float: left;
	display: inline;
}
.credits {
	margin:0;
	font-size:0.9em;
	color: #666666;
	line-height: 1em;
}

/* rubrique Accueil */
#accueil {
	margin-bottom:5px; /* pour cacher le bloc accueil */
	padding: 10px;
	color: #666666;
	font-size: 1em;
	background: #FFFFFF;
	float: left;
}
#accueil p{
	font-size:1em;
	margin: 0em 0em 1em;
}
#titreAccueil {
	margin-bottom:15px;
	border-bottom: 1px solid #CCD7D9;
}
#titreAccueil h2 {
	margin: 0;
	display:none;
}
#titreAccueil h2 span {
	display:none;
}
#accueil a:hover {
	text-decoration:underline;
}
#accueil h5, #unes h5 {
	display:inline;
	font-size: 1em;
	margin: 1em 0em 0.5em;
}
#accueil h5 span{
	display: inline;
}
#accueil .photo table {
	font-size: 1em;
}
#accueil .photo table td {
	font-size: 1em;
	text-align: left;
}
#accueil .photo p {
	margin: 0em 0em 0.5em;
	font-size: 1em;
}

/* Une */

#content_pa #unes {
	background: #E2E9EB;
	padding: 5px 15px 15px 10px;
	margin-bottom: 6px;
	color: #666666;
}
#content_pa #unes h2 {
	background: #E2E9EB;
	margin: 0px;
	padding: 0px 0px 3px;
	border-bottom: 1px solid #eae3d8;
}
#content_pa #unes .une_blck {
	width: 100%;
	margin: 15px 0 10px;
	color: #610946;
}
#content_pa #unes .une_blck h3 {
	margin: 0;
	font: normal 1em Verdana, Arial, Helvetica, sans-serif;
	color: #666666;
}
#content_pa #unes .une_blck h3 a {
	font-weight: bold;
	color: #610946;
}
#content_pa #unes a {
	font-weight: bold;
	color: #610946;
}
#content_pa #unes img {
	padding-top: 5px;
	padding-bottom:5px;
}
#content_pa #unes .toute {
	text-align: right;
	border-top: 1px solid #FFFFFF;
	padding-top: 10px;
	margin-bottom: 0px;
}
#content_pa #unes .toute a {
	font-weight: normal;
	color: #FF0000;
	background: url(../pix/images_sin/puce_a_bg_clair.gif) no-repeat 0px 0px;
	padding: 1px 0px 1px 15px;
}

#content_pa #unes .une_blck table {
	width: 100%;
	display: block;
	margin: 7px 0 0 0;
	font-size: 1em;
}
#content_pa #unes .une_blck a.suite {
	font-weight: normal;
	color: #FF0000;
	background: url(../pix/images_sin/puce_a_bg_clair.gif) no-repeat 0px 0px;
	padding: 1px 0px 1px 15px;
}
#content_pa #unes .une_blck table p {
	margin: 0 0 1em;
	color: #666666;
}
#content_pa #unes .une_blck .illustration {
	vertical-align: top;
	color: #999999;
	font-size: 0.9em;
}
#content_pa #unes .une_blck .illustration img {
	margin-right: 6px;
}
#content_pa #actus {
	width: 480px;
	background: #e7eded;
	padding: 20px 15px 10px 10px;
	margin-bottom: 6px;
	color: #666666;
}
#content_pa #actus h2 {
	background: #e7eded;
	margin: 0px 0px 15px;
	padding: 0px 0px 3px;
	border-bottom: 1px solid #FFFFFF;
}
#content_pa #actus p {
	margin: 0em 0em 1em;
}
#content_pa #actus a {
	font-weight: bold;
	color: #610946;
}
#content_pa #actus .toute {
	text-align: right;
	border-top: 1px solid #FFFFFF;
	padding-top: 10px;
	margin-bottom: 0px;
}
#content_pa #actus .toute a {
	font-weight: normal;
	color: #FF0000;
	background: url(../pix/images_sin/puce_a_bg_clair.gif) no-repeat 0px 0px;
	padding: 1px 0px 1px 15px;
}
#content_pa #zoom {
	background: #596d6e;
	padding: 18px 10px 10px;
	margin-bottom: 6px;
	color: #ede5da;
}
#content_pa #zoom img {
	margin: 2em 0em 0em;
}
#content_pa #zoom h2 {
	background: #596d6e;
	margin: 0px 0px 3px;
	padding: 0px 0px 3px;
	border-bottom: 1px solid #7a8a8b;
}
#content_pa #zoom table {
	display: block;
	font-size: 1em;
}
#content_pa #zoom table td {
	vertical-align: top;
}
#content_pa #zoom p {
	margin: 0.5em 0em 0em;
}
#content_pa #zoom a {
	font-weight: bold;
	color: #FFFFFF;
	text-align:left;	
}
#content_pa #zoom .titre {
	font-size:1.3em;	
}

/* FIN DU NOUVEAU CODE */

/* rubrique Actualites */
#actualites {
	margin-bottom:5px; /* pour cacher le bloc actualites */
	padding: 10px;
	background: #e7eded;
	clear: left;
}
#titreActualites {
	margin-bottom:6px;
}
#titreActualites .titre h1 span{
	display:inline;
}
#listeActualites {
	width:auto;
	text-align: right;
	margin-top: 5px;
}
#listeActualites h1 {
	margin:2px 0 0;
	font-weight:normal;
	padding-left:8px;
	font-size: 1em;
}
#listeActualites h1 a {
	color:#22537F;
	background: url(../pix/images_sin/puce_a_bg_clair.gif) no-repeat 0px 0px;
	padding: 1px 0px 2px 15px;
}
#listeActualites  h1 a:hover {
}

/* pour afficher ou cacher les 3 Actualites */
.actualite1,.actualite2, .actualite3 {
	font-size: 1em;
	margin: 0 0 1.5em;
}
.puceActualite {
	display:block;	width:100%;
}
#actualites p {
	padding:0;
	margin:0em 0em 0.5em;
	font-size:1em;
}
#actualites a {
	color:#003366;
}
#actualites h2 {
	color:#003366;
	font-weight:bold;
	margin:2px 0px;
	font-size: 1em;
}
#actualites h2 a:hover {
	text-decoration:underline;
}
#contenuActualites p{
	color:#003366;
	font-family:Verdana, Arial, Helvetica, sans-serif;
	margin:0;
	padding:0;
}
#actualites h2 a {
	color:#003366; /* style pour code couleur */
	text-decoration:none;
}
#actualites .lireActualites a {
	background: url(../pix/images_sin/puce_a_bg_clair.gif) no-repeat 0px 0px;
	font-size: 1em;
	font-weight: normal;
	padding: 1px 0px 2px 17px;
	color: #ff0012;
}
#actualites .lireActualites  h1{
	margin:0px;
	font-size: 1em;
}
#actualites .lireActualites  h1 span{
	display:inline;
}
#actualites .lireActualites  a:hover {
}

/********************************
* Colonne de droite
*********************************/
#rightnav {
	float: right;
	width: 180px;
	margin: 0;
	height: 1%;
}

/* rubrique */
.boxDroite {
	width:180px;
	margin-bottom:1px;
}
#rightnav .lienBoxDroite {
	background: #5A6E6F;
	padding: 10px 10px 10px 5px;
	margin-top: 1px;
}
#rightnav h1 {
	color:#cfdcdd;
	font-weight:bold;
	font-size:1.1em;
	margin:0;
	background: #610946;
	padding: 5px 5px 5px 10px;
	line-height: 1.1em;
}


/* focus video */
#rightnav #focus_video {
	background: #596d6e;
	padding: 5px;
	margin-bottom: 1px;
	color: #ede5da;
}
#rightnav #focus_video p {
	margin: 0.5em 0em 0em;
}
#rightnav #focus_video a {
	font-weight: bold;
	color: #FFFFFF;
}


/* rubrique Justice en France  */

ul.justiceFrance {
	list-style: none;
	margin: 0px 0px 0px 20px;
	padding:0px;
}
li.justiceFrance a {
	color:#FFFFFF;
	font-weight: bold;
	font-size: 1em;
}

/* rubrique Mise en ligne */

#rightnav h2.miseEnLigne{
	display: none;
}
ul.miseEnLigne {
	list-style: none;
	margin: 0px 0px 0px 20px;
	padding:0px;
}
li.miseEnLigne a {
	color:#FFFFFF;
	font-weight: bold;
	font-size: 1em;
}
li.miseEnLigne {
	font-size: 1em;
	list-style: url(../pix/images_sin/puce_li_bg_grisfonce.gif) outside;
	margin: 0em 0em 0.5em;
	color: #e7eded;
}

/* rubrique Autres liens*/
h1.autresLiens span{
	display:block;
}
li.autresLiensExternes a {
	color:#074B87;
}
li.autresLiensExternes {
	font-size:10px;
	margin: 3px;
	padding: 0px;
	display:list-item;
	list-style-image: url(../pix/images/picto_bleu/picto_lien_externe.gif); /* style pour code couleur */
}
ul.autresLiensExternes {
	list-style: none;
	margin: 0px 0px 0px 20px;
	padding:0px;
}
#rightnav h2.justiceFrance{
	margin:0px;
	height:0px;
	width:150px; /* style pour code couleur */
	display: none;
}
li.justiceFrance {
	font-size: 1em;
	list-style: url(../pix/images_sin/puce_li_bg_grisfonce.gif) outside;
	margin: 0em 0em 0.5em;
	color: #e7eded;
}
h1.autresLiens{
	margin-bottom: 1px;
}
#rightnav h2.autresLiens{
	margin:0px;
	height:0px;
	width:150px; /* style pour code couleur */
	display: none;
}
ul.autresLiensInternes {
	list-style: none;
	margin: 0px 0px 0px 20px;
	padding:0; /* style pour code couleur */
}
li.autresLiensInternes a {
	color:#FFFFFF;
	font-weight: bold;
	font-size: 1em;
}
li.autresLiensInternes {
	font-size: 1em;
	list-style: url(../pix/images_sin/puce_li_bg_grisfonce.gif) outside;
	margin: 0em 0em 0.5em;
	color: #e7eded;
}
/* FIN CODE ORIGINAL */

#rightnav #point_presse, #rightnav #agenda_gds, #rightnav #actu_gds, #rightnav #actu_gds_all, #rightnav #mel {
	background: #5a6e6f;
	margin: 1px 0px;
	padding: 20px 5px 10px;
}
#rightnav h2 {
	background: #610946;
	padding: 0px 5px 0px 10px;
}

#rightnav #actu_gds_all {
	padding: 5px;
}
#rightnav #point_presse ul, #rightnav #agenda_gds ul, #rightnav #actu_gds ul, #rightnav #actu_gds_all ul, #rightnav #mel ul {
	margin: 0px;
	padding: 0px;
	list-style: none;
}
#rightnav #point_presse li, #rightnav #agenda_gds li, #rightnav #actu_gds li, #rightnav #actu_gds_all li, #rightnav #mel li {
	color: #ede5da;
	background: url(../pix/images_sin/puce_li_bg_grisfonce.gif) no-repeat 0px 2px;
	padding-left: 15px;
	margin: 0;
	list-style: none outside;
}
#rightnav #point_presse li, #rightnav #agenda_gds li, #rightnav #actu_gds li, #rightnav #mel li {
	margin-bottom: 10px;
}
#rightnav #point_presse a, #rightnav #agenda_gds a, #rightnav #actu_gds a, #rightnav #mel a {
	font-weight: bold;
	color: #FFFFFF;
}
#rightnav #actu_gds_all a {
	font-weight: bold;
	color: #610946;
}
#rightnav #eregie {
	margin: 1px 0px;
}

/****************************************************************************
* PAGE ARTICLE
****************************************************************************/
#contentExtensible {
	padding: 10px;
	background: #FFFFFF;
	float: left; /*right */
	margin-left: 5px;
	width: 670px; /* width pour marge droite blanche */
}
#article  {
	color: #666666;
	padding: 0px 0px 10px;
	width: 544px;
}
#article h5 {
	clear: both;
}
#article a {
	font-weight: bold;
	color: #610946;
	/*background: url(../pix/images_sin/puce_a_bg_clair.gif) no-repeat 0px 0px;*/
	/*padding: 1px 0px 2px 17px;*/
}

/* bloc avec date */
#dateETautre {
}
/* date */
#dateETautre h1 {
	margin: 0px 0px 5px;
	font-weight:bold;
	color:#5A6E6F; /* style pour code couleur */
	font-size:0.9em;
}

/* titre */
#article h2 {
	color:#610946;
	margin:0 0 0.5em;
	font-size: 1.5em;
}
#article h3 {
	margin:0px 0px 10px;
	color: #660033;
	font-size: 1.1em;
	font-weight: normal;
}
/* titre */
#article .sousTitre {
	margin:0 0 1em;
	font-size: 1.3em;
	color: #666666;
	font-weight: bold;
}
#article .chapo { /* Ne sert à rien à cause de #article h3 qui est prioritaire */
	/*color:#003366;
	margin:0px 0px 10px;
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size: 0.7em;
	font-weight: bold;*/
}

/* image */
#article img {
	margin:5px 0px 5px 0px;
	border:0;
}

/* bloc avec bouton imprimer et pagination */
#imprimerETpagination {
	clear: both;
}

/* bouton imprimer */
#imprimerETpagination a.print {
	font-size:0.9em;
	color:#003366;
	background: url(../pix/images/picto_bleu/picto_imprimer.gif) no-repeat left center;
	padding: 2px 0px 2px 22px;
	float: left;
}
#imprimerETpagination br {
	font-size:0;
	height: 0;
	line-height: 0;
}

/* Pagination */
#pagination {
	color:#003366;
	font-size:0.9em;
	margin-bottom: 15px;
	text-align: right;
}
#pagination a {
	color:#003366;
}
#pagination .clic {
	color:#610946;
	font-weight:bold;
}


/****************************************************************************
* PAGE INDEX
****************************************************************************/

#index  {
	padding-bottom:15px;
}
#index h1 {
	margin: 0px;
	white-space:nowrap;
	font-size:0.7em;
	font-family:Arial, Helvetica, sans-serif;
	color:#003366;
	font-weight:normal;
	padding-bottom:5px;
}
#index .date {
	font-weight:bold;
	color:#FF8A00;
	padding-left:0px;
}

/* texte */
#index p {
	margin:0;
	color:#003366;
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:0.6em;
}
#articleIndex {
	background: #5a6e6f;
	padding: 1px 5px 5px;
	color: #e7eded;
	margin: 0px 0px 1px;
}
#articleIndex .titreRubrique {
	height:20px;
	border-bottom:1px solid #D8E0E9;
	margin-bottom:10px;
	display: none;
}
#articleIndex .titreRubrique h1 {
	background:#fff url(../pix/images/common_bleu/titre_articles.gif) no-repeat top left;
	margin:0;
	height:14px;
}
#articleIndex .titreRubrique h1 span{
	display:none;
}
#articleIndex .dateArticle{
	height:10px;
	float: left;
	clear: left;
	margin: 10px 5px 0px 0px;
}
#articleIndex .dateIndex {
	font-size:1em;
	margin: 0;
	float:left;
}
#articleIndex a.pieceJointe {
	/*display:none;*/ /* pour cacher la piece jointe */
	height:11px;
	width:10px;
	background: url(../pix/images_sin/puce_li_bg_grisfonce.gif) no-repeat 0px 1px;
	text-decoration:none;
	float: left;
	margin-right: 7px;
	padding: 0;
}
#articleIndex p {
	margin:0px 0px 0px 135px;
	font-size:1em;
}
#articleIndex a.article {
	color:#E7EDED;
	background: url(none);
	padding: 0px;
	font-weight: normal;
}
#articleIndex .lireActualites {
	display: none;
}
#sousRubriqueIndex {
	background: #5a6e6f;
	color: #e7eded;
	padding: 5px 5px 0;
}
#sousRubriqueIndex .titreRubrique {
	height:22px;
	padding-bottom:0px;
	border-bottom:1px solid #D8E0E9;
	display: none;
}
#sousRubriqueIndex .titreRubrique h1 {
	background:#fff url(../pix/images/common_bleu/titre_sous_rubrique.gif) no-repeat top left;
	margin:0;
	height:18px;
}
#sousRubriqueIndex .titreRubrique h1 span{
	display:none;
}
#sousRubriqueIndex .ligneSousRubrique {
	padding: 0px 0px 10px;
}
#sousRubriqueIndex .ligneSousRubrique .titreArticle{
	padding-left:17px;
	background: url(../pix/images_sin/puce_li_bg_grisfonce.gif) no-repeat 0px 0px;
	font-size:1em;
	float: left;
}
#sousRubriqueIndex a {
	color:#FFFFFF;
	font-weight: bold;
	margin-right: 5px;
	background: url(none);
	padding: 0px;
}
#sousRubriqueIndex .ligneSousRubrique .nbreArticle{
	font-size:1em;
}
#sousRubriqueIndex .ligneSousRubrique .nbreArticle a {
	font-weight: normal;
	margin: 0;
}

/********************************
* Colonne de droite VOIR AUSSI
*********************************/
#rightnavVoirAussi {
	clear: both;
	padding: 5px;
	background: #e7eded;
	margin: 10px 0px;
}

/* rubrique */
#rightnavVoirAussi h1 {
	margin: 0em 0em 1em;
	padding: 0px 0px 5px;
	border-bottom: 1px solid #FFFFFF;
	font-size: 1.4em;
	color: #a4b8b9;
}

/* lien téléchargement */
#article h2.voirAussi{
	display:none;
}
ul.voirAussiDownload {
	list-style: url(none) none inside;
	margin: 0 0 2em;
	padding:0;
	line-height: 1.0em;
}
li.voirAussiDownload, li.voirAussiDownloadpdf, li.voirAussiDownloadhlp ,li.voirAussiDownloadxls , li.voirAussiDownloadswf, li.voirAussiDownloadgif, li.voirAussiDownloadhtml , li.voirAussiDownloadhtm ,li.voirAussiDownloadjpg , li.voirAussiDownloadjpeg , li.voirAussiDownloadjpe , li.voirAussiDownloadmp3, li.voirAussiDownloadmpeg, li.voirAussiDownloadmpg, li.voirAussiDownloadpng ,li.voirAussiDownloadppt , li.voirAussiDownloaddoc, li.voirAussiDownloadrtf, li.voirAussiDownloadzip {
	color: #660033;
	margin: 0;
	padding: 0 0 5px;
}
li.voirAussiDownload a, li.voirAussiDownloadpdf a, li.voirAussiDownloadhlp a, li.voirAussiDownloadxls a, li.voirAussiDownloadswf a, li.voirAussiDownloadgif a, li.voirAussiDownloadhtml a, li.voirAussiDownloadhtm a, li.voirAussiDownloadjpg a, li.voirAussiDownloadjpeg a, li.voirAussiDownloadjpe a, li.voirAussiDownloadmp3 a, li.voirAussiDownloadmpeg a, li.voirAussiDownloadmpg a, li.voirAussiDownloadpng a, li.voirAussiDownloadppt a, #li.voirAussiDownloaddoc a, li.voirAussiDownloadrtf a, li.voirAussiDownloadzip a {
	color: #660033;
	font-weight: bold;
	background: url(none);
	padding: 0;
}
#rightnavVoirAussi li a {
	color: #660033;
	font-weight: bold;
	background: url(none);
	padding: 0;
}
li.voirAussiDownload {
	list-style: url(../pix/images_sin/puce_a_bg_clair.gif) inside;
}
li.voirAussiDownloadhlp {
	list-style: url(../pix/picto/picto_aide.gif) inside;
}
li.voirAussiDownloadpdf {
	list-style: url(../pix/picto/picto_acrobat.gif) inside;
}
li.voirAussiDownloadxls {
	list-style: url(../pix/picto/picto_excel.gif) inside;
}
li.voirAussiDownloadswf {
	list-style: url(../pix/picto/picto_flash.gif) inside;
}
li.voirAussiDownloadgif {
	list-style: url(../pix/picto/picto_gif.gif) inside;
}
li.voirAussiDownloadhtml, li.voirAussiDownloadhtm {
	list-style: url(../pix/picto/picto_html.gif) inside;
}
li.voirAussiDownloadjpg ,li.voirAussiDownloadjpeg ,li.voirAussiDownloadjpe {
	list-style: url(../pix/picto/picto_jpg.gif) inside;
}
li.voirAussiDownloadmp3 {
	list-style: url(../pix/picto/picto_mp3.gif) inside;
}
li.voirAussiDownloadmpeg, li.voirAussiDownloadmpg {
	list-style: url(../pix/picto/picto_mpeg.gif) inside;
}
li.voirAussiDownloadpng {
	list-style: url(../pix/picto/picto_png.gif) inside;
}
li.voirAussiDownloadppt {
	list-style: url(../pix/picto/picto_ppt.gif) inside;
}
li.voirAussiDownloaddoc, li.voirAussiDownloadrtf {
	list-style: url(../pix/picto/picto_word.gif) inside;
}
li.voirAussiDownloadzip {
	list-style: url(../pix/picto/picto_zip.gif) inside;
}
ul.voirAussiArticle {
	margin: 0px;
	padding:0px;
	color:#610946;
}
li.voirAussiArticle {
	margin: 0 0 5px 0;
	padding: 0px 0 2px 0px;
	list-style: url(../pix/images_sin/puce_a_bg_clair.gif) inside;
}


/****************************************************************************
* BAS DE PAGE COMMUN
****************************************************************************/

/********************************
* Pied de page
*********************************/
#pieddepage {
	padding: 6px 0px;
	text-align: center;
	color: #000066;
	clear: both;
}
#pieddepage a {
	color: #000066;
}
#pieddepage h1 {
	display: none;
}


/****************************************************************************
* PLAN DU SITE
****************************************************************************/

#planSite {
	font-size: 1em;
}
#planSite p {
	font-weight:bold;
	color:#003366;
	margin-left:8px;
	padding:0px;
}
#planSite h1 {
	font-weight:bold;
	margin:0px;
	padding:12px 10px 9px;
	font-size: 1em;
	background: #5a6e6f;
}
#planSite h1 a {
	color: #FFFFFF;
	border-bottom: 1px solid #8c9a9a;
	display: block;
	margin: 0;
	padding: 0px 0px 5px 17px;
	background: url(../pix/images_sin/puce_li_bg_grisfonce.gif) no-repeat 0px 2px;
}
#planSite br {
	display: none;
}
#planSite hr {
	color:#003366;
	margin: 0px 0px 0px 10px;
	display: none;
}
#planSite ul {
	background: #e7eded;
	margin: 0px 0px 10px;
	padding: 15px 10px 5px;
	list-style: none;
}
#planSite li {
	margin: 0px 0px 7px;
}
#planSite a {
	color:#610946;
	font-weight: bold;
	background: url(../pix/images_sin/puce_a_bg_clair.gif) no-repeat 0px 0px;
	padding: 1px 0px 2px 17px;
}

/********************************
* Personnalisables
*********************************/

#bandeau .btLogo {
	float:left;
	height:148px;
	width:173px;
	background:url(../pix/images_sin/logo.jpg) no-repeat 0px 0px; /* style pour code couleur */
}
#moteurEToutils {
	background:url(../pix/images_sin/bg_rech_outil2b.jpg) repeat-x 0px 0px;/* style pour code couleur */
	margin: 0px;
	height:45px;
	clear:both;
	padding: 8px 0px 22px;
}
#moteurEToutils  a.rechercheAvancee {
	margin-left:6px;
	padding-right:6px;
	text-decoration:none;
	font-family:Arial, Helvetica, sans-serif;
	font-size:10px;
	color:#fff;
	border-right:1px solid #C5D2E4; /* style pour code couleur */
	vertical-align:middle;
}
/* Fil d'ariane */
#filAriane {
	background:#FFFFFF; /* style pour code couleur */
	margin-bottom: 5px;
	/*height:22px;*/
	min-height:22px;
	padding: 5px;
}
#filAriane .accueilAriane {
	float:left;
	background:url(../pix/images_sin/bouton_fil_ariane.gif) no-repeat 50% 0%; /* style pour code couleur */
	width:19px;
	height:22px;
}
#filAriane .debutAriane {
	float:left; /* style pour code couleur */
	width:5px;
	height:22px;
}
#filAriane .texteAriane {
	color: #758f90; /* style pour code couleur */
	float: none;
	font-size: 1em;
	padding-top: 5px;
	padding-right: 0px;
	padding-bottom: 5px;
	padding-left: 0px;
}
#filAriane .current {
	font-weight: bold;
}
#filAriane .texteAriane a{
	color:#758f90; /* style pour code couleur */
}
#filAriane .finAriane {
	float:none;
	height:0px;
	clear: both;
	line-height: 0px;
	font-size: 0px;
	display: none;
}
h1.servicesPratiques {
	/* font-family:Verdana, Arial, Helvetica, sans-serif;
	color:#FFFFFF;
	font-weight:bold;
	font-size:1em;
	padding-left:9px;
	padding-top:3px;
	margin:0;
	height:17px;
	border-bottom:1px solid #fff;
	background:#2F6190 url(../pix/images/common_bleu/titre_rubrique_gauche.png) no-repeat top left; style pour code couleur */
	color:#FFFFFF;
	font-weight:bold;
	font-size:1.1em;
	margin:0;
	background: #610946;
	padding: 5px 5px 5px 10px;
	line-height: 1.1em;
}
#leftnav h2.servicesPratiques {
	display: none;
	margin:0px;
	height:30px;
	width:169px;
	background: url(../pix/images/common_bleu/visuel_services_pratiques.jpg) no-repeat top left; /* style pour code couleur */
}
#lienBoxGauche {
	background: #5A6E6F;
	padding: 10px 10px 10px 5px;
	margin-top: 1px;
}
li.servicesPratiques {
	font-size: 1em;
	list-style: url(../pix/images_sin/puce_li_bg_grisfonce.gif) outside;
	margin: 0em 0em 0.5em;
	color: #e7eded;
}
#titreAccueil h1 {
	margin: 0;
	border-bottom: 1px solid #CCCCCC;
	font-size: 2em;
	font-weight: normal;
	font-style: italic;
	color: #610946;
	line-height: 1em;
	padding: 10px 0px 2px;
	width: 360px;
}
#accueil h2 {
	color:#003366;
	font-weight:bold;
	margin:0px 0px 15px;
	background: #FFFFFF;
	padding: 0px;
	font-size: 1em;
}
#accueil a {
	color:#003366; /* style pour code couleur */
	text-decoration:none;
}
#accueil  a.lireArticle, #unes a.lireArticle {
	background: url(../pix/images_sin/puce_a_bg_clair.gif) no-repeat 0px 0px;
	font-size: 1em;
	font-weight: normal;
	padding: 1px 0px 2px 17px;
	color: #ff0012;
}
#titreActualites .titre h1 {
	margin: 0;
	border-bottom: 1px solid #FFFFFF;
	font-size: 2em;
	font-weight: normal;
	font-style: italic;
	color: #610946;
	line-height: 1em;
	padding: 10px 0px 2px;
}
.dateActualites {
	margin: 0;
	padding: 0;
	line-height:1.2em; /* style pour code couleur */
}

#articleIndex h2 {
	float:none;
	font-size:1em;
	margin: 10px 0 0 135px;
	width: auto;
}
#articleIndex h2 a {
	color:#FFFFFF;
	background: url(none);
	padding: 0px;
}

#article .left {
	float:left;
	padding-right:10px;
}
.right {
	float:right;
	padding-left:10px;
}
.left {
	float:left;
	padding-right:5px;
}
.f_bt {
	color: #993300;
	background: url(../pix/images_sin/puce_a_bg_clair.gif) no-repeat 0px 0px;
	padding: 0px 0px 0px 13px;
	border-style: none;
	font-weight: bold;
	width: auto;
	margin-right: 15px;
}

@media print {
	body {
		background-image: none;
		font-size: 80%;
	}
	#container {
	  width: 600px;
	}
	#article {
	  width: 600px;
	}
	#bandeau, #leftnav, #profils, #moteurEToutils {
		display: none;
	}
}
#eregiebas {
	margin-bottom: 10px;
}
