function popup_windowNorm(sUrl)
{
	popup_windowSize(sUrl,600,440);
}

function popup_windowLarge(sUrl)
{
	popup_windowSize(sUrl,802,600);
}

function popup_windowAdvancedSearch(sUrl)
{
	var win_width = (screen.availWidth)-6;
	var win_height = (screen.availHeight)-30;
	
	var urlTest=window.location.href; 
    var exprReg=/^http:\/\/iww\./gi; 
    var isValidSearchIntranet= exprReg.test(urlTest); 
    if (isValidSearchIntranet) {    
		window.open(sUrl,'','top=0,left=0,height='+win_height+',width='+win_width+',status=no,toolbar=no,directories=no,menubar=no,location=no,resizable=yes,titlebar=yes,scrollbars=yes');
	}
	else {
		popup_windowSize(sUrl,802,600,'status=yes,toolbar=no,directories=no,menubar=no,location=no,resizable=yes,titlebar=yes,scrollbars=yes');
	}
}

function popup_windowSearch(sName, sType)
{
	var win_width = (screen.availWidth)-6;
	var win_height = (screen.availHeight)-30;
	
    var urlTest=window.location.href; 
    var exprReg=/^http:\/\/iww\./gi; 
    var isValidSearchIntranet= exprReg.test(urlTest); 

    if (sType!=null||isValidSearchIntranet) 
		window.open('',sName,'top=0,left=0,height='+win_height+',width='+win_width+',status=no,toolbar=no,directories=no,menubar=no,location=no,resizable=yes,titlebar=yes,scrollbars=yes');
	else
		window.open('',sName,'top=0,left=0,height=600,width=802,status=no,toolbar=no,directories=no,menubar=no,location=no,resizable=yes,titlebar=yes,scrollbars=yes');
}

function popup_windowLargeMenuBar(sUrl)
{
	window.open(sUrl,'','top=0,left=0,height=600,width=802,status=no,toolbar=no,directories=no,menubar=yes,location=yes,resizable=yes,titlebar=yes,scrollbars=yes');	
}
function popup_windowPrintMenuBar(sUrl)
{
	window.open(sUrl,'','top=0,left=0,height=600,width=600,status=no,toolbar=no,directories=no,menubar=yes,location=yes,resizable=yes,titlebar=yes,scrollbars=yes');	
}

function popup_windowLargeToolBar(sUrl)
{
	window.open(sUrl,'','top=0,left=0,height=600,width=802,status=yes,toolbar=yes,directories=no,menubar=yes,location=yes,resizable=yes,titlebar=yes,scrollbars=yes');	
}

function popup_windowSize(sUrl,width,height,features)
{	
	if ( features!=null )
		window.open(sUrl,'','top=0,left=0,height='+height+',width='+width+','+features);
	else
		window.open(sUrl,'','top=0,left=0,height='+height+',width='+width+',status=no,toolbar=no,directories=no,menubar=no,location=no,resizable=yes,titlebar=yes,scrollbars=yes');	
}

function popup_windowNAV(sUrl)
{
	if (sUrl!="") {
		var winNAV = window.open(sUrl,"","top=0,left=0,width=650,height=440,status=no,toolbar=no,directories=no,menubar=no,location=no,resizable=yes,titlebar=no,scrollbars=yes");
		return winNAV;
	}
}
function popup_windowNAVSmall(sUrl)
{
	if (sUrl!="") {
		var winNAV = window.open(sUrl,"","top=0,left=0,width=428,height=318,status=no,toolbar=no,directories=no,menubar=no,location=no,resizable=no,titlebar=no,scrollbars=no");
		return winNAV;
	}
}
function popup_windowPrint(sUrl)
{
	if (sUrl!="") {
		var winNAV = window.open(sUrl,"","top=0,left=0,width=650,height=440,status=no,toolbar=yes,directories=no,menubar=no,location=no,resizable=yes,titlebar=no,scrollbars=yes");
		return winNAV;
	}
}


function ouvrirFen(theURL, theSize) { //v2.0
	if (top.altair) {
	altair.location=theURL;
	}
	else{
	fenAltair=window.open(theURL,'altair',theSize);
	}
	fenAltair.focus();
}

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function popup_windowFull(sUrl)
{
	if (sUrl!="") {
		var winNAV = window.open(sUrl,"","top=0,left=0,width=650,height=440,status=yes,toolbar=yes,directories=no,menubar=yes,location=yes,resizable=yes,titlebar=yes,scrollbars=yes");
		return winNAV;
	}
}
function popup_windowLink(sUrl)
{
	if (sUrl!="") {
		var winNAV = window.open(sUrl,"","top=0,left=0,width=650,height=440,status=yes,toolbar=yes,directories=no,menubar=yes,location=yes,resizable=yes,titlebar=yes,scrollbars=yes");
		return winNAV;
	}
}

function popup_showModalLogin(sUrl)
{
	if (sUrl!="") {
		var winNAV = window.showModalDialog(sUrl,"Login","dialogWidth:530px;dialogHeight:440px;center:1;scroll:0;help:0;status:0");
		return winNAV;
	}
}

function popup_windowLogin(sUrl)
{
	if (sUrl!="") {
		var winNAV = window.open(sUrl,"Login","top=0,left=0,width=530,height=440,status=no,toolbar=no,directories=no,menubar=no,location=no,resizable=yes,titlebar=no,scrollbars=yes");
		return winNAV;
	}
}


function showSite(url) {   
	if (!url=="")
	{
		popup_windowSize(url,802,600);
	}
}  

function showSiteFromSelect(o)
{
	var v = o.options[o.selectedIndex].value;
	o.selectedIndex=0;
	showSite(v);
}
function goToUrlFromSelect(o)
{
	var v = o.options[o.selectedIndex].value;
	o.selectedIndex=0;
	var tmp = v.split(";");
	vUrl = tmp[1]
	if (tmp[0]=='same'){		
		if (vUrl!=''){
			self.document.location=vUrl;
		}
	}
	else if (tmp[0]=='new'){
		if (vUrl!=''){
			window.open(vUrl);
		}
	}
  else{
	  popup_windowLargeMenuBar(vUrl);
	}
}


function showOpnw3FromSelect(o)
{
	var v = o.options[o.selectedIndex].value;
	o.selectedIndex=0;
	window.location=v;
}

function popupEnlargeToContent()
{
    self.document.body.scroll="no";

    var wBodyAff = self.document.body.clientWidth;
    var wBodyNeeded = self.document.body.scrollWidth;
    var wResizeBy = 0;
    if (wBodyAff < wBodyNeeded)
    {       
        wResizeBy = wBodyNeeded - wBodyAff; 
    }
    var resizeBy = 0;
    var hBodyAff = self.document.body.clientHeight;
    var hBodyNeeded = self.document.body.scrollHeight;
    if (hBodyAff < hBodyNeeded)
    {
        hResizeBy = hBodyNeeded - hBodyAff;
    }
    self.window.resizeBy(wResizeBy, hResizeBy);
}

function previewEnlargeToContent()
{
    
    var wBodyAff = self.document.body.clientWidth;
    var wBodyNeeded = self.document.body.scrollWidth;
    var wScreen = self.screen.width;
    var wResizeBy = 0;

    if (wBodyNeeded > wScreen)
    {       
        self.document.body.scroll.horizontal="yes"; 
        wResizeBy = wScreen - wBodyAff - 30;
    }
    else if (wBodyNeeded > wBodyAff)
    {
        wResizeBy = wBodyNeeded -wBodyAff;
    }
    
    var hBodyAff = self.document.body.clientHeight;
    var hBodyNeeded = self.document.body.scrollHeight;
    var hScreen = self.screen.height;
    var hResizeBy = 0;
    //alert (hBodyAff+" "+hBodyNeeded+" "+hScreen);
    if (hBodyNeeded > hScreen-120)
    {       
        self.document.body.scroll.vertical="yes"; 
        hResizeBy = hScreen - hBodyAff-200;
    }
    else if (hBodyNeeded > hBodyAff)
    {
        self.document.body.scroll.vertical="no";
        hResizeBy = hBodyNeeded - hBodyAff;
    }
            
    //alert(hResizeBy);
    self.window.resizeBy(wResizeBy, hResizeBy);
    self.window.moveTo(0,0);

}

function gotoLanguageSelector(select)
{
    var index = select.selectedIndex;
    if (index < 0) return;
    var value = select.options[index].value;
    document.location.href = value;
}


function writeFlash(flashURL, width, height, align)
{
	document.write('<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" ');
	if (width!=null && width!='') {
	   document.write(' width="'+width+'" ');
  }
  if (height!=null && height!='') {
	   document.write(' height="'+height+'" ');
  }
  if (align!=null && align!='') {
	   document.write(' align="'+align+'" ');
  }
	document.write('><param value="'+flashURL+'" name="movie">');
	document.write('<param value="high" name="quality">');
	document.write('<param value="transparent" name="wmode">');
	document.write('<embed type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" quality="high" wmode="transparent" src="'+flashURL+'" ');
	if (width!=null && width!='') {
	   document.write(' width="'+width+'" ');
  }
  if (height!=null && height!='') {
	   document.write(' height="'+height+'" ');
  }	
	document.write('></embed></object>');
}
