function glossaireTooltip(id) {
	var tooltipWidth = 420
	var xhr_object = null;
	var position = id;
	if(window.XMLHttpRequest)  xhr_object = new XMLHttpRequest();
	else if (window.ActiveXObject)  xhr_object = new ActiveXObject("Microsoft.XMLHTTP");

	document.getElementById('glossaireContent').innerHTML = '<img src="'+images_path+'loading.gif" /> Chargement...';

	xhr_object.open("GET", c_reverse_path+'service_project/glossaire/index.php?id='+id.replace('glossaire_tooltip_',''), true);
	xhr_object.onreadystatechange = function(){
		if ( xhr_object.readyState == 4 ) {
			document.getElementById('glossaireContent').innerHTML = xhr_object.responseText;
		}
	}
	document.getElementById('glossaireBox').style.width = tooltipWidth + 'px'
	centerDiv(document.getElementById('glossaireBox'),tooltipWidth)
	xhr_object.send(null);
}

function centerDiv(divobj,divobjWidth){
	var standardbody=(document.compatMode=="CSS1Compat")? document.documentElement : document.body //create reference to common "body" across doctypes
	var ie=document.all && !window.opera
	var dom=document.getElementById
	var scroll_top=(ie)? standardbody.scrollTop : window.pageYOffset
	var scroll_left=(ie)? standardbody.scrollLeft : window.pageXOffset
	var docwidth=(ie)? standardbody.clientWidth : window.innerWidth-16
	var docheight=(ie)? standardbody.clientHeight: window.innerHeight
	var docheightcomplete=(standardbody.offsetHeight>standardbody.scrollHeight)? standardbody.offsetHeight : standardbody.scrollHeight //Full scroll height of document
	var objwidth=divobjWidth //width of div element
	var objheight=divobj.offsetHeight //height of div element
	var topposition=(docheight>objheight)? scroll_top+docheight/2-objheight/2+"px" : scroll_top+10+"px" //Vertical position of div element: Either centered, or if element height larger than viewpoint height, 10px from top of viewpoint
	divobj.style.left=docwidth/2-objwidth/2+"px" //Center div element horizontally
	divobj.style.top=Math.floor(parseInt(topposition))+"px"
	divobj.style.zIndex=3000
	divobj.style.display=""
}

//***************************************************AFFICHER LIGNE
function showMe(portlet) {
		document.getElementById(portlet).style.display="";
}
function hideMe(portlet) {
		document.getElementById(portlet).style.display="none";
}

function envoieRequete(url,id) {
	var xhr_object = null;
	var position = id;
	if(window.XMLHttpRequest)  xhr_object = new XMLHttpRequest();
	else if (window.ActiveXObject)  xhr_object = new ActiveXObject("Microsoft.XMLHTTP");

	xhr_object.open("GET", url, true);
	xhr_object.onreadystatechange = function(){
		if ( xhr_object.readyState == 4 ) {
			document.getElementById(position).innerHTML = xhr_object.responseText;
		}
	}
	xhr_object.send(null);
}

//*************************************************AIDE A LA SAISIE
function SelObj(formname,selname,textname,str) {
	this.formname = formname;
	this.selname = selname;
	this.textname = textname;
	this.select_str = str || '';
	this.selectArr = new Array();
	this.initialize = initialize;
	this.bldInitial = bldInitial;
	this.bldUpdate = bldUpdate;
}

function initialize() {
	if (this.select_str =='') {
		for(var i=0;i<document.forms[this.formname][this.selname].options.length;i++) {
			this.selectArr[i] = document.forms[this.formname][this.selname].options[i];
			this.select_str += document.forms[this.formname][this.selname].options[i].value+":"+
			document.forms[this.formname][this.selname].options[i].text+",";
		}
	}
	else {
		var tempArr = this.select_str.split(',');
		for(var i=0;i<tempArr.length;i++) {
			var prop = tempArr[i].split(':');
			this.selectArr[i] = new Option(prop[1],prop[0]);
		}
	}
	return;
}

function bldInitial() {
	this.initialize();
	for(var i=0;i<this.selectArr.length;i++)
		document.forms[this.formname][this.selname].options[i] = this.selectArr[i];
	document.forms[this.formname][this.selname].options.length = this.selectArr.length;
	return;
}

function bldUpdate() {
	var str = document.forms[this.formname][this.textname].value.replace('^\\s*','');
	if(str == '') {this.bldInitial();return;}
	this.initialize(); //lwf
	var j = 0;
	pattern1 = new RegExp("^"+str,"i");
	for(var i=0;i<this.selectArr.length;i++)
		if(pattern1.test(this.selectArr[i].text))
			document.forms[this.formname][this.selname].options[j++] = this.selectArr[i];
	document.forms[this.formname][this.selname].options.length = j;
	if(j==1) {
		document.forms[this.formname][this.selname].options[0].selected = true;
	}
}

function setUp() {
	obj1 = new SelObj('menuform','itemlist','entry');
	obj1.bldInitial();
}

function popupscroll(url, width, height, full, name){
	var top=(screen.height-height)/2;
	var left=(screen.width-width)/2;
	var nsNav = (document.layers) ? 1 : 0;
	var ieNav = (document.all) ? 1 : 0;

	// popup plein ecran
	if(full){
		height = screen.availHeight - 30;
		width = screen.availWidth - 10;
		top=0;
		left=0;
	}
    if (name) {
        winname = name;
    } else {
        winname = "popupscroll";
    }
	wintype="toolbar=no,menubar=no,location='no',scrollbars=yes,top="+top+",left="+left;
	wintype=wintype + ",height=" + height + ",width=" + width;
	var newwin = window.open(url,winname,wintype);
	newwin.focus();
}

function integerStrictlyPositive(value) {
	var StrValidChars = "0123456789";
	var StrString = value;
	var StrChar;

	if (StrString != '') {
		for (i = 0; i < StrString.length ; i++) {
			StrChar = StrString.charAt(i);
			if (StrValidChars.indexOf(StrChar) == -1) {
				return false;
			}
		}
	}
	return true;
}