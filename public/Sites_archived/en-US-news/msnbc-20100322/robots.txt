# robots.txt file for www.msnbc.com, www.cnbc.com

User-agent: *
Disallow: error404.aspx

Sitemap: http://www.msnbc.msn.com/xml/SitemapIndex.xml
Sitemap: http://www.cnbc.com/xml/SitemapIndexCNBC.xml