/* Style Variablen f�r Hauptnavi �berschreiben */
g_homemenu_hl_fontcolor = "#CC0000"; //"rgb(204, 0, 0)";
g_homemenu_hl_background_color = "transparent";
g_homemenu_hl_background_color_Level5 = "#FFFFFF";
g_homemenu_ll_fontcolor_level0 = "#000000";
g_homemenu_ll_fontcolor_level1 = "#000000";
g_homemenu_ll_background_color = "#eef2f5"; 
g_homemenu_ll_background_image_level0 = "url(/images/arrow-black.gif)";
g_homemenu_ll_background_image_level1 = "url(/images/arrow-black.gif)";
g_homemenu_ll_background_position_level0 = "210px -592px";
g_homemenu_ll_background_position_level1 = "210px -592px";
g_jdpowerphase = "200803";

/* Hauptmen�: Anzahl der Men�eintr�ge in Subnavi erh�hen */
G_MAXSUBLAYERS = 18;

/* Teaser Scroller */
var g_firstVisible = 1;
var g_lastvisible = 4;
var g_LastTeaser = 0;
var g_teaserWidth = 167 + 9;
var g_xposLastVisibleTeaser = g_teaserWidth * (g_lastvisible - g_firstVisible);

function setLastTeaser()
{
  var elem = document.getElementsByTagName("li");
  if (elem)
  {
    var i = 0;
    for (i = 0; i < elem.length; i++)
    {
      if (elem[i].className)
      {
        if (elem[i].className.indexOf("teaser") >= 0)
        {
          // PS4 rendert kein id Attribut
          if (!elem[i].id)
          {
            elem[i].id = elem[i].className;
          }
          elem[i].style.left = (g_LastTeaser * g_teaserWidth).toString()+"px";
          g_LastTeaser++;
        }
      }
    }
  }
}

function showHideTeaserNavbar()
{
    var leftNav = document.getElementById("teaserbarNavLeftLink");
    if(g_firstVisible == 1)
        leftNav.style.display = "none";
    else
        leftNav.style.display = "block";

    var rightNav = document.getElementById("teaserbarNavRightLink");
    if(g_lastvisible == g_LastTeaser)
        rightNav.style.display = "none";
    else
        rightNav.style.display = "block";
}
function moveTeaserbar()
{
    var wrapper = document.getElementById("teaserBarWrapper");
    if(!wrapper)
        return;
    var elem = document.getElementsByTagName("ul");
    if(!elem)
        return;
    var i = 0;
    for(i; i < elem.length; i++)
    {
        if(elem[i].className.indexOf("teaserBar") >= 0)
            break;
    }
    var teaserBar = elem[i];
    if(!teaserBar)
        return;
    wrapper.appendChild(teaserBar);
    teaserBar.style.display = 'block';
    if (g_LastTeaser == 0)
    {
        setLastTeaser();
    }
    showHideTeaserNavbar();
}
function scrollTeaserBar(direction)
{
    if(!direction)
        return;
    if (g_LastTeaser == 0)
    {
        setLastTeaser();
    }

  var numTeasers = 4;

  if(direction == 'next')
  {
    if(g_lastvisible < g_LastTeaser)
    {
      if((g_lastvisible + 4) > g_LastTeaser)
      {
        // weniger als 4 �brig:
        numTeasers = g_LastTeaser - g_lastvisible;
      }
      g_lastvisible += numTeasers;
      g_firstVisible = g_lastvisible - 4 + 1;
      numTeasers = -numTeasers;
      scrollTeaserBarBy( numTeasers * g_teaserWidth);
    }
  }

  if(direction == 'previous')
  {
    if(g_lastvisible > 4)
    {
      if((g_lastvisible - 4) >= 1)
      {
        // weniger als 4 �brig:
        numTeasers = g_lastvisible - 4;
      }
      g_lastvisible -= numTeasers;
      g_firstVisible = g_lastvisible - 4 + 1;
      scrollTeaserBarBy( numTeasers * g_teaserWidth);
    }
  }
}
function scrollTeaserBarBy(px)
{
    var elem = document.getElementsByTagName("ul");
    if(!elem)
        return;
    var i = 0;
    for(i; i < elem.length; i++)
    {
        if(elem[i].className.indexOf("teaserBar") >= 0)
            break;
    }
    var teaserBar = elem[i];
    if(!teaserBar)
        return;

    var step = getStep(px);
    if(px < 0)
    {
        step = -step;
        if(px < step)
        {
            teaserBar.style.left = (teaserBar.offsetLeft + step).toString() + "px";
            px -= step;
            window.setTimeout('scrollTeaserBarBy(' + px.toString() + ')', 10);
        }
        else
        {
            teaserBar.style.left = (teaserBar.offsetLeft + px).toString() + "px";
            showHideTeaserNavbar();
        }
    }
    else
    {
        if(px > step)
        {
            teaserBar.style.left = (teaserBar.offsetLeft + step).toString() + "px";
            px -= step;
            window.setTimeout('scrollTeaserBarBy(' + px.toString() + ')', 10);
        }
        else
        {
            teaserBar.style.left = (teaserBar.offsetLeft + px).toString() + "px";
            showHideTeaserNavbar();
        }
    }
}

function getStep(px)
{
  // abs()
  if(px < 0)
    px = -px;

  if(px >= 300)
    return 50;

  if(px >= 50)
    return 20;

  if(px >= 15)
    return 5;

  return 2;
}