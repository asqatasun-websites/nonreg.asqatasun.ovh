EditoPoll = function(id, question, open)
{
	this._id=id;
	this._question=question;
	this._answers=new Array();
	this._hits=0;
	this._choice=null;
	this._open=open;
	//this._container = null;
	this._instanceNumber=EditoPoll.nbInstances++;
	EditoPoll.polls[this._instanceNumber]=this;
	
};

EditoPoll.nbInstances = 0;
EditoPoll.polls = new Array();
EditoPoll.cookie = new idsCookie('polls', new Date(2099, 12, 31), '/');

EditoPoll.prototype.addAnswer = function(id, text, hits)
{
	this._hits+=hits;
	return this._answers['op'+id]=new EditoPollAnswer(id, text, hits);
};

EditoPoll.prototype.getElementId = function(what)
{
	return 'poll'+this._instanceNumber+'-'+what;
};

EditoPoll.prototype.init = function()
{
	var buffer = document.createElement('div');
	var container = document.createElement('div');
	buffer.appendChild(container);
	container.setAttribute('id', this.getElementId('container'));
	container.setAttribute('class', 'poll-container');
	if(EditoPoll.cookie.isId(this._id)||(!this._open))
	{
		document.write(buffer.innerHTML);
		this.writeResults(); // after because need the dom to be ready
	}
	else
	{
		this.writeForm(container);
		document.write(buffer.innerHTML);
	}
};

EditoPoll.prototype.writeForm = function(container)
{
	var form = document.createElement('form');
	form.setAttribute('id', this.getElementId('form'));
	form.setAttribute('class', 'poll-form');
	form.setAttribute('onsubmit', 'EditoPoll.polls['+this._instanceNumber+'].vote(this);return false;');
	
	var question =  document.createElement('div');
	form.appendChild(question);
	question.setAttribute('class', 'question');
	question.innerHTML=this._question;
	
	for(var i in this._answers)
	{
		var reponse = document.createElement('div');
		form.appendChild(reponse);
		reponse.setAttribute('class', 'reponse');
		
		var input=null;
		try {
			// cause ie sucks
			input = document.createElement('<input type="radio" name="poll">');
		}
		catch(e)
		{
			input=null;
		}
		if(!input)
		{
			input = document.createElement('input');
			input.setAttribute('type', 'radio');
			input.setAttribute('name', 'poll');
		}
		input.setAttribute('id', this.getElementId('opt'+this._answers[i]._id));
		input.setAttribute('value', this._answers[i]._id);
		reponse.appendChild(input);
		
		var label = document.createElement('label');
		reponse.appendChild(label);
		label.setAttribute('for', this.getElementId('opt'+this._answers[i]._id));
		label.innerHTML=this._answers[i]._text;
	}

	var submit = null;
	try {
		// cause ie sucks
		submit = document.createElement('<input type="submit">');
	}
	catch(e)
	{
		submit = document.createElement('input');
		submit.setAttribute('type', 'submit');
	}
	form.appendChild(submit);
	
	submit.setAttribute('value', 'Votez !');

	container.appendChild(form);
	
};

EditoPoll.prototype.vote = function(form)
{
	this._choice=null;
	for(var i=0;i<form.poll.length;i++)
	{
		if(form.poll[i].checked)
		{
			this._choice=form.poll[i].value;
		}
	}
	if(!this._choice)
	{
		alert('Choisissez une r�ponse !');
		return;
	}
	var op=this._choice;
	
	if(EditoPoll.cookie.isId(this._id))
	{
		op='';
	}
	
	var xhr_object;
	if(window.XMLHttpRequest) // Firefox 
		xhr_object = new XMLHttpRequest(); 
	else if(window.ActiveXObject) // Internet Explorer 
		xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
	else { // XMLHttpRequest non support� par le navigateur 
		alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
	return false; 
	} 
	xhr_object.open("GET", xmlBaseUrl+'?w=poll&id='+this._id+'&op='+this._choice, true); 
	var self=this;
	xhr_object.onreadystatechange = function() {
	  if(xhr_object.aborted) return;
      if(xhr_object.readyState == 4)
      {
      	if(xhr_object.status  == 200)
      	{
      		var docXML = xhr_object.responseXML;
      		if(docXML.getElementsByTagName("code")&&docXML.getElementsByTagName("code").item(0)&&docXML.getElementsByTagName("code").item(0).firstChild.data=='ok')
      		{
      			var hits=0;
				for(var i=0;i<docXML.getElementsByTagName("reponse").length;i++)
				{
					var node_reponse=docXML.getElementsByTagName("reponse")[i];
					var h=parseInt(node_reponse.getAttribute('votes'));
					self._answers['op'+node_reponse.getAttribute('id')]._hits=h;
					hits+=h;
				}
				self._hits=hits;
      			self.voteOk();
      		}
      		else
      		{
      			self.voteKo();
      		}
      	}
     } 
   };
 
   xhr_object.send(null); 

};

EditoPoll.prototype.voteOk = function()
{
	EditoPoll.cookie.addId(this._id);
	EditoPoll.cookie.write();
	this.writeResults();
};

EditoPoll.prototype.voteKo = function()
{
	alert('Une erreur est survenue, essayez plus tard !');
};

EditoPoll.prototype.writeResults = function()
{

	var buffer = document.createElement('div');
	var results = document.createElement('div');
	buffer.appendChild(results);
	results.setAttribute('id', this.getElementId('results'));
	results.setAttribute('class', 'poll-results');

	var table = document.createElement('table');
	results.appendChild(table);
	table.setAttribute('class', 'graph');	
	
	var tr = document.createElement('tr');
	table.appendChild(tr);
	
	var td = document.createElement('td');
	tr.appendChild(td);
	td.setAttribute('class', 'title');
	td.setAttribute('colspan', '3');
	td.innerHTML=this._question;
	
	
	var total_percent=0;
	for(var i in this._answers)
	{
		var tr = document.createElement('tr');
		table.appendChild(tr);
		tr.setAttribute('class', 'graph');
		
//	var results_html = "<div id='poll-results'><h3>Poll Results</h3>\n<dl class='graph'>\n";
	  var percent=0;
	  if(this._hits>0) percent=Math.round(this._answers[i]._hits/this._hits*100);
	  if((percent+total_percent)>100) percent=100-total_percent;
	  total_percent+=percent;
	  
	  var td = document.createElement('td');
	  tr.appendChild(td);
	  td.setAttribute('class', 'bar-title');
	  td.innerHTML=this._answers[i]._text;
	  
	  td = document.createElement('td');
	  tr.appendChild(td);
	  td.setAttribute('class', 'bar-container');
	  
	  var bar = document.createElement('div');
	  td.appendChild(bar);
	  bar.setAttribute('id', this.getElementId(this._answers[i]._id+'-bar'));
	  if(this._choice==this._answers[i]._id)
	  {
		  bar.setAttribute('class', 'poll-bar voted');
	  }
	  else
	  {
		  bar.setAttribute('class', 'poll-bar');
	  }
	  bar.setAttribute('style', 'width: 0%');
	  bar.innerHTML='&nbsp;';
	  
	  lib = document.createElement('td');
	  tr.appendChild(lib);
	  lib.setAttribute('class', 'poll-lib');
	  lib.innerHTML=percent+' %';
	  
	}

	tr = document.createElement('tr');
	table.appendChild(tr);
	
	td = document.createElement('td');
	tr.appendChild(td);
	td.setAttribute('class', 'total');
	td.setAttribute('colspan', '3');
	td.innerHTML='Nombre de votes : '+this._hits;
	if(!this._open) td.innerHTML+=' (ferm�)';
	
	var self=this;
	$('#'+this.getElementId('container')).fadeOut("slow", function() {
		$('#'+self.getElementId('container')).replaceWith(buffer.innerHTML).fadeIn("slow", function(){ self.animateResults(); });	
	});
	
};

EditoPoll.prototype.animateResults = function(){

	var total_percent=0;
	for(var i in this._answers)
	{
		var percent=0;
		if(this._hits>0) percent=Math.round(this._answers[i]._hits/this._hits*100);
		if((percent+total_percent)>100) percent=100-total_percent;
		total_percent+=percent;
		percent+='px';
		$('#'+this.getElementId(this._answers[i]._id+'-bar')).css({width: "0px"}).animate({width: percent}, 'slow');
	}
/*	$('#poll'+this._id+'-results .poll-bar').each(function(){
		var percentage = parseInt($(this).next().text())+'%';
	    $(this).css({width: "0%"}).animate({
					width: percentage}, 'slow');
	});*/
};

EditoPollAnswer = function(id, text, hits)
{
	this._id=id;
	this._text=text;
	this._hits=hits;
	//EditoPoll.polls.push(this);
};


/*
EditoPoll.initAll = function()
{
	for(var i in EditoPoll.polls)
	{
		EditoPoll.polls[i].init();
	}
}
*/

//mca.push(function(){EditoPoll.initAll();});
