/* Sophus3 logging request. www.sophus3.com
 * Custom page identification code for Ford Firefly environment
 * Copyright (c) Sophus Ltd Ltd 2001-2002. All rights reserved. Patent Pending.
 *
 * Change the value of tc_logging_active to switch off logging on the site.
 */

if (typeof tc_logging_active == 'undefined') 
	tc_logging_active = true;

/* Custom Code to attache campaignids to the page alias*/
var usePair = new Array();
usePair['campaignid'] = 1;
usePair['advertiserid'] = 1;
usePair['bannerid'] = 1;
usePair['pagevar'] = 1;

if (location.search != null && location.search.length > 1 && typeof tc_page_alias!="undefined") {
	var locSearch = location.search.substring(1);
	var s3Params = locSearch.split('&');
	var s3_params = '';
	var nextAppender = (tc_page_alias.indexOf('?') == -1) ? '?' : '&';

	for (var i=0; i<s3Params.length; i++) {
		if (s3Params[i].indexOf('=') != -1) {
			var pair = s3Params[i].split('=');
			if (usePair[pair[0]] == 1) {
				s3_params += nextAppender + s3Params[i];
				nextAppender = '&'; // Change from ? to &
			}
		}
	}
	tc_page_alias += s3_params;
}
/* End */


if (typeof tc_site_id=='undefined') tc_site_id = tc_make_ford_siteID();
// required configuration parameters
tc_server_url = "ford.touchclarity.com";
tc_log_path = "/cs/ENGInE/js/logging/sophus";
document.write("<scr"+"ipt language='JavaScript' type='text/javascript' src='"+tc_log_path+"/logging-code.js'></scr"+"ipt>");

// custom functions for Ford sites

function tc_make_ford_siteID() {
	var domain = document.location.hostname;
	enddomain = domain.substring(domain.lastIndexOf(".")+1);
 		if (domain.indexOf("talkbackclub.com") != -1) return 31;
	else if (enddomain.toLowerCase() == "at") tc_site_id = 122;
	else if (enddomain.toLowerCase() == "be") tc_site_id = 123;
	else if (enddomain.toLowerCase() == "ch") tc_site_id = 132;
	else if (enddomain.toLowerCase() == "cz") tc_site_id = 124;
	else if (enddomain.toLowerCase() == "de") tc_site_id = 32;
	else if (enddomain.toLowerCase() == "dk") tc_site_id = 125;
	else if (enddomain.toLowerCase() == "es") tc_site_id = 33;
	else if (enddomain.toLowerCase() == "fr") tc_site_id = 34;
	else if (enddomain.toLowerCase() == "hu") tc_site_id = 126;
	else if (enddomain.toLowerCase() == "ie") tc_site_id = 127;
	else if (enddomain.toLowerCase() == "it") tc_site_id = 35;
	else if (enddomain.toLowerCase() == "nl") tc_site_id = 128;
	else if (enddomain.toLowerCase() == "pl") tc_site_id = 129;
	else if (enddomain.toLowerCase() == "pt") tc_site_id = 130;
	else if (enddomain.toLowerCase() == "se") tc_site_id = 131;
	else if (enddomain.toLowerCase() == "uk") tc_site_id = 31;
	else if (enddomain.toLowerCase() == "no") tc_site_id = 272;
	else if (enddomain.toLowerCase() == "gr") tc_site_id = 271;
	else if (enddomain.toLowerCase() == "lu") tc_site_id = 277;
	else if (enddomain.toLowerCase() == "ru") tc_site_id = 278;
	else if (enddomain.toLowerCase() == "tr") tc_site_id = 279;
	else if (enddomain.toLowerCase() == "si") tc_site_id = 280;
	else if (enddomain.toLowerCase() == "fi") tc_site_id = 281;
	else if (enddomain.toLowerCase() == "ec") tc_site_id = 405;
	else if (enddomain.toLowerCase() == "co") tc_site_id = 404;
	else if (enddomain.toLowerCase() == "ve") tc_site_id = 403;
	else tc_site_id = 168;  // Ford Europe
	return tc_site_id;
}
