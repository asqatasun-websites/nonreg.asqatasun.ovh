var GB_CURRENT=null;
GB_hide=function(cb){
	GB_CURRENT.hide(cb);
	};
GreyBox=new overlay.Class({init:function(_2){
		this.use_fx=overlay.fx;
		this.type="page";
		this.overlay_click_close=false;
		this.callback_fns=[];
		this.reload_on_close=false;
		this.src_loader="/scripts2/greybox/iframe_lp.htm";
		var _3=window.location.hostname.indexOf("www");
		var _4=this.src_loader.indexOf("www");
		if(_3!=-1&&_4==-1){
			this.src_loader=this.src_loader.replace("://","://www.");
			}
		if(_3==-1&&_4!=-1){
			this.src_loader=this.src_loader.replace("://www.","://");
			}
		this.show_loading=true;
		overlay.update(this,_2);
		},addCallback:function(fn){
		if(fn){
			this.callback_fns.push(fn);
			}
		},show:function(_6){
		GB_CURRENT=this;
		this.url=_6;
		var _7=[overlay.$bytc("object"),overlay.$bytc("select")];
		overlay.map(overlay.flattenList(_7),function(_8){
			_8.style.visibility="hidden";
			});
		this.createElements();
		return false;
		},hide:function(cb){
		var me=this;
		overlay.callLater(function(){
			var _b=me.callback_fns;
			if(_b!=[]){
				overlay.map(_b,function(fn){
					fn();
					});
				}
			me.onHide();
			if(me.use_fx){
				var _d=me.overlay;
				overlay.fx.fadeOut(me.overlay,{onComplete:function(){
						overlay.removeElement(_d);
						_d=null;
						},duration:400});
				overlay.removeElement(me.g_window);
				}else{
				overlay.removeElement(me.g_window,me.overlay);
				}
			me.removeFrame();
			overlay.REV(window,"scroll",_GB_setOverlayDimension);
			overlay.REV(window,"resize",_GB_update);
			var _e=[overlay.$bytc("object"),overlay.$bytc("select")];
			overlay.map(overlay.flattenList(_e),function(_f){
				_f.style.visibility="visible";
				});
			GB_CURRENT=null;
			if(me.reload_on_close){
				window.location.reload();
				}
			if(overlay.isFunction(cb)){
				cb();
				}
			},10);
		},update:function(){
		this.setOverlayDimension();
		this.setFrameSize();
		this.setWindowPosition();
		},createElements:function(){
		this.initOverlay();
		this.g_window=overlay.DIV({"id":"GB_window"});
		overlay.hideElement(this.g_window);
		overlay.getBody().insertBefore(this.g_window,this.overlay.nextSibling);
		this.initFrame();
		this.initHook();
		this.update();
		var me=this;
		if(this.use_fx){
			overlay.fx.fadeIn(this.overlay,{duration:400,to:0.8,onComplete:function(){
					me.onShow();
					overlay.showElement(me.g_window);
					me.startLoading();
					}});
			}else{
			overlay.setOpacity(this.overlay,0.8);
			overlay.showElement(this.g_window);
			this.onShow();
			this.startLoading();
			}
		overlay.AEV(window,"scroll",_GB_setOverlayDimension);
		overlay.AEV(window,"resize",_GB_update);
		},removeFrame:function(){
		try{
			overlay.removeElement(this.iframe);
			}
		catch(e){
			}
		this.iframe=null;
		},startLoading:function(){
		this.iframe.src=this.src_loader;
		overlay.showElement(this.iframe);
		},setOverlayDimension:function(){
		var _11=overlay.getWindowSize();
		if(overlay.isMozilla()||overlay.isOpera()){
			overlay.setWidth(this.overlay,"100%");
			}else{
			overlay.setWidth(this.overlay,_11.w);
			}
		var _12=Math.max(overlay.getScrollTop()+_11.h,overlay.getScrollTop()+this.height);
		if(_12<overlay.getScrollTop()){
			overlay.setHeight(this.overlay,_12);
			}else{
			overlay.setHeight(this.overlay,overlay.getScrollTop()+_11.h);
			}
		},initOverlay:function(){
		this.overlay=overlay.DIV({"id":"GB_overlay"});
		if(this.overlay_click_close){
			overlay.AEV(this.overlay,"click",GB_hide);
			}
		overlay.setOpacity(this.overlay,0);
		overlay.getBody().insertBefore(this.overlay,overlay.getBody().firstChild);
		},initFrame:function(){
		if(!this.iframe){
			var d={"name":"GB_frame","class":"GB_frame","frameBorder":0,"allowTransparency":"true"};
			if(overlay.isIe()){
				d.src="javascript:false;document.write(\"\");";
				}
			this.iframe=overlay.IFRAME(d);
			this.middle_cnt=overlay.DIV({"class":"content"},this.iframe);
			this.top_cnt=overlay.DIV();
			this.bottom_cnt=overlay.DIV();
			overlay.ACN(this.g_window,this.middle_cnt,this.bottom_cnt);
			}
		},onHide:function(){
		},onShow:function(){
		},setFrameSize:function(){
		},setWindowPosition:function(){
		},initHook:function(){
		}});
_GB_update=function(){
	if(GB_CURRENT){
		GB_CURRENT.update();
		}
	};
_GB_setOverlayDimension=function(){
	if(GB_CURRENT){
		GB_CURRENT.setOverlayDimension();
		}
	};
script_loaded=true;
var GB_SETS={};
function decoGreyboxLinks(){
	var as=overlay.$bytc("a");
	overlay.map(as,function(a){
		if(a.getAttribute("href")&&a.getAttribute("rel")){
			var rel=a.getAttribute("rel");
			if(rel.indexOf("gb_")==0){
				var _17=rel.match(/\w+/)[0];
				var _18=rel.match(/\[(.*)\]/)[1];
				var _19=0;
				var _1a={"caption":a.title||"","url":a.href};
				if(_17=="gb_pageset"||_17=="gb_imageset"){
					if(!GB_SETS[_18]){
						GB_SETS[_18]=[];
						}
					GB_SETS[_18].push(_1a);
					_19=GB_SETS[_18].length;
					}
				if(_17=="gb_pageset"){
					a.onclick=function(){
						GB_showFullScreenSet(GB_SETS[_18],_19);
						return false;
						};
					}
				if(_17=="gb_imageset"){
					a.onclick=function(){
						GB_showImageSet(GB_SETS[_18],_19);
						return false;
						};
					}
				if(_17=="gb_image"){
					a.onclick=function(){
						GB_showImage(_1a.caption,_1a.url);
						return false;
						};
					}
				if(_17=="gb_page"){
					a.onclick=function(){
						var sp=_18.split(/, ?/);
						GB_show(_1a.caption,_1a.url,parseInt(sp[1]),parseInt(sp[0]));
						return false;
						};
					}
				if(_17=="gb_page_fs"){
					a.onclick=function(){
						GB_showFullScreen(_1a.caption,_1a.url);
						return false;
						};
					}
				if(_17=="gb_page_center"){
					a.onclick=function(){
						var sp=_18.split(/, ?/);
						GB_showCenter(_1a.caption,_1a.url,parseInt(sp[1]),parseInt(sp[0]));
						return false;
						};
					}
				}
			}
		});
	}
overlay.AEV(window,"load",decoGreyboxLinks);
GB_showImage=function(_1d,url,_1f){
	var _20={width:300,height:300,type:"image",fullscreen:false,center_win:true,caption:_1d,callback_fn:_1f};
	var win=new GB_Gallery(_20);
	return win.show(url);
	};
GB_showPage=function(_22,url,_24){
	var _25={type:"page",caption:_22,callback_fn:_24,fullscreen:true,center_win:false};
	var win=new GB_Gallery(_25);
	return win.show(url);
	};
GB_show=function(_45,url,_47,_48,_49){
	var _4a={caption:_45,height:_47||500,width:_48||500,fullscreen:false,callback_fn:_49};
	var win=new GB_Window(_4a);
	return win.show(url);
	};
GB_showCenter=function(_4c,url,_4e,_4f,_50){
	var _51={caption:_4c,center_win:true,height:_4e||500,width:_4f||500,fullscreen:false,callback_fn:_50};
	var win=new GB_Window(_51);
	return win.show(url);
	};
GB_showFullScreen=function(_53,url,_55){
	var _56={caption:_53,fullscreen:true,callback_fn:_55};
	var win=new GB_Window(_56);
	return win.show(url);
	};
GB_Window=GreyBox.extend({init:function(_58){
		this.parent({});
		this.show_close_img=true;
		overlay.update(this,_58);
		this.addCallback(this.callback_fn);
		},initHook:function(){
		overlay.addClass(this.g_window,"GB_Window");
		overlay.ACN(this.top_cnt);
		if(this.fullscreen){
			overlay.AEV(window,"scroll",overlay.$b(this.setWindowPosition,this));
			}
		},setFrameSize:function(){
		if(this.fullscreen){
			var _5e=overlay.getWindowSize();
			overlay_h=_5e.h;
			this.width=Math.round(this.overlay.offsetWidth-(this.overlay.offsetWidth/100)*10);
			this.height=Math.round(overlay_h-(overlay_h/100)*10);
			}
		overlay.setWidth(this.iframe,this.width);
		overlay.setHeight(this.iframe,this.height);
		},setWindowPosition:function(){
		var _5f=overlay.getWindowSize();
		overlay.setLeft(this.g_window,((_5f.w-this.width)/2)-13);
		if(!this.center_win){
			overlay.setTop(this.g_window,overlay.getScrollTop());
			}else{
			var fl=((_5f.h-this.height)/2)-20+overlay.getScrollTop();
			if(fl<0){
				fl=0;
				}
			overlay.setTop(this.g_window,fl);
			}
		}});


script_loaded=true;