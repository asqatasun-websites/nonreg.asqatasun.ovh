var i18n = {
  _: function(n1, n2) {
    if(this[n1] && this[n1][n2]) {return this[n1][n2];}
  },
  
  "changeSize": {
    "title": "Taille du texte",
    "small": "Petite taille",
    "middle": "Taille moyenne",
    "large": "Grande taille"
  }
};