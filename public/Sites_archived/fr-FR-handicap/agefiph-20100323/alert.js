﻿var validerAlert = new Class({
	initialize: function(id, formId, submitButton, alertXML, alertStyle, alertType, onSubmitForm){
		this.id = id;
		this.form = $(formId);
		this.submitButton = $(submitButton);
		this.alertXML = alertXML;
		this.alertStyle = alertStyle;
		this.alertType = alertType || "layer";
		this.onSubmitForm = onSubmitForm;
		this.waitInterval = null;
		if (this.form && this.submitButton) {
			this.build();
			this.initForm();
		}
	},

	build: function() {
		var thisObject = this;
		var bgDiv = new Element("div").setProperties({id: thisObject.id}).setStyles({
			font: thisObject.alertStyle && thisObject.alertStyle.font ? thisObject.alertStyle.font : "10px/10px Arial",
			color: thisObject.alertStyle && thisObject.alertStyle.txtColor ? thisObject.alertStyle.txtColor : "#000",
			background: thisObject.alertStyle && thisObject.alertStyle.background ? thisObject.alertStyle.background : "#fff",
			border: thisObject.alertStyle && thisObject.alertStyle.border ? thisObject.alertStyle.border : "1px solid #000000",
			width: thisObject.alertStyle && thisObject.alertStyle.width ? thisObject.alertStyle.width : "120px",
			height: "auto",
			position: "absolute",
			top: "0px",
			left: "0px",
			padding: "3px",
			visibility: "hidden",
			zIndex: "101"
		}).injectInside($$("body")[0]);
		var closeImg = new Element("img").setProperties({
			id: thisObject.id+"Close", 
			src: thisObject.alertStyle && thisObject.alertStyle.closeImage ? thisObject.alertStyle.closeImage : "../images/_alert_close.gif"
		}).setStyles({
			styleFloat: "right",
			cssFloat: "right",
			cursor: "pointer"
		}).injectInside(bgDiv);
		closeImg.addEvent("click", function(){
			thisObject.hide();
		});
		var contDiv = new Element("p").setProperties({
			id: thisObject.id+"Cont"
		}).setStyles({
			padding: "5px 3px 3px 3px", 
			margin: "0",
			clear: "both"
		}).injectInside(bgDiv);
		if (window.ie) {
			var iframeDiv = new Element("iframe").setProperties({
				id: thisObject.id+"Iframe"
				}).setStyles({
				width: thisObject.alertStyle && thisObject.alertStyle.width ? thisObject.alertStyle.width : "120px",
				filter: "progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)",
				position: "absolute",
				top: "0px",
				left: "0px",
				visibility: "hidden",
				zIndex: "100"
			}).injectInside($$("body")[0]);
		}
		//
		//
		var bgP = new Element("div").setProperties({id: thisObject.id+"P"}).setStyles({
			font: thisObject.alertStyle && thisObject.alertStyle.font ? thisObject.alertStyle.font : "10px/10px Arial",
			color: thisObject.alertStyle && thisObject.alertStyle.appTxtColor ? thisObject.alertStyle.appTxtColor : "#f00",
			width: "auto",
			height: "auto",
			textAlign: "left",
			position: "absolute",
			top: "0px",
			left: "0px",
			padding: "0px",
			visibility: "hidden",
			zIndex: "101"
		}).injectInside($$("body")[0]);
		//
		//
		if (thisObject.alertType == "highlight") {
			var alertPlace = new Element("p").setProperties({id: thisObject.id+"H"}).setStyles({
				font: thisObject.alertStyle && thisObject.alertStyle.font ? thisObject.alertStyle.font : "10px/10px Arial",
				color: thisObject.alertStyle && thisObject.alertStyle.appTxtColor ? thisObject.alertStyle.appTxtColor : "#f00",
				width: "auto",
				height: "auto",
				textAlign: "left",
				padding: "0px",
				display: "none"
			}).setText("Sample").injectBefore(thisObject.form.getFirst());
		}
	},
	
	initForm: function() {
		for (var i = 0; i < this.alertXML.length; i++) {
			var inputCtrl = $(this.alertXML[i].name[0].data);
			if (inputCtrl) {				
				for (var j = 0; j < this.form.length; j++) {
					if (this.form[j].name == inputCtrl.name) {
						if (!this.form[j].valid) {
							this.form[j].valid = new Array();
						}
						this.form[j].valid.push({
							condition: this.alertXML[i].type[0].data,
							alert: this.alertXML[i].alert[0].data
						});
						//
						if (typeof(this.alertXML[i].restrict) != "undefined") {
							this.form[j].restrict = this.alertXML[i].restrict[0].data.replace("{number}","0-9").replace("number","[0-9]");
							this.form[j].addEvent("keypress", function(event){
								var e = new Event(event);
								var code = e.code;
								var co = ( code == null || code == 0 || code == 8 || code == 9 || code == 13 || code == 27 || code == 35|| code == 36 || code == 37 || code == 39 || code == 45|| code == 46|| code == 47);
								var key = e.key;
								var re = new RegExp(this.restrict);
								if (!key.test(re) && !co) {
									e.stop();
								}
							});
						}
						//
						if (typeof(this.alertXML[i].popup) != "undefined" && this.alertXML[i].popup[0].data == "true") {
							this.form[j].popup = true;
						}
						//
						if (typeof(this.alertXML[i].init) != "undefined") {
							this.form[j].init = this.alertXML[i].init[0].data.trim();
							this.form[j].value = this.form[j].init;
							this.form[j].addEvent("focus", function(event){
								if (this.value.trim() == this.init){
									this.value = "";
								}
							});
							this.form[j].addEvent("blur", function(event){
								if (this.value.trim() == ""){
									this.value = this.init;
								}
							});
						}
					} // end if inputCtrl.name
				} // end for
			} // end if inputCtrl			
		} // end for
		//
		var thisObject = this;
		thisObject.submitButton.addEvent("click", function(event) {
			new Event(event).stop();
			if (thisObject.valid()) {
				if (thisObject.onSubmitForm) {
					thisObject.onSubmitForm();
				} else {
					thisObject.form.submit();
				}
			}
		});
	},
	
	show: function(elementId, warning) {
		var thisObject = this;
		if (thisObject.alertType == "layer" || $(elementId).popup) {
			if (thisObject.alertType == "layer") {
				$(thisObject.id).setStyles({
					top: $(elementId).getPosition().y+$(elementId).getCoordinates().height+"px",
					left: $(elementId).getPosition().x+"px"
				});
			} else {
				var layerTop = $(elementId).getPosition().y-30;
				if ($(elementId).type == "hidden") layerTop = Math.floor(thisObject.form.getPosition().y+(thisObject.form.getCoordinates().height-$(thisObject.id).getCoordinates().height)/2);
				$(thisObject.id).setStyles({
					top: layerTop+"px",
					left: (thisObject.form.getPosition().x+(thisObject.form.getCoordinates().width-$(thisObject.id).getCoordinates().width)/2)+"px"
				});
			}
			$(thisObject.id+"Cont").setHTML(warning);
			if (window.ie) {
				$(thisObject.id+"Iframe").setStyles({
					visibility: "visible",
					top: $(elementId).getPosition().y+$(elementId).getCoordinates().height+"px",
					left: $(elementId).getPosition().x+"px",
					width: $(thisObject.id).getCoordinates().width,
					height: $(thisObject.id).getCoordinates().height
				})
			}
			//
			new Fx.Style($(thisObject.id), "opacity", {
				duration: 500,
				onComplete: function(){
					clearInterval(thisObject.waitInterval);
					thisObject.waitInterval = setInterval(function() {
						clearInterval(thisObject.waitInterval);
						thisObject.hide();
					}, 5000);
				}
			}).start(0, 1);
		} else if (thisObject.alertType == "append") {
			if (document.append) document.append.remove();
			document.append = new Element("div").setStyles({
				display: "block",
				width: "auto"
			}).injectAfter(elementId);
			$(thisObject.id+"P").setStyles({
				top: $(elementId).getPosition().y+$(elementId).getCoordinates().height+"px",
				left: $(elementId).getPosition().x+"px"
			}).setOpacity(1).setHTML(warning);
			document.append.setStyles({
				height: $(thisObject.id+"P").getCoordinates().height+"px"
			});
		} else if (thisObject.alertType == "highlight") {
			$(thisObject.id+"H").setStyles({
				display: "block"
			}).setHTML(warning);
			var label = $(elementId).getPrevious();
			if (label) {
				document.highlightLabel = label;
				label.setStyles({
					color: thisObject.alertStyle && thisObject.alertStyle.txtColor ? thisObject.alertStyle.txtColor : "#f00"
				});
			}
		} else {
			if ($(thisObject.alertType)) {
				$(thisObject.alertType).setHTML(warning).setStyle("visibility", "visible");
			} else {
				alert(warning);
			}
		}
		//
		if($(elementId).type == 'radio'){
			var similars = $(this.form).getElements('input[name=' + elementId.name + ']');			
			if(similars.length > 1){
				similars.each(function(item){
					$(item).addEvents({
						"keyup": function(){
							clearInterval(thisObject.waitInterval);
							thisObject.hide();
						},
						"click": function(){
							clearInterval(thisObject.waitInterval);
							thisObject.hide();
						}
					});
					$(item).focus();
				});
			}
		}
		$(elementId).addEvents({
			"keyup": function(){
				clearInterval(thisObject.waitInterval);
				thisObject.hide();
			},
			"click": function(){
				clearInterval(thisObject.waitInterval);
				thisObject.hide();
			}
		});
		$(elementId).focus();
	},
	
	hide: function() {
		var thisObject = this;
		clearInterval(this.waitInterval);
		if ($(this.id)) $(this.id).setStyles({visibility: "hidden"});
		if ($(this.id+"P")) $(this.id+"P").setStyles({visibility: "hidden"});
		if ($(this.id+"H")) $(this.id+"H").setStyles({display: "none"});
		if (window.ie) {
			$(this.id+"Iframe").setStyles({visibility: "hidden"});
		}
		//
		if (document.append) {
			document.append.remove();
			document.append = null;
		}
		//
		if ($(thisObject.alertType)) {
			$(thisObject.alertType).setHTML("").setStyle("visibility", "hidden");
		}
		//
		if (document.highlightLabel && $(document.highlightLabel)) {
			$(document.highlightLabel).setStyle("color", "");
		}
	},
	
	valid: function() {
		var validArray = new Array();
		validArray.splice(0, validArray.length);
		for (var i = 0; i < this.alertXML.length; i++) {
			var inputCtrl = $(this.alertXML[i].name[0].data);
			if (inputCtrl) {			
				if (inputCtrl.name.indexOf("[]") != -1) {				
					for (var j = 0; j < this.form.length; j++) {
						if (this.form[j].name == inputCtrl.name) {
							validArray.push({
								control: this.form[j], 
								type: this.alertXML[i].type[0].data, 
								alert: this.alertXML[i].alert[0].data,
								init: this.alertXML[i].init
							});
						}
					}
				} else {
					validArray.push({
						control: inputCtrl, 
						type: this.alertXML[i].type[0].data, 
						alert: this.alertXML[i].alert[0].data,
						init: this.alertXML[i].init
					});
				}
			}
		}
		//		
		for (i = 0; i < validArray.length; i++) {
			if (!this.check(validArray[i].control, validArray[i].type, validArray[i].init)) {			
				this.show(validArray[i].control, validArray[i].alert);
				return false;
			}
		}
		return true;
		//
	},
	
	check: function(elementId, condition, extraInfo) {
		var formElement = $(elementId);
		if (condition.indexOf("EVAL") !== -1) {
			var evalCon = getQuote(condition, "[", "]");
			return eval(evalCon);
		}
		if (typeof(formElement.init) != "undefined" && typeof(extraInfo) != "undefined") {
			return (formElement.value != formElement.init);
		}
		if (condition.indexOf("required") !== -1) {
			var minLength = Math.max(Number(getQuote(condition, "(", ")")), 1);
			return (formElement.value.trim().length >= minLength);
		}
		if (condition.indexOf("min") !== -1) {
			var minNumber = Number(getQuote(condition, "(", ")"));
			return (formElement.value >= minNumber);
		}
		if (condition.indexOf(">") !== -1) {
			if (condition.indexOf(">=") !== -1) {
				var otherCtrl = $(condition.replace(">=",""));
				return (formElement.value >= otherCtrl.value);
			} else {
				var otherCtrl = $(condition.replace(">",""));
				return (formElement.value > otherCtrl.value);
			}
		}
		if (condition.indexOf("<") !== -1) {
			if (condition.indexOf("<=") !== -1) {
				var otherCtrl = $(condition.replace("<=",""));
				return (formElement.value <= otherCtrl.value);
			} else {
				var otherCtrl = $(condition.replace(">",""));
				return (formElement.value < otherCtrl.value);
			}
		}
		if (condition.indexOf("=") !== -1 && condition.indexOf(">") == -1 && condition.indexOf("<") == -1) {
			var otherCtrl = $(condition.replace("=",""));
			return (formElement.value == otherCtrl.value);
		}
		if (condition.indexOf("max") !== -1) {
			var minNumber = Number(getQuote(condition, "(", ")"));
			return (formElement.value <= minNumber);
		}
		if (condition === "select") {
			return (formElement.selectedIndex !== 0);
		}
		if (condition === "check") {
			return (formElement.checked);
		}
		
		if (condition === "email") {		
			var isEmailValid = true;
			var aEmailArray = formElement.value.split(";");
			for (var emailCount = 0; emailCount<aEmailArray.length; emailCount++) {
				if (!isEmail(aEmailArray[emailCount].trim())) {
					isEmailValid = false;
					break;
				}
			}
			return isEmailValid;
		}
		if (condition === "phone") {
			return isPhone(formElement.value);
		}
		if (condition === "none") {
			return true;
		}
		if (condition.indexOf("date") !== -1) {
			var f = getQuote(condition, "(", ")");
			return isDate(f, formElement.value);
		}
		if (condition.indexOf("future") !== -1) {
			var c = getQuote(condition, "(", ")").toString();
			var p = c.split(",");
			if (p.length == 0) {
				return false;
			}
			var f = p[0];
			if (p[2] && p[2] == "false" && isBlank(formElement.value)) {
				return true;
			}
			if (!isDate(f, formElement.value) || (p[1] && $(p[1]) && !isDate(f, $(p[1]).value))) {
				return false;
			}
			if (p[1] && $(p[1])) {
				return (compareDate(f, formElement.value, $(p[1]).value) == -1);
			} else {
				return (compareDate(f, formElement.value) == -1);
			}
		}
		//
		//
		//
		//
		function getQuote(str, start, end) {
			return str.substring(str.indexOf(start)+1, str.indexOf(end));
		}
		//
		function isEmail(s){
			var re = new RegExp("^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]{2,4}$");
			return (s.search(re) != -1);
		}
		//
		function isPhone(s) {
			var re = new RegExp("^[ .0-9]{3,}$");
			return (s.search(re) != -1);
		}
		//
		function isDate(f, s) {
			var r = new RegExp("[.\/-]");
			var p = s.split(r);
			if (p.length != 3) {
				return false;
			}
			var y = Number(p[f.indexOf("y")]);
			var m = Number(p[f.indexOf("m")]);
			var d = Number(p[f.indexOf("d")]);
			var n = new Date(y, m-1, d);
			return (y == n.getFullYear() && m == n.getMonth()+1 && d == n.getDate());
		}
		//
		function compareDate(format, date1, date2){
			var s = new RegExp("[.\/-]");
			var d1 = date1.split(s);
			var y1 = Number(d1[format.indexOf("y")]);
			var m1 = Number(d1[format.indexOf("m")]);
			var d1 = Number(d1[format.indexOf("d")]);
			if (date2) {
				var d2 = date2.split(s);
				var y2 = Number(d2[format.indexOf("y")]);
				var m2 = Number(d2[format.indexOf("m")]);
				var d2 = Number(d2[format.indexOf("d")]);
			} else {
				var d2 = new Date();
				var y2 = d2.getFullYear();
				var m2 = d2.getMonth()+1;
				var d2 = d2.getDate();
			}
			//
			if (y2 > y1) {
				return 1;
			} else if (y2 < y1) {
				return -1;
			} else {
				if (m2 > m1) {
					return 1;
				} else if (m2 < m1) {
					return -1;
				} else {
					if (d2 > d1) {
						return 1;
					} else if (d2 < d1) {
						return -1;
					} else {
						return 0;
					}
				}
			}
		}
		//
		function isBlank(s) {
			return (s.trim() == "");
		}
		//
	}
});
//