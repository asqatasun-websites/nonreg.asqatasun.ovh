function noteObjet(note){
	if(AccepteCookies()){
		if(LireCookie("a_note_" + global_element + "_" + global_element_id)!="ok"){
			var url = contextPath+"/include/note_objet.jspz";
			var pars = "";
			pars += "&element="+global_element;
			pars += "&id="+global_element_id;
			pars += "&note="+note;
			var myAjax = new Ajax.Request(url, {method: "post", parameters: pars, onComplete: traiteReponseNoteObjet, onFailure: afficheErreurNoteObjet, asynchronous:true});
		}
		else{
			alert("Vous avez d�j� donn� une note.");
			return false;
		}
	}
	else{
		alert("Pour pouvoir noter, votre navigateur doit accepter les cookies.");
	}
}

function traiteReponseNoteObjet(res){
	var str = res.responseText;
	if(str.lastIndexOf("|") != -1){
		var tab = new Array();
		tab = str.split("|");
		var moyenne = parseFloat(tab[0]);
		var nbvotes = parseInt(tab[1]);
		afficheNouvelleMoyenne(moyenne, nbvotes);
		var date=new Date();
		date.setFullYear(date.getFullYear()+1);
		EcrireCookie("a_note_" + global_element + "_" + global_element_id, "ok", date);
		alert("Merci d'avoir vot� !");
	}
	else{
		afficheErreurNoteObjet(str);
	}
}

function afficheNouvelleMoyenne(moyenne, nbvotes){
	if(moyenne==0){
		nomImage = "0.gif";
	}
	if(moyenne > 0 &&  moyenne <= 0.7){
		nomImage = "0-5.gif";
	}
	if(moyenne > 0.7 &&  moyenne <= 1.25){
		nomImage = "1.gif";
	}
	if(moyenne > 1.25 &&  moyenne <= 1.75){
		nomImage = "1-5.gif";
	}
	if(moyenne > 1.75 &&  moyenne <= 2.25){
		nomImage = "2.gif";
	}
	if(moyenne > 2.25 &&  moyenne <= 2.75){
		nomImage = "2-5.gif";
	}
	if(moyenne > 2.75 &&  moyenne <= 3.25){
		nomImage = "3.gif";
	}
	if(moyenne > 3.25 &&  moyenne <= 3.75){
		nomImage = "3-5.gif";
	}
	if(moyenne > 3.75 &&  moyenne <= 4.25){
		nomImage = "4.gif";
	}
	if(moyenne > 4.25 &&  moyenne <= 4.75){
		nomImage = "4-5.gif";
	}
	if(moyenne > 4.75){
		nomImage = "5.gif";
	}
	var str_moyenne = "";
	if(nbvotes==0) str_moyenne = '0 vote';
	else if(nbvotes==1) str_moyenne = '1 vote';
	else str_moyenne = nbvotes + ' votes';
	$('noteMoyenne').innerHTML = '<img src="' + contextPath+StarPathg + nomImage + '" /> ' + str_moyenne + '';
}

function afficheErreurNoteObjet(chaine){
	alert("Une erreur est survenue : " + chaine);
	return false;
}