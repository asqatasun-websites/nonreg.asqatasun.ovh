
if (typeof audi_ngw == 'undefined') {
	audi_ngw = {};
}
audi_ngw.configurator = {};

audi_ngw.configurator.accxParseParams = function (params) {
	paramArray = params.split(",")
	paramsHash = new Array();
	for(p in paramArray) {
		if (!isNaN(p)) {
	  	keyValue = paramArray[p].split("=");
	  	paramsHash[keyValue[0]] = keyValue[1];
	  }
	}
	return paramsHash;
};
// open the Configurator with params
audi_ngw.configurator.openACCx = function (paramsAsString) {
if(paramsAsString.indexOf("http") == 0) {
	params = new Array();
	params["url"] = paramsAsString;
} else {
	params = audi_ngw.configurator.accxParseParams(paramsAsString);
}
var accxURL = "http://ak4-fr.audi.de/entry?"
var mandant=""

s="scrollbars=no,directories=no,menubar=no,toolbar=no,width=1014,height=700,status=yes,resizable=no";

var vc="";
var pr=""
var next="next=carline-page"

if ('undefined' != typeof params['url']) {
	accxURL = params['url'];
} else {
	if ('undefined' != typeof params['carline']) {
	  vc="vc=" + params['carline'];
	  if ('undefined' == typeof params['target']) {
	  	next = "next=model-page"
	  }
	}
	if ('undefined' != typeof params['model']) {
	  pr="pr=" + params['model'];
	  if ('undefined' == typeof params['target']) {
	  	next = "next=exterior-page"
	  }
	}
	if ('undefined' != typeof params['target']) {
	  next="next=" + params['target'];
	}
	if ('undefined' != typeof params['exteriorcolour']) {
	  pr +="|" + params['exteriorcolour'];
	}
	if ('undefined' != typeof params['interiorcolour']) {
	  pr +="|" + params['interiorcolour'];
	}
	if ('undefined' != typeof params['rims']) {
	  pr +="|" + params['rims'];
	}
	accxURL += mandant + "&" + vc + "&" + pr + "&" + next
}
sat=window.open(accxURL,"AK4SATELLIT",s);
sat.focus();
};

audi_ngw.configurator.open_with_carline = function(carline) {
	audi_ngw.configurator.openACCx('carline=' + carline);
};
audi_ngw.configurator.open_with_model = function(model, extcolor, rim) {
	p = "model=" + model;
	if('undefined' != typeof extcolor) {
		p += ",exteriorcolour=" + extcolor
	}
	if('undefined' != typeof rim) {
		p += ",rim=" + rim
	}
	audi_ngw.configurator.openACCx(p);
};

audi_ngw.configurator.accxParseParams = function (params) {
	paramArray = params.split(",")
	paramsHash = new Array();
	for(p in paramArray) {
		if (!isNaN(p)) {
			keyValue = paramArray[p].split("=");
			paramsHash[keyValue[0]] = keyValue[1];
		}
	}
	return paramsHash;
};
