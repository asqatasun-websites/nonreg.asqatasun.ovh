/* Permet d'ajouter ou de supprimer un �v�nement par objet dans la page */
//~ http://www.quirksmode.org/blog/archives/2005/10/_and_the_winner_1.html
function addEvent( obj, type, fn )
{
	if (obj.addEventListener)
		obj.addEventListener( type, fn, false );
	else if (obj.attachEvent)
	{
		obj["e"+type+fn] = fn;
		obj[type+fn] = function() { obj["e"+type+fn]( window.event ); }
		obj.attachEvent( "on"+type, obj[type+fn] );
	}
}
/* */

function changeDisplay(obj) {
  if(document.getElementById(obj).style.display != 'block' ) {
    document.getElementById(obj).style.display = 'block';
  } else {
    document.getElementById(obj).style.display = 'none';
  }
}

function changeDisplay2(obj) {
    return function() {
        changeDisplay(obj);
    }
}

/* Infobulle � l'activation */
var idClicAnchor    = 'wf-a';
var idClicInfobulle = 'wf-infobulle';


/* Infobulle au survol */
var idSurvolAnchor    = 'wf2-a';
var idSurvolInfobulle = 'wf2-infobulle';


function launch() {
  /* Infobulle au clic */
  if(document.getElementById(idClicInfobulle)) {
      var infobulle = document.getElementById(idClicInfobulle);
      
      // Hide Infobulle if Javascript activated
      infobulle.style.display = 'none';
      
      // Add Closing Infobulle Tag in Infobulle
      infobulle.getElementsByTagName('div')[infobulle.getElementsByTagName('div').length-1].innerHTML += '<div id="wf-closer"><a href="javascript:changeDisplay(idClicInfobulle)">Fermer</a></div>';
      
      // Add Opening Infobulle Event
      if(document.getElementById(idClicAnchor)) {
          document.getElementById(idClicAnchor).setAttribute('href','javascript:changeDisplay(idClicInfobulle);');
      }
  }
  
  
  /* Infobulle au survol */
  if(document.getElementById(idSurvolInfobulle)) {
      var infobulle = document.getElementById(idSurvolInfobulle);
      
      // Hide Infobulle if Javascript activated
      infobulle.style.display = 'none';
      
      // Add Opening Infobulle Event
      if(document.getElementById(idSurvolAnchor)) {
          document.getElementById(idSurvolAnchor).setAttribute('href','#');
          addEvent(document.getElementById(idSurvolAnchor),'mouseover',changeDisplay2(idSurvolInfobulle));
          addEvent(document.getElementById(idSurvolAnchor),'focus',changeDisplay2(idSurvolInfobulle));
          addEvent(document.getElementById(idSurvolAnchor),'mouseout',changeDisplay2(idSurvolInfobulle));
          addEvent(document.getElementById(idSurvolAnchor),'blur',changeDisplay2(idSurvolInfobulle));
      }
  }
}

addEvent(window,'load',launch);