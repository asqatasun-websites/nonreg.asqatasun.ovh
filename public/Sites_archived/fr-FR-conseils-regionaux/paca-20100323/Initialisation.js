// ***********************************************************************************************
// Initalisation lors du chargement complet de la page
// ***********************************************************************************************

function ini(){
	iniAgrandirReduirePolice();
	
	if(document.all){
		versionNavigateur = navigator.appVersion;
		siIE6 = versionNavigateur.indexOf("MSIE 6");
		if(siIE6 > -1){
			iniIE6();
		}
	}

	PlayerActivationLien();	

}

function iniIE6(){
	reSize();
	window.setInterval("reSize()", 300);
}

function reSize(){

	var largeur = document.getElementById("navPrincipale").offsetWidth;
	var hauteur = document.getElementById("navPrincipale").offsetHeight;
	var pointeurElement = document.getElementById("menuImageFond");
	pointeurElement.style.width = largeur + "px";
	pointeurElement.style.height = hauteur - 2 + "px";

}

function PlayerActivationLien(){
	
	if(document.getElementById("espacePlayer")){
		for(i=1; i<=4; i++){
			nomId = "sousTitreActus" + i;
			document.getElementById(nomId).onmouseover = function(){
				pointeurElement = this;
				pointeurElement.className = pointeurElement.className + " onmouseover";
				
				pointeurElement = this.getElementsByTagName("a")[0];
				pointeurElement.className = pointeurElement.className + " onmouseover";
			}
			document.getElementById(nomId).onmouseout= function(){
				pointeurElement = this;
				listeDesClasses = pointeurElement.className;
				pointeurElement.className = listeDesClasses.replace(/ onmouseover/, "");
				
				pointeurElement = this.getElementsByTagName("a")[0];
				listeDesClasses = pointeurElement.className;
				pointeurElement.className = listeDesClasses.replace(/ onmouseover/, "");
			}
		}
	}
	
}


// ***********************************************************************************************
// Initalisation des champs Input

function clearInput(pointeurElement){
	
	if(!pointeurElement.clear){
		pointeurElement.clear = true;
		pointeurElement.value = "";
	}
	
}

function clearInputPassword(pointeurElement){
	clearInput(pointeurElement);
	pointeurElement.setAttribute("type","password");
}

function selectAllInput(pointeurElement){
	pointeurElement.select();
}


$(document).ready(function(){
	var first = 0;
	
	if($("div.rubrique").size() > 0) {
		$("div.rubrique").hide();
	}
	
	/*  Actualite */
	if($("div#rubriqueActualites").size() > 0) {
		$("div#rubriqueActualites").hide();
		$("div#navRubriques li#rubriqueActualites").show();
		if(first == 0) {
			first = 1;
			$("div#navRubriques li#rubriqueActualites").toggleClass("actif");
			$("div#rubriqueActualites").show();
		}
	} else {
		$("div#navRubriques li#rubriqueActualites").hide();
	}

	/*  Initialisation TVRegion */
	if($("div#rubriqueTVRegion").size() > 0) {
		$("div#rubriqueTVRegion").hide();
		$("li#rubriqueTVRegion").show();
		if(first == 0) {
			first = 1;
			$("div#navRubriques li#rubriqueTVRegion").toggleClass("actif");
			$("div#rubriqueTVRegion").show();
		}
	} else {
		$("div#navRubriques li#rubriqueTVRegion").hide();
	}

	/* Documents */
	if($("div#rubriqueDocuments").size() > 0) {
		$("div#rubriqueDocuments").hide();
		$("div#navRubriques li#rubriqueDocuments").show();
		if(first == 0) {
			first = 1;
			$("div#navRubriques li#rubriqueDocuments").toggleClass("actif");
			$("div#rubriqueDocuments").show();
		}
	} else {
		$("div#navRubriques li#rubriqueDocuments").hide();
	}
	
	/* rubriqueGuideDesAides */
	if($("div#rubriqueGuideDesAides").size() > 0) {
		$("div#rubriqueGuideDesAides").hide();
		$("li#rubriqueGuideDesAides").show();
		if(first == 0) {
			first = 1;
			$("div#navRubriques li#rubriqueGuideDesAides").toggleClass("actif");
			$("div#rubriqueGuideDesAides").show();
		}
	} else {
		$("div#navRubriques li#rubriqueGuideDesAides").hide();
	}
	
	/* rubriqueJournaux */
	if($("div#rubriqueJournaux").size() > 0) {
		$("div#rubriqueJournaux").hide();
		$("li#rubriqueJournaux").show();
		if(first == 0) {
			first = 1;
			$("div#navRubriques li#rubriqueJournaux").toggleClass("actif");
			$("div#rubriqueJournaux").show();
		}
	} else {
		$("div#navRubriques li#rubriqueJournaux").hide();
	}
	
	/* rubriqueSitesDedies */
	if($("div#rubriqueSitesDedies").size() > 0) {
		$("div#rubriqueSitesDedies").hide();
		$("li#rubriqueSitesDedies").show();
		if(first == 0) {
			first = 1;
			$("div#navRubriques li#rubriqueSitesDedies").toggleClass("actif");
			$("div#rubriqueSitesDedies").show();
		}
	} else {
		$("div#navRubriques li#rubriqueSitesDedies").hide();
	}
	
	/* rubriqueSitesWeb */
	if($("div#rubriqueSitesWeb").size() > 0) {
		$("div#rubriqueSitesWeb").hide();
		$("div#navRubriques li#rubriqueSitesWeb").show();
		if(first == 0) {
			first = 1;
			$("div#navRubriques li#rubriqueSitesWeb").toggleClass("actif");
			$("div#rubriqueSitesWeb").show();
		}
	} else {
		$("div#navRubriques li#rubriqueSitesWeb").hide();
	}
	
	/* rubriqueVosQuestions */
	if($("div#rubriqueVosQuestions").size() > 0) {
		$("div#rubriqueVosQuestions").hide();
		$("li#rubriqueVosQuestions").show();
		if(first == 0) {
			first = 1;
			$("div#navRubriques li#rubriqueVosQuestions").toggleClass("actif");
			$("div#rubriqueVosQuestions").show();
		}
	} else {
		$("div#navRubriques li#rubriqueVosQuestions").hide();
	}
	
	/* rubriqueDossierduMois */
	if($("div#rubriqueDossierduMois").size() > 0) {
		$("div#rubriqueDossierduMois").hide();
		$("li#rubriqueDossierduMois").show();
		if(first == 0) {
			first = 1;
			$("div#navRubriques li#rubriqueDossierduMois").toggleClass("actif");
			$("div#rubriqueDossierduMois").show();
		}
	} else {
		$("div#navRubriques li#rubriqueDossierDuMois").hide();
	}
	
		/* rubriqueServiceEnLigne */
	if($("div#rubriqueServiceEnLigne").size() > 0) {
	
		$("div#rubriqueServiceEnLigne").hide();
		$("li#rubriqueServiceEnLigne").show();
		if(first == 0) {
			first = 1;
			$("div#navRubriques li#rubriqueServiceEnLigne").toggleClass("actif");
			$("div#rubriqueServiceEnLigne").show();
		}
	} else {
		$("div#navRubriques li#rubriqueServiceEnLigne").hide();
	}
			/* rubriqueAgenda */
	if($("div#rubriqueAgenda").size() > 0) {
	
		$("div#rubriqueAgenda").hide();
		$("li#rubriqueAgenda").show();
		if(first == 0) {
			first = 1;
			$("div#navRubriques li#rubriqueAgenda").toggleClass("actif");
			$("div#rubriqueAgenda").show();
		}
	} else {
		$("div#navRubriques li#rubriqueAgenda").hide();
	}
	
});

