// Affichage du menu
function menu_open()
{
	var btn = this.getElementsByTagName("a")[0];
	var ul = this.getElementsByTagName("ul");
	
	if (ul.length > 0)
	{
		var lvl = this.parentNode.style.zIndex;
		if (menu_last[lvl] != undefined) document.getElementById(menu_last[lvl]).style.display = "none";
		menu_last[lvl] = ul[0].id;
		clearTimeout(menu_timeout[ul[0].id]);
		ul[0].style.display = 'block';
		btn.className = "over parent";
	}
	else
	{
		btn.className = "over";
	}
}

// Fermeture du menu
function menu_close()
{
	var btn = this.getElementsByTagName("a")[0];
	var ul = this.getElementsByTagName("ul");
	
	if (ul.length > 0)
	{
		menu_timeout[ul[0].id] = setTimeout("document.getElementById('"+ul[0].id+"').style.display='none'", 500);
		btn.className = "out parent";
	}
	else
	{
		btn.className = "out";
	}
}

// Initialisation du menu
function menu_init(div)
{
	menu_timeout =  new Array();
	menu_last = new Array();
	var nav = /MSIE/;
	var ie = nav.test(navigator.appVersion);
	var li = document.getElementById(div).getElementsByTagName("li");
	var ul = document.getElementById(div).getElementsByTagName("ul");
	
	// Configuration des LI
	for (var i=0; i<li.length; i++)
	{
		// Evenements de la souris
		li[i].onmouseover = menu_open;
		li[i].onmouseout = menu_close;
		
		// D�tection des sous-menus
		if (li[i].getElementsByTagName("ul").length > 0)
		{
			li[i].getElementsByTagName("a")[0].className = "out parent";
		}
		
		// Bug fix pour IE (Microsoft please learn to code)
		if (ie == true) li[i].style.display = "inline";
	}
	
	// Configuration des UL
	for (var i=0; i<ul.length; i++)
	{
		// D�tecion du niveau d'arborescence
		var lvl = 1;
		var item = ul[i];
		while (item.parentNode.id != div)
		{
			item = item.parentNode.parentNode;
			lvl++;
		}

		// Param�tres de l'�lement
		ul[i].style.zIndex = lvl;
		ul[i].id = "menu_"+div+"_"+i;
	}
}
