document.write("<SCRIPT language='JavaScript' src='js/wiki.js'                    type='text/javascript'></SCRIPT>");
document.write("<SCRIPT language='JavaScript' src='js/calendar/calendar.js'       type='text/javascript'></SCRIPT>");
document.write("<SCRIPT language='JavaScript' src='js/calendar/calendar-setup.js' type='text/javascript'></SCRIPT>");
document.write("<SCRIPT language='JavaScript' src='js/treecat.js'                 type='text/javascript'></SCRIPT>");

// -----------------------------------------------------------------------------
//  INITIALISATION
// -----------------------------------------------------------------------------

Event.observe(window, 'load', function() { setupSingleSubmitButton(); });


// -----------------------------------------------------------------------------
// Data Chooser 
// -----------------------------------------------------------------------------

function openDataChooser(window, ctxPath, form, widget, pos, jspPath, width, height){ // ctxPath must not end with '/', and jspPath must start with '/'
 var formName   = getFormName(window.document, form);
 var widgetPos  = getFormElementPos(form,widget)+pos;
 var chooser    = ctxPath+jspPath+"targetInput=document."+formName+".elements["+widgetPos+"].value&targetLabel=document."+formName+".elements["+(widgetPos-1)+"].value";
 popupWindow(chooser, 'DataChooser', width, height,'no','yes','yes',false);
}

// -----------------------------------------------------------------------------
// Date Chooser 
// -----------------------------------------------------------------------------

function openDateChooser(window, ctxPath, form, widget, pos, dateOnly, showWeek){ 
 var formName   = getFormName(window.document, form);
 var widgetPos   = getFormElementPos(form,widget)+pos;
 var fieldValue  = escape(form.elements[widgetPos].value);
 var targetInput = "document."+formName+".elements["+widgetPos+"].value";
 var chooser     = ctxPath+"/work/calendarPopup.jsp?showWeekNbr="+showWeek+"&dateOnly="+dateOnly+"&datetime="+fieldValue+"&targetInput="+targetInput;
 popupWindow(chooser, 'calendar', '300', dateOnly ? '260' : '275');
}

// -----------------------------------------------------------------------------
// Doc Chooser 
// -----------------------------------------------------------------------------

function openDocChooser(window, ctxPath, form, widget, pos, nbElt, id){ 
 var formName   = getFormName(window.document, form);
 var widgetPos  = getFormElementPos(form,widget) + pos;
 var chooser    = ctxPath+"/work/docChooser.jsp?form="+formName+"&nbElt="+nbElt+"&startElt="+(widgetPos-1);
 
 if (id){
  chooser   = chooser+"&id="+id;
 }
 popupWindow(chooser, 'DocChooser', 640, 520,'no','yes','yes',false);
}

// -----------------------------------------------------------------------------
// File Chooser 
// -----------------------------------------------------------------------------

function openFileChooser(window, ctxPath, path, form, widget, pos){ 
 var formName   = getFormName(window.document, form);
 var widgetPos  = getFormElementPos(form,widget)+pos;
 var chooser    = ctxPath+"/work/fileChooser.jsp?"+path+"targetInput="+formName+".elements["+widgetPos+"]&targetInputValue="+escape(form.elements[widgetPos].value);
 popupWindow(chooser, 'FileChooser', 700, 500,'no','yes','yes',false);
}

// -----------------------------------------------------------------------------
// Color Chooser 
// -----------------------------------------------------------------------------

function openColorChooser(window, ctxPath, form, widget, pos){ 
 var formName   = getFormName(window.document, form);
 var widgetPos  = getFormElementPos(form,widget)+pos;
 var chooser    = ctxPath+"/work/wysiwyg/wysiwyg_colorChooser.jsp?targetInput="+formName+".elements["+widgetPos+"]";
 popupWindow(chooser, 'ColorChooser', 235, 220,'no','no','yes',false);
}

// -----------------------------------------------------------------------------
// Query Chooser 
// -----------------------------------------------------------------------------

function openQueryChooser(window, ctxPath, form, widget, pos){ 
 var formName   = getFormName(window.document, form);
 var widgetPos  = getFormElementPos(form,widget)+pos;
 var chooser    = ctxPath+"/work/queryChooser.jsp?targetInput=document."+formName+".elements["+widgetPos+"].value&targetLabel=document."+formName+".elements["+(widgetPos-1)+"].value&qs="+escape(form.elements[widgetPos].value);
 popupWindow(chooser, 'QueryChooser', 800, 400,'no','yes','yes',false);
}

// -----------------------------------------------------------------------------
// Image Chooser
// -----------------------------------------------------------------------------

function openImageChooser(window, ctxPath, form, widget, pos){ 
 internalOpenImageChooser(window, ctxPath, form, widget, pos, false);
}
function openImagePubChooser(window, ctxPath, form, widget, pos){ 
 internalOpenImageChooser(window, ctxPath, form, widget, pos, true);
}

function internalOpenImageChooser(window, ctxPath, form, widget, pos, isPubChooser){ 
  var widgetPos  = getFormElementPos(form,widget)+pos;
  var chooser    = ctxPath + "/work/mediaBrowser.jsp?medias=image&jsFunc="
                  + (isPubChooser ? "insertSelectedPub" : "insertSelectedMedia")
                  + "&targetInput=" + widgetPos
                  + (isPubChooser ? ("&targetLabel="+(widgetPos-1)) : "");
 
  popupWindow(chooser, 'MediaBrowser', 930, 570,'no','no','no',false);
}

// -----------------------------------------------------------------------------
// Media Chooser 
// -----------------------------------------------------------------------------

function openMediaChooser(window, ctxPath, form, widget, pos){ 
 internalOpenMediaChooser(window, ctxPath, form, widget, pos, 'insertSelectedMedia');
}
function openMediaPubChooser(window, ctxPath, form, widget, pos){ 
 internalOpenMediaChooser(window, ctxPath, form, widget, pos, 'insertSelectedPub', true);
}
function openWikiMediaChooser(window, ctxPath, form, widget, pos){ 
 internalOpenMediaChooser(window, ctxPath, form, widget, pos, 'replaceSelectedMedia');
}
function internalOpenMediaChooser(window, ctxPath, form, widget, pos, jsfunc, label){ 
 var widgetPos  = getFormElementPos(form,widget)+pos;
 var chooser    = ctxPath + "/work/mediaBrowser.jsp?media=all&jsFunc="+jsfunc
                  + "&targetInput=" + widgetPos
                  + (label ? ("&targetLabel="+(widgetPos-1)) : "");
 popupWindow(chooser, 'MediaBrowser', 930, 570,'no','no','no',false);
}

// -----------------------------------------------------------------------------
// Image Map Preview 
// -----------------------------------------------------------------------------

function previewImageMap(window, ctxPath, form, widget, pos, title){
  var formName  = getFormName(window.document, form);
  var mapPos    = getFormElementPos(form,widget+'Map')+pos;
  var widgetPos = getFormElementPos(form,widget)+pos;
  var imgurl    = form.elements[widgetPos].value;
  var chooser   = ctxPath+"/work/previewImageMap.jsp?formName="+formName+"&mapPos="+mapPos+"&url="+imgurl;
  popupWindow(chooser, title, 640, 480,'no','yes','yes',false);
}

// -----------------------------------------------------------------------------
// WIKI PubLink
// -----------------------------------------------------------------------------

function wikiPubLink(window, ctxPath, form, widget, pos, title, jsfunc){
  var formName  = getFormName(window.document, form);
  var widgetPos = getFormElementPos(form,widget)+pos;
  var chooser   = ctxPath+"/work/pubChooser.jsp?jsFunc="+jsfunc;
  window.wiki   = form.elements[widgetPos];
  popupWindow(chooser, title, 900, 500,'no','yes','yes',false);
}

// -----------------------------------------------------------------------------
// WIKI Link
// -----------------------------------------------------------------------------

function wikiLink(window, ctxPath, form, widget, pos, title){
  var formName  = getFormName(window.document, form);
  var widgetPos = getFormElementPos(form,widget)+pos;
  var chooser   = ctxPath+"/work/wikiLink.jsp?mode=imageMap&showText=false&formElt="+widgetPos;
  
  var txt = getSelectionText(form.elements[widgetPos]);
  popupWindow(chooser+"&txt="+txt, title, 325, 60,'no','yes','yes',false);
}

// -----------------------------------------------------------------------------
// IMAGE MAP Func
// -----------------------------------------------------------------------------

function pubLinkImageMap(id, label) {
  url = "display.jsp?id=" + id ;
  replaceSelection(window.wiki, url) ;
}

function insertLinkImageMap(input, url) {
  replaceSelection(input, url);
}

// -----------------------------------------------------------------------------
// TEST SQLQuery
// -----------------------------------------------------------------------------


function testSQLQuery(window, ctxPath, form, widget, pos, title){
  var sqlQuery   = form.elements[getFormElementPos(form,widget)+pos];
	var dataSource = form.elements[getFormElementPos(form,widget+"DataSource")+pos];
	var maxRows    = form.elements[getFormElementPos(form,widget+"MaxRows")+pos];
	
	var chooser    = ctxPath+"/work/checkSQLQuery.jsp?";
	    chooser   += "dataSource=" +escape(dataSource.value);
	    chooser   += "&maxRows="   +escape(maxRows.value);
	    chooser   += "&sqlQuery="  +escape(sqlQuery.value);
	
	// alert("pos: "+pos+"\ndataSource "+dataSource.value + "\nmaxRows " + maxRows.value + "\nsqlQuery "+sqlQuery.value);
	popupWindow(chooser, title, 400, 600,'no','yes','yes',false);   
}


// -----------------------------------------------------------------------------
// Common jsFunc callback functions : Image and Media Chooser 
// -----------------------------------------------------------------------------

function insertSelectedPub(id, label, targetInput, targetLabel, imgUrl) {
  document.editForm.elements[ targetInput ].value = id;
  document.editForm.elements[ targetLabel ].value = label;
}

function insertSelectedMedia(id, label, targetInput, targetLabel, mediaUrl) {
  document.editForm.elements[ targetInput ].value = mediaUrl;
}

function replaceSelectedMedia(id, label, targetInput, targetLabel, mediaUrl) {
  insertImage(document.editForm.elements[ targetInput ], mediaUrl)
}

// -----------------------------------------------------------------------------
// Duration Chooser 
// -----------------------------------------------------------------------------

function fillDurationField(window, form, widget, pos, resolution){
  var widgetPos  = getFormElementPos(form,widget)+pos;
  form.elements[widgetPos].value = form.elements[widgetPos+1].value * form.elements[widgetPos+2].value / resolution;
}

// -----------------------------------------------------------------------------
// Setup WYSIWYG
// -----------------------------------------------------------------------------

function setupAllWYSIWYG(window, form){
  // Get all elements
  var elms = form.elements;
 
  // Find out the (first) current element
  for (var i = 0 ; i < elms.length ; i++){
    if (elms[i].className == "wysiwyg"){
      elms[i].value = getFrameElementHTML(window , elms[i].id);
    }
  }
}

function getFrameElementHTML(window, name){
  // Get all elements
  var fram = window.frames;
  
  // Find out the (first) current element
  for (var i = 0 ; i < fram.length ; i++){
    if (fram[i].name == name){
      return fram[i].getHTML();
    }
  }
  return "";
}

// -----------------------------------------------------------------------------
// Fields 
// -----------------------------------------------------------------------------


function fillOpenerField(window, form, widget, pos, id, label){
  var widgetPos  = getFormElementPos(form,widget)+pos;
  form.elements[widgetPos].value   = id;
  form.elements[widgetPos-1].value = label;
  window.close();
}

function submitAddCount(window, form, widget, fullwidget, addCount, langCount, id){
  var elmCount = getFormElementCount(form, widget, id) / langCount;
  fullwidget.value = elmCount+addCount;
  simpleSubmitForm(window, form, "opRefresh", "Wait WYSIWYG");
}

function clearDefField(def, e1, e2, e3, e4) {
    if (e1 != null) {
      if (e1.options)
        e1.options[0].selected = true;
      else
        e1.value = def;
    }
    if (e2 != null) {
      if (e2.options)
        e2.options[0].selected = true;
      else
        e2.value = def;
    }
    if (e3 != null) {
      if (e3.options)
        e3.options[0].selected = true;
      else
        e3.value = def;
    }
    if (e4 != null) {
      if (e4.options)
        e4.options[0].selected = true;
      else
        e4.value = def;
    }
}


function moveFormElement(form, widget, relativepos, op, relfirst, id) {
  var array = form.elements;
  var first = getFormElementPos(form,widget,id)+relfirst;
  var last  = getFormElementLastPos(form, widget, true,id)+relfirst;
  var pos   = first + relativepos;

  // alert(id+': '+first+'->'+pos+'<-'+last);

  // Move Up
  if (op == "up") {
    if (pos > first) { 
      tmp = array[pos].value;
      array[pos].value = array[pos - 1].value;
      array[pos - 1].value = tmp;
    } else {
      tmp = array[first].value;
      for(i = first; i < last; i++) {
        array[i].value = array[i + 1].value;
      }
      array[last].value = tmp;
    }
  } 
  // Move Down
  else if (op == "down") {
    if (pos < last) {
      tmp = array[pos].value;
      array[pos].value = array[pos + 1].value;
      array[pos + 1].value = tmp;
    } else {
      tmp = array[last].value;
      for(i = last; i > first; i--) {
        array[i].value = array[i - 1].value;
      }
      array[first].value = tmp;
    }
  }
  // Remove Element
  else if (op == "remove") {
    for(i = pos; i < last; i++) {
      array[i].value = array[i + 1].value;
    }
    array[last].value = "";

  }
}

function move2FormElement(form, widget, relativepos, op, relfirst, id) {
  var array = form.elements;
  
  // Find position -1 because we locate the second field not the first one
  var first = getFormElementPos(form,widget,id) + relfirst;
  var last  = getFormElementLastPos(form, widget, false,id) + relfirst;
  var pos   = first + relativepos;

  // Move Up
  if (op == "up") {
    if (pos > first) {
      tmp1 = array[pos].value;
      tmp2 = array[pos+1].value;
      array[pos    ].value = array[pos - 2].value;
      array[pos + 1].value = array[pos - 1].value;
      array[pos - 2].value = tmp1;
      array[pos - 1].value = tmp2;
    } else {
      tmp1 = array[first].value;
      tmp2 = array[first+1].value;
      for(i = first; i < last; i+=2) {
        array[i  ].value = array[i + 2].value;
        array[i+1].value = array[i + 3].value;
      }
      array[last].value = tmp1;
      array[last+1].value = tmp2;
    }
  } 
  // Move Down
  else if (op == "down") {
    if (pos < last) {
      tmp1 = array[pos].value;
      tmp2 = array[pos+1].value;
      array[pos    ].value = array[pos + 2].value;
      array[pos + 1].value = array[pos + 3].value;
      array[pos + 2].value = tmp1;
      array[pos + 3].value = tmp2;
    } else {
      tmp1 = array[last    ].value;
      tmp2 = array[last + 1].value;
      for(i = last; i > first; i-=2) {
        array[i].value = array[i - 2].value;
        array[i+1].value = array[i - 1].value;
      }
      array[first    ].value = tmp1;
      array[first + 1].value = tmp2;
    }
  }
  // Remove Element
  else if (op == "remove") {
    for(i = pos; i < last; i+=2) {
      array[i].value   = array[i + 2].value;
      array[i+1].value = array[i + 3].value;
    }
    array[last].value = "";
    array[last+1].value = "";
  }
}

function move3FormElement(form, widget, relativepos, op, relfirst, id) {
  var array = form.elements;
  
  // Find position -1 because we locate the second field not the first one
  var first = getFormElementPos(form,widget,id) + relfirst;
  var last  = getFormElementLastPos(form, widget, false, id) + relfirst;
  var pos   = first + relativepos;

  // Move Up
  if (op == "up") {
    if (pos > first) {
      tmp1 = array[pos].value;
      tmp2 = array[pos+1].value;
      tmp3 = array[pos+2].value;
      array[pos    ].value = array[pos - 3].value;
      array[pos + 1].value = array[pos - 2].value;
      array[pos + 2].value = array[pos - 1].value;
      array[pos - 3].value = tmp1;
      array[pos - 2].value = tmp2;
      array[pos - 1].value = tmp3;
    } else {
      tmp1 = array[first].value;
      tmp2 = array[first+1].value;
      tmp3 = array[first+2].value;
      for(i = first; i < last; i+=3) {
        array[i  ].value = array[i + 3].value;
        array[i+1].value = array[i + 4].value;
        array[i+2].value = array[i + 5].value;
      }
      array[last].value = tmp1;
      array[last+1].value = tmp2;
      array[last+2].value = tmp3;
    }
  } 
  // Move Down
  else if (op == "down") {
    if (pos < last) {
      tmp1 = array[pos].value;
      tmp2 = array[pos+1].value;
      tmp3 = array[pos+2].value;
      array[pos    ].value = array[pos + 3].value;
      array[pos + 1].value = array[pos + 4].value;
      array[pos + 2].value = array[pos + 5].value;
      array[pos + 3].value = tmp1;
      array[pos + 4].value = tmp2;
      array[pos + 5].value = tmp3;
    } else {
      tmp1 = array[last    ].value;
      tmp2 = array[last + 1].value;
      tmp3 = array[last + 2].value;
      for(i = last; i > first; i-=3) {
        array[i].value = array[i - 3].value;
        array[i+1].value = array[i - 2].value;
        array[i+2].value = array[i - 1].value;
      }
      array[first    ].value = tmp1;
      array[first + 1].value = tmp2;
      array[first + 2].value = tmp3;
    }
  }
  // Remove Element
  else if (op == "remove") {
    for(i = pos; i < last; i+=3) {
      array[i].value   = array[i + 3].value;
      array[i+1].value = array[i + 4].value;
      array[i+2].value = array[i + 5].value;
    }
    array[last].value = "";
    array[last+1].value = "";
    array[last+2].value = "";
  }
}

// -----------------------------------------------------------------------------
// Form Element Access 
// -----------------------------------------------------------------------------

function getFormElementCount(form, name, id){
  // Get all elements
  var elms  = form.elements;
  var count = 0;
  
  // Count element
  for (var i = 0 ; i < elms.length ; i++){
    if (elms[i].name != name)
      continue;
      
    if (!id){
      count++;
      continue;
    }
    
    if ((elms[i].id == id) || (elms[i].id.indexOf(id) > -1)){ // very ugly
      count++;
    }
  }
  return count;
}

function getFormElementPos(form, name, id){
  // Get all elements
  var elms = form.elements;
 
  // Find out the (first) current element
  for (var i = 0 ; i < elms.length ; i++){
    if (elms[i].name != name)
      continue;
      
    if (!id)
      return i;
    
    if ((elms[i].id == id) || (elms[i].id == id+'0')){ // very ugly
      return i;
    }
  }
  return -1;
}

function getFormElementLastPos(form, name, first, id){
  // Get all elements
  var elms = form.elements;
 
  // Find out the (last) current element
  var last = -1;
  for (var i = 0 ; i < elms.length ; i++){

    if (elms[i].name != name){
      if (last > 0 && first)
        break;
      continue;
    }
      
    if (!id){
      last = i;
      continue;
    }
      
    if (elms[i].id.indexOf(id) > -1){
      last = i;
      continue;
    }
    
    if (last > 0 && first)
      break;
  }
  return last;
}

// -----------------------------------------------------------------------------
// Button 
// -----------------------------------------------------------------------------


function toggleSingleSubmitButton(evt, form, on){

	// check "this" is a form, otherwise, use given form
	if (this.elements) {
	  form = this;
	}

	// Enable/Disable all formButton
	var elements = $(form.elements);
	for (var i = 0 ; i < elements.length ; i++){
    var elm = $(elements[i]);

	  if (!on){
		  if (!elm.hasClassName("formButton")) { continue; }
		  elm.addClassName("disabledButton");
		  elm.oldclick = elm.onclick;
		  elm.onclick  = function(event){ return false; }
	  } else {
	    if (!elm.hasClassName("disabledButton")) { continue;  }
	    elm.removeClassName("disabledButton");
		  elm.onclick  = elm.oldclick;
	  }
  }
  
  // Register/Unregister myself
  if (!on){
	  form.oldsubmit = form.onsubmit;
	  form.onsubmit  =  function(event){ return false; }
  }
  else{
	  form.onsubmit  =  form.oldsubmit;
  }
}

function setupSingleSubmitButton(){
  
  // Register function
  var forms = document.forms;
  for (var i = 0 ; i < forms.length ; i++){
    var form = $(forms[i]);
    
    if (form.hasClassName("noSingleSubmitButton"))
      continue;
      
    var onsubmit = form.onsubmit;
    if (typeof onsubmit == 'function') {
      form.onsubmit = function(evt){
        toggleSingleSubmitButton(evt, this);
        return onsubmit(evt, this);
      };
    }
    else{
      form.onsubmit = toggleSingleSubmitButton;
    }
  }
}

// -----------------------------------------------------------------------------
// Form 
// -----------------------------------------------------------------------------

/**
 * form should be like: window.document.editForm
 */ 
function simpleSubmitForm(window, fullform, action, warn, anchorValue, actionValue) {
  
  if(fullform.pageLoaded && fullform.pageLoaded.value=='false') {
    alert(warn);
    return;
  }

  setupAllWYSIWYG(window, fullform);
  
  for (var i = 0; i < fullform.elements.length; i++) {
    if (fullform.elements[i].jcmsSubmitCallBack != null) {
      fullform.elements[i].jcmsSubmitCallBack(window, fullform, action, warn, anchorValue);
    }
  }

  if (anchorValue){
  	fullform.anchor.value = anchorValue;
  }
  
  if (action && fullform.action){
    fullform.action.name  = action;
    if (actionValue)
      fullform.action.value = actionValue;
  }
  
  if (fullform.onsubmit)
    fullform.onsubmit();
  fullform.submit();
}

function confirmSubmitForm(window, fullform, action, msg, warn, anchorValue, actionValue) {
  if (top.confirm(msg)) {
    simpleSubmitForm(window, fullform, action, warn, anchorValue, actionValue);
  }
}

function getFormName(document, form){
  if (document.all){
    if (form.attributes){
      if (form.attributes.getNamedItem){
      	return form.attributes.getNamedItem("NAME").value;
      }
      else{
      	if (form.attributes["name"])
      	  return form.attributes["name"];
      	else if (form.attributes["NAME"])
      	  return form.attributes["NAME"];
      	else
      	  return 'editForm';
      }
    }
    else{
    	return 'editForm';
    }
  }
  else{
    return form.getAttribute("NAME");
  }
}

// -----------------------------------------------------------------------------
// EditPubFooter: DEPRECATED Should be removed or replaced
// -----------------------------------------------------------------------------
	

	function submitForm(action, anchor, actionvalue) {  
	  simpleSubmitForm(window, window.document.editForm, action,'Error',anchor, actionvalue);
	}
	function confirmSubmit(msg, action, anchor, actionvalue) {
    confirmSubmitForm(window, window.document.editForm, action, msg, anchor, actionvalue)
	}
	function submitAction(action, value) {
	  simpleSubmitForm(window, window.document.editForm, action,'Error',null, value);
	}
	
	/* This function are no longer used in JCMS 5.5
		function removeField(element, anchor) {
		  element.value = "__remove__";
		  submitForm("opRefresh", anchor);
		}
		function removeSelectedIndex(element, anchor) {
		  element.options[element.selectedIndex].value = "__remove__";
		  submitForm("opRefresh", anchor);
		}
		function clearSelectedIndex(element) {
		  element.selectedIndex=0;
		}
		function validateListItem(action, list, pos) {
		  window.document.editForm.opItem.name = action;
		  window.document.editForm.opItem.value = list;
		  window.document.editForm.itemPos.value = pos;
		  submitForm("opRefresh", list);
		}
	*/

