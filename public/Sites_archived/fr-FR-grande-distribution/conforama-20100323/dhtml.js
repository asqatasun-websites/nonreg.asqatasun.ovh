/**
 * elements dhtml statiques du site conforama corp, uniquement les evenements
 * utilisateurs sans transfers de donn&eacute;es.
 * 
**/


// la farandole

var AllFarandoles={
	// éléments constantes dans la page
	listeFarandole: "",// tableau de toutes les farandoles de la page
	
	_Init: function(){
		AllFarandoles.listeFarandole=$("."+SetConstant.tamponFarandoles);
		var mesFarandoles=new Array();
		AllFarandoles.listeFarandole.each(function(i){
			var monIndice=i;
			mesFarandoles[monIndice] ={
				_Init: function(htmlElement, indiceFarandole){
					mesFarandoles[monIndice].largeur=690;//largeur de la farandole
					mesFarandoles[monIndice].numElems="";//nombre d'éléments de la farandole
					mesFarandoles[monIndice].largeurElem="";//nombre d'éléments de la farandole
					mesFarandoles[monIndice].numRow="";//nombre d'éléments de la farandole
					mesFarandoles[monIndice].maxHeightRow="";// taille la plus haute de la ligne courrante
					mesFarandoles[monIndice].tabRowElements= new Array();// tableau des éléments de la ligne courrante
					mesFarandoles[monIndice].tabRowElementsHeight=new Array(); //table des éléments de hauteurs de la ligne courrante
					
					//initialisation des valeurs par defaut
					mesFarandoles[monIndice].numElems = $("."+SetConstant.tamponFarandoleElement, $(htmlElement)).length;//attribution du nombre d'éléments dans la farandole
					mesFarandoles[monIndice].numRow=Math.ceil(mesFarandoles[monIndice].numElems/SetConstant.nbElementsByRowFarandole);// Math.round(Farandole.numElems % SetConstant.nbElementsByRowFarandole)
					mesFarandoles[monIndice].currentRow=SetConstant.currentPageFarandoles;
					
					// modification de la structure html
					mesFarandoles[monIndice]._SetTabRowElements(htmlElement, indiceFarandole);
					mesFarandoles[monIndice]._ComposeMenu(htmlElement, indiceFarandole);
				},
				
				_ComposeMenu: function(htmlElement, indiceFarandole){
					if(mesFarandoles[indiceFarandole].numRow>1){
						var htmlMenu="<ul class='menuFarandole_"+indiceFarandole+"'>";
						for(var i=0; i<(mesFarandoles[indiceFarandole].numRow); i++){
							if(i==(mesFarandoles[indiceFarandole].currentRow-1)){
								htmlMenu+='<li class="pictoPageFarandole pageFarandole_0'+(i+1)+'"><span><span class="displayNone">vous êtes ici : page '+(i+1)+' sur '+(mesFarandoles[indiceFarandole].numRow)+' de la farandole</span></span></li>';
							}else{
								htmlMenu+='<li class="pictoPageFarandole pageFarandole_0'+(i+1)+'"><a href="#"><span class="displayNone">aller &agrave; la page '+(i+1)+' sur '+(mesFarandoles[indiceFarandole].numRow)+' de la farandole</span></a></li>';	
							}
						}
						if(mesFarandoles[indiceFarandole].currentRow==1){
							htmlMenu+='<li class="pictoPagePrev"><span><span class="displayNone">Page Pr&eacutec&eacutedente</span></span></li>';
						}else{
							htmlMenu+='<li class="pictoPagePrev"><a href="#"><span class="displayNone">Page Pr&eacutec&eacutedente</span></a></li>';
						}
						if(mesFarandoles[indiceFarandole].currentRow==mesFarandoles[indiceFarandole].numRow){
							htmlMenu+='<li class="pictoPageNext"><span><span class="displayNone">Page Suivante</span></span></li>';
						}else{
							htmlMenu+='<li class="pictoPageNext"><a href="#"><span class="displayNone">Page Suivante</span></a></li>';
						}
						htmlMenu+='</ul>';
						
						//insertion du menu dans la page
						$($(".titleMenuFarandole")[indiceFarandole]).append(htmlMenu);
						
						$(".pictoPageFarandole a", $($(".titleMenuFarandole")[indiceFarandole])).click(function(){
							mesFarandoles[indiceFarandole]._PictoPageFarandole($(this), htmlElement, indiceFarandole);
							return(false);
						})//fonction au click sur une pastille de page;
						
						$(".pictoPageNext a", $($(".titleMenuFarandole")[indiceFarandole])).click(function(){
							mesFarandoles[indiceFarandole]._PictoPageNext($(this), htmlElement, indiceFarandole);
							return(false);
						})//fonction au click sur une pastille de page;
						
						$(".pictoPagePrev a", $(".titleMenuFarandole")[indiceFarandole]).click(function(){
							mesFarandoles[indiceFarandole]._PictoPagePrev($(this), htmlElement, indiceFarandole);
							return(false);
						})//fonction au click sur une pastille de page;
					}
					
				},
				_PictoPagePrev: function(myLink, htmlElement, indiceFarandole){
					var maPage=Number(mesFarandoles[indiceFarandole].currentRow)-1;
					mesFarandoles[indiceFarandole].currentRow=maPage;
					var monLeft=-((mesFarandoles[indiceFarandole].largeur*maPage)-mesFarandoles[indiceFarandole].largeur);
					$("ul.menuFarandole_"+indiceFarandole+" span").remove();
					$("ul.menuFarandole_"+indiceFarandole+" a").remove();
					$("ul.menuFarandole_"+indiceFarandole+" li").remove();
					$("ul.menuFarandole_"+indiceFarandole).remove();
					
					mesFarandoles[indiceFarandole]._ComposeMenu(htmlElement, indiceFarandole);
					
					if($.browser.safari){
						$(".setWidth", $(htmlElement)).css({left: monLeft+"px"});
					}else{
						$(".setWidth", $(htmlElement)).animate({left: monLeft}, "500", function(){
							$(this).css({left: monLeft});
						});
					}
					return(false);
				},
				_PictoPageNext: function(myLink, htmlElement, indiceFarandole){
					var maPage=Number(mesFarandoles[indiceFarandole].currentRow)+1;
					mesFarandoles[indiceFarandole].currentRow=maPage;
					var monLeft=-((mesFarandoles[indiceFarandole].largeur*maPage)-mesFarandoles[indiceFarandole].largeur);
					$("ul.menuFarandole_"+indiceFarandole+" span").remove();
					$("ul.menuFarandole_"+indiceFarandole+" a").remove();
					$("ul.menuFarandole_"+indiceFarandole+" li").remove();
					$("ul.menuFarandole_"+indiceFarandole).remove();
					
					mesFarandoles[indiceFarandole]._ComposeMenu(htmlElement, indiceFarandole);
					
					if($.browser.safari){
						$(".setWidth", $(htmlElement)).css({left: monLeft+"px"});
					}else{
						$(".setWidth", $(htmlElement)).animate({left: monLeft}, "500", function(){
							$(this).css({left: monLeft});
						});
					}
					return(false);
				},
				_PictoPageFarandole: function(myLink, htmlElement, indiceFarandole){
					var maPage=$(myLink).parent().attr("class").replace("pictoPageFarandole pageFarandole_0", "");
					mesFarandoles[indiceFarandole].currentRow=maPage;
					var monLeft=-((mesFarandoles[indiceFarandole].largeur*maPage)-mesFarandoles[indiceFarandole].largeur);
					
					$("ul.menuFarandole_"+indiceFarandole+" span").remove();
					$("ul.menuFarandole_"+indiceFarandole+" a").remove();
					$("ul.menuFarandole_"+indiceFarandole+" li").remove();
					$("ul.menuFarandole_"+indiceFarandole).remove();
					mesFarandoles[indiceFarandole]._ComposeMenu(htmlElement, indiceFarandole);
					
					if($.browser.safari) {
						$(".setWidth", $(htmlElement)).css({left: monLeft+"px"});
					}else{
						$(".setWidth", $(htmlElement)).animate({left: monLeft}, "500", function(){
							$(this).css({left: monLeft});
						});
					}
					return(false);
					
				},
				_SetTabRowElements: function(htmlElement, indiceFarandole){
					
					mesFarandoles[indiceFarandole].maxHeightRow=$($("."+SetConstant.tamponFarandoleElementDescript, $(htmlElement))[0]).height();
					
					$("."+SetConstant.tamponFarandoleElementDescript, htmlElement).each(function(i){
						if($(this).height() > mesFarandoles[indiceFarandole].maxHeightRow){
							mesFarandoles[indiceFarandole].maxHeightRow=$(this).height();
						}
					});
					
					$("."+SetConstant.tamponFarandoleElementDescript, htmlElement).css({height: (mesFarandoles[indiceFarandole].maxHeightRow+20)+"px"});
					
					$(".setWidth", $(htmlElement)).css({width: (mesFarandoles[indiceFarandole].largeur * mesFarandoles[indiceFarandole].numRow)+"px", position: "absolute", left: -((mesFarandoles[indiceFarandole].largeur*SetConstant.currentPageFarandoles)-mesFarandoles[indiceFarandole].largeur)+"px"});
					$(".setOverflow", $(htmlElement)).css({width: (mesFarandoles[indiceFarandole].largeur-20)+"px", overflow: "hidden", position: "relative", height: (mesFarandoles[indiceFarandole].maxHeightRow+20+110)+"px"});
					$(".lastFarandoleRowElement", $(htmlElement)).each(function(i){
						if(i!=($(".lastFarandoleRowElement", $(htmlElement)).length-1)){
							$(".lastFarandoleRowElement", $(htmlElement)).removeClass(".lastFarandoleRowElement");
						}
					});
					
				}
			};// gestion d'un éléments de farandole
			
			mesFarandoles[monIndice]._Init(AllFarandoles.listeFarandole[monIndice], monIndice);
		});
	}//initialisation des farandole
};
// la farandole

var miniFarandole={
	_Init:function(){
		miniFarandole.currentRow=0,
		miniFarandole.nbElements=$(".complemataryProducts").length;
		miniFarandole.nbRows=Math.ceil(miniFarandole.nbElements/4);
		$(".complemataryProducts").addClass("displayNone");
		$(".complemataryProducts").eq(0).removeClass("displayNone");
		
		miniFarandole._ComposeMenu(miniFarandole.currentRow);//composeMenu
		miniFarandole._ComposeVignettes();//initialisation du menu
	},
	_ComposeVignettes: function(){
		miniFarandole.indiceCurrent=0;
		miniFarandole.currentRow=0;
		miniFarandole.vignettesMenu='<ul>';
		$(".complemataryProducts").each(function(i){
			var curClass=(i==0)?'thumbon':'thumboff';
			miniFarandole.vignettesMenu+='<li><a href="#" class="'+curClass+' indiceElement_'+i+'"><img src="'+$("span.floatLeft img",$(".complemataryProducts").eq(i)).attr("src").replace("130x100", "065x050").replace("N_", "V_").replace("no-photo","no-photo-65x50")+'" /></a></li>';
		});
		miniFarandole.vignettesMenu+='</ul>';
		$("#contentVignettesMiniFarandole").append(miniFarandole.vignettesMenu);
		$("#contentVignettesMiniFarandole ul").css("width", $(".complemataryProducts").length*($("#contentVignettesMiniFarandole ul li").eq(0).width()+parseInt($("#contentVignettesMiniFarandole ul li").eq(0).css("marginRight")))+"px");
		
		$("#contentVignettesMiniFarandole ul li a").each(function(i){
			$("#contentVignettesMiniFarandole ul li a").eq(i).click(function(){
				
				$("#contentVignettesMiniFarandole ul li a.thumbon").addClass("thumboff");
				$("#contentVignettesMiniFarandole ul li a.thumbon").removeClass("thumbon");
				$(this).removeClass("thumboff");
				$(this).addClass("thumbon");
				
				$(".complemataryProducts").eq(miniFarandole.indiceCurrent).attr( "class", $(".complemataryProducts").eq(miniFarandole.indiceCurrent).attr( "class")+" displayNone");
				
				$(".complemataryProducts").eq($(this).attr("class").replace("thumbon", "").replace("indiceElement_", "").replace("displayNone", "")).removeClass("displayNone");
				
				miniFarandole.indiceCurrent=parseInt($(this).attr("class").replace("thumbon", "").replace("indiceElement_", "").replace("displayNone", ""));
				return(false);
			});
		});
	},
	_PictoPageNext: function(){
		if(miniFarandole.currentRow+1<miniFarandole.nbRows){
			miniFarandole.currentRow++;
			var monLeft=($("#contentVignettesMiniFarandole").width()+23)*miniFarandole.currentRow;
			
			if($.browser.safari) {
				$("#contentVignettesMiniFarandole ul").css({left: (monLeft*-1)+"px"});
			}else{
				$("#contentVignettesMiniFarandole ul").animate({left: monLeft*-1}, "500", function(){
					$(this).css({left: monLeft*-1});
				});
			}
			
			miniFarandole._ComposeMenu(miniFarandole.currentRow);
		}
	},
	_PictoPagePrev: function(){
		if(miniFarandole.currentRow>0){
			miniFarandole.currentRow--;
			var monLeft=($("#contentVignettesMiniFarandole").width()+23)*miniFarandole.currentRow;
			
			if($.browser.safari) {
				$("#contentVignettesMiniFarandole ul").css({left: (monLeft*-1)+"px"});
			}else{
				$("#contentVignettesMiniFarandole ul").animate({left: monLeft*-1}, "500", function(){
					$(this).css({left: (monLeft*-1)});
				});
			}
			miniFarandole._ComposeMenu(miniFarandole.currentRow);
		}
	},
	_PictoPage: function(indiceElement){
		miniFarandole.currentRow=indiceElement;
		var monLeft=($("#contentVignettesMiniFarandole").width()+23)*miniFarandole.currentRow;
		if($.browser.safari) {
			$("#contentVignettesMiniFarandole ul").css({left: (monLeft*-1)+"px"});
		}else{
			$("#contentVignettesMiniFarandole ul").animate({left: monLeft*-1}, "500", function(){
				$(this).css({left: (monLeft*-1)});
			});
		}
		miniFarandole._ComposeMenu(miniFarandole.currentRow);
	},
		
	_ComposeMenu: function(currentRow){
		miniFarandole.htmlMenu='<ul id="menuMiniFarandole">';
		for(var i=0; miniFarandole.nbRows>i; i++){
			if(i==(currentRow)){
				miniFarandole.htmlMenu+='<li class="pictoPageFarandole pageFarandole_0'+(i)+'"><span><span class="displayNone">vous êtes ici : page '+(i+1)+' de la farandole</span></span></li>';
			}else{
				miniFarandole.htmlMenu+='<li class="pictoPageFarandole pageFarandole_0'+(i)+'"><a href="#"><span class="displayNone">aller &agrave; la page '+(i+1)+' de la farandole</span></a></li>';	
			}
		}
		if(currentRow==0){
			miniFarandole.htmlMenu+='<li class="pictoPagePrev"><span><span class="displayNone">Page Pr&eacutec&eacutedente</span></span></li>';
		}else{
			miniFarandole.htmlMenu+='<li class="pictoPagePrev"><a href="#"><span class="displayNone">Page Pr&eacutec&eacutedente</span></a></li>';
		}
		if(currentRow==(miniFarandole.nbRows-1)){
		miniFarandole.htmlMenu+='<li class="pictoPageNext"><span><span class="displayNone">Page Suivante</span></span></li>';
		}else{
		miniFarandole.htmlMenu+='<li class="pictoPageNext"><a href="#"><span class="displayNone">Page Suivante</span></a></li>';
		}
		miniFarandole.htmlMenu+='</ul>';
		//insertion du menu dans la page
		$("#contentMenuMiniFarandole").empty();
		$("#contentMenuMiniFarandole").append(miniFarandole.htmlMenu);
		
		$("#contentMenuMiniFarandole .pictoPageNext").click(function(){
			miniFarandole._PictoPageNext();
			return(false);
		});
		
		$("#contentMenuMiniFarandole .pictoPagePrev").click(function(){
			miniFarandole._PictoPagePrev();
			return(false);
		});
		
		$("#contentMenuMiniFarandole .pictoPageFarandole").click(function(){
			miniFarandole._PictoPage(parseInt($(this).attr("class").replace("pictoPageFarandole pageFarandole_", 0)));
			return(false);
		});
		
		
	}
};//initialisation des farandole



 $(document).ready(function() {
	AllFarandoles._Init();// initialisation des farandoles
 });//initialisation du document