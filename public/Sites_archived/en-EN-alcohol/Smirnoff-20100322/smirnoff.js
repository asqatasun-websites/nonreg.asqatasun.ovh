// Create the namespace
if (typeof Smirnoff === 'undefined')
    var Smirnoff = {};
    
var Menu = null;    
    
(function($) {
    var URL_REGEX = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    
    Smirnoff.util = {
        /**
         * Detect IE6 only once and save the boolean.
         */
        isIE6: function() {
            var m = navigator.userAgent.match(/MSIE (\d+)\.(\d+);/);
            if (!m) return false;
            return (m[1] == 6);
        } (),
        /**
         * List all properties of an object
         *
         * @param {Object} o
         */
        dumpVar: function(o) {
            var h = '';
            for (var prop in o)
            h += prop + "\n";
            Smirnoff.util.log.debug(h);
            Smirnoff.util.log.dump();
        },
        /**
         * Get image dimensions that conform to a maximum width and height.
         *
         * @param {Object} w Original width
         * @param {Object} h Original height
         * @param {Object} maxwidth Maximum width
         * @param {Object} maxheight Maximum height
         * @returns {Object}
         */
        thumbDimensions: function(w, h, maxwidth, maxheight)
        {
            var width,
            height,
            ratio;
            if (w == 0 || h == 0) {
                width = maxwidth;
                height = maxheight;
            }
            else
            if (w > h) {
                if (w > maxwidth) {
                    width = maxwidth;
                    ratio = maxwidth / w;
                    height = ratio * h;
                }
            }
            else {
                if (h > maxheight) {
                    height = maxheight;
                    ratio = maxheight / h;
                    width = ratio * w;
                }
                else {
                    width = w;
                    height = h;
                }
            }
            return {
                width: width,
                height: height
            };
        },
        /**
         * Simple logger.
         *
         * Usage: Smirnoff.util.log.debug('my message');
         *
         * From the console, run Smirnoff.util.log.dump(); .
         */
        log: (function() {
            var _log = [];
            return {
                debug: function(txt) {
                    _log.push(new Date() + ": " + txt);
                },
                get: function() {
                    return _log;
                },
                dump: function() {
                    return _log.join("\n");
                },
                clear: function() {
                    _log = [];
                }
            };
        })(),
        
        /**
         * If png files have special characters in their names, AlphaImageLoader won't handle them correctly
         * and therefore pngFix won't work. In such cases, call this function beforehand to escape them.
         * 
         * Curreently only works in conjunction with unitpngfix.js.
         */
        pngRename: function(img) {
            if (!Smirnoff.util.isIE6) { return; }
            
            if (!img) {
                img = jQuery('.unitPng');
            }
            
            img.each(function() {
                $(this).attr('src', escape($(this).attr('src')));
            });
        
        },
        
        /**
         * Home-grown pngfix
         * 
         * @param {Object} list jQuery array of elements to fix
         */
        pngFix: function(list) {
            if (Smirnoff.util.isIE6) {
                return list.each(function() {
                    var el = $(this),
                        src = el.attr('src'),
                        filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader" + "(src='" + src + "?rd=" + Math.floor(Math.random() * 100001) + "', sizingMethod='scale')";
                         
                    el
                        .attr('src', '/images/blank.gif')
                        .css({
                            'background': 'transparent none',
                            'filter': filter
                        });
                });
            }
        },
        
        /**
         * Parse the query string and get a parameter
         * 
         * @param {Object} name
         */
        getUrlParam: function(name) {
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regexS = "[\\?&]" + name + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(window.location.href);
            if (results == null)
            return "";
            else
            return results[1];
        },

        /**
         * Fire code when a specified condition is true
         * 
         * @param {Object} params
         */
        waitFor: function(params) {
            var condition = params.condition;
            var callback = params.callback;
            var interval = params.interval || 100;
            var maxTries = params.maxTries || 5;
            var currentTry = params._currentTry || 0; // private
            // If condition passes, run the code
            if (condition() === true) 
                return callback();
            
            // Limit the # of attempts
            if (currentTry < maxTries) {
                // Increment the attempt #
                params._currentTry = currentTry + 1;
                
                // Create the recursive call
                var f = function(){
                    return Smirnoff.util.waitFor(params);
                }
                
                // Wait for one interval and execute
                setTimeout(f, interval);
            }
            else {
                // alert( 'Maximum tries used for waitFor()...quitting' );
            }
        }
    };
    Smirnoff.DOM = {
        /**
         * Add browser-specific class to <body>.
         */
        addBodyClass: function() {
            $.each($.browser,
            function(i, val) {
                if (val && i !== 'version') {
                    $(document.body).addClass(i);
                }
                if (i === 'mozilla' && val && document.getElementsByClassName) {
                    $(document.body).addClass('ff3');
                }
            });
        },
        
        /**
         * Return the number of pixels to offset an element by
         *  in order to vertically center it
         * 
         * @param {Object} el The jQuery element
         */
        getVCenterTop: function(el) {
            return ((el.parent().height() / 2) - (el.height()/2))
        },
        
        /**
         * Move the AddThis popup to be near the flash button
         */
        repositionAddThis: function() {
            var target, offset, top, left;

            /*
             * Find something to position the popup near
             * 
             * TODO - refactor flash html to make this unnecessary
             */            
            target = $('#thereVideoPlayerSwf');
            if (!target.length) target = $('#there_home_swf');
            if (!target.length) target = $('#flashGoesHere');
            
            
            offset = target.offset();
            
            Smirnoff.util.log.debug(target);
            Smirnoff.util.log.debug(offset);
            Smirnoff.util.log.debug(target.width() + ", " + target.height());
            
            
            
            top  = offset.top + target.height() + 6 + "px";
            left = offset.left + ( target.width() -  jQuery('#at15s').width()) + "px";
            
            Smirnoff.util.log.debug('top: '+top);
            Smirnoff.util.log.debug('left: '+left);
            
            Smirnoff.util.waitFor({
                condition: function() {
                    Smirnoff.util.log.debug('checking for element: ' + (jQuery('#at20mc').length > 0));
                    return jQuery('#at15s').length > 0;
                },
                callback: function() {
                    Smirnoff.util.log.debug('callback: setting top,left to  '+top + ', '+left);
                    jQuery('#at15s').css({
                        'top': top,
                        'left': left
                    });                    
                },
                interval: 100,
                maxTries: 20
            })
        },
        
        /**
         * Show the AddThis popup programmatically
         */
        showAddThis: function() {
            $('#flashShareThis').mouseover();
        }
    };
    Smirnoff.UI = (function() {
        /*
         * Contains all 'state' dropdown values, indexed by locale
         */
        var hashCountryStates = {},
            hashEventTypes = {};
        
        return {
            updateStateSelect: function(e) {
                var value = $(this).attr('value');
                var country = value === '0' || value ==='ROW' ? 'row': value.split('-')[1].toLowerCase();
                var locale = value === '0' || value ==='ROW' ? 'row': value;
                var city = $('input#ctl00_city'),
                stateSelect,
                zip,
                emailOptin,
                smsOptin;
                // Actually a fieldset
                stateSelect = $('.state').parent();
                zip = $('.zip').parent();
                if (hashCountryStates && hashCountryStates[locale]) {
                    Smirnoff.Session.locale(locale);
                    Smirnoff.UI.processStateInfo(locale, hashCountryStates[locale], country);
                } else {
                    /**
                     * Fetch states, put them into hashCountryStates
                     *
                     */
                    $.ajax({
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('ajax', 'true');
                        },
                        data: '',
                        dataType: 'json',
                        type: 'POST',
                        url: "/templates/retrieveStatesByCountry.aspx?locale=" + locale + "&rd=" + Math.floor(Math.random() * 100001),
                        success: function(data, ts) {
                            if (data.states) {
                                Smirnoff.UI.processStateInfo(locale, data.states, country);
                                hashCountryStates[locale] = data.states;
                            } else {
                                $('.state').attr('disabled', 'disabled').addClass('off').removeClass('on');
                                $('#ctl00_state').removeAttr('disabled').removeClass('off').addClass('on');
                                hashCountryStates[locale] = [];
                            }
                        },
                        error: function() {}
                    });
                }
                city.defaultAsLabel();
                stateSelect.find('input').defaultAsLabel();
                zip.find('input').defaultAsLabel();
            },
            
            /**
             * If data is passed, populate state select box with it. Otherwise,
             * hide it and enable the state text box.
             *
             * Also manipulates locale-specific opt-in fields.
             * TODO - move this outta here!
             *
             * @param {Object} locale
             * @param {Object} data
             * @param {Object} country
             */
            processStateInfo: function(locale, data, country) {
                // This is a fieldset, not a select
                stateSelect = $('.state').parent();
                var city = $('input#ctl00_city');
                // Replace the state dropdown contents
                if (data.length) {
                    $('.state').removeAttr('disabled').removeClass('off').addClass('on');
                    $('#ctl00_state').attr('disabled', 'disabled').addClass('off').removeClass('on');
                    var options = '';
                    for (var i = 0; i < data.length; i++) {
                        var key = data[i];
                        if (i == 0) {
                            options += '<option value=""';
                        } else {
                            options += '<option value="' + key + '"';
                        }
                        options += '>' + key + '</option>';
                    }
                    stateSelect.find('select').html(options);
                } else {
                    // Disable the select
                    $('.state').attr('disabled', 'disabled').addClass('off').removeClass('on');
                    // Enable the text box
                    $('#ctl00_state').removeAttr('disabled').removeClass('off').addClass('on');
                }
                switch (country) {
                case 'au':
                    $('.emailOptin input').attr('disabled', 'disabled').parent('div').addClass('off')
                    .filter('.au').removeClass('off').find('input').removeAttr('disabled');
                    $('.smsOptin input').attr('disabled', 'disabled').parent('div').addClass('off')
                    .filter('.au').removeClass('off').find('input').removeAttr('disabled');
                    city.css("width", 98);
                    break;
                default:
                    $('.emailOptin input').attr('disabled', 'disabled').parent('div').addClass('off')
                    .filter('.row').removeClass('off').find('input').removeAttr('disabled');
                    $('.smsOptin input').attr('disabled', 'disabled').parent('div').addClass('off')
                    .filter('.row').removeClass('off').find('input').removeAttr('disabled');
                    city.css("width", 98);
                    break;
                };
            },
            
            updateEventTypeSelect: function(value) {
                if (value === "") {value = "0";}
                
                var country = value === '0' || value ==='ROW' ? 'row': value.split('-')[1].toLowerCase();
                var locale = value === '0' || value ==='ROW' ? 'row': value;
                
                if (hashEventTypes && hashEventTypes[locale]) {
                    Smirnoff.Session.locale(locale);
                    //Smirnoff.UI.processStateInfo(locale, hashCountryStates[locale], country);
                } else {
                    /**
                     * Fetch states, put them into hashCountryStates
                     *
                     */
                    $.ajax({
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('ajax', 'true');
                        },
                        data: '',
                        dataType: 'json',
                        type: 'POST',
                        url: "/templates/events/retrieveCalendarEventsByCountry.aspx?locale=" + locale + "&rd=" + Math.floor(Math.random() * 100001),
                        success: function(data, ts) {
                            sel = $('#ctl00_eventDescType').children().remove().end();
                            
                            if (data.events && data.events.length) {
                                var arr = data.events;
                                
                                // Replace dropdown contents
                                var options = '';
                                for (var i = 0, l=arr.length; i < l; i++) {
                                    
                                    options += '<option value="' + arr[i][0] + '"';
                                    
                                    options += '>' + arr[i][1] + '</option>';
                                }
                                
                                sel.html(options).removeAttr('disabled');
                                
                                // Save data to cache
                                hashEventTypes[locale] = data.events;
                            } else {
                                // Disable the select
                                sel.attr('disabled', 'disabled').hide();
                                hashEventTypes[locale] = [];
                            }
                        },
                        error: function() {}
                    });
                }
                
            },
            
            showAgeGate: function() {
                Smirnoff.util.log.debug('showAgeGate()');
                
                Smirnoff.UI.Dialogs.ContentManager.show({
                    id: 'dialog_agegate',
                    url: "/dialogs/dialog_agegate.aspx"+"?rd=" + Math.floor(Math.random() * 100001),
                    containerClass: 'dialog_agegate',
                    width: 678,
                    showCloseButton: false,
                    afterLoad: function() {                            
                        Smirnoff.util.log.debug('Agegate afterLoad()');  

                        Smirnoff.util.pngFix($('#dialog_agegate .png'));

                        $('#flash-wrapper').html('<img src="/images/home/carouselAlt.png" alt="Smirnoff: Be There" width="684" height="385" />');
                        var gateway = new Gateway();

                        $("#agegate_register").unbind().bind("click", function(e) {
                            e.preventDefault();
                            Smirnoff.UI.RegistrationDialog.pageOne.show(true);
                            Smirnoff.Event.notify('age-gate-passed');
                        });
                        $("#agegate_login").unbind().bind("click",
                        function(e) {
                            e.preventDefault();
                            Smirnoff.UI.LoginDialog.show(true);
                            Smirnoff.Event.notify('age-gate-passed');
                        });
                    }

                });
               
                
                
                
            }
        };
    })();
    
    /**
     * Shared code for all modal dialogs
     */
    Smirnoff.UI.Dialogs = {
       
        /**
         * Show the login dialog if required.
         *
         * @param {Object} force Force the login dialog to show.
         */
        authenticate: function(force, msg) {
            Smirnoff.util.log.debug('authenticate('+force+', '+msg+')');
            
            var S = Smirnoff.Session;
            
            if (force !== true) var force = false;
            
            if (force === false && S.loggedIn() || S.loggedInToECM()) return true;
            
            Smirnoff.UI.LoginDialog.show(true, msg);
            return false;
        },

        bindForm: function(dialog, f) {
            Smirnoff.util.log.debug('Smirnoff.UI.Dialogs.bindForm()');  
            var MD = Smirnoff.UI.MomentDialog,
                D  = Smirnoff.UI.Dialogs;

            f
            .unbind('submit')
            .submit(function(e) {
                e.preventDefault();
                D.showLoader(f);

                // Attach login/register callbacks
                if (dialog.options.afterAuthentication) {
                    D.saveCallbacks(dialog.options.afterAuthentication, dialog.options.afterAuthentication);
                }

                Smirnoff.util.log.debug('bindForm.submit()');   
                    
                D.submitForm(dialog, f);
                return false;
            });
        },  
        
        /**
         * Clear form elements
         */
        resetForm: function(f) {
            var D = Smirnoff.UI.Dialogs;

            f[0].reset();
            D.hidePreviews(f);
            D.resetFileInputs(f);
        },

        resetFileInputs: function(f) {
            f.find('.faux-file-text').html('');
        },
        
        
        submitForm: function(dialog, f) {
            var MD = Smirnoff.UI.MomentDialog, 
                D = Smirnoff.UI.Dialogs;
                
            D.disableButtons(f);
            
            // Submit the contest
            $.ajax({
                url: dialog.options.submitUrl,
                dataType: 'json',
                type: 'post',
                data: f.serialize(),
                beforeSend: function(xhr){
                    xhr.setRequestHeader('ajax', 'true');
                },
                complete: function(data, status){
                    D.hideLoader(f);
                    
                    // Re-enable submission
                    D.enableButtons(f);
                },
                success: function(data, status){
                    Smirnoff.util.log.debug('bindForm.success()');
                    // data.loggedIn indicates a valid user session
                    // data.success indicates whether the form is valid
                    if (data.loggedin === false && dialog.options.requireLogin === true) {
                        Smirnoff.util.log.debug('bindForm.submit(): FAIL: not logged in');
                        
                        // User is not logged in, show login dialog
                        D.authenticate(true, data.errors);
                    }
                    else {
                        if (data.success && data.success === true) {
                            Smirnoff.util.log.debug('bindForm.submit(): AOK!');
                            
                            // Success: execute the callback
                            if (dialog.options.submitSuccess) {
                                dialog.options.submitSuccess();
                            }
                        }
                        else {
                            f.find('.status_message').html(data.errors).show();
                            
                            if (dialog.options.submitError) {
                                dialog.options.submitError();
                            }
                        }
                    }
                },
                error: function(data, status, e){
                
                }
            });
        },
        

        /**
         * Close all dialogs, since only one should be open at a time.
         */
        close: function() {
            Smirnoff.util.log.debug('Smirnoff.UI.Dialogs.close()');
            var dialog = $.modal.impl.dialog;
            if (dialog.container) dialog.container.hide();
            if (dialog.overlay) dialog.overlay.hide();
            if (dialog.iframe) dialog.iframe.hide();
            if (Smirnoff.Session.needsAgeGate()) {
                Smirnoff.UI.showAgeGate();
            }
        },
        fadeOut: function() {
            // TODO - add animation here
            //this.close();
            var dialog = $.modal.impl.dialog,
            params = 'medium';
            // TODO - make this really animate
            if (dialog.container) dialog.container.hide();
            if (dialog.overlay) dialog.overlay.hide();
            if (dialog.iframe) dialog.iframe.hide();
            if (Smirnoff.Session.needsAgeGate()) {
                Smirnoff.UI.showAgeGate();
            }
        },
        hasErrors: function(data) {
            return ! data.success;
        },
        /**
         * Highlight fields with errors
         *
         * @param {Object} form
         * @param {Object} data
         * @returns {String} message Concatenated error text
         */
        displayErrors: function(form, data) {
            var error,
            errors = data.errors,
            message = '';
            
            switch (errors.constructor) {
                case Object:
                    for (fieldName in errors) {
                        var el = $('#' + fieldName);
                        /*
                         * Special case; this ID is the hidden field associated with the State select
                         */
                        if (fieldName === 'ctl00_state') {
                            form.find('.state-wrapper').addClass('error-field')
                            /*
                         * IE doesn't support borders on <select>, so add border to container.
                         */
                        } else if (el.is('select')) {
                            el.parent().addClass('error-field');
                        }
                        else {
                            form.find("#" + fieldName + ',.' + fieldName).addClass("error-field");
                        }
                        message = message + ' ' + errors[fieldName];
                    }
                break;
                
                case Array:
                    message = errors[0];
                    form.find('.status_message').html(message);
                break;
                
                case String:
                    message = errors;
                    form.find('.status_message').html(message);
                break;
            }

            return message;
        },
    
        /**
         * Insert a thumbnail for the uploaded file
         *
         * @param {Object} src
         */
        previewImage: function(src, id) {
            Smirnoff.util.log.dump('previewImage()');
            var  D = Smirnoff.UI.Dialogs,
                 modal = D.getModal(),
                 
                 // find the preview element with a similar name
                 container = modal.find('#' + id.replace(/file/, 'preview'));
            
                 var img = container
                        .find('.preview').remove().end()
                        .append('<img class="preview" style="visibility: hidden" src="' + src + '" />').find('.preview');
                   
                 
            // When the thumbnail loads, resize it to fit the container
            img.unbind()
                .load(function() {
                    Smirnoff.util.log.debug('preview image load()');     
                    D.showPreview(container);
                });
        },
        showPreview: function(container) {
            Smirnoff.util.log.debug('showPreview()');
            
            var D = Smirnoff.UI.Dialogs,
                img = container.find('.preview'),
                newSize = Smirnoff.util.thumbDimensions(img.width(), img.height(), 133, 133);
                
            img.css({
                width: newSize.width,
                height: newSize.height,
                visibility: 'visible'
            });
            
            // Open the container. Make it just tall enough to fit the new image
            //  and some padding;
            container.animate({
                borderWidth: '1px',
                height: (newSize.height + 40) + 'px'
            }, {
                complete: function(){
                    D.hideLoader(container.parents('form:first'));
                }
            });
        },
        hidePreviews: function(form) {
            Smirnoff.util.log.debug('hidePreviews()');
        
            form.find('.uploadPreview').html('').css({
                borderWidth: '0px',
                height: '0px'
            });
        },
        

        /**
         * Swap out the Login/Registration callbacks so that we can do
         * something else after authentication
         */
        saveCallbacks: function(afterLogin, afterRegister) {
            var RD = Smirnoff.UI.RegistrationDialog,
                LD = Smirnoff.UI.LoginDialog,
                S = Smirnoff.Session;
            
            S.savedModalDialogCallbacks = {
                registration: RD.pageTwo.afterRegister,
                login: LD.afterLogin
            };
            RD.pageTwo.afterRegister = afterRegister;
            LD.afterLogin = afterLogin;
        },

        /**
         * Restore the default code that runs after registration/login.
         */
        restoreCallbacks: function() {
            var RD = Smirnoff.UI.RegistrationDialog,
                LD = Smirnoff.UI.LoginDialog,
                S = Smirnoff.Session;
                
            var saved = S.savedModalDialogCallbacks;
            if (!saved) { return; }

            RD.pageTwo.afterRegister = saved.registration;
            LD.afterLogin = saved.login;
        },
        
        /**
         * Populate hidden field with an image path
         * // TODO - rename this appropriately
         */
        swapFileUpload: function(sel, val) {
            if (!val) val = "";
            $('#' + sel.replace(/\-file/, '')).attr('value', val);
        },

        /**
         * Update fake file upload with file path
         * 
         * @param {Object} id
         * @param {Object} path
         */
        updateFileText: function(id, path) {
            $('#'+id).parent().find('.faux-file-text').html( path );
        },
        
        bindFileInputs: function(f) {
            var D = Smirnoff.UI.Dialogs;
                
            f.find('input[type=file]')
             .unbind('change')
                .each(function(i) {
                    var el = $(this);
                    var id = el.attr('id');
                    el.change(function(e) {
                        Smirnoff.util.log.debug('file input change() - id: '+id);  

                        D.uploadImage(f, id, {});
                    });
                })
                .end()
            .end();
        },


        uploadImage: function(form, fileFieldId, options, callback) {
            var D = Smirnoff.UI.Dialogs,
                el = $('#' + fileFieldId),
                path = el.attr('value');
                
            Smirnoff.util.log.debug('uploadImage()');

            if (el.hasClass('upload-in-progress')) {
                Smirnoff.util.log.debug('uploadImage()');
                return;
            }
                
            D.showLoader(form);
            el.addClass('upload-in-progress');

            $.ajaxFileUpload({
                type: "POST",
                url: "/dialogs/addMoment_Image.aspx" + "?rd=" + Math.floor(Math.random() * 100001),
                fileElementId: fileFieldId,
                dataType: 'json',
                complete: function() {
                    el.removeClass('upload-in-progress');
                    
                    // Notify the form that we're done with uploads
                    if (!D.uploadInProgress(form)) {
                        form.trigger('uploads-complete');
                    }
                    
                    // Fire custom callback
                    if (options.complete)
                      options.complete();
                },
                beforeSend: function(xhr) {
                    if (options.beforeSend)
                      options.beforeSend();
                },
                /*
                 * If upload goes ok, make another post to contest url
                 */
                success: function(data, status) {
                    Smirnoff.util.log.debug('D.uploadImage(): ajax success');
                    var D = Smirnoff.UI.Dialogs;
                          
                    // data.loggedIn indicates a valid user session
                    // data.success indicates whether the form is valid
                    if (data.success && data.success === true) {
                        Smirnoff.util.log.debug('D.uploadImage(): AOK!');
                        D.swapFileUpload(fileFieldId, data.imageid);
                        D.updateFileText(fileFieldId, path);
                        D.previewImage(data.imageid,  fileFieldId);
                        
                        if (callback) callback(data);
                    } else {
                        Smirnoff.util.log.debug('D.uploadImage(): FAIL - ' + data.success + ", " + typeof(data.success));
    
                        D.displayErrors(form, data);
                        D.hideLoader(form);
                    }
                    // Rebind the change() event, since it's being removed by ajaxFileUpload
                    D.bindFileInputs(form);
                },
                /*
                 * Show an error message
                 */
                error: function(data, status, e) {
                    Smirnoff.util.log.debug('D.uploadImage(): error: ' + e.message);
                    D.hideLoader(form);
                }
            });
        },

        uploadInProgress: function(f) {
            return f.find('input[type=file]').hasClass('upload-in-progress');
        },
        
        handleRedirect: function(json) {
            if (typeof json.redirect !== 'undefined' && URL_REGEX.test(json.redirect)) {
                $.reloadPage(json.redirect);
                return true;
            }
            
            return false;
            
        },
        
        getDialog: function() {
            if ($("#dialog").length < 1) {
                $(".wrap").append("<div id='dialog'></div>");
            }
            return $('#dialog');
        },
        getModal: function() {
            return $('#simplemodal-container');
        },
        showModal: function() {
            Smirnoff.util.log.debug('Smirnoff.UI.Dialogs.showModal()');
            var dialog = $.modal.impl.dialog;
            if (dialog.container) dialog.container.show();
            if (dialog.overlay) dialog.overlay.show();
            if (dialog.iframe) dialog.iframe.show();
            $.scrollTo('#simplemodal-container');
            $(window).trigger("resize.simplemodal");
        },

        showLoader: function(form, callback) {
            Smirnoff.util.log.debug('showLoader()');
            form.find('.spinner').fadeIn();
        },
        hideLoader: function(form, callback) {    
            Smirnoff.util.log.debug('hideLoader()'); 
            form.find('.spinner').fadeOut();
        },
        
        enableButtons: function(f) {
            f.find('button').attr('disabled', false);
        },
        
        disableButtons: function(f) {
            f.find('button').attr('disabled', true);
        } 
    };
    
    /**
     * Class representing a single page of content in the simplemodal dialog.
     * 
     * @param {Object} options
     * @constructor
     */
    Smirnoff.UI.DialogContentBlock = function(options) {
        var defaults = {cache: true, showCloseButton: true};
        this._loading = false;
        this._eventsBound = false;
        this.options = $.extend({}, defaults, options);
    };
    
    Smirnoff.UI.DialogContentBlock.prototype = {
        /**
         * Default ajax callback
         *
         * @param {Object} txt
         * @param {Object} status
         * @param {Object} xhr
         */
        afterLoad: function(txt, status, xhr) {
            Smirnoff.util.log.debug('DialogContentBlock.afterLoad()');
                
            
            // Run code for all instances
            this.loaded = true;
            this.element = $('#' + this.options.id);
            
            // Add a close button in each content block, since we hacked the 
            //  simplemodal plugin to not insert it. Also, this lets us put it
            //  where we want it.
            if (this.options.showCloseButton) {
                this.element.prepend('<a href="#" class="modalCloseImg modal_dialog_close" title="Close"></a>');
            }
            
            
            // Run instance-specific callback
            if (this.options.afterLoad) {
                this.options.afterLoad.apply(this, arguments);
            }
            
        },
        
        /**
         * Attach events that apply to every instance individually
         */
        bind: function() {
            // Only do it once
            if (this._eventsBound === true) { return; }
            
            this.bindButtons();

            this._eventsBound = true;
        },
        
        /**
         * Add button rollovers
         */
        bindButtons: function() {
            
            this.element.find('.buttonWrapper')
            // Prevent event duplication
            // TODO - make these attach only once per content dialog
            .unbind('mouseover')
            .unbind('mouseout')
            .unbind('mousedown')
            .unbind('mouseup')
            .hover(function() {
                $(this).addClass('hover');
            },
            function() {
                $(this).removeClass('hover');
            })
            .mousedown(function() {
                $(this).addClass('down');
            }).mouseup(function() {
                $(this).removeClass('down');
            });
        },
        
        getOption: function(optName) {
            return this.options[optName] || null;
        },

        showMsg: function(txt) {
            this.element.find('.status_message').html(txt);
        },
        hideMsg: function() {
            this.element.find('.status_message').hide();
        },

        /**
         * Load html via ajax
         */
        load: function(options, callback) {
            Smirnoff.util.log.debug('Smirnoff.UI.DialogContentBlock.load()');

            // 
            if (options.url !== this.options.url) {
                
            }

            var DCB = this;
            $.ajax({
                url: this.options.url,
                success: function(txt, status, xhr) {
                    Smirnoff.util.log.debug('Smirnoff.UI.DialogContentBlock.load() success!');
                    Smirnoff.UI.Dialogs.ContentManager.append(txt);
                        
                    // Run default and instance-specific callbacks 
                    DCB.afterLoad.apply(DCB, arguments);
                        
                    // Run passed-in callback
                    if (callback)
                        callback.apply(DCB, arguments);
                },
                error: function() {
                          
                }
            });
            
            
        },
        /**
         * Getter/setter to indicate AJAX in progress
         */
        loading: function() {
            if (typeof arguments[0] !== 'undefined') {
                this._loading = arguments[0];
            }
            return this._loading;
        },
        show: function() {
            // Clear messages
            if (this.options.beforeShow) this.options.beforeShow();
            this.element
                .removeClass('hide')
                .show();

            if (this.options.afterShow) this.options.afterShow();
        },

        /**
         * Return true if existing config is different from passed one
         */
        configChanged: function(o) {
            // No comparison if we're comparing an object to a string
            if (o.constructor !== this.options.constructor) {
                return false;
            }
            
            for (var prop in this.options) {
                if (this.options[prop] != o[prop]) {
                    return true;
                }
            };
            
            return false;
        }
    };
    
    /**
     * Singleton to manage DialogContentBlock instances
     * 
     * @constructor
     */
    Smirnoff.UI.Dialogs.ContentManager = (function() {
        var _items = {},
            _modal = {},
            _active = null;
        return {
            /**
             * Insert AJAX-loaded content. Since simplemodal moves
             *  content from one div to another, we determine the
             *  target container based on whether the modal is loaded.
             *
             * @param {Object} html
             */
            append: function(html) {
                Smirnoff.util.log.debug('CM.append()');
                if ($('#simplemodal-container').length > 0) {
                    Smirnoff.UI.Dialogs.getModal().find('.simplemodal-inner').append(html);
                } 
                    
                else {
                    Smirnoff.UI.Dialogs.getDialog().append(html);
                }
                    
            },
            /**
             * Create a new DialogContentBlock and store it
             *
             * @param {Object} Options
             */
            add: function(options) {
                Smirnoff.util.log.debug('add()');
                
                if (!options.id) 
                    throw ("Smirnoff.UI.Dialogs.contentManager.add() requires an ID");

                if (_items[options.id]) 
                    return;

                _items[options.id] = new Smirnoff.UI.DialogContentBlock(options);
    
                return _items[options.id];
            },
            /**
             * Attach events that apply to the dialog object as a whole (regardless of content)
             */
            bind: function() {
                var D = Smirnoff.UI.Dialogs;
                
                D.getModal()
                            .bind('click',
                            function(e) {
                                
                                var target = $(e.target);
                                
                                if (target.is('.modalCloseImg')) {
                                    e.preventDefault();
                                    
                                    D.close();
                                    D.ContentManager.clearCurrentForm();        
                                    D.restoreCallbacks();
                                    
                                    return false;                           
                                }

                                
                            });
            },
            /**
             * See if the agegate or another dialog has created the modal already
             */
            modalLoaded: function() {
                return ($('#simplemodal-container').length > 0);
            },
            clearCurrentForm: function() {
                Smirnoff.util.log.dump('clearCurrentForm()');   
                _active.element.find('form').each(function() {
                    this.reset();
                });
            },

            destroyItem: function(id) {
                _items[id].element.remove();
                delete _items[id];
            },

            items: function() {
                return _items;
            },

            getItem: function(id) {
                return _items[id];
            },

            /**
             * Hide all containers with class 'modal-content'.
             *  
             * @param {Object} id If specified, a single container NOT to hide.
             */
            hideAll: function(id) {   
                Smirnoff.UI.Dialogs.getModal()
                    .find('.modal-content').each(function() {
                        // Adding the .hide class moves the element offscreen;
                        //required for IE6 to prevent the showing of form elements
                        $(this)
                              .addClass('hide')
                              .end()                        
    
                        // Force re-render in IE
                        .toggleClass('junk')
                        .find('.buttonWrapper')
                           .toggleClass('junk');
                           
                    });
            },
            /**
             * Display a string in the current form's message area
             * 
             * @param {Object} msg
             */
            showMsg: function(msg) {
                _active.element.find('.status_message').html(msg).fadeIn();
            },
            
            /**
             * Hide the message area in ALL loaded forms
             */
            hideMsg: function() {
                var modal = Smirnoff.UI.Dialogs.getModal();
                
                modal.find('.status_message').fadeOut();
            },

            /**
             * Call the jQuery SimpleModal plugin
             */
            loadModal: function(itemId) {                   
                // Invoke the plugin
                _modal = _items[itemId].element.modal({
                    overlayCss: {
                        'height': '100000px'
                    },
                    onShow: function() {
                        if (Smirnoff.util.isIE6) {
                            $('#simplemodal-overlay .png').pngFix({
                                blankgif: '/images/blank.gif'
                            });
                        }
                    },
                    onClose: function() {
                        Smirnoff.UI.Dialogs.close();
                        return false;
                    },
                    unbindOnClose: false
                });
                    
                return _modal;
            },
            /**
             * Because of inconsistent CSS, the "Close" button on the simplemodal dialog needs a custom class added
             *
             * @param {Object} itemId
             */
            setCloseClass: function(itemId) {
                var link = Smirnoff.UI.Dialogs.getModal().find('.modalCloseImg');
                // Add this again, since agegate may have needed it removed
                link.addClass('modal_dialog_close');
                // Find DialogContentBlock with the target itemId
                for (id in _items) {
                    var className;
                    if (id === itemId) {
                        className = _items[id].getOption('closeClass');
                        link.addClass(className);
                    }
                    else {
                        className = _items[id].getOption('closeClass');
                        link.removeClass(className);
                    }
                }
            },
            /**
             * Toggle dialog-specific classes on the simplemodal container. This gives
             * us the ability to target the css on each dialog.
             *
             * @param {Object} itemId
             */
            setContainerClass: function(itemId) {
                var el = Smirnoff.UI.Dialogs.getModal();
                // Find DialogContentBlock with the target itemId
                for (id in _items) {
                    var className;
                    if (id === itemId) {
                        className = _items[id].getOption('containerClass');
                        el.addClass(className);
                    }
                    else {
                        className = _items[id].getOption('containerClass');
                        el.removeClass(className);
                    }
                }
            },
            
            /**
             * Allows for variable-width dialogs
             * 
             * @param {Object} itemId
             */
            setContainerWidth: function(itemId) {
                var el = Smirnoff.UI.Dialogs.getModal(),
                    block = _items[itemId],
                    width = (block.options.width ? block.options.width + 'px' : null);
                    
                    el.css('width', width);
            },
            
            
            /**
             * Force the close element to visible, since we may have hidden it for the agegate
             */
            showCloseEl: function() {
                var link = Smirnoff.UI.Dialogs.getModal().find('.modal_dialog_close');
                link.show();
            },
            /**
             * Show the block, loading it if neccessary
             *
             * @param {Object} o ID of the block, or config object
             */
            show: function(o) {
                var itemId,
                item,
                CM = this,
                D = Smirnoff.UI.Dialogs;

                if (o.constructor === String)
                    itemId = o;
                else
                    itemId = o.id;
                    
                item = _items[itemId];
            
                
                // Ignore this call if AJAX is in progress
                if (item && item.loading()) {
                      
                    Smirnoff.util.log.debug('load in progress, returning.');
                    return;
                }

                // If we're injecting a different config, wipe it out and re-load
                if (item && item.loaded && item.configChanged(o)) {
                    CM.destroyItem(itemId);
                }
                
                // Instantsiate it
                if (!_items[itemId]) {
                    CM.add(o);
                    item = _items[itemId];
                }

                // Load it, then call this again
                if (!item.loaded) {
                    Smirnoff.util.log.debug('content item not loaded, loading '+itemId+'...');
                        
                    // Set a flag so we don't load it twice at the same time
                    _items[itemId].loading(true);
                    _items[itemId].load(o, function(txt, status, xhr) {
                          
                            
                        Smirnoff.util.log.debug('ajax success in CM.show()');
                        // Set completion flag
                        _items[itemId].loading(false);
                        
                        return CM.show(itemId);
                    });
                    return;
                }
                
                //  
                
                // See if the agegate or another dialog has created the modal already
                // Create the modal only once
                if (!CM.modalLoaded()) {
                    Smirnoff.util.log.debug('modal not loaded, loading '+itemId+'...');
                    _modal = CM.loadModal(itemId);

                    // Attach events that apply to the whole dialog
                    CM.bind();
                }

                // Toggle pages
                Smirnoff.util.log.debug('toggling page '+itemId+'...');
                _active = _items[itemId];
                
                // Attach block-specific events
                _active.bind();
                
                CM.hideAll();
                CM.setContainerClass(itemId);
                CM.setContainerWidth(itemId);
                CM.showCloseEl();
                _active.show();
                D.showModal();
            }
        };
    })();
    /*
     * Login modal dialog
     */
    Smirnoff.UI.LoginDialog = (function() {
        var D = Smirnoff.UI.Dialogs,
        LD = Smirnoff.UI.LoginDialog;
        return {
            error: function() {
                $('#login_page1 .spinner').fadeOut();
                $('#login_page1 .status_message').html('There was an error with our server, please try again later.').fadeIn();
                //cms id 4294967624
            },
            isValid: function(data) {
                /* client side validation */
                return true;
            },
            loaded: function() {
                var D = Smirnoff.UI.Dialogs,
                LD = Smirnoff.UI.LoginDialog;
                // coremetrics
                cmCreatePageviewTag(cm_pageid + ": Login Form Loaded", cm_categoryid, cm_search);
                cmCreateConversionEventTag(cm_pageid + ": Login Form", 1);
                // setup
                var dialog = D.getModal();
                // Add events
                $('form#login_page1').submit(function(e) {
                    e.preventDefault();
                    LD.submit();
                    return false;
                })
                .find('.register-link').click(function(e) {
                    e.preventDefault();
                    Smirnoff.UI.RegistrationDialog.pageOne.show(false);
                });
            },
            reset: function() {
                Smirnoff.UI.LoginDialog.regdirectLocale = false;
            },
            show: function(isAgeGateOpen, msg) {
                Smirnoff.util.log.debug('LoginDialog.show()');    
                var LD = Smirnoff.UI.LoginDialog,
                D = Smirnoff.UI.Dialogs;
                LD.reset();
                
                Smirnoff.SecondaryDialogs.hideOverlays();
                
                Smirnoff.UI.Dialogs.ContentManager.show({
                    id: 'modal-content-login',
                    url: LOGIN_PAGE_URL + "&rd=" + Math.floor(Math.random() * 100001),
                    closeClass: 'dialog_login_close',
                    containerClass: 'dialog_login_container',
                    onClose: function() {
                        if (Smirnoff.Session.needsAgeGate())
                        Smirnoff.UI.showAgeGate();
                    },
                    afterLoad: function() { 
                        Smirnoff.util.log.debug('ContentManager.show.afterLoad()');
                        if (msg) $('#modal-content-login').find('.status_message').html(msg);
                        LD.loaded();
                    }
                });
            },
            submit: function() {
                var LD = Smirnoff.UI.LoginDialog,
                    D = Smirnoff.UI.Dialogs,
                    dialog = D.getModal(),
                    form = dialog.find('form#login_page1');
                    
                dialog.find('.spinner').fadeIn();
                D.disableButtons(form);
                
                var values = form.find('input,select').serializeArray();
                if (LD.isValid(values)) {
                    $.ajax({
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('ajax', 'true');
                        },
                        complete: function() {
                            D.enableButtons(form);
                        },
                        type: 'POST',
                        url: LOGIN_URL + "&rd=" + Math.floor(Math.random() * 100001),
                        data: values,
                        dataType: 'json',
                        success: LD.success,
                        error: LD.error
                    });
                } else {
                    D.enableButtons(form);
                }
                return false;
            },
            success: function(data) {
                var dialog = D.getModal(),
                form = dialog.find('#register_page2 form'),
                LD = Smirnoff.UI.LoginDialog;
                dialog.find('.spinner').fadeOut();
                var message;
                
                if (D.hasErrors(data)) {
                    message = D.displayErrors(form, data);
                    message = message ? message: 'Wrong username/password combination, please, try again.';
                    //cms id=4294967623
                    dialog.find('.status_message').html(message).show();
                } else {
                    // coremetrics
                    cmCreateConversionEventTag(cm_pageid + ': Login Form', 2);
                    LD.afterLogin(data);
                    Smirnoff.Session.afterLogin(data);
                }
            },
            /**
             * Default login Callback. This method can be temporarily
             * overridden by dialogs that need have different functionality after
             * logging in.
             */
            afterLogin: function(data) {
                Smirnoff.util.log.debug('LoginDialog.afterLogin()');
                var LD = Smirnoff.UI.LoginDialog,
                D = Smirnoff.UI.Dialogs,
                CM = Smirnoff.UI.Dialogs.ContentManager;
                if (D.hasErrors(data)) return;
                CM.clearCurrentForm();
                
                CM.show({
                    id: 'modal-content-login-complete',
                    url: LOGIN_PAGETWO_URL + "&rd=" + Math.floor(Math.random() * 100001),
                    closeClass: 'dialog_login_close',
                    containerClass: 'dialog_login_container',
                    afterLoad: function() {
                        Smirnoff.util.log.debug('ContentManager.show.afterLoad()');
                        LD.pageTwoDisplay();
                    }
                });
            },
            pageTwoDisplay: function() {
                var LD = Smirnoff.UI.LoginDialog,
                D = Smirnoff.UI.Dialogs;
                var dialog = D.getModal();
                cmCreatePageviewTag(cm_pageid + ': Login Form Completed', cm_categoryid, cm_search);
                $(window).trigger("resize.simplemodal");
                setTimeout(function() {
                    Smirnoff.UI.Dialogs.fadeOut();
                },
                3000);
            }
        };
    })();
    Smirnoff.UI.RegistrationDialog = (function() {
        var D = Smirnoff.UI.Dialogs,
        RD = Smirnoff.UI.RegistrationDialog;
        return {
            ajaxComplete: function() {
                $('.dialog_register .spinner').fadeOut();
                $(window).trigger('resize.simplemodal');
            },
            /*
             * Encapsulates the "Join Us" registration page in the modal overlay
             */
            pageOne: {
                error: function() {
                    RD.ajaxComplete();
                    $('.dialog_register .status_message').html('There was an error with our server, please try again later.').fadeIn();
                },
                isValid: function(values) {
                    /* for client side validation */
                    return true;
                },
                loaded: function() {
                    var RD = Smirnoff.UI.RegistrationDialog;
                    //coremetrics
                    cmCreatePageviewTag(cm_pageid + ": Registration Form Page 1", cm_categoryid, cm_search);
                    cmCreateConversionEventTag(cm_pageid + ": Registration Form Page 1", 1);
                    var dialog = D.getDialog();
                    $('form#register_page1').submit(RD.pageOne.submit)
                        .find('input[name=city], input[name=zip], input[name=state]')
                        .defaultAsLabel();
                    
                    var country = $('#country').change(Smirnoff.UI.updateStateSelect);
                    Smirnoff.UI.updateStateSelect.apply(country);
                },
                /**
                 * Set birthdate fields with values from cookie
                 */
                setDOB: function() {
                    Smirnoff.util.log.debug('Smirnoff.UI.RegistrationDialog.pageOne.setDOB()'); 
                    var dialog = D.getModal();
                    if (parseInt($.cookie('gw_mo')) >= 0) {
                        dialog.find('#ctl00_dob_m option[value=' + $.cookie('gw_mo') + ']').attr('selected', 'selected');
                    }
                    if (parseInt($.cookie('gw_dy')) >= 0) {
                        dialog.find('#ctl00_dob_d option[value=' + $.cookie('gw_dy') + ']').attr('selected', 'selected');
                    }
                    if (parseInt($.cookie('gw_yr')) >= 0) {
                        dialog.find('#ctl00_dob_y option[value=' + $.cookie('gw_yr') + ']').attr('selected', 'selected');
                    }
                },
                show: function(isAgeGateOpen) {
                    Smirnoff.util.log.debug('RegistrationDialog.pageOne.show()');
                    var D = Smirnoff.UI.Dialogs,
                    RD = Smirnoff.UI.RegistrationDialog;
                    Smirnoff.util.log.debug('Smirnoff.UI.RegistrationDialog.pageOne.show()');
                    RD.reset();
                    
                    Smirnoff.SecondaryDialogs.hideOverlays()
                    
                    Smirnoff.UI.Dialogs.ContentManager.show({
                        id: 'modal-content-register',
                        url: REGISTRATION_PAGEONE_URL + "&rd=" + Math.floor(Math.random() * 100001),
                        containerClass: 'dialog_register_container',
                        onClose: function() {
                            if (Smirnoff.Session.needsAgeGate())
                            Smirnoff.UI.showAgeGate();
                        },
                        afterLoad: function() {
                            RD.pageOne.loaded();
                        },
                        afterShow: function() {
                            RD.pageOne.setDOB();
                        }
                    });
                    return false;
                },
                submit: function(e) {
                    Smirnoff.util.log.debug('RegistrationDialog.pageOne.submit()');
                    var dialog = D.getModal(),
                    form = dialog.find('form#register_page1'),
                    RD = Smirnoff.UI.RegistrationDialog;
                    form.find('.error-field').removeClass('error-field');
                    dialog.find('.status_message').hide().empty();
                    dialog.find('.spinner').fadeIn();
                    D.disableButtons(form);
                    
                    var values = form.find('input,select').serializeArray();
                    if (RD.pageOne.isValid(values)) {
                        $.ajax({
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('ajax', 'true');
                            },
                            type: 'POST',
                            url: REGISTRATION_URL + "&rd=" + Math.floor(Math.random() * 100001),
                            data: values,
                            dataType: 'json',
                            complete: function() {
                                D.enableButtons(form);
                            },
                            success: RD.pageOne.success,
                            error: RD.pageOne.error
                        });
                    }
                    return false;
                },
                success: function(data) {
                    Smirnoff.util.log.debug('RegistrationDialog.pageOne.success()');
                    var RD = Smirnoff.UI.RegistrationDialog;
                    RD.ajaxComplete();
                    var dialog = D.getModal();
                    var form = dialog.find('form#register_page1');
                    var locale = form.find('#country').attr('value');
                    var message;
                    
                    if (D.hasErrors(data)) {
                        if (D.handleRedirect(data)) {return;}
                        
                        message = D.displayErrors(form, data);
                        dialog.find('.status_message').html(message).show();
                    }
                    else {
                        Smirnoff.UI.Dialogs.ContentManager.show({
                            id: 'modal-content-register-detail',
                            url: REGISTRATION_PAGETWO_URL + '?locale=' + locale + "&rd=" + Math.floor(Math.random() * 100001),
                            closeClass: 'dialog_register_close',
                            containerClass: 'dialog_register_container',
                            afterLoad: function() {
                                Smirnoff.util.log.debug('ContentManager.show.afterLoad()');
                                RD.pageTwo.loaded();
                                // coremetrics
                                cmCreatePageviewTag(cm_pageid + ": Registration Form Page 2", cm_categoryid, cm_search);
                                cmCreateConversionEventTag(cm_pageid + ": Registration Form Page 2", 1);
                            }
                        });
                        Smirnoff.Session.afterLogin(data);
                        $('#pageball_page1,#pageball_page2').toggleClass('pageball_hidden');
                        // coremetrics
                        cmCreateConversionEventTag(cm_pageid + ": Registration Form Page 1", 2);
                    }
                }
            },
            /*
             * Registration page 2, "Personalize Your Experience"
             */
            pageTwo: {
                error: function() {
                    RD.ajaxComplete();
                    $('.dialog_register .status_message').html('There was an error with our server, please try again later.').fadeIn();
                },
                isValid: function(values) {
                    /* for client side validation */
                    return true;
                },
                loaded: function() {
                    var RD = Smirnoff.UI.RegistrationDialog,
                    dialog = D.getModal();
                    RD.pageTwo.isLoaded = true;
                    dialog.find('form#register_page2').submit(RD.pageTwo.submit);
                },
                submit: function(e) {
                    Smirnoff.util.log.debug('RegistrationDialog.pageTwo.submit()');
                    var dialog = D.getModal(),
                    form = dialog.find('form#register_page2')
                    RD = Smirnoff.UI.RegistrationDialog;
                    form.find('.error-field').removeClass('error-field');
                    dialog.find('.status_message').hide().empty();
                    dialog.find('.spinner').fadeIn();
                    D.disableButtons(form);
                    
                    var values = form.find('input,select').serializeArray();
                    if (RD.pageTwo.isValid(values)) {
                        $.ajax({
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('ajax', 'true');
                            },
                            complete: function() {
                                D.enableButtons(form);
                            },
                            type: 'POST',
                            url: REGISTRATION_URL + "&rd=" + Math.floor(Math.random() * 100001),
                            data: values,
                            dataType: 'json',
                            success: function(data) {
                                RD.data = data;
                                RD.pageTwo.success();
                            },
                            error: RD.pageTwo.error
                        });
                    }
                    return false;
                },
                success: function() {
                    Smirnoff.util.log.debug('RegistrationDialog.pageTwo.submit()');
                    var RD = Smirnoff.UI.RegistrationDialog,
                    CM = Smirnoff.UI.Dialogs.ContentManager,
                    data = RD.data;
                    RD.ajaxComplete();
                    var dialog = D.getModal();
                    var form = dialog.find('#register_page2 form');
                    var message;
                    if (D.hasErrors(data)) {
                        message = D.displayErrors(form, data);
                        dialog.find('.status_message').html(message).show();
                    } else {
                        // coremetrics
                        cmCreateConversionEventTag(cm_pageid + ": Registration Form Page 2", 2);
                        cmCreatePageviewTag(cm_pageid, cm_categoryid);
                        CM.clearCurrentForm();
                    }
                    RD.pageTwo.afterRegister();
                },
                /**
                 * Default registration page 2 Callback. This method can be temporarily
                 * overridden by dialogs that need have different functionality after
                 * registering.
                 */
                afterRegister: function() {
                    Smirnoff.util.log.debug('RegistrationDialog.pageTwo.afterRegister()');
                    var RD = Smirnoff.UI.RegistrationDialog,
                    CM = Smirnoff.UI.Dialogs.ContentManager,
                    D = Smirnoff.UI.Dialogs;
                    if (D.hasErrors(RD.data)) return;
                    CM.clearCurrentForm();
                    Smirnoff.UI.Dialogs.ContentManager.show({
                        id: 'modal-content-register-thanks',
                        url: REGISTRATION_PAGETHREE_URL + '?&rd=' + Math.floor(Math.random() * 100001),
                        closeClass: 'dialog_register_close',
                        containerClass: 'dialog_register_container',
                        afterLoad: function() {
                            Smirnoff.util.log.debug('ContentManager.show.afterLoad()');
                            setTimeout(function() {
                                Smirnoff.UI.Dialogs.fadeOut();
                            },
                            3000);
                        }
                    });
                }
            },
            redirectLocale: false,
            reset: function() {
                var RD = Smirnoff.UI.RegistrationDialog;
                RD.redirectLocale = false;
                RD.pageTwo.isLoaded = false;
            }
        };
    })();
    
    Smirnoff.UI.Carousel = (function() {
        return {
            init: function() {
                var C = Smirnoff.UI.Carousel;
                   
                // Inject some html elements before creating carousel object
                var browserUl = $('#browser > ul')
                    .wrap('<div class="lg_mask"></div>')
                    .addClass('lg_content');
                
                if (browserUl.length) {   
                    var mask = browserUl.parent();
                    var next = mask
                                .after('<span class="next nav"><a href="#" class="lg_next nav"><span>Next</span></a></span>')
                                .siblings('.next');
                    var prev = mask
                                .before('<span class="prev nav"><a href="#" class="lg_prev nav"><span>Previous</span></a></span>')
                                .siblings('.prev');
                    
                    // make the first element the second before starting the carousel
                    browserUl.prepend(browserUl.find('li:last'));
                    browserUl.carousel({
                        next: next,
                        previous: prev,
                        onCreate: C.create,
                        onNext: C.select,
                        onPrevious: C.select
                    });
                    
                    if (Smirnoff.util.isIE6) {
                        $('div#browser span.nav').pngFix({blankgif:'/images/blank.gif'});
                    }
                }
            },
            
            create: function(e,carousel) {
                var C = Smirnoff.UI.Carousel;
                
                carousel.items.each(C.itemSetup);
                C.select(null, carousel);
            },
            itemSetup: function() {
                var C = Smirnoff.UI.Carousel;
                var li = $(this);
                var div = li.wrapInner(document.createElement('div'));
                $(':first', div).addClass('lg_pane_content');
                li.css('opacity', 0.5).addClass('lg_pane');
            },
            select: function(e,carousel) {
                var C = Smirnoff.UI.Carousel;
                
                if (e) {
                    e.preventDefault();
                    e.stopPropagation();
                }
                carousel.items.filter('.current').fadeTo('normal', 0.5).removeClass('current');
                var item = $(carousel.items[carousel.position + 1]);
                item.fadeTo('normal', 1).addClass('current');
            }

        };
    })();
    
    /**
     * Contains shared code for nonmodal dialogs (homepage reg, add event, etc) 
     */
    Smirnoff.SecondaryDialogs = {
        show: function(overlay, storyRollover, callback) {
            Smirnoff.util.log.debug('show()');
            
            overlay.el.css('display', '');
            
            var styles = {
                opacity: 1
            };
            var duration = 500;
            $('.momentsText').attr('disabled', 'disabled');
            
            if(storyRollover) {
                storyRollover.hide();
            }
            
            overlay.el.addClass('active');
            
            /*
             * Prevent z-index issues in IE6 by hiding the <select>s
             */
            if (Smirnoff.util.isIE6) {
                $('#submitEvent select').css({
                    visibility: 'hidden'
                });
            }
            
            overlay.animate(styles, duration,
            function() {
                Smirnoff.util.log.debug('show() animation callback');
                $('.buttonWrapper').hover(function() {
                    $(this).addClass('hover');
                },
                function() {
                    $(this).removeClass('hover');
                })
                .mousedown(function() {
                    $(this).addClass('down');
                }).mouseup(function() {
                    $(this).removeClass('down');
                })
                .append('<span class="cap leftCap"></span><span class="cap rightCap"></span>')
                .find('button')
                .focus(function() {
                    $(this).parent().addClass('hover');
                })
                .blur(function() {
                    $(this).parent().removeClass('hover');
                });

                Smirnoff.util.log.debug('before show() pasted callback');
                if (callback) {callback();}
                Smirnoff.util.log.debug('after show() pasted callback');
            });  
        },
        
        hide: function(overlay, storyRollover, callback) {
            Smirnoff.util.log.debug('hide()');
            
            /*
             * Re-display <selects> for events pages
             */
            if (Smirnoff.util.isIE6) {
                $('#submitEvent select').css({
                    visibility: 'visible'
                });
            }
                
            var styles = {
                opacity: 0
            };
            var duration = 300;
            $('.momentsText').removeAttr('disabled');
            
            overlay.animate(styles, duration, function() {
                Smirnoff.util.log.debug('before hide() animate callback');
                
                overlay.el.css('display', 'none');

                if (typeof callback !== 'undefined') {
                    try {
                        callback();                     
                    }
                    catch (e) {
                        
                    }

                }
                
                Smirnoff.util.log.debug('after hide() animate callback');    
            });
            return false;
        },
        
        countryChange: function() {
            Smirnoff.util.log.debug('countryChange()');
            var value = $(this).attr('value');
            var country = value === '0' ? 'row': value.split('-')[1].substring(0, 2).toLowerCase();
            var state,
            zip;
            
        
            switch (country) {
            case 'au':
                
                $('.state').attr('disabled', 'disabled').addClass('off')
                    .filter('.au').removeAttr('disabled').removeClass('off');
                    
                $('.zip').attr('disabled', 'disabled').addClass('off')
                    .filter('.row').removeAttr('disabled').removeClass('off');
                    
                $('.emailOptin input').attr('disabled', 'disabled').parent('div').addClass('off')
                    .filter('.au').removeClass('off').find('input').removeAttr('disabled');
                    
                $('.smsOptin input').attr('disabled', 'disabled').parent('div').addClass('off')
                    .filter('.au').removeClass('off').find('input').removeAttr('disabled');
                break;
            case 'us':
                $('.state').attr('disabled', 'disabled').addClass('off')
                    .filter('.us').removeAttr('disabled').removeClass('off');
                $('.zip').attr('disabled', 'disabled').addClass('off')
                    .filter('.us').removeAttr('disabled').removeClass('off');
                $('.emailOptin input').attr('disabled', 'disabled').parent('div').addClass('off')
                    .filter('.row').removeClass('off').find('input').removeAttr('disabled');
                $('.smsOptin input').attr('disabled', 'disabled').parent('div').addClass('off')
                    .filter('.row').removeClass('off').find('input').removeAttr('disabled');
                break;
            case 'ca':
                $('.state').attr('disabled', 'disabled').addClass('off')
                    .filter('.ca').removeAttr('disabled').removeClass('off');
                $('.zip').attr('disabled', 'disabled').addClass('off')
                    .filter('.row').removeAttr('disabled').removeClass('off');
                $('.emailOptin input').attr('disabled', 'disabled').parent('div').addClass('off')
                    .filter('.row').removeClass('off').find('input').removeAttr('disabled');
                $('.smsOptin input').attr('disabled', 'disabled').parent('div').addClass('off')
                    .filter('.row').removeClass('off').find('input').removeAttr('disabled');
                break;
            default:
                $('.state').attr('disabled', 'disabled').addClass('off')
                    .filter('.row').removeAttr('disabled').removeClass('off');
                $('.zip').attr('disabled', 'disabled').addClass('off')
                    .filter('.row').removeAttr('disabled').removeClass('off');
                $('.emailOptin input').attr('disabled', 'disabled').parent('div').addClass('off')
                    .filter('.row').removeClass('off').find('input').removeAttr('disabled');
                $('.smsOptin input').attr('disabled', 'disabled').parent('div').addClass('off')
                    .filter('.row').removeClass('off').find('input').removeAttr('disabled');
                break;
            }
        },
        
        hideOverlays: function() {
            var SD = Smirnoff.SecondaryDialogs,
                sr = (typeof SD.storyRollover === 'undefined' ? null : SD.storyRollover);
            
            if (!SD.overlay) {
                return;
            }
            
            Smirnoff.SecondaryDialogs.overlay.hide(SD.overlay, sr, null);
        },
        
        /**
         * Display an error message
         * 
         * @param {Object} overlay The overlay object
         * @param {Object} data The XHR response object
         * @param {Boolean} showSecondaryError Whether to show the errors in red at the bottom of the dialog.
         */
        loadErrors: function(overlay, data, showSecondaryError) {
            Smirnoff.util.log.debug('loadErrors()');

            overlay.el.addClass('errors').find('.error').removeClass('error');
            var message = '';
            var errors = data.errors;
            var error,
                els,
                errorMessage = overlay.el.find('.errorMessage');
            
            if (data.errors.constructor === String) {
                message = data.errors;
            } else if (data.errors.constructor == Array && data.errors.length > 0) {
                message = data.errors[0];
            } else {
                for (error in errors) {
                    var els = overlay.el.find('#' + error + ', label[for=' + error + ']').addClass('error');
                    if (els.length === 0) {
                        errorMessage.text(errors[error]);
                    }
                    message = message + ' ' + errors[error];
                }
            }
            
            /*
             * TODO - Standardize/genericize this
             */
            if (showSecondaryError === true) {
                $('#display-reg-errors').html(message).show();
            } else {
                errorMessage.text(message);
            }
        },
        
        setupRegisterDialog: function(overlay, register, login) {
            Smirnoff.util.log.debug('setupRegisterDialog()');
            
            var SD = Smirnoff.SecondaryDialogs;
            
            cmCreatePageviewTag(cm_pageid + ": Registration Form", cm_categoryid, cm_search);
            cmCreateConversionEventTag(cm_pageid + ": Registration Form", 1);
            
            overlay.el.find('.member a').click(SD.loginDialog.loadPanel);
            
            var m = $.cookie('gw_mo'),
                d = $.cookie('gw_dy'),
                y = $.cookie('gw_yr');
            overlay.el.find('#ctl00_dob_m option[value=' + m + ']').attr('selected', 'selected');
            overlay.el.find('#ctl00_dob_d option[value=' + d + ']').attr('selected', 'selected');
            overlay.el.find('#ctl00_dob_y option[value=' + y + ']').attr('selected', 'selected');
            
            overlay.el.find('button.cancel').click(function(e) {
                overlay.hide();
                return false;
            });
            
            overlay.setupForm(REGISTRATION_URL + "&rd=" + Math.floor(Math.random() * 100001), register.error, register.success);
            
            var country = overlay.el.find('#country').change(register.countryChange);
            register.countryChange.apply(country);
            
            overlay.setupEdit();
        },
        
        showEventDialog: function(addEventDialog) {
            var SD = Smirnoff.SecondaryDialogs;
            
            if (check_login()) {
                addEventDialog.loadPanel();
            } else {
                SD.loginDialog.loadPanel();
            }
            
            SD.textAndImage = {
                verificationString: '',
                eventName: SD.eventName,
                eventDescription: SD.eventDescription,
                eventWebSiteURL: SD.eventWebSiteURL,
                eventStartTime: SD.eventStartTime,
                eventStartTimeHours: SD.eventStartTimeHours,
                eventStartTimeMinutes: SD.eventStartTimeMinutes,
                eventStartTimeSeconds: SD.eventStartTimeSeconds,
                eventLocation: SD.eventLocation,
                eventStreet: SD.eventStreet,
                eventCity: SD.eventCity,
                eventCountry: SD.eventCountry,
                eventDescType: SD.eventDescType,
                eventImageURL: ''
            };            
        }
    };

    /**
     * Wrapper for async.js
     */
    Smirnoff.Event = function() {
        var q = new Async.Observer();
        return {
            /**
             * Fire code after something else happens.
             *
             * @param {String} e Name of event to wait on.
             * @param {Function} f Code to fire.
             */
            waitFor: function(e, f) {
                // Reversing params here, this makes the code neater
                q.subscribe(f, e);
            },
            /**
             * Fire the event.
             *
             * @param {Object} e
             */
            notify: function(e) {
                q.notify(e);
            },
            q: q
        };
    } ();
    Smirnoff.Flash = {
        /**
         * Show the video player;
         *
         * NOTE: flvsrc and flashTrackingId are currently GLOBAL vars.
         */
        videoEmbed: function() {
            //time value to bypass cache
            var time = (new Date()).getTime();
            //set swf preloader path
            var swfSource = "/resources/global/flash/thereVideoPlayer_v2.swf",
            width = 918,
            height = 517,
            embedded = "false",
            //if true, we know that this is embedded on a blog or another website
            share = "false",
            //if true...sharing will be enabled
            closeable = "false";
            //if true...a close button will appear in the upper right
            //var closeAction = ""; //action that is called in the js when the flash close button is clicked
            //var ccPath = "";  //path to closed captioning xml
            //flash embed vars
            flashvars = {
                width: width,
                height: height,
                flvsrc: flvsrc,
                trackingId: flashTrackingId,
                closeable: closeable,
                share: share,
                embedded: embedded
            },
            params = {
                menu: "false",
                scale: "noscale",
                align: "T",
                allowScriptAccess: "always",
                bgcolor: "#000000",
                allowFullScreen: "true",
                wmode: "opaque"
            },
            attributes = {
                id: "thereVideoPlayerSwf"
            };
            swfobject.embedSWF(swfSource, "flashGoesHere", "918", "517", "10.0.0", "", flashvars, params, attributes);
        }
    };
    Smirnoff.Session = function() {
        return {
            ageGateDisabled: function() {
                return (Smirnoff.util.getUrlParam("noagegate") == "1");
            },
            ageGateLoaded: function() {
                return $('#dialog_agegate').length > 0;
            },
            ageGatePassed: function() {
                return $.cookie('allowAccess') != null;
            },
            /**
             * Return true if logged into smirnoff.com
             */
            loggedIn: function() {
                var c = $.cookie("allowAccess");
                return (c === "true");
            },
            /**
             * Getter/setter
             */
            locale: function() {
                if (typeof arguments[0] !== 'undefined') {
                    this._locale = arguments[0];
                }
                return this._locale;
            },
            /**
             * Return true if logged into Ektron CMS
             *
             * TODO: this will change when Ektron changes their cookie-setting method.
             */
            loggedInToECM: function() {
                if ($.cookie("ecm")) {
                    var params = $.unparam($.cookie("ecm"));
                    if (params.user_id != 0) {
                        return true;
                    }
                }
                return false;
            },
            /**
             * Logic for showing age gate
             */
            needsAgeGate: function() {
				if ($(document.body).hasClass('no-age-gate')) return;
			
                if (Smirnoff.Session.ageGatePassed() && !Smirnoff.Session.rememberMe()) {
                    $.cookie('gw_chkRemember','true');
                    $.cookie('gw_mo','01');
                    $.cookie('gw_dy','01');
                    $.cookie('gw_yr','1980');
                }
                return (!Smirnoff.Session.rememberMe()
                && !Smirnoff.Session.loggedInToECM()
                && !Smirnoff.Session.ageGateDisabled()
                && !Smirnoff.Session.ageGatePassed()
                && !Smirnoff.Session.loggedIn());
            },
            /**
             * Return false if this person hasn't passed the age gate yet
             */
            passedAgeGate: function() {
                },
            rememberMe: function() {
                return ($.cookie("gw_chkRemember") === "true");
            },
            afterLogin: function(data) {
                Smirnoff.util.log.debug('Session.afterLogin()');
                
                $(document.body).addClass('loggedIn');
                $("#commentingcontrol").css("display", "block");
                $("#content a.reg").fadeOut();

                if (data.redirect) {
                    $.reloadPage(data.langType);
                }
            },
            afterLogout: function() {
                Smirnoff.util.log.debug('Session.afterLogout()');
                $(document.body).removeClass('loggedIn');
            }
        };
    } ();

    /**
     * Events for "There/NotThere" & "Flag This" UI Control
     */
    $.fn.ratingControl = function(o) {
        /*
         * track one message animation for all buttons
         */
        var timeout = null, 
            options = $.extend({}, o);
        
        function rateSuccess(data,textStatus,el) {

            var there, notthere, message = el.find('.message');
            
            if (data.success === true) {
                there = el.find('a.there span.count');
                there.html('(' + data.fives + ')');
                notthere = el.find('a.notthere span.count');
                notthere.html('(' + data.ones + ')');
            }

            /*
             * Show an error message
             */
            if (data.message) {
                message
                    .html(data.message)
                    .fadeIn('fast', function() {
                        timeout = setTimeout(function() {
                            message
                                .fadeOut('fast',function() {
                                    Smirnoff.Session._votingInProgress = false;
                            });
                        },1500);
                });
                
                return;
            }
            
            // No message, clear flag anyway
            Smirnoff.Session._votingInProgress = false;
        }
        
        function flagThisSuccess(data,textStatus,el) {
            el.find('a.flagThis')
                .replaceWith($('<span class="flagThis">' + data.message + '</span>'))
                .fadeIn('fast');
            
            // clear the flag
            Smirnoff.Session._votingInProgress = false;
        }
        
        function getVotingUrl(element) {
            // if user is logged in
            if (element.hasClass('requireLogin') || Smirnoff.Session.loggedInToECM()) {
                return '/templates/ThereNotThereReqLogin.aspx';
            }
            else {
                return '/templates/thereNotThere.aspx';
            }
        }
        
        /*
         * For each .voting-ui block...
         */
        return $(this).each(function() {
                var votingUrl, 
                    data,
                    successFunction,
                    container = $(this),
                    id = container.find('input.momentid,input.ratingid,input.detailid').attr('value'),
                    message = container.find('span.message');
            /*
             * Inject the error message element if it's not there
             */
            if (!message.length) {
                message = container.append('<span class="message" style="display: none"></span>')
                            .find('span.message');
            }

            /*
             * Attach one click event
             */
            container.click(function(e) {
                e.preventDefault();

                var target = $(e.target);

                Smirnoff.util.log.debug('voting ui clicked, checking if _votingInProgress is true...');

                // Make sure nothing's currently running
                if (Smirnoff.Session._votingInProgress === true) {
                    Smirnoff.util.log.debug('...TRUE, returning.');
                    return false;
                }

                Smirnoff.util.log.debug('...FALSE, proceeding.');

                // Prevent multiple asynchronous operations (ajax/animation)            
                Smirnoff.Session._votingInProgress = true;

                /*
                 * Delegate the click
                 */
                if (target.is('a.there')) {
                    votingUrl = getVotingUrl(target);
                    data = {cid:id,type:'Five'};
                    successFunction = rateSuccess;
                } else if (target.is('a.notthere')) {
                    votingUrl = getVotingUrl(target);
                    data = {cid:id,type:'One'};
                    successFunction = rateSuccess;
                } else if (target.is('a.flagThis')) {
                    votingUrl = '/templates/flagThis.aspx';
                    data = {cid:id,action:'flagthis'};
                    successFunction = flagThisSuccess;
                } else {
                    return false;
                }
                
                $.ajax({
                    url: votingUrl,
                    data: data,
                    dataType: 'json',
                    success: function(data, ts){                        
                        successFunction(data, ts, container);
                    }
                });                    
            });
        });
    };

    /**
     * Inject elements to make a file input styleable
     */
    $.fn.addFakeFileUpload = function() {
        return $(this).each(function() {
            $(this)
            .append($('<div class="fakefile" />')
            .append($('<input type="text" />')
            .attr('id', $(this).attr('id') + '__fake'))
            .append($('<img src="../images/text_browse_file.gif" alt="" />')));
        });
    };
    
    /**
     * Redirect to correct locale
     *
     * @param {Object} langType
     */
    $.reloadPage = function(str) {
        if (URL_REGEX.test(str)) {
            location.href = str;
            return;
        }
        
        var href = location.href;
        var search = location.search;
        var langType = 'LangType=' + str;
        if (location.search) {
            if (search.indexOf('LangType') > -1) {
                search = search.replace(/LangType=\d+/, langType);
            } else {
                search = search + '&' + langType
            }
            href = href.replace(location.search, search);
        } else {
            href = href + '?' + langType;
        }
        location = href;
    };
    
    //Allow for the header menu to set an active state to a selected item
    Menu = {
        // Indexes for 'tabs'
        items : {
            home : 0,
            inspired : 2,
            stories : 4,
            contests : 6,
            events : 8,
            drinks : 10
        },
        setItem : function(pos,main) {
            
        }
    };
    
    /*
     * This is called by Flash
     */
    window.shareThis = {
        /**
         * Flash callback
         */
        popup: function() {
            Smirnoff.DOM.showAddThis();
            Smirnoff.DOM.repositionAddThis();
        }
    }
    
    $(function() {
        var baseUrl = window.location.protocol + '//' + window.location.hostname;

        $("a.shareThis").each(function() {
            var a = $(this),
                url = a.attr('href');
                
            /*
             * If link is empty, pull the page url
             */
            if (url == "#") url = location.href;
            else url = baseUrl + url;
                
            Smirnoff.util.log.debug('addthis url: ' + url);
            
            addthis.button(a[0], {}, {url: url});
        });
    });
})(jQuery);



