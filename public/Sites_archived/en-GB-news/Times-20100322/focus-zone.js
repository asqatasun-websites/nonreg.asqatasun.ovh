<!--
// -----------------------
// GLOBAL VARIABLES
// -----------------------
n2ColCurrentItemFocus = 0;
var swapblocksFocus = 0;
var messagesFocus = new Array();
var nCurrentCarouselFocus=0;
var bNavButtonClickFocus=false;
// -----------------------
// FUNCTION: fWrite2ColTeasers
// DESCRIPTION: A function that writes the HTML to the teaser
// ARGUMENTS: sTeaserID, sLinkURL, sHdrSection, sImgURL, sImgAlt, sTitle, sTitleLinkURL, sTeaser, sShowTeaserText
// RETURN: None
// -----------------------
function fWrite2ColTeasersFocus(sTeaserID, sLinkURL, sHdrSection, sImgURL, sImgAlt, sTitle, sTitleLinkURL, sTeaser, sShowTeaserText) {
    var sHTML = '<div class="float-left" id="Carousel2Col1"><div class="activeblocks" style="display:block"><div class="text-left bg-000" style="height:100px;"><div class="float-right padding-left-10"><a href="' + sLinkURL + '"><img src="' + sImgURL + '" width="100" height="100" alt="Focus Zone" class="display-block" /></a></div><div class="padding-left-10 padding-top-5">';
    if( ! sImgURL == "") {
        sHTML += '<h2 class="sub-heading padding-bottom-3 padding-top-3"><a href="' + sLinkURL + '" class="link-fff">' + sHdrSection + '</a></h2>';
        sHTML += '<p class="small color-fff">' + sTeaser + '</p></div></div></div></div>';
    }
    var sTeaserIDNameFocus = 'Carousel2ColFocus' + sTeaserID;
    document.getElementById(sTeaserIDNameFocus).innerHTML = sHTML;
}
// -----------------------
// FUNCTION: fMoveCarousel2Col
// DESCRIPTION: A function that navigates the number of caraousels
// ARGUMENTS: nDirection, sShowTeaserText
// RETURN: None
// -----------------------
function fMoveCarousel2ColFocus(nDirection, sShowTeaserText) {
	bNavButtonClickFocus=true;
	//clearTimeout(sRoateCaraousel);
	n2ColCurrentItemFocus=nCurrentCarouselFocus;//Uncomment this block if navigation is to be strted from current caraousel
    if(nDirection == 1) {
        if(n2ColCurrentItemFocus == n2ColTotalCarouselItemsFocus - 1) {
            n2ColCurrentItemFocus = 0;
        }  else {
            n2ColCurrentItemFocus ++ ;
        }
		nCurrentCarouselFocus=n2ColCurrentItemFocus;//
		fChangeLinkStyleFocus();
        var i = n2ColCurrentItemFocus;
        for(var x = 1; x < 2; x ++ ) {
            fWrite2ColTeasersFocus(x, aTeaser2ColFocus[i][0], aTeaser2ColFocus[i][1], aTeaser2ColFocus[i][2], aTeaser2ColFocus[i][6], aTeaser2ColFocus[i][4], aTeaser2ColFocus[i][5], aTeaser2ColFocus[i][3], sShowTeaserText);
            if(i == n2ColTotalCarouselItemsFocus - 1) {
                i = 0;
            }  else {
                i ++ ;
            }
        }
    }  else {
        if(n2ColCurrentItemFocus == 0) {
            n2ColCurrentItemFocus = n2ColTotalCarouselItemsFocus - 1;
        }  else {
            n2ColCurrentItemFocus -- ;
        }
		//alert(n2ColCurrentItemFocus);
		nCurrentCarouselFocus=n2ColCurrentItemFocus;//
		fChangeLinkStyleFocus();
        var i = n2ColCurrentItemFocus;
        for(var x = 1; x < 2; x ++ ) {
            fWrite2ColTeasersFocus(x, aTeaser2ColFocus[i][0], aTeaser2ColFocus[i][1], aTeaser2ColFocus[i][2], aTeaser2ColFocus[i][6], aTeaser2ColFocus[i][4], aTeaser2ColFocus[i][5], aTeaser2ColFocus[i][3], sShowTeaserText);
            if(i == n2ColTotalCarouselItemsFocus - 1) {
                i = 0;
            }  else {
                i ++ ;
            }
        }
    }
    return true;
}

// -----------------------
// FUNCTION: swapcontent
// DESCRIPTION: A function swaps the content of caraousel
// ARGUMENTS: 
// RETURN: None
// -----------------------
function swapcontentFocus() {
	swapblocksFocus = (swapblocksFocus < messagesFocus.length - 1) ? swapblocksFocus + 1: 0;
	reverseblocks = (swapblocksFocus == 0) ? messagesFocus.length - 1: swapblocksFocus - 1;
	messagesFocus[swapblocksFocus].style.display = "block";
	messagesFocus[reverseblocks].style.display = "none";
	if(!bNavButtonClickFocus){
			nCurrentCarouselFocus++;
		if(nCurrentCarouselFocus>(n2ColTotalCarouselItemsFocus-1)){
		nCurrentCarouselFocus=0;//Assigning value less than 0 so that It becomes 0 next time the function callls
	}
		fChangeLinkStyleFocus();//changing the link color
	}
	
}

// -----------------------
// FUNCTION: getElementByClass
// DESCRIPTION: A function that changes the class name
// ARGUMENTS: 
// RETURN: None
// -----------------------
function getElementByClassFocus(classname) {
    var inc = 0
    var allblocks = document.getElementsByTagName("*")
        
    for(i = 0; i < allblocks.length; i ++ ) {
        if(allblocks[i].className == classname)
            messagesFocus[inc ++ ] = allblocks[i]
        }
}

// -----------------------
// FUNCTION: fChangeLinkStyle
// DESCRIPTION: A function that changes the color of the bottom links
// ARGUMENTS: 
// RETURN: None
// -----------------------
function fChangeLinkStyleFocus(){
	var eParentElement=document.getElementById("bottom-links-container");
	var eElements=eParentElement.getElementsByTagName("a");
	for (i=0;i<eElements.length;i++){
		if(i==nCurrentCarouselFocus){
			eElements[i].className= "link-06c";
		}
		else{
			eElements[i].className= "link-666";
		}
	}
	
	
}

/*window.onload = function() { //Calling the function on window load
    if(document.getElementById) {
        getElementByClass("activeblocks");
         setInterval("swapcontent()", 2000);
        }
}*/


// -->