//Embeddded Attachment Media Help Global Variable
var embeddedAttachmentCount = 0;
var embeddedID = 0;

/* Funtion for finding HTML Control */
function getBannerObj(id) 
{
	if (document.getElementById) {
		return document.getElementById(id);
		}
	else if (document.all) {
		return document.all[id];
		}
}

 /* It is responsible for assigning the random image to corresponding HTML control*/
function random_image()
{
	if (getBannerObj("sBackgroundImage")==null) return;
	getBannerObj("sBackgroundImage").style.display = ''
	if (document.all)
	    {
		document.images("BackgroundImage").src = random_background[random_background_number];
		document.images("BackgroundImage").alt = random_background_alt[random_background_number];
		getBannerObj("BannerContainer").height = document.images("BackgroundImage").height
		}
	else
	    {
		document.getElementById("BackgroundImage").setAttribute("src", random_background[random_background_number]);
		document.getElementById("BackgroundImage").setAttribute("alt", random_background_alt[random_background_number]);
		document.getElementById("BannerContainer").height = document.getElementById("BackgroundImage").height
		}
}

function AnchorBookmark(url)
{
	var currentURL = document.location.href;
	var sharpLocation = currentURL.lastIndexOf('#')
	
	if (sharpLocation != -1) currentURL = currentURL.substring(0,sharpLocation)
	
	location = currentURL + url;
}

function writeBannerCookie(name, value)
{
  try
  {
	document.cookie = name + '=' + escape(value);
  }
  catch(Err)
  {}
}

/* function for reading cookies */
function readBannerCookie(name)
{
  var cookieValue = '';
  var search = name + '=';
  if(document.cookie.length > 0)
  { 
    offset = document.cookie.indexOf(search);
    if (offset != -1)
    { 
      offset += search.length;
      end = document.cookie.indexOf(';', offset);
      if (end == -1) end = document.cookie.length;
      cookieValue = unescape(document.cookie.substring(offset, end))
    }
  }
  return cookieValue;
}

function searchKeyPress(searchValue){ 
		if (navigator.appName.indexOf("Microsoft Internet Explorer")>(-1))
		{ 
			if (event.keyCode == 13)
			{ 
				writeBannerCookie("SearchBox",searchValue);
			} 
		} 
		else 			
		{ 
			
			try
			{
				if (e.keyCode == 13 || e.which == 13)
				{ 
					writeBannerCookie("SearchBox",searchValue);
				}
			}
			catch(Err)
			{}
		}		
}