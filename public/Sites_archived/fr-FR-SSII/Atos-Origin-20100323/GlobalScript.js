String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
	return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
	return this.replace(/\s+$/,"");
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function aoKey(e){
    var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
    if (keyCode == 13) {
        return false;
    } else {
        return true;
    }
}
function aoKeySubmit(e, fnCheck){
    var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
    if (keyCode == 13) {
		if(eval('fnCheck()')){
			return true;
		}
        return false;
    } else {
        return true;
    }
}
//document.write('<div id=\'PopupDiv\' style=\'position:absolute;top:-1500px;left:-1500px\'></div>');
function fnShowMessage(sText,x,y,width,height) {
	var oNewWindow = window.open('','NewWindow','left='+x+',top='+y+',width='+width+',height='+height+',toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=1');
	oNewWindow.window.location.href = "about:blank";
	var sNewHTML = "<html><body leftmargin=5 topmargin=5 bottommargin=5 rightmargin=5>" + sText + "</body></html>";
	oNewWindow.document.write(sNewHTML);
	oNewWindow.focus();
}

/* Client-side access to querystring name=value pairs
	Version 1.2.3
	22 Jun 2005
	Adam Vandenberg
	
	Usage: new Querystring([qs]) 
	
	Parse the current page's querystring
	var qs = new Querystring()

	OR

	Parse a given querystring
	var qs2 = new Querystring("name1=value1&amp;name2=value2")
	
	THEN  Querstrying.get(name[, default_value])
	
	var v1 = qs2.get("name1")
	var v3 = qs2.get("name3", "default value")

	
*/
function Querystring(qs) { // optionally pass a querystring to parse
	this.params = new Object()
	this.get=Querystring_get
	
	if (qs == null)
		qs=location.search.substring(1,location.search.length)

	if (qs.length == 0) return

// Turn <plus> back to <space>
// See: http://www.w3.org/TR/REC-html40/interact/forms.html#h-17.13.4.1
	qs = qs.replace(/\+/g, ' ')
	var args = qs.split('&') // parse out name/value pairs separated via &
	
// split out each name=value pair
	for (var i=0;i<args.length;i++) {
		var value;
		var pair = args[i].split('=')
		var name = unescape(pair[0])

		if (pair.length == 2)
			value = unescape(pair[1])
		else
			value = name
		
		this.params[name] = value
	}
}

function Querystring_get(key, default_) {
	// This silly looking line changes UNDEFINED to NULL
	if (default_ == null) default_ = null;
	
	var value=this.params[key]
	if (value==null) value=default_;
	
	return value
}

//-----------------------------------------------------------------------------
function AjaxSendAsynck(ajaxUrl, ajaxDiv, ajaxOutput){
    function GetAjaxObject(){
        if (document.all && !window.opera){
            obj = new ActiveXObject("Microsoft.XMLHTTP");
        } else {
            obj = new XMLHttpRequest();
        }
        return obj;
    }
    
    var ajaxHttp = GetAjaxObject();
    ajaxHttp.open("GET", ajaxUrl);
    ajaxHttp.onreadystatechange = function(){
        if(ajaxHttp.readyState == 4){
            var ajaxResponse = ajaxHttp.responseText;
            if (ajaxOutput == "innerHTML"){
                document.getElementById(ajaxDiv).innerHTML = ajaxResponse;
            } else if (ajaxOutput == "value"){
                document.getElementById(ajaxDiv).value = ajaxResponse;
            }
        }
    }
    
    ajaxHttp.send(null);
}
function AjaxSendSynck(ajaxUrl){
    function GetAjaxObject(){
        if (document.all && !window.opera){
            obj = new ActiveXObject("Microsoft.XMLHTTP");
        } else {
            obj = new XMLHttpRequest();
        }
        return obj;
    }
    
    var ajaxHttp = GetAjaxObject();
    ajaxHttp.open("GET", ajaxUrl, false);
    ajaxHttp.send(null);
    return ajaxHttp.responseText;
}
//-----------------------------------------------------------------------------
function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {    
    window.onload = function() {
        oldonload();      
        func();    
    }  
  }
}
//-----------------------------------------------------------------------------
