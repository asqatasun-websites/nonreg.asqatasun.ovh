/* Simple AJAX Code-Kit (SACK) v1.6.1 */
/* �2005 Gregory Wild-Smith */
/* www.twilightuniverse.com */
/* Software licenced under a modified X11 licence,
   see documentation or authors website for more details */

function sack(file) {
	this.xmlhttp = null;
	this.resetData = function() {
		this.method = "POST";
  		this.queryStringSeparator = "?";
		this.argumentSeparator = "&";
		this.URLString = "";
		this.encodeURIString = true;
  		this.execute = false;
  		this.element = null;
		this.elementObj = null;
		this.requestFile = file;
		this.vars = new Object();
		this.responseStatus = new Array(2);
  	};

	this.resetFunctions = function() {
  		this.onLoading = function() { };
  		this.onLoaded = function() { };
  		this.onInteractive = function() { };
  		this.onCompletion = function() { };
  		this.onError = function() { };
		this.onFail = function() { };
	};

	this.reset = function() {
		this.resetFunctions();
		this.resetData();
	};

	this.createAJAX = function() {
		try {
			this.xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e1) {
			try {
				this.xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e2) {
				this.xmlhttp = null;
			}
		}

		if (! this.xmlhttp) {
			if (typeof XMLHttpRequest != "undefined") {
				this.xmlhttp = new XMLHttpRequest();
			} else {
				this.failed = true;
			}
		}
	};

	this.setVar = function(name, value){
		this.vars[name] = Array(value, false);
	};

	this.encVar = function(name, value, returnvars) {
		if (true == returnvars) {
			return Array(encodeURIComponent(name), encodeURIComponent(value));
		} else {
			this.vars[encodeURIComponent(name)] = Array(encodeURIComponent(value), true);
		}
	}

	this.processURLString = function(string, encode) {
		encoded = encodeURIComponent(this.argumentSeparator);
		regexp = new RegExp(this.argumentSeparator + "|" + encoded);
		varArray = string.split(regexp);
		for (i = 0; i < varArray.length; i++){
			urlVars = varArray[i].split("=");
			if (true == encode){
				this.encVar(urlVars[0], urlVars[1]);
			} else {
				this.setVar(urlVars[0], urlVars[1]);
			}
		}
	}

	this.createURLString = function(urlstring) {
		if (this.encodeURIString && this.URLString.length) {
			this.processURLString(this.URLString, true);
		}

		if (urlstring) {
			if (this.URLString.length) {
				this.URLString += this.argumentSeparator + urlstring;
			} else {
				this.URLString = urlstring;
			}
		}

		// prevents caching of URLString
		this.setVar("rndval", new Date().getTime());

		urlstringtemp = new Array();
		for (key in this.vars) {
			if (false == this.vars[key][1] && true == this.encodeURIString) {
				encoded = this.encVar(key, this.vars[key][0], true);
				delete this.vars[key];
				this.vars[encoded[0]] = Array(encoded[1], true);
				key = encoded[0];
			}

			urlstringtemp[urlstringtemp.length] = key + "=" + this.vars[key][0];
		}
		if (urlstring){
			this.URLString += this.argumentSeparator + urlstringtemp.join(this.argumentSeparator);
		} else {
			this.URLString += urlstringtemp.join(this.argumentSeparator);
		}
	}

	this.runResponse = function() {
		eval(this.response);
	}

	this.runAJAX = function(urlstring) {
		if (this.failed) {
			this.onFail();
		} else {
			this.createURLString(urlstring);
			if (this.element) {
				this.elementObj = document.getElementById(this.element);
			}
			if (this.xmlhttp) {
				var self = this;
				if (this.method == "GET") {
					totalurlstring = this.requestFile + this.queryStringSeparator + this.URLString;
					this.xmlhttp.open(this.method, totalurlstring, true);
				} else {
					this.xmlhttp.open(this.method, this.requestFile, true);
					try {
						this.xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
					} catch (e) { }
				}

				this.xmlhttp.onreadystatechange = function() {
					switch (self.xmlhttp.readyState) {
						case 1:
							self.onLoading();
							break;
						case 2:
							self.onLoaded();
							break;
						case 3:
							self.onInteractive();
							break;
						case 4:
							self.response = self.xmlhttp.responseText;
							self.responseXML = self.xmlhttp.responseXML;
							self.responseStatus[0] = self.xmlhttp.status;
							self.responseStatus[1] = self.xmlhttp.statusText;

							if (self.execute) {
								self.runResponse();
							}

							if (self.elementObj) {
								elemNodeName = self.elementObj.nodeName;
								elemNodeName.toLowerCase();
								if (elemNodeName == "input"
								|| elemNodeName == "select"
								|| elemNodeName == "option"
								|| elemNodeName == "textarea") {
									self.elementObj.value = self.response;
								} else {
									self.elementObj.innerHTML = self.response;
								}
							}
							if (self.responseStatus[0] == "200") {
								self.onCompletion();
							} else {
								self.onError();
							}

							self.URLString = "";
							break;
					}
				};

				this.xmlhttp.send(this.URLString);
			}
		}
	};

	this.reset();
	this.createAJAX();
}


/************************************************************************************************************
Ajax poller
Copyright (C) 2006  DTHMLGoodies.com, Alf Magne Kalleland

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

Dhtmlgoodies.com., hereby disclaims all copyright interest in this script
written by Alf Magne Kalleland.

Alf Magne Kalleland, 2006
Owner of DHTMLgoodies.com
	
************************************************************************************************************/	



var path = '';

var serverSideFile = path+'/do/poller/';
var voteLeftImage = path+'images/graph_left_1.gif';
var voteRightImage = path+'images/graph_right_1.gif';
var voteCenterImage = path+'images/graph_middle_1.gif';

var graphMaxWidth = 220;	// It will actually be a little wider than this because of the rounded image at the left and right
var graphMinWidth = 15;	// Minimum size of graph
var pollScrollSpeed = 5;	// Lower = faster
var txt_totalVotes = 'Total number of votes: ';

var ajaxObjects = new Array();
var pollVotes = new Array();
var pollVoteCounted = new Array();
var totalVotes = new Array();

Number.prototype.isFloat = function() {
	return /\./.test(this.toString());
};
/* Preload images */

var preloadedImages = new Array();
/*preloadedImages[0] = new Image();
preloadedImages[0].src = voteLeftImage;
preloadedImages[1] = new Image();
preloadedImages[1].src = voteRightImage;
preloadedImages[2] = new Image();
preloadedImages[2].src = voteCenterImage;*/

/*
These cookie functions are downloaded from 
http://www.mach5.com/support/analyzer/manual/html/General/CookiesJavaScript.htm
*/	
function Poller_Get_Cookie(name) { 
   var start = document.cookie.indexOf(name+"="); 
   var len = start+name.length+1; 
   if ((!start) && (name != document.cookie.substring(0,name.length))) return null; 
   if (start == -1) return null; 
   var end = document.cookie.indexOf(";",len); 
   if (end == -1) end = document.cookie.length; 
   return unescape(document.cookie.substring(len,end)); 
} 
// This function has been slightly modified
function Poller_Set_Cookie(name,value,expires,path,domain,secure) { 
	expires = expires * 60*60*24*1000;
	var today = new Date();
	var expires_date = new Date( today.getTime() + (expires) );
    var cookieString = name + "=" +escape(value) + 
       ( (expires) ? ";expires=" + expires_date.toGMTString() : "") + 
       ";path=/" +
       ( (domain) ? ";domain=" + domain : "") + 
       ( (secure) ? ";secure" : ""); 
    document.cookie = cookieString; 
}

	
function showVoteResults(pollId,ajaxIndex)
{
	var xml = ajaxObjects[ajaxIndex].response;
	xml = xml.replace(/\n/gi,'');
	
    var questions= document.getElementById('sondage');
    var boite_sondage = document.getElementById('boite_contenu_poll');
    var pied = document.getElementById('footer_poll_int');
    
    questions.style.background = 'none';
    if(pied) {
        pied.className = 'footer_poll_int_results';
    }
    boite_sondage.className = 'boite_contenu_poll_results';
    //document.getElementById('sondage').style.height = "95px";
    //document.getElementById('boite_contenu_poll').style.height = "95px";  
	
	var reg = new RegExp("^.*?<pollerTitle>(.*?)<.*$","gi");
	var pollerTitle = xml.replace(reg,'$1');
	
	
	
	var resultTab = document.getElementById('poller_results' + pollId);
	
	//var tbody_results = document.createElement('tbody');
	
	var title = new Element('div', {'class':'result_pollerTitle'});
	title.innerHTML = pollerTitle;
	resultTab.appendChild(title);
	
	//resultTab.appendChild(tbody_results);
	
	var options = xml.split(/<option>/gi);
	
	pollVotes[pollId] = new Array();
	totalVotes[pollId] = 0;
	
	for(var no=1;no<options.length;no++){
		
		var div_resultat = new Element('div', {'class':'resultTitle'});
		resultTab.appendChild(div_resultat);
		
		var div_barre = new Element ('div', {'class':'resultBarre'});
		resultTab.appendChild(div_barre);
		
		var elements = options[no].split(/</gi);
		var currentOptionId = false;
		for(var no2=0;no2<elements.length;no2++){
			if(elements[no2].substring(0,1)!='/'){
				var key = elements[no2].replace(/^(.*?)>.*$/gi,'$1');
				var value = elements[no2].replace(/^.*?>(.*)$/gi,'$1');
			
				if(key.indexOf('optionText')>=0){
					var option = document.createElement('div');
					option.className='result_pollerOption';
					option.innerHTML = value;
					div_resultat.appendChild(option);
				}
				
				if(key.indexOf('optionId')>=0){
					currentOptionId = value/1;
				}
				
				if(key.indexOf('votes')>=0){
					var barre_span = document.createElement("span");
					barre_span.className = 'result_voteTxt result_voteTxt' + no;
                    barre_span.id = 'result_voteTxt' + currentOptionId;
                    div_barre.appendChild(barre_span);	
                    
                    var number_span = document.createElement('span');	
                    number_span.className = 'result_numberTxt';
                    number_span.id = 'result_numberTxt' + currentOptionId;
                    number_span.innerHTML = "0%";		
					div_barre.appendChild(number_span); 
					
					pollVotes[pollId][currentOptionId] = value;
					totalVotes[pollId] = totalVotes[pollId]/1 + value/1;
				}
			}
		}
	}
	
	//alert(pollVotes[pollId].length);
	
	var totalVoteP = document.createElement('P');
	totalVoteP.className = 'result_totalVotes';
	totalVoteP.innerHTML = txt_totalVotes + totalVotes[pollId];
	//vote_td.appendChild(totalVoteP);	
	//console.log("------------------------------");
	//console.log("Total votes "+totalVotes[pollId]);
	
	setPercentageVotes(pollId);
	slideVotes(pollId,0);
}

function setPercentageVotes(pollId)
{
	var totalVotesPoll = 0;
	var valueMiniVotesPoll = 100;
	var idMiniVotesPoll = 0;
	for(var i=1 ; i < pollVotes[pollId].length ; i++){
		if(pollVotes[pollId][i] != undefined){
			//pollVotes[pollId][i] =  Math.round( (pollVotes[pollId][i] / totalVotes[pollId]) * 100);
			perCentValueID =  Math.round(((pollVotes[pollId][i] / totalVotes[pollId]) * 100)*Math.pow(10,1))/Math.pow(10,1);
			if(perCentValueID <= valueMiniVotesPoll){
				valueMiniVotesPoll = perCentValueID;
				idMiniVotesPoll = i;
			}
			pollVotes[pollId][i] = perCentValueID;
			totalVotesPoll += perCentValueID;
			totalVotesPoll = Math.round(totalVotesPoll*Math.pow(10,1))/Math.pow(10,1);
			//console.log("ajout de",perCentValueID, "valeur totale :",totalVotesPoll);
		}
	}
	//console.log("minimum de",valueMiniVotesPoll,"en position",idMiniVotesPoll);
	var differenceTotaleVotes = 0;
	var newValueMinVotePoll = 0;
	if (totalVotesPoll != 100){
		differenceTotaleVotes = Math.round((100-totalVotesPoll)*Math.pow(10,1))/Math.pow(10,1);
		//console.log("difference de",differenceTotaleVotes);
		newValueMinVotePoll = valueMiniVotesPoll+differenceTotaleVotes;
		pollVotes[pollId][idMiniVotesPoll] = Math.round(newValueMinVotePoll*Math.pow(10,1))/Math.pow(10,1);
		//console.log("nouvelle valeur minimum:",pollVotes[pollId][idMiniVotesPoll],"en position",idMiniVotesPoll);
	}
	
	var currentSum = 0;
	for(var i=1 ; i < pollVotes[pollId].length ; i++){
		//alert("poll votes = "+pollVotes[pollId][i]+" nb elements : "+pollVotes[pollId].length);
		if(pollVotes[pollId][i] != undefined){
			//currentSum = currentSum + pollVotes[pollId][i]/1;
			currentSum = currentSum + pollVotes[pollId][i];
		}else{
		}
	}
	pollVotes[pollId][i] = pollVotes[pollId][i] + (100-currentSum);
}

function slideVotes(pollId,currentPercent)
{
	currentPercent = currentPercent/1 + 1;
	/*var totalOverPercent = 0;
	var actualVotesNum = 0;
	var diffTotalPercent = 0;
	for (var j=1; j<pollVotes[pollId].length; j++){
		actualVotesNum=pollVotes[pollId][j];
		if(isNaN(actualVotesNum) == false){
			actualPercentValue = parseFloat(pollVotes[pollId][j]);
			totalOverPercent = totalOverPercent + actualPercentValue;
			//console.log(totalOverPercent);
			if(totalOverPercent==100){
				totalOverPercent = parseFloat("100");
				var diffTotalPercent = Math.round((totalOverPercent-100)*Math.pow(10,1))/Math.pow(10,1);
				//console.log(diffTotalPercent);
			}else if(totalOverPercent>100){
				totalOverPercent = parseFloat(""+totalOverPercent);
				var diffTotalPercent = Math.round((totalOverPercent-100)*Math.pow(10,1))/Math.pow(10,1);
			}
			if(totalOverPercent>100){
				//console.log(parseFloat("100"));
				pollVotes[pollId][j] = actualPercentValue - diffTotalPercent;
			}
		}
	}*/
	for(var i=1 ; i < pollVotes[pollId].length ; i++){
		currentOverPercent = pollVotes[pollId][i];
		if(pollVotes[pollId][i]>=currentPercent){
			var obj = document.getElementById('result_voteTxt' + i);
			obj.style.width = Math.max(graphMinWidth,Math.round(currentPercent/100*graphMaxWidth)) + 'px';
			
			$('result_numberTxt' + i).innerHTML = currentPercent + '%';
		}
		if(pollVotes[pollId][i]<=currentPercent){
			$('result_numberTxt' + i).innerHTML = currentOverPercent + '%';
		}
	}
	if(currentPercent<100)setTimeout('slideVotes("' + pollId + '","' + currentPercent + '")',pollScrollSpeed);
}

function prepareForPollResults(pollId)
{
    if (document.getElementById('poller_waitMessage' + pollId)) {
        document.getElementById('poller_waitMessage' + pollId).style.display = 'block';
    }
    if (document.getElementById('poller_question' + pollId)) {
        document.getElementById('poller_question' + pollId).style.display = 'none';
    }
    if (document.getElementById('poller_question')) {
        document.getElementById('poller_question').style.display = 'none';
    }
    if (document.getElementById('links')) {
        document.getElementById('links').style.display = 'none';
    }
}

function castMyVote(pollId,formObj)
{	
	var elements = formObj.elements['vote[' + pollId + ']'];
	var optionId = false;
	for(var no=0;no<elements.length;no++){
		if(elements[no].checked)optionId = elements[no].value;
	}
	Poller_Set_Cookie('dhtmlgoodies_poller_' + pollId,'1',6000000);
	if(optionId){
		var ajaxIndex = ajaxObjects.length;
		ajaxObjects[ajaxIndex] = new sack();
		ajaxObjects[ajaxIndex].requestFile = serverSideFile + 'vote?vote=' + optionId + '&id=' + pollId;
		prepareForPollResults(pollId);
		ajaxObjects[ajaxIndex].onCompletion = function(){ showVoteResults(pollId,ajaxIndex); };	// Specify function that will be executed after file has been found
		ajaxObjects[ajaxIndex].runAJAX();		// Execute AJAX function	
	}
}	

function displayResultsWithoutVoting(pollId)
{
	var ajaxIndex = ajaxObjects.length;
	ajaxObjects[ajaxIndex] = new sack();
	ajaxObjects[ajaxIndex].requestFile = serverSideFile + 'result?result=' + pollId;
	prepareForPollResults(pollId);
	ajaxObjects[ajaxIndex].onCompletion = function(){ showVoteResults(pollId,ajaxIndex);};	// Specify function that will be executed after file has been found
	ajaxObjects[ajaxIndex].runAJAX(); // Execute AJAX function
}

