function getPositionCarte(e)
{
	var x;
	var y;

	if (e)
	{
		// FF & al. (ok si la div ou l'img est en position relative)
		x = e.layerX;
		y = e.layerY;
		//alert("x=" + x + ", y=" + y);
	}
	else
	{
		// IE
		x = event.offsetX;
		y = event.offsetY;
	}
	selectionnerAgencePosition(
		x,
		y,
		32,
		32,
		document.getElementById("codeTO").value
	);
}
