function insertFlash( strSrc, intWidth, intHeight, strWMode, strScale, strPlayMode, strCLSID, strCODEBASE, strBGcolor, strPosition, strFlashVars, strContentDiv, strFlashParameter, returnAsString, isInteractive )
{
	if(strWMode == null || strWMode == "") strWMode = "Opaque";
	if(strScale == null || strScale == "") strScale = "noscale";
	if(strPlayMode == null || strPlayMode == "") strPlayMode = "true";
	if(strContentDiv == null || strContentDiv == "") strContentDiv = "";
	if(returnAsString == null || returnAsString == "") returnAsString = false;
	var strIDIE = isInteractive ? " id=\"embflashie\"" : "";
	var strIDMoz = isInteractive ? " id=\"embflash\"" : "";
	var strLiveConn = isInteractive ? " swliveconnect=\"true\"" : "";


	var strFlash	=  '<object' + strIDIE + ' classid="' + strCLSID + '" codebase="' + strCODEBASE + '" ';
	var embedFlashParameter = "";
	strFlash += ' width="' + intWidth + '" height="' + intHeight + '">';

	strFlash += '<param name="movie" value="' + strSrc + '" />';
	strFlash += '<param name="quality" value="high" />';
	strFlash += '<param name="play" value="' + strPlayMode + '" />';
	strFlash += '<param name="wmode" value="' + strWMode + '" />';
	strFlash += '<param name="scale" value="' + strScale + '" />';
	strFlash += '<param name="menu" value="false" />';

	/**
	*	handle flash params
	*/
	if(strFlashVars == null) {
		strFlashVars = "";
	}
	// add slt system variable for flash tool
	if(window.cms_sltpath) {
		if(strFlashVars == "") {
			strFlashVars += "cms_sltpath=" + window.cms_sltpath;
		} else {
			strFlashVars += '&cms_sltpath=' + window.cms_sltpath;
		}
	}

	if(strFlashParameter != null && strFlashParameter != "") {
		var flashParameter = strFlashParameter.split("&");
		for (var i = 0; i < flashParameter.length; i++){
			var flashNameValue = flashParameter[i].split("=");
			if(flashNameValue.length==1) {
				strFlash += '<param name="'+flashNameValue[0]+'" value="" />';
				embedFlashParameter += ' ' + flashNameValue[0]+'="" ';
			} else if(flashNameValue.length==2) {
				strFlash += '<param name="'+flashNameValue[0]+'" value="' + flashNameValue[1] + '" />';
				embedFlashParameter += ' ' + flashNameValue[0]+'="'+flashNameValue[1] + '" ';
			}
		}
	}

	if(strFlashVars != null && strFlashVars != "") strFlash += '<param name="FlashVars" value="' + strFlashVars + '" />';
	if(strPosition != null && strPosition != "") strFlash += '<param name="align" value="' + strPosition + '" />';
  	if(strBGcolor != null && strBGcolor != "") strFlash	+= '<param name="bgcolor" value="' + strBGcolor + '" />';

	strFlash += '<embed' + strIDMoz +  strLiveConn + ' src="' + strSrc + '" width="' + intWidth + '" height="' + intHeight + '" wmode="' + strWMode + '" play="' + strPlayMode + '" scale="' + strScale  + '" menu="false"';
	strFlash	+= ' bgcolor="#FFFFFF"';
	if(embedFlashParameter != null && embedFlashParameter != "") strFlash	+= embedFlashParameter;
	if(strFlashVars != null && strFlashVars != "") strFlash	+= ' FlashVars="' + strFlashVars + '"';
	if(strPosition != null && strPosition != "") strFlash	+= ' align="' + strPosition + '"';
	strFlash += ' quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash">';
	strFlash += '</embed>';
	strFlash += '</object>';

	// If require, return as String
	if (returnAsString) {
		return strFlash;
	}

	if(strContentDiv != "" && document.getElementById(strContentDiv)) {
		document.getElementById(strContentDiv).innerHTML = strFlash;
	}
	else {
		document.write( strFlash );
		// BUG Drag 'n' Drop: Document must not be closed!
		// document.close( );
	}
}

function flash_versioncheck(vno) {
	var isIE=(navigator.appVersion.indexOf("MSIE") != -1) ? true : false;
	var isWin=(navigator.appVersion.toLowerCase().indexOf("win") != -1) ? true : false;

	if(isIE && isWin) {
		try {
			var flashplayerversion = "ShockwaveFlash.ShockwaveFlash." + vno;
			return ((typeof(ActiveXObject) == "function") && (typeof(new ActiveXObject(flashplayerversion)) == "object"));
		} catch (error) {
			return false;
		}
	} else if (navigator.mimeTypes && navigator.mimeTypes["application/x-shockwave-flash"]) {
		var flashplugin = navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin;
		var flashplugindesc;
		if (flashplugin!=null && flashplugin.description) {
			flashplugindesc = parseInt(flashplugin.description.substring(flashplugin.description.indexOf(".")-2));
		}
	   	return !(!flashplugindesc || flashplugindesc < vno);
	} else {
		return false;
	}
}

function insertQuickTime( strSrc, strID, intWidth, intHeight, strScale, strPlayMode, strController, strCLSID, strCODEBASE, strContentDiv )
{
	if(strScale == null || strScale == "") strScale = "TOFIT";
	if(strPlayMode == null || strPlayMode == "") strPlayMode = "true";
	if(strController == null || strController == "") strController = "true";

  	var strQuickTime = '<object classid="' + strCLSID + '" codebase="' + strCODEBASE + '" ';
  	strQuickTime += 'width="' + intWidth + '" height="' + intHeight + '" ';
  	strQuickTime += 'id="' + strID + '">';
  	strQuickTime += '<param name="src" value="' + strSrc + '"/>';
  	strQuickTime += '<param name="AUTOPLAY" value="' + strPlayMode + '" />';
  	strQuickTime += '<param name="CONTROLLER" value="' + strController + '" />';
  	strQuickTime += '<param name="SCALE" value="' + strScale + '" />';
  	strQuickTime += '<embed src="' + strSrc + '" type="video/quicktime" pluginspace="http://www.apple.com/quicktime/download/" width="' + intWidth + '" height="' + intHeight + '" ';
  	strQuickTime += 'scale="' + strScale + '" autoplay="' + strPlayMode + '" controller="' + strController + '" name="' + strID + '" >';
  	strQuickTime += '</embed></object>';

	if(strContentDiv != "" && document.getElementById(strContentDiv)) {
		document.getElementById(strContentDiv).innerHTML = strQuickTime;
	}
	else {
		document.write( strQuickTime );
		// BUG Drag 'n' Drop: Document must not be closed!
		// document.close( );
	}
}

function insertRealVideo( strSrc, strID, intWidth, intHeight, strPosition, strPlayMode, strController, strCLSID, strCODEBASE, strContentDiv )
{
  	if(strID == null || strID == "") strID = "realmovie";
  	if(strPosition == null || strPosition == "") strPosition = "true";
  	if(strPlayMode == null || strPlayMode == "") strPlayMode = "false";
  	if(strController == null || strController == "") strController = "ImageWindow";

  	var strRealVideo = '<object classid="' + strCLSID + '" codebase="' + strCODEBASE + '" ';
  	strRealVideo += 'width="' + intWidth + '" height="' + intHeight + '">';
  	strRealVideo += '<param name="src" value="' + strSrc + '"/>';
  	strRealVideo += '<param name="console" value="' + strID + '"/>';
  	strRealVideo += '<param name="controls" value="' + strController + '" />';
  	strRealVideo += '<param name="center" value="' + strPosition + '" />';
  	strRealVideo += '<param name="autostart" value="' + strPlayMode + '" />';
  	strRealVideo += '</object>';

	if(strContentDiv != "" && document.getElementById(strContentDiv)) {
		document.getElementById(strContentDiv).innerHTML = strRealVideo;
	}
	else {
		document.write( strRealVideo );
		// BUG Drag 'n' Drop: Document must not be closed!
		// document.close( );
	}
}


function insertApplet( strSrc, strCode, intWidth, intHeight, strPosition, strCLSID, strCODEBASE, strContentDiv )
{
	if(strPosition == null || strPosition == "") strPosition = "baseline";

  	var strApplet = '<object classid="' + strCLSID + '" codebase="' + strCODEBASE + '" ';
  	strApplet += 'width="' + intWidth + '" height="' + intHeight + '" align="' + strPosition + '" code="' + strSrc + '">';
  	strApplet += '<param name="code" value="' + strSrc + '"/>';
  	strApplet += '<param name="type" value="application/x-java-applet"/>';
  	strApplet += '</object>';

	if(strContentDiv != "" && document.getElementById(strContentDiv)) {
		document.getElementById(strContentDiv).innerHTML = strApplet;
	}
	else {
		document.write( strApplet );
		// BUG Drag 'n' Drop: Document must not be closed!
		// document.close( );
	}
}
