



img {
  border:0px;
}

/* style de la barre d'administration */

.adminmenu {
    vertical-align: middle;
    white-space: nowrap;
}
.adminmenu a {
    color: #666666;
}


.topmenubg {
    background-color: #cccccc;
}
.topmenubuttons {
    white-space: nowrap;
    background-color: #cccccc;
    padding-left: 2px;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.backcolor2 {
    background-color: #999999;
}
.tab_off a {
    color: #666666;
}
.tab_on a {
    color: #666666;
}

span.hl {
    background: #FFFF66;
}



 /* calendar */
div.calendar {
	background-color: #FFFFFF;
	display: block;
	/*float: left;*/
	/*font-family: Arial, Helvetica, sans-serif;*/
	font-family: Courrier;
	font-size: 11px;
	line-height: 17px;
	margin-right: 10px; /* Invalid value: width: 170; */
}
div.calendar div.pre {
    white-space: pre;
}

div.calendar a {
	color: #000066;
	text-decoration: none;
    font-family: "Courier New", Courier, mono;
	font-size: 11px;
}
div.calendar a.weekOfYear {
    color: #75777B;
}
div.calendar .currentweek,
div.calendar a.currentday {
	background-color: #E7E7E7;
	border: 1px solid #B42C29;
	font-weight: bold;
}

div.calendar a:hover {
	background-color: #E7E7E7;
}

div.calendar h2 {
	color: #006699;
	font-weight: bold;
	font-size: 12px;
	text-align: center;
}

div.calendar h2 a {
	color: #006699;
	padding: 0 5px 0 5px;
}
div.calendar span.days {
	background-color: #E7E7E7;
	font-weight: bold;
	padding: 0 2px 0 2px;
}


/* D�claration CG21 */

/* GLOBAL */

a {	text-decoration: none;	color:#0C66A0;}
a:hover { text-decoration:underline;}

body {margin:0;	font-family:Arial, Helvetica, sans-serif;	font-size: 0.70em;	}
p {margin:0;}

form {
    margin: 0;
    padding: 0;
}

/* DIV CONTENANTS */

#footer {margin:15px 0 15px 0; font-size:1.1em;}

.element_perime{background-color:#DDC9C9;}
.element_previsionnel{background-color:#C9DDC9;}


h1 {font-size:1.6em;color:#0C66A0;font-weight:bold;border-bottom:1px solid;margin-bottom:15px;}
h2 {font-size:1.3em;color:#FF8C28;font-weight:bold;border-bottom:1px solid;margin-bottom:15px;}
h3 {font-size:1.1em;color:#0C66A0;font-weight:bold;margin-bottom:10px;}

/* Partie g�rant la position du logo */
#logo {position:absolute;top:0px;left:-4px;display:block;float:left;z-index:100;}
#logo_logged_root {position:absolute;top:48px;left:-4px;display:block;float:left;z-index:100;}
#logo_logged_user {position:absolute;top:25px;left:-4px;display:block;float:left;z-index:100;}

#logo_central {position:absolute;top:0px;left:185px;display:block;float:left;z-index:100;}
#logo_central_logged_root {position:absolute;top:233px;left:10px;display:block;float:left;z-index:100;}
#logo_central_logged_user {position:absolute;top:210px;left:10px;display:block;float:left;z-index:100;}

/* Partie g�rant la position de la barre de lien rapide */

#header {font-size:1em;position:absolute;right:200px;top: 1px;}
#header .header_separator {float:right; background :url(fond_header.gif) center top no-repeat;margin:0 5px 0 5px;}
#header a {float:right}
#header a.quicklinkCustom {color:#FFFFFF; text-decoration:none;font-size: 1em; float:right;}
#header a.quicklinkCustom:hover {text-decoration:none;}

#header_logged_root {font-size:1em;position:absolute;right:200px;top: 49px;}
#header_logged_root .header_separator {float:right; background :url(fond_header.gif) center top no-repeat;margin:0 5px 0 5px;}
#header_logged_root a {float:right}
#header_logged_root a.quicklinkCustom {color:#FFFFFF; text-decoration:none;font-size: 1em; float:right;}
#header_logged_root a.quicklinkCustom:hover {text-decoration:none;}

#header_logged_user {font-size:1em;	position:absolute;	right:200px;	top: 26px;}
#header_logged_user .header_separator {float:right; background :url(fond_header.gif) center top no-repeat;margin:0 5px 0 5px;}
#header_logged_user a {float:right}
#header_logged_user a.quicklinkCustom {color:#FFFFFF; text-decoration:none;font-size: 1em; float:right;}
#header_logged_user a.quicklinkCustom:hover {text-decoration:none;}

#bloc_gauche {margin: 55px 0 0 0;}
#bloc_gauche_form {margin: 10px 0 0 0;}
.bloc_gauche_form p{padding : 0 10px 0px 15px;text-align:left;font-weight:bold;}
.bloc_gauche_form select{margin:0 0 5px 0;}
.bloc_gauche_form a{font-size: 0.91em; text-decoration:underline; color:#0867A0;}
.bloc_gauche_form a:hover {font-size: 0.91em; text-decoration:none; color:#2693D4;}
.bloc_gauche_form input {font-size: 0.9em;height:1.5em;width: 10em;}

.menu_gauche {font-size:0.91em;padding:0 10px 0 10px;margin: 10px 0 0 0;}
.menu_gauche .title{color:#0C66A0;font-size:1.45em;font-weight:bold;}
.menu_gauche .separator{font-size:1px;height:1px;padding:0;margin:1px 0 2px 0; background : url(fond_menu3.gif) repeat-x;width:90%;}
.menu_gauche a.menuGauche {color:#000000; font-weight:bold; text-decoration:none;text-align:left;}
.menu_gauche a.menuGauche:hover {text-decoration:underline;}

#menu_gauche_02 {color:#0C66A0;background-color:#FFF6ED;border-top:#FF8C28 1px solid;border-bottom:#FF8C28 1px solid;margin:10px 0 10px 0;}
#menu_gauche_02 .titre {padding:6px 6px 6px 6px;font-weight:bold;color:#FF8C28;}
#menu_gauche_02 ul {margin:0 0 0 6px;padding:0;list-style-type:none;}
#menu_gauche_02 ul ul {font-size:0.90em;}
#menu_gauche_02 a.lienMenuOrange {color:#0C66A0;text-decoration:none;}
#menu_gauche_02 a.lienMenuOrange:hover {text-decoration:underline;}
#menu_gauche_02 .select , #menu_gauche_02 .select a {color:#FF8C28;}

#menu_gauche_02 a.leftlevelOrange1 {	padding-left: 0px;	padding-top: 1px;	padding-bottom: 1px;	color:#0C66A0;text-decoration:none;}
#menu_gauche_02 a.leftlevelOrange1:hover {text-decoration:underline;}
#menu_gauche_02 a.leftlevelOrange2 {	padding-left: 0px;	color:#0C66A0;text-decoration:none;}
#menu_gauche_02 a.leftlevelOrange2:hover {text-decoration:underline;}
#menu_gauche_02 a.leftlevelOrange3 {	padding-left: 0px;	color:#0C66A0;text-decoration:none;}
#menu_gauche_02 a.leftlevelOrange3:hover {text-decoration:underline;}
#menu_gauche_02 a.leftlevelOrange4 {	padding-left: 0px;	color:#0C66A0;text-decoration:none;}
#menu_gauche_02 a.leftlevelOrange4:hover {text-decoration:underline;}
#menu_gauche_02 a.leftlevelOrange5 {	padding-left: 0px;	color:#0C66A0;text-decoration:none;}
#menu_gauche_02 a.leftlevelOrange5:hover {text-decoration:underline;}
#menu_gauche_02 a.leftlevelOrange6 {	padding-left: 0px;	color:#0C66A0;text-decoration:none;}
#menu_gauche_02 a.leftlevelOrange6:hover {text-decoration:underline;}

#menu_gauche_02 .select a.leftlevelOrange1 {	padding-left: 0px;	padding-top: 1px;	padding-bottom: 1px;	color:#FF8C28;text-decoration:none;}
#menu_gauche_02 .select a.leftlevelOrange1:hover {text-decoration:underline;}
#menu_gauche_02 .select a.leftlevelOrange2 {	padding-left: 0px;	color:#FF8C28;text-decoration:none;}
#menu_gauche_02 .select a.leftlevelOrange2:hover {text-decoration:underline;}
#menu_gauche_02 .select a.leftlevelOrange3 {	padding-left: 0px;	color:#FF8C28;text-decoration:none;}
#menu_gauche_02 .select a.leftlevelOrange3:hover {text-decoration:underline;}
#menu_gauche_02 .select a.leftlevelOrange4 {	padding-left: 0px;	color:#FF8C28;text-decoration:none;}
#menu_gauche_02 .select a.leftlevelOrange4:hover {text-decoration:underline;}
#menu_gauche_02 .select a.leftlevelOrange5 {	padding-left: 0px;	color:#FF8C28;text-decoration:none;}
#menu_gauche_02 .select a.leftlevelOrange5:hover {text-decoration:underline;}
#menu_gauche_02 .select a.leftlevelOrange6 {	padding-left: 0px;	color:#FF8C28;text-decoration:none;}
#menu_gauche_02 .select a.leftlevelOrange6:hover {text-decoration:underline;}


/* CONTENU principal*/

#cdf {margin:0 10px 10px 10px ;color:#0C66A0;}
#cdf a:hover {color:#0C66A0;}

#lienGris{color:#AAAAAA;}
#lienGris a.lienextern_gris{color:#AAAAAA;}
#lienGris a.lienextern_gris:hover{color:#AAAAAA;text-decoration:underline;}

.contenu {margin:10px 10px 10px 10px;text-align: justify;}
.contenu a{color:#0C66A0;}
.contenu a:hover{text-decoration:underline;}
.contenu p.carnetAdresse{margin:10px 0px 10px 0px;}
.contenu li.carnetAdresse{margin:0px 10px 0px 10px;}
.contenu .titre_page01{font-size:1.6em;color:#0C66A0;font-weight:bold;border-bottom:1px solid;margin-bottom:15px;}
.contenu .titre_page012{font-size:1.2em;color:#0C66A0;font-weight:bold;border-bottom:1px solid;margin-bottom:15px;}
.contenu .titre_page011{font-size:1.2em;color:#0C66A0;font-weight:bold;border-bottom:0px solid;margin-bottom:5px;margin-top:5px;}
.contenu .titre_page02{font-size:1.3em;color:#FF8C28;font-weight:bold;border-bottom:1px solid;margin-bottom:15px;}
.contenu .titre_page03{font-size:1.1em;color:#0C66A0;font-weight:bold;margin-bottom:10px;}
.contenu .titre_page04{font-size:1.1em;color:#000000;font-weight:bold;margin-bottom:10px; }
.contenu .titre_page05{font-size:1.2em;color:#0C66A0;font-weight:bold;margin-bottom:5px;margin-top:5px;}
.contenu .ttr_list01 {font-size:1.1em;color:#0C66A0;font-weight:bold;}

.contenu .contenu_page04 {font-size:1em;color:#000000;font-weight:normal;margin-bottom:10px; }
.contenu .contenu_page05 {font-size:1em;color:#000000;font-weight:bold;margin-bottom:5px;margin-top:5px; }
.contenu .contenu_page06 {font-size:1em;color:#000000;font-weight:bold;text-decoration:none;margin-bottom:5px;margin-top:5px; }

.contenu .menu_gauche {font-size:0.91em;padding:0 0px 0 5px;margin: 0px 0 0 0;}

.contenu_droit {margin:10px 10px 10px 10px;text-align: left;}
.contenu_droit a{color:#0C66A0;}
.contenu_droit a:hover{text-decoration:underline;}
.contenu_droit .titre_page01{font-size:1.6em;color:#0C66A0;font-weight:bold;border-bottom:1px solid;margin-bottom:15px;}
.contenu_droit .titre_page02{font-size:1.3em;color:#FF8C28;font-weight:bold;border-bottom:1px solid;margin-bottom:15px;}
.contenu_droit .titre_page03{font-size:1.1em;color:#0C66A0;font-weight:bold;margin-bottom:10px;}
.contenu_droit .ttr_list01 {font-size:1.1em;color:#0C66A0;font-weight:bold;}


#faq .titre_list {font-size:1.1em;font-weight:bold;margin-bottom:3px;color:#FF8C28;}

#encart01 {position:relative;padding:10px 10px 10px 10px;}
#encart01 .titre01 {font-size:1.2em;}
#encart01 .date, #encart01 .titre01 {color:#0C66A0;font-weight:bold;}

#nav_rech {text-align:center;color:#FF8C28;}
#nav_rech a{color:#0C66A0;font-weight:bold;}
#nav_rech a:hover{text-decoration:none;}

.list01 ul{margin-left:12px;padding:0;list-style-image:url(bullet_list01.gif);}
* html .list01 ul{margin-left:10px;}
.list01 li {margin-bottom:5px;margin-top:5px;}
.list02  ul{margin:0 0 15px 15px;padding:0 0 6px 0;padding:0;list-style-image:url(bullet_list02.gif);}
* html .list02  ul{margin:0 0 15px 10px;}
.list02 li {margin-bottom:5px;}
.list03 ul{margin:10px 0 0 12px;padding:0;list-style-image:url(bullet_list01.gif);}
* html .list03 ul{margin-left:10px;}
.list03 li {margin-bottom:5px;}

/* CSS DIVERS */
.tiret01 {border-bottom:1px solid #C2CDE8;height:15px;margin-bottom:5px;margin-top:5px;}
/*.tiret03 {border-bottom:1px solid #C2CDE8;height:15px;margin-bottom:5px;margin-top:5px;text-align: left;}*/

.orange {color:#FF8C28;}
.bleu {color:#0C66A0;}
.noir {color:#000000;}

a.centreFAQ{color:#FF8C28;}
a.centreFAQ:hover{color:#0C66A0;text-decoration:underline;}


#main_content{margin : 0 10px 0 10px;}

.a_la_une {margin:10px 0 0 0 ; width:100%;border:0px solid #000000;background-color:#EEF5FB;}
.a_la_une .title{color:#0C66A0;font-size:2em;font-weight:bold;}
.a_la_une .sub_title{color:#0C66A0;font-size:1.45em;font-weight:bold;}
.a_la_une .accroche{color:#000000;font-size:1.1em;font-weight:bold;}
.a_la_une p{color:#000000;font-size:1em;}
.a_la_une img{margin:0 15px 0 15px;float:left;}

.accueil_thematique {width:100%;border:0px solid #000000;margin: 2px 0 2px 0;}
.accueil_thematique .title{color:#FF8C28;margin:0;height:21px;font-size:1.3em;font-weight:bold;font-style:normal;}
.accueil_thematique .sub_title{color:#000000;font-size:1.1em;font-weight:bold;}
.accueil_thematique p{color:#000000;font-size:1em; }
.accueil_thematique img{margin:0 15px 0 15px;float:left;}
.accueil_thematique .lisere {background:url(pixel_orange.gif) bottom repeat-x;}

.infos_pratiques {width:100%;border:0px solid #000000;color:#000000;font-size:1em;margin:2px 0 2px 0; padding:0 5px 0 5px;}
.infos_pratiques .title{color:#0C66A0;font-size:1.45em;font-weight:bold;}
.infos_pratiques .title p{margin:0 2px 0 0;float:left;}
.infos_pratiques .title img{padding:0.2em 0 0 0;}
.infos_pratiques .trait_bleu_hori {background : url(pixel_bleu.gif) top right repeat-x;width:100%;}
.infos_pratiques .trait_bleu_vert {background : url(bord_bleu.gif) top left no-repeat;}
.infos_pratiques a {}
.infos_pratiques a.toutes_vos_ { font-size: 0.91em; text-decoration:none; color:#1B75BC;}
.infos_pratiques a.toutes_vos_:hover {text-decoration:underline;}
.infos_pratiques a.theme {color:#000000;font-weight:bold;}
.infos_pratiques a.theme:hover {text-decoration:underline;}
.infos_pratiques a.centre {font-size: 1em; color:#000000;}
.infos_pratiques a.centre:hover {text-decoration:underline;}
.infos_pratiques a.centreAccroche {font-size: 1em; color:#1B75BC;}
.infos_pratiques a.centreAccroche:hover {text-decoration:underline;}
.infos_pratiques a.lienListeTheme {font-size: 1em; color:#1B75BC;text-decoration:none;font-weight:normal;}
.infos_pratiques a.lienListeTheme:hover {text-decoration:underline;}
.infos_pratiques a.toutes_vos_ { font-size: 0.91em; text-decoration:none; color:#1B75BC;}
.infos_pratiques a.toutes_vos_:hover  {text-decoration:underline;}
.infos_pratiques .bloc_bleu {padding:11px 10px 5px 10px; background-color:#A4C8E4;background:url(demarche.jpg) no-repeat;height:100px;}
.infos_pratiques .bloc_bleu_abbonnement {padding:11px 10px 5px 10px; background-color:#A4C8E4;background:url(fond_abonnez.jpg) repeat;height:129px;}
.infos_pratiques ul {padding:0; margin:10px 0 0 15px;font-weight:bold;}
.infos_pratiques .bloc_bleu_abbonnement input,.infos_pratiques .bloc_bleu_abbonnement select{font-size: 0.9em;height:1.5em;width:5.5em;}

#menu_droit {border:0px solid #000000;background-color:#E1EDF6;}
#menu_droit .title{color:#0C66A0;font-size:1.3em;font-weight:bold;padding:10px;}
#menu_droit .sub_title{color:#0C66A0;font-size:1.3em;font-weight:bold;padding:10px;}
#menu_droit .txt{color:#000000;font-size:1em;padding:5px;}
#menu_droit .image {text-align:center;}
#menu_droit a.menuDroit { font-size: 1em; text-decoration:none; color:#1B75BC;font-weight:bold;}
#menu_droit a.menuDroit:hover { text-decoration:underline;}
#menu_droit hr{color:#0C66A0;border:1px solid;height:1px;}


/* STYLE MENU HEADER */
#main_menu { margin:0px 0 0 0;padding:0 0 0 15px;font-size:1.1em;border-color:#FFCC00;}
#main_menu a.menuBarre  {text-decoration: none; font-weight:bold;color: #0867A0;position:relative;}
#main_menu a.menuBarre:hover {color:#FF8C28;}
#main_menu p {float:left;}
#conteneur_smenu {padding-top:15px;margin : 0 15px 0 15px;}
#conteneur_smenu div.smenu{font-size: 0.9em;position:relative;display:none;}
#conteneur_smenu div.smenu a{color: #0867A0; font-weight:normal;text-decoration:underline;}
#conteneur_smenu div.smenu a:hover {color: #FF8C28;}

#main_menu_logged_root { margin:0px 0 0 0;padding:0 0 0 15px;font-size:1.1em;color:#0867A0; border-color:#FFCC00;}
#main_menu_logged_root a.menuBarre { position:relative;text-decoration: none; font-weight:bold;color: #0867A0;}
#main_menu_logged_root a.menuBarre:hover {color:#FF8C28;}
#main_menu_logged_root p {float:left;}
#conteneur_smenu_logged_root {padding-top:0px;margin : 0 15px 0 15px;}
#conteneur_smenu_logged_root div.smenu{font-size: 0.9em;position:relative;display:none;}
#conteneur_smenu_logged_root div.smenu a{color: #0867A0; font-weight:normal;text-decoration:underline;}
#conteneur_smenu_logged_root div.smenu a:hover {color: #FF8C28;}

#main_menu_logged_user { margin:0px 0 0 0;padding:0 0 0 15px;font-size:1.1em;color:#0867A0; border-color:#FFCC00; }
#main_menu_logged_user a.menuBarre {text-decoration: none; font-weight:bold;color: #0867A0;position:relative;}
#main_menu_logged_user a.menuBarre:hover {color:#FF8C28;}
#main_menu_logged_user p {float:left;}
#conteneur_smenu_logged_user {padding-top:15px;margin : 0 15px 0 15px;}
#conteneur_smenu_logged_user div.smenu{font-size: 0.9em;position:relative;display:none;}
#conteneur_smenu_logged_user div.smenu a{color: #0867A0; font-weight:normal;text-decoration:underline;}
#conteneur_smenu_logged_user div.smenu a:hover {color: #FF8C28;}

/* STYLE NIVEAU 1 */
#presentation_Niv1 {margin:30px 0 0 0 ; width:100%;border:0px solid #000000;background-color:#EEF5FB;}
#presentation_Niv1 .title{color:#0C66A0;font-size:2em;font-weight:bold;}

#corp_Niv1 {margin:30px 0 0 0 ; width:100%;}
#corp_Niv1 .sub_title{color:#0C66A0;font-size:1.25em;font-weight:bold;}
#corp_Niv1 p{color:#000000;font-size:1em;}
#corp_Niv1 img{margin:0 15px 0 15px;float:left;}

