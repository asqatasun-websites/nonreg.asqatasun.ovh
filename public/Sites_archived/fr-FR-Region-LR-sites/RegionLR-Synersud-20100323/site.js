window.addEvent('domready', function()
{
	var mainPath = "fileadmin/templates/";
	/* SUCKERFISH MENU */
	if(Browser.Engine.trident && Browser.Engine.version < 5)
	{
		//Suckerfish Alternative for IE, using MOOTOOLS.
		$$('#menu li').each(function(el, index)
		{
			el.addEvents(
			{
				'mouseenter':function(e)
				{
					this.addClass('over');
				},
				'mouseleave':function(e)
				{
					this.removeClass('over');
				}
			});
		});
	}
	
	if ($('responsablesMb')) {
		
		var sortableListsArray = $$('#sortables');
		var sortableLists = new Sortables(sortableListsArray, {
			clone: true,
			handle: '.handle', 
			revert: {
				//accepts Fx options
				duration: 50
			},
			opacity: .1,
			onStart: function(el){
				//passes element you are dragging
				//$('start_ind').highlight('#F3F865');	
				el.highlight('#F3F865');	
			},
			onSort: function(el) {
				//passes element you are dragging
				//$('sort_ind').highlight('#F3F865');
			},
			onComplete: function(el) {
				
				var liste = sortableLists.serialize(0);
				
				var reqResp = new Request({
					url:'http://www.synersud.com/index.php?id=65', 
					method: 'post',
					data : 'fesynersud_liste='+liste,
					encoding:'iso-8859-1',
					onSuccess: function(html) {
						$('messageAjax').set('html', html);
					},
					onFailure: function() {
						$('messageAjax').set('text', 'Le compte n\'est pas accéssible pour l\'instant.');
					},
					onRequest: function() {
						$('messageAjax').addClass('waiting');
					}
					
				}).send();
				
			}
		});
		
	}

	var testD = function(el, ind)
	{
		var dd = $$('.logo a')[ind];
		if(dd)
		{
			// alert('coucou');
			var mtop = (dd.getSize().y - el.getSize().y) / 2;
			el.setStyle('margin-top',mtop.round());
		}
	};
	
	$$('.logo a img').each(function(el, ind){
		testD(el, ind);
	});
	
	var testF = function(el, ind)
	{
		var dd = $$('#ficheMenuPrinc .item a')[ind];
		if(dd)
		{
			// alert('coucou');
			var mtop = (dd.getSize().y - el.getSize().y) / 2;
			el.setStyle('margin-top',mtop.round());
		}
	};
	
	$$('#ficheMenuPrinc .item a img').each(function(el, ind){
		testF(el, ind);
	});
	
	if($('actus'))
	{
		var scroller = new fmcScrollTo({
			slides: $$('#actus .actu'),
			container: $('actus'),
			duration:700
		});
		var steps = $$('#actus .actu').length; 
		var theStep = 0;
		var stepLength = 1;
		var scrollable = true;
		var stepsArray = new Array();
		var el = $('actusNav');
		var navCanMove = true;
		var btns = {
			next:el.getElements('.next')[0],
			prev:el.getElements('.prev')[0]
		};
		$('headline').addEvents(
		{
			'mouseenter':function()
			{
				navCanMove=false;
			},
			'mouseleave':function()
			{
				navCanMove=true;
			}
		});
		checkBTNS(scroller.startIndex, steps, stepLength, btns);
		el.getElements('.prev')[0].addEvent('click',function(e)
		{
			if(e) e.stop();
			if(scroller.startIndex != 0 && (scroller.startIndex-stepLength)>=0)
			{
				scroller.scrollToEl(scroller.startIndex-stepLength);
			}else{
				scroller.scrollToEl(0);
			}
			checkBTNS(scroller.startIndex, steps, stepLength, btns);
		});
		el.getElements('.next')[0].addEvent('click',function(e)
		{
			if(e) e.stop();
			if(scroller.startIndex < steps-1 && Math.ceil(scroller.startIndex + stepLength / stepLength) - 1 <= steps)
			{
				scroller.scrollToEl(scroller.startIndex+stepLength);
			}else{
				//alert('Last : '+ scroller.startIndex +' - '+ (stepLength*steps-stepLength));
				scroller.scrollToEl(stepLength*steps-stepLength);
			}
			checkBTNS(scroller.startIndex, steps, stepLength, btns);
		});
		var moveAlone = (function()
		{
			if(scroller.startIndex == steps-1 ) scroller.startIndex = -1;
			if(navCanMove) btns.next.fireEvent('click');
		}).periodical(5000, this);
	}
	if($$('.accordeon-c').length > 1)
	{
		var togglers = $$('.accordeon-c .csc-header', '.accordeon-c2 .csc-header');
		var elements = $$('.accordeon-c .tretre', '.accordeon-c2 .tretre');
		var myAccordion = new Fx.Accordion(togglers, elements, {alwaysHide:true});
	}
	
	var carte = new Swiff(mainPath+"flash/mapXML.swf", { id: 'carteAnim', width:295, height: 330, params: { 'wmode': 'transparent','AllowScriptAccess':'always' }, vars: { xmlFile:"index.php?id=51" } });
	carte.inject('carte');
	var myTips = new Tips('.secteurs img', {title:'title'});
});