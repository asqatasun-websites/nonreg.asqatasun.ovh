// ouverture de pop-up

function popupHelpXmlDocuments()
{
    var url="plugins/xml/HelpXmlDocuments.jsp";
    window.open(url,'','toolbar=no, scrollbars=no, status=no, location=no, directories=no, menubar=no, width=400, height=350');
}

function popupHelpChildPage()
{
    var url="plugins/childpages/HelpChildPage.jsp";
    window.open(url,'','toolbar=no, scrollbars=no, status=no, location=no, directories=no, menubar=no, width=400, height=350');
}

function popupActor(actor_id)
{
    var url="../../jsp/site/plugins/actor/PopupActor.jsp?actor_id="+actor_id;
    window.open(url,'','toolbar=no, scrollbars=yes, status=no, location=no, directories=no, menubar=no, width=350, height=450');
}

function popupPrintArticle(article_id)
{
    var url="../../jsp/site/plugins/article/PopupPrintArticle.jsp?article_id="+article_id;
    window.open(url,'','toolbar=no, scrollbars=yes, status=no, location=no, directories=no, menubar=yes, width=550, height=500');
}

function popupSendArticle(article_id)
{
    var url="../../jsp/site/plugins/article/PopupSendArticle.jsp?article_id="+article_id;
    window.open(url,'','toolbar=no, scrollbars=yes, status=no, location=no, directories=no, menubar=no, width=600, height=420');
}

function popupCredits()
{
    var url="../../jsp/site/PopupCredits.jsp";
    window.open(url,'','toolbar=no, scrollbars=yes, status=no, location=no, directories=no, menubar=no, width=420, height=420');
}
function popupLegalInfo()
{
    var url="../../jsp/site/PopupLegalInfo.jsp";
    window.open(url,'','toolbar=no, scrollbars=yes, status=no, location=no, directories=no, menubar=no, width=420, height=420');
}

function popupUnregistrationAdmin( email, newsletter_id )
{
	var url='../../../../jsp/admin/plugins/newsletter/UnsubscribeNewsLetter.jsp?subscriber_email=' + email + '&newsletter_id=' + newsletter_id ;
	window.open(url,'','toolbar=no, scrollbars=yes, status=no, location=no, directories=no, menubar=no, width=420, height=420');
}

function popupPlugin(plugin_name)
{
	var url="PluginPopup.jsp?plugin_name="+plugin_name;
	window.open(url,'','toolbar=no, scrollbars=yes, status=no, location=no, directories=no, menubar=no, width=390, height=500');
}

