(function($){
	$.facebox = function(data, klass){
		init(data);
		$.facebox.loading();
		if(data.ajax || data.url){
			fillFaceboxFromAjax(data.url ? data.url : data.ajax, klass);
		}else if($.isFunction(data)){
			data.call($);
		}else{
			$.facebox.reveal(data, klass);
		}
	};
	$.extend($.facebox, {
		settings: {
			url:false,
			overlay : 0.25,
			titre : '',
			skin : 'facebook',
			width : '480px',
			loadingImage : 'loading.gif',
			closeImage : 'null.gif',
			imageTypes : ['png', 'jpg', 'jpeg', 'gif'],
			faceboxHtml : '\
<div id="facebox" style="display:none;"> \
	<div class="popup"> \
		<table> \
			<tbody> \
				<tr><td class="tl"/><td class="b"/><td class="tr"/></tr> \
				<tr> \
					<td class="b"/> \
						<td class="body"> \
							<div class="header"> \
								<div class="titleb"></div> \
								<div class="closeb"><a href="javascript:;" class="close"><img src="/api/js/facebox/null.gif" width="14px" height=14px" title="Fermer" class="close_image" /></a></div> \
								<div style="clear:both;"></div> \
							</div> \
							<div class="content"></div> \
						</td> \
					<td class="b"/> \
				</tr> \
				<tr><td class="bl"/><td class="b"/><td class="br"/></tr> \
			</tbody> \
		</table> \
	</div> \
</div>'
		},
		loading: function(){
			init();
			if($('#facebox .loading').length == 1){
				return true;
			}
			showOverlay();
			$('#facebox .content').empty();
			$('#facebox .body').children().hide().end().append('<div class="loading"><img src="/api/js/facebox/'+$.facebox.settings.loadingImage+'"/></div>');
			$('#facebox').css({top:'100px', left: '50%', 'margin-left': '-'+($.facebox.settings.width.replace('px', '') / 2)+'px'}).show();
			$(document).bind('keydown.facebox', function(e){
				if(e.keyCode == 27){
					$.facebox.close();
				}
				return true;
			});
			$(document).trigger('loading.facebox');
		},
		reveal: function(data, klass, titre){
			$('#facebox .body').addClass($.facebox.settings.skin);
			$(document).trigger('beforeReveal.facebox');
			if (klass){
				$('#facebox .content').addClass(klass);
			}
			if(titre){
				$('#facebox .header .titleb').html(titre);
			}
			$('#facebox .content').append(data);
			$('#facebox .loading').remove();
			$('#facebox .body').children().fadeIn('normal');
			$('#facebox').css({'top': ($(window).scrollTop() + ($(window).height() / 10))});
			$('#facebox').css({'left':'50%', 'margin-left': '-'+Math.floor($('#facebox table').width() / 2)+'px'});
			$(document).trigger('reveal.facebox').trigger('afterReveal.facebox');
		},
		close: function(){
			$(document).trigger('close.facebox');
			return false;
		}
	});
	$.fn.facebox = function(settings){
		init(settings);
		function clickHandler(){
			$.facebox.loading(true);
			var klass = this.rel.match(/facebox\[?\.(\w+)\]?/);
			if (klass){
				klass = klass[1];
			}
			fillFaceboxFromAjax(this.href, klass);
			return false;
		}
		return this.click(clickHandler);
	};
	function init(settings) {
		$(document).trigger('init.facebox');
		var imageTypes = $.facebox.settings.imageTypes.join('|');
		$.facebox.settings.imageTypesRegexp = new RegExp('\.' + imageTypes + '$', 'i');
		if (settings){
			$.extend($.facebox.settings, settings);
		}
		if($('#facebox').length == 0){
			$('body').append($.facebox.settings.faceboxHtml);
		}
		$('#facebox .body').css({width:$.facebox.settings.width}).removeClass().addClass('body').addClass($.facebox.settings.skin);
		$('#facebox .header .titleb').html($.facebox.settings.titre);
		if($('#facebox').length == 0){
			var preload = [new Image(), new Image()];
			preload[0].src = '/api/js/facebox/'+$.facebox.settings.closeImage;
			preload[1].src = '/api/js/facebox/'+$.facebox.settings.loadingImage;
			$('#facebox').find('.b:first, .bl, .br, .tl, .tr').each(function(){
				preload.push(new Image());
				preload.slice(-1).src = $(this).css('background-image').replace(/url\((.+)\)/, '$1');
			});
		}
		$('#facebox .close').click($.facebox.close);
		$('#facebox .close_image').attr('src', '/api/js/facebox/'+$.facebox.settings.closeImage);
	};
	function fillFaceboxFromAjax(href, klass){
		$.ajax({
			url: href,
			error: function(err){
				if(err.status == '404'){
					$.facebox.reveal("<div class='main' style='text-align:center;'><h1>Une erreur est survenue !</h1>Veuillez r�essayer ulterieurement.</div><div class='footer'><input type='button' value='Fermer' class='cancel' onclick=\"$('.close_image').trigger('click');\" /></div>", klass, 'Erreur !');
				}
			},
			success: function(data){
				$.facebox.reveal(data, klass);
			}
		});
	}
	function showOverlay() {
		if($.facebox.settings.overlay > 0){
			if($('facebox_overlay').length == 0){ 
				$("body").append('<div id="facebox_overlay" class="facebox_hide"></div>');
			}
			$('#facebox_overlay').hide().addClass("facebox_overlayBG").css('opacity', $.facebox.settings.overlay).click(function(){
				$(document).trigger('close.facebox');
			}).fadeIn(200);
			return false;
		}else{
			return;
		}
	}
	function hideOverlay() {
		if($.facebox.settings.overlay > 0){
			$('#facebox_overlay').fadeOut(200, function(){
				$("#facebox_overlay").removeClass("facebox_overlayBG");
				$("#facebox_overlay").addClass("facebox_hide");
				$("#facebox_overlay").remove();
			});
			return false;
		}else{
			return;
		}
	}
	$(document).bind('close.facebox', function(){
		$(document).unbind('keydown.facebox');
		$('#facebox').fadeOut(function(){
			$('#facebox .content').removeClass().addClass('content');
			hideOverlay();
			$('#facebox .loading').remove();
		});
	});
})(jQuery);
