/****************************************************************************************/
/**************************** Survol IE >> afficher le sous menu  ***********************/
/***************************************************************************************/	
sfHover = function() {
	
	var sfEls = document.getElementById("menu_header").getElementsByTagName("li");
	for (var i=0; i<sfEls.length; i++) {
		sfEls[i].onmouseover=function() {
			this.className+=" sfhover";			
		}
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
		}
	}
}

function closeAllUl()
{
	var ulNumber = document.getElementById('ulNumber').value;
	var currentUl = null;
	for(var i=0; i<=ulNumber; i++)
	{
		currentUl = document.getElementById('ul'+i);
		if(currentUl!=null)
		{
			currentUl.className = "hide";
			currentUl.parentNode.className = "close";
		}
	}
}

function expand(ulId)
{
	var ulToExpand = document.getElementById(ulId);
	if(ulToExpand != null)
	{
		if(ulToExpand.className == "")
		{
			ulToExpand.className = "hide";
			ulToExpand.parentNode.className = "close";
		}
		else
		{
			closeAllUl();
			ulToExpand.className = "";
			ulToExpand.parentNode.className = "open";
		}
	}
}


if (window.attachEvent) window.attachEvent("onload", sfHover);