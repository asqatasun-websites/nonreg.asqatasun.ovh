window.addEvent('domready', function(){	
	var flashMessage = false;
	var overlay = false;
	var fx = false;
	var height = false;
	var overlayOpacity = 0.5;
	
	if($('flashMessage'))
	{
		flashMessage = $('flashMessage');
		overlay = new Element("div", {id: "flashMessageOverlay"});
		positionOverlay();
		
		fx = new Fx.Tween(flashMessage);
		
		flashMessage.setStyle('visibility', 'visible');
		
		var height = flashMessage.getHeight() - parseInt(flashMessage.getStyle('padding-top'));
		
		flashMessage.setStyle('margin-top', '-' + height + 'px');				
		
		fx.start('margin-top', '-' + (height+50) + 'px', '-' + height + 'px');
		
		if(Browser.Platform.name =="mac" && Browser.Engine.name == "gecko" && Browser.Engine.version < 19)
		{
			flashMessage.setStyle('opacity', 1);
			overlay.setStyle('opacity', overlayOpacity);			
		}
		else
		{
			overlay.injectAfter(flashMessage);
			overlay.fade(0, overlayOpacity);
			flashMessage.fade(0,1);
		}
		
		flashMessage.setStyles({
			'margin-top': '-' + flashMessage.getHeight()/2 + 'px',
			'margin-left': '-' + flashMessage.getWidth()/2 + 'px'
		});		
		
		closeOverlay.delay(3000);
		
		window.addEvent("scroll", function(){positionOverlay();});
		window.addEvent("resize", function(){positionOverlay();});
	}	
	
	function positionOverlay()
	{
		flashMessage.setStyles({'top': parseInt(window.getScrollTop() + (parseInt(window.getHeight()) / 2) - flashMessage.getHeight()) + 60 + 'px'});
		overlay.setStyles({'top': window.getScrollTop(), 'height': window.getHeight()});	
	}
	
	function closeOverlay()
	{
		fx.start('margin-top', '-' + height + 'px', '-' + (height+50) + 'px');
		if(Browser.Platform.name =="mac" && Browser.Engine.name == "gecko" && Browser.Engine.version < 19)
		{
			flashMessage.destroy();
			overlay.destroy();
		}
		else
		{
			flashMessage.fade(1, 0);		
			overlay.fade(0.2, 0);			
			removeOverlay.delay(1500);
		}
	}
	
	function removeOverlay()
	{
		flashMessage.destroy();
		overlay.destroy();
	}

});