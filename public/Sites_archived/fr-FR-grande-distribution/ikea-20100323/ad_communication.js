function postData(form,layout) {
	try {
	switch (layout) {
		case ( "optinnewsletter"):
			subscribeNewsletter(form);
		break
		default:
			alert('no handler found');
			//return true if no handler is found - then the form will be submited as normal
			return  true;
		break
	}
	//if a handler is found, return false - then the form wont be submited
	return false;
	} catch (e) {
		dev_e=e;
		return false;
		//return true if something goes wrong - then the form will be submited as normal
		return  true;
	}
}
function showError(string) {
	alert(string);
	//$('newsletterFormErrorContainer').show();
}
function subscribeNewsletter(form) {
	if (!form.action.endsWith('xml')) {
		form.action=form.action+'&returnType=xml';
		errorHeadline = $('newsletterFormErrorContainer').innerHTML;
		//set up loader
		$('progressbar').hide();
		$('progressbar').update("<img src=\"/ms/img/form/ajaxloader.gif\">");
	} else {
		$(form).getElements().each(function(item) {
			if(item.name=='zipCode' || item.name=='storeNumber' || item.name=='email1') {
				item.setStyle({
					backgroundColor: '#FFF'
				});
			}
		});
		$('newsletterFormErrorContainer').hide();
	}
	//hide all form fields and show a loader
	$(form).immediateDescendants().each(function(item) {
		item.setStyle({
			visibility: 'hidden'
		});
	});
	//disable the form until we have an answer
	$('progressbar').show();
	$('progressbar').setStyle({
			visibility: 'visible',
			margin: '-8px'
	});	
	$(form).request({ 
		method: 'get', 
		onFailure: function () {
			//failed fetching product information page... not correct status code
			showError('newsletter subscription service unavailable');
		},
		onException: function (instance,object) {
			//failed fetching product information page... misc error
			showError('misc error while loading');
			dev_instance=instance;
			dev_object=object;
		},
		onInteractive: function () {
			//show progress here
		},
		onSuccess: function(transport){ 
			$('progressbar').hide();	
			var requestXML = transport.responseXML;
				var validation  = requestXML.getElementsByTagName('validation')[0];
				var validationStatus = validation.attributes[1].nodeValue;
				if (validationStatus != 'true') {
					//handle errors
					var errorFields = validation.getElementsByTagName('field');
					var errorList='<ul>';
					for (i=0;i<errorFields.length;i++) {
						var item = errorFields[i];
						var itemName=item.getElementsByTagName('name')[0].firstChild.data;
						var itemMessage=item.getElementsByTagName('message')[0].firstChild.data;
						errorList=errorList+'<li>- '+itemMessage+'</li>';
						$(form).getElements().each(function(item) {
							if (item.name == itemName) {
								item.setStyle({
									backgroundColor: '#FF9797'
								});
							}
						});
					}
					errorList=errorList+'</ul>';
					//the answer returned erros, enable the form again
					$(form).immediateDescendants().each(function(item) {
						item.setStyle({
							visibility: 'visible'
						});
					});					
					$('newsletterFormErrorContainer').update(errorHeadline+errorList);
					$('newsletterFormErrorContainer').show();
					$('newsletterFormErrorContainer').siblings()[1].hide();
				} else {
					//post ok
					$('newsletterFormContainer').hide();
					$('newsletterFormConfirmationContainer').show();
					$('newsletterFormErrorContainer').hide();
				}
			return;
		}
	});
}
function subscribeNewsletter_error() {

}
function retest() {
	$('newsletterFormConfirmationContainer').hide();
	$('newsletterFormContainer').show();
	$('newsletterFormErrorContainer').hide();
	$('progressbar').update('');
	return false;
}
