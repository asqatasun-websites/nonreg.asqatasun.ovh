function init_survey() {}


/* for backward compatibility / facelift */
function setPositions() {
	var footer = getLayer("ms-footer");
	if(footer) {
		footer.style.display = "block";
	}
}

if (typeof handleScroll == "undefined") {
	function handleScroll() {}
}
if (typeof setDocumentPosition == "undefined") {
	function setDocumentPosition() {}
}
function initPage(statustext) {
	setStatusText(escape(statustext));
}

/*** showroom ***/
var showroomImg = new Object();
var showroomTo = null;
function showModel(id, imgSrc) {
	try {
		if(showroomTo != null) {
			clearTimeout(showroomTo);
		}
		showroomTo = setTimeout("showNewModel('" + id + "', '" + imgSrc + "')", 100);
	} catch(e) {}
}

function showNewModel(id, imgSrc) {
	try {
	getLayer("showroom$initial").style.display = "none";
	getLayer("showroom$img").style.display = "block";
	if(!showroomImg[id]) {
		showroomImg[id] = new Image(480, 255);
		showroomImg[id].src = imgSrc;
	}
	getLayer("showroom$img").getElementsByTagName("img")[0].src = showroomImg[id].src;
	getLayer("showroom$facts").style.display = "block";
	getLayer("showroom$factsinner").innerHTML = getLayer(id).innerHTML;
	} catch(e) {}
}


/* HPC */
var ishpc = false;

function setApplicationType(type) {
	if(document.all && !window.opera) {
		embflashlayer = getLayer("embflashie");
	} else {
		embflashlayer = getLayer("embflash");
	}

	if(type != null && type != "") {
		if(type == "hpc") {
			ishpc = true;

			if(location.href.indexOf(".skipintro") != -1) {
				embflashlayer.SetVariable("nowelcome", true);
				embflashlayer.setVariableFl10("nowelcome", true);
			} else {
				embflashlayer.SetVariable("nowelcome", false);
				embflashlayer.setVariableFl10("nowelcome", false);
			}
      addEvent(embflashlayer, "mouseover", ms_restartFlash);
		}
	}
}

/**
 * sets the default status text
 */
function setStatusText(statusText) {
	window.status = unescape(statusText);
	window.defaultStatus = unescape(statusText);
}

/**
 * opens a popup window
 */
var popUps = new Array();
var DEF_POPUP_ATTR = "width=440,height=550,left=20,top=20,scrollbars=yes,resizable=no,status=no";
var DEF_POPUP_NAME = "emb_popup";
function openChild(source, name, attributes) {
	if(!source) {
		return;
	}
	attributes = attributes || DEF_POPUP_ATTR;
	name = name || DEF_POPUP_NAME;
	if (!popUps[name] || popUps[name].closed==true) {
		window.open(source,name,attributes);
	} else {
		popUps[name].close();
		popUps[name] = window.open(source,name,attributes);
	}
}

var focusTimout;
function jsCallFromFlash(flashcall) {
	if(flashcall != null && flashcall != "") {
		var flashcall = flashcall.replace("return false;", "");
		eval(flashcall);
		if((flashcall.indexOf("window.open") >= 0) && neww) {
		  if(document.all && !window.opera) var embFlashLayer = getLayer("embflashie");
    	else var embFlashLayer = getLayer("embflash");
    	embFlashLayer.stopChapter();
    	focusTimeout = window.setTimeout("addFocusEvent()", 500);
    }
	}
}

function addFocusEvent()
{
  window.clearTimeout(focusTimeout);
  addEvent(window, "focus", resumeChapter);
}

function resumeChapter()
{
  removeEvent(window, "focus", resumeChapter);
  if(document.all && !window.opera) var embFlashLayer = getLayer("embflashie");
  else var embFlashLayer = getLayer("embflash");
  embFlashLayer.playChapter();
}

function jsCall(style)
{
  if(document.all && !window.opera) var embFlashLayer = getLayer("embflashie");
  else var embFlashLayer = getLayer("embflash");
  if(style == "stop") embFlashLayer.stopChapter();
  else embFlashLayer.playChapter();
}

/**
	some common helper functions
*/

function getLayer( strID )
{
	if( document.all && !window.opera )
		return document.all[ strID ];
	else
		return document.getElementById( strID );
}

function setClass(element, className) {
	if(element == null || className == null) {
		return;
	}

	if(document.all && !window.opera) {
		element.className = className;
	} else{
		element.setAttribute("class", className);
	}
}

function removeClass(element) {
	if(element == null) {
		return;
	}

	if(document.all && !window.opera) {
		element.className = "";
	} else{
		element.removeAttribute("class");
	}
}

function getClassName(element) {
	if(element == null) {
		return "";
	}
	var className = document.all && !window.opera ? element.className : element.getAttribute("class");
	return className == null ? "" : className;
}

/**
* set events, w3c-compatible and ie
*/
function addEvent(obj, eventType, func, useCaption)
{
	if (!obj || !eventType || !func) {
		return false;
	} else if (obj.addEventListener) {
		obj.addEventListener(eventType, func, useCaption);
		return true;
	} else if (obj.attachEvent) {
    var retVal = obj.attachEvent("on"+eventType, func);
		return retVal;
	} else {
		return false;
	}
}

/**
* remove events, w3c-compatible and ie
*/
function removeEvent(obj, eventType, func, useCaption)
{
	if (!obj || !eventType || !func) {
		return false;
	} else if (obj.removeEventListener) {
		obj.removeEventListener(eventType, func, useCaption);
		return true;
	} else if (obj.detachEvent) {
		var retVal = obj.detachEvent("on"+eventType, func);
		return retVal;
	} else {
		return false;
	}
}


/**
 * special handling for ie 6, to place the bottom of the window
 */
if(document.all && !window.opera) {
	try {
		if (navigator.appName=="Microsoft Internet Explorer" && navigator.appVersion.indexOf("MSIE")>0) {		
			var str = navigator.appVersion;
			var ind = parseInt(str.indexOf("MSIE"))+5;
			var ver = parseInt(str.substring(ind, ind+1));
			if (ver<7) {
				addEvent(window, "load", initFooterIE);
			}
		}
	} catch(e) {}
}

function initFooterIE() {
	try {
		var iframe = '<iframe src="/emb_iframeie6.html" width="1000" height="27" name="footerFrame" id="footerFrameie6" marginheight="0" marginwidth="0" frameborder="0" style="display: none;"></iframe>';
		if(!getLayer("footerFrameie6")) {
			objContainer = getLayer("cms_position") ? getLayer("cms_position") : document.body;
			objContainer.insertAdjacentHTML("BeforeEnd", iframe);	
		}
	} catch(e) {
	}
	window.onscroll = positionFooter;
	window.onresize = positionFooter;
	positionFooter();
}

function positionFooter() {
	try {
		var toolbarHeight = getLayer("cms_position") && getLayer("CFCToolBarDiv") ? getLayer("CFCToolBarDiv").offsetHeight : 0;
		var topValue = document.documentElement.clientHeight + document.documentElement.scrollTop - getLayer("ms-footer").offsetHeight - toolbarHeight;
		getLayer("ms-footer").style.position = "absolute";
		getLayer("ms-footer").style.display = "block";		
		// special value for t9091 in ie6 and only on author-instance
		if( getLayer("ms-footer").parentNode==getLayer("ms-website") && document.body.getAttribute('instance')=='Author' ) {
			topValue -= 44;
		}				
		getLayer("ms-footer").style.top = (topValue);
		var iframe = getLayer("footerFrameie6");
		if(iframe) {
			getLayer("footerFrameie6").style.top = topValue;
		}
	} catch(e) {}
}

function positionMBTitle() {
	var titleLayer = getLayer("ms-mb-title");
	var menuLayer = getLayer("ms-navi-home");
	var left = 0; // used as margin

	// Error handling
	if (titleLayer == undefined || menuLayer == undefined) {
		return;
	}

	// Add menu position and width
	left += menuLayer.offsetLeft;
	left += menuLayer.offsetWidth;

	// Set position tags
	menuLayer.style.position = "absolute";
	menuLayer.style.cssFloat = "none";
	titleLayer.style.position = "absolute";
	titleLayer.style.cssFloat = "none";

	// Set left position
	titleLayer.style.left = left + "px";
}

// stop flash if popup has been opened
function ms_checkFlashstop() {
	if(document.all && !window.opera) {
		embflashlayer = getLayer("embflashie");
	} else {
		embflashlayer = getLayer("embflash");
	}

	if(window.ms_setT998Cookie) {
		return;
	}
	if(embflashlayer == null || (!embflashlayer)) {
		ms_openT998Popup();
	} else {
		if(!window.ms_update) {
			ms_stopFlash();
		}
	}
}


function ms_stopFlash() {
	if(document.all && !window.opera) {
		embflashlayer = getLayer("embflashie");
	} else {
		embflashlayer = getLayer("embflash");
	}

	try{
		if(embflashlayer.PercentLoaded() != 100) {
			setTimeout("ms_checkFlashstop()", 500);
			return;
		}
	}
	catch(err){
	}
	
  if(ms_openT998Popup()) {
		embflashlayer.StopPlay();
		ms_checkPopupClose();
	}
}

function ms_openT998Popup() {
	if(!window.set_cookie) {
		return;
	}
	var ms_popupLink = document.cookie;
	if(ms_popupLink == null) {
		return false;
	}
	var ms_expTime = new Date();
  	set_cookie("$ms_externalFlash", "none", 1000 * 60 * 60 * 24, "/content");
	ms_popupLink = ms_popupLink.match(/\$ms_externalFlash=[^;]+/);
	if(ms_popupLink == null || ms_popupLink == "" || !ms_popupLink.length) {
		return false;
	}
	ms_popupLink = ms_popupLink[0].match(/=.+/)[0].substr(1);
	if(ms_popupLink == "none") {
    return false;
	}
	window["$ms_extPopup"] = window.open(unescape(ms_popupLink), "daimler");
	return true;
}

addEvent(window, "load", ms_checkFlashstop);

// checks, if opened popup has been closed to start flash again
function ms_checkPopupClose() {
	if(window["$ms_extPopup"] && !window["$ms_extPopup"].closed) {
		setTimeout("ms_checkPopupClose()", 500);
		return;
	}
	ms_restartFlash();
}

// go on with flash if popup has been closed
function ms_restartFlash() {
	if(document.all && !window.opera) {
		embflashlayer = getLayer("embflashie");
	} else {
		embflashlayer = getLayer("embflash");
	}

	if(embflashlayer == null || (!embflashlayer)) {
		return;
	}

	if((ishpc) && window["$ms_extPopup"]) {
		embflashlayer.Play();
	}

	if(!ishpc) {
		embflashlayer.Play();
	}
}
