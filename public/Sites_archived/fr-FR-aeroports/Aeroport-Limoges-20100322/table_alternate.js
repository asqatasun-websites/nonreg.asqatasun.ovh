function table_alternate() {
   $('TABLE.tableau_donnees tbody tr').each(function(index){
   if (index % 2 == 1)
	  //$(this).css('background-color','#F2F2F2');
	  $(this).toggleClass('tableau_donnees_alt');
	else
	  $(this).css('background-color','white');
   });
 }

 function table_alternate_vols_journaliers() {
   $('TABLE.tableau_vols_journaliers tbody tr').each(function(index){
   if (index % 2 == 1)
	  //$(this).css('background-color','#F2F2F2');
	  $(this).toggleClass('tableau_vols_journaliers_alt');
	else
	  $(this).css('background-color','white');
   });
 }

 
$(document).ready(function()
{
	table_alternate();
	table_alternate_vols_journaliers();
});