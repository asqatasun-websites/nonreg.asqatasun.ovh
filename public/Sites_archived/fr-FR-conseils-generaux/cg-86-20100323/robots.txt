User-agent: *
Disallow: /cms/
Disallow: /include/

Disallow: /accueil/utilitaires/
Disallow: /actualites/
Disallow: /cotes/
Disallow: /fichiers/
Disallow: /fileadmin/
Disallow: /no_cache/
Disallow: /pratique/guide-muses/
Disallow: /typo3conf/
Disallow: /upload/
Disallow: /uploads/tx_sqlipublications/
Disallow: /vous-cherchez/enseignement/devoir-de-memoire/ 
Disallow: /pratique/documents/

Sitemap: /sitemap.xml

