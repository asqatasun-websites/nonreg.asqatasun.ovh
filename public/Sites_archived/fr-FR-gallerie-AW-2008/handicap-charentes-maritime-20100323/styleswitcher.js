// JavaScript Document

var varChangeTxt = "Changez la taille du texte <a href=\"#\" class=\"txt_normal selected\" onclick=\"fontSize('medium'); return false;\">A<span class=\"hidden\"> Texte normal</span></a> <a href=\"#\" class=\"txt_large\" onclick=\"fontSize('large'); return false;\">A<span class=\"hidden\"> Texte grand</span></a>";

/* cookies functions */
function createCookie(name,value,days) {
  if (days) {
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    var expires = "; expires="+date.toGMTString();
  }
  else expires = "";
  document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}

/* loading functions */
function loadWhileLoading() {
	if (!document.body) {
		setTimeout('loadWhileLoading()',10)
	} else {
		setActiveStyleSheet();
		getCustomFontSize();
	}
}
loadWhileLoading();

function getCustomFontSize() {
	var fontsize = readCookie('fontsize');
	if (!fontsize) {
		fontsize = 'medium';
	}
	fontSize(fontsize);
}

function fontSize(size){
	var d = document.body;
	d.className = d.className.replace(/\b(medium|large|huge)\b/g,"")+" "+size;
	createCookie('fontsize',size,365);
	var fontSizeBlock = document.getElementById("fontsizeSwitcher");
	if (fontSizeBlock) {
		var as = fontSizeBlock.getElementsByTagName("a");
		for (var i=0; i<as.length; i++) {
			var x = as[i];
			var Xsize = (x.onclick+"").match(/fontSize\(.?([a-zA-Z0-9]+).?\)/);
			if (Xsize && Xsize.length>1) {
				Xsize=Xsize[1];
				x.className = x.className.replace(/selected/g,"")+ (size==Xsize ? " selected" : "");
				
			}
		}
	}
}
/* activate StyleSheet */ 
function setActiveStyleSheet(title,elm) {
	var blockOfLinks = document.getElementById("styleswitcher");
	if (blockOfLinks && blockOfLinks.offsetHeight==0) return;
	var customColor = title ? title : readCookie("style");
	var head = document.getElementsByTagName("head")[0];
	var link = head.getElementsByTagName("link");
	for (var i=0; i<link.length; i++) {
		var x = link[i];
		if (x.rel=="stylesheet" && !customColor && !title && x.href.match(/\bstruct[a-zA-Z0-9]+\.css\b/i)) {
			customColor = x.href.match(/\bstruct([a-zA-Z0-9]+)\.css\b/)[1];
		}
	}
	createCookie("style",customColor,365);
	for (var i=0; i<link.length; i++) {
		var x = link[i];
		if (x.href.match(/\bstruct[a-zA-Z0-9]+\.css\b/)) {
			x.disabled = true;
			if (x.href.indexOf("struct"+customColor+".css")>=0) {
				x.disabled = false;
			}
		}
	}
	if (blockOfLinks) {
		var imgs = blockOfLinks.getElementsByTagName("img");
		for (var i=0; i<imgs.length; i++) {
			var x = imgs[i];
			var colorMatching = x.src.match(/\bpicto-skin-([a-zA-Z0-9]+)\./);
			if (colorMatching.length>1) {
				colorMatching = colorMatching[1];
				if (colorMatching.toLowerCase()!="current") x.oldColor = colorMatching;
				if (customColor && customColor.toLowerCase()==colorMatching.toLowerCase()) {
					x.src = x.src.replace(/picto-skin-[a-zA-Z0-9]+\./,"picto-skin-current.");
				} else {
					if (x.oldColor) {
						var colorReplacement = elm && elm==x.parentNode ? "current" : x.oldColor;
						x.src = x.src.replace(/picto-skin-current\./,"picto-skin-"+colorReplacement+".")
					}
				}
			}
			
		}
	}

	
}


/* addEvent */
function addEvent( obj, type, fn ) {
	if (obj.addEventListener)
		obj.addEventListener( type, fn, false );
	else if (obj.attachEvent)
	{
		obj["e"+type+fn] = fn;
		obj[type+fn] = function() { obj["e"+type+fn]( window.event ); }
		obj.attachEvent( "on"+type, obj[type+fn] );
	}
}



/*
var cookie = readCookie("style");
var title = cookie ? cookie : getPreferredStyleSheet();
setActiveStyleSheet(title);
*/
