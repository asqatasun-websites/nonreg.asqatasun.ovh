// Cookie io functions

function createCookie(name,value,days,compt,lastDate,nbrVisit) {
	var now = new Date();
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000*365));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
	document.cookie = nbrVisit+"="+compt+expires+";path=/";
	document.cookie = lastDate+"="+now+expires+";path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}


function fCompareDate(dateWcs,dateCookie) {
	var DureeDebut = Date.parse(dateWcs);
	var DureeFin = Date.parse(dateCookie);
	var iComparaison= DureeFin - DureeDebut;
	return iComparaison;
} 

function activePop(name,last_visit_name,last_date_name,nbrVisitCookie,nbrVisitCat,config,url){
		var win2;
		var date = new Date();
		if(nbrVisitCookie!=null){
			if (nbrVisitCookie<nbrVisitCat) {
				win2 = window.open(url,'popunderConforama',config);	
		       	win2.blur();
		       	eraseCookie(last_visit_name);
		       	var totalVisit = nbrVisitCookie+1;
				createCookie(name,date.getTime(),true,totalVisit,last_date_name,last_visit_name);
			} 
		}else{
			win2 = window.open(url,'popunderConforama',config);
       		if(win2!=null){
       			win2.blur();
       			createCookie(name,date.getTime(),true,1,last_date_name,last_visit_name);
       		}
		}	
} 

function Popunder(nameCat,url,compteur_visit,larg,long,dateInit){
	var isNumberLong = long;
	var isNumberLarg =  larg;
	if(isNaN(long) || isNaN(larg)){
		isNumberLong = screen.height;
		isNumberLarg = screen.width ;
	}
	var config = 'width='+isNumberLarg+',height='+isNumberLong+',toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1';
	var date_name = 'date_name'+nameCat;
	var visit_name = 'visit_name'+nameCat;
	var last_date_value = readCookie(date_name);
	var last_open_value = readCookie(nameCat);
	var last_visit_value = readCookie(visit_name);
	var date = new Date();
	if(dateInit != null){
		var temp = dateInit.split("-");
		var startDate = new Date(0);
		startDate.setFullYear(temp[0]);
		startDate.setMonth(parseInt(temp[1]-1));
		startDate.setDate(temp[2]); 
	}
	if(last_date_value != null && (startDate != null && isValidDate(startDate))){
		if(fCompareDate(startDate,last_date_value)< 0){
	       	activePop(nameCat,visit_name,date_name,0,compteur_visit,config,url);
		}else{
			if(last_visit_value != null){
				var compt_visit_cookie = parseInt(last_visit_value);
				if (compt_visit_cookie<compteur_visit) {
					activePop(nameCat,visit_name,date_name,compt_visit_cookie,compteur_visit,config,url);
				} 
			}
		}
	}else{
		if(last_visit_value != null){
				var compt_visit_cookie = parseInt(last_visit_value);
				if (compt_visit_cookie<compteur_visit) {
					activePop(nameCat,visit_name,date_name,compt_visit_cookie,compteur_visit,config,url);
				} 
		}else{
			if(compteur_visit>0){
				activePop(nameCat,visit_name,date_name,null,compteur_visit,config,url);
			}
		}
	}
        return(true);
}


function isValidDate(d) {
  if ( Object.prototype.toString.call(d) !== "[object Date]" )
    return false;
  return !isNaN(d.getTime());
}


function appelPopunder(url,choix_action,nameCookie,compteur,width,height,dateInit,hostPath){
	var urlCourant = document.location.href;
	if(choix_action != null && choix_action != "" ){
		if(choix_action == 0){
			if(url != null && url != ''){
				window.onbeforeunload = function () {
					Popunder(nameCookie,url,compteur,width,height,dateInit);
				}
			}
		}else{
			if(url != null && url != ''){
				if(url.lastIndexOf("/", 0)== -1){
					if(urlCourant.indexOf(choix_action)!= -1){
						Popunder(nameCookie,url,compteur,width,height,dateInit);
					}
				}else{
					var fichierExt = hostPath+url;
					if(urlCourant.indexOf(choix_action)!= -1){
						Popunder(nameCookie,fichierExt,compteur,width,height,dateInit);
					}
				}
			}
		}
	}
}