
/* The selected text function */

var selectedString="";

function getSelectedHTML() {
  selectedString="";
  var rng=undefined;
  if (window.getSelection) {
    selobj = window.getSelection();
    if (!selobj.isCollapsed) {
      if (selobj.getRangeAt) {
        rng=selobj.getRangeAt(0);
      }
      else {
        rng = document.createRange();
        rng.setStart(selobj.anchorNode,selobj.anchorOffset);
        rng.setEnd(selobj.focusNode,selobj.focusOffset);
      }
      if (rng) {
        DOM = rng.cloneContents();
        object = document.createElement('div');
        object.appendChild(DOM.cloneNode(true));
        selectedString=object.innerHTML;
      }
      else {
        selectedString=selobj;
      }
    }
  }
  else if (document.selection) {
    selobj = document.selection;
    rng = selobj.createRange();
    if (rng && rng.htmlText) {
      selectedString = rng.htmlText;
    }
    else if (rng && rng.text) {
      selectedString = rng.text;
    }
  }
  else if (document.getSelection) {
    selectedString=document.getSelection();
  }
}

function copyselected()
{
  setTimeout("getSelectedHTML()",50);
  return true;
}

document.onmouseup = copyselected;
document.onkeyup = copyselected;

/* The expanding function */

function readspeaker(rs_call)
{
  if (selectedString.length>0) {
    rs_call=rs_call.replace("/cgi-bin/rsent?","/enterprise/rsent_wrapper.php?");
  }
  savelink=rs_call+"&save=1";
  start_rs_table="<div style='border:1px solid #ed4d05; font-size: 10px;background: #fff; padding: 0.2em;'>";
  rs_embed="<object type='application/x-shockwave-flash' data='http://media.readspeaker.com/flash/readspeaker20.swf?mp3="+escape(rs_call)+"&autoplay=1&rskin=bump' height='20' width='250'><param name='movie' value='http://media.readspeaker.com/flash/readspeaker20.swf?mp3="+escape(rs_call)+"&autoplay=1&rskin=bump' /><param name='quality' value='high' /><param name='SCALE' value='exactfit' /><param name='wmode' value='transparent' /><embed wmode='transparent' src='http://media.readspeaker.com/flash/readspeaker20.swf?mp3="+escape(rs_call)+"&autoplay=1&rskin=bump' quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer' type='application/x-shockwaveflash' scale='exactfit' height='20' width='250' /></embed></object>";
  rs_downloadlink="<br /><a href='"+savelink+"'><img src='fileadmin/templates/internet/medias/images/commun/fichiers/fichier_mp3.png' alt='Format MP3' />TÚlÚcharger le fichier audio</a>";
  close_rs="<br /><a href='#' onclick='close_rs_div(); return false;' id='btn_fermer'>Fermer</a>";
  end_rs_table="</div>";

  var x=document.getElementById('rs_div');

  x.innerHTML=start_rs_table+rs_embed+rs_downloadlink+close_rs+end_rs_table;
}

function close_rs_div()
{
  var x=document.getElementById('rs_div');
  x.innerHTML="";
}