<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html style="overflow-x:hidden;">
<head>
	<meta http-equiv="Content-Type" content="text/HTML; charset=iso-8859-1">
	<meta http-equiv="expires" content="0">
	<meta name="description" content="Bienvenue sur le site de Languedoc Roussillon Amenagement">
	<title>Accueil</title>
	<script type="text/javascript">
	<!--
	function OuvrirLien(URL,NOM,FEATURE) {
		window.open(URL,NOM,FEATURE);
	}
	//-->
	</script>
	<script type="text/javascript" src="frontoffice.js"></script>
	<script type="text/javascript" src="formCheck.js"></script>
	<script type="text/javascript" src="cal2.js"></script>
			<link rel="stylesheet" href="serlr_accueil.css" type="text/css" media="all">
		<!-- En commentaire avant integration du FO
	<link rel="stylesheet" href="/include/styles/navigation.css" type="text/css" media="all">
	--></head>
<body>
<div id="document">
<a name="top"></a>
		<div id="planetcontact">
			
		

	<table width="780" cellpadding="0" cellspacing="0">
		<tr>
			<td width="380"><IMG SRC="spacer.gif" WIDTH="6" height="1" ALT=" "></td>
			<td width="157" align="center"><div id="plandusite"><a href="http://www.lr-amenagement.fr/pages/page.php?num=205&amp;lan=1&amp;PHPSESSID=4ec1caab1ca61696d3331da58b584694">plan du site</a></div></td>
			<td width="241"><div id="contact"><a href="http://www.lr-amenagement.fr/pages/page.php?num=204&amp;lan=1&amp;PHPSESSID=4ec1caab1ca61696d3331da58b584694">Contact</a></div></td>
		</tr>
	</table>
</div>
<div id="menuhaut">
	  <h1><a href="http://www.lr-amenagement.fr/pages/page.php?lan=1&amp;PHPSESSID=4ec1caab1ca61696d3331da58b584694"><img src="logo_LRA.gif" border="0"></a></h1>	
		<div class="image"></div>
		
	 <form action="http://www.lr-amenagement.fr/pages/page.php?num=167&amp;lan=1" name="Search" method="" style="margin:0;padding:0;"><input type="hidden" name="PHPSESSID" value="4ec1caab1ca61696d3331da58b584694" />
      <table width="100%" height="50" border="0" cellpadding="0" cellspacing="0">
        <tr> 
		  <td valign="bottom">Rechercher : </td>
          
        <td width="24"><img src="menuhaut_drapeaux.gif" width="24" height="31" border="0" usemap="#Map"></td>
        </tr>
        <tr> 
          <td colspan="2" align="center">
		  <input name="SearchString" class="recherche" type="text" value="" size="10"></td>
        </tr>
        <tr align="right" valign="top"> 
          <td colspan="2" class="ok"><a href="javascript:document.Search.submit();"><span class="blanc10V">Ok</span></a></td>
        </tr>
      </table>
    </form>
	
	</div>
<map name="Map">
    <area shape="rect" coords="0,-2,22,15">
		  <area shape="rect" coords="1,16,23,29">
	
</map>
		<TABLE cellSpacing=0 cellPadding=0 width="780" border="0">
	<TR>
			<td valign="top" width="10" class="ColonneGauche" id="ColonneGauche">
		
<div id="menugauche" class="clear">
	  <div class="menu menu1"><a href="http://www.lr-amenagement.fr/pages/page.php?num=190&amp;lan=1&amp;PHPSESSID=4ec1caab1ca61696d3331da58b584694" title="" >LR Am�nagement</a></div>  <div class="menu menu2"><a href="http://www.lr-amenagement.fr/pages/page.php?num=197&amp;lan=1&amp;PHPSESSID=4ec1caab1ca61696d3331da58b584694" title="" >Comp�tences</a></div>  <div class="menu menu3"><a href="http://www.lr-amenagement.fr/pages/page.php?num=200&amp;lan=1&amp;PHPSESSID=4ec1caab1ca61696d3331da58b584694" title="" >Modes d'intervention</a></div>  <div class="menu menu4"><a href="http://www.lr-amenagement.fr/pages/page.php?num=187&amp;lan=1&amp;PHPSESSID=4ec1caab1ca61696d3331da58b584694" title="" >Avis publics</a></div>  <div class="menu menu5"><a href="http://www.lr-amenagement.fr/pages/page.php?num=188&amp;lan=1&amp;PHPSESSID=4ec1caab1ca61696d3331da58b584694" title="" >T�l�chargements</a></div></div>
		</td>
		<td class="Contenu" valign="top" align="left">
					<div id="MenuUn">
								</div>
		
	<div id="contenu">
	<h1><p class="premierelettre">O</p>
	util du d�veloppement �conomique r�gional</h1>
	<h2><p class="premierelettre">A</p>ctualit&eacute;s</h2>
	<br class="clear">
<div id="zonetexte">
			<div id="fond">
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="left"><h3>Mises en concurrence en ligne</h3><p>Comme publi� dans plusieurs journaux officiels, Languedoc Roussillon Am�nagement met en ligne les avis de mise en concurrence sur son site internet, � la rubrique "Avis publics".</p>			<p align="right"><a href="http://www.lr-amenagement.fr/include/popupActus.php?idactu=38&amp;PHPSESSID=4ec1caab1ca61696d3331da58b584694" onclick="window.open(this.href,'Actus','height=250,width=637,left=100,top=100,resizable,scrollbars');return false;"  onkeypress="window.open(this.href,'Actus','height=450,width=500,left=100,top=100,resizable,scrollbars');return false;" class="liensuite">en savoir + &gt;&gt;</a> </p></td>
			</tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr></table><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="left"><h3>Avis publics en ligne</h3><p>Nos derniers avis publics sont consultables � la rubrique "Avis publics".</p>			<p align="right"><a href="http://www.lr-amenagement.fr/include/popupActus.php?idactu=39&amp;PHPSESSID=4ec1caab1ca61696d3331da58b584694" onclick="window.open(this.href,'Actus','height=250,width=637,left=100,top=100,resizable,scrollbars');return false;"  onkeypress="window.open(this.href,'Actus','height=450,width=500,left=100,top=100,resizable,scrollbars');return false;" class="liensuite">en savoir + &gt;&gt;</a> </p></td>
			</tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr></table>	
</div>
</div>
</div>	</td>
	
	</tr>
	</table>
	<div id="basdepage">&copy; LANGUEDOC ROUSSILLON AMENAGEMENT - <a href="http://www.lr-amenagement.fr/pages/page.php?num=241&amp;lan=1&amp;PHPSESSID=4ec1caab1ca61696d3331da58b584694#Anc1">Cr&eacute;dits</a></div>
	</div>
</body>
</html>