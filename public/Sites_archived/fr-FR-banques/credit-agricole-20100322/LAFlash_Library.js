// LA_Flash V2 -  2.0.0.3.12
var LA_AppName = navigator.appName.toLowerCase();
var AppVer = parseFloat(navigator.appVersion);
if (LA_AppName.indexOf("microsoft internet") != -1)
	AppVer = parseFloat(navigator.appVersion.substring((navigator.appVersion.indexOf("MSIE") + 4)));
var LA_WIN32 = (window.navigator.platform.indexOf("Win32")!= -1);
var LA_WIN95 = navigator.appVersion.indexOf("95")!= -1;
var LA_OSX = (window.navigator.userAgent.indexOf("Mac OS X") != -1);
var LA_NAV4  = (LA_AppName.indexOf("netscape") != -1 && AppVer >= 4.0 && AppVer <= 4.99);
var LA_NAV6  = (LA_AppName.indexOf("netscape") != -1 && AppVer >= 5.0 );
var LA_IE4   = (LA_AppName.indexOf("microsoft internet") != -1 && AppVer >= 4.0 && AppVer < 5.0);
var LA_IE5   = (LA_AppName.indexOf("microsoft internet") != -1 && AppVer >= 5.0);
var LA_IE   = (LA_AppName.indexOf("microsoft internet") != -1 && AppVer >= 4.0 );
var LA_SP2 = (window.navigator.userAgent.indexOf("SV1") != -1);
var LA_DomBody=(document.documentElement && document.documentElement.clientHeight)?document.documentElement:document.body;


//events
var LA_OnActorLoaded	= null;
var LA_OnDragStart	= null;
var LA_OnDragEnd	= null;
var LA_OnBookmark	= null;
var LA_OnDialogID	= null;
var LA_OnSelect		= null;
var LA_OnHide		= null;
var LA_OnShow		= null;
var LA_OnQuit		= null;
var LA_OnReady		= null;
var LA_OnSoundOn	= null;
var LA_OnSoundOff	= null;
var LA_OnNormalMode = null;
var LA_OnUserTextInput=null;
//status
var LA_E_UNINITIALIZED = 0;
var LA_E_INITIALIZED = 1;
var LA_E_INSTALLING = 2;
var LA_E_LOADING_ACTOR = 3;
var LA_E_STARTED = 4;
//globals
var ACTOR = null;
var LA_GENERIC_FLASH_PLAYER = null;
var LA_ACTOR_URL = ""; //internal use
var LA_UserDefined = ""; //internal use
var LA_DRAG_THREAD_ID = 0;
var LA_DIALOG_VISIBILITY = true;
var LA_FISRTPLAYERUPDATE = true;
var LA_UID = new Date();
LA_UID = LA_UID.getTime();
var LA_AllObservableAdded = false; // observer observables 
var LA_ObjectIDs = new Array();
var LA_ACTOR_DRAG_ENABLE = true;
var LA_USER_INPUT_LAYER = false;
var LA_USER_INPUT_DRAG = true;
var LA_DEBUG_LAYER = false;
var LA_PROGRESSBAR_VISIBLE = true;
//toggles
var TOGGLE_DOWNLOADING_DIALOGBOX = "1";
var TOGGLE_LOADING_DIALOG_DIALOGBOX = "2";
var TOGGLE_ACTOR_DIALOG = "3";
var TOGGLE_ANTIALIASING = "4";
var TOGGLE_SOUND = "5";
var TOGGLE_PAINTER = "7";
var TOGGLE_SYSTEM_ICON = "6";
var TOGGLE_TOOLBAR = "8";
var TOGGLE_MOUSE_INTERACTION = "9";
var TOGGLE_ACTOR_DRAG = "10";

function ActorLoading()
{
	if (LA_GENERIC_FLASH_PLAYER != null)
	{
		if (ACTOR==null)
		{
			ACTOR = new Actor(LA_GENERIC_FLASH_PLAYER);
			ACTOR.LoadActor(LA_ACTOR_URL);
		}
		if(Main_LivingActor)
		{
			Main_LivingActor(LA_UserDefined);
		}
	}
}

function Init_LivingActor(anActorPath,userValue)
{
	LA_ACTOR_URL = anActorPath;
	LA_UserDefined = userValue;
		if (LA_GENERIC_FLASH_PLAYER==null)
		{
			LA_GENERIC_FLASH_PLAYER = document.FLASH_PLAYER_ID;
		}
		ActorLoading();
}

function LoadNewActor(actorURL,dialogURL,uservalue)
{
	ACTOR.Stop();
	//reinit library variables..
	//LA_CurrentStatus = LA_E_UNINITIALIZED;
	ACTOR = null;
	LA_ACTOR_URL = ""; 
	LA_UserDefined = ""; 
	LA_DRAG_THREAD_ID = 0;
	document.getElementById("LA_DialogLayer").style.left = (parseInt(document.getElementById('LA_DialogLayer').style.left) - 2000)+"px";
	document.FLASH_DIALOG_ID.LoadMovie(0,dialogURL);
	Init_LivingActor(actorURL,uservalue);
}

function TerminateLivingActor()
{
	ACTOR = null;
	LA_GENERIC_FLASH_PLAYER = null;
}

function LA_BodySize()
{
	this.Width=LA_DomBody.scrollWidth;
	this.Height=LA_DomBody.scrollHeight;
}

function LA_WindowSize()
{
	if(LA_NAV6)
	{
	this.Height = window.innerHeight;
	this.Width = window.innerWidth;
	this.ScrollTop=window.pageYOffset;
	this.ScrollLeft=window.pageXOffset;
	}
	else if(LA_IE)
	{
	this.Height = LA_DomBody.clientHeight;
	this.Width = LA_DomBody.clientWidth;
	this.ScrollTop=LA_DomBody.scrollTop;
	this.ScrollLeft=LA_DomBody.scrollLeft;
	}
}

// observable object
function LA_ObservableReadyObject(LA_anObject,LA_aType){
	this.target = LA_anObject;
	this.type = LA_aType;
	this.readyflag = false;
	this.isReady = function(){
		if(this.type=="ExternalReady"){
			return this.readyflag;	
		}
		else if(this.type=="htmlObject"){
			anobject=LA_FindObjectInWindow(this.target);
			if(anobject)
			{
				anobjX=LA_findPagePos(anobject)[0];
				anobjY=LA_findPagePos(anobject)[1];
				if((anobjX==0) && (anobjY==0)){
					return false;
				}
				else return true;
			}
			else return true;
		}
	}
}

function LA_Add_ObservableObject(LA_anObject,LA_aType){
	LA_ObjectIDs[LA_ObjectIDs.length] = new LA_ObservableReadyObject(LA_anObject,LA_aType);
}

function LA_Get_ObservableObject(LA_aName){
	for(i=0;i<LA_ObjectIDs.length;i++){
		if(LA_ObjectIDs[i].target==LA_aName){
			return LA_ObjectIDs[i];
		}
	}
	return null;
}

// observation
function LA_Check_Objects_Ready_Loop(){
	if(LA_AllObservableAdded){
//alert("check ready loop: "+LA_ObjectIDs.length);
		for(i=0;i<LA_ObjectIDs.length;i++) {
			if(LA_ObjectIDs[i].isReady()==false){
				setTimeout("LA_Check_Objects_Ready_Loop()",500);
				return;
			}
		}
		if(LA_OnReady){LA_OnReady();}
	}
	else{
		setTimeout("LA_Check_Objects_Ready_Loop()",500);
	}
}

// fill already the observer with la_flash objects
LA_Add_ObservableObject("FLASH_PLAYER_ID","ExternalReady");
LA_Add_ObservableObject("FLASH_DIALOG_ID","ExternalReady");
//LA_Add_ObservableObject("FLASH_PROGRESSBAR_ID","ExternalReady");

//utility function
function LA_Add_ObservableHTMLObjectsArray(aStringList){
	anArray=aStringList.split(",");
	for(i=0;i<anArray.length;i++){
		LA_Add_ObservableObject(anArray[i],"htmlObject");
	}
}

function LA_Drag_Thread()
{
	LA_sizeOfWindow =new LA_WindowSize;
	LA_sizeOfBody =new LA_BodySize;

	var LA_actorLeft = parseInt(document.getElementById('LA_ActorLayer').style.left) + ACTOR.f_DRAG_X;
	var LA_actorTop = parseInt(document.getElementById('LA_ActorLayer').style.top) + ACTOR.f_DRAG_Y;
	var LA_actorWidth = parseInt(document.getElementById('LA_ActorLayer').style.width);
	var LA_actorHeight = parseInt(document.getElementById('LA_ActorLayer').style.height);
	var LA_windowWidth = LA_sizeOfBody.Width;
	var LA_windowHeight = LA_sizeOfBody.Height;
	LA_actorLeft = (LA_actorLeft+(LA_actorWidth/2)<0)?-(LA_actorWidth/2):LA_actorLeft;
	LA_actorTop = (LA_actorTop<0)?0:LA_actorTop;
	document.getElementById('LA_ActorLayer').style.left = LA_actorLeft+"px";	
	document.getElementById('LA_ActorLayer').style.top = LA_actorTop+"px";
	
	if (ACTOR.f_isSpeaking && LA_DIALOG_VISIBILITY)
	{
		//var LA_dialogLeft = parseInt(document.getElementById('LA_ActorLayer').style.left) + parseInt(document.getElementById('LA_ActorLayer').style.width) / 2 - parseInt(document.getElementById('LA_DialogLayer').style.width) / 2;
		var LA_dialogWidth = parseInt(document.getElementById('LA_DialogLayer').style.width);
		var LA_dialogHeight = parseInt(document.getElementById('LA_DialogLayer').style.height);
		var LA_dialogTop = LA_actorTop + ACTOR.f_BBoxMinY - LA_dialogHeight;
		var LA_dialogLeft = (LA_actorLeft + (LA_actorWidth /2)) - (LA_dialogWidth /2);
		
		LA_dialogLeft = LA_dialogLeft <0?0:LA_dialogLeft;
		LA_dialogTop = (LA_dialogTop+LA_dialogHeight < ACTOR.f_DialogHeight)?ACTOR.f_DialogHeight-LA_dialogHeight:LA_dialogTop;
		LA_dialogLeft = ((LA_dialogLeft+LA_dialogWidth) > LA_windowWidth)?LA_windowWidth-LA_dialogWidth:LA_dialogLeft;
		LA_dialogTop = ((LA_dialogTop+LA_dialogHeight) > LA_windowHeight)?LA_windowHeight-LA_dialogHeight:LA_dialogTop;
		
		if (LA_dialogTop+LA_dialogHeight <= ACTOR.f_DialogHeight)
		{
			if (LA_actorLeft + ACTOR.f_BBoxMaxX +LA_dialogWidth < LA_windowWidth)
			{
				LA_dialogLeft = LA_actorLeft + ACTOR.f_BBoxMaxX;
			}
			else
			{
				LA_dialogLeft = LA_actorLeft + ACTOR.f_BBoxMinX - LA_dialogWidth;
			}
		}
		document.getElementById('LA_DialogLayer').style.left = LA_dialogLeft+"px";	
		document.getElementById('LA_DialogLayer').style.top = LA_dialogTop+"px";		
	}	
	if(LA_USER_INPUT_LAYER)
	{
		document.getElementById('LA_UserInputLayer').style.left = (LA_actorLeft + (LA_actorWidth /2)) - (parseInt(document.getElementById('LA_UserInputLayer').style.width) /2)+"px";	
		document.getElementById('LA_UserInputLayer').style.top = LA_actorTop + LA_actorHeight+"px";	
	}
}

// Hook for Internet Explorer ( copyright macromedia)
if (navigator.appName && navigator.appName.indexOf("Microsoft") != -1 && navigator.userAgent.indexOf("Windows") != -1 && navigator.userAgent.indexOf("Windows 3.1") == -1) {
	document.write('<script language=\"VBScript\"\>\n');
	document.write('On Error Resume Next\n');
	document.write('Sub FLASH_PLAYER_ID_FSCommand(ByVal command, ByVal args)\n');
	document.write('	Call FLASH_PLAYER_ID_DoFSCommand(command, args)\n');
	document.write('End Sub\n');
	document.write('</script\>\n');
	
	document.write('<script language=\"VBScript\"\>\n');
	document.write('On Error Resume Next\n');
	document.write('Sub FLASH_DIALOG_ID_FSCommand(ByVal command, ByVal args)\n');
	document.write('	Call FLASH_DIALOG_ID_DoFSCommand(command, args)\n');
	document.write('End Sub\n');
	document.write('</script\>\n');

	document.write('<script language=\"VBScript\"\>\n');
	document.write('On Error Resume Next\n');
	document.write('Sub FLASH_PROGRESSBAR_ID_FSCommand(ByVal command, ByVal args)\n');
	document.write('	Call FLASH_PROGRESSBAR_ID_DoFSCommand(command, args)\n');
	document.write('End Sub\n');
	document.write('</script\>\n');	
	
	document.write('<script language=\"VBScript\"\>\n');
	document.write('On Error Resume Next\n');
	document.write('Sub FLASH_USER_INPUT_ID_FSCommand(ByVal command, ByVal args)\n');
	document.write('	Call FLASH_USER_INPUT_ID_DoFSCommand(command, args)\n');
	document.write('End Sub\n');
	document.write('</script\>\n');		
}

function FLASH_USER_INPUT_ID_DoFSCommand(eventType,eventValue)
{
	if(eventType.indexOf("FSCommand:")!=-1){
		eventType2 = eventType.substring(10,eventType.length); 
	}
	else{
		eventType2 = eventType;
	}	
	if(eventType2=="userinput"){
		if(LA_OnUserTextInput){
			LA_OnUserTextInput(eventValue);
		}	
	}
}

function FLASH_DIALOG_ID_DoFSCommand(eventType,eventValue)
{
	if(eventType.indexOf("FSCommand:")!=-1){
		eventType2 = eventType.substring(10,eventType.length); 
	}
	else{
		eventType2 = eventType;
	}	
	if(eventType2=="check_ready"){
		document.FLASH_DIALOG_ID.SetVariable("JS_READY","true");	
	}
	if(eventType2=="get_local_connection_id"){
		if(LA_Get_ObservableObject("FLASH_DIALOG_ID")!=null){
			LA_Get_ObservableObject("FLASH_DIALOG_ID").readyflag = true;
		}
		//TODO : if null.. pb !! 
		document.FLASH_DIALOG_ID.SetVariable("LOCAL_CONNECTION_ID",LA_UID);
	}
	if(eventType2=="debug"){
		window.status = eventValue;
	}	
}

function FLASH_PROGRESSBAR_ID_DoFSCommand(eventType,LA_eventValue)
{
	if(eventType.indexOf("FSCommand:")!=-1){
		eventType2 = eventType.substring(10,eventType.length); 
	}
	else{
		eventType2 = eventType;
	}		
	if(eventType2=="check_ready"){
		document.FLASH_PROGRESSBAR_ID.SetVariable("JS_READY","true");
	}
	/*
	if(eventType2=="get_local_connection_id"){
		if(LA_Get_ObservableObject("FLASH_PROGRESSBAR_ID")!=null){
			LA_Get_ObservableObject("FLASH_PROGRESSBAR_ID").readyflag = true;
		}
		document.FLASH_PROGRESSBAR_ID.SetVariable("LOCAL_CONNECTION_ID",LA_UID);
	}
	*/
}

function FLASH_PLAYER_ID_DoFSCommand(eventType,LA_eventValue)
{
	if(eventType.indexOf("FSCommand:")!=-1){
		eventType2 = eventType.substring(10,eventType.length); 
	}
	else{
		eventType2 = eventType;
	}	
	switch (eventType2) 
	{
		case "check_ready" :
			document.FLASH_PLAYER_ID.SetVariable("JS_READY","true");
			break;
		case "get_local_connection_id" :
			document.FLASH_PLAYER_ID.SetVariable("LOCAL_CONNECTION_ID",LA_UID);
			break;
		case "ONACTORLOADED":
			ACTOR.Data = LA_eventValue.split(',');
			/*
			idsceneSize = parseInt(ACTOR.GetIndexData("SCENESIZE"));
			if(idsceneSize!=-1){
				LA_actorLayerx = parseInt(ACTOR.Data[idsceneSize+1]);
				LA_actorLayery = parseInt(ACTOR.Data[idsceneSize+2]);
				if((typeof(LA_actorLayerx)=="number")&&(LA_actorLayerx!=0)){
					document.FLASH_PLAYER_ID.width = LA_actorLayerx;
					document.getElementById("LA_ActorLayer").style.width = LA_actorLayerx+"px";
					document.FLASH_PLAYER_ID.height = LA_actorLayery;
					document.getElementById("LA_ActorLayer").style.height = LA_actorLayery+"px";	
				}
			}
			*/
			if(LA_OnActorLoaded)
			{
				LA_OnActorLoaded(LA_eventValue);
			}
			break;
		case "flash_alert" :
			if (LA_eventValue!="")	
			{
				window.status = LA_eventValue;
				alert("alert : "+LA_eventValue);
			}
			break;
		case "flash_update":
			if(LA_FISRTPLAYERUPDATE){
				if(LA_Get_ObservableObject("FLASH_PLAYER_ID")!=null){
				LA_Get_ObservableObject("FLASH_PLAYER_ID").readyflag = true;
				}
				LA_FISRTPLAYERUPDATE=false;
			}

			if (LA_GENERIC_FLASH_PLAYER!=null)
			{
				LA_GENERIC_FLASH_PLAYER.SetVariable("FromJavaScript",ACTOR.scenario.join("<&#&>"));
				ACTOR.scenario = new Array;
			}
			break;
		case "ONPROGRESSBARSHOW":
			LA_PLAYER_URL = LA_eventValue;
			LA_sizeOfWindow =new LA_WindowSize;
			if(LA_PROGRESSBAR_VISIBLE){
				var LA_PB_centerX = (LA_sizeOfWindow.Width - document.getElementById('LA_ProgressBarLayer').offsetWidth)/2;
				var LA_PB_centerY = ((LA_sizeOfWindow.Height - document.getElementById('LA_ProgressBarLayer').offsetHeight)/2) + LA_sizeOfWindow.ScrollTop;
				//alert("X="+LA_PB_centerX+"Y="+LA_PB_centerY);
				document.getElementById('LA_ProgressBarLayer').style.left = LA_PB_centerX+"px";
				document.getElementById('LA_ProgressBarLayer').style.top = LA_PB_centerY+"px";
			}
			break;
		case "OnProgressBar":	
			document.FLASH_PROGRESSBAR_ID.SetVariable("PROGRESS",LA_eventValue);
			break;
		case "ONPROGRESSBARHIDE":
			document.getElementById('LA_ProgressBarLayer').style.left = -1000+"px";			
			break;
		case "ONDRAG":
		 	if(LA_ACTOR_DRAG_ENABLE==true){	
			    LA_ind = LA_eventValue.lastIndexOf(',');
				ACTOR.f_DRAG_X = LA_eventValue.substr(0,LA_ind) * 1;
				ACTOR.f_DRAG_Y = LA_eventValue.substr(LA_ind+1,LA_eventValue.length) * 1;
				clearTimeout(LA_DRAG_THREAD_ID);			
				LA_DRAG_THREAD_ID = setTimeout('LA_Drag_Thread()',10);
			}
			break;
		case "ONSTARTSPEAK":
			ACTOR.f_isSpeaking = true;
			ar = LA_eventValue.split(",");
			ACTOR.f_DialogHeight = parseInt(ar[0]);
			ACTOR.f_BBoxMinX = parseInt(ar[1]);
			ACTOR.f_BBoxMinY = parseInt(ar[2]);
			ACTOR.f_BBoxMaxX = parseInt(ar[3]);
			ACTOR.f_BBoxMaxY = parseInt(ar[4]);
			LA_Drag_Thread();
			break;
		case "ONSTOPSPEAK":
			ACTOR.f_isSpeaking = false;
			document.getElementById("LA_DialogLayer").style.left = (parseInt(document.getElementById('LA_DialogLayer').style.left) - 2000)+"px";
			break;			
		case "ONPLACEATPIXEL":
				ACTOR.f_DRAG_X = 0;
				ACTOR.f_DRAG_Y = 0;						
				LA_ind = LA_eventValue.lastIndexOf(',');
				var LA_x = LA_eventValue.substr(0,LA_ind) * 1;
				var LA_y = LA_eventValue.substr(LA_ind+1,LA_eventValue.length) * 1;
				document.getElementById('LA_ActorLayer').style.left = LA_x+"px";
				document.getElementById('LA_ActorLayer').style.top = LA_y+"px";
				break;
		case "ONALIGNHTMLOBJECT":
				params = LA_eventValue.split(",");
				res = LA_AlignToHTMLObject(params[0],params[1],params[2],params[3],params[4]);
				//alert('params: '+params + ' , res:'+res);
				document.FLASH_PLAYER_ID.SetVariable("JS_OBJECTPOSITION",res);
				break;
		case "ONCENTERDOCUMENTONHTMLOBJECT":
				LA_CenterDocumentOnHTMLObject(LA_eventValue);
				break;
		case "ONSHOW" :
			{
				LA_Drag_Thread();
				ACTOR.f_DRAG_X = 0;
				ACTOR.f_DRAG_Y = 0;
				if(LA_OnShow)
				{
					LA_OnShow();
				}
			}
			break; 
		case "ONQUIT" :
			if(LA_OnQuit)
			{
				LA_OnQuit();
			
			}
			break;
		case "ONHIDE" :
			{
				document.getElementById("LA_ActorLayer").style.left = (parseInt(document.getElementById('LA_ActorLayer').style.left) - 2000)+"px";
				document.getElementById("LA_DialogLayer").style.left = (parseInt(document.getElementById('LA_DialogLayer').style.left) - 2000)+"px";
				if(LA_USER_INPUT_LAYER){document.getElementById('LA_UserInputLayer').style.left = "-2000px"; }
				ACTOR.f_DRAG_X = 2000;
				if(LA_OnHide)
				{
					LA_OnHide();
				}
			}
			break; 				
		case "ONDRAGSTART" :
			if(LA_ACTOR_DRAG_ENABLE==true){	
				if(LA_OnDragStart)
				{
					LA_OnDragStart();
				}
			}
			break; 
		case "ONDRAGEND" : 
			if(LA_ACTOR_DRAG_ENABLE==true){	
				clearTimeout(LA_DRAG_THREAD_ID);
				ACTOR.f_DRAG_X = 0;
				ACTOR.f_DRAG_Y = 0;
				LA_Drag_Thread();
				if(LA_OnDragEnd)
				{
					LA_OnDragEnd(document.getElementById('LA_ActorLayer').style.left,document.getElementById('LA_ActorLayer').style.top);
				}
			}
			break; 
		case "TEXTINPUTCHANGE":
			LA_ind = LA_eventValue.indexOf(',');
			var LA_TextInputName = LA_eventValue.substr(0,LA_ind);
			var LA_TextInputValue = LA_eventValue.substr(LA_ind+1,LA_eventValue.length);
			for (var i=0;i<ACTOR.LA_TEXTINPUTS.length;i++)
			{
				if (ACTOR.LA_TEXTINPUTS[i].f_Name == LA_TextInputName)
				{
					ACTOR.LA_TEXTINPUTS[i].f_Value = LA_TextInputValue;
					LA_TextInputValue = "";
				}
			}
			if (LA_TextInputValue != "")
			{
				anEdit = new LA_TEXTINPUT(LA_TextInputName,LA_TextInputValue);
				ACTOR.LA_TEXTINPUTS.push(anEdit);
			}
			break;
		
		case "ONBOOKMARK" : 
			if(LA_OnBookmark)
			{
				LA_OnBookmark(LA_eventValue);
			}
			break; 
		case "ONDIALOGID" : 
			if(LA_OnDialogID)
			{
				LA_OnDialogID(LA_eventValue);
			}
			break; 
			/*
		case "ONSELECT" : 
			if(LA_OnSelect)
			{
				LA_ind = LA_eventValue.lastIndexOf(',');
				var LA_material = LA_eventValue.substr(0,LA_ind);
				var LA_bone = LA_eventValue.substr(LA_ind+1,LA_eventValue.length);
				LA_OnSelect(LA_material,LA_bone);
			}
			break;
			*/ 
		case "ONSOUNDON" : 
			if(LA_OnSoundOn)
			{
				LA_OnSoundOn();
			}
			break;
		case "ONSOUNDOFF" : 
			if(LA_OnSoundOff)
			{
				LA_OnSoundOff();
			}
			break; 
		case "ONNORMALMODE" : 
			if(LA_OnNormalMode)
			{
				LA_OnNormalMode(LA_eventValue);
			}
			break; 
		case "Stop" : 
			if(LA_Stop)
			{
				LA_Stop();
			}
			break; 
	default :
	} 
	
}

function LA_PathFor(LA_anActorName)
{
	var LA_path;
	var LA_completePath = document.location.href;
	LA_completePath = unescape(LA_completePath);
	var LastSlashIndex = LA_completePath.lastIndexOf("/");
	LA_path = LA_completePath.substr(0,LastSlashIndex+1);
	LA_path += LA_anActorName;
	return LA_path;
}
function LA_GetCookie(LA_NameOfCookie)
{  
	if (document.cookie.length > 0) 
	{     
		LA_begin = document.cookie.indexOf(LA_NameOfCookie+"="); 
		if (LA_begin != -1)   
		{
			LA_begin += LA_NameOfCookie.length+1; 
			LA_end = document.cookie.indexOf(";", LA_begin);
			if (LA_end == -1)
			{
				LA_end = document.cookie.length;
			}
			return unescape(document.cookie.substring(LA_begin, LA_end)); 
		}
	}
	return null; 
}
function LA_SetCookie(LA_NameOfCookie, LA_value, LA_expiredays,LA_path) 
{
 var LA_ExpireDate = new Date ();
 LA_ExpireDate.setTime(LA_ExpireDate.getTime() + (LA_expiredays * 24 * 3600 * 1000));
 document.cookie = LA_NameOfCookie + "=" + escape(LA_value) + ((LA_expiredays == null) ? "" : ";expires=" + LA_ExpireDate.toGMTString()) + ";path=/";
}
function LA_DelCookie(LA_NameOfCookie) 
{
 if (LA_GetCookie(LA_NameOfCookie))
 {
  document.cookie = LA_NameOfCookie + "=; expires=Thu, 01-Jan-70 00:00:01 GMT" + "; path=/"; 
 }
}

function SetLACookie(LA_NameOfCookie, LA_value)
{
	var LA_ExpireDate = new Date ();
	LA_ExpireDate.setTime(LA_ExpireDate.getTime() + (3600 * 1000)); //2 hours = 2 * 3600 * 1000
	document.cookie = LA_NameOfCookie + "=" + escape(LA_value) + ";expires=" + LA_ExpireDate.toGMTString() + ";LA_path=/" ;
}

function GetLACookie(LA_NameOfCookie)
{
	return LA_GetCookie(LA_NameOfCookie);		
}

function LA_PlayRandomFromBaseName(LA_basename,LA_nummax)
{
	//alert("LA_nummax "+LA_nummax);
	LA_lastplayedNum = 1;
	if(LA_GetCookie(LA_basename)==null)
	{
		LA_SetCookie(LA_basename,"1", 30, true);
	}
	else
	{
 		LA_lastplayedNum = LA_GetCookie(LA_basename);
	}
	
	(LA_nummax<=1)?eval(LA_basename + "1();"):false;
	
	if(LA_nummax==2)
	{
	LA_choosedNum = (LA_lastplayedNum==2)?1:2;
	eval(LA_basename + LA_choosedNum + "();");
	LA_SetCookie(LA_basename,LA_choosedNum, 30, true);
	}
	
	while(LA_nummax>2)
	{
		LA_choosedNum =Math.floor(Math.random()* LA_nummax);
		if((LA_choosedNum!=LA_lastplayedNum)&&(LA_choosedNum!=0))
		{
			//alert("LA_choosedNum : "+LA_choosedNum);
			LA_SetCookie(LA_basename,LA_choosedNum, 30, true);
			eval(LA_basename + LA_choosedNum + "();");
			return true;
		}
	}
}

function LA_TEXTINPUT(_TextInputName,_TextInputValue)
{
	this.f_Name = _TextInputName;
	this.f_Value = _TextInputValue;
}

function Actor(_FlashPlugin)
{
	this.FlashPlugin = _FlashPlugin;
	this.f_BBoxMinX = 0;
	this.f_BBoxMinY = 0;
	this.f_BBoxMaxX = 0;
	this.f_BBoxMaxY = 0;
	this.f_DialogHeight = 0;
	this.f_isSpeaking = false;
	this.f_DRAG_X = 0;
	this.f_DRAG_Y = 0;
	this.scenario = new Array();
	this.LA_TEXTINPUTS = new Array;
	this.Data = null;
	
	this.ex0=function(name){
	 	return this.scenario.push(name +"()");	 
	 };
	 this.ex1=function(name,par1){
	 	return this.scenario.push(name +"(" + par1 + ")");	 
	 };
	 this.ex2=function(name,par1,par2){
	 	return this.scenario.push(name +"(" + par1 + "," + par2 + ")");	 
	 };
	 this.ex3=function(name,par1,par2,par3){
	 	return this.scenario.push(name +"(" + par1 + "," + par2 + "," + par3 + ")");	 
	 };
	 this.ex5=function(name,par1,par2,par3,par4,par5){
	 	return this.scenario.push(name +"(" + par1 + "," + par2 + "," + par3 + "," + par4+ "," + par5+ ")");	 
	 }	 ;

	//this.GetLoadStatus = function(){}	//comp
	//BASIC
	this.AlignToHTMLObject = function(a,b,c,d,e){return this.ex5("ALIGNTOHTMLOBJECT",a,b,c,d,e);}; //name,alignH,offsetH,alignV,offsetV
	this.CenterDocumentOnHTMLObject = function(objname){return this.ex1("CenterDocumentOnHTMLObject",objname);};
	this.Disable = function(toggle){if(toggle==TOGGLE_ACTOR_DIALOG){LA_DIALOG_VISIBILITY = false;}else if(toggle==TOGGLE_ACTOR_DRAG){LA_ACTOR_DRAG_ENABLE = false;} else return this.ex1("Disable",toggle);};
	this.Enable = function(toggle){if(toggle==TOGGLE_ACTOR_DIALOG){LA_DIALOG_VISIBILITY = true;}else if(toggle==TOGGLE_ACTOR_DRAG){LA_ACTOR_DRAG_ENABLE = true;} else return this.ex1("Enable",toggle);};
	this.GestureAtPixel = function(x,y){return this.ex2("GestureAtPixel",x,y);};
	this.GestureAtScreen = function(x,y){return this.ex2("GestureAtScreen",x,y);};
	this.GoWaitMode = function(id){return this.ex1("GOWAITMODE",id);};
	/*
	this.GetBookmark = function(){}	
	this.GetDialogEditText = function(){}	//v1
	this.GetDialogEvent = function(){}
	this.GetStatus = function(){}
	*/
	this.Hide = function(av){if(!av){av="";}return this.ex1("Hide",av);}     	 
	this.LoadActor	= function(actorurl){return this.ex1("LoadActor",actorurl);}
	//this.LookAtPixel = function(){}
	//this.LookAtScreen = function(){}
	this.MenuAddItem = function(item,id){return this.ex2("MenuAddItem",item,id);}
	this.MenuClear = function(){return this.ex0("MenuClear");}
	this.MenuRemoveItem = function(id){return this.ex1("MenuRemoveItem",id);}	 
	//this.MoveToPixel = function(){}	 
	//this.MoveToScreen = function(){}
	this.PlaceAtFrame = function(x,y){bod=new LA_BodySize(); return this.ex2("PlaceAtPixel",Math.floor(x*bod.Width),Math.floor(y*bod.Height));}
	this.PlaceAtVisibleFrame = function(x,y){w=new LA_WindowSize(); return this.ex2("PlaceAtPixel",Math.floor((x*w.Width)+w.ScrollLeft),Math.floor((y*w.Height)+w.ScrollTop));}
	this.PlaceAtPixel = function(x,y){return this.ex2("PlaceAtPixel",x,y);}
	//this.PlaceAtScreen = function(){}
	this.Play = function(anim,av){if(!av){av="";}return this.ex2("Play",anim,av);}
	//this.PlayScenario = function(){}
	//this.PlaySound = function(){}  	 
	//this.Resume = function(){}
	this.SetBookmark = function(book){return this.ex1("SetBookmark",book);}
	//this.SetDialogWidth = function(){}
	//this.SetDialogSkin = function(){}
	//this.SetExpression = function(){}
	this.SetScale = function(s){return this.ex1("SetScale",s);}
	//this.SetTTSID = function(){}
	this.Show = function(av){if(!av){av="";}return this.ex1("Show",av);}
	this.Speak = function(txt){return this.scenario.push('Speak("' + txt  + '")');}
	this.Stop = function(){return this.ex0("Stop");}
	//this.Suspend = function(){}
	this.Wakeup = function(){return this.ex0("Wakeup");}
	//ADVANCED COMMANDS
	/*
	this.DeleteActorPreferences = function(){}
	this.GetActorHeight = function(){}
	this.GetActorPositionX = function(){}
	this.GetActorPositionY = function(){}
	this.GetActorPreference = function(){}
	this.GetActorWidth = function(){} 
	*/
	this.GetAnimation = function(id){f= this.GetIndexData("ANIMATIONS")+1;return this.Data[id + f];};
	this.GetAnimationCount = function(){f= this.GetIndexData("ANIMATIONS");l=this.GetIndexData("ENDANIMATIONS");if((f!=-1)&&(l!=-1)){return l-f;}else return -1};
	/*
	this.GetError = function(){}
	this.GetErrorCount = function(){}
	this.GetExpression = function(){}
	this.GetExpressionCount = function(){}
	this.GetSceneSize = function(){}
	
	this.GetMaterialCount = function(){}
	this.GetMaterialDiffuseBlue = function(){}
	this.GetMaterialDiffuseGreen = function(){}
	this.GetMaterialDiffuseRed = function(){}
	this.GetMaterial = function(){}
	
	this.SaveActorPreferences = function(){}
	this.SetActorPreference = function(){}	 
	this.SetCameraPosition = function(){}
	this.SetDialogFont = function(){}
	this.SetImmediateScale = function(){}
	this.SetLightColor = function(){}	 
	this.SetLightPosition = function(){}
	this.SetTTSInfos = function(){}
	this.SetTTSPitch = function(){} 
	this.SetTTSSpeed = function(){}
	*/
	//Utilities
	this.GetIndexData = function(adata){for(i=0;i<this.Data.length;i++){if(this.Data[i]==adata){return i;}}return -1;}
}
//positions

function LA_findPagePos(obj)
{
	objLeft=LA_ObjPosition_getPageOffsetLeft(obj);
	objTop=LA_ObjPosition_getPageOffsetTop(obj);
	return [objLeft,objTop];
}

function LA_ObjPosition_getPageOffsetLeft(el)
{
	var ol=el.offsetLeft;
	while ((el=el.offsetParent) != null) { ol += el.offsetLeft; }
	return ol;
}
function LA_ObjPosition_getPageOffsetTop(el)
{
	var ot=el.offsetTop;
	while((el=el.offsetParent) != null) { ot += el.offsetTop; }
	return ot;
}
function LA_FindObjectInWindow(objname)
{
	object=(document.getElementById(objname)!=null)?document.getElementById(objname):null;
	if(object==null)document.objname;
	return object;
}
function LA_AlignToHTMLObject(objname,alignH,offsetH,alignV,offsetV,oldtype)
{
	var object=LA_FindObjectInWindow(objname);
	if(object)
	{
		var objX=LA_findPagePos(object)[0];
		var objY=LA_findPagePos(object)[1];
		var objW=object.offsetWidth;
		var objH=object.offsetHeight;
		
		offsetH=parseInt(offsetH);
		offsetV=parseInt(offsetV);
	
		var LayerW = document.getElementById('LA_ActorLayer').offsetWidth;
		var LayerH = document.getElementById('LA_ActorLayer').offsetHeight; 
		var resX=0;
		var resY=0;
//alert("objname:"+objname+  " ,offsetH:"+offsetH+" ,objX:" + objX + ",objY:" + objY + ",objW:" + objW + ",objH:" + objH + ",LayerW:" + LayerW + ",LayerH:" + LayerH );
		
		if(alignH=="LEFT")
		{
		resX = objX - (LayerW/2) - offsetH;
		}
		else if(alignH=="RIGHT")
		{  	
		resX = objX + objW + (LayerW/2) + offsetH;
		}
		else if(alignH=="MIDDLE")
		{  	
		resX = objX + (objW/2) + offsetH;
		}
		
		if(alignV=="TOP")
		{
		resY = objY - offsetV;
		}
		else if(alignV=="BOTTOM")
		{
		resY = objY + objH + LayerH + offsetV;
		}
		else if(alignV=="MIDDLE")
		{
		resY = objY + (objH/2) + (LayerH/2) + offsetV;
		}
		
		//PlaceActor
//alert("ACTOR.X:"+resX+"|ACTOR.Y:"+resY);
		//stringtodo= "ACTOR.PlaceAtPixel(" + resX + "," + resY + ");";
		//eval(stringtodo);
		return resX + "," + resY;
	}
	else return "0,0";
}

function LA_CenterDocumentOnHTMLObject(objname)
{
	object=LA_FindObjectInWindow(objname);
	if(object)
	{
		//if(object.offsetLeft==0 || object.offsetTop==0)
		//{
		//	setTimeout("centerOnObject('"+objname+"')",300);
		//}
		//else
		//{
			objY=LA_findPagePos(object)[1];
			sizeOfWindow =new LA_WindowSize();
			deltaY=objY-(sizeOfWindow.Height/2);
			window.scrollTo(1,deltaY);
		//}
	}
	else return false;
}


// start the check objects ready loop.
LA_Check_Objects_Ready_Loop();

//layers html
var LA_RELATIVE_PATH="./LAData/";
var LA_ACTORFILE_NAME="Cathy_CA";
var LA_PLAYER_NAME="LivingActorPlayer62003.swf";
var LA_PLAYER_WIDTH=190;
var LA_PLAYER_HEIGHT=270;
//------------------------------------
if(typeof LA_OVERRIDE_FLASHDATA!="undefined"){
	LA_OVERRIDE_FLASHDATA();
}
var LA_DIALOG_URL=LA_RELATIVE_PATH + LA_ACTORFILE_NAME +"_Dialog.swf";
var LA_DIALOG_WIDTH=215;
var LA_DIALOG_HEIGHT=300;
var LA_PROGRESSBAR_URL=LA_RELATIVE_PATH + "LivingActorProgressBar.swf";
var LA_PROGRESSBAR_WIDTH=210;
var LA_PROGRESSBAR_HEIGHT=70;
var LA_PLAYER_URL=LA_RELATIVE_PATH+LA_PLAYER_NAME;
var LA_DIALOG_LEFT = LA_DIALOG_WIDTH-1;
var LA_PROGRESSBAR_LEFT = LA_PROGRESSBAR_WIDTH-1;
var LA_PLAYER_LEFT = LA_PLAYER_WIDTH-1;

var LA_USER_INPUT_URL = LA_RELATIVE_PATH+"userInput.swf";
var LA_USER_INPUT_WIDTH = 270;
var LA_USER_INPUT_HEIGHT = 100;
var LA_USER_INPUT_LEFT = 1-LA_USER_INPUT_WIDTH;
var LA_USER_INPUT_TOP = 1;


document.write('<div id="LA_DialogLayer" style="position:absolute; left:-'+LA_DIALOG_LEFT+'px; top:10px; width:'+LA_DIALOG_WIDTH+'px; height:'+LA_DIALOG_HEIGHT+'px; z-index:2; border: 1px none #000000;">');
document.write('<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,79,0" width="'+LA_DIALOG_WIDTH+'" height="'+LA_DIALOG_HEIGHT+'" align="middle" id="FLASH_DIALOG_ID">');
document.write('<param name="allowScriptAccess" value="sameDomain" />');
document.write('<param name="movie" value="'+LA_DIALOG_URL+'" />');
document.write('<param name="wmode" value="transparent" />');
document.write('<embed src="'+LA_DIALOG_URL+'" width="'+LA_DIALOG_WIDTH+'" height="'+LA_DIALOG_HEIGHT+'" align="middle" quality="high" salign="l" wmode="transparent" bgcolor="#ffffff" name="FLASH_DIALOG_ID" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />');
document.write('</object>');
document.write('</div>');
document.write('<div id="LA_ProgressBarLayer" style="position:absolute; left:-'+LA_PROGRESSBAR_LEFT+'px; top:10px; width:'+LA_PROGRESSBAR_WIDTH+'px; height:'+LA_PROGRESSBAR_HEIGHT+'px; z-index:1; border: 1px none #000000;">');
document.write('<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,79,0" width="'+LA_PROGRESSBAR_WIDTH+'" height="'+LA_PROGRESSBAR_HEIGHT+'" align="middle" id="FLASH_PROGRESSBAR_ID">');
document.write('<param name="allowScriptAccess" value="sameDomain" />');
document.write('<param name="movie" value="'+LA_PROGRESSBAR_URL+'" />');
document.write('<param name="wmode" value="transparent" />');
document.write('<embed src="'+LA_PROGRESSBAR_URL+'" width="'+LA_PROGRESSBAR_WIDTH+'" height="'+LA_PROGRESSBAR_HEIGHT+'" align="middle" quality="high" salign="l" wmode="transparent" bgcolor="#ffffff" name="FLASH_PROGRESSBAR_ID" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />');
document.write('</object>');
document.write('</div>');
document.write('<div id="LA_ActorLayer" style="position:absolute; left:-'+LA_PLAYER_LEFT+'px; top:10px; width:'+LA_PLAYER_WIDTH+'px; height:'+LA_PLAYER_HEIGHT+'px; z-index:1; border: 1px none #000000;">');
document.write('<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,79,0" width="'+LA_PLAYER_WIDTH+'" height="'+LA_PLAYER_HEIGHT+'" id="FLASH_PLAYER_ID">');
document.write('<param name="allowScriptAccess" value="sameDomain" />');
document.write('<param name="movie" value="'+LA_PLAYER_URL+'" />');
document.write('<param name="wmode" value="transparent" />');
document.write('<embed src="'+LA_PLAYER_URL+'" width="'+LA_PLAYER_WIDTH+'" height="'+LA_PLAYER_HEIGHT+'" quality="high" salign="l" wmode="transparent" bgcolor="#ffffff" name="FLASH_PLAYER_ID" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />');
document.write('</object>');
document.write('</div>');

if(LA_USER_INPUT_LAYER){
document.write('<div id="LA_UserInputLayer" style="position:absolute; left:'+LA_USER_INPUT_LEFT+'px; top:'+LA_USER_INPUT_TOP+'px; width:'+LA_USER_INPUT_WIDTH+'px; height:'+LA_USER_INPUT_HEIGHT+'px; z-index:1; border: 1px none #000000;">');
document.write('<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,79,0" width="'+LA_USER_INPUT_WIDTH+'" height="'+LA_USER_INPUT_HEIGHT+'" id="FLASH_USER_INPUT_ID">');
document.write('<param name="allowScriptAccess" value="sameDomain" />');
document.write('<param name="movie" value="'+LA_USER_INPUT_URL+'" />');
document.write('<param name="wmode" value="transparent" />');
document.write('<embed src="'+LA_USER_INPUT_URL+'" width="'+LA_USER_INPUT_WIDTH+'" height="'+LA_USER_INPUT_HEIGHT+'" quality="high" salign="l" wmode="transparent" bgcolor="#ffffff" name="FLASH_USER_INPUT_ID" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />');
document.write('</object>');
document.write('</div>');	
	
}

if(LA_DEBUG_LAYER){
document.write('<div id="LA_DEBUG_LAYER" style="font-family: verdana, times, sans-serif ; color: white ; font-size: 8pt ;position:absolute; top:1px; left:1px; width:250px; height:150px; background-color:black; color:white;text-align:left;">');
document.write('</div>');		
}
//document.getElementById('LA_DEBUG_LAYER').innerHTML +=