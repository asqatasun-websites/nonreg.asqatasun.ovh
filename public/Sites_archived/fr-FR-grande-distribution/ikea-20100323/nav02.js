/*** 
*************************************
JavaScript functions for: nav02
Created by Lars Henrik Rotnes
Last modified: 2002-10-07
*************************************
**/

/*
CVS Version : $Id: nav02.js,v 1.13 2003/06/03 13:09:54 bohuslav Exp $

*/

//browsercheck jonk 20030428
var w3c = (document.getElementById)? true:false; //ie 5 and up + mozilla + netscape 6 and up
var ie = (document.all)? true:false;
var ns = (document.layers)? true:false;

if(document.images) {
	var arrow_over = new Image();
	arrow_over.src = "/ms/img/navigation/goBtn_over.gif";
	var arrow = new Image();
	arrow.src = "/ms/img/navigation/goBtn.gif";
}

/**
*initialize the document, display default values in the option box
*the text and values that the option box is poulated with is collected
*from two arrays, aText and aValue which is defined in a separate file, navOptions.js
**/
function init(initVal){
	var selectID = "select1";  //the id of the select field that should be populated
	var catID = "catid";
	//check if the browser can run this script
	if (!optionTestIt(selectID) ){
		alert(js_fn_NOT_VALID_BROWSER); 
		return false;
	} 
	
	//netscape 6 && ie 5>
	if (document.getElementById){
		tmp = document.getElementById(selectID);
		tmp2 = document.getElementById(catID);
	}else if (document.all){ //Explorer 4
	  tmp = document.all[selectID];
	  tmp2 = document.all[catID];
	}else{ 
		alert(js_fn_NOT_VALID_BROWSER); 
		return false; 	
	}
	tmp2.value = pifCatalogId;
	tmp.options[0] = new Option(initVal,'#');
 	for(var i = 0; i < aText.length; i++ ){
 		tmp.options[i+1] = new Option(aText[i],aValue[i]);
 	}
 	tmp.selectedIndex = 0;
}

/**
*initialize the document, display default values in the option box
*the text and values that the option box is poulated with is collected
*from two arrays, aText and aValue which is defined in a separate file, navOptions.js
**/
function initNews(initVal, newVal){
	var selectID = "select1";  //the id of the select field that should be populated
	var catID = "catid";
	//check if the browser can run this script
	if (!optionTestIt(selectID) ){
		alert(js_fn_NOT_VALID_BROWSER); 
		return false;
	} 
	
	//netscape 6 && ie 5>
	if (document.getElementById){
		tmp = document.getElementById(selectID);
		tmp2 = document.getElementById(catID);
	}else if (document.all){ //Explorer 4
	  tmp = document.all[selectID];
	  tmp2 = document.all[catID];
	}else{ 
		alert(js_fn_NOT_VALID_BROWSER); 
		return false; 	
	}
	tmp2.value = pifCatalogId;
	tmp.options[0] = new Option(initVal,'#');
	tmp.options[1] = new Option(newVal,'NEW');
 	for(var i = 0; i < aText.length; i++ ){
 		tmp.options[i+2] = new Option(aText[i],aValue[i]);
 	}
 	tmp.selectedIndex = 0;
}
/**
*Open a new page
*
*argFieldId string  = id of the field that triggered the function
**/
function openNewPage(argFieldId){
	//netscape 6 && ie 5>
	if (document.getElementById){
		tmp = document.getElementById(argFieldId);
	}else if (document.all){ //Explorer 4
	  tmp = document.all[argFieldId];
	}else{ 
		alert(js_fn_NOT_VALID_BROWSER); 
		return false; 	
	}
	
	//if the first element in the dropdown is selected do nothing
	if( tmp.selectedIndex < 1 )
		return false;
	
	document.location.href = aValue[tmp.selectedIndex];
}


/**
*function to find out if the browser supports dynamic content handling with 
*option boxes.
*
*argFieldId string  = the id of the select field, used to test if the browser
*											handle dynamic populating of this field
**/
function optionTestIt(argFieldId)
{
	optionTest = true;

	//netscape 6 && ie 5>
	if (document.getElementById){
		tmp = document.getElementById(argFieldId);
	}else if (document.all){ //Explorer 4
	  tmp = document.all[argFieldId];
	}else{ 
 		optionTest = false;
 	}
 	
	lgth = tmp.options.length - 1;
	tmp.options[lgth] = null;
	if (tmp.options[lgth]) 
			optionTest = false;
	
	return optionTest;
}

/** Invoked when the mouse moves over of given image **/
function out(imgName) {
	if(document.images)
	eval('document.' + imgName + '.src = ' + imgName.substr(0, imgName.length-1) + '.src')
}

/** Invoked when the mouse moves over fo given image. **/
function over(imgName) {
	if(document.images)
	eval('document.' + imgName + '.src = ' + imgName.substr(0, imgName.length-1) + '_over.src')
}

function go(){
if (document.selecter.prod.options[document.selecter.prod.selectedIndex].value != "none") {
location = document.selecter.prod.options[document.selecter.prod.selectedIndex].value
		}
	}

//function for submitting dropdowns - jonk 20030402
//with button
/*
function browseIt(formName,selectName,formAction) {
	var w3c = (document.getElementById)? true:false;
	if (formAction == null){
		formAction = "/webapp/wcs/stores/servlet/CategoryDisplay";
	}
	if (w3c) {
		if (document.getElementById(selectName).value == "#") {
			document.getElementById(formName).action = "#";
		} else {
			document.getElementById(formName).action = formAction;
			document.getElementById(formName).submit();
		}
	} else {
		if (document.all[selectName].value == "#") {
			document.all[formName].action = "#";
		} else {
			document.all[formName].action = formAction;
			document.all[formName].submit();
		}
	}
}
*/
//function for submitting dropdowns - jonk 20030429
//without button
//added lines for cross-market store reference - bohuslav 20030603
function browseIt(formId,selectId) {
	if (w3c) {
		if (document.getElementById(selectId).value != "#") { //if the user uses the "back"-button the last selected value is still selected, this makes sure that the "browse here.." (#) doesn't get submitted.
			//document.getElementById(formId).action = document.getElementById(selectId).value; //set the form-action to the value of the selected option
			if (document.getElementById(selectId).value.indexOf(";;") != -1) {
				// parse harcoded link to store in another market
				var hc_link = document.getElementById(selectId).value;
				var keyVals = hc_link.split(";;");
				document.getElementById(formId).storeId.value = keyVals[0];
				document.getElementById(formId).langId.value = keyVals[1];
				document.getElementById(selectId).options[document.getElementById(selectId).options.length] = new Option ("Cross Link", keyVals[2], true, true);
			}
			else if (document.getElementById(selectId).value.indexOf("NEW") != -1) {
				document.getElementById(formId).action = '/webapp/wcs/stores/servlet/IkeamsNews';
			}	
			document.getElementById(formId).submit(); //submit the form
		}
	} else {
		if (document.all[selectId].value != "#") { //if the user uses the "back"-button the last selected value is still selected, this makes sure that the "browse here.." (#) doesn't get submitted.
			//document.all[formId].action = document.all[selectId].value; //set the form-action to the value of the selected option
			document.all[formId].submit(); //submit the form
		}
	}
}

//self-submitting select jonk 2005-10-07
function selectBrowse(frameName,formName,selectName) {
	var temp = "document." + formName + "." + selectName + ".options[document." + formName + "." + selectName + ".options.selectedIndex].value";
	if (eval (temp) != "do_nothing") {
		if (frameName=="") {
			location.href = eval (temp);
		} else if (frameName=="new") {
			window.open(eval (temp))
		} else {
			temp = frameName + ".location.href = " + temp;
			eval (temp);
		}
	}
}

//RangeIV change language
function changeLangId(langId) {
	var x = location.href.indexOf('langId');
	var y = location.href.indexOf('&', x);
	var newHref = location.href.substring(0, x);
	newHref = newHref.concat('langId=', langId);
	if (x < y ) {
		newHref = newHref.concat(location.href.substring(y));
	}
	location.href = newHref;
}

//RangeIV select store (ad version)
function selectStore(storeId, langId, storeNumber) {
	location.href = '/webapp/wcs/stores/servlet/IkeaNearYouView?storeId=' + storeId + '&langId=' + langId + '&StoreNumber=' + storeNumber;
}

//RangeIV select store (family version)
function selectStoreF(locale, storeNumber) {
    setCookie('selected_store_' + locale, storeNumber, null, '/' );

    if (arguments[2]) {
       location.href = location.href + '&localStore=' + arguments[2];
    } else {
        location.reload(true);
    }
}