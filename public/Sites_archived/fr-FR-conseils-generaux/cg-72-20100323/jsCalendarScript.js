// This function gets called when the end-user clicks on some date.
function selected(cal, date) {
	if (cal.dateInput != null) {
		var str = twoDigits(cal.date.getDate()) + "/" + twoDigits(cal.date.getMonth() + 1) + "/" + cal.date.getFullYear();	
		if(cal.showsTime) {
			str += " " + twoDigits(cal.date.getHours()) + ":" + twoDigits(cal.date.getMinutes())
		}
		cal.dateInput.value = str;
	} else {
		if (cal.year != null)
			cal.year.value = cal.date.getFullYear(); // just update the date in the input field.
		if (cal.month != null) {
			var monthValue = cal.date.getMonth()+1;
			if (cal.date.getMonth()+1 < 10) {
				cal.month.value = "0" + monthValue; // just update the date in the input field.
			} else {
				cal.month.value  = monthValue;
			}
		}
		if (cal.day != null) {
			var dayValue = cal.date.getDate();
			if (cal.date.getDate() < 10) {
				cal.day.value = "0" + dayValue; // just update the date in the input field.
			} else {
				cal.day.value = dayValue;
			}
		}
	}	
	// if (cal.dateClicked && (cal.sel.id == "sel1" || cal.sel.id == "sel3"))
    // if we add this call we close the calendar on single-click.
    // just to exemplify both cases, we are using this only for the 1st
    // and the 3rd field, while 2nd and 4th will still require double-click.
	//	cal.callCloseHandler();
	// updateCalendar(cal.baseId, false);
	if (cal.dateClicked)
		cal.callCloseHandler();
	updateCalendar(cal.baseId, false, cal.showsTime);
}

// And this gets called when the end-user clicks on the _selected_ date,
// or clicks on the "Close" button.  It just hides the calendar without
// destroying it.
function closeHandler(cal) {
  cal.hide();                        // hide the calendar
  cal.destroy();
  calendar = null;
}

function parseFullYear(str)
{
	var fullYear = parseInt(str, 10);
	if (!isNaN(fullYear))
		if (fullYear<100) fullYear += 2000;
	return fullYear;
}

// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendar(id, format, showsTime) 
{

	///Recuperation des champs associes au controle de date:
	///3 element possible : day, month, year.
	if (format == null) format = '%d/%m/%Y';
	var  labelDate = id + "_date";
	var  labelD = id + "_d";
	var  labelM = id + "_m";
	var  labelY = id + "_y";

	var elDate= document.getElementById(labelDate);
	if(elDate == null) {
		elDate= document.getElementById(id);
	}
	var elD= document.getElementById(labelD);
	var elM= document.getElementById(labelM);
	var elY= document.getElementById(labelY);

	var fullyear;
	var month;
	var day;
	var hours = 0;
	var minutes = 0;
	if(elD == null) {
		var array = /(\d{1,2})\/(\d{1,2})\/(\d{4})(\s+(\d{1,2}):(\d{1,2}))?/.exec(elDate.value)
		if(array) {
			fullyear = parseFullYear(array[3]);
			month = parseInt(array[2], 10);
			day = parseInt(array[1], 10);
			if(array[4] != "" && array[4] != null) {
				hours = parseInt(array[5], 10);
				minutes = parseInt(array[6], 10);
			}
		}
	} else {
		fullyear = parseFullYear(elY.value);
		month = parseInt(elM.value, 10);
		day = parseInt(elD.value, 10);
	}
	var dateStr = null;
	if(!isNaN(fullyear) && !isNaN(month) && !isNaN(day)) {
		dateStr = month + "/" + day + "/" + fullyear;
		dateStr += " " + hours + ":" + minutes;
	}

	if (calendar != null) {
		// we already have some calendar created
		calendar.hide();                 // so we hide it first.
	} else {
		// first-time call, create the calendar.
		var cal = new Calendar(true, dateStr, selected, closeHandler, showsTime);
		// uncomment the following line to hide the week numbers
		// cal.weekNumbers = false;
		calendar = cal;                  // remember it in the global var
		cal.setRange(1900, 2070);        // min/max year allowed.
		cal.create();
	}
	calendar.setDateFormat(format);    // set the specified date format
	var elFocus;
	if(elDate != null) {
		calendar.dateInput = elDate;                 // inform it what input field we use
		elFocus = elDate;
	} else {
		calendar.year = elY;                 // inform it what input field we use
		calendar.month = elM;                 // inform it what input field we use
		calendar.day = elD;                 // inform it what input field we use
		elFocus = elY;
	}
	calendar.baseId = id;                 // inform it what input field we use
	calendar.showAtElement(elFocus, "Bl");        // show the calendar
	return false;
}

function twoDigits(n)
{
	n = "" +n;
	if(n.length == 1) n = "0" + n;
	return n; 
}
function updateCalendar(id, init)
{
	var  labelDate = id + "_date";
	var  labelD = id + "_d";
	var  labelM = id + "_m";
	var  labelY = id + "_y";

	var elDate= document.getElementById(labelDate);
	var elD= document.getElementById(labelD);
	var elM= document.getElementById(labelM);
	var elY= document.getElementById(labelY);
	var elH= document.getElementById(id);

	if(!elD || !elM || !elY || !elH) {
		if(!elDate) return;
		if(init) {
			var timestamp = parseInt(elH.value, 10);
			if(isNaN(timestamp)) {
				elDate.value = "";
			} else {
				var date = new Date(timestamp);
				elDate.value = twoDigits(date.getDate()) + "/" + twoDigits(date.getMonth() + 1) + "/" + date.getFullYear();
			}
		} else {
			var fields = elDate.value.split("/");
			var fullyear = parseFullYear(fields[2]);
			var month = parseInt(fields[1], 10);
			var day = parseInt(fields[0], 10);
			var dateStr = null;
			if(!isNaN(fullyear) && !isNaN(month) && !isNaN(day)) {
				dateStr = month + "/" + day + "/" + fullyear;
				elH.value = new Date(dateStr).getTime();
			} else {
				elH.value = "";
			}		

		}
	} else {
		if(init) {
			var timestamp = parseInt(elH.value, 10);
			if(isNaN(timestamp)) {
				elD.value = "";
				elM.value = "";
				elY.value = "";
			} else {
				var date = new Date(timestamp);
				elD.value = twoDigits(date.getDate());
				elM.value = twoDigits(date.getMonth() + 1);
				elY.value = date.getFullYear();
			}
		} else {
			var fullyear = parseFullYear(elY.value);
			var month = parseInt(elM.value, 10);
			var day = parseInt(elD.value, 10);
			var dateStr = null;
			if(!isNaN(fullyear) && !isNaN(month) && !isNaN(day)) {
				dateStr = month + "/" + day + "/" + fullyear;
				elH.value = new Date(dateStr).getTime();
			} else {
				elH.value = "";
			}		

		}
	}
}


function checkDateInput(event)
{
	return true;
}