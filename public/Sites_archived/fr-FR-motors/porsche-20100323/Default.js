/************************************/
/* Erweiterung f�r JD Power 2008/03 */
/* -> Mouseovers konfiguriertbar    */
/* -> hl = "hilite"                 */
/* -> ll = "lolite"                 */
/************************************/
var g_homemenu_hl_fontcolor = "rgb(204, 0, 0)";
var g_homemenu_hl_background_color = "rgb(255,255,255)";
var g_homemenu_hl_background_color_Level5 = "rgb(255,255,255)";
var g_homemenu_ll_fontcolor_level0 = "rgb(255,255,255)";
var g_homemenu_ll_fontcolor_level1 = "rgb(0, 0, 0)";
var g_homemenu_ll_background_color = "transparent";
var g_homemenu_ll_background_image_level0 = "url(/images/arrows.gif)"; //"url(/images/arrow-white.gif)";
var g_homemenu_ll_background_image_level1 = "url(/images/arrows.gif)"; //"url(/images/arrow-gray.gif)";
var g_homemenu_ll_background_position_level0 = "210px -292px";
var g_homemenu_ll_background_position_level1 = "210px -292px";
var g_jdpowerphase = "none";

var g_kabseLinkCurrentPosition = -1; // STV, 2007.10.25: f�r vor/zur�ck im KBaseLayer
var g_refresh = false;

var g_psyma_navi_clicked = null;

// var g_timeout = -1;
var g_timeout = 1;
/* var g_timeoutTime = 80; */
var g_timeoutTime = 250; 
var g_openZoom = 0;
var g_openKnowledgeBase = 0;
var g_imageNr = 1;
var g_req;
var g_is_home = false;
var g_kBaseUrl = "";

var g_pageWidth = 839;
var g_initHome = false;
var g_timeoutHide1 = 0;
var g_timeoutHide2 = 0;
var g_timeoutHide3 = 0;
var g_timeoutHide4 = 0;
var g_timeoutHead = 0;
var g_currentNav1 = "";
var g_currentNav2 = "";
var g_currentNav3 = "";
var g_currentNav4 = "";

var g_currentHi1 = "";
var g_currentHi2 = "";
var g_currentHi3 = "";
var g_currentHi4 = "";

var G_MAXSUBLAYERS = 16;
var G_MAXITEMS_MAINNAVI = 20;

var g_blackValue = 0;
var g_blackTimeout = 0;
var g_navShadeMax = .35;

var g_currentModelLink = '';
var g_echoCount = 0;
var g_is_opera = false;
var g_is_ie = false;
var g_is_ie5 = false;
var g_is_ie5_5 = false;
var g_is_ie6 = false;
var g_is_safari = false;
// var g_is_macmoz = false; // mozilla on mac has flash problems on the homepage.
var g_is_mac = false;
var g_is_linux = false;
var g_contextDebug = "";
var g_fallbackImage = "";

var g_newWinFocus;
//var haveqt = isQTInstalled();


/////////////////////////////////////////////////////////
// Homepage Men� - Position der Baureihengrafik berechnen
var g_home_modelnavi_background_xPos = 0;               // X-Position der HD Grafik f�r die Baureihen
var g_home_modelnavi_gridsize = 50;                     // H�heneinheit des Grids - alle 50px sitzt der Y-Achsen Nullpunkt der n�chsten Baureihengrafik
var g_home_modelnavi_num_modelranges = 5;               // Anzahl Baureihen in der Grafik
var g_home_modelnavi_num_modelranges_background = g_home_modelnavi_num_modelranges + 1; // der HG der Baureihen befindet sich nach dem letzter Baureihengrafik (wird f�r der platzverbrauch wie eine zus�tzliche Bauriehe gerechnet)

///
// Gibt die CSS background-position f�r eine Baureihe zur�ck (string).
// modelno: Position der Baureihe im Men� -> 1 = erste eingepflegte Baureihe
// hl     : true  -> die Y-Position f�r die Highlight/Mouse-Over Grafik der Baureihe wird zur�ckgegeben
//          false -> die Y-Position f�r die normale Grafik der Baureihe wird zur�ckgegeben
function get_homenavi_background_position(modelno, hl) {
    return g_home_modelnavi_background_xPos.toString() + "px" + " " + get_Background_ypos(modelno, hl).toString() + "px";
}
function get_Background_ypos(modelno, hl) {
    // Berechnung: y-H�he des Grids*2 [noramle grafik + HL grafik] * (eingepflegte Bauriehe -1) [da die erste Baureihe den Nullpunkt bei 0 hat)
    var tmpYPos = g_home_modelnavi_gridsize * 2 * (modelno - 1)
    if (hl == true)
        tmpYPos = tmpYPos + g_home_modelnavi_gridsize; // HL-Grafik wird unter der normalen Grafik eingepflegt
    return (-1 * tmpYPos); // * -1, da das HG Bild nach Oben verschoben wird um das n�chste Bild anzuzeigen
}
/////////////////////////////////////////////////////////

/****************************************/
//global control variables for flash homepage.
// turns flash on homepage on/off globally.
var g_useFlashHomepage;
// load flash hp configuration in the following xml object; 
var g_xmlFlashHPConfig;
// also, copy the [pool].jpg into the fallback images directory.
var g_flashHPFallbackOverride;
/****************************************/

// test for jQuery first
if (typeof (jQuery) != 'undefined') {
    $(document).ready(function() {
        initAll();
        if (typeof (wiredminds) != 'undefined' && typeof (WIREDMINDSEXEC) != 'undefined') WIREDMINDSEXEC();
    });
}
else {
    log("jQuery not available. You must load the jquery core library before this document.");
}

function initAll()
{
    var sClass = "";
    if (document.body.className) {
        sClass = document.body.className;
    }
    else {
        sClass = document.body.getAttribute("class");
    }
    if (sClass != undefined) {
        g_is_home = sClass.indexOf("home") >= 1;
    }
    else {
        g_is_home = false;
    }

    log("HOMEPAGE: ", g_is_home);
    
    positionFooter();

    var noteConflict = false;
    sniffAll();
    if (document.getElementById) {
        g_canHandleTransparency = !g_is_ie5;
    }
    else if (noteConflict) {
        gotoUrl("/browser.htm");
    }
    if (g_is_home) {
        initHome();
		flashHP_getFlashHPConf(); //check if we want to load a flash animation on the homepage
    }

    HideIfLoggedIn("ifLoggedOut");
    ShowIfLoggedIn("ifLoggedIn");
    HideIfLoggedIn("ifmetaloggedout");
    ShowIfLoggedIn("ifmetaLoggedin");

    initGallerySubtitles();

    checkKBase();

    initPCNADealerSearchbox();
    //initDealerSearchbox();
}

function checkKBase() 
{
    var kbase = '';
    kbase = getQueryVariable('kbase');
    if (kbase == '')
        return;
    kbase = kbase.substring(6);
    clickKnowledgebase(kbase);
}
function getQueryVariable(variable) {
    var query = '';
    query = window.location.search.substring(1);
    if (query.match(/^kbase=/))
        return query;
  //var vars = query.split("&");
  //for (var i=0;i<vars.length;i++) {
  //  var pair = vars[i].split("=");
  // if (pair[0] == variable) {
  //   return pair[1];
  //  }
  //}
  return '';
} 
function sendRequest(url,callback,postData) {
	var req = createXMLHTTPObject();
	if (!req) return;
	var method = (postData) ? "POST" : "GET";
	req.open(method,url,true);
	//req.setRequestHeader('User-Agent','XMLHTTP/1.0');
	if (postData)
		req.setRequestHeader('Content-type','application/x-www-form-urlencoded');
		
	req.onreadystatechange = function () {
		if (req.readyState != 4) return;
		if (req.status != 200 && req.status != 304) {
//			alert('HTTP error ' + req.status);
			return;
		}
		if(callback)
		    callback(req);
	}
	if (req.readyState == 4) return;

	req.send();
}


function flashHP_getFlashHPConf()
{
	var req = createXMLHTTPObject();
	if (!req) return;
	
	req.open("GET","/all/media/flash/videos/homepage/settings.xml",true);
	//req.setRequestHeader('User-Agent','XMLHTTP/1.0');
	req.onreadystatechange = function () {
		if (req.readyState != 4) return;
		if (req.status != 200 && req.status != 304) {
			//alert('HTTP error ' + req.status);
			return;
		}
		
		g_xmlFlashHPConfig = req.responseXML;
		flashHP_initFlashHomepage();
	}
	if (req.readyState == 4) return;
	req.send(null);
}

var XMLHttpFactories = [
	function () {return new XMLHttpRequest()},
	function () {return new ActiveXObject("Msxml2.XMLHTTP")},
	function () {return new ActiveXObject("Msxml3.XMLHTTP")},
	function () {return new ActiveXObject("Microsoft.XMLHTTP")}
];

function createXMLHTTPObject() {
	var xmlhttp = false;
	for (var i=0;i<XMLHttpFactories.length;i++) {
		try {
			xmlhttp = XMLHttpFactories[i]();
		}
		catch (e) {
			continue;
		}
		break;
	}
	return xmlhttp;
}

function flashHP_getSettings(xPoolNode, xGlobalNode)
{
	var pool = new Object();
	pool.id        = xPoolNode.getAttribute("id");
	pool.lang	   = xPoolNode.getAttribute("lang");
	
	var hasFlash = (xPoolNode.getAttribute("hasFlash") != null) ? xPoolNode.getAttribute("hasFlash") : xGlobalNode.getAttribute("hasFlash");
	pool.hasFlash  = eval(hasFlash); // "true" oder "false";

	var overrideFallback = (xPoolNode.getAttribute("overrideFallback") != null) ? xPoolNode.getAttribute("overrideFallback") : xGlobalNode.getAttribute("overrideFallback");
	pool.overrideFallback = eval(overrideFallback);
	
	var timeOut = (xPoolNode.getAttribute("timeOut") != null) ? xPoolNode.getAttribute("timeOut") : xGlobalNode.getAttribute("timeOut");
	pool.timeOut   = (isInteger(timeOut)) ? parseInt(timeOut) : 0;

	pool.flashName = (xPoolNode.getAttribute("flashName") != null) ? xPoolNode.getAttribute("flashName") : xGlobalNode.getAttribute("flashName");
	pool.claimPool = (xPoolNode.getElementsByTagName("claim")[0].getAttribute("pool") != null) ? xPoolNode.getElementsByTagName("claim")[0].getAttribute("pool") : xGlobalNode.getElementsByTagName("claim")[0].getAttribute("pool");	
	pool.claimId   = (xPoolNode.getElementsByTagName("claim")[0].getAttribute("id") != null) ? xPoolNode.getElementsByTagName("claim")[0].getAttribute("id") : xGlobalNode.getElementsByTagName("claim")[0].getAttribute("id");
	pool.claimLang = (xPoolNode.getElementsByTagName("claim")[0].getAttribute("lang") != null) ? xPoolNode.getElementsByTagName("claim")[0].getAttribute("lang") : xGlobalNode.getElementsByTagName("claim")[0].getAttribute("lang");
	
	var claimLink = (xPoolNode.getElementsByTagName("claim")[0].getAttribute("link") != null) ? xPoolNode.getElementsByTagName("claim")[0].getAttribute("link") : xGlobalNode.getElementsByTagName("claim")[0].getAttribute("link");
	
	if (claimLink.indexOf("[pool_id]") != -1)
		claimLink = myreplace(claimLink, "[pool_id]", pool.id);
		
	if (claimLink.indexOf("[pool_lang]") != -1)
		claimLink = myreplace(claimLink, "[pool_lang]", pool.lang);
	
	pool.claimLink = claimLink;
	
	return pool;
}


function isInteger (s)
{
    var i;

    if ((s == null) || (s.length == 0))
    if (isInteger.arguments.length == 1) return 0;
    else return (isInteger.arguments[1] == true);

    for (i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);

        if (!isDigit(c)) return false;
    }

    return true;
}


function isDigit (c)
{
    return ((c >= "0") && (c <= "9"))
}




function flashHP_getGlobalAttribute(setting)
{
	var retVal;
	try
	{
		retVal = g_xmlFlashHPConfig.documentElement.getElementsByTagName("globalsettings")[0].getAttribute(setting);
	}
	catch(e)
	{
		retVal = null;
	}
	
	return retVal;	
}


function flashHP_getPoolNode(poolName, poolLang)
{
	var xmlRoot = g_xmlFlashHPConfig.documentElement;

	var poolNodeList = xmlRoot.getElementsByTagName("pool");
	var poolNode = null;
	
	var i = 0;
	var poolFound = false;
	
	while (!poolFound && i < poolNodeList.length)
	{
		if (poolNodeList[i].getAttribute("id") == poolName && poolNodeList[i].getAttribute("lang") == poolLang)
		{
			poolFound = true;
			poolNode = poolNodeList[i];
		}
		i++;
	}
	return poolNode;
}


function flashHP_initFlashHomepage()
{
	// pool id
	var poolName = getPoolName().toLowerCase();
	
	// pool language
	var poolLang = getPoolLang(poolName).toLowerCase();
	
	var poolHasFlashHP = true;

	if (!g_xmlFlashHPConfig) {
	    g_useFlashHomepage = false;
	    poolHasFlashHP = false;
	    // alert("debug: Fehler: konnte das Xml-Dokument NICHT laden.");
	}
	else {
	    g_useFlashHomepage = eval(flashHP_getGlobalAttribute("useflashHomepage"));

	    // get pool Settings
	    var xmlPoolNode = flashHP_getPoolNode(poolName, poolLang);
	    // get global pool settings
	    var xmlGlobalsNode = flashHP_getPoolNode("global", "none");

	    if ((xmlPoolNode == null)||(xmlGlobalsNode == null)){
	        g_useFlashHomepage = false;
	        poolHasFlashHP = false;
	    }
	    else {
	        var objSettings = flashHP_getSettings(xmlPoolNode, xmlGlobalsNode);

	        // override these variables in case of deviation in the switch statement below.
	        // var timeOut = 2480;
	        var rootPath = flashHP_getGlobalAttribute("rootPath");
	        var cachePrevent = flashHP_getGlobalAttribute("cachePrevent");

	        // flash settings
	        poolHasFlashHP = objSettings.hasFlash;
	        var timeOut = objSettings.timeOut;
	        var flashName = objSettings.flashName;
	        g_flashHPFallbackOverride = objSettings.overrideFallback;
	        // claim settings
	        var claimPool = objSettings.claimPool;
	        var claimId = objSettings.claimId;
	        var claimLang = objSettings.claimLang;
	        var claimLink = objSettings.claimLink;


	        /*
	        alert(
	        "debug:\nclaimPool = " + objSettings.claimPool + 
	        "\npool-id = " + objSettings.id + 
	        "\npoolLang = " + objSettings.lang + 
	        "\npoolHasFlashHP = " + poolHasFlashHP + 
	        "\nclaimId = " + objSettings.claimId +  
	        "\nclaimLang = " + objSettings.claimLang + 
	        "\nclaimLink = " + objSettings.claimLink + 
	        "\nrootPath = " + rootPath +
	        "\ncachePrevent = " + cachePrevent +
	        "\ntimeOut = " + objSettings.timeOut + 
	        ""		
	        );
	        */



	        // building the parts together..
	        var antiCacheSuffix = "?rid=" + cachePrevent;
	        var flashPath = rootPath + flashName + antiCacheSuffix;
	    }
	}
		
	// if both global and local variables are positive, go for it..
	if (g_useFlashHomepage && poolHasFlashHP)
	{
		var introElem = getElementsByClassName('introImage')[0];
		if (introElem) 
		{
			var introImg = introElem.getElementsByTagName("img")[0];
			if (introImg) 
			{
			    //STV: 
			    // >> Flashgr��e = Gr��e Introimage ... sollte eigentlich f�r jeden Markt funktionieren ...
			    if((poolName == 'usa') || (poolName == 'canada'))
			    {
			        var imgWidth = parseInt(introImg.style.width);
			        var imgHeight = parseInt(introImg.style.height);
				    showFlash(flashPath, "introImage", imgWidth, imgHeight, 8, "#000", "loop=true,menu=true,quality=high,wmode=opaque", "pool=" + claimPool +  ",id=" + claimId + ",type=image,lang=" + claimLang + ",filetype=normal,claimlink=" + claimLink);
				    //showFlash(flashPath, "introImage", "1063", "455", 8, "#000", "loop=true,menu=true,quality=high,wmode=opaque", "pool=" + claimPool +  ",id=" + claimId + ",type=image,lang=" + claimLang + ",filetype=normal,claimlink=" + claimLink);
                }
                else
                {
				    showFlash(flashPath, "introImage", "839", "440", 8, "#000", "loop=true,menu=true,quality=high,wmode=opaque", "pool=" + claimPool +  ",id=" + claimId + ",type=image,lang=" + claimLang + ",filetype=normal,claimlink=" + claimLink);
                }
				if (timeOut > 0) {
					window.setTimeout("showHomepageNavigation()", timeOut);
				}
				else {
					showHomepageNavigation();
					// The claim is called from within Flash..
				}
			}
		}
	}
	else
	// back to regular homepage.
	{
		// showHomepageNavigation();
		// showHomepageClaim();
	}
	
}

// this is a porschewebsite hack and would probably not work in other cases..
// paints a grey border below the introImage div. 
// Used by the flash videos which are triggered on the same page by a button (not hmoepage).
function setIntroImageBorder() {
	var elm = document.getElementById('introImage');
	if (elm) {
		elm.style.borderBottom = 'solid 1px #CECECE';
		elm.style.width = '615px';
	}
}

/*
Flash Homepage functions
*/


// Delivers an array of DOM Elements matching the input criteria
// 
// Returns: an array of found dom elements
function getElementsByClassName(strClass, strTag, objContElm) {
  strTag = strTag || "*";
  objContElm = objContElm || document;
  var objColl = (strTag == '*' && document.all && !window.opera) ? document.all : objContElm.getElementsByTagName(strTag);
  var arr = new Array();
  var delim = strClass.indexOf('|') != -1  ? '|' : ' ';
  var arrClass = strClass.split(delim);
  for (var i = 0, j = objColl.length; i < j; i++) {
    var arrObjClass = objColl[i].className.split(' ');
    if (delim == ' ' && arrClass.length > arrObjClass.length) continue;
    var c = 0;
    comparisonLoop:
    for (var k = 0, l = arrObjClass.length; k < l; k++) {
      for (var m = 0, n = arrClass.length; m < n; m++) {
        if (arrClass[m] == arrObjClass[k]) c++;
        if (( delim == '|' && c == 1) || (delim == ' ' && c == arrClass.length)) {
          arr.push(objColl[i]);
          break comparisonLoop;
        }
      }
    }
  }
  return arr;
}
function flash_onVideoStop() {
    var divLogo = document.getElementById('porscheWappen');
    var divHomeMenuShadow = document.getElementById('homeShadow');
    var divHomeMenu = document.getElementById('homemenue');

    if (divLogo)
        divLogo.style.display = 'block';
    if (divHomeMenu)
        divHomeMenu.style.display = 'block';
    if (divHomeMenuShadow)
        divHomeMenuShadow.style.display = 'block';
}

function flash_onVideoStart() {
    var divLogo = document.getElementById('porscheWappen');
    var divHomeMenuShadow = document.getElementById('homeShadow');
    var divHomeMenu = document.getElementById('homemenue');

    if(divLogo)
        divLogo.style.display = 'none';
    if(divHomeMenu)
        divHomeMenu.style.display = 'none';
    if(divHomeMenuShadow)
        divHomeMenuShadow.style.display = 'none';
}


// finds the first Element carrying the class name and changes its visibility.
function showHideByClassName(clsName, visible)
{
	var visibility = (visible == true) ? 'visible' : 'hidden';
	var display = (visible == true) ? 'block' : 'none';
	
	var elm = getElementsByClassName(clsName)[0];
	if (elm)
		elm.style.visibility = visibility;
		elm.style.display = display;
}


// this loads back the original Fallback-Image manually, and make the navi visible, in case the Flash animation fails to load.
function fallBackFromFlash(pool) {
    if (document.getElementById('introImage'))
        introImgDiv = document.getElementById('introImage');
    else
        introImgDiv = getElementsByClassName('introImage')[0];
    var introImg = introImgDiv.getElementsByTagName("img")[0];
    var fbSrc;
    if (g_flashHPFallbackOverride) {
        fbSrc = pool + ".jpg";
    }
    else {
        fbSrc = "default.jpg";
    }

    if (introImg) {
    }
    else {
        introImg = document.createElement("img");
        introImgDiv.appendChild(introImg);
    }
    introImg.src = "/all/media/flash/videos/homepage/fallback/" + fbSrc;

    showHomepageClaim();
    showHomepageNavigation();
}


//shows Navigation bar and shopping buttons
function showHomepageNavigation()
{
	// we don't use showHideClassByName here because of speed issues in IE6..
	// document.getElementById('topnav1').parentNode.style.visibility = 'visible';
	// getElementsByClassName("shoppingButtons", "span", document)[0].style.visibility = 'visible';
	// getElementsByClassName("shoppingShade", "div", document)[0].style.visibility = 'visible';
	showHideByClassName("homeNavigation", true);
	showHideByClassName('shoppingButtons', true);
	showHideByClassName("shoppingShade", true);
}
//hides Navigation bar and shopping buttons
function hideHomepageNavigation()
{
	showHideByClassName("homeNavigation", false);
	showHideByClassName("shoppingButtons", false);
	showHideByClassName("shoppingShade", false);
}

// makes the claim element invisible (flash loaded)
function hideHomepageClaim() {
	var claimElm = getHomepageClaim();
	claimElm.style.visibility = 'hidden';
	claimElm.style.display = 'none';
}

// makes the claim element visible (no flash loaded)
function showHomepageClaim() {
	var claimElm = getHomepageClaim();
	claimElm.style.visibility = 'visible';
	claimElm.style.display = 'block';
}
// returns the claim element to hide when loading the flash animation.
function getHomepageClaim() {
	return document.getElementById('homeHead');
}
// extracts pool name from the url. 
// Use the global variable if available.
function getPoolName() {
    if (typeof (CURRENTPOOL) != "undefined" && CURRENTPOOL != '') {
        //log('CURRENTPOOL:' + CURRENTPOOL);
        //log('CURRENTLANGUAGE:' + CURRENTLANGUAGE);
        return CURRENTPOOL;
    }
    else {
        // use the (brute) force
        var url = window.location.href;
        var startIndex = url.indexOf(window.location.hostname);
        var str = url.substr(startIndex, url.length - 1);
        retVal = str.split("/")[1];
        return retVal;
    }
}

// extracts site language from the url.
// determine default language, if no language is given.
// Example: http://www.porsche.com/japan/ ,where 'jp' is the implicit language)
// Use the global variable if available.
function getPoolLang(poolName) {
    // for importer pools
    if (typeof (CURRENTLANGUAGE) != "undefined" && CURRENTLANGUAGE != '') {
        return CURRENTLANGUAGE;
    }
    else {
        // use the (brute) force
        var url = window.location.href;
        var startIndex = url.indexOf(window.location.hostname);
        var str = url.substr(startIndex, url.length - 1);
        var lang = str.split("/")[2];
        if (
		lang == "de" ||
		lang == "en" ||
		lang == "es" ||
		lang == "fr" ||
		lang == "it" ||
		lang == "jp" ||
		lang == "nl" ||
		lang == "ko" ||
		lang == "th" ||
		lang == "zh" ||
		lang == "pt"
		) {
            return (lang);
        }
        else {
            // check if the market has a default lang;
            switch (poolName) {
                case "china":
                    lang = "zh";
                    break;
                case "belgium":
                    lang = "en";
                    break;
                case "korea":
                    lang = "ko";
                    break;
                case "netherlands":
                    lang = "nl";
                    break;
                case "swiss":
                    lang = "de";
                    break;
                case "taiwan":
                    lang = "zh";
                    break;
                case "thailand":
                    lang = "th";
                    break;
                case "pco":
                    lang = "de";
                    break;
                default:
                    lang = "none";
                    break;
            }
            return (lang);
        }
    }
}




function fitFrameFromParent(iHeight)
{
    addContextDebug("iHeight", iHeight);

    var elm = document.getElementById("mainframe");
    var footer = document.getElementById("footer");
    var search = document.getElementById("search");

    if (elm) {
        addContextDebug("enteredElm", true);
        elm.style.height = (iHeight + 40 + 140) + "px";
        if (search) { search.style.display = "none"; }
        footer.style.bottom = "-1px";
        if (search) { search.style.display = "block"; }
        addContextDebug("finishedElm", true);
    }
}

function addContextDebug(sName, sValue)
{
    g_contextDebug += sName + "=" + sValue + "\r\n";
}

function showContextDebug()
{
    if (g_contextDebug != "") {
        alert(g_contextDebug);
    }
}

function positionFooter()
{
    var footer = document.getElementById("footer");
    var search = document.getElementById("search");
    
    if (!g_is_home) setDocumentSize();

    if (footer) { footer.style.bottom = "-1px"; }
    if (search) { search.style.bottom = "-1px"; }
}

function setDocumentSize() {
	var size = 350; //160 + 105 + 85;

	if (getPoolName() == "usa") {
	    return;
// 
//  >> not needed anymore for USA
//	    
//	    var navi = document.getElementById("sitenavigation");
//	    if (navi) {
//	        //alert(navi.offsetHeight);
//	        size = 128 + navi.offsetHeight + 24 + 20; /* = Wappen + Navi + Mindestabstand zum Footer + Hoehe Footer*/
//	        if(g_is_ie6)
//	           size += 40;
//	    }
	}
	else {
	    var elm = document.getElementsByTagName("a");

	    if (elm) {
	        for (i = 0; i < elm.length; i++) {
	            var className = elm[i].className;

	            switch (true) {
	                case containsStr(className, "navigationBelowHome"):
	                    size += 24;
	                    break;
	                case containsStr(className, "subNavigationChapter"):
	                case containsStr(className, "subSubNavigation"):
	                case containsStr(className, "subSubNavigationActive"):
	                    size += 20; // 1 line: 16, 2 lines: 31;					
	                    break;
	            }
	        }
	    }
	}

	var elm = document.getElementsByTagName("div");
	
	if (elm) {
		for (i = 0; i < elm.length; i++){
		    if (elm[i].className.indexOf("content ") > -1) {
				//elm[i].style.border = "1px solid red";
				elm[i].style.minHeight = size + "px";
				elm[i].style.height = size + "px";
				break;			
			}
		}
	}
}

function containsStr(s, find) {
	if(s)return (s.indexOf(find) != -1);
}

function overHome()
{
    if (g_openKnowledgeBase == 0 && !g_is_safari) {
        if (g_timeout != -1) { clearTimeout(g_timeout); }
        showNavHomeSub();
    }
}

function outHome()
{
    if (g_openKnowledgeBase == 0 && !g_is_safari) {
        g_timeout = setTimeout("hideNavHomeSub()", g_timeoutTime);
    }
}

function overNavHomeSub()
{
    if (g_timeout != -1) { clearTimeout(g_timeout); }
}

function outNavHomeSub()
{
    if (g_timeout != -1) { clearTimeout(g_timeout); }
    g_timeout = setTimeout("hideNavHomeSub()", g_timeoutTime);
}

function showNavHomeSub()
{
    showLr("navigationHomeSub");
    showLr("navigationShade");
    setBackground("navigationHome", "rgb(242,242,242)");
}

function hideNavHomeSub()
{
    hideLr("navigationHomeSub");
    hideLr("navigationShade");
    setBackground("navigationHome", "rgb(255,255,255)");
}

function setBackground(id, sBack)
{
    var elm = document.getElementById(id);
    if(elm)elm.style.backgroundColor = sBack;
}

/* DIV Popup */
g_divPopupOpen = 0;
g_divPopupShadeborder = 8;
g_divPopupContentBorder = 15;
g_divPopupNaviSpace = 30;
g_divPopupHasCloseButton = false;

function openDivPopup(sUrl, imgWidth, imgHeight)
{
  if (g_divPopupOpen == 0)
  {
    g_divPopupHasCloseButton = true;
    // magic numbers follow, do not try at home
    var divHeight = imgHeight + (g_divPopupContentBorder * 2) + g_divPopupNaviSpace;
    var divWidth = imgWidth + (g_divPopupContentBorder * 2);
    var shadeHeight = imgHeight + (g_divPopupContentBorder * 2) + (g_divPopupShadeborder * 2) + g_divPopupNaviSpace + 2;
    var shadeWidth = imgWidth + (g_divPopupContentBorder * 2) + (g_divPopupShadeborder * 2) + 2;

    var elmShade = document.getElementById("divPopupShade");
    var elmDivPop = document.getElementById("divPopup");
    elmShade.className = 'bannerlibShade';
    elmDivPop.className = 'bannerlib';

    var imgX = g_pageWidth / 2 - imgWidth / 2;
    var imgY = Math.round( (viewportGetHeight() - imgHeight)/2 ) + viewportGetScrollY();

    if (imgY < 0) { imgY = 0; }
    if (imgX < 0) { imgX = 0; }

    elmShade.style.left = (imgX - g_divPopupShadeborder) + "px";
    elmShade.style.top = (imgY - g_divPopupShadeborder) + "px";
    elmShade.style.width = shadeWidth + "px";
    elmShade.style.height = shadeHeight + "px";

    elmDivPop.style.left = imgX + "px";
    elmDivPop.style.top = imgY + "px";
    elmDivPop.style.height = divHeight + "px";
    elmDivPop.style.width = divWidth + "px";

    var elmCloseButton = document.getElementById("closeButtonDivPopup");
    if (elmCloseButton) {
      elmCloseButton.style.left = (imgWidth - 19) + "px";
    }

    g_divPopupOpen = sUrl;

    prepareFillDivPopup(sUrl);
  }
}

function openDivPopupTemplate(sUrl, sType)
{
    if (g_divPopupOpen == 0)
    {
        g_divPopupHasCloseButton = true;
        var cssClassName = sType;
        switch(sType)
        {
          case 'bannerlib':
            var imgWidth = 758;
            var imgHeight = 640;
            break;
          case 'bannerlibsky':
            var imgWidth = 192;
            var imgHeight = 670;
            break;
          case 'bannerlibfull':
            var imgWidth = 498;
            var imgHeight = 126;
            break;
          case 'bannerlibhalf':
            var imgWidth = 264;
            var imgHeight = 126;
            break;
          case 'bannerlibrect':
            var imgWidth = 210;
            var imgHeight = 216;
            break;
          case 'bannerlibmrect':
            var imgWidth = 330;
            var imgHeight = 316;
            break;
          case 'bannerlibpopuph':
            var imgWidth = 230;
            var imgHeight = 366;
            break;
          case 'bannerlibpopupr':
            var imgWidth = 280;
            var imgHeight = 316;
            break;
        }
        // magic numbers follow, do not try at home
        var divHeight = imgHeight;
        var divWidth = imgWidth;
        var shadeHeight = imgHeight + (g_divPopupShadeborder * 2) + 2;
        var shadeWidth = imgWidth + (g_divPopupShadeborder * 2) + 2;

        var elmShade = document.getElementById("divPopupShade");
        var elmDivPop = document.getElementById("divPopup");
        elmShade.className = cssClassName + 'Shade';
        elmDivPop.className = cssClassName;

        var imgX = g_pageWidth / 2 - imgWidth / 2;
        var imgY = Math.round( (viewportGetHeight() - imgHeight)/2 ) + viewportGetScrollY();

        if (imgY < 0) { imgY = 0; }
        if (imgX < 0) { imgX = 0; }

        elmShade.style.left = (imgX - g_divPopupShadeborder) + "px";
        elmShade.style.top = (imgY - g_divPopupShadeborder) + "px";
        elmShade.style.width = shadeWidth + "px";
        elmShade.style.height = shadeHeight + "px";

        elmDivPop.style.left = imgX + "px";
        elmDivPop.style.top = imgY + "px";
        elmDivPop.style.height = divHeight + "px";
        elmDivPop.style.width = divWidth + "px";

        var elmCloseButton = document.getElementById("closeButtonDivPopup");
        if (elmCloseButton) {
            elmCloseButton.style.left = (imgWidth - 19) + "px";
        }

        g_divPopupOpen = sUrl;

        prepareFillDivPopup(sUrl);
    }
}

function closeDivPopup()
{
    var elm = document.getElementById("divPopup");
    elm.innerHTML = "<p>&nbsp;</p>";
    elm.style.display = "none";

    hideLr("divPopupShade");
    g_divPopupOpen = 0;

    if (g_is_safari) {
        var introElm = document.getElementById("introImage");
        if (introElm) {
            introElm.style.visibility = "visible";
        }
    }
}

function prepareFillDivPopup(sUrl)
{
    if(sUrl.indexOf('?') > -1)sUrl += "&" + getCachePrevent();
    else sUrl += "?" + getCachePrevent();
    if (window.XMLHttpRequest) {
        g_req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        g_req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    g_req.onreadystatechange = fillDivPopup;
    g_req.open("GET", sUrl, true);
    g_req.send(null);
}

function fillDivPopup()
{
    if (g_req.readyState == 4)
    {
        if (g_req.status == 200)
        {
            if (g_is_safari) {
                var introElm = document.getElementById("introImage");
                if (introElm) {
                    introElm.style.visibility = "hidden";
                }
            }

            var s = "";
        
            var elmKB = document.getElementById("divPopup");
            var divPopWidth = elmKB.style.width;
            divPopWidth = divPopWidth.replace(/px/,'');
            var divPopHeight = elmKB.style.height;
            divPopHeight = divPopHeight.replace(/px/,'');

            divPopPageHeight = divPopHeight - (2 * g_divPopupContentBorder)
            divPopPageWidth = divPopWidth - (2 * g_divPopupContentBorder)

            divPopWidth-=17;
            divPopHeight-=17;

            s += "";
            if(g_divPopupHasCloseButton == true)
            {
              s += "<div style=\"left:" + divPopWidth + "px;\" class=\"closeButton\" id=\"closeButtonDivPopup\" onclick=\"closeDivPopup();\"><img src=\"/Images/close-button.gif\" alt=\"[X]\" title=\"Close\" /></div>";
            }            
            s += g_req.responseText;
            s = replStr(s, "<content>", "");
            s = replStr(s, "</content>", "");

            var sRun = getTextBetween(s, "// <![CDATA[", "// ]]>");
            sRun = replStr(sRun, ",wmode=opaque", ",");

            elmKB.innerHTML = s;

            var elmShade = document.getElementById("divPopupShade");
            var elmKB = document.getElementById("divPopup");

            if (screen.width <= 800) {
                var imgHeight = 350;
                var elmWrapper = document.getElementById("divPopupWrapper");
                elmWrapper.style.height = (imgHeight - 80) + "px";
            }

            g_maxDivPage = getMaxDivPage('page');
            for(i=1;i<=g_maxDivPage;i++)
            {
              pageElm = document.getElementById('page' + i);
              if(pageElm)
              {
                pageElm.style.width = divPopPageWidth + 'px';
                pageElm.style.height = divPopPageHeight + 'px';
              }
            }
            naviElm = document.getElementById('divPopupNavi');
            if(naviElm)
            {
              naviElm.style.top = divPopPageHeight + 'px';
              naviElm.style.left = g_divPopupContentBorder + (divPopPageWidth/2 - 90/2) + 'px';
            }

            elmKB.style.display = "block";
            elmShade.style.display = "block";

            if (sRun != "") { eval(sRun); }
        }
        else
        {
            alert("Can't retrieve XML: " + g_req.statusText);
        }
    }
}

function getMaxDivPage(sName)
{
    var max = 0;
    var testElm = null;

    do
    {
      max++;
      testElm = document.getElementById(sName + max);
    }
    while(testElm != null)
    max--;
    return max;
}

g_currentDivPage = 1;
g_maxDivPage = 0;
function prevDivPage(sName)
{
    if(g_maxDivPage == 0)
    {
      g_maxDivPage = getMaxDivPage(sName);
    }
    var n = g_currentDivPage;
    n--;
    if (n <= 0){ n = g_maxDivPage;}

    if(n != g_currentDivPage)
    {
      showLr(sName + n);
      hideLr(sName + g_currentDivPage);
    }
    g_currentDivPage = n;
    return false;
}

function nextDivPage(sName)
{
    if(g_maxDivPage == 0)
    {
      g_maxDivPage = getMaxDivPage(sName);
    }
    var n = g_currentDivPage;
    n++;
    if (n > g_maxDivPage){ n = 1; }

    if(n != g_currentDivPage && g_maxDivPage > 1)
    {
      showLr(sName + n);
      hideLr(sName + g_currentDivPage);
    }
    g_currentDivPage = n;
    return false;
}

function clickKnowledgebase(sUrl) {
    g_kBaseUrl = sUrl;

    if ((g_openKnowledgeBase == 0) || g_refresh) {


        g_openKnowledgeBase = sUrl;
        prepareFillKbase(sUrl);
    }
}

function closeKnowledgebase() {
    toggleMilkyBackground("off");

    // IE Hack for dropdowns
    ieToggleDropDowns(getListIEDropDowns(), false);

    var elm = document.getElementById("knowledgeBase");
    elm.innerHTML = "<p>&nbsp;</p>";
    elm.style.display = "none";

    hideLr("knowledgeBaseShade");
    g_openKnowledgeBase = 0;

    if (g_is_safari) {
        var introElm = document.getElementById("introImage");
        if (introElm) {
            introElm.style.visibility = "visible";
        }
    }
}


function prepareFillKbase(sUrl) {
    if (sUrl.indexOf('?') > -1) sUrl += "&" + getCachePrevent();
    else sUrl += "?" + getCachePrevent();
    if (window.XMLHttpRequest) {
        g_req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        g_req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    g_req.onreadystatechange = fillKbase;
    g_req.open("GET", sUrl, true);
    g_req.send(null);

}


function fillKbase() {
    if (g_req.readyState == 4) {
        // IE hack to prevent dropdowns to stay in view (which will then be visible within the popup layer).
        ieToggleDropDowns(getListIEDropDowns(), true);

        if (g_req.status == 200) {
            if (g_is_safari) {
                var introElm = document.getElementById("introImage");
                if (introElm) {
                    introElm.style.visibility = "hidden";
                }
            }

            var s = "";

            var elmKB = document.getElementById("knowledgeBase");

            s += "";
            s += "<div class=\"closeButton\" id=\"closeButtonKbase\" onclick=\"closeKnowledgebase()\"><img src=\"/Images/close-button.gif\" alt=\"[X]\" title=\"Close\" /></div>";
            //s += addKnowledgebasePrevNextButtons(); // STV
            s += g_req.responseText;
            s = replStr(s, "<content>", "");
            s = replStr(s, "</content>", "");

            var sRun = getTextBetween(s, "// <![CDATA[", "// ]]>");
            sRun = replStr(sRun, ",wmode=opaque", ",");

            elmKB.innerHTML = s;

            var elmShade = document.getElementById("knowledgeBaseShade");

            if (screen.width <= 800) {
                var imgHeight = 350;
            }

            elmKB.style.visibility = "hidden";
            elmShade.style.visibility = "hidden";

            elmKB.style.display = "block";
            elmShade.style.display = "block";

            log("Kbase elements set up.");

            if (sRun != "") { eval(sRun); }

            moveKbButtons(); // STV

            // content is inserted, set the dimensions accordingly
            setKBaseDimensions();


            elmKB.style.visibility = "visible";
            elmShade.style.visibility = "visible";

            //if(getPoolName() == 'usa')
            toggleMilkyBackground("on");



        }
        else {
            alert("Can't retrieve XML: " + g_req.statusText);
        }
    }
}

function moveKbButtons() {
    //    var naviKB = document.getElementById('kbNavi');
    //    var elmKB = document.getElementById("knowledgeBase");
    //    if (elmKB && naviKB) {
    //        var kbaseHeading = elmKB.getElementsByTagName('h2')
    //        if (kbaseHeading) {
    //            kbaseHeading[0].appendChild(naviKB);
    //        }
    //    }
    log("entering moveKbButtons");
    var buttonsHTML = addKnowledgebasePrevNextButtons();
    if (buttonsHTML != '') {
        $(buttonsHTML).appendTo('#knowledgeBase');
    }

    g_refresh = false;
}
function clickNext() {
    var nextKBLink = document.getElementById('kbaseLink' + (g_kabseLinkCurrentPosition + 1));
    if (nextKBLink) {
        //alert(nextKBLink.href.replace('javascript:', ''));
        g_refresh = true;
        eval(nextKBLink.href);
    }
}
function clickPrevious() {
    var previousKBLink = document.getElementById('kbaseLink' + (g_kabseLinkCurrentPosition - 1));
    if (previousKBLink) {
        g_refresh = true;
        eval(previousKBLink.href);
    }
}
function addKnowledgebasePrevNextButtons() {
    if (g_kabseLinkCurrentPosition == -1)
        return '';

    var linkButtons = '<h2 class="kbNavi">';
    var renderPrevButton = document.getElementById('kbaseLink' + (g_kabseLinkCurrentPosition - 1));
    var renderNextButton = document.getElementById('kbaseLink' + (g_kabseLinkCurrentPosition + 1));
    linkButtons += '<div id="kbNavi">'

    if (getPoolName() == 'usa') {
        if (renderPrevButton)
            linkButtons += '<a id="kbPrev" title="Previous Topic: ' + renderPrevButton.title + '" href="javascript: clickPrevious();"><img src="/images/jdp_layer_previous.gif" /></a>';
        if (renderNextButton)
            linkButtons += '<a id="kbNext" title="Next Topic: ' + renderNextButton.title + '"  href="javascript: clickNext();"><img src="/images/jdp_layer_next.gif" /></a>';
    }
    else {
        var lang = '';
        if (getPoolName() == 'canada')
            lang = (getPoolLang('canada') == 'fr' ? '-fr' : '');

        if (renderPrevButton)
            linkButtons += '<a id="kbPrev" title="Previous Topic: ' + renderPrevButton.title + '" href="javascript: clickPrevious();"><img src="/images/kbasenavi-previous' + lang + '.gif" /></a>';
        if (renderNextButton)
            linkButtons += '<a id="kbNext" title="Next Topic: ' + renderNextButton.title + '"  href="javascript: clickNext();"><img src="/images/kbasenavi-next' + lang + '.gif" /></a>';
    }

    linkButtons += '</div>';
    linkButtons += '</h2>'

    return linkButtons;
}

function setKBaseDimensions() {
    log("entering setKBaseDimensions");
    var shadeborder = 8;

    var imgWidth = 656;
    var imgHeight = 535;

    var maxContainerHeight;
    var minContainerHeight;
    var contentHeight;
    var kbHeight;
    var shadeHeight;

    var kbNaviHeight = 47;
    if ($("#knowledgeBase h2.kbNavi").length == 0) {
        log("No kbNavi detected. Extra height disabled.");
        kbNaviHeight = 0;
    }


    // settings max and min height a kbase container can have.
    maxContainerHeight = $(window).height() - (2 * 16) - (2 * shadeborder) - kbNaviHeight;
    if (maxContainerHeight < 0) maxContainerHeight = 0;

    minContainerHeight = 575 - kbNaviHeight;

    log("$('#knowledgebaseWrapper').outerHeight(true): " + $('#knowledgebaseWrapper').outerHeight(true));
    contentHeight = $('#knowledgebaseWrapper').outerHeight(true) + 47;

    if ($("#introImageKBase_flash").length > 0) {
        var flashHeight = $("#introImageKBase_flash").attr("height");
        log("flashHeight: " + flashHeight);
        if (flashHeight != '' && typeof (flashHeight) != 'undefined') {
            contentHeight += flashHeight;
        }
    }

    log("contentHeight: " + contentHeight);

    // Adjust container height according to content height
    if (contentHeight > maxContainerHeight) {
        kbHeight = maxContainerHeight;
    }
    else if (contentHeight < minContainerHeight) {
        kbHeight = minContainerHeight;
    }
    else
        kbHeight = contentHeight;


    // Navigation arrows (previous, next)..
    // kbNavi height affects total height (absolute positioned, and not within "#knowledgebaseWrapper").

    kbHeight = kbHeight + kbNaviHeight;
    kbHeight -= 60 // BU: don't ask why.

    shadeHeight = kbHeight + (2 * shadeborder) + 61;
    $("#knowledgebaseWrapper").height(kbHeight - 6 - kbNaviHeight);

    //log("maxContainerHeight: " + maxContainerHeight);
    //log("minContainerHeight: " + minContainerHeight);
    //log("kbHeight: " + kbHeight);

    var elmShade = document.getElementById("knowledgeBaseShade");
    var elmKB = document.getElementById("knowledgeBase");

    if (getPoolName() == 'usa') {
        g_pageWidth = 1063;
    }

    var imgX = g_pageWidth / 2 - imgWidth / 2;
    var imgY = Math.round(($(window).height() - shadeHeight) / 2) + viewportGetScrollY() + 8;

    if (imgY < 0) { imgY = 0; }
    if (imgX < 0) { imgX = 0; }

    // set KBase-Shade dimensions
    elmShade.style.left = (imgX - shadeborder) + "px";
    elmShade.style.top = (imgY - shadeborder) + "px";
    elmShade.style.height = shadeHeight + "px";

    // set KBase Dimensions
    elmKB.style.left = imgX + "px";
    elmKB.style.top = imgY + "px";
    elmKB.style.height = kbHeight + "px";

}

function toggleMilkyBackground(onOff)
{
    var milkyBG = document.getElementById("kbaseMilkyBG");

    if (onOff == "on")    
    {
        if (milkyBG) return;
        // insert semi-transparent milky background
        var transbg = document.createElement("div");
        var trbgstyle = document.createAttribute("id");
        trbgstyle.nodeValue = "kbaseMilkyBG";
        transbg.setAttributeNode(trbgstyle);
        var kBaseLayer = document.getElementById("knowledgeBase");
        if (kBaseLayer && kBaseLayer.parentNode)
        {
            kBaseLayer.parentNode.appendChild(transbg, kBaseLayer);
            //kBaseLayer.parentNode.insertBefore(transbg, kBaseLayer);
        }
            
    }
    else
    {
        if (milkyBG)
        {   
            milkyBG.style.display = "none";
            if (document.removeChild)
                milkyBG.parentNode.removeChild(milkyBG);
        }
    }
}


function getTextBetween(sAll, sStart, sEnd)
{
    var sPart = "";
    var posStart = sAll.indexOf(sStart);
    var posEnd = sAll.indexOf(sEnd);
    if (posStart >= 0 && posEnd >= 0 && posEnd > posStart) {
        sPart = sAll.substring(posStart + sStart.length, posEnd);
    }
    return sPart;
}

function clickZoomable(id, imgWidth, imgHeight)
{
    if (g_openZoom == 0)
    {
        var shadeborder = 8;

        var elmShade = document.getElementById("zoomImageShade");
        var elmImage = document.getElementById("zoomImage" + id);
        var elmCloseButton = document.getElementById("closeButton" + id);

        var imgX = g_pageWidth / 2 - imgWidth / 2;
        var imgY = Math.round( (viewportGetHeight() - imgHeight)/2 ) + viewportGetScrollY();

        if (imgY < 0) { imgY = 0; }
        // if (imgX < 0) { imgX = 0; }

        elmShade.style.left = (imgX - shadeborder) + "px";
        elmShade.style.width = (imgWidth + shadeborder * 2) + "px";
        elmShade.style.top = (imgY - shadeborder) + "px";
        elmShade.style.height = (imgHeight + shadeborder * 2) + "px";

        elmImage.style.left = imgX + "px";
        elmImage.style.top = imgY + "px";
        if (elmCloseButton) {
            elmCloseButton.style.left = (imgWidth - 19) + "px";
        }

        elmImage.style.display = "block";
        elmShade.style.display = "block";

        g_openZoom = id;
    }
}

function clickZoomed()
{
    hideLr("zoomImageShade");
    hideLr("zoomImage" + g_openZoom);
    g_openZoom = 0;
}

function showLr(id)
{
    var elm = document.getElementById(id);
    if (elm) {
        elm.style.display = "block";
    }
}

function hideLr(id)
{
    var elm = document.getElementById(id);
    if (elm) {
        elm.style.display = "none";
    }
}

function toggleLr(id)
{
    var elm = document.getElementById(id);
    if (!elm) return;
    
    if (elm.style.display != "block") showLr(id);
    else hideLr(id);
}



/* Expandable module */

function expandCollapse(id)
{
    var elm = document.getElementById("expandable_" + id);

    if (elm)
    {
        /* Hack alert -- footer will be hidden temporarily
        to recalculate bottom position correctly.
        */

        var footer = document.getElementById("footer");
        footer.style.display = "none";

        if (elm.style.height != "auto")
        {
            collapseAllElements();
            showElement(id);        
        }
        else
        {
            collapseElement(id);
        }

        positionFooter();
        footer.style.display = "block";
    }
}

function showElement(id) {
    var elm = document.getElementById("expandable_" + id);
    elm.style.height = "auto";

    var h3 = document.getElementById("expandableHead_" + id);
    if (h3) {                
        h3.style.backgroundColor = "rgb(102,102,102)";                 
        h3.style.color = "rgb(255,255,255)";              
        h3.style.backgroundImage = "url(/Images/arrow-lightgray-down.gif)";            
        h3.style.backgroundPosition = "9px 9px";
    }
}

function collapseElement(id) {
    var elm = document.getElementById("expandable_" + id);
    elm.style.height = "19px";

    var h3 = document.getElementById("expandableHead_" + id);
    if (h3) {
        h3.style.backgroundColor = "rgb(204,204,204)";                
        h3.style.color = "rgb(0,0,0)";
        h3.style.backgroundImage = "url(/Images/arrow-gray.gif)";          
        h3.style.backgroundPosition = "11px 7px";
    }
}

function collapseAllElements() {
    var i=1;
    do {
        var elm = document.getElementById("expandable_" + i);
        if (elm) collapseElement(i);
        i++;
    } while(elm);
}

function viewportGetHeight()
{
    var retval = 0;

    if (window.innerHeight)
        retval = window.innerHeight - 18;
    else if (document.documentElement && document.documentElement.clientHeight) 
        retval = document.documentElement.clientHeight;
    else if (document.body && document.body.clientHeight) 
        retval = document.body.clientHeight;

    return retval;    
}

function viewportGetWidth()
{
    var retval = 0;

    if (window.innerWidth)
        retval = window.innerWidth - 18;
    else if (document.documentElement && document.documentElement.clientWidth) 
        retval = document.documentElement.clientWidth;
    else if (document.body && document.body.clientWidth) 
        retval = document.body.clientWidth;

    return retval;    
}

function viewportGetScrollY()
{
    var retval = 0;

    if (typeof window.pageYOffset == "number")
        retval = window.pageYOffset;
    else if (document.documentElement && document.documentElement.scrollTop)
        retval = document.documentElement.scrollTop;
    else if (document.body && document.body.scrollTop) 
        retval = document.body.scrollTop; 
    else if (window.scrollY)
        retval = window.scrollY;

    return retval;
}

function showGalleryFlash(pool, movieFolder, bandwidth, flashParams)
{
    var elmImg = document.getElementById("galleryImage");
    if (elmImg) {
        g_fallbackImage = elmImg.src;
    }

    if (pool == 'usa') {
        var url = "/all/media/flash/videoplayer.swf";
        showFlash(url, "galleryContent", "799", "335", "7", "#FFFFFF",
            "loop=true,menu=true,quality=high,wmode=transparent",
            "pool=" + pool + ",id=" + movieFolder + ",bandwidth=" + bandwidth + ",eventhandler=flashEnded," + flashParams);
    }
    else {
        var url = "/all/media/flash/videoplayer.swf";
        showFlash(url, "galleryContent", "595", "317", "7", "#FFFFFF",
            "loop=true,menu=true,quality=high,wmode=opaque",
            "pool=" + pool + ",id=" + movieFolder + ",bandwidth=" + bandwidth + ",eventhandler=flashEnded," + flashParams);
    }
}

function showGalleryPanoramaFlash(url, flashParams)
{
    var elmImg = document.getElementById("galleryImage");
    if (elmImg) {
        g_fallbackImage = elmImg.src;
    }
   
    showFlash(url, "galleryContent", "595", "317", "7", "#FFFFFF","loop=true,menu=true,quality=high,wmode=opaque","eventhandler=flashEnded," + flashParams);
}

function flashEnded()
{
    if (g_fallbackImage != "") {
        var elmContent = document.getElementById("galleryContent");
        elmContent.innerHTML = "<img src=\"" + g_fallbackImage + "\" alt=\"\" id=\"galleryImage\" />";
    }
}

function showFlash(url, id, width, height, version, bgcolor, playerparamstr, flashparamstr) {
    var flashElmId = id + "_flash";

    width = "" + width;
    height = "" + height;
    version = "" + version;
    
    var elm = document.getElementById(id) || getElementsByClassName(id)[0];
    if (!elm) {
        return;
    }

    var flashvars = JSONify(flashparamstr);
    var params = JSONify(playerparamstr);
    var attributes = false;

    params.bgcolor = bgcolor;
    params.align = "middle";

    var expressinstall = "/all/media/flash/expressInstall.swf";
    if (g_is_home) {
        expressinstall = false;
    }

    var playFlash = true; // on the homepage, if flash check fails, do not attempt to run  flash..

    if (g_is_home) {
        if (!swfobject.hasFlashPlayerVersion(version)) {
            // fall back to regular page (without flash)
            fallBackFromFlash(getPoolName());
            playFlash = false; // on the homepage, if flash check fails, do not attempt to run  flash..
        }
        else {
            // prepare the homepage before loading flash animation.
            hideHomepageNavigation();
            hideHomepageClaim();
        }
    }
    else
    {
        if ((swfobject.getFlashPlayerVersion().major+'') == '0') // no Flash installed
            playFlash = false;
    }


    // go
    if (playFlash) {
        elm.innerHTML = "<div id=\"" + flashElmId + "\"></div>";
        swfobject.embedSWF(url, flashElmId, width, height, version, expressinstall, flashvars, params, attributes);
    }
}

// parses a json string and returns an object.
function JSONify(str) {
    
    //alert("JSONify ing: " + str);
    if (str == undefined || str == null) return "{}";
    var json = "{";
    var trap = false;
    var params = str.split(",");
    for (var i = 0; i < params.length; i++) {
        var param = params[i].split("=");
        if (param[0] == undefined || param[0] == null || param[0] == "") {
            trap = true;
            continue;
        }
        else {
            if (i > 0 && !trap) {
                json += ",";
                trap = false;
            }
            json += param[0] + ":\"" + param[1] + "\"";
        }
    }
    json += "}";
    return eval('(' + json + ')');
   
}


function swfIsVideoPlayer(url) {
    var retVal = false;
    var FILENAME = 'videoplayer.swf';
    var foundSWF = url.substr(url.lastIndexOf('/') + 1);

    if (foundSWF && (foundSWF.toLowerCase() == FILENAME))
        retVal = true;

    return retVal;
}



/* Gallery module */

var g_lastNumber = 1;

function setImage(elm, n)
{
    if (g_lastNumber != n)
    {
        var lastElm = document.getElementById("thumbnail" + g_lastNumber);
        lastElm.className = "";
        lastElm.style.border = "1px solid #ccc";

        var thisElm = document.getElementById("thumbnail" + n);
        thisElm.className = "selected";
        thisElm.style.border = "1px solid rgb(204,0,0)";

        g_lastNumber = n;

        sHref = elm.href;
        document.getElementById("galleryImage").src = sHref;
        
        showGallerySubtitle(n);
        showGalleryClaim(n);
    }
    return false;
}

function setThumbnail(elm, n)
{
    if (g_lastNumber != n)
    {
        var lastElm = document.getElementById("thumbnail" + g_lastNumber);
        lastElm.className = "";
        lastElm.style.border = "1px solid #ccc";

        var thisElm = document.getElementById("thumbnail" + n);
        thisElm.className = "selected";
        thisElm.style.border = "1px solid rgb(204,0,0)";

        g_lastNumber = n;
    }

    return false;
}

function prevgalleryImage(img, imgMax)
{
    var lastElm = document.getElementById("thumbnail" + g_lastNumber);
    if (lastElm) {    
    	lastElm.className = "";
    	lastElm.style.border = "1px solid #ccc";
    }

    var n = --g_lastNumber;
    if (n < 1) { n = getMaxNumberGallery(); }

    var thisElm = document.getElementById("thumbnail" + n);
    if (thisElm) {     
	    thisElm.className = "selected";
	    thisElm.style.border = "1px solid rgb(204,0,0)";
	}

    var elm = document.getElementById("href" + n);
    sHref = elm.href;
    document.getElementById("galleryImage").src = sHref;

    g_lastNumber = n;
    
    showGallerySubtitle(n);
	showGalleryClaim(n);
	
    return false;
}

function nextgalleryImage()
{
    var lastElm = document.getElementById("thumbnail" + g_lastNumber);
    if (lastElm) {
	    lastElm.className = "";
	    lastElm.style.border = "1px solid #ccc";
    }

    var n = ++g_lastNumber;
    var max = getMaxNumberGallery();
    if (n > max) { n = 1; }

    var thisElm = document.getElementById("thumbnail" + n);
    if (thisElm) {    
	    thisElm.className = "selected";
	    thisElm.style.border = "1px solid rgb(204,0,0)";
	}

    var elm = document.getElementById("href" + n);
    sHref = elm.href;
    document.getElementById("galleryImage").src = sHref;

    g_lastNumber = n;
    
    showGallerySubtitle(n);
    showGalleryClaim(n);

    return false;
}

function initGallerySubtitles()
{
	var n = getGalleryItemNo();
	if (n == -1) n=1;
		
    var subtitles = document.getElementById("gallerySubtitles")               	
    if (subtitles) {
        showGallerySubtitle(n);
        subtitles.style.display = "block";
    }

	var claims = document.getElementById("galleryClaims")
    if (claims) {
	    showGalleryClaim(n);	        
        claims.style.display = "block";
    }
}


function getGalleryItemNo() 
{
    var no = -1;
    
    var regex = /itemindex=(\d*)/;
    var result = regex.exec(document.URL);
    
    if (result != null) { 
        no = result[1]
    }            
    
    return no;          
}

function showGallerySubtitle(n)
{
    var subtitles = document.getElementById("gallerySubtitles");

    if (subtitles) {   
		var tabSubtitles = subtitles.getElementsByTagName("div");
		 	        		
		var tab = document.getElementById("gallerySubtitlesTab" + getGallerySubtitleTabNo());
		if (tab) 
		{
			tabSubtitles = tab.getElementsByTagName("div");	
			tab.style.display = "block";				
		}

		/************************/
		/* STV: JDPower 2008-11 */
		var currentPool = getPoolName();
		var gallerySubtitleDisplayStyle = "block";
		if (currentPool == "usa"){
			if (getPageTitle() == "Gallery")
				gallerySubtitleDisplayStyle = "inline";
		}
		/************************/

		if (tabSubtitles) 
		{            
			for (var i = 0; i < tabSubtitles.length; i++) {
			    tabSubtitles[i].style.display = (i == n - 1) ? gallerySubtitleDisplayStyle : "none";				
			}
		}
    }
}
function getPageTitle() {
	for (var i = 0; i < document.getElementsByTagName('meta').length; i++) {
		if (document.getElementsByTagName('meta')[i].getAttribute('name') == 'titel') {
			pageTitle = document.getElementsByTagName('meta')[i].getAttribute('content');
}
}
}


function showGalleryClaim(n) {
    var claims = document.getElementById("galleryClaims");	   
    if (claims) 
    {    	
        var tabClaims = claims.getElementsByTagName("div");  
        
        var tab = document.getElementById("galleryClaimsTab" + getGallerySubtitleTabNo());
		if (tab) 
		{
			// zuerst nach Div suchen
			tabClaims = tab.getElementsByTagName("div");
			// 15.03.2006 by BU + MIP 
			// wenn kein <Div> vorhanden dann <Img> nehmen
			if (tabClaims.length == 0) {
				tabClaims = tab.getElementsByTagName("img");
			}
			tab.style.display = "block";				
		}

        /************************/
        /* STV: JDPower 2008-11 */
		var currentPool = getPoolName();
		var galleryClaimDisplayStyle = "block";
		if (currentPool == "usa")
		    galleryClaimDisplayStyle = "inline";
		/************************/
		
        if (tabClaims) {            
            for (var i = 0; i < tabClaims.length; i++) {
                tabClaims[i].style.display = (i == n - 1) ? galleryClaimDisplayStyle : "none";                
            }
        }
    }    
}


function getGalleryClaimTab()
{   
    var claim = document.getElementById("galleryClaims");
	return claim;	
}


function getGallerySubtitleTab()
{    
    var tab = document.getElementById("gallerySubtitles");    
    return tab;          
}


function getGallerySubtitleTabNo()
{
    var no = 1;
    
    var regex = /tabindex=(\d)/;
    var result = regex.exec(document.URL);
    
    if (result != null) { 
        no = result[1]
    }            
    
    return no;          
}

function getMaxNumberGallery()
{
    var max = 0;
    for (var i = 50; i >= 1 && max == 0; i--) {
        var testElm = document.getElementById("thumbnail" + i);
        if (testElm) {
            max = i;
            break;
        }
    }

    return max;
}

function showWallpaper(img, iWidth, iHeight)
{
    gotoUrlNewWin(img);
}


/* Open Selection Shop item in a window */
function openSelectionShopItem(dept_id, pf_id)
{
	gotoUrlNewWinSizeScrollableWithMenu('http://shop.eu.porsche.com/germany/product2.asp?dept_id=' + dept_id + '&pf_id=' + pf_id + '&comefrom=teq911', 870, 700)
}

function openPDDSShop(poolId) {
	switch (poolId) {
		case "germany":
			gotoUrlNewWinSizeScrollableWithMenuR('http://shop.porsche.com/germany/',870,700);
			break;
		
		case "usa":
			openPDDSPopup('http://shop.porsche.com/usa/',false);
			break;
		
		case "uk":
			openPDDSPopup('http://shop.porsche.com/uk/', false);
			break;
		
		case "france":
			openPDDSPopup('http://shop.porsche.com/france/', false);
			break;

		case "italy":
			openPDDSPopup('http://shop.porsche.com/italy/', false);
			break;

		case "australia":
			openPDDSPopup('http://shop.porsche.com/australia/', false);
			break;
	}
}

function openPDDSPopup(s, isRedesign) {
	var sWidth
	var sHeight;
	var sOffset;
	
	var resizable = 'no';
	var locationbar = 'no';
	var toolbar = 'no';
	var menubar = 'no';

	if (isRedesign) {
		sWidth = 890;
		sHeight = 750;
		resizable = 'yes';
		locationbar = 'yes';
		toolbar = 'yes';
		menubar = 'yes';
	}
	else {
		sWidth = 890;
		sHeight = 725;
	}
		
	
    if (screen.height < 768) {
        if (sHeight > 450) sHeight = 430;
        if (sWidth > 783) sWidth = 790;
        sOffset = 0;
    }
    else if (screen.height < 1024) {
        if (sHeight > 550) sHeight = 550;
        sOffset = 10;
    }
    else {
        sOffset = 40;
    }


    newWin = window.open(s, "newWin" + getRandomInt(10000), "width=" + sWidth + ",height=" + sHeight + ",left=40,top=40,dependent=yes,location=" + locationbar + ",menubar=" + menubar + ",toolbar=" + toolbar + ",resizable=" + resizable + ",scrollbars=yes,status=no");
}


// sMarket = Language / market version e.g. 'us' for PCNA market
// sModel = The Model typecode e.g. '987110'
// sMode = '2D', '3D' or null
function openCC(sMarketId, sModel, sMode, blnOpenWindow)
{
    var sWidth = 980;
    var sHeight = 680;
    sWidth -= g_is_ie ? 15 : 20;
    sHeight -= 25;
    var sUrl = '';
    var sHostName = '';
    var sOriginHostName = '';
    var sLang = '';
    var iViewMode=0;
	var sTestFolder = '';
    if(blnOpenWindow == null)blnOpenWindow = true;
    if(g_is_mac == true && sMode != null)sMode='2D';
	
	  switch(window.location.hostname) {
	      case "intranet.porsche.com":
		      sHostName = "http://cc.web.porsche.de";
		      sOriginHostName = "http://cc.web.porsche.de";
		      break;
        case "preview.porsche.com":
		      sHostName = "http://testarea-cc.porsche.com";
		      sOriginHostName = "http://testarea-cc.porsche.com";
			  sTestFolder = '_uat';
		      break;
        default:
		      sHostName = "http://cc.porsche.com";
		      OriginHostName = "http://origin-cc.porsche.com";
		      break;
	  }

    if(sMode == null) {
      if(sModel == null) {
        sUrl = sHostName + "/pva_new{TESTFOLDER}/ui/pva/index.jsp?sprache={LANG}&modelRange=null";
      }
      else {
        sUrl = sHostName + "/pva_new{TESTFOLDER}/colorConfigurator.do?userID={USER}&lang={LANG}&PARAM={PARAM}&ORDERTYPE={MODEL}";
      }
    }
    else {
      if(sMode == '2D')iViewMode=2;
      if(sMode == '3D')iViewMode=4;
      if(sModel == null) {
        sUrl = sHostName + "/pva_new{TESTFOLDER}/preConfiguration.do?userID={USER}&lang={LANG}&PARAM={PARAM}&PRECONFIG_ID=default&vLevel={MODE}";
      }
      else {
        sUrl = sHostName + "/pva_new{TESTFOLDER}/preConfiguration.do?userID={USER}&lang={LANG}&PARAM={PARAM}&PRECONFIG_ID=default&vLevel={MODE}";
      }
    }

    switch (sMarketId) {
        case "us":
            sUser = "US";
            sLang = "us"
            sParam = "parameter_internet_us"
			break;
            
        default:
            sUrl = sHostName + "/pva_new{TESTFOLDER}/colorConfigurator.do?userID=US&lang=us&PARAM=parameter_internet_us&ORDERTYPE=" + sModel;
            break;
    }
    if(sUrl != '') {
      sUrl = sUrl.replace(/{USER}/g, sUser);
      sUrl = sUrl.replace(/{LANG}/g, sLang);
      sUrl = sUrl.replace(/{PARAM}/g, sParam);
      sUrl = sUrl.replace(/{MODEL}/g, sModel);
      sUrl = sUrl.replace(/{MODE}/g, iViewMode);
      sUrl = sUrl.replace(/{TESTFOLDER}/g, sTestFolder);
    
    
    var screenParam = getScreenXYUrlParam();
    
    // CC Down Hack
    //sUrl = sHostName + "/maintenance/CC_down.html";
    if (sUrl && sUrl.length > 0)
    {
        sUrl += screenParam;
        sUrl += "&RT=" + new Date().getTime();
    }
      
      
      if(blnOpenWindow)gotoUrlNewWinSizeByName(sUrl, sWidth, sHeight, "PVA");
      else window.location.href=sUrl;
    }
}


/* Windows module */

function configureCar(marketId)
{
    /* For new CC inner size is important, but gotoUrlNewWinSizeByName() calcs outer size */
    var sWidth = 980;
    var sHeight = 680;
    sWidth -= g_is_ie ? 15 : 20;
    sHeight -= 25;
    var testFolder = '';

	/* R.W. Anpassung f�r den Intranet Server, um unterschiedliche CC URL's auszuliefern.*/
	var HostName = "";
	if (window.location.hostname == "intranet.porsche.com") {
		HostName = "http://cc.web.porsche.de";
		OriginHostName = "http://cc.web.porsche.de";
	} else if (window.location.hostname == "preview.porsche.com") {
		HostName = "http://testarea-cc.porsche.com";
		OriginHostName = "http://testarea-cc.porsche.com";
		testFolder = '_uat';
	} else {
		HostName = "http://cc.porsche.com";
		OriginHostName = "http://origin-cc.porsche.com";
	}
		
    var sUrl = '';
    switch (marketId) {
        case "de":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=de&modelRange=null";
            break;

        case "us":
            /* JDPower Test */
            sUrl = "/all/usa/startcc/default.htm?t=";
			if (window.location.hostname == "preview.porsche.com") {
				sUrl = "http://testarea-cc.porsche.com/pva_new" + testFolder + "/ui/pva/index.jsp?sprache=us";
			}
            //sUrl = HostName + "/pva_new/ui/pva/index.jsp?sprache=us&modelRange=null";
            break;

        case "ca":
            sUrl = HostName + "/pva_new" + testFolder + "/ui/pva/index.jsp?sprache=ca&modelRange=null";
            break;

        case "ca-fr":
            sUrl = HostName + "/pva_new" + testFolder + "/ui/pva/index.jsp?market=USCCF&sprache=cf&modelRange=null";
            break;

        case "fr":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=fr&modelRange=null";
            break;

        case "it":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=it&modelRange=null";
            break;

        case "en":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=en&modelRange=null";
            break;

        case "sp":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=sp&modelRange=null";
            break;

        case "mx":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=mx&modelRange=null";
            break;
            
        case "hk":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=hk&modelRange=null";
            break;
		
        case "me":
            sUrl = "/all/transitional/middle-east/models/countryselector/default.htm?t=";
            break;
		
		case "du":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=du&market=PDA&modelRange=null";
            break;
		case "ba":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=ba&market=PDB&modelRange=null";
            break;
		case "va":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=va&market=PDD&modelRange=null";
            break;
		case "kw":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=kw&market=PDK&modelRange=null";
            break;
		case "oa":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=oa&market=PDO&modelRange=null";
            break;
		case "qu":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=qu&market=PDT&modelRange=null";
            break;	
		case "ks":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=ks&market=PDS&modelRange=null";
            break;
		case "sa":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=sa&market=PDR&modelRange=null";
            break;
		
        case "jp":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=pj&modelRange=null";
            break;

        case "zh":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=cn";
            break;
        case "ce":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=ce&market=PACCE";
            break;
        case "ba":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?market=PDB&sprache=ba";
            break;
        case "va":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?market=PDD&sprache=va";
            break;
        case "kw":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?market=PDK&sprache=kw";
            break;
        case "oa":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?market=PDO&sprache=oa";
            break;
        case "qu":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?market=PDT&sprache=qu";
            break;
        case "ks":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?market=PDS&sprache=ks";
            break;
        case "cur":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=cc&customID=cur";
            break;
        case "pap":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=cc&customID=pap";
            break;
        case "bru":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=cc&customID=bru";
            break;
        case "ind":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=cc&customID=ind";
            break;
        case "mal":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=cc&customID=mal";
            break;
        case "nca":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=cc&customID=nca";
            break;
        case "sri":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=cc&customID=sri";
            break;
        case "phi":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=cc&customID=phi";
            break;
        case "du":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=du&market=PDA";
            break;
        case "sa":
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=sa&market=PDR";
            break;
		case "prt":
			sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=po";
			break;
		case "swiss-de":
			sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?market=CHSD&sprache=sd";
			break;
		case "swiss-fr":
			sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?market=CHSF&sprache=sf";
			break;
		case "swiss-it":
			sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?market=CHSI&sprache=si";
			break;
		case "bef":
			sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=cc&customID=bef";
			break;			
		case "ben":
			sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=cc&customID=ben";
			break;
	
        default:
            sUrl = HostName + "/icc_euro" + testFolder + "/ui/pva/index.jsp?sprache=" + marketId + "&modelRange=null";
            break;
    }
    
    var screenParam = getScreenXYUrlParam();
    
    if (sUrl && sUrl.length > 0)
    {
        sUrl += screenParam;
        sUrl += "&RT=" + new Date().getTime();
    }

    // CC Down Hack
    //sUrl = HostName + "/maintenance/CC_down.html";
    
    //log(sUrl);
    gotoUrlNewWinSizeByName(sUrl, sWidth, sHeight, "PVA");
}


function getScreenXYUrlParam()
{
    var clientWidth = screen.width;
    var clientHeight = screen.height;
    return  '&screen='+clientWidth+'x'+clientHeight;
}

/*
Function compareModels
Description: 
	Wrapper for compareModelMulti with empty model parameters. Is mainly used to open the CM Popup from the shopping buttons.
Input: 
	marketId: name of the market to be loaded in compare models.
Output: void
*/
function compareModels(marketId) {
	compareModelMulti(marketId, '', '', '', true);
}

/*
Function compareModelWith
Description:
	Wrapper for compareModelMulti to Open compare models tool (select page) with the given market name and two models to be compared to each other. 
Input:
	marketId: market name
	model1: First model's id.
	model2: Second model's id.	
Output: void.	
*/
function compareModelWith(marketId, model1, model2) {
	compareModelMulti(marketId, model1, model2, '', false);
}


/*
Function compareModelMulti
Description:
	Opens a popup window of variable size. 
	to load the compare models page for a given market, and three model IDs.
	The size can be adjusted for each market (pool) by setting the oneSizeFitsAll to false 
	and defining windowSizeX and windowSizeY for each pool.
Input:
	marketId: market (pool) name to be opened in the compare models tool.
	model1: First model's id.
	model2: Second model's id.	
	model3: Third model's id.
	openSelect: true if Select page is to be opened, false if compare page.
	(If further models are to be added, make the necessary changes in the url "cmUrl" and the window sizes.)
Output: void.	
*/
function compareModelMulti(marketId, model1, model2, model3, openSelect) {
	windowSizeX = 1003;
	windowSizeY = 625;
	oneSizeFitsAll = true;

	if (openSelect) 
		cmPage = "Select"
	else
		cmPage = "Compare";
		
	// make the window sizes pool dependent, if desired.
	if (!oneSizeFitsAll)
		switch (marketId) {
			case "usa":
				windowSizeX = 1003;
				windowSizeY = 625;
				break;
			case "canada":
				windowSizeX = 975;
				windowSizeY = 625;
				break;
		}
	// create the window and load the url.
	cmUrl = "/all/comparemodels/" +cmPage + ".aspx?pool=" + marketId + "&model1=" + model1 + "&model2=" + model2 + "&model3=" + model3 + "";
	gotoUrlNewWinSize(cmUrl, windowSizeX, windowSizeY);
}


function gotoUrlNewWinSimple(s)
{
    newWin = window.open(s, "newWin" + getRandomInt(10000));
}

function gotoUrlNewWin(s)
{
    sWidth = 826;
    sHeight = 610;

    sWidth += 20;
    sHeight += 25;
    newWin = window.open(s, "newWin" + getRandomInt(10000), "width=" + sWidth + ",height=" + sHeight + ",left=40,top=40,dependent=yes,location=no,resizable=no,scrollbars=no,status=no");
}

function gotoUrlNewWinSize(s, sWidth, sHeight)
{
    sWidth += 20;
    sHeight += 25;
    newWin = window.open(s, "newWin" + getRandomInt(10000), "width=" + sWidth + ",height=" + sHeight + ",left=40,top=40,dependent=yes,location=no,resizable=no,scrollbars=no,status=no");
}

	
function gotoUrlNewWinSizeCloseOnBlur(s, sWidth, sHeight)
{
    sWidth += 20;
    sHeight += 25;
    g_newWinFocus = window.open(s, "newWinCloseOnBlur", "width=" + sWidth + ",height=" + sHeight + ",left=40,top=40,location=no,resizable=no,scrollbars=no,status=no");	   	   	
    g_newWinFocus.focus();	    
    
    window.onfocus = function()
    {
    	if (g_newWinFocus)
    	{
    		g_newWinFocus.close()
    		g_newWinFocus = null;	    	
    	}
    }
}		

function gotoUrlNewWinSizeScrollable(s, sWidth, sHeight)
{
    sWidth += 20;
    sHeight += 25;
    newWin = window.open(s, "newWin" + getRandomInt(10000), "width=" + sWidth + ",height=" + sHeight + ",left=40,top=40,dependent=yes,location=no,resizable=no,scrollbars=yes,status=no");
}


function gotoUrlNewWinSizeScrollableWithMenu(s, sWidth, sHeight)
{
    sWidth += 20;
    sHeight += 50;

    if (screen.height < 768) {
        if (sHeight > 450) sHeight = 430;
        if (sWidth > 783) sWidth = 790;
        sOffset = 0;
    }
    else if (screen.height < 1024) {
        if (sHeight > 550) sHeight = 550;
        sOffset = 10;
    }
    else {
        sOffset = 40;
    }

    newWin = window.open(s, "newWin" + getRandomInt(10000), "width=" + sWidth + ",height=" + sHeight + ",left=" + sOffset + ",top=" + sOffset + ",dependent=yes,location=yes,menubar=yes,toolbar=yes,resizable=no,scrollbars=yes,status=yes");
}

function gotoUrlNewWinSizeScrollableWithMenuR(s, sWidth, sHeight)
{
    sWidth += 20;
    sHeight += 50;

    if (screen.height < 768) {
        if (sHeight > 450) sHeight = 430;
        if (sWidth > 783) sWidth = 790;
        sOffset = 0;
    }
    else if (screen.height < 1024) {
        if (sHeight > 550) sHeight = 550;
        sOffset = 10;
    }
    else {
        sOffset = 40;
    }

    newWin = window.open(s, "newWin" + getRandomInt(10000), "width=" + sWidth + ",height=" + sHeight + ",left=" + sOffset + ",top=" + sOffset + ",dependent=yes,location=yes,menubar=yes,toolbar=yes,resizable=yes,scrollbars=yes,status=yes");
}


function gotoUrlNewWinDefaultSize(s)
{
    sWidth = 570;
    sHeight = 610;

    sWidth += 20;
    sHeight += 25;
    newWin = window.open(s, "newWin" + getRandomInt(10000), "width=" + sWidth + ",height=" + sHeight + ",left=40,top=40,dependent=yes,location=no,resizable=no,scrollbars=no,status=no");
}

function gotoUrlNewWinSizeCentered(s, iWidth, iHeight)
{
    iWidth += 20;
    iHeight += 25;    
    iLeft = Math.round((screen.width - iWidth) / 2);
    iTop = Math.round((screen.height - iHeight) / 2);
    newWin = window.open(s, "newWin" + getRandomInt(10000), "width=" + iWidth + ",height=" + iHeight + ",left=" + iLeft + ",top=" + iTop + ",dependent=yes,location=no,resizable=no,scrollbars=no,status=no");
}

function gotoUrlNewWinSizeScrollableResizeable(s, sWidth, sHeight)
{
    sWidth += 20;
    sHeight += 25;
    newWin = window.open(s, "newWin" + getRandomInt(10000), "width=" + sWidth + ",height=" + sHeight + ",left=40,top=40,dependent=yes,location=no,resizable=yes,scrollbars=yes,status=no");
}

function gotoUrlNewWinSizeByName(s, sWidth, sHeight, sName)
{
    sWidth += g_is_ie ? 15 : 20;
    sHeight += 25;
    newWin = window.open(s, sName, "width=" + sWidth + ",height=" + sHeight + ",left=40,top=40,dependent=yes,location=no,resizable=no,scrollbars=no,status=no");
}

function openLeanWin(sUrl, sWidth, sHeight)
{
    sWidth += 20;
    sHeight += 25;
    newWin = window.open(sUrl, "newWin" + getRandomInt(10000), "width=" + sWidth + ",height=" + sHeight + ",left=140,top=140,dependent=yes,location=no,resizable=no,scrollbars=no,status=no,menubar=no,toolbar=no");
}

function gotoUrlFullscreen(sUrl, bScroll) {
	var width = screen.width;
	var height = screen.height;
	var left = 0;
	var top = 0;
	
	if (screen.width > 1920)
	{
		width = 1920;
		height = 1200;
		left = Math.round((screen.width - width) / 2);
		top = Math.round((screen.height - height) / 2);
	}
	
    var scrollbars = 0;
    if (bScroll) scrollbars = 1;
    
	var sProps = "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=" + scrollbars + ",resizable=0,left=" + left + ",top=" + top + ",width=" + (width - 10) + ",height=" + (height - 55);	
	var site = window.open(sUrl,"porschemicrosite", sProps);
	site.focus();		
}

function gotoUrlFullscreenResizable(sUrl) {
	var width = screen.width;
	var height = screen.height;
	var left = 0;
	var top = 0;
	
	if (screen.width > 1920)
	{
		width = 1920;
		height = 1200;
		left = Math.round((screen.width - width) / 2);
		top = Math.round((screen.height - height) / 2);
	}
	
	var sProps = "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=1,left=" + left + ",top=" + top + ",width=" + width + ",height=" + height;	
	var site = window.open(sUrl,"porschemicrosite_rs", sProps);
	site.focus();
}


function openDealerLocator(refForm, sWidth, sHeight, sName, sUrl, windowoptions) {
    if (!sUrl) sUrl = "";

    var topOffset = 40;

    if (sHeight > (self.screen.availHeight + topOffset - 40)) {
        sHeight = self.screen.availHeight - 40;
        topOffset = 0;
    }
    var options = "width=" + sWidth + ",height=" + sHeight + ",left=40,top=" + topOffset + ",dependent=yes,location=no,resizable=no,scrollbars=no,status=no";
    if (windowoptions) options = windowoptions;
    newWin = window.open(sUrl, sName, options);

    if (refForm) {
        refForm.target = sName;
        return true;
    }
    newWin.focus();
}

function openModelAdvisor(siteId)
{
	var iWidth = 831;
	var iHeight = 503;
	window.open("/all/modeladvisor/" + siteId + ".aspx", "newWin" + getRandomInt(10000) , "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0,left=" + Math.round((screen.width - iWidth) / 2) + ",top=" + Math.round((screen.height - iHeight) / 2) + ",,width=" + iWidth + ",height=" + iHeight);
}



function getRandomInt(max)
{
    return Math.round( Math.random() * (max-1) );
}


/* Ad-Tracker module */

function getAdTrackerNumber()
{
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    return a;
}



/* Home module */

function initHome() {

    
    if (document.getElementById) {
        g_initHome = true;
    }

    if (!g_is_ie) {

        // FF 3.x opacity+cleartype bugfix
        var i = 0;
        var topNavs = getElementsByClassName('topnavlink');

        for (i = 0; i < topNavs.length; i++) {
            topNavs[i].innerHTML += "&#160;";
        }

        var navSubs = getElementsByClassName('subnav');
        for (i = 0; i < navSubs.length; i++) {
            navSubs[i].innerHTML += "&#160;";
        }

    }

}

function bannerLinkToHi(sId)
{

    if (g_initHome)
    {
        var elm = document.getElementById(sId);
        if (elm)
        {
            elm.style.color = "#c00";
            //elm.style.backgroundImage = "url(/images/arrow-red.gif)";
            elm.style.backgroundPosition = "210px -142px;";
        }
    }

}

function bannerLinkToLo(sId)
{

    if (g_initHome)
    {
        var elm = document.getElementById(sId);
        if (elm)
        {
            elm.style.color = "rgb(102,102,102)";
            //elm.style.backgroundImage = "url(/images/arrow-gray.gif)";
            elm.style.backgroundPosition = "210px -292px;";
        }
    }
}

function navShadeIn(level)
{

    if (g_initHome)
    {
        if (level == 1)
        {
            if (g_timeoutHide1 != 0) { clearTimeout(g_timeoutHide1); }
        }
        else if (level == 2)
        {
            if (g_timeoutHide1 != 0) { clearTimeout(g_timeoutHide1); }
            if (g_timeoutHide2 != 0) { clearTimeout(g_timeoutHide2); }
        }
        else if (level == 3)
        {
            if (g_timeoutHide1 != 0) { clearTimeout(g_timeoutHide1); }
            if (g_timeoutHide2 != 0) { clearTimeout(g_timeoutHide2); }
            if (g_timeoutHide3 != 0) { clearTimeout(g_timeoutHide3); }
        }
    }
}

function navShadeOut(level)
{

    if (g_initHome)
    {
        if (level == 1)
        {
            g_timeoutHide1 = setTimeout("hideNav(1,\"" + g_currentNav1 + "\",\"" + g_currentHi1 + "\")", g_timeoutTime);
        }
        else if (level == 2)
        {
            g_timeoutHide1 = setTimeout("hideNav(1,\"" + g_currentNav1 + "\",\"" + g_currentHi1 + "\")", g_timeoutTime);
            g_timeoutHide2 = setTimeout("hideNav(2,\"" + g_currentNav2 + "\",\"" + g_currentHi2 + "\")", g_timeoutTime);
        }
        else if (level == 3)
        {
            g_timeoutHide1 = setTimeout("hideNav(1,\"" + g_currentNav1 + "\",\"" + g_currentHi1 + "\")", g_timeoutTime);
            g_timeoutHide2 = setTimeout("hideNav(2,\"" + g_currentNav2 + "\",\"" + g_currentHi2 + "\")", g_timeoutTime);
            g_timeoutHide3 = setTimeout("hideNav(3,\"" + g_currentNav3 + "\",\"" + g_currentHi3 + "\")", g_timeoutTime);
        }
    }
}

function overNav(level, sName, sSelf)
{
    if (g_initHome)
    {
        if (level == 0 || level == "0")
        {
            echo("level in 0");

            if (g_timeoutHide1 != 0) { clearTimeout(g_timeoutHide1); }

            hideSubLayers(g_currentNav1);
            loliteLr(level, g_currentHi1);

            if (g_currentNav1 != "") { hideSubLayers(g_currentNav1); }
            if (g_currentHi1 != "") { loliteLr(level, g_currentHi1); }

            showNav(level + 1, sName, sSelf);
            g_currentNav1 = sName;
            g_currentHi1 = sSelf;            
        }
        else if (level == 1 || level == "1")
        {
            echo("level in 1");

            if (g_timeoutHide1 != 0) { clearTimeout(g_timeoutHide1); }
            if (g_timeoutHide2 != 0) { clearTimeout(g_timeoutHide2); }

            if (g_currentNav2 != "") { hideSubLayers(g_currentNav2); }
            if (g_currentHi2 != "") { loliteLr(level, g_currentHi2); }

            showNav(level + 1, sName, sSelf);
            g_currentNav2 = sName;
            g_currentHi2 = sSelf;
        }
        else if (level == 2 || level == "2")
        {
            echo("level in 2");

            if (g_timeoutHide1 != 0) { clearTimeout(g_timeoutHide1); }
            if (g_timeoutHide2 != 0) { clearTimeout(g_timeoutHide2); }
            if (g_timeoutHide3 != 0) { clearTimeout(g_timeoutHide3); }

            if (g_currentNav3 != "") { hideSubLayers(g_currentNav3); }
            if (g_currentHi3 != "") { loliteLr(level, g_currentHi3); }

            showNav(level + 1, sName, sSelf);
            g_currentNav3 = sName;
            g_currentHi3 = sSelf;            
        }
        else if (level == 3 || level == "3")
        {
            echo("level in 3");

            if (g_timeoutHide1 != 0) { clearTimeout(g_timeoutHide1); }
            if (g_timeoutHide2 != 0) { clearTimeout(g_timeoutHide2); }
            if (g_timeoutHide3 != 0) { clearTimeout(g_timeoutHide3); }
            if (g_timeoutHide4 != 0) { clearTimeout(g_timeoutHide4); }

            if (g_currentNav4 != "") { hideSubLayers(g_currentNav4); }

            showNav(level + 1, sName, sSelf);
            g_currentNav4 = sName;
        }
    }
}

function loadImage(sName, sUrl)
{ 
    var elem = document.getElementById(sName + "_image");
    if (elem) elem.src = sUrl;
}



function outNav(level)
{
    if (g_initHome)
    {
        if (level == 0 || level == "0")
        {
            echo("level out 0");
            g_timeoutHide1 = setTimeout("hideNav(1,\"" + g_currentNav1 + "\",\"" + g_currentHi1 + "\")", g_timeoutTime);      
        }
        else if (level == 1 || level == "1")
        {
            echo("level out 1");
            g_timeoutHide1 = setTimeout("hideNav(1,\"" + g_currentNav1 + "\",\"" + g_currentHi1 + "\")", g_timeoutTime);
            g_timeoutHide2 = setTimeout("hideNav(2,\"" + g_currentNav2 + "\",\"" + g_currentHi2 + "\")", g_timeoutTime);
        }
        else if (level == 2 || level == "2")
        {
            echo("level out 2");
            g_timeoutHide1 = setTimeout("hideNav(1,\"" + g_currentNav1 + "\",\"" + g_currentHi1 + "\")", g_timeoutTime);
            g_timeoutHide2 = setTimeout("hideNav(2,\"" + g_currentNav2 + "\",\"" + g_currentHi2 + "\")", g_timeoutTime);
            g_timeoutHide3 = setTimeout("hideNav(3,\"" + g_currentNav3 + "\",\"" + g_currentHi3 + "\")", g_timeoutTime);
        }
        else if (level == 3 || level == "3")
        {
            echo("level out 3");
            g_timeoutHide1 = setTimeout("hideNav(1,\"" + g_currentNav1 + "\",\"" + g_currentHi1 + "\")", g_timeoutTime);
            g_timeoutHide2 = setTimeout("hideNav(2,\"" + g_currentNav2 + "\",\"" + g_currentHi2 + "\")", g_timeoutTime);
            g_timeoutHide3 = setTimeout("hideNav(3,\"" + g_currentNav3 + "\",\"" + g_currentHi3 + "\")", g_timeoutTime);
            g_timeoutHide4 = setTimeout("hideNav(4,\"" + g_currentNav4 + "\",\"" + g_currentHi4 + "\")", g_timeoutTime);
        }
        else
        {
            echo("level out is else: " + level);
        }         
    }
}

function dimHeadline(doOn)
{         
    var elem = document.getElementById("homeHead");
    
    if (elem) {
		// wtf?
        // if (parseInt(elem.style.top.replace(/px/, ""),10) > 88) {   
            
                if (doOn)
                {
                    elem.style.MozOpacity = ".99";
                    elem.style.Opacity = "1.00";
                    elem.style.opacity = "1.00";
                    elem.style.filter = "alpha(opacity=100)";
                }
                else
                {
                    elem.style.MozOpacity = ".40";
                    elem.style.Opacity = ".40";
                    elem.style.opacity = ".40";
                    elem.style.filter = "alpha(opacity=40)";
                }
        //}
    }
}

function showNav(level, sName, sSelf)
{
    dimHeadline(false);

    hiliteLr(level - 1, sSelf);
    var foundOne = showSubLayers(sName);
    if (foundOne)
    {
        if (g_canHandleTransparency)
        {
            var elm = document.getElementById("navshade" + level);
            showLr("navshade" + level);
        }
        else
        {
            var elm = document.getElementById("navshadeSimple" + level);
            showLr("navshadeSimple" + level);
        }
    }
    else
    {
        if (g_canHandleTransparency)
        {
            hideLr("navshade" + level);
        }
        else
        {
            hideLr("navshadeSimple" + level);
        }
    }     
}

function hideNav(level, sName, sSelf)
{
    if (level == 1) { g_timeoutHide1 = 0; g_currentNav1 = ""; g_currentSelf1 = ""; dimHeadline(true); }
    if (level == 2) { g_timeoutHide2 = 0; g_currentNav2 = ""; g_currentSelf2 = ""; }
    if (level == 3) { g_timeoutHide3 = 0; g_currentNav3 = ""; g_currentSelf3 = ""; }
    if (level == 4) { g_timeoutHide4 = 0; g_currentNav4 = ""; }

    loliteLr(level - 1, sSelf);

    if (g_canHandleTransparency)
    {
        hideLr("navshade" + level);
    }
    else
    {
        hideLr("navshadeSimple" + level);
    }

    hideSubLayers(sName);
}
function fadeAllOut(elem)
{
  if(elem == undefined)
    return;

  if(g_jdpowerphase == "none")
   return;

  var ID = elem.id
  var i = 0;
  var which = parseInt(ID.replace('link', '').replace('topnav', ''));
  for (; i <= G_MAXITEMS_MAINNAVI; i++)
  {
    if((i <= 5) && (which != i))
    {
      fadeOut('topnavlink' + i.toString());
    }
    if((i >= 6) && (which != i))
      fadeOut('topnavlink' + i.toString());
  }
}
function fadeAllIn(elem)
{
  if(elem == undefined)
    return;

  if(g_jdpowerphase == "none")
   return;

  var ID = elem.id
  var i = 0;
  var which = parseInt(ID.replace('link', '').replace('topnav', ''));
  for (; i <= G_MAXITEMS_MAINNAVI; i++)
  {
    if((i <= 5) && (which != i))
    {
      fadeIn('topnavlink' + i.toString());
    }
    if((i >= 6) && (which != i))
      fadeIn('topnavlink' + i.toString());
  }
}
function fadeOut(ID)
{
  var elem = document.getElementById(ID);
  if(elem)
  {
    elem.style.MozOpacity = ".5"; 
    elem.style.Opacity = ".5"; 
    elem.style.opacity = ".5"; 
    elem.style.filter = "Alpha(opacity=50, finishopacity=50, style=2)";
  }
}
function fadeIn(ID)
{
  var elem = document.getElementById(ID);
  if(elem)
  {
    elem.style.MozOpacity = "1.00"; // hack around weeeiiird bug which causes "1" to break NS7.02
    elem.style.Opacity = "1.00"; 
    elem.style.opacity = "1.00"; 
    elem.style.filter = "Alpha(opacity=100, finishopacity=100, style=2)";
  }
}

function hiliteLr(level, sName)
{
    var elem = document.getElementById(sName);
    if (elem)
    {
        if (level == 0 || level == "0")
        {
            //////////////////////
            var itemNo = parseInt(sName.replace('topnavlink', '').replace('topnav', ''));
            if (itemNo >= 6)
            {
                elem.style.backgroundColor = g_homemenu_hl_background_color_Level5;
            }
            else
            {
                elem.style.backgroundColor = g_homemenu_hl_background_color;
            }
            if ((g_jdpowerphase == "200803") && (itemNo <= 5))
            {
                var topNavi = document.getElementById("topnav" + itemNo.toString()); 
                topNavi.style.backgroundPosition = get_homenavi_background_position(g_home_modelnavi_num_modelranges_background, true); //"0px -550px";
                elem.style.backgroundPosition = get_homenavi_background_position(itemNo, true); //img;
            }
            else
            {
                elem.style.color = g_homemenu_hl_fontcolor; 
                if (! (elem.className.indexOf("final") >= 0) )
                {
                    //elem.style.backgroundImage = "url(/images/arrow-red.gif)";
                    elem.style.backgroundPosition = "210px -142px";
                }
            }
            fadeAllOut(elem);
            //////////////////////
        }
        else
        {
            elem.style.color = g_homemenu_hl_fontcolor; //"rgb(204, 0, 0)";
            elem.style.backgroundColor = "#FFFFFF";
            if (! (elem.className.indexOf("final") >= 0) )
            {
                //elem.style.backgroundImage = "url(/images/arrow-red.gif)";
                elem.style.backgroundPosition = "210px -142px";
            }

            elem.style.MozOpacity = ".99"; // hack around weeeiiird bug which causes "1" to break NS7.02
            elem.style.Opacity = "1.00"; 
            elem.style.opacity = "1.00"; 
            elem.style.filter = "alpha(opacity=100)";
        }
    }
}

function loliteLr(level, sName)
{
    var elem = document.getElementById(sName);
    if (elem)
    {
        if (level == 0 || level == "0")
        {

            //////////////////////
            var itemNo = parseInt(sName.replace('topnavlink', '').replace('topnav', ''));
	    if (itemNo >= 6)
            {
                elem.style.backgroundColor = g_homemenu_ll_background_color; 
            }
			
            if ((g_jdpowerphase == "200803") && (itemNo <= 5))
            {
                var topNavi = document.getElementById("topnav" + itemNo.toString()); // NOP
                topNavi.style.backgroundPosition = get_homenavi_background_position(g_home_modelnavi_num_modelranges_background, false); //"0px -500px";
                elem.style.backgroundPosition = get_homenavi_background_position(itemNo, false);
            }
            else
            {
                elem.style.color = g_homemenu_ll_fontcolor_level0; 
                elem.style.backgroundColor = g_homemenu_ll_background_color; 
                if (! (elem.className.indexOf("final") >= 0) )
                {
                    //elem.style.backgroundImage = g_homemenu_ll_background_image_level0;
                    elem.style.backgroundPosition = g_homemenu_ll_background_position_level0;
                }
            }
            fadeAllIn(elem);
            //////////////////////
        }
        else
        {
            elem.style.color = g_homemenu_ll_fontcolor_level1;
            elem.style.backgroundColor = "#FFFFFF";
            if (! (elem.className.indexOf("final") >= 0) )
            {
                //elem.style.backgroundImage = g_homemenu_ll_background_image_level1;
                elem.style.backgroundPosition = g_homemenu_ll_background_position_level1;
            }

            elem.style.MozOpacity = ".86";
            elem.style.Opacity = ".86"; 
            elem.style.opacity = ".86"; 
            elem.style.filter = "alpha(opacity=86)";
        }
    }
}

function showSubLayers(sName)
{
    var i = 0;
    var foundOne = false;

    for (i = 1; i <= G_MAXSUBLAYERS; i++)
    {
        var s = "nav_" + sName + "_" + i;
        var elem = document.getElementById(s);
        if (elem)
        {
            foundOne = true;
        }
        else
        {
            break;
        }

        showLr(s);
    }

    return foundOne;
}

function hideSubLayers(sName)
{
    var i = 0;
    var foundOne = false;

    for (i = 1; i <= G_MAXSUBLAYERS; i++)
    {
        var s = "nav_" + sName + "_" + i;
        var elem = document.getElementById(s);
        if (elem)
        {
            foundOne = true;
        }
        else
        {
            break;
        }

        hideLr(s);
    }

    return foundOne;
}

function echo(s)
{
    /*
    var elDebug = document.getElementById("debug");
    elDebug.innerHTML = "<div><span>" + (++g_echoCount) + ".</span> " + s + "</div>" + elDebug.innerHTML;
    */
}

function setModel(s)
{

    if (g_initHome)
    {
        g_currentModelLink = s;
    }
}

function gotoModel()
{

    if (g_initHome)
    {
        if (g_currentModelLink != '')
        {
            gotoUrl(g_currentModelLink);
        }
    }
}

function gotoUrl(s)
{
    g_psyma_navi_clicked = true;
    document.location.href = s;
}

function gotoUrlDropDown(e)
{
	if (e.options)
	{
		var url = e.options[e.options.selectedIndex].value;
		if(url != '')gotoUrl(url);			
	}
}

function gotoUrlTimeout(s, timeout)
{
  setTimeout("gotoUrl('" + s + "')", timeout)
}
function nogo()
{
    //
}

function selectAllInput(elm)
{
    if (elm)
    {
        elm.focus();
        elm.select();
    }
}

function changedCountry(elmThis)
{
    if (document.getElementById)
    {
        var sUrl = elmThis.value;

        if (sUrl != "")
        {
            document.location.href = sUrl;
        }
    }
}

function sniffAll()
{
    /* JavaScript Browser Sniffer
       Eric Krok, Andy King, Michel Plungjan Jan. 31, 2002
       see http://www.webreference.com/ for more information
       This program is free software */

    var agt=navigator.userAgent.toLowerCase();
    var appVer = navigator.appVersion.toLowerCase();

    var is_minor = parseFloat(appVer);
    var is_major = parseInt(is_minor);

    var is_opera = (agt.indexOf("opera") != -1);

    /*
    var is_opera6 = (agt.indexOf("opera 6") != -1 || agt.indexOf("opera/6") != -1);
    var is_opera7 = (agt.indexOf("opera 7") != -1 || agt.indexOf("opera/7") != -1);
    var is_opera6up = (is_opera && !is_opera2 && !is_opera3 && !is_opera4 && !is_opera5);
    var is_opera7up = (is_opera && !is_opera2 && !is_opera3 && !is_opera4 && !is_opera5 && !is_opera6);
    */

    var iePos  = appVer.indexOf('msie');
    if (iePos !=-1) {
       is_minor = parseFloat(appVer.substring(iePos+5,appVer.indexOf(';',iePos)));
       is_major = parseInt(is_minor);
    }

	
    var is_konq = false;
    var kqPos   = agt.indexOf('konqueror');
    if (kqPos !=-1) {                 
       is_konq  = true;
       is_minor = parseFloat(agt.substring(kqPos+10,agt.indexOf(';',kqPos)));
       is_major = parseInt(is_minor);
    }

    var is_getElementById   = (document.getElementById) ? "true" : "false";
    var is_getElementsByTagName = (document.getElementsByTagName) ? "true" : "false";
    var is_documentElement = (document.documentElement) ? "true" : "false";

    var is_safari = ((agt.indexOf('safari')!=-1)&&(agt.indexOf('mac')!=-1))?true:false;
    var is_khtml  = (is_safari || is_konq);

    var is_gecko = ((!is_khtml)&&(navigator.product)&&(navigator.product.toLowerCase()=="gecko"))?true:false;
    var is_gver  = 0;
    if (is_gecko) is_gver=navigator.productSub;

    var is_moz   = ((agt.indexOf('mozilla/5')!=-1) && (agt.indexOf('spoofer')==-1) &&
                    (agt.indexOf('compatible')==-1) && (agt.indexOf('opera')==-1)  &&
                    (agt.indexOf('webtv')==-1) && (agt.indexOf('hotjava')==-1)     &&
                    (is_gecko) && 
                    ((navigator.vendor=="")||(navigator.vendor=="Mozilla")));

	var is_mac = ((agt.indexOf('macintosh')!=-1))?true:false;
	var is_linux = (navigator.platform.toLowerCase().indexOf('linux')!=-1)?true:false;
	var is_win = (navigator.platform.toLowerCase().indexOf('win')!=-1)?true:false;
	
    if (is_moz) {
       var is_moz_ver = (navigator.vendorSub)?navigator.vendorSub:0;
       if(!(is_moz_ver)) {
           is_moz_ver = agt.indexOf('rv:');
           is_moz_ver = agt.substring(is_moz_ver+3);
           is_paren   = is_moz_ver.indexOf(')');
           is_moz_ver = is_moz_ver.substring(0,is_paren);
       }
       is_minor = is_moz_ver;
       is_major = parseInt(is_moz_ver);
    }

    var is_nav  = ((agt.indexOf('mozilla')!=-1) && (agt.indexOf('spoofer')==-1)
                && (agt.indexOf('compatible') == -1) && (agt.indexOf('opera')==-1)
                && (agt.indexOf('webtv')==-1) && (agt.indexOf('hotjava')==-1)
                && (!is_khtml) && (!(is_moz)));

    if ((navigator.vendor)&&
        ((navigator.vendor=="Netscape6")||(navigator.vendor=="Netscape"))&&
        (is_nav)) {
       is_major = parseInt(navigator.vendorSub);
       is_minor = parseFloat(navigator.vendorSub);
    }

    var is_nav2 = (is_nav && (is_major == 2));
    var is_nav3 = (is_nav && (is_major == 3));
    var is_nav4 = (is_nav && (is_major == 4));
    var is_nav4up = (is_nav && is_minor >= 4); 
    var is_navonly  = (is_nav && ((agt.indexOf(";nav") != -1) ||
                          (agt.indexOf("; nav") != -1)) );

    var is_nav6   = (is_nav && is_major==6);
    var is_nav6up = (is_nav && is_minor >= 6);

    var is_nav5   = (is_nav && is_major == 5 && !is_nav6);
    var is_nav5up = (is_nav && is_minor >= 5);

    var is_nav7   = (is_nav && is_major == 7);
    var is_nav7up = (is_nav && is_minor >= 7);

    var is_ie   = ((iePos!=-1) && (!is_opera) && (!is_khtml));
    var is_ie3  = (is_ie && (is_major < 4));

    var is_ie4   = (is_ie && is_major == 4);
    var is_ie4up = (is_ie && is_minor >= 4);
    var is_ie5   = (is_ie && is_major == 5);
    var is_ie5up = (is_ie && is_minor >= 5);
    
    var is_ie5_5  = (is_ie && (agt.indexOf("msie 5.5") !=-1));
    var is_ie5_5up =(is_ie && is_minor >= 5.5);
    
    var is_ie6   = (is_ie && is_major == 6);
    var is_ie6up = (is_ie && is_minor >= 6);

	// OS
	g_is_win = is_win;
	g_is_mac = is_mac;
	g_is_linux = is_linux;
	
    g_is_ie = is_ie;
    g_is_opera = is_opera;
    g_is_ie5 = is_ie5;
    g_is_ie5_5 = is_ie5_5;
    g_is_ie6 = is_ie6;
    g_is_safari = is_safari;
}



/* Module Motorschaubild */

function showDetail(item) {
    getDiagramDetail(item).style.display = "block";
}
    
function hideDetail(item) {
    getDiagramDetail(item).style.display = "none";
}    
    
function getDiagramDetail(item) {
    return document.getElementById("engineDiagramDetail" + item.getAttribute("id").substr(17,2));
}

function switchDiagramPage()
{
   var page1 = document.getElementById("engineDiagramPage1");     
   var page2 = document.getElementById("engineDiagramPage2");
     
   if (page1 && page2) {
        var show2ndPage = ((page1.style.display == "") || (page1.style.display == "block"));
        
        var control = document.getElementById("engineDiagramControl");
                
        if (show2ndPage) {
                page1.style.display = "none";
                page2.style.display = "block";
                control.innerHTML = "<a href=\"javascript:switchDiagramPage();\">&lt; 2/2</a>";
        } else {
                page1.style.display = "block";
                page2.style.display = "none";
                control.innerHTML = "<a href=\"javascript:switchDiagramPage();\">1/2 &gt;</a>";                
        }
   } 
}

var g_currentDiagramPage = 1;

function nextDiagramPage(diagramID)
{
	showDiagramPage(g_currentDiagramPage + 1, diagramID);
}
function previousDiagramPage(diagramID)
{
	showDiagramPage(g_currentDiagramPage - 1, diagramID);
}
function showDiagramPage(pageToDisplay, diagramID)
{
	if(!pageToDisplay)
		pageToDisplay = 1;
	if(!diagramID)
		diagramID = "";

    var pageToShow = null;
    var pageCount = 1;
    var page = document.getElementById(diagramID + "Page" + pageCount);
    while(page){
        pageCount++;
        var page = document.getElementById(diagramID + "Page" + pageCount);
    }
    var maxPages = pageCount - 1;

	var pageToHide = document.getElementById(diagramID + "Page" + g_currentDiagramPage);
	if(pageToHide)
		pageToHide.style.display = "none";

	var	pageToShow = document.getElementById(diagramID + "Page" + pageToDisplay);
	if(pageToShow)
		pageToShow.style.display = "block";

	var pages = pageToDisplay + "&#160;/&#160;" + maxPages;
	var previous = (pageToDisplay == 1) ? "&#160;" : "&#160;&#160;&lt;&#160;&#160;";
	var next = (pageToDisplay == maxPages) ? "&#160;&#160;&#160;&#160;&#160;&#160;" : "&#160;&#160;&gt;&#160;&#160;";
	var control = document.getElementById(diagramID + "Control");
	if(control)
	{
		if(maxPages == 1)
			control.innerHTML = previous + pages + next;
		else if (pageToDisplay == maxPages)
			control.innerHTML = "<a href=\"javascript:previousDiagramPage('" + diagramID + "');\">" + previous + "</a>" + pages + next;
		else if(pageToDisplay == 1)
			control.innerHTML = previous + pages + "<a href=\"javascript:nextDiagramPage('" + diagramID + "');\">" + next + "</a>";
		else
			control.innerHTML = "<a href=\"javascript:previousDiagramPage('" + diagramID + "');\">" + previous + "</a>"
							 + pages
							 + "<a href=\"javascript:nextDiagramPage('" + diagramID + "');\">" + next + "</a>";

		g_currentDiagramPage = pageToDisplay;
	}
}
  
/* Module Image Switch */

function updateImageSwitch(elm, state)
{
	var elmChild = null;
	
	if (elm.childNodes[0].style != undefined) {
		elmChild = elm.childNodes[0];
	} else {
		elmChild = elm.childNodes[1];
	}

    switch (state) {
        case "active":    

            var i = 1;
            do {
                var elmInactive = document.getElementById("switchImage" + i);
                if (elmInactive != null) {
                    updateImageSwitch(elmInactive, "inactive");
                }
                i++;
            } while (elmInactive != null)

            elm.style.color = "rgb(204,0,0)";
            elm.style.background = "white url(/images/arrow-red.gif) no-repeat left 3px";
            elmChild.style.display = "block";                      
            break;
        case "inactive":
            elm.style.color = "rgb(102,102,102)";
            elm.style.background = "white url(/images/arrow-gray.gif) no-repeat left 3px";
            elmChild.style.display = "none";                                      
    }
}

/* Macht ein Redirect zur Value des ausgew�hlten Elements der angegebenen Select Box */
function RedirectToOptionValue(name)
{
  obj = document.getElementById(name);
  if(obj)
  {
    link = obj.options[obj.selectedIndex].value;
    if(link.length > 0)
    {
      document.location.href = link;
    }
  }
}


/* SSO Cookie Check (to support caching) */

function ShowIfLoggedIn(id)
{
    if ( isLoggedIn() ) {
        var elm = document.getElementById(id);
        if (elm) {
            elm.style.display = "inline";
        }
    }
}

function HideIfLoggedIn(id)
{
    if ( isLoggedIn() ) {
        var elm = document.getElementById(id);
        if (elm) {
            elm.style.display = "none";
        }
    }
}

function isLoggedIn()
{
    var status = getCookie("SSO_LOGIN");
    if (status == null || status=='false') { status = false; }
    return status;
}

function getCookie(name)
{
    var thisCookie = document.cookie;
    var index = thisCookie.indexOf(name + "=");
    if (index == -1) return null;
    index = thisCookie.indexOf("=", index) + 1;
    var endstr = thisCookie.indexOf(";", index);
    if (endstr == -1) endstr = thisCookie.length;
    return unescape(thisCookie.substring(index, endstr));
}

function replStr(str,oldStr,newStr)
{ var strPos=str.indexOf(oldStr);
  return (strPos>=0) ?
    str.substring(0,strPos) + newStr +
       replStr( str.substring(strPos +
       oldStr.length), oldStr, newStr ) :
    str
}

function gotoUrlIE(url)
{
    g_psyma_navi_clicked = true;
    if (g_is_ie) {
        gotoUrl(url);
    }
}

function submitSDSTrainingSearchForm()
{
	var sfsForm = document.getElementById("SDSTrainingSearchForm");
	if (sfsForm.month.options.selectedIndex != 0)
		sfsForm.submit();
}

//
// Function ieToggleDropDowns: 
//
// Description: Toggles dropdown visibility in case of a dynamic layer 
// covering them pops up, and has a z-index higher than these. IE does not handle them correctly.
//
// Input: 
// 		strAttr: String Array, whose elements are the <DIV> id's to toggle. See function: getListIEDropDowns() 
//		boolHide: Boolean toggle. true, if the layer should be made visible, false otherwise.
// Returns : none.
function ieToggleDropDowns(strArr, boolHide) {
	// collection of dropdowns
	
	if (strArr.length > 0) {
		for (i=0; i < strArr.length; i++) {
			dd = document.getElementById(strArr[i]);
			if (dd) 
				if (boolHide) 
				{
					dd.style.visibility = 'hidden'
				}
				else
				{
					dd.style.visibility = 'visible';
				}
		}
	}
}

//
// Function getListIEDropDowns():
//
// Description: Builds an array of Dropdowns whose visibility should be toggled 
// in case of a dynamic layer covering them.
// Input: none.
// Returns: String array of <DIV> id's.
function getListIEDropDowns() {
	return (new Array("monthDropDown", "modelprice", "modelpower"));
}

/*
	Created		: 09.01.2006 by Martin Ibanez
	Last edited	: 10.01.2006 by Martin Ibanez
	Comment		: Wurde f�r die Galerie erstellt
*/
function showGalleryQuicktime(src)
{
	var elmImg = document.getElementById("galleryImage");
    if (elmImg) {
        g_fallbackImage = elmImg.src;
    }
    
	showQuicktime(src, 'galleryContent', '595', '317');
}

/*
	Created		: 09.01.2006 by Martin Ibanez
	Last edited	: 28.03.2006 by Stefan Kokemueller
	Comment		: Wurde f�r die Galerie erstellt
*/
function showQuicktime(src, layer , width, height)
{
	var elm = document.getElementById(layer);
	
	if (elm && isQTInstalled())
	{
		var qtstring = '';
		qtstring += '<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab" width="'+width+'" height="'+height+'" id="galleryQuicktimeMovie">' 
		qtstring += '<param name="src" value="'+src+'"><embed height='+height+' width='+width+' src="'+src+'" type="video/quicktime" pluginspage="www.apple.com/quicktime/download" enablejavascript="true" name="galleryQuicktimeMovie" />'
		qtstring += '</object>'
		
		elm.innerHTML = qtstring;
	}
}

/*
	Created		: 09.01.2006 by Martin Ibanez
	Last edited	: 10.01.2006 by Martin Ibanez
	Comment		: Wurde f�r die Galerie erstellt
*/
function showFullscreenQuicktime(src)
{
    if (isQTInstalled())
	{
		sWidth = window.screen.availWidth;
		sHeight = window.screen.availHeight;
		var doc = window.open("/all/transitional/all/panoramaPopup/panoramaPopup.asp?qt=" + src, "newWin" + getRandomInt(10000), "width=" + sWidth + ",height=" + sHeight + ",left=0,top=0,dependent=yes,location=no,resizable=yes,scrollbars=no,status=no");			
		doc.focus();
	}
}

/*
	Created		: 28.03.2006 by Stefan Kokemueller
	Last edited	: 28.03.2006 by Stefan Kokemueller
	Comment		: Wurde f�r die Galerie erstellt
*/
function showFullscreenQuicktimeObject(src)
{
    if (isQTInstalled())
	{
		sWidth = window.screen.availWidth;
		sHeight = window.screen.availHeight;
		var doc = window.open("/all/transitional/all/panoramaPopup/objectPopup.asp?qt=" + src, "newWin" + getRandomInt(10000), "width=" + sWidth + ",height=" + sHeight + ",left=0,top=0,dependent=yes,location=no,resizable=yes,scrollbars=no,status=no");			
		doc.focus();
	}
}
function showPanoramaPopup(pool, id, lang, panoramaversion, width, height, callpath)
{
	sWidth = window.screen.availWidth;
	sHeight = window.screen.availHeight;
	
	if(width && width > 0)
	{
		sWidth = width;		
	}
	
	if(height && height > 0)
	{
		sHeight = height;		
	}
	
	var url = "/panoramaPopup.aspx?Pool=" + pool + "&ID=" + id + "&Lang=" + lang + "&Panoramaversion=" + panoramaversion;	
	if (callpath && callpath.length > 0)
	{
		url += "&callpath=" + callpath;
	}
	
	var doc = window.open(url , "newWin" + getRandomInt(10000), "width=" + sWidth + ",height=" + sHeight + ",left=0,top=0,dependent=yes,location=no,resizable=yes,scrollbars=no,status=no");			
	doc.focus();
}
function writePopupQuicktimeObject(src,width,height)
{
    if (isQTInstalled())
	{
		var s = '';								
	
		var sWidth = '100%';
		var sHeight = '100%';
	
		var divWidth = "100%";
		var divHeight = "87%";
		
		if (width && width > 0)
		{
			sWidth = width;
			divWidth = width;
		}
		
		if (height && height > 0)
		{			
			sHeight = height + 15;
			divHeight = height + 15;
			
			if ( viewportGetHeight() > (sHeight + 50) )
			{
			  spacerHeight = (viewportGetHeight() - sHeight) * 0.43;					  
			  s += '<div style="height: ' + spacerHeight + 'px; width:100%; font-size:1px;"></div>\n';
			}
			else
			{
			  document.body.style.overflow = 'auto';
			}			
		}	
		
		s += '<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" width="'+sWidth+'" height="'+sHeight+'" codebase="http://www.apple.com/qtactivex/qtplugin.cab" id="quicktimePlugin">';
		s += '	<param name="scale" value="tofit"><param name="controller" value="true"><param name="cache" value="false"><param name="bgcolor" value="#E9E9E9"><param name="volume" value="30"><param name="kioskmode" value="false"><param name="moviename" value="spincontrolled">';
		s += '	<param name="src" value="'+src+'">';
		s += '	<embed src="'+src+'" ';
		s += '		width="'+sWidth+'" ';
		s += '		height="'+sHeight+'" ';
		s += '		type="video/quicktime" ';
		s += '		controller="true"';
		s += '		cache="false" ';
		s += '		volume="30"';
		s += '		kioskmode="false"';
		s += '		scale="tofit" ';
		s += '		pluginspage="http://www.apple.com/quicktime/download/" ';
		s += '		bgcolor="#E9E9E9"';
		s += '		moviename="spincontrolled"';
		s += '		/>';
		s += '</object>';
		
		var elm = document.getElementById('panorama');	
		if(elm)
		{
			elm.style.width = divWidth;
			elm.style.height = divHeight;		
			elm.innerHTML = s;
		}			
		
	}
	else
	{
		document.getElementById('noPlugin').style.display = 'block';
		document.getElementById('claim').style.display = 'none';
	}	
}
function myreplace(text,from,to) 
{
	ti = text.indexOf(from);
	text = text.substring(0,ti)+to+text.substr(ti+from.length);
	
	return text;
} 
function writePopupFlashObject(src,width,height)
{	
	if (swfobject.hasFlashPlayerVersion("6"))
	{
		var sWidth = '100%';
		var sHeight = '100%';
		
		var divWidth = '100%';
		var divHeight = '87%';
		
		if (width && width > 0)
		{
			sWidth = width;
			divWidth = width;
		}
		
		if (height && height > 0)
		{
			sHeight = height;
			divHeight = height;
		}

		var elm = document.getElementById('panorama');	
		if(elm)
		{
			elm.style.width = divWidth;
			elm.style.height = divHeight;
		}	
			
		// show flash
		showFlash(src, 'panorama', sWidth, sHeight, 6, '#E9E9E9', 'loop=true,menu=true,quality=high,wmode=opaque', null)
		
	}
	else
	{
		document.getElementById('noPlugin').style.display = 'block';
		document.getElementById('claim').style.display = 'none';
	}	
}
// geh�rt zu jeder gescheiten JS-Bibliothek.
function trim(str)
{
 	return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
}

/* Toggleable language selector */

function writeLanguageSelector(pool, codes, texts, urls, selectedCode, containerId, defaultText)
{
    if (document.addEventListener) 
    {
			document.addEventListener("DOMFocusIn", langSelectorCheckBlur, true);
	}
	document.onmousedown = langSelectorCheckBlur;
	listId = 'HomeLangSelect';
	var i;
	var selectedIndex = 0;
	
	for (i=0; i<codes.length; i++)
	{
	    if (codes[i] == selectedCode) 
	    {
	        selectedIndex = i;
	        break;
	    }
	}
	var html = "";
	html += "<div class=\"langSelect\" onclick=\"toggleLr('" + listId + "')\">\n";
	html += "<div id='langSelectDiv'><div type=\"text\" class=\"langSelectCurrent\">" + (defaultText == undefined ? texts[selectedIndex] : defaultText)  + "</div>\n";
	html += "<div class=\"langSelectArrow\"></div></div>\n";
	html += "</div>\n";
	
	html += "<ul id=\"" + listId + "\" class=\"langSelectList\">\n";
	for (i=0; i< codes.length; i++)
	{
	    html += "<li onmouseover=\"mOverLangList(this, '"+ pool + "');\" onmouseout=\"mOutLangList(this, '"+ pool + "');\"><a href=\"" + urls[i] + "\">" + texts[i] + "</a></li>\n";
	}
	html += "</ul>\n";
	
	var container = document.getElementById(containerId);
	container.innerHTML = html;
}

function mOverLangList(elm, pool)
{
    if (!elm) return;
    poolid = pool.toLowerCase();
    switch(poolid)
    {
        case 'pco':
            elm.style.backgroundColor = '#E9E9E9';
			break;
        default:
            elm.style.backgroundColor = '#E9E9E9';
			break;
    }
}


function mOutLangList(elm, pool)
{
    if (!elm) return;
    poolid = pool.toLowerCase();
    switch(poolid)
    {
        case 'pco':
            elm.style.backgroundColor = '#FFFFFF';
			break;
        default:
            elm.style.backgroundColor = '#FFFFFF';
			break;
    }
}

if (window.Node && Node.prototype && !Node.prototype.contains) 
{
    Node.prototype.contains = function (arg)
    {
	    return !!(this.compareDocumentPosition(arg) & 16);
	};
}

function langSelectorCheckBlur(e)
{
    element = document.getElementById("langSelectDiv");	
	dropdownList = document.getElementById("HomeLangSelect");
	e = e || window.event;
	var target = e.target || e.srcElement || false;
	if (!target) {
		return;
	}

	if (target == element || element.contains(target) || target == dropdownList || dropdownList.contains(target)) 
	{		
	}
	else
	{
		hideLr("HomeLangSelect");
	}
	
}

function checkClassic(data)
{
	var now = new Date();
	var result = new Array();
	
	// wenn das Produktionsjahr l�nger als 10 Jahre zur�ckliegt, und im 11. Jahr schon August ist, dann kommt der wagen zu Classic
	for(i = 0; i < data.length; i++)
	{
		var diff = now.getFullYear() - data[i][3];
		if(diff > 10)
		{
			if(diff == 11 && now.getMonth() < 8) {}
			else result.push(data[i]);
		}
	}
	return result;	
}

/* Start dealer search */
function porscheCenterSearchHandleError(errCode, errorMessage, fallBackMessage, searchBlockShadowId) {
    preInitPorscheCenterSearch(searchBlockShadowId);
    var msg;
    if (errorMessage && trim(errorMessage) != '') {
        msg = errorMessage;
        log("INFO: Text \"" + errCode + "\" gefunden.");
    }
    else {
        msg = fallBackMessage; // fallback error message.
        log("FEHLER: Text \"" + errCode + "\" nicht gefunden! Falls zur�ck auf den generischen Text '" + escape(fallBackMessage) + "'");
    }
    $("<p class=\"error\"></p>").text(msg).appendTo("#porscheCenterSearchResults");    
}


function preInitPorscheCenterSearch(searchBlockShadowId) {

    //$("a.homeHead, #p_searchSite *").fadeTo(150, 0.3);
    setupFadeEffectsDealerSearch(true);
    
    var currentPool = getPoolName();
    // handle intro-facts
    if ($(".introFacts").length > 0) {
        // stretch the result container downwards
        $("#porscheCenterSearchResults").css("height", "259px");
    }
    // set initial height;
    if (currentPool == 'usa') {
        $("#" + searchBlockShadowId + "").css("height", "60px");
        // "loading.." Gif animation.
        $("#porscheCenterSearchResults").addClass("loading");
    }
    else {
        // "loading.." Gif animation.
        $("#porscheCenterSearchResults").addClass("loading");
   
    }
    
    
}

function setupFadeEffectsDealerSearch(fadeIn) {
    var currentPool = getPoolName();
    var fadeFactor = (fadeIn ? 0.3 : 1.0);
    var fadeSpeed = 150;
    var elements;
    if (currentPool == 'usa') {
        elements = "a.homeHead";
    }
    else {
        elements = "a.homeHead, #p_searchSite *";
    }
    $(elements).fadeTo(fadeSpeed, fadeFactor);
}

// initDealerSearchbox helper function
function initPorscheCenterResultsDisplay(searchBlockId) {

    // hide introfacts elements
    $('.introFactsShade, .introFactsContent').hide();
    // cancel the "loading.." sequence.
    $('#porscheCenterSearchResults').removeClass('loading');
    // make site search almost almost transparent 
    //to draw the focus on the center search.
    
    // setup region click sensing to close the window when clicked "outside".
    handleClickOutside($("#" + searchBlockId), "PCSearchClickedOutsideEvents(\"" + searchBlockId + "\")", true);
    $(".porscheCenterSearchBG").show();
}

function PCSearchClickedOutsideEvents() {
    var currentPool = getPoolName();
    // reset heights
    $("#porscheCenterSearchResults").css("height", "auto");
    //$(".porscheCenterSearchBG").css("height", ""); // STV: Hompage IE 7
    // reset site search visibility
    setupFadeEffectsDealerSearch(false);
    // hide result container.
    $('.porscheCenterSearchBG').hide();
    // reset searchbox shadow to original height
    var height;
    if (currentPool == 'usa') {
        height = "64px";
    }
    else {
        height = "34px";
    }
    $("#dealersearchHomeShadow, #searchsitePageShadow").css("height", height);
    // if they are available, re-display intro-facts elements.
    $('.introFactsShade, .introFactsContent').show();

}

function handleClickOutside(obj, doEvent, bindOnce) {
    
    var elm;
    if (obj.length && obj.length > 0)
        elm = obj[0];
    else
        elm = obj;
  
    //
    // *******************************
    //  click management 
    // ******************************* 
    //
    $(document).click(function(e) { // when anywhere in the doc is clicked
        var clickedOutside = true; // start searching assuming we clicked outside
        $(e.target).parents().andSelf().each(function() { // search parents and self
            // if the original element selector is the click's target or a parent of the target
            // we have not clicked outside the box
            if (this == elm) {
                clickedOutside = false; // found
                log("ClickedOutside = false. Returning..")
                return false; // stop searching
            }
        });
        if (clickedOutside) {
            log("ClickedOutside = true. executing " + doEvent);
            eval(doEvent);
            //$(elm).fadeOut("fast");
            if (bindOnce) {
                // unbind this listener, we're done with it
                $(document).unbind('click', arguments.callee);
            }
        }
    });
}

function setupDealerSearchFormSubmitEvent(searchBlockId, baseURL) {
    if (getPoolName() == 'usa') return;
    // bind form submit event
    $("#porscheCenterSearchForm").submit(function() {
        log("Porsche Center search form submitted");
        var searchKeywords = $("#" + searchBlockId + " .searchfield").val();
        log("searching for: " + searchKeywords);
        openDealerLocator(null, 980, 725, 'dealer', baseURL + "?geoip=true&searchKey=" + encodeURIComponent(searchKeywords));
        PCSearchClickedOutsideEvents();
        return false;
    });
}

function getPoolLangUrl(pool, lang) {
    return (lang != 'none') ? pool + '/' + lang : pool;
}

// dealer search box on the homepage
function initPCNADealerSearchbox() {
    log("initPCNADealerSearchbox() entered.");
    var baseURL = "/all/dealer2/" + getPoolLangUrl(CURRENTPOOL, CURRENTLANGUAGE) + "/";
    var pageClassName = $(".page").length > 0 ? $(".page")[0].className : '';
    var isModelPage = containsStr(pageClassName, "PAGEmodels");
    var isGalleryPage = containsStr(pageClassName, "PAGEgallery");
    var isFlashPage = ($("#page #introImage_flash").length > 0);

    log('isGalleryPage: ' + isGalleryPage);
    log('isFlashPage: ' + isFlashPage);
    if (isModelPage)
    {
        if (!isGalleryPage && !isFlashPage) {
            //$(".page #searchsitePage, .page #searchsitePageShadow").css('display', display);
            $(".page #searchsitePage, .page #searchsitePageShadow").show();
        }
    }
    
    var searchBlockId;
    var searchBlockShadowId;

    if (CURRENTPOOL == 'usa') {
        searchBlockId = "dealersearchHome";
        searchBlockShadowId = "dealersearchHomeShadow";
        if (!g_is_home) {
            searchBlockId = "searchsitePage";
            searchBlockShadowId = "searchsitePageShadow";
        }
    }
    else {
        searchBlockId = "porscheCenterSearchBox";
        searchBlockShadowId = "dealersearchHomeShadow";
    }

    setupDealerSearchFormSubmitEvent(searchBlockId, baseURL);


    //var searchFieldBGColor = $("#dealersearchHome .searchfield").css("background-color"); // initial background color
    var searchFieldBGColor = $("#" + searchBlockId + " .searchfield").css("background-color"); // initial background color
    $("#" + searchBlockId + " .searchfield").click(function() {
        //log("haendler suchbox geklickt!");

    }).focus(function() {
        //log("#" + searchBlockId + " .searchfield gotFocus");
        $(this).css("background-color", "#fff");
        // empty default value
        if (this.value == this.defaultValue) {
            this.value = "";
        }
        // initialize search results container
        var searchResultContainer = $("#porscheCenterSearchResults");


        if (searchResultContainer.length == 0) {
            //log("First time call. setting up searchResultContainer");
            // layout setup
            //$("#search").append("<div class=\"porscheCenterSearchBG\"><div id=\"porscheCenterSearchResults\"><ul></ul></div></div>");
            $("<div class=\"porscheCenterSearchBG\"><div id=\"porscheCenterSearchResults\"></div></div>").insertAfter("#" + searchBlockShadowId); // <ul></ul>


            // intro text
            var titleImgHtml = "<img src=\"/" + CURRENTPOOL + "/ImageMachines/LinkBlockTitle.ashx/rendered.gif?text=" + encodeURIComponent($("input[name='porscheCentersNearYou']").val()) + "\" />";
            //log("titleImgHtml: " + titleImgHtml);
            $("<p></p>").html(titleImgHtml).prependTo("#porscheCenterSearchResults");

            preInitPorscheCenterSearch(searchBlockShadowId);

            $(".porscheCenterSearchBG").show();

            //load and display results
            var searchUrl = "";
            searchUrl += "/all/dealer2/search.aspx?siteid=" + CURRENTPOOL + "&market=" + CURRENTPOOL + "&language=" + CURRENTLANGUAGE + "&mode=nearestbyip&locationtype=centre&maxresults=3";

            $("body").css("cursor", "progress");
            $.ajax({
                url: searchUrl,
                dataType: "xml",
                success: function(data) {
                    // process resulting html here
                    var dealersFound = false;
                    $(data).find('#dealerNonHTML dealer').each(function() {
                        // raise flag "at least one dealer found!"
                        if (!dealersFound) dealersFound = true;
                        // Porsche center URL (Google Porsche Center Search)
                        var pcUrl = "";
                        if (CURRENTPOOL == "usa") {
                            pcUrl = $(this).find('dealerlink').text();
                        }
                        else {
                            pcUrl = "/all/dealer2/" + getPoolLangUrl(CURRENTPOOL, CURRENTLANGUAGE) + "/location.aspx?locationId=" + $(this).attr("nr") + "&locationtype=centre";
                        }
                        // build Porsche center link
                        var link = $("<a href=\"" + pcUrl + "\" class=\"porscheCenterLink\">" + $(this).find('name').text() + "</a>");
                        // wrap link in <li> and append the whole thing to the <ul>.
                        if ($('#porscheCenterSearchResults ul').length == 0) {
                            $("<ul></ul>").appendTo('#porscheCenterSearchResults');
                        }
                        $("<li></li>").append(link).appendTo('#porscheCenterSearchResults ul');
                        // setup porsche center link click function.
                        // (yes, we can refer to an object after inserting it into the dom)
                        $(link).click(function() {
                            // Porsche center link Click events 
                            try {
                                //click_track('Klick-Startseite/Suchformular/H�ndlersuche');
                                click_track('Klick-Startseite/Search/Dealer');
                            }
                            catch (e) {
                                log("FEHLER: WIREDMINDS tracking gescheitert. Auf DEV-Server kann man diesen Fehler ignorieren.");
                            }


                            var winOptions = false;
                            if (CURRENTPOOL == 'usa') {
                                winOptions = "left=40,top=40,dependent=yes,location=yes,resizable=yes,scrollbars=yes,status=yes,toolbar=yes,menubar=yes,directories=yes,personalbar=yes";
                            }
                            log("winOptions: " + winOptions);
                            openDealerLocator(null, 980, 725, 'dealer', pcUrl, winOptions);
                            return false; // we don't want to open the link in the current window.
                        });
                    }); //.each
                    if (!dealersFound) {
                        // We could not find any dealers,
                        // display an appropriate message
                        log("No centers found");
                        porscheCenterSearchHandleError('noCentersFound', $("input[name='noCentersFound']").val(), "NO PORSCHE CENTERS FOUND.");
                    }
                    else {
                        //$(".porscheCenterSearchBG").css("height", "60px"); // STV: Hompage IE 7 
                    }
                }, //success: function(data)
                error: function(request, error) {
                    log("HTTP-FEHLER: Typ '" + error + "'");

                    porscheCenterSearchHandleError('errorInProcess', $("input[name='errorInProcess']").val(), "HTTP-ERROR.", searchBlockShadowId);
                },
                complete: function() {
                    $("body").css("cursor", "default");
                    preInitPorscheCenterSearch(searchBlockShadowId);
                    initPorscheCenterResultsDisplay(searchBlockId);
                }
            }); // $.ajax

        }
        else {
            log("Search container exists");
            preInitPorscheCenterSearch(searchBlockShadowId);

            initPorscheCenterResultsDisplay(searchBlockId);
        }

    }).blur(function() {
        // reset default value if lost focus
        $(this).css("background-color", searchFieldBGColor);
        if (!this.value.length) {
            this.value = this.defaultValue;
        }
        log("haendler suchbox: blur");

    });    
    return false;
}
 /*END DEALER SEARCH */
// Wiredminds tracking
function click_track(value) { 
// trackt einen Besucher Klick
    if (g_is_home) {   // wm_cd1= value;
        try {
            wiredminds.count(value);
        }
        catch (e) {
            log("wiredminds tracking gescheitert! Value: " + value);
        }

    }
}

// Quicktime detection script
function isQTInstalled() {
    var qtInstalled = false;
    qtObj = false;
    if (navigator.plugins && navigator.plugins.length) {
        for (var i = 0; i < navigator.plugins.length; i++) {
            var plugin = navigator.plugins[i];
            if (plugin.name.indexOf("QuickTime") > -1) {
                qtInstalled = true;
            }
        }
    } else {
        execScript('on error resume next: qtObj = IsObject(CreateObject("QuickTimeCheckObject.QuickTimeCheck.1"))', 'VBScript');
        qtInstalled = qtObj;
    }
    return qtInstalled;
}


// Console logging for firebug.
// keep it silent on other browsers.
function log(str) {
    try {
        console.log(str);
    }
    catch (e) {
    }
}
// Class Hashtable
function Hashtable() {
    this.length = 0;
    this.items = new Array();
    for (var i = 0; i < arguments.length; i += 2) {
        if (typeof (arguments[i + 1]) != 'undefined') {
            this.items[arguments[i]] = arguments[i + 1];
            this.length++;
        }
    }

    this.removeItem = function(in_key) {
        var tmp_value;
        if (typeof (this.items[in_key]) != 'undefined') {
            this.length--;
            var tmp_value = this.items[in_key];
            delete this.items[in_key];
        }

        return tmp_value;
    }

    this.getItem = function(in_key) {
        return this.items[in_key];
    }

    this.setItem = function(in_key, in_value) {
        if (typeof (in_value) != 'undefined') {
            if (typeof (this.items[in_key]) == 'undefined') {
                this.length++;
            }

            this.items[in_key] = in_value;
        }

        return in_value;
    }

    this.hasItem = function(in_key) {
        return typeof (this.items[in_key]) != 'undefined';
    }
}
// Ziel URL f�r button.swf
function getButtonTarget() {
    return buttonTarget;
}

function getCachePrevent() {
    return "rand=" + escape(Math.round(Math.random() * 10000));
}

function conditionLinkFix(ids) {
    var conditionMatched = window.location.href.match(/_.+_/ig);
    var elmIds = ids.split(/[\s,;]+/);
    $.each(elmIds, function() {
        if (!conditionMatched) $("#" + this).css({ 'left': '-10000px', 'visibility': 'hidden' });
    });
}
