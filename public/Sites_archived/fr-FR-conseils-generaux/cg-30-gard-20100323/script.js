var recherche= /.*presSsMenu.*/;
var recherche2= /.*hover.*/;
var recherche3= /.*courant.*/;


String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

function swap_menu(element) {

    if (element.tagName.toLowerCase() == 'ul') {
        var listChilds = element.childNodes;
        for (var i = 0; i < listChilds.length; i++) {
            if ((listChilds.item(i).tagName == undefined) ||
                (listChilds.item(i).tagName == null) ||
                (listChilds.item(i).tagName.toLowerCase() != 'li'))
                continue;
            var entryChilds = listChilds.item(i).childNodes;
            var current_is_in = listChilds.item(i).className.indexOf('hover');
            for (var j = 0; j < entryChilds.length; j++) {
                if ((entryChilds.item(j).tagName == undefined) ||
                    (entryChilds.item(j).tagName == null) ||
                    (entryChilds.item(j).tagName.toLowerCase() != 'ul') ||
                    (current_is_in > -1)){
                      continue;
				}
                entryChilds.item(j).style.display = 'none';	
            }
        }
    }else if (element.tagName.toLowerCase() == 'li') {
    	var color = new Array('#FDC600','#45A12B','#007CBF','#E75113','#E2001A');
        var listChilds = element.parentNode.childNodes;
        var colorNumber = 0;
        var stopInc = false;
        for (var i = 0; i < listChilds.length; i++) {
            if ((listChilds.item(i).tagName == undefined) ||
                (listChilds.item(i).tagName == null) ||
                (listChilds.item(i).tagName.toLowerCase() != 'li'))
                continue;
            if (listChilds.item(i)!=element && !stopInc) colorNumber++;
            else if (listChilds.item(i)==element) stopInc=true;
            listChilds.item(i).className = listChilds.item(i).className.replace('hover', '').trim();
            if (listChilds.item(i).className.indexOf('courant')==-1)	listChilds.item(i).style.backgroundColor='#9C9D9F';
			
        }
        swap_menu(element.parentNode);
        element.className = (element.className + ' hover').trim();
		
		if(recherche.test(element.className) && recherche2.test(element.className))
			document.getElementById('ariane').style.marginTop = "3em";
		else
			document.getElementById('ariane').style.marginTop = "1em";
        element.style.backgroundColor=color[colorNumber];
        var entryChilds = element.childNodes;
        element.parentNode.parentNode.blocked = true;
        for (var j = 0; j < entryChilds.length; j++) {
            if ((entryChilds.item(j).tagName == undefined) ||
                (entryChilds.item(j).tagName == null) ||
                (entryChilds.item(j).tagName.toLowerCase() != 'ul'))
                continue;
            entryChilds.item(j).style.display = 'block';
            if (hasli(entryChilds.item(j))) entryChilds.item(j).style.backgroundColor=color[colorNumber];
            //else entryChilds.item(j).style.backgroundColor='white';
            if (element.blocked)
                element.blocked = false;
            else
                swap_menu(entryChilds.item(j));
        }
    }
}


function hasli(element) {
  var listChilds = element.childNodes;
  for (var i = 0; i < listChilds.length; i++) {
    if ((listChilds.item(i).tagName != undefined) &&
       (listChilds.item(i).tagName != null) &&
       (listChilds.item(i).tagName.toLowerCase() == 'li'))
         return true;
  }
  return false;
}


function enrouge(elem)
{
  var listTag=elem.parentNode.parentNode.getElementsByTagName('span');
  listTag.item(0).setAttribute("class","li_onfocus");
}

function engris(elem)
{
  var listTag=elem.parentNode.parentNode.getElementsByTagName('span');
  listTag.item(0).setAttribute("class","li_onblur");
}

function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (oldonload) {
        oldonload();
      }
      func();
    }
  }
}

function fonction()
{
	var listChilds = document.getElementById('menuHaut').childNodes;
	var list;
	for (var i = 0; i < listChilds.length; i++) {
		if(listChilds.item(i).className == "menu"){
			list = listChilds.item(i).childNodes;
			for (var i = 0; i < list.length; i++) {
				if(list.item(i).className == "courant presSsMenu")
					document.getElementById('ariane').style.marginTop = "3em";
			}
		}
	}
}
function changeColor(element)
{
	var color = new Array('#FDC600','#45A12B','#007CBF','#E75113','#E2001A');
	var listChilds = element.parentNode.childNodes;
	var stopInc = false;
	var colorNumber = 0;
	for (var i = 0; i < listChilds.length; i++) {
		if (listChilds.item(i)!=element && !stopInc)
			colorNumber++;
		else if (listChilds.item(i)==element)
			stopInc=true;
		if (listChilds.item(i).className.indexOf('courant')==-1)	listChilds.item(i).style.backgroundColor='#9C9D9F';
	}
	element.style.backgroundColor=color[colorNumber];
	
}
function clearColor(element)
{
	var listChilds = element.parentNode.childNodes;
	for (var i = 0; i < listChilds.length; i++) {
		if (listChilds.item(i).className.indexOf('courant')==-1)	listChilds.item(i).style.backgroundColor='#9C9D9F';
	}
}
//addLoadEvent(fonction);
jQuery(document).ready(fonction);


function conserneVisite(element)
{
	var value = element.options[element.selectedIndex].firstChild.nodeValue;
	if (value == "Administration") document.location.href="http://www.gard.fr/fr/le-conseil-general/les-services-du-cg.html";
	if (value == "Aide sociale") document.location.href="http://www.gard.fr/fr/nos-actions/solidarite-sante/lutte-contre-les-exclusions.html";
	if (value == "Adoption") document.location.href="http://www.gard.fr/fr/nos-actions/solidarite-sante/la-petite-enfance-lenfance-et-la-famille/adopter.html";
	if (value == "Agenda21");
	if (value == "APA") document.location.href="http://www.gard.fr/fr/nos-actions/solidarite-sante/personnes-agees.html";
	if (value == "Archives d�partementales") document.location.href="http://www.gard.fr/fr/nos-actions/temps-libre.html";
	if (value == "Assistante maternelle") document.location.href="http://www.gard.fr/fr/nos-actions/solidarite-sante/lieux-daccueil/les-relais-assistantes-maternelles-ram.html";
	if (value == "Associations") document.location.href="http://www.gard.fr/fr/nos-actions/temps-libre.html";
	if (value == "Biblioth�que d�partementale") document.location.href="http://www.gard.fr/fr/nos-actions/temps-libre.html";
	if (value == "Bourses") document.location.href="http://www.gard.fr/fr/nos-actions/education/soutien-a-lenseignement.html";
	if (value == "Budget") document.location.href="http://www.gard.fr/fr/le-conseil-general/le-budget.html";
	if (value == "Centre m�dico-sociaux") document.location.href="http://www.gard.fr/fr/nos-actions/deplacements/le-transport-scolaire-gratuit.html";
	if (value == "Coll�ges") document.location.href="http://www.gard.fr/fr/nos-actions/education/colleges.html";
	if (value == "Cr�ation d�entreprise") document.location.href="http://www.gard.fr/fr/nos-actions/economie/creer-une-entreprise.html";
	if (value == "Culture") document.location.href="http://www.gard.fr/fr/nos-actions/temps-libre.html";
	if (value == "D�lib�rations");
	if (value == "Demande de subventions") document.location.href="http://www.gard.fr/fr/nos-actions/temps-libre.html";
	if (value == "Laboratoire d�analyse du GARD") document.location.href="http://www.gard.fr/fr/nos-actions/securite/veille-sanitaire.html";
	if (value == "Edgard") document.location.href="http://www.gard.fr/fr/nos-actions/deplacements/le-nouveau-reseau-edgard.html";
	if (value == "Elus") document.location.href="http://www.gard.fr/fr/le-conseil-general/vos-elus.html";
	if (value == "Environnement") document.location.href="http://www.gard.fr/fr/nos-actions/environnement.html";
	if (value == "Espaces naturels sensibles") document.location.href="http://www.gard.fr/fr/nos-actions/environnement/mieux-connaitre-les-espaces-naturels-sensibles.html";
	if (value == "Gard magazine") document.location.href="http://www.wobook.com/WBt56TG4vP3E"
	if (value == "Garde d�enfants") document.location.href="http://www.gard.fr/fr/nos-actions/solidarite-sante/la-petite-enfance-lenfance-et-la-famille/choisir-un-mode-de-garde-pour-bebe.html";
	if (value == "Handicap") document.location.href="http://www.gard.fr/fr/nos-actions/solidarite-sante/personnes-handicapees.html";
	if (value == "Inondations") document.location.href="http://www.gard.fr/fr/nos-actions/securite/sensibilisation-inondation.html";
	if (value == "March�s publics") document.location.href="http://www.gard.fr/fr/le-gard-online/marches-publics.html";
	if (value == "Maternit�") document.location.href="http://www.gard.fr/fr/nos-actions/solidarite-sante/la-petite-enfance-lenfance-et-la-famille/faire-suivre-sa-grossesse.html";
	if (value == "Maltraitance") document.location.href="http://www.gard.fr/fr/nos-actions/solidarite-sante/la-petite-enfance-lenfance-et-la-famille/protection-de-lenfance.html";
	if (value == "MDPH") document.location.href="http://www.gard.fr/fr/nos-actions/solidarite-sante/lieux-daccueil/la-maison-departementale-des-personnes-handicapees-mdph.html";
	if (value == "Mus�e") document.location.href="http://www.gard.fr/fr/nos-actions/temps-libre/musees-du-gard.html";
	if (value == "Risques incendie") document.location.href="http://www.gard.fr/fr/nos-actions/securite/prevention-incendie.html";
	if (value == "Routes") document.location.href="http://www.gard.fr/fr/nos-actions/deplacements.html";
	if (value == "Sant�-Jeune") document.location.href="http://www.gard.fr/fr/nos-actions/solidarite-sante/sante-et-prevention.html";
	if (value == "Transports scolaires") document.location.href="http://www.gard.fr/fr/nos-actions/deplacements/le-transport-scolaire-gratuit.html";
}


function vousEtes(element)
{
	var value = element.options[element.selectedIndex].firstChild.nodeValue;
	if(value=="Coll�gien")document.location.href="http://www.gard.fr/fr/nos-actions/education.html";
	if(value=="Parent")document.location.href="http://www.gard.fr/fr/nos-actions/solidarite-sante/la-petite-enfance-lenfance-et-la-famille.html";
	if(value=="Personne �g�e")document.location.href="http://www.gard.fr/fr/nos-actions/solidarite-sante/personnes-agees.html";
	if(value=="Personne handicap�e")document.location.href="http://www.gard.fr/fr/nos-actions/solidarite-sante/personnes-handicapees.html";
	if(value=="Entrepreneur")document.location.href="http://www.gard.fr/fr/nos-actions/economie.html";
}