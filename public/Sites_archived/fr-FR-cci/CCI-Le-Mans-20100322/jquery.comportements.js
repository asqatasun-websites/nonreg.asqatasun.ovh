// JavaScript Document

	// Emule le focus sous IE
	function focusfix(selector, className) {
		$(selector).focus(function() {
			$(this).addClass(className);
		});
		$(selector).blur(function() {
			$(this).removeClass(className);
		});
	}

	$(document).ready(function() {
		
		// Emule le focus sous IE
		focusfix('a', 'focus');
		focusfix('input', 'focus');
		focusfix('textarea', 'focus');
		focusfix('select', 'focus');
		
		// Onglets
		$('ul.onglets').tabs({selected:0, fx:{opacity:"toggle"}});
		
		// Lightbox			   
		$('a.lightbox').lightBox();
		
		// Etend la zone cliquable � un conteneur
		$("div.vue_line").mouseover(function(){
			$(this).addClass('hover');
		});
		$("div.vue_line").mouseout(function(){
			$(this).removeClass('hover');
		});
		$("div.vue_line").click(function(){
			window.location=$(this).find("a").attr("href"); return false;
		});

		// Identifie les liens pointant vers des sites externes (ajout d'un pictogramme � droite du lien)
		$("#main a[@href^=\"http\"]").addClass("externe");
		$("a.lightbox").removeClass("externe");
		
		// Aspect des lignes (<tr>) des tableaux de donn�es au survol et altern�
		$('table.tableau_donnees tr').mouseover(function(){$(this).addClass('survol');}).mouseout(function(){$(this).removeClass('survol');});
		$("table.tableau_donnees tr:even").addClass("alterne");
		
	});