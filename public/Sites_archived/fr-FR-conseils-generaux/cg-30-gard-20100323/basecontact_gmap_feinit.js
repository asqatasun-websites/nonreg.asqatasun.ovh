window.basecontact_gmap_initializers = new Array();
function basecontact_gmap_initialize()
{
	for (var name in window.basecontact_gmap_initializers)
		window.basecontact_gmap_initializers[name]();
}
function basecontact_gmap_array2url(array, prefix, output)
{
	for (var name in array)
	{
		if ((typeof array[name] == "object") || (typeof array[name] == "array"))
			basecontact_gmap_array2url(array[name], (prefix == '') ? (name) : (prefix + '[' + name + ']'), output);
		else
			output.push(encodeURIComponent((prefix == '') ? (name) : (prefix + '[' + name + ']')) + '=' + encodeURIComponent(array[name]));
	}
}
function BaseContactGMap()
{
	this.map = null;
	this.container = null;
	this.markers = null;
	this.ajaxParams = null;
}
/**
 * Initialize the BaseContactGMap GoogleMap instance.
 */
BaseContactGMap.prototype.init = function(placeHolderId, useAjax)
{
	if (GBrowserIsCompatible()) {
		var mapContainer = document.getElementById(placeHolderId);
		if (this.width != null) mapContainer.style.width = this.width;
		if (this.height != null) mapContainer.style.height = this.height;
		this.map = new google.maps.Map2(mapContainer, { googleBarOptions: { onGenerateMarkerHtmlCallback: function (marker, html, result) { return ''; }}});
		this.map.setCenter(new google.maps.LatLng(this.latlng[0], this.latlng[1]), this.zoom);
		this.map.enableScrollWheelZoom();
		//this.map.addControl(new google.maps.LargeMapControl());
		//this.map.addControl(new google.maps.MapTypeControl());
		this.map.setUIToDefault();
		var bounds = null;
	//	var gx = new GGeoXml("http://cg-gard.ic-s.org/fileadmin/KML/export.kmz");
	//	this.map.addOverlay(gx);
		if (useAjax)
			this.fillMapAjax();
		else
			this.fillMapStatic();
	}
};
/**
 * Instantiate an XMLHttpRequest object.
 * @return A new XMLHttpRequest object.
 */
BaseContactGMap.prototype.createXmlHttp = function()
{
	var A;
	try
	{
		A = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch (e)
	{
		try
		{
			A = new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch (oc)
		{
			A = null;
		}
	}
	if (!A && typeof XMLHttpRequest != 'undefined')
	{
		A = new XMLHttpRequest();
	}
	return A;
};
/**
 * Send an XMLHTTP query to the serveur with specified parameters.
 * @params params The parameters to send in the request.
 */
BaseContactGMap.prototype.query = function(params)
{
	var x;
	x = this.createXmlHttp();
	if (!x)
		return;
	var bcgmap = this;
	var content = [ ];
	params.state = this.state;
	basecontact_gmap_array2url(params, '', content);
	content = content.join('&');
	x.open('POST', 'index.php?eID=basecontact_gmap_query', true);
	x.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	x.setRequestHeader('Content-Length', content.length);
	x.setRequestHeader('Connection', 'close');
	x.onreadystatechange = function()
	{
		if (x.readyState != 4)  {
			return;
		}
		if ((x.status != 200) || (x.responseText == ''))
		{
			bcgmap.queryError(x, params);
			return;
		}
		var result = x.responseText;
		try
		{
			result = eval('(' + result + ')');
		}
		catch (e)
		{
			result = false;
		}
		bcgmap.queryCallback(result, params);
	}
	x.send(content);
};
/**
 * Called when the ajax query has failed.
 * @params map The GoogleMap map object.
 * @params xmlhttp The XMLHttpRequest object used for the query.
 * @params params The parameters sent in the query.
 */
BaseContactGMap.prototype.queryError = function(xmlhttp, params)
{
};
/**
 * Called when the ajax query has finished and is successfull.
 * @params map The GoogleMap map object.
 * @params result The parsed result data.
 * @params params The parameters sent in the query.
 */
BaseContactGMap.prototype.queryCallback = function(result, params)
{
	var bounds = null;
	for (var name in result)
	{
		var row = result[name];
		var coords = new google.maps.LatLng(parseFloat(row.coords.lat), parseFloat(row.coords.lng));
		var content = this.getInfoWindowDescription(row);
		content = basecontact_createElements([{'tag': 'div', 'children': content}]);
		content = content[0];
		if (bounds == null)
			bounds = new google.maps.LatLngBounds(coords, coords);
		else
			bounds.extend(coords);
		var marker = this.createMarker(google.maps.Marker, row, coords);
		marker.bindInfoWindow(content);
		this.map.addOverlay(marker);
	}
	if (bounds != null)
	{
		this.map.setZoom(this.map.getBoundsZoomLevel(bounds));
		this.map.panTo(bounds.getCenter());
	}
	return bounds;
};
/**
 * Returns the description of the info window for the specified row.
 * See EXT:basecontact/res/js/bcCreateElement.js:basecontact_createElements for the
 * syntax of the object to return.
 */
BaseContactGMap.prototype.getInfoWindowDescription = function(row)
{
	return [
		{
			'tag': 'p',
			'children': [
				{
					'tag': '',
					'value': row.name
				}
			]
		}
	];
};
/**
 * Create the marker to enable user-defined style and behaviour.
 */
BaseContactGMap.prototype.createMarker = function(constructor, row, coords)
{
	return new constructor(coords);
}
/**
 * Fills the map with markers, using ajax. The XMLHttpRequest is started.
 */
BaseContactGMap.prototype.fillMapAjax = function()
{
	this.query(this.ajaxParams);
}
/**
 * Fills the map with markers, the classic way. Use list of markers written in the body of the page.
 */
BaseContactGMap.prototype.fillMapStatic = function()
{
	var bounds = null;
	for (var name in this.markers)
	{
		var row = this.markers[name];
		var coords = new google.maps.LatLng(parseFloat(row.coords.lat), parseFloat(row.coords.lng));
		var content = this.getInfoWindowDescription(row);
		content = basecontact_createElements([{'tag': 'div', 'children': content}]);
		content = content[0];
		if (bounds == null)
			bounds = new google.maps.LatLngBounds(coords, coords);
		else
			bounds.extend(coords);
		var marker = this.createMarker(google.maps.Marker, row, coords);
		marker.bindInfoWindow(content);
		this.map.addOverlay(marker);
	}
	if (bounds != null)
	{
		this.map.setZoom(this.map.getBoundsZoomLevel(bounds));
		this.map.panTo(bounds.getCenter());
	}
}
