/******************************************************************************/
/*************** script affichage/masquage des menus dynamiques ***************/
/******************************************************************************/
/***************        Yoann Le Crom - Septembre 2006         ****************/
/***************                  version 4.1                  ****************/
/******************************************************************************/
/***************    ce script utilise une image "fleche.png"   ****************/
/***************  qui se situe dans le sous-r�pertoire images  ****************/
/******************************************************************************/

function init(){
   var menuRacine = document.getElementById("menu");
   chercherSousMenus(menuRacine,true);
}

function initMenuGauche(){
   var menuRacine = document.getElementById("menu_gauche");
   chercherSousMenus(menuRacine,true);
}

function chercherSousMenus(menu,isRacine) {
   for(var i=0;i<menu.childNodes.length;i++){
      if(menu.childNodes[i].nodeName == "LI"){
         for(var j=0;j<menu.childNodes[i].childNodes.length;j++){
            if(menu.childNodes[i].childNodes[j].nodeName == "A"){
               var itemMenu = menu.childNodes[i].childNodes[j];
               masquage(menu, itemMenu);
            }
            if(menu.childNodes[i].childNodes[j].nodeName == "UL"){
               if(!isRacine) {
                  var image = document.createElement("img");
                  image.setAttribute("src","images/fleche.png");
                  image.setAttribute("alt","&gt;&gt;");
                  image.setAttribute("style","float:right;margin:3px;");//moz
                  image.style.styleFloat = "right"; //ie
                  image.style.margin = "3px"; //ie
                  itemMenu.insertBefore(image,itemMenu.firstChild);
               }
               attacherMenu(itemMenu,menu.childNodes[i].childNodes[j], menu);
               chercherSousMenus(menu.childNodes[i].childNodes[j],false);
            }
         }
      }
   }
}

function attacherMenu(menu, sousMenu, menuParent) {
   menu.onmouseover = function(){
      //pour g�rer le masquage de tous les menus
   	  if((sousMenu.className == "sousSousMenuOff")||(sousMenu.className == "sousSousMenu")){
	      masquerSousMenus(menuParent);
	  }else{
	  	  masquerMenus();
	  }
      if(sousMenu.className == "sousSousMenuOff" ){
      	sousMenu.className = "sousSousMenu";  //ie
      }
      sousMenu.style.display="block";
   }
   //menu.onclick = function(){blur()}; //cas firefox encadrement lien au clic, mais fais planter IE
}


function masquage(menuParent, menu) {
   menu.onmouseover = function(){
//      masquerSousMenus(menuParent);
   }
}

function masquerSousMenus(menu) {
   for(var i=0;i<menu.childNodes.length;i++){
      if(menu.childNodes[i].nodeName == "LI"){
         for(var j=0;j<menu.childNodes[i].childNodes.length;j++){
            if(menu.childNodes[i].childNodes[j].nodeName == "A"){
               var itemMenu = menu.childNodes[i].childNodes[j];
            }
            if(menu.childNodes[i].childNodes[j].nodeName == "UL"){
               if(menu.childNodes[i].childNodes[j].className == "sousSousMenu") menu.childNodes[i].childNodes[j].className ="sousSousMenuOff"; //ie
               menu.childNodes[i].childNodes[j].style.display = "none";
               masquerSousMenus(menu.childNodes[i].childNodes[j]);
            }
         }
      }
   }
}

function masquerMenus() {
   var menuRacine = document.getElementById("menu");
   if(menuRacine != null){
	   masquerSousMenus(menuRacine);
   }
   var menuGauche = document.getElementById("menu_gauche");
   if(menuGauche != null){
		masquerSousMenus(menuGauche);
   }
}
