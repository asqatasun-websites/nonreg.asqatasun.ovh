function addFavorite()
{
	var url = window.location;
	var titre = document.title;	
	if (window.sidebar)
		window.sidebar.addPanel(titre,url,"");
	else
		window.external.AddFavorite(url,titre);
	return false;
}

function httpGet(key_str) {
	if(window.location.search) {
		var query = window.location.search.substr(1);
		var pairs = query.split("&");
		for(var i = 0; i < pairs.length; i++) {
			var pair = pairs[i].split("=");
			if(unescape(pair[0]) == key_str)
			return unescape(pair[1]);
		}
	}
}

// retourne l'index de l'horaire passé en paramètre 
function getDelta(ts){
	for(var i=1; i< arrayDif.length; i++){
		if(arrayDif[i] > ts){
			return i-1;
		}
	}
	return 0;
}

function playTeaserFlash(index){
	document.getElementById('teaser_home').setIndex(index);
}

var div_courant=1;	
	  	                            
function switch_slides_avant(max) {
    	Effect.Fade('image' + div_courant);
    	Effect.Fade('txt' + div_courant);
    	if (div_courant == 1)
    	{div_courant=max;}
    	else
    	{div_courant= div_courant - 1;}
        setTimeout("Effect.Appear('image" + div_courant + "');", 850);
        setTimeout("Effect.Appear('txt" + div_courant + "');", 850);
}

function switch_slides_apres(max) {
    	Effect.Fade('image' + div_courant);
    	Effect.Fade('txt' + div_courant);
    	if (div_courant == max)
    	{div_courant=1;}
    	else
    	{div_courant= div_courant + 1;}
        setTimeout("Effect.Appear('image" + div_courant + "');", 850);
        setTimeout("Effect.Appear('txt" + div_courant + "');", 850);   
}

function isDate(date)
{
		// On sépare la date en 3 variables pour vérification, parseInt() converti du texte en entier
		var tabDate = date.split('/');
		var j = parseInt(tabDate[0],"10");
		var m = parseInt(tabDate[1],"10");
		var a = parseInt(tabDate[2],"10");
		
		if(isNaN(a)||isNaN(m)||isNaN(j)){
			return false;
		}
		else{
		    // Définition du dernier jour de février
		    // Année bissextile si annnée divisible par 4 et que ce n'est pas un siècle, ou bien si divisible par 400
		    if ((a%4 == 0 && a%100 !=0) || (a%400 == 0)) {var fev = 29;}
		    else {var fev = 28;}
		    
		
		    // Nombre de jours pour chaque mois
		    if(fev==28){
		    	var nbJours = new Array(31,28,31,30,31,30,31,31,30,31,30,31);
		    }
		    else if(fev==29){
		    	var nbJours = new Array(31,29,31,30,31,30,31,31,30,31,30,31);
		    }
		    
		    // Enfin, retourne vrai si le jour est bien entre 1 et le bon nombre de jours, idem pour les mois, sinon retourn faux
		    return ( (m >= 1) &&(m <=12) && (j>= 1) && (j <= nbJours[m-1]) );
		}
}

function verifMail(emailString)
{
		pass = false;
		
		for(var j=1;j<(emailString.length);j++){
			if(emailString.charAt(j)=='@'){
				if(j<(emailString.length-4)){
					for(var k=j;k<(emailString.length-2);k++){
						if(emailString.charAt(k)=='.') pass=true;
					}
				}
			}
		}
		return pass;
}
	
function checknumber(atester)
{
		var anum=/(^\d+$)|(^\d+\.\d+$)/;
		
		if (anum.test(atester)){
			pass=true;
		}
		else{
			pass=false;
		}
		
		return pass;
}
		
function isMineur()
{
	//Date saisie
	var sD = document.getElementById('jour').value;
	var sM = document.getElementById('mois').value;
	var sY = document.getElementById('annee').value;
		
	//Date du jour
	var d = new Date();
	var curr_dayOfMonth = d.getDate();
	var curr_month = d.getMonth()+1;
	var curr_year = d.getFullYear();
		
	if(curr_year-sY>18 || (curr_year-sY==18 && curr_month>=sM && curr_dayOfMonth>=sD)){
		return false;
	}
	else{
		return true;
	}
}

function checkPlanPartenaires()
{
	var checkIsMineur = isMineur();
	var verifThisEmail = verifMail(document.getElementById('email').value);
	
	if(checkIsMineur&&document.getElementById('offres_tiji_on').checked == true){
		
		if(document.getElementById('planPartenaires').style.display == 'none')
		{
			document.getElementById('planPartenaires').style.display = 'block';
		}
	}
	else{
		
		if(document.getElementById('planPartenaires').style.display == 'block')
		{
			document.getElementById('planPartenaires').style.display = 'none';
		}
	}
}


function checkPlanPartenairesBis()
{
	var checkIsMineur = isMineur();
	var verifThisEmail = verifMail(document.getElementById('email').value);
	
	if(checkIsMineur&&document.getElementById('offres_tiji_on').checked == true){
		
		if(document.getElementById('planPartenaires').style.display == 'none')
		{
			document.getElementById('planPartenaires').style.display = 'block';
		}
	}
	else{
		
		if(document.getElementById('planPartenaires').style.display == 'block')
		{
			document.getElementById('planPartenaires').style.display = 'none';
		}
	}
}


function checkFields(){
		var err = '';
		var jour = document.getElementById('jour').value;
		var mois = document.getElementById('mois').value;
		var annee = document.getElementById('annee').value;
		
		if(jour!=''&&mois!=''&&annee!=''){
			var dateNaissance = jour+'/'+mois+'/'+annee;
		}
		else{
			var dateNaissance = '';
		}

		if((document.getElementById('mypseudo').value=='')||(document.getElementById('mypseudo').value==' ')){
			err += 'Renseigne ton pseudo !\n';
		}
		if(document.getElementById('mypseudo').value!=''&&document.getElementById('mypseudo').value.length<=4){
			err += 'Ton pseudo doit contenir au moins 5 caracteres !\n';
		}
		if(document.getElementById('password').value==''){
			err += 'Renseigne ton mot de passe !\n';
		}
		if(document.getElementById('confirmmotdepasse').value==''){
			err += 'Confirme ton mot de passe !\n';
		}
		if(document.getElementById('password').value!=document.getElementById('confirmmotdepasse').value){
			err += 'Le mot de passe et la confirmation ne sont pas identiques !\n';
		}
		if(document.getElementById('firstname').value==''){
			err += 'Renseigne ton prenom !\n';
		}
		if(document.getElementById('firstname').value!=''&&document.getElementById('firstname').value.length<=2){
			err += 'Ton prenom doit contenir au moins 3 caracteres !\n';
		}
		if(document.getElementById('last_name').value==''){
			err += 'Renseigne ton nom !\n';
		}
		if(dateNaissance==''){
			err += 'Renseigne ta date de naissance !\n';
		}
		if(dateNaissance!=''&&isDate(dateNaissance)==false){
			err += 'Le format de ta date de naissance est incorrect !\n';
		}
		if(document.getElementById('email').value==''){
			err += 'Renseigne ton email !\n';
		}
		if(document.getElementById('email').value!=''&&verifMail(document.getElementById('email').value)==false){
			err += 'Le format de ton email est incorrect !\n';
		}
		if(document.getElementById('code_postal').value==''){
			err += 'Renseigne ton code postal !\n';
		}
		if(document.getElementById('code_postal').value!=''&&checknumber(parseInt(document.getElementById('code_postal').value))==false){
			err += 'Le format de ton code postal est incorrect !\n';
		}
		if(document.getElementById('email_parent').value=='' && isMineur() && document.getElementById('offres_tiji_on').checked == true && document.getElementById('email').value!=''){
			if(document.getElementById('planPartenaires').style.display == 'none')
			{
				document.getElementById('maskPlanPartenaires').style.display = 'none';
				document.getElementById('planPartenaires').style.display = 'block';
			}
			
			err += 'Renseigne l\'email de tes parents pour recevoir les offres partenaires !\n';
		}
		else if(document.getElementById('email_parent').value!='' && isMineur() && verifMail(document.getElementById('email_parent').value)==false){
			err += 'Le format de l\'email de tes parents est incorrect !\n';
		}
		
		if (!document.getElementById('newsletter_gulli_on').checked && !document.getElementById('newsletter_gulli_off').checked)
		{
			err += 'Une reponse pour chaque newsletters !\n';
		}
		else if (!document.getElementById('newsletter_tiji_on').checked && !document.getElementById('newsletter_tiji_off').checked)
		{
			err += 'Une reponse pour chaque newsletters !\n';
		}
		else if (!document.getElementById('newsletter_canalj_on').checked && !document.getElementById('newsletter_canalj_off').checked)
		{
			err += 'Une reponse pour chaque newsletters !\n';
		}
		else if (!document.getElementById('offres_tiji_on').checked && !document.getElementById('offres_tiji_off').checked)
		{
			err += 'Une reponse pour chaque newsletters !\n';
		}		

		if(err=='')
		{
			return true;
		}
		else{
			window.alert(err);
			return false;
		}
}

// pop  up Radio musiline

var winRadio;

function popUpRadio(){
	if(typeof winRadio != 'undefined'){
	   try{
	      for(var i in winRadio){}//ffx
	      winRadio.focus();
	    }
	    catch(e)
	    {
	      lancePopUpRadio();
	    }
	}
	else{
	  lancePopUpRadio();
	}
}

function lancePopUpRadio(){
	winRadio = window.open(sHost+'/modalbox/radio','playerradio','width=923,height=353,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,copyhistory=no,resizable=no');
	winRadio.focus();
}


/* Pour Jeux Concours */

function verifyFormatEmail(mail)
{
	if( mail.value.indexOf('@',0) == (-1) || mail.value.indexOf('.',0) == (-1) || mail.value.length<4  )
	{
		return false;
	}
	else
	{
		return true;
	}
}

function verifyParrainage( nom,prenom,mail,m1,m2,m3)
{	
	
	if(nom.value == '' ||prenom.value == '' || mail.value == '' )
	{
		alert('Tu dois mettre ton prénom, ton nom et ton email');
		return false;
	}
	if (! verifyFormatEmail(mail))
	{
		alert('Ton email n\'est pas valide');
		return false;
	}
	
	if(m1.value == '' && m2.value == '' && m3.value == '' )
	{
		alert('Tu dois parrainer au moins un copain');
		return false;
	}
	
	if(m1.value != '' && (! verifyFormatEmail(m1)) )
	{
		alert('Le premier mail n\'est pas valide');
		return false;
	}
	if(m2.value != '' && (! verifyFormatEmail(m2))  )
	{
		alert('Le deuxieme mail n\'est pas valide');
		return false;
	}
	if(m3.value != '' && (! verifyFormatEmail(m3))  )
	{
		alert('Le troisième mail n\'est pas valide');
		return false;
	}
	document.getElementById('parrainage').submit();
}
function verfifyAuthentification( loginUrl, formName )
{
	if(GetCookie("user_logged") == "oui"){
			document.getElementById(formName).submit();
	}else{		
		baseIdForm = formName;
		callModalBox(loginUrl, {title: 'Login', height: 800, width: 950, overlayOpacity: 0.75});
	}
}



function chargerPhotoSuivante(imgcourante,nbimage,sens)
{
	if (imgcourante == 0 && sens ==0)
	{
		imgsuivante = nbimage - 1;
		Effect.Fade($('image_'+imgcourante),
					{duration:0.15,
					afterFinish: function() {
						Effect.Appear($('image_'+imgsuivante));
						}
					});
		document.getElementById('img_courante').value = imgsuivante;
	}
	else if (imgcourante == (nbimage-1) && sens ==1)
	{
		imgsuivante = parseInt(0,"10");
		Effect.Fade($('image_'+imgcourante),
					{duration:0.15,
					afterFinish: function() {
						Effect.Appear($('image_'+imgsuivante));
						}
					});
		document.getElementById('img_courante').value = imgsuivante;
	}
	else if (sens ==1)
	{ 
		imgsuivante = parseInt(imgcourante,"10")+parseInt(1,"10");
		Effect.Fade($('image_'+imgcourante),
					{duration:0.15,
					afterFinish: function() {
						Effect.Appear($('image_'+imgsuivante));
						}
					});
		document.getElementById('img_courante').value = imgsuivante;
	}
	else
	{ 
		imgsuivante = parseInt(imgcourante,"10") - 1;
		Effect.Fade($('image_'+imgcourante),
					{duration:0.15,
					afterFinish: function() {
						Effect.Appear($('image_'+imgsuivante));
						}
					});
		document.getElementById('img_courante').value = imgsuivante;
	}
	
}

function chargerPhoto(imgcourante,imgsuivante)
{
	Effect.Fade($('image_modale_'+imgcourante),
					{duration:0.15,
					afterFinish: function() {
						Effect.Appear($('image_modale_'+imgsuivante));
						}
					});
	Effect.Fade($('txt_'+imgcourante),
					{duration:0.15,
					afterFinish: function() {
						Effect.Appear($('txt_'+imgsuivante));
						}
					});
	Effect.Fade($('fd_ecran_'+imgcourante),
					{duration:0.15,
					afterFinish: function() {
						Effect.Appear($('fd_ecran_'+imgsuivante));
						}
					});
	document.getElementById('img_modal').value = imgsuivante;
}

/***
 * Fonctions de gestion des Modalbox de requalification
 * 
 * rq_check_form(): Pour la gestion des formulaire
 * 
 * rq_play_again(): pour la redirection suite à l'apparitioin de la ModalBox de requalification
 *
 */
function rq_check_form(param, action){

	var radio_flag = false;
	var entry_radio = false;
	var name_radio;
	var complete_form = false;

	var tabElement = document.getElementsByClassName('qualif_check');

	for(i=0;i<tabElement.length;i++)
	{
		
		if( tabElement[i].type=='radio' )
	    {
	    
	     var obl_elt = tabElement[i].name + '_obligatoire';
	     if (document.getElementById(obl_elt).value == 1)
	      {
		      var label_radio = tabElement[i].name + '_label';
		      if ( document.getElementById('oui_'+tabElement[i].name).checked || document.getElementById('non_'+tabElement[i].name).checked)
		      	{
		     		$(label_radio).style.color='black' ;
		     	}
		     	else
		     	{
			     	$(label_radio).style.color='red' ;
			     	complete_form=true;
		     	}
		   }
	     	
	    }
		else
	    {
	    	var obl_elt = tabElement[i].name + '_obligatoire';
	    	if (document.getElementById(obl_elt).value!=null && document.getElementById(obl_elt).value == 1)
	      	{
		    	var label_input = tabElement[i].name + '_label';
		    	if(tabElement[i].value=='')
		        {			
				  	$(label_input).style.color='red' ;		
				    complete_form=true;
		        }
		      	else
			    {
			      	$(label_input).style.color='black' ;
			    }
		    }
	    }
			    
	}

	if(complete_form)
	    {
	    alert('Veuillez compléter le formulaire\n pour valider votre participation');
	    return false;
	    }
	$('questions_requalif').hide();
	$('divLoading').show();
	new Ajax.Updater( 'MB_content', '/action/requalificationJeux', {
	            asynchronous:true, 	
	            parameters:Form.serialize('modifyform') ,
	            onComplete: function()		
	            {
	            	Modalbox.hide();
	            	if (action == "jeux")
					{
						/*document.getElementById(param).submit();*/
					}
					else
					{
						window.href = param;
					}
	            
	            }
	             }); 
	return false; 
}
