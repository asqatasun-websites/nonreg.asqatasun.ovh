function bof_appeler(p_categorie, url, path, lnk) {
	//alert(p_categorie) ;
	
	var plusDeTopVentes = document.getElementById('bestsellcat0');
	plusDeTopVentes.href = path + 'Catalogue/ListeProduit.aspx?TypeAction=goTopV&idnoeud=' + p_categorie;
	
	var XHR = new XHRConnection() ;
	XHR.resetData() ;
	XHR.appendData('IdNoeud',p_categorie) ;
	XHR.sendAndLoad(url,'POST',bof_retourXML) ; //appel de la nouvelle m�thode (r�cup�ration depuis le WebService des informations d�j� format�es en HTML)
	
	// on affiche l'image d'attente pendant l'interrogation
	document.getElementById('id_dibBestSells').innerHTML = '<img id="AttenteTopVentes" src="' + path + 'img/wait_Large_Fond_blanc.gif" style="display:block">'
	
	if(lnk)highlight(lnk);
}

// fonction de retour apr�s l'interrogation du WebService
var bof_retourXML = function(obj) {
	var reponse = clean(obj.responseXML.documentElement) ;
	
	var xmlProduits = reponse.getElementsByTagName('product') ;
	var produits = new Array ;
	for (var i = 0 ; i < xmlProduits.length ; i ++) {
		produits.push(creerProduitDepuisNoeud(xmlProduits[i])) ;
	}
	
	//alert(xmlProduits.length); // nombre de produits retourn�s
		
	document.getElementById('id_dibBestSells').innerHTML = '' ;
	
	// on utilise un tableau pour optimiser la concat�nation lors de la construction du champ "id_dibBestSells"
  var tabProduits = new Array();
  
  // au d�part, le tableau contient l'image d'attente, param�tr�e en invisible
  tabProduits.push('<img id="AttenteTopVentes" src="../img/wait_Large_Fond_blanc.gif" style="display:none">');
  
  // pour chaque produit trouv�, on rajoute un pav� HTML au tableau
	for (var i = 0 ; i < produits.length ; i ++)
	   tabProduits.push(produits[i].detail);
	
	// on affecte la concat�nation des �l�ments du tableau au champ "id_dibBestSells"
	document.getElementById('id_dibBestSells').innerHTML = tabProduits.join("");
	
}













var bof_retourXML_old = function(obj) {
	var reponse = clean(obj.responseXML.documentElement) ;
	
	var xmlProduits = reponse.getElementsByTagName('product') ;
	var produits = new Array ;
	for (var i = 0 ; i < xmlProduits.length ; i ++) {
		produits.push(creerProduitDepuisNoeud_old(xmlProduits[i])) ;
	}
	
	//alert(xmlProduits.length); // nombre de produits retourn�s
	//alert('1');
	
	document.getElementById('id_dibBestSells').innerHTML = '' ;
	
	// on utilise un tableau pour optimiser la concat�nation lors de la construction du champ "id_dibBestSells"
  var tabProduits = new Array();
  
  // au d�part, le tableau contient l'image d'attente, param�tr�e en invisible
  tabProduits.push('<img id="AttenteTopVentes" src="../img/wait_Large_Fond_blanc.gif" style="display:none">');
  
  // pour chaque produit trouv�, on rajoute un pav� HTML au tableau
	for (var i = 0 ; i < produits.length ; i ++)
	   tabProduits.push(bof_newEmptyProdLine(i + 1));
	   // avant --> document.getElementById('id_dibBestSells').innerHTML += bof_newEmptyProdLine(i + 1) ;
	//alert('2');
	// on affecte la concat�nation des �l�ments du tableau au champ "id_dibBestSells"
	document.getElementById('id_dibBestSells').innerHTML = tabProduits.join("");
	//alert('3');
	
	for (var i = 0 ; i < produits.length ; i ++) {		
		//var total = parseFloat(produits[i].price) + parseFloat(produits[i].ecoPart) ;
		
		setImage('id_imgProd', 		i,  produits[i].image) ;
		setPrice('id_price', 	i,  produits[i].price ) ;
		setTypePrice('id_typePrice', 	i,  produits[i].typePrice ) ;
		setPriceBarre('id_price_barre', 	i,  produits[i].pricebarre) ;
		setInnerHtml('id_title', 	i,  produits[i].title) ;
		setInnerHtml('id_by', 		i,  produits[i].by) ;
		setAvis('id_avis', 			i, 	produits[i].note, produits[i].noteLink, produits[i].notePicto) ;
		setImage('id_picto',	i,	produits[i].picto) ;
		setHRef('id_hrefProd', 		i,  produits[i].productLink) ;
		setHRef('id_hrefPrixBarre', 		i,  produits[i].productLink) ;
		setHRef('id_hrefImgProd', 	i,  produits[i].productLink) ;
		setHRef('id_hrefBy', 		i,  produits[i].productLink) ;
	}
	//alert('4');
}

function bof_viderBestSell(p_produits) {
	for (var i = 0 ; i < p_produits.length ; i ++) {		
		setImage('id_imgProd', 		i,  '') ;
		setPrice('id_price', 	i,  '') ;
		setTypePrice('id_typePrice', 	i,  '') ;
		setPriceBarre('id_price_barre', 	i,  '') ;
		setInnerHtml('id_title', 	i,  '') ;
		setInnerHtml('id_by', 		i,  '') ;
		setAvis('id_avis', 			i, 	'') ;
		setImage('id_picto',		i,	'') ;
		setHRef('id_hrefProd', 		i,  '') ;
		setHRef('id_hrefPrixBarre', 		i,  '') ;
		setHRef('id_hrefImgProd', 	i,  '') ;
		setHRef('id_hrefBy', 		i,  '') ;
	}
}

function bof_newEmptyProdLine(p_i) {
	var str =
		'<div class="ligne_prod">'
		+ '<a id="id_hrefImgProd' + p_i + '" href="">'
		+ '<img id="id_imgProd' + p_i + '" src="" width="80" height="80" alt="" class="visu_prod" />'
		+ '</a>'
		+ '<div class="desc_prod">'
		+ '<a id="id_hrefPrixBarre' + p_i + '" href=""><span id="id_price_barre' + p_i + '" class="price_barre"></span><br/></a>'
		+ '<a id="id_hrefProd' + p_i + '" href=""><span id="id_price' + p_i + '" class="price"></span>&nbsp;<span id="id_typePrice' + p_i + '" class="typeprice"></span></a>'
		+ '<p>'
		+ '<a id="id_hrefby' + p_i + '" href=""><span id="id_title' + p_i + '" class="descriptionproduit"></span><br />'
		+ '<span id="id_by' + p_i + '" class="marque"></span></p></a>'
		+ '</div>'
		+ '<div id="id_avis' + p_i + '" class="opinion_prod">'
		+ '<p><a href="" class="redarrow">Donner son avis</a></p>'
		+ '</div>'
		+ '<img id="id_picto' + p_i + '" src="" width="55" height="49" class="picto_price" alt="" />'
		+ '</div>' ;
		
	return str ;
}

function setInnerHtml(p_idGenerique, p_index, p_valeur) {
	var idZone 	= p_idGenerique + (p_index + 1) ;
	var prc 		= document.getElementById(idZone) ;
	if (prc) prc.innerHTML = p_valeur  ;
}

function setPrice(p_idGenerique, p_index, p_valeur) {
	var idPrice 	= p_idGenerique + (p_index + 1) ;
	var zn 		= document.getElementById(idPrice) ;
	if (zn) zn.innerHTML = p_valeur + '&euro;';
}

function setTypePrice(p_idGenerique, p_index, p_valeur) {
	var idTypePrice 	= p_idGenerique + (p_index + 1) ;
	var zn 		= document.getElementById(idTypePrice) ;
	if (zn) zn.innerHTML = p_valeur ;
}

function setPriceBarre(p_idGenerique, p_index, p_valeur) {
	var idPriceBarre 	= p_idGenerique + (p_index + 1) ;
	var zn 		= document.getElementById(idPriceBarre) ;
	
	var idPriceBarreAnchor = 'id_hrefPrixBarre' + (p_index + 1) ;
	var pricebarreanchor = document.getElementById(idPriceBarreAnchor) ;
	
	if (p_valeur == null ) {
		if (pricebarreanchor) pricebarreanchor.style.display = "none";
		if (zn) zn.style.display = "none";
	} else{
		if (p_valeur == "" ) {
			if (pricebarreanchor) pricebarreanchor.style.display = "none";
			if (zn) zn.style.display = "none";
		} else {
			if (zn) zn.innerHTML = p_valeur + '&euro;';
		}
	}
}

function setImage(p_idGenerique, p_index, p_valeur) {
	var idImg 	= p_idGenerique + (p_index + 1) ;
	var img	 = document.getElementById(idImg) ;
	if (img) {
		if ( p_valeur == null )
			img.style.display = "none";
		else {
			 if ( p_valeur == "" ) 
				img.style.display = "none";
			else
				img.src = p_valeur;
		}
	}
}

function setHRef(p_idGenerique, p_index, p_valeur) {
	var idHRef 	= p_idGenerique + (p_index + 1) ;
	var ref		= document.getElementById(idHRef) ;
	if (ref) ref.href = p_valeur ;
}

function setAvis(p_idGenerique, p_index, p_note, p_lien, p_picto) {
	var idAvis 	= p_idGenerique + (p_index + 1) ;
	var div 	= document.getElementById(idAvis) ;
	
	if (div) {
		if (p_note ==  -1) {
			var contenu = '<p><a href="' + p_lien + '" class="redarrow">Donner son avis</a></p>' ;
		}
		else {
			var contenu = '<p class="aligncenter"><a href="' + p_lien + '">Avis<br />'
						  + '<img src="' + p_picto + '" width="72" height="11" alt="" />'
						  + '</a></p>' ;
		}
		div.innerHTML = contenu ;
	}
}

function highlight(obj){
	var bof_nav=document.getElementById('navLeft_BOF').getElementsByTagName('a');
	if(bof_nav && obj)
	{
		for(var i=0; i<bof_nav.length; i++){
			if(bof_nav[i]!=obj)
			{
				bof_nav[i].parentNode.className='';
			}
			else
			{
				bof_nav[i].parentNode.className='bestsells_active';
			}
		}
	}
}
