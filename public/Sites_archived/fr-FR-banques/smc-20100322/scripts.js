// JavaScript Document
<!--
function mm_jumpmenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->

var t;			
t = 0.76;
function changerTaille(modif) {
t = t + modif;
document.getElementsByTagName("body")[0].style.fontSize = t + "em";		
} 	


function fonctionJS(accTaillePlus) {
t = t + 0.05;
document.getElementsByTagName("body")[0].style.fontSize = t + "em";		
} 

function fonctionJS(accTailleMoins) {
t = t - 0.05;
document.getElementsByTagName("body")[0].style.fontSize = t + "em";		
} 

function infosSupClient (Valeur) {
    if (Valeur == "oui") {
        $('divNomAgence').show();
        $('divCodePostal').hide();
    }
    else {
        $('divNomAgence').hide();
        $('divCodePostal').show();
    }
}

function afficheMasqueCoupleBloc (coupleBlocs, Valeur) {
	if (Valeur == "Oui") {
        $(coupleBlocs[0]).show();
        $(coupleBlocs[1]).hide();
    }
    else {
        $(coupleBlocs[0]).hide();
        $(coupleBlocs[1]).show();
    }
}

function afficheMasqueBlocCheckBox (Bloc, checkBoxChecked) {
	if (checkBoxChecked == true) {
        $(Bloc).show();
    }
    else {
        $(Bloc).hide();
    }
}

function nettoyerMessagesErreur() {
	$$(".blocLabelChampErr").each( function (blocChampForm) {
		$(blocChampForm).removeClassName("blocLabelChampErr");
	});
	
	$$(".informationErreur").each( function (messageErreur) {
		
		$(messageErreur).remove();
	});
}

function emailValide (adresseTest) {
	var stringTest = new String(adresseTest);
	if (!stringTest.match('^[-_\.0-9a-zA-Z]{1,}@[-_\.0-9a-zA-Z]{1,}[\.][0-9a-zA-Z]{2,}$')) {
		return false;
	}
	else {
		return true;
	}
}

function compteValide (numCompte) {
	var stringTest = new String(numCompte);
	if (!stringTest.match(/\d{4}-\d{6}[a-z]/i)) {
		return false;
	}
	else {
		return true;
	}
}


    
var focusErreur = "";

function marquerChampFocus(leChamp) {
	if ( focusErreur == "" ) {
		focusErreur = leChamp;
	}
}

function valideFormLivretA () {
	nettoyerMessagesErreur();
	focusErreur 	= "";
	var formValide 	= true;
	var avecEnfants	= false;
	/**
	 * Coordonn�es
	 *************************************************************************************/
	
	["nom", "prenom"].each( function (Champ) {
		if ( $F(Champ) == "" ) {
			afficherErreurForm(Champ, "Veuillez renseigner ce champ");
			formValide = false;
			marquerChampFocus(Champ);
		}
	});
	
	numTelephone = $F("telephone");
	if (!new String(numTelephone).match(/\d{10}/i)) {
		afficherErreurForm("telephone", "Veuillez saisir un num�ro de t�l�phone valide : 10 chiffres sans espace");
		formValide = false;
		$("telephone").value = numTelephone;
		marquerChampFocus("telephone");
	}
	
	//--> Nom de jeune fille
	if ( $F("civilite") == "Madame" && $F("nom_jeunefille") == "" ) {
		afficherErreurForm("nom_jeunefille", "Veuillez renseigner ce champ");
		formValide = false;
		marquerChampFocus("nom_jeunefille");
	}
	
	//--> Adresse e-mail
	var adresseEmail = $F("email");
	if ( emailValide( adresseEmail ) == false ) {
		afficherErreurForm("email", "Veuillez renseigner une adresse e-mail valide");
		formValide = false;
		$("email").value = adresseEmail;
		marquerChampFocus("email");
	}
	
	/**
	 * Situation Parent
	 *************************************************************************************/
		
	if ( $("est_client_0").checked == true ) {
		var numCompte = $("deb_num_compte").value + "-" + $("fin_num_compte").value;
		if ( compteValide( numCompte ) == false ) {
			afficherErreurForm("num_compte", "Veuillez renseigner un num�ro de compte valide. (1234 123456X)");
			formValide = false;
			// $("num_compte").value = numCompte;
			marquerChampFocus("deb_num_compte");
		}
		
		if ( $F("agence") == "") {
			afficherErreurForm("agence", "Veuillez renseigner ce champ");
			formValide = false;
			marquerChampFocus("agence");
		}
		
	}
	else {
		["adresse", "code_postal", "ville", "date_naissance", "lieu_naissance"].each( function (Champ) {
			if ( $F(Champ) == "" ) {
				afficherErreurForm(Champ, "Veuillez renseigner ce champ");
				formValide = false;
				marquerChampFocus(Champ);
			}
		});
	}
	
	
	/**
	 * Les enfants
	 *************************************************************************************/
	
	[1,2,3,4].each( function (numEnfant) { 
		if ( $F("nom_" + numEnfant) != "" ) {
			avecEnfants = true;
			["prenom_" + numEnfant, "parente_" + numEnfant].each( function (Champ) {
				if ( $F(Champ) == "" ) {
					afficherErreurForm(Champ, "Veuillez renseigner ce champ");
					formValide = false;
					marquerChampFocus(Champ);
				}
			});
			
			if ($("enfant_client_" + numEnfant + "_0").checked == true) {
				var numCompte = $("deb_num_compte_" + numEnfant).value + "-" + $("fin_num_compte_" + numEnfant).value;
				if ( compteValide( numCompte ) == false ) {
					afficherErreurForm("num_compte_" + numEnfant, "Veuillez renseigner un num�ro de compte valide. (1234 123456X)");
					formValide = false;
					// $("num_compte_" + numEnfant).value = numCompte;
					marquerChampFocus("deb_num_compte_" + numEnfant);
				}
				
				if ( $F("agence_" + numEnfant) == "") {
					afficherErreurForm("agence_" + numEnfant, "Veuillez renseigner ce champ");
					formValide = false;
					marquerChampFocus("agence_" + numEnfant);
				}
			}
			else {
				["date_naissance_" + numEnfant, "lieu_naissance_" + numEnfant].each( function (Champ) {
					if ( $F(Champ) == "" ) {
						afficherErreurForm(Champ, "Veuillez renseigner ce champ");
						formValide = false;
						marquerChampFocus(Champ);
					}
				});
			}
		}
	});
	
	//--> TEST SI MOI OU ENFANT COCH�
	
	if ( $("demande_ouverture_moi_0").checked == false && $("demande_ouverture_enfant_0").checked == false ) {
		formValide = false;
		$("erreurBeneficiaire").addClassName("blocLabelChampErr");
		$("erreurBeneficiaire").innerHTML += '<p class="informationErreur">Veuillez choisir un b�n�ficiaire pour l\'ouverture du Livret A</p>';
		marquerChampFocus("demande_ouverture_moi_0");
	}
	
	if ( $("demande_ouverture_enfant_0").checked == true && avecEnfants == false ) {
		formValide = false;
		$("erreurBeneficiaire").addClassName("blocLabelChampErr");
		$("erreurBeneficiaire").innerHTML += '<p class="informationErreur">Veuillez renseigner les information d\'au moins un enfant</p>';
		marquerChampFocus("demande_ouverture_enfant_0");
	}
	
	if ( formValide == true ) {
		$("formLivretA").submit();
	}
	else {
		alert("Une ou plusieurs informations ne sont pas correctement renseign�es. Merci de v�rifier votre saisie. ");
		if ( focusErreur != null ) {
			$(focusErreur).focus();
		}
	}
}

function afficherErreurForm(nomChamp, messageErreur) {
	var blocChamp = "blocChamp" + nomChamp;
	$(blocChamp).addClassName("blocLabelChampErr");
	$(blocChamp).innerHTML += '<p class="informationErreur">' + messageErreur + "</p>";
}

function annulerEnfant(numEnfant) {
	if ( confirm("Annuler et effacer toutes les informations saisies pour cet enfant ? ") ) {
		
		["parente", "nom", "prenom", "deb_num_compte", "fin_num_compte", "agence", "adresse", "code_postal", "date_naissance", "lieu_naissance", "ville"].each( function (nomChamp) {
			$(nomChamp + "_" + numEnfant).value = "";
		});
		
		if (numEnfant > 1) {
			$("formEnfant" + numEnfant).hide();
		}
		
	}
}

function profilDemandeDevenirClient (Valeur) {
    if (Valeur == "Particulier") {
        $('divParticulier').show();
        $('divEntreprise').hide();
    }
    else {
        $('divParticulier').hide();
        $('divEntreprise').show();
    }
}

function menu_gammes(id){
  select = document.getElementById(id);
  valeur = select.options[select.selectedIndex].value;
  if(valeur != "NULL")window.location.href = "index.php?Page=voir_gamme&fid="+valeur;
}

function mea_fiche(fid){
  elem    = $("fiche_" + fid);
  leLien  = $("lienGamme_" + fid);
  if(elem.style.display == "none"){
    elem.show();
    leLien.addClassName('lienTitreGammeEnCours');
  }
  else{
    elem.hide();
    leLien.removeClassName('lienTitreGammeEnCours');
  }
}

function fct_tri(valeur){
  $('tri').value = valeur;
  $('form_recherche').submit();
}