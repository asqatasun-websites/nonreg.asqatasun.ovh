var WS_Popup = new Class({
    Implements: [Events, Options],

    options: {
        container: 'document',
        onReturn: false,
        width: 400,
        overlay: 0.9,
        overlayClass : 'overlay',
        popupClass : 'popup',
        textOk : 'OK',
        textCancel : 'Annuler',
        onShow: function(type, popup) {
            popup.setStyle('display', 'block');

            if (type == 'iFrame') {
                popup.set('styles', {
                    'left': (window.getScroll().x + (window.getSize().x - popup.getSize().x) / 2).toInt(),
                    'top': $('navPrimaire').getSize().y + $('navPrimaire').getPosition().y,
                    'opacity': 1
                });
            } else if (type == 'video') {
                popup.set('styles', {
                    'left': (window.getScroll().x + (window.getSize().x - popup.getSize().x) / 2).toInt(),
                    'top': (window.getScroll().y + (window.getSize().y - 250) / 2).toInt(),
                    'opacity': 1
                });
            } else {
                popup.set('styles', {
                    'left': (window.getScroll().x + (window.getSize().x - popup.getSize().x) / 2).toInt(),
                    'top': ((window.getSize().y - popup.getSize().y) / 2).toInt(),
                    'opacity': 1
                });
            }

            if(type == 'confirm')
                this.actionCancel.focus();
            else if(type == 'prompt')
                this.actionInput.focus();
            else if (this.actionOk && type != 'iFrame')
                if (!Browser.Engine.trident)
                    this.actionOk.focus();

        },
        onClose: $empty,
        ajax: null,
        data: {}
    },

    initialize: function(type, el, options){
        this.setOptions(options);
        this.container = $(this.options.container) || $(document.body);

        if(this.options.overlay.toFloat() == 0)
            this.options.overlay = false;

        if(this.options.overlay) {
            if (type == 'iFrame') {
                this.overlay = new Element('div', {
                        'class': this.options.overlayClass,
                        'styles': {
                            'display': 'block',
                            'z-index': 10000,
                            'position': 'absolute',
                            'top': $('navPrimaire').getSize().y + $('navPrimaire').getPosition().y,
                            'left': '0',
                            'opacity': this.options.overlay.toFloat(),
                            'height': window.getScrollSize().y + 'px',
                            'width': window.getScrollSize().x + 'px'
                        }
                });
            } else {
                this.overlay = new Element('div', {
                        'class': this.options.overlayClass,
                        'styles': {
                            'display': 'block',
                            'z-index': 10000,
                            'position': 'absolute',
                            'top': '0',
                            'left': '0',
                            'opacity': this.options.overlay.toFloat(),
                            'height': window.getScrollSize().y + 'px',
                            'width': window.getScrollSize().x + 'px'
                        }
                });
            }
            $(document.body).adopt(this.overlay);
        }

        this.popup = new Element('div', {
            'class': this.options.popupClass,
            'styles': {
                'display': 'none',
                'z-index': 100000,
                'position': 'absolute',
                'width': this.options.width.toInt(),
                'opacity': 0
            }
        });
        this.container.adopt(this.popup);

        if($type(el) == 'string') {
            if (type == 'iFrame') {
                this.popup.adopt(new Element('iframe', {
                    'id' : 'iFrameAltibus',
                    'class' : 'nomask',
                    'width' : this.options.width,
                    'height' : 560,
                    'frameborder' : 0,
                    'src' : el,
                    'name' : 'iFrameAltibus'
                }));
            } else
                this.popup.set('html', el);
        } else if($type(el) == 'element') {
            this.popup.adopt(el);
        }

        this.actions = new Element('div', {
            'class': 'actions'
        });
        this.popup.adopt(this.actions);

        this.actionOk = new Element('input', {
            'id': 'actionOk',
            'class': 'submit',
            'type': 'submit',
            'value': this.options.textOk,
            'styles': {
                'width': '130px'
            }
        });

        this.actionCancel = new Element('input', {
            'id': 'actionCancel',
            'class': 'submit',
            'type': 'submit',
            'value': this.options.textCancel,
            'styles': {
                'width': '130px'
            }
        });

        if(type == 'prompt')
            this.prompt();
        else if(type == 'confirm')
            this.confirm();
        else if(type == 'confirm')
            this.alert();
        else if(type == 'ajax')
            this.ajax();
        else if(type == 'iFrame')
            this.iFrame();
        else if(type == 'video')
            this.video(el);
        else
            this.alert();
    },

    show: function(type){
        // Hack IE, masque les sélect & les object
        if (Browser.Engine.trident && Browser.Engine.version <= 6)
           $$('select').each(function(elem) {elem.setStyle('visibility', 'hidden');})
        $$('object[class!=nomask]').each(function(elem) {elem.style.visibility = 'hidden';})
        $$('iframe[class!=nomask]').each(function(elem) {elem.style.visibility = 'hidden';})

        return this.fireEvent('onShow', [type, this.popup]);
    },

    hide: function(){
        var returnValue = this.options.onReturn;
        if(this.options.overlay)
            this.overlay.destroy();
        this.popup.destroy();

        // Hack IE, réaffiche les sélect & les object
        if (Browser.Engine.trident && Browser.Engine.version <= 6)
           $$('select').each(function(elem) {elem.setStyle('visibility', 'visible');})
        $$('object[class!=nomask]').each(function(elem) {elem.style.visibility = 'visible';})
        $$('iframe[class!=nomask]').each(function(elem) {elem.style.visibility = 'visible';})

        return this.fireEvent('onClose', returnValue);
    },

    alert: function(){
        this.popup.addClass('alert');

        this.actionOk.addEvent('click', function() {
            this.options.onReturn = true;
            this.hide();
        }.bind(this));

        this.actions.adopt(this.actionOk);

        this.show('alert');
    },

    confirm: function(el){
        this.popup.addClass('confirm');

        this.actionOk.addEvent('click', function() {
            this.options.onReturn = true;
            this.hide();
        }.bind(this));
        this.actions.adopt(this.actionOk);

        this.actionCancel.addEvent('click', function() {
            this.options.onReturn = false;
            this.hide();
        }.bind(this));
        this.actions.adopt(this.actionCancel);

        this.show('confirm');
    },

    prompt: function(el){
        this.popup.addClass('prompt');

        this.actionInput = new Element('input', {
                'id': 'actionInput',
                'class': 'input',
                'type': 'text',
                'value': '',
                'styles': {
                    'width': '250px'
                }
        });
        this.actions.adopt(this.actionInput);

        this.actions.adopt(new Element('br'));

        this.actionOk.addEvent('click', function() {
            this.options.onReturn = this.actionInput.value;
            this.hide();
        }.bind(this));
        this.actions.adopt(this.actionOk);

        this.actionCancel.addEvent('click', function() {
            this.options.onReturn = false;
            this.hide();
        }.bind(this));
        this.actions.adopt(this.actionCancel);

        this.show('prompt');
    },

    ajax: function(){
        this.options.ajax.onSuccess = this.ajaxResponse.bind(this);

        new Request.HTML(this.options.ajax).send();
    },

    ajaxResponse: function(responseTree, responseElements, responseHTML, responseJavaScript){
        eval(responseJavaScript);

        this.popup.addClass('ajax');

        this.actionOk.addEvent('click', function() {
            this.options.onReturn = true;
            this.hide();
        }.bind(this));

        this.actions.adopt(this.actionOk);

        this.content = new Element('div').set('html', responseHTML);

        this.popup.adopt(this.content);
        this.popup.adopt(this.actions);

        this.popup.getElements('a').each(function(e) {
            e.addEvent('click', function(e) {
                this.hide();
            }.bind(this));
        }.bind(this));

        this.show('ajax');
    },

    iFrame: function(el){
        this.popup.addClass('iFrame');

        this.actionOk.addEvent('click', function() {
            this.options.onReturn = true;
            this.hide();
        }.bind(this));

        this.actions.adopt(this.actionOk);

        this.show('iFrame');

        new Fx.Scroll(window).toTop();
    },

    video: function(el){
        this.popup.addClass('video');

        this.show('video');

        if (Browser.Engine.trident) {
            this.popup.adopt(new Element('iframe', {
                'id' : 'iFrameVideo',
                'class' : 'nomask',
                'width' : this.options.width,
                'height' : 250,
                'frameborder' : 0,
                'src' : '/include/inc.insertVideoFlash.php?id=' + el,
                'name' : 'iFrameVideo'
            }));

            this.actions.inject(this.popup, 'bottom');
            this.actions.adopt(this.actionOk);

            this.actionOk.addEvent('click', function() {
                this.options.onReturn = true;
                this.hide();
            }.bind(this));

            this.actions.adopt(this.actionOk);
        } else {
            new Request.HTML({
                data: 'id=' + el,
                update: this.popup,
                onSuccess: function() {
                    this.actions.inject(this.popup, 'bottom');
                    this.actions.adopt(this.actionOk);

                    this.actionOk.addEvent('click', function() {
                        this.options.onReturn = true;
                        this.hide();
                    }.bind(this));

                    this.actions.adopt(this.actionOk);

                }.bind(this)
            }).get('/include/inc.insertVideoFlash.php');
        }
    }
});

Window.implement({
    $alert: function(el,options) {
        new WS_Popup('alert', el, options);
    },
    $confirm: function(el,options) {
        new WS_Popup('confirm', el, options);
    },
    $prompt: function(el,options) {
        new WS_Popup('prompt', el, options);
    },
    $ajax: function(options) {
        new WS_Popup('ajax', null, options);
    },
    $iFrame: function(el) {
        new WS_Popup('iFrame', el, {
            width: 652,
            textOk: 'Fermer'
        });
    },
    $video: function(flv) {
        $$('object').each(function(elem) { elem.SetVariable("player:jsStop", ""); });
        new WS_Popup('video', flv, {
            width:  375,
            textOk: 'Fermer',
            container: 'document.body',
            overlay: 0.6
        });
    }
});
