/* BEGIN: SETTINGS */

// Definitions for main menu
var menu_length = new Array();
menu_length['menu_point_1'] = { width: false, items: 3, full: false }
menu_length['menu_point_2'] = { width: false, items: 4, full: false }
menu_length['menu_point_3'] = { width: false, items: 4, full: false }
//menu_length['menu_point_4'] = { width: false, items: false, full: true }
//menu_length['menu_point_5'] = { width: false, items: false, full: true }

/* END: SETTINGS */


// TOOLS
	/*function $() {
	// getElementById
		var elements = new Array();
		for (var i = 0; i < arguments.length; i++) {
			var element = arguments[i];
			if (typeof element == 'string')
				element = document.getElementById(element);
			if (arguments.length == 1)
				return element;
			elements.push(element);
		}
		return elements;
	}*/
	
	function getTop(_obj){
		var obj = _obj;
		var top = obj.offsetTop;
			
		while (obj.tagName != "BODY") {
			obj = obj.offsetParent ;
			top += obj.offsetTop ;
		}
		return top ;
	}

	function getLeft(_obj){
		var obj = _obj ;
		var left = obj.offsetLeft ;
				
		while (obj.tagName != "BODY") {
			obj = obj.offsetParent ;
			left += obj.offsetLeft ;
		}

		return left ;
	}

// END OF TOOLS


// PROCESSING DEFAULT INPUTES
	function inputFocus() {
		if (this.type == "text") {
			if (this.value==this.getAttribute('defaultValue')) this.value="";
		}
	}

	function inputBlur() {
		if (this.value.length==0) {
			this.value = this.getAttribute('defaultValue')
		}
	}
// 


// MON ESPACE CLIENT POPUP
	function showMEC() {
		document.getElementById("monEspaceClient").style.display = "block";
		document.getElementById("monEspaceClient").style.visibility = "visible";
		document.getElementById("monEspaceClientSmall").style.display = "block";
	}
	function hideMEC() {
		document.getElementById("monEspaceClient").style.display = "none";
		document.getElementById("monEspaceClient").style.visibility = "hidden";
		document.getElementById("monEspaceClientSmall").style.display = "none";
	}
	function showStore() {
		document.getElementById("store").style.display = "block";
		document.getElementById("store").style.visibility = "visible";
		document.getElementById("store").style.display = "block";
	}
	function hideStore() {
		document.getElementById("store").style.display = "none";
		document.getElementById("store").style.visibility = "hidden";
		document.getElementById("store").style.display = "none";
	}
// EOF MON ESPACE CLIENT POPUP



// ROUNDED CORNERS GENERATOR
	function DomCheck(){
		return(document.createElement && document.getElementById)
	}

	function DomCorners(id, bk, h, tries){
		var el=$(id)[0];
		var c = new Array(4);
		for(var i=0;i<4;i++) {                      // create the four elements for rounded corners
			c[i]=document.createElement("b");
			if (i==2) {
				c[i].className = "rcgBL";
			}
			if (i==0) {
				c[i].className = "rcgTL";
			}
		}
		c[0].appendChild(c[1]);
		c[2].appendChild(c[3]);
		el.style.padding="0";
		el.insertBefore(c[0],el.firstChild);       // add top corners
		el.appendChild(c[2]);                      // and bottom ones
	}
// EOF ROUNDED CORNERS GENERATOR


$(window).load(function() {
	jQuery.each($(".grayFrame"), function () {DomCorners (this, "/cast/images/rc.png", 6);});
	
	fixScrolling();

	
	var inpArr = document.getElementById('newsletterMail');
	if (inpArr.value.length>0) {
		inpArr.setAttribute('defaultValue', inpArr.value);
		inpArr.onfocus = inputFocus;
		inpArr.onblur = inputBlur;
	}


	document.getElementById("mecLabel").onmouseover = showMEC;
	document.getElementById("monEspaceClient").onmouseover = showMEC;
	document.getElementById("monEspaceClientSmall").onmouseover = showMEC;
	document.getElementById("monEspaceClient").onmouseout = hideMEC;
	document.getElementById("monEspaceClientSmall").onmouseout = hideMEC;

	/* BEGIN: resize all menus */
	var list_all = document.getElementById("mainMenuUL").childNodes;
	var list_elements = new Array();

	for (i = 0; i < list_all.length; i++) {
		if (list_all[i].tagName == "LI") {
			var li_key = list_all[i].className.replace(/\smainMenuPoint$/, "") 
			list_elements[li_key] = { liElement: list_all[i] };
			var findDiv = list_all[i].childNodes;
			for (j = 0; j < findDiv.length; j++) {
				if (findDiv[j].className == "upperMenuPopup") {
					list_elements[li_key].popupMenu = findDiv[j];
					break;
				}
			}
		}
	}

	for (ki in menu_length) {
		if (menu_length[ki].width) {
                	list_elements[ki].popupMenu.style.width = menu_length[ki].width + "px";
		} else if (menu_length[ki].full) {
			list_elements = getLiParams(list_elements);

			var first = true;	
			var f_element = l_element = null;

			for (j in list_elements) {
				if (!/^menu\_point\_([0-9])+/.test(j)) {
					continue;
				}
				if (first) {
					f_element = j;
					first = false;
				}
				l_element = j;
			}

			list_elements[ki].popupMenu.style.marginLeft = "-"+ (list_elements[ki].coords.x - list_elements[f_element].coords.x) + "px";
			list_elements[ki].popupMenu.style.width = list_elements[l_element].coords.x + list_elements[l_element].coords.width - list_elements[f_element].coords.x + "px";
		} else {			
			list_elements = getLiParams(list_elements);
			
			var l_element = null
			var check = false;
			var count = 0;

			for (j in list_elements) {
				if (j == ki) {
					check = true;	
				}
				if (check && count < menu_length[ki].items) {
					count++;
					l_element = j;
				}
			}

			if(/MSIE\s6\.0/.test(navigator.userAgent)) {
				list_elements[ki].popupMenu.style.width = list_elements[l_element].coords.x + list_elements[l_element].coords.width - 10 - list_elements[ki].coords.x + "px";
			} else {
				list_elements[ki].popupMenu.style.width = list_elements[l_element].coords.x + list_elements[l_element].coords.width - list_elements[ki].coords.x + "px";
			}
		}
	}

	if(document.all && document.getElementById) { 
		for (i = 0; i < list_all.length; i++) {
			node = list_all[i]; 
			if (node.nodeName == "LI") {
				node.onmouseover = function() { 
					this.className += " over";
					var overLinkList = this.childNodes;
					for (j = 0; j < overLinkList.length; j++) {
						if (overLinkList[j].nodeName == "A") {
							overLinkList[j].className += " over";
							break;
						}
					}
				}
				node.onmouseout = function() { 
					this.className = this.className.replace(/\sover$/, ""); 
					var overLinkList = this.childNodes;
					for (j = 0; j < overLinkList.length; j++) {
						if (overLinkList[j].nodeName == "A") {
							overLinkList[j].className = overLinkList[j].className.replace(/\sover$/, "");
							break;
						}
					}
				} 
			} 
		} 
	} 
	/* END: resize all menus */

	resizeOverlay();
	
	
	
	// FOOTER BREADCRUMBS are changed for product details page
	var headerBreadcrumbs = $(".breadcrumbs div.homeBreadIco");
	var sameBreadcrumsOnPage = $("#sameBreadcrumsOnPage");
	
	if(sameBreadcrumsOnPage.length != 0){
		if(headerBreadcrumbs.length != 0){
			var topBreadcrumbsHtml = headerBreadcrumbs.parent().html();
			var footerBreadcrumbs = $(".footerBreadcrumbs");
			var homeIcoUrl = $("#homeIcoUrl").text();
	
	        footerBreadcrumbs.html(topBreadcrumbsHtml);
			var active = footerBreadcrumbs.find("div.active");
			var activeHtml = active.html();
			footerBreadcrumbs.removeClass("active").addClass("blue");
			active.removeClass("active").addClass("blue").html("<strong>"+activeHtml+"</strong>");
			
			footerBreadcrumbs.find("div.homeBreadIco").find("img").attr("src", homeIcoUrl);
			footerBreadcrumbs.find("div.homeBreadIco").removeClass();
		}	  
	} 

});

$(window).resize(function() {
		resizeOverlay();
		fixScrolling();
});

function fixScrolling() {
	if ((document.documentElement.clientWidth > 990) & (document.documentElement.clientWidth < 1026)) {
		document.documentElement.style.overflowX = "hidden"
	} else {
		document.documentElement.style.overflowX = "auto"
	}
}


function resizeOverlay() {
	$(".gray_overlay").height(($("#whiteContainer").height() > $(document).height())?$("#whiteContainer").height():$(document).height());
}

function showPopup(id) {
	$(".gray_overlay").show();
	$("#"+id).show();
	resizeOverlay();
	setNewCompPosition(id);
}

function showQuestionPopup(id) {
	showPopup(id);
	$("#newMessage").hide();
}

function hidePopup(_obj) {
	$(".gray_overlay").hide();
	$(_obj).parents(".whitePopupContainer").hide();	
	resizeOverlay();
}
function hideQuestionPopup(_obj) {
	hidePopup(_obj);
	showPopup('newMessage');
}

function setNewCompPosition(_obj) {
	winHeight = $(window).height();
	popupHeight = $('#'+_obj+'.whitePopupContainer').height();
	
	if (winHeight < popupHeight) {

		topPosition = $('#'+_obj+'.whitePopupContainer').position().top;
		$('#'+_obj+'.whitePopupContainer').css("top", topPosition);
		$('#'+_obj+'.whitePopupContainer').css("position", "absolute");
		
		if($.browser.msie && $.browser.version=="6.0") {
			$.scrollTo('.whitePopupContainer', 800);
		}
		
	}
	
}

function getLiParams (_obj) {
	for (i in _obj) {
		_obj[i].coords = getElementPosition(_obj[i].liElement);
	}
	return _obj;
}

function shuffleMenu(_obj) {
	if (_obj.parentNode.className != "menuPoint open") {
		var childs = _obj.parentNode.parentNode.childNodes;
		for (i = 0; i < childs.length; i++) {
			if (childs[i].tagName == "DIV") {
				childs[i].className = "menuPoint";
			}
		}
		_obj.parentNode.className = "menuPoint open";
	} else {
		_obj.parentNode.className = "menuPoint";	
	}
}

function getElementPosition(_obj) {
    var elem = _obj;
    
    var w = elem.offsetWidth;
    var h = elem.offsetHeight;
    
    var l = 0;
    var t = 0;
    
    while (elem) {
        l += elem.offsetLeft;
        t += elem.offsetTop;
        elem = elem.offsetParent;
    }

    return {"x":l, "y":t, "width": w, "height":h};
}

// show / hide products info
var timeoutIn, timeoutOut;
function elargeImage(_obj, _bool) {
	var inner_blocks = _obj.getElementsByTagName('div');
	for (i = 0; i < inner_blocks.length; i++) {
		if (inner_blocks[i].className == "elargeProduct") {
			var cur_block = inner_blocks[i];
			if (_bool) {
				clearTimeout(timeoutOut);
				timeoutIn = setTimeout(function() {
					clearTimeout(timeoutIn);					
					var all_popups = document.getElementsByTagName('div');
					
					for (j = 0; j < all_popups.length; j++) {
						if (all_popups[j].className == "elargeProduct") {
	                                		all_popups[j].style.visibility = "hidden"; 
							all_popups[j].parentNode.parentNode.style.zIndex = "5";
						}
					}
                               		cur_block.style.visibility = "visible"; 
					cur_block.parentNode.parentNode.style.zIndex = "6"; 
				}, 500);
				
			} else {				
				clearTimeout(timeoutIn);				
				timeoutOut = setTimeout(function() {
					clearTimeout(timeoutOut);
					cur_block.style.visibility = "hidden";
					
					var all_popups = document.getElementsByTagName('div');
					for (j = 0; j < all_popups.length; j++) {
						if (all_popups[j].className == "elargeProduct") {
	                                		all_popups[j].style.visibility = "hidden"; 
							all_popups[j].parentNode.parentNode.style.zIndex = "5";
						}
					}
					cur_block.parentNode.parentNode.style.zIndex = "5"; 
				}, 200);
			}
		}
	}
}

function toggleCamparateur() {
	var campExpaned = document.getElementById('campExpanded');
	var campCollapsed = document.getElementById('campCollapsed');
	
	if (campExpaned.style.display == "block") {
		campExpaned.style.display = "none";
		campCollapsed.style.display = "block";
	} else {
		campExpaned.style.display = "block";
		campCollapsed.style.display = "none";
	}
}

function collapseTxt(_obj) {
	if ($(_obj).parent("div").find("textarea:eq(0)").css("display") == "inline") {
		$(_obj).parent("div").find("textarea").hide();
		$(_obj).parent("div").find(".scrCollapser").html("&gt;");
	} else {
		$(_obj).parent("div").find("textarea").show();
		$(_obj).parent("div").find(".scrCollapser").html("v");
	}
}

function switchSearchTab(_obj){
}

function changeImage(obj) {
	var src = $(obj).find("IMG:eq(0)").attr('src');	
	//src = src.replace(/43x43/, "270x270");	
	if ($(obj).parents("#zoomedImage:eq(0)").length) {
		$("#popupImgLrgActive").css("visibility", "hidden");	
		$("#popupImgLrgActive").attr('src', src);
	} else {
		$("#lrgImg").css("visibility", "hidden");	
		$("#lrgImg").attr('src', src);
	}
		
	$(obj).parents("UL:eq(0)").find(".lImBorder").hide();
	$(obj).find(".lImBorder").show();
}

function changeMagasinImage(obj) {
	var src = $(obj).find("IMG:eq(0)").attr('src');	
	$("#lrgImg").css("visibility", "hidden");	
	$("#lrgImg").attr('src', src);
	$(obj).parents("UL:eq(0)").find(".lImBorder").hide();
	$(obj).find(".lImBorder").show();
	}


$(window).load(function(){
  
  $('.styled').selectbox({debug: false});
  
	$('.mainMenuUL').mouseover(function(){		
		$("INPUT").blur();
	})
	$('.ajoutpanierContainer').show();
	
	$('.azcontent .paginator A').click(function(){
		$('.azcontent .paginator A.active').removeClass('active');
		$(this).addClass('active');
	})

	
	// ************************************************************
	// Set IFRAME only for IE6 (Fix selectbox overlay bug)
	// ************************************************************
	
	if($.browser.msie && $.browser.version=="6.0") {
		$('.gray_overlay').html('<iframe frameborder="0"></iframe>');
		
		userWidth = $('#monEspaceClient .userShadow').width();
		userHeight = $('#monEspaceClient .userShadow').height();
			
		$('#monEspaceClient .userShadow').before('<iframe frameborder="0" class="clientIframe"></iframe>');
		$('.clientIframe').width(userWidth);
		$('.clientIframe').height(userHeight);
		
	}
	
	
	
	// ************************************************************
	// Count comparator items and set equal width for product rows.
	// ************************************************************ 

	var prodImageRow = $('.productImages TD');
	
	if (prodImageRow.size() == 1) {
		
	} else if (prodImageRow.size() == 2) {
		prodImageRow.width(342);
	} else if (prodImageRow.size() == 3) {
		prodImageRow.width(228);
	} else if (prodImageRow.size() == 4) {
		prodImageRow.width(171);
	};
});

function showAjouterPopup(id) {		
	$('.autresLnk').parent("DIV").not(id).css("z-index", 999);	
	if ($(id).is(":visible")) {
		$(id).hide();
	} else {	
		$(id).show();								
	}
	$(id).parent("DIV").css("z-index", 1000);
			
} 

function showContPopup(_id, _param) {
	if ($('.autresPopup').length > 1) {
		
		$('.autresPopup')
			.not('div#ajouterPopup_'+_id)
			.hide(1, function () {
				});
				
		$('div#ajouterPopup_'+_id)
					.toggle()
					.parent('div')
					.css('z-index', 1000);
		var loaded = $("div#ajouterPopup_" + _id).attr("loaded");
		if (loaded == 'false') {
			
			$("div#ajouterPopup_" + _id).load(_param, null, function() {$("div#ajouterPopup_" + _id).attr("loaded", 'true');});
			
		}		
				
		$('.autresPopup')
			.not('div#ajouterPopup_'+_id)					
			.parent('div')
			.css('z-index', 999);
			/**
			$('div#ajouterPopup_'+_id)
					.toggle()
					.parent('div')
					.css('z-index', 1000);
				}
			*/
		/*var loaded = $("div#ajouterPopup_" + _id).attr("loaded");
		if (loaded == 'false') {
			
			$("div#ajouterPopup_" + _id).load(_param);
			$("div#ajouterPopup_" + _id).attr("loaded", 'true');
		}*/
	} else {
		var loaded = $("div#ajouterPopup_" + _id).attr("loaded");
		if (loaded == 'false') {
			
			$("div#ajouterPopup_" + _id).load(_param, null, function() {$("div#ajouterPopup_" + _id).attr("loaded", 'true');});
			
		}
		$('.autresPopup')
			.toggle()
			.parent('div')
			.css('z-index', 1000);
	}
}

function showAtoutContPopup(_param) {
	var loaded = $("div#atoutPopup").attr("loaded");
	if (loaded == 'false'){
		$("div#atoutPopup").load(_param, null, function() {$("div#atoutPopup").attr("loaded", 'true'); $(".gray_overlay").show();});
	}else{
		$("div#atoutPopup").show();
		$(".gray_overlay").show();
	}
	
}

function closeAtoutContPopup(){
	$("div#atoutPopup").hide();
	$(".gray_overlay").hide();
}

function showVideoPopup(_id, _param){
	$("#" + _id).show();	
	$("#" + _id).load(_param+"&uid="+Math.random() * 100, null, function() {$(".gray_overlay").show(); resizeOverlay();});	
}

function hideVideoPopup(_id, _param){
	$("#" + _id).empty().hide();
	$(".gray_overlay").hide();	
	
}

function showImgPopup(_obj) {

	var imgUrl = $(_obj).parents(".productImage:eq(0)").find("#lrgImg").attr("src");	
	$("#popupImgLrgActive").attr("src", imgUrl);
	$("#zoomedImage").find(".popupContentContainer:eq(0)").find(".imageViews").remove();
	var previews = $(_obj).parents(".productImageColumn:eq(0)").find(".imageViews").clone();	
	
	$("#zoomedImage").find(".popupContentContainer:eq(0)").append(previews);
	var previewsIns = $("#zoomedImage").find(".popupContentContainer:eq(0)").find(".imageViews");
	previewsIns.find("h3").remove();
	previewsIns.append("<div class='clear'></div>");
	previewsIns.find("a").removeAttr("onclick");
  previewsIns.find("a").click(function(){
    changeImage(this);
  });
	
	if(_obj.className = "controlZoom") {
    $('#zoomedImage .imageViews A:first').click();
  };
	
	$("#zoomedImage").find(".whitePopupContent").css("width", "642px");
	$("#zoomedImage").show();
		
	$(".gray_overlay").show();	
	resizeOverlay();
}

function showImgPopup2(_obj) {

  $(_obj).parents("UL:eq(0)").find(".lImBorder").hide();
  $(_obj).find(".lImBorder").show();

  var imgUrl = $(_obj).find("img").attr("src");  
  $("#popupImgLrgActive").attr("src", imgUrl);
  $("#zoomedImage").find(".popupContentContainer:eq(0)").find(".imageViews").remove();
  var previews = $(_obj).parents(".productImageColumn:eq(0)").find(".imageViews").clone();
  
  $("#zoomedImage").find(".popupContentContainer:eq(0)").append(previews);
  var previewsIns = $("#zoomedImage").find(".popupContentContainer:eq(0)").find(".imageViews");
  previewsIns.find("h3").remove();
  previewsIns.append("<div class='clear'></div>");
  previewsIns.find("a").removeAttr("onclick");
  previewsIns.find("a").click(function(){
    changeImage(this);
  });

  
  $("#zoomedImage").find(".whitePopupContent").css("width", "642px");
  $("#zoomedImage").show();
    
  $(".gray_overlay").show();  
  resizeOverlay();
  
  
}

function counAndShowCarItems() {  
  var carouselItems = $('.carThumbs UL > LI').size();
    if (carouselItems > 10) {
      $('.carousel').prepend('<div class="carProdCountHolder"><div class="carProdCount">' + carouselItems + ' produits</div></div>');
    }
    
    if ($('.breadcrumbs').length == 0) {
        $('.carProdCount').css("top","-15px");
    }
}

function disableButton(_obj) {
   if(_obj) {
    var name = _obj.name;
    var value = _obj.value;
    var form = $(_obj).parent('form');
    if(form) {
      $(_obj).attr("name", name + "_");
      $(_obj).attr("value", value);
      $(_obj).attr("disabled", true);
       // create hidden input with the same name and value
       form.append("<input type='hidden' name='" + name + "' value='" + value + "' />");
       
       form.submit();
    }
     return false;
   } 
   return true;
}

function typeGiftMessage() {
  var giftMessage = $("#giftMessage").attr("value");
  var giftCheckbox = $("#isGift");
  if(giftCheckbox) {
	  if(giftMessage && giftMessage.length > 0) {
	    giftCheckbox.attr("checked","true");
	  } else {
      giftCheckbox.attr("checked","");
	  }
  }
}

function removeGiftMessage(_obj) {
  if(!_obj.checked) {
    $("#giftMessage").attr("value", "");
  }
}