function chargerSecteur()
{
	var liste = ''; 
	liste+='<option value="1">Administration / Service public</option>';
	liste+='<option value="2">Agriculture / Agroalimentaire</option>';
	liste+='<option value="3">Art / Culture / Edition / Cin&eacute;ma</option>';
	liste+='<option value="4">Associatif / B&eacute;n&eacute;volat</option>';
	liste+='<option value="5">Audit / Conseil / Droit</option>';
	liste+='<option value="6">Banques / Assurances / Finance</option>';
	liste+='<option value="7">Commerce / Distribution</option>';
	liste+='<option value="8">Communication / Publicit&eacute; / M&eacute;dia</option>';
	liste+='<option value="9">Construction / BTP / Immobilier</option>';
	liste+='<option value="10">Energie / Environnement</option>';
	liste+='<option value="11">Enseignement / Formation</option>';
	liste+='<option value="12">Industries / Bureau d\'&eacute;tudes / Ing&eacute;nierie</option>'; 
	liste+='<option value="13">Informatique / T&eacute;l&eacute;com</option>';
	liste+='<option value="14">Internet / Multim&eacute;dia</option>';
	liste+='<option value="15">Logistique / Transport</option>';
	liste+='<option value="16">Sant&eacute; / Social</option>';
	liste+='<option value="17">Services aux entreprises</option>'; 
	liste+='<option value="18">Services &agrave; la personne</option>';
	liste+='<option value="19">Tourisme / H&ocirc;tellerie / Restauration / Loisirs</option>';

   	return liste;
}

function chargerMetier()
{
	var liste = ''; 
	liste+='<option value="1">Achat / Logistique</option>';
	liste+='<option value="12">Administration / Services g&eacute;n&eacute;raux</option>';
	liste+='<option value="18">Art / Culture</option>';
	liste+='<option value="30">Audit / Conseil</option>';
	liste+='<option value="44">Banque / Assurance</option>';
	liste+='<option value="59">Commercial / Vente</option>';
	liste+='<option value="80">Comptabilit&eacute; / Gestion / Finance</option>';
	liste+='<option value="91">Direction g&eacute;n&eacute;rale</option>';
	liste+='<option value="102">Droit / Juridique</option>';
	liste+='<option value="112">Enseignement / Formation</option>';
	liste+='<option value="126">Etudes / Recherche et d&eacute;veloppement</option>';
	liste+='<option value="141">H&ocirc;tellerie / Restauration</option>';
	liste+='<option value="145">Immobilier</option>';
	liste+='<option value="150">Informatique / T&eacute;l&eacute;com</option>';
	liste+='<option value="169">Marketing</option>';
	liste+='<option value="185">M&eacute;dias / Communication / Cr&eacute;ation</option>';
	liste+='<option value="216">Production / Fabrication / Construction / Environnement / S&eacute;curit&eacute;</option>';
	liste+='<option value="253">Ressources humaines</option>';
	liste+='<option value="262">Sant&eacute; / Social / Associatif</option>';
	liste+='<option value="280">Tourisme / Loisirs</option>';
	liste+='<option value="287">Transports</option>';
	liste+='<option value="294">Web / Multim&eacute;dia</option>';

	return liste;
}

function chargerLocalisation()
{
	var liste = ''; 
	liste+='<option value="1">Alsace</option>';
	liste+='<option value="2">Aquitaine</option>';
	liste+='<option value="3">Auvergne</option>';
	liste+='<option value="4">Basse-Normandie</option>';
	liste+='<option value="5">Bourgogne</option>';
	liste+='<option value="6">Bretagne</option>';
	liste+='<option value="7">Centre</option>';
	liste+='<option value="8">Champagne-Ardenne</option>';
	liste+='<option value="9">Corse</option>';
	liste+='<option value="10">DOM-TOM</option>';
	liste+='<option value="11">Franche-Comt&eacute;</option>';
	liste+='<option value="12">Haute-Normandie</option>';
	liste+='<option value="13">Ile-de-France</option>';
	liste+='<option value="14">Languedoc-Roussillon</option>';
	liste+='<option value="15">Limousin</option>';
	liste+='<option value="16">Lorraine</option>';
	liste+='<option value="17">Midi-Pyr&eacute;n&eacute;es</option>';
	liste+='<option value="18">Nord-Pas-de-Calais</option>';
	liste+='<option value="19">Pays de la Loire</option>';
	liste+='<option value="20">Picardie</option>';
	liste+='<option value="21">Poitou-Charentes</option>';
	liste+='<option value="22">Provence-Alpes-C&ocirc;te-d\'Azur</option>';
	liste+='<option value="23">Rh&ocirc;ne-Alpes</option>';
	
	return liste;
}