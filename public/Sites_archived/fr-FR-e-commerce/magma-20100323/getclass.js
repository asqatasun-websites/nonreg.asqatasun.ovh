function GetElementsWithClassName(elementName, className) {
	var allElements = document.getElementsByTagName(elementName);
	var elemColl = new Array();
	for (i = 0; i < allElements.length; i++) {
	if (allElements[i].className == className) {
	elemColl[elemColl.length] = allElements[i];
	}
	}
	return elemColl;
}