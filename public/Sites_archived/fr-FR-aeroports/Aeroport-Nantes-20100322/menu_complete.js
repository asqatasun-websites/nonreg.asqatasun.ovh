/*
 tableau des reponses necessaires � la correspondance entre la suggestion et les donn�es
 Vous pouvez bien entendu le generer en php ou bien de facon dynamique avec ajax ;)
 */
 
 
 /*
 fonction de recalcul de l'offset top et left qui se refere � l'offsetParent pour palier au probleme de positionnement dans les tableaux
 */
 
function positionAbsolute(obj, mode)
{
	currentValue = 0;
	if (obj.offsetParent)
	{
		if (mode == 'top')
			currentValue = obj.offsetTop;
		else if (mode == 'left')
			currentValue = obj.offsetLeft;
		while (obj = obj.offsetParent)
			if (mode == 'top')
				currentValue += obj.offsetTop;
			else if (mode == 'left')
				currentValue += obj.offsetLeft;
	}
	return currentValue;
}
function getPosition(element)
{
    var left = 0;
    var top = 0;
    /*On r�cup�re l'�l�ment*/
    var e = element;
    /*Tant que l'on a un �l�ment parent*/
    while (e.offsetParent != undefined && e.offsetParent != null)
    {
        /*On ajoute la position de l'�l�ment parent*/
        left += e.offsetLeft + (e.clientLeft != null ? e.clientLeft : 0);
        top += e.offsetTop + (e.clientTop != null ? e.clientTop : 0);
        e = e.offsetParent;
    }
    return new Array(left,top);
}

 /*
 la fonction principale qui remplis le div des differentes suggestions possibles en cor�lation avec le champ input
 */
 function showValue(inside, miniForm)
 {
 divDest = document.getElementById('menuRightHidden');
 divDest.innerHTML = '';
 valueOfElement = '';
 exist = 0;
 nbOfElement = 0;
 divDest.className = 'visibleDiv';
 positionInside = getPosition(inside);
 divDest.style.display = "block";
 divDest.style.visibility = "visible";
if(navigateur == "IE"){
adaptationTopIe = 10;
adaptationLeftIe = 10;
}else{
adaptationTopIe = 0;
adaptationLeftIe = 0;
}
 if(miniForm){
	divDest.style.top = (inside.offsetTop + inside.offsetHeight + adaptationTopIe)+"px";
	divDest.style.left = (inside.offsetLeft + adaptationLeftIe)+"px";
	/*divDest.style.offsetTop = inside.offsetTop;
	divDest.style.offsetLeft = inside.offsetLeft;*/
 }else{
	divDest.style.top = (positionInside[1] + inside.offsetHeight)+"px";
	divDest.style.left = (positionInside[0])+"px";
 }
 /*alert(divDest.style.top+", "+divDest.style.left);*/
 /*divDest.style.top = positionAbsolute(inside, 'top') + inside.offsetHeight;
 divDest.style.left = positionAbsolute(inside, 'left');
 divDest.style.width = 2 * inside.offsetWidth;*/
 var j = true;
 for (i = 0; i < suggests.length; i++)
 {
	 if ( (suggests[i].substr(4).search(new RegExp(inside.value, 'i')) == 0) && (inside.value != '') )
	 {   
	 	 nbOfElement++;
		 exist = 1;
		 valueOfElement = suggests[i].substr(4);
		 currentDiv = document.createElement("div");
		 currentInner = document.createTextNode(suggests[i].substr(4));
		 currentDiv.indice = suggests[i].substr(4);
		 currentDiv.iata = suggests[i].substr(1,3);
		 if( j ){
			document.getElementById('aeroport').value = suggests[i].substr(1,3);
			j = false;
		 }
		 currentDiv.className = 'currentDiv';
		 currentDiv.onclick = function()
		 {
			 inside.value = this.indice;
			 document.getElementById('aeroport').value = this.iata;
			 divDest.className = 'hiddenDiv';
			 if(document.getElementById('selectheure')!=null) document.getElementById('selectheure').style.visibility='visible';
		 }
		 currentDiv.appendChild(currentInner);
		 // on ajoute l'image et le texte en cas de vol direct :
		 if (suggests[i].substr(0,1) == 'D') {
		 	// ajout de l'image :
		 	var newDetailImg = document.createElement('img');
		 	// 20071116 cpetit : emplacement relatif � la page
		 	newDetailImg.setAttribute("src","../../projet-ressources/element_oag/picto-voldirect.png");
		 	currentDiv.appendChild(newDetailImg);
		 	// ajout du texte : 
		 	var newDetail = document.createElement('font');
//		 	newDetail.setAttribute("class","infovoldirect");
		 	newDetail.setAttribute("color","#dd0980");
		 	var newTxtDetail = document.createTextNode(' Vol direct');
		 	newDetail.appendChild(newTxtDetail);
		 	currentDiv.appendChild(newDetail);
		 }
		 // on ajoute la ligne au div en popup
		 divDest.appendChild(currentDiv);
	 }
 }
 
	if (exist == 0 || (valueOfElement.length == inside.value.length && nbOfElement == 1)){
		divDest.className = 'hiddenDiv';
		if(document.getElementById('selectheure')!=null) document.getElementById('selectheure').style.visibility='visible';
	}
	else{
		divDest.className = 'visibleDiv';
		if(document.getElementById('selectheure')!=null) document.getElementById('selectheure').style.visibility='hidden';
	}
}

 /*
 intialisation de la div qui contiendra les differentes suggestions
 */
 /*
function initMenuRightDiv()
{
	 menuRightDiv = document.createElement("div");
	 menuRightDiv.className = 'hiddenDiv';
	 menuRightDiv.id = 'menuRightHidden';
	 window.document.body.appendChild(menuRightDiv);
}
*/
/* init au chargement */
//window.onload = initMenuRightDiv;
