// Librairie de scripts 


//****************************************************************
// Fonction d'ajout d'articles dans le panier
//****************************************************************
function funcAddBasket(varId, varWebId, varPanierMini) {
	var oXmlHttp = null;
	
//alert('varId : ' + varId);

	// Mozilla
	if (window.XMLHttpRequest)
		{
		oXmlHttp = new XMLHttpRequest();
		}
	// IE
	else if (window.ActiveXObject)
		{
		oXmlHttp = new ActiveXObject("Microsoft.XmlHttp");
		}
		
	if (oXmlHttp != null) {
		var strDetail = '';
		for (var k=1; k<4; k++)
			if (document.getElementById('sel_feature_' + k))
				{
				if (k > 1)
					strDetail = strDetail +  " - "
				if (document.getElementById('sel_feature_' + k).selectedIndex != null)
					strDetail = strDetail + document.getElementById('sel_feature_' + k).title + ' : ' + document.getElementById('sel_feature_' + k).options[document.getElementById('sel_feature_' + k).selectedIndex].innerHTML;
				else
					strDetail = strDetail + document.getElementById('sel_feature_' + k).title + ' : ' + document.getElementById('sel_feature_' + k).value;					
				}
		
		
		var intQte
		if (document.getElementById('art_Qte'))
			intQte = document.getElementById('art_Qte').value;
		else
			intQte = 1;
//alert("../_inc/XML_funcAddPanier.asp?strId=" + varId + "&strQte=" + intQte + "&strDetail=" + strDetail + "&strWebId=" + varWebId);		
		oXmlHttp.open("GET","../_inc/XML_funcAddPanier.asp?ms="+ new Date().getTime() +"&strId=" + varId + "&strQte=" + intQte + "&strDetail=" + escape(strDetail) + "&strWebId=" + varWebId, false);
		oXmlHttp.setRequestHeader('Content-Type','text/html; charset=iso-8859-1')
		oXmlHttp.setRequestHeader("Cache-Control","no-cache");
		oXmlHttp.setRequestHeader('Pragma','no-cache');
		oXmlHttp.send(null);
//	alert(oXmlHttp.responseText);
		if (oXmlHttp.responseText == '')
			alert('Article(s) ajout�(s) dans le panier');
		if ((varPanierMini.toLowerCase() == 'oui') || (varPanierMini.toLowerCase() == 'true')) {
			funcDisplayBasketMini(varWebId, 'mini');
			}
		if (document.getElementById('qte_' + varId))
			document.getElementById('qte_' + varId).value='1';
		}
	}
	
function funcDisplayBasketMini(varWebId, varMode) {
	var oXmlHttp = null;
	// Mozilla
	if (window.XMLHttpRequest)
		{
		oXmlHttp = new XMLHttpRequest();
		}
	// IE
	else if (window.ActiveXObject)
		{
		oXmlHttp = new ActiveXObject("Microsoft.XmlHttp");
		}
		
	if (oXmlHttp != null) {
//alert("../_inc/XML_funcDisplayPanier.asp?ms="+ new Date().getTime() +"&strMode=" + varMode + "&strWebId="+ varWebId);
		oXmlHttp.open("GET","../_inc/XML_funcDisplayPanier.asp?ms="+ new Date().getTime() +"&strMode=" + varMode + "&strWebId="+ varWebId, false);
		oXmlHttp.setRequestHeader('Content-Type','text/html; charset=iso-8859-1');
		oXmlHttp.setRequestHeader("Cache-Control","no-cache");
		oXmlHttp.setRequestHeader('Pragma','no-cache');
		oXmlHttp.send(null);
//alert(oXmlHttp.responseText);
		return eval(oXmlHttp.responseText);
		}
	else
		alert('Fonctionnalit� non disponible avec votre navigateur.');
}


function funcPanierRecalc(varQte, varCmdeId, varSelect, varCmdeDetailId, varPanierMini, varContactId, varWebId) {
	if ((varQte == -1) || (varQte == 0))
		if (!confirm('D�sirez-vous vraiment enlever cet article de votre panier ?')) 
			return;
		else
			varQte = -1;
	
	var oXmlHttp = null;
	try {
		oXmlHttp = new ActiveXObject("Msxml2.XmlHttp");
		}
	catch (e) {
		try {
			oXmlHttp = new ActiveXObject("Microsoft.XmlHttp");
			}
		catch (err) {
			oXmlHttp = null;
			}
		}
		
	// Mozilla
	if (!oXmlHttp && typeof XMLHttpRequest != 'undefined')
		{
		try
			{
			oXmlHttp = new XMLHttpRequest();
//			alert("XMLHttpRequest");
			}
		catch(e)
			{
			oXmlHttp = false;
			}
		}
		
	if (oXmlHttp) {
		oXmlHttp.open("GET","../_inc/XML_funcUpdatePanier.asp?ms="+ new Date().getTime() +"&strCmdeId=" + varCmdeId + "&strId=" + varCmdeDetailId + "&strQte="+ varQte + "&strSelect=" + varSelect, false);
		oXmlHttp.setRequestHeader('Content-Type','text/html; charset=iso-8859-1')
		oXmlHttp.setRequestHeader("Cache-Control","no-cache");
		oXmlHttp.setRequestHeader('Pragma','no-cache');
		oXmlHttp.send(null);
//		eval(oXmlHttp.responseText);
		}
	eval(oXmlHttp.responseText);
	
	if (varPanierMini == 'oui') {
		funcDisplayBasketMini(varWebId, 'mini');
		} 

	funcCheckPanier();
	}
	
function funcCheckPanier() {
	var totalTTC = 0;
	var totalSelectTTC = 0;
	var tempElt = '';
	var blnPanier = false;
	var blnSelect = false;
	for (var k=0; k<document.all.length; k++)
		{
		if(document.all[k].id.indexOf('cb_')>-1)
			{
			blnPanier = true;
			if (document.getElementById(document.all[k].id.replace('cb_', 'totArticleTTC_')))
				tempElt = document.getElementById(document.all[k].id.replace('cb_', 'totArticleTTC_')).innerHTML.toLowerCase();
			else if (document.getElementById(document.all[k].id.replace('cb_', 'totArticleHT_')))
				tempElt = document.getElementById(document.all[k].id.replace('cb_', 'totArticleHT_')).innerHTML.toLowerCase();
			if (document.all[k].checked == true)
				{
				blnSelect = true;
				totalSelectTTC = totalSelectTTC + parseFloat(tempElt.replace(' ', '').replace(',','.').replace('<b>', '').replace('</b>', ''));
				}
			totalTTC = totalTTC + parseFloat(tempElt.replace(' ', '').replace(',','.').replace('<b>', '').replace('</b>', ''));
			}
		}
		
		if (document.getElementById('tr_empty'))	
			document.getElementById('tr_empty').style.display='none';
		if (document.getElementById('totalTTC'))
			{
			document.getElementById('totalTTC').innerHTML=formatNumber(totalTTC, 2, ' ');
			document.getElementById('totalSelectTTC').innerHTML='<b>' + formatNumber(totalSelectTTC, 2, ' ') + '</b>';
			}
		else if (document.getElementById('totalHT'))
			{
			document.getElementById('totalHT').innerHTML=formatNumber(totalTTC, 2, ' ');
			document.getElementById('totalSelectHT').innerHTML='<b>' + formatNumber(totalSelectTTC, 2, ' ') + '</b>';
			}
	
	if (blnPanier)
		{
		if (blnSelect)
			{
			if (document.getElementById('a_Cmde'))
				{
				if((document.getElementById('a_Cmde').innerHTML.indexOf('Ajouter')==0) || (document.getElementById('a_Cmde').innerHTML.indexOf('Afficher')==0))
					document.getElementById('a_Cmde').innerHTML = 'Ajouter les articles s�lectionn�s � votre commande en cours'
				document.getElementById('a_Cmde').style.display = 'inline';
				}
			if (document.getElementById('span_Cmde'))
				document.getElementById('span_Cmde').style.display = 'none';
			}
		else
			{
	//alert(document.getElementById('a_Cmde').innerHTML);
			if (document.getElementById('a_Cmde'))
				{									
				if((document.getElementById('a_Cmde').innerHTML.indexOf('Ajouter')==0) || (document.getElementById('a_Cmde').innerHTML.indexOf('Afficher')==0))
					{
					document.getElementById('span_Cmde').style.display = 'none';
					document.getElementById('a_Cmde').style.display = 'inline';
					document.getElementById('a_Cmde').innerText = 'Afficher votre commande en cours';
					}
				else
					{
					document.getElementById('a_Cmde').style.display = 'none';
					document.getElementById('span_Cmde').style.display = 'inline';
					}
				}
			else if (document.getElementById('span_Cmde'))
				document.getElementById('span_Cmde').style.display = 'inline';		
			}
		}
	else
		{
		if (document.getElementById('tr_empty'))	
			document.getElementById('tr_empty').style.display='block';
		}
	}
	
function funcValidCmde(varAction)
	{
//		switch (document.getElementById('strAction').value) {
	switch (varAction) {
		case '1':
			document.getElementById('div_CmdeArticle').style.display='inline';
			document.getElementById('div_CmdeCoord').style.display='none';
			document.getElementById('strAction').value='2';
			break;	
		case '2':
			document.getElementById('div_CmdeArticle').style.display='none';
			document.getElementById('div_CmdeCoord').style.display='inline';
			if (funcCtrlSaisie(document.getElementById('formCmde')) == true) {
				document.getElementById('strAction').value='3';
				document.getElementById('formCmde').submit;
			}
			break;
		case '3':
			alert('ok2');
		}
	}

function formatNumber(valeur,decimal,separateur) {
//	alert(valeur);
// formate un chiffre avec 'decimal' chiffres apr�s la virgule et un separateur
	var deci=Math.round( Math.pow(10,decimal)*(Math.abs(valeur)-Math.floor(Math.abs(valeur)))) ; 
	var val=Math.floor(Math.abs(valeur));
	if ((decimal==0)||(deci==Math.pow(10,decimal))) {val=Math.floor(Math.abs(valeur)); deci=0;}
	var val_format=val+"";
	var nb=val_format.length;
	for (var i=1;i<4;i++) {
		if (val>=Math.pow(10,(3*i))) {
			val_format=val_format.substring(0,nb-(3*i))+separateur+val_format.substring(nb-(3*i));
		}
	}
	if (decimal>0) {
		var decim=""; 
		for (var j=0;j<(decimal-deci.toString().length);j++) {decim+="0";}
		deci=decim+deci.toString();
		val_format=val_format+"."+deci;
	}
	if (parseFloat(valeur)<0) {val_format="-"+val_format;}
	return val_format.replace('.', ',');
}	



function funcDisplayFeature(varArticleId, varFeatureId, varDisplayDispo)
	{
	if (varArticleId == '')
		{
		return;
		}
//alert(varArticleId + ' - ' + varFeatureId + ' - ' + varDisplayDispo );

	var oXmlHttp = null;
	// Mozilla
	if (window.XMLHttpRequest)
		{
		oXmlHttp = new XMLHttpRequest();
		}
	// IE
	else if (window.ActiveXObject)
		{
		oXmlHttp = new ActiveXObject("Microsoft.XmlHttp");
		}
		
	if (oXmlHttp != null) {
		oXmlHttp.open("GET","../_inc/XML_funcDisplayFeature.asp?ms="+ new Date().getTime() +"&strArticleId=" + varArticleId + "&strFeatureId=" + varFeatureId + "&strDisplayDispo=" + varDisplayDispo, false);
		oXmlHttp.setRequestHeader('Content-Type','text/html; charset=iso-8859-1')				
		oXmlHttp.setRequestHeader("Cache-Control","no-cache");
		oXmlHttp.setRequestHeader('Pragma','no-cache');
		oXmlHttp.send(null);
//alert(oXmlHttp.responseText);
		eval(oXmlHttp.responseText);
		}
	else
		alert('Fonctionnalit� non disponible avec votre navigateur.');
	}



function funcChangeArticle(varArtId, varDisplayDispo) {
//alert(varArtId);
	var oXmlHttp = null;
	// Mozilla
	if (window.XMLHttpRequest)
		{
		oXmlHttp = new XMLHttpRequest();
		}
	// IE
	else if (window.ActiveXObject)
		{
		oXmlHttp = new ActiveXObject("Microsoft.XmlHttp");
		}
		
	if (oXmlHttp != null) {
		oXmlHttp.open("GET","../_inc/XML_funcChangeArticle.asp?ms="+ new Date().getTime() +"&strId=" + varArtId + '&strDisplayDispo=' + varDisplayDispo, false);
		oXmlHttp.setRequestHeader('Content-Type','text/html; charset=iso-8859-1')
		oXmlHttp.setRequestHeader("Cache-Control","no-cache");
		oXmlHttp.setRequestHeader('Pragma','no-cache');
		oXmlHttp.send(null);
//alert(oXmlHttp.responseText);
		eval(oXmlHttp.responseText);
		}
	else
		alert('Fonctionnalit� non disponible avec votre navigateur.');

	oXmlHttp = null;
}
