var PERIODE_BANDEAUX="5000";
var PERIODE_ACTUS="6000";
Element.extend({hide:function(){
return this.setStyle("display","none")},show:function(){
return this.setStyle("display","block")}});
var DropdownMenu=new Class({initialize:function(a){
$A($(a).childNodes).each(function(b){
if(b.nodeName.toLowerCase()=="li"){
$A($(b).childNodes).each(function(c){
if(c.nodeName.toLowerCase()=="ul"){
$(c).setStyle("display","none");
b.addEvent("mouseover",function(){
c.setStyle("display","block");
if(c.className="universite"){
$each($("acces_direct").getElements("select"),function(e,d){
e.setStyle("display","none")})}return false});
b.addEvent("mouseout",function(){
c.setStyle("display","none");
if(c.className="universite"){
$each($("acces_direct").getElements("select"),function(e,d){
e.setStyle("display","block")})}});
new DropdownMenu(c)}})}});
return this}});
function changeStyleMenu(a,b){
if(b=="over"){
a.className="over"}else{a.className=""}}function changeLocation(a){
document.location=a}var cpt_actu=0;
var alaune=function changeActu(){
var b=document.getElements(".bloc_actu");
if(b&&b.length>1){
b[cpt_actu].fade("out");
b[cpt_actu].setStyle("display","none");
var a=cpt_actu;
if(cpt_actu<b.length-1){
cpt_actu++}else{cpt_actu=0}b[cpt_actu].fade("in");
b[cpt_actu].setStyle("display","block")}};
var changeHeader=function(){
if(images_loaded){
images_loaded[cpt_img].fade("0.0");
if(cpt_img<images_loaded.length-1){
cpt_img++}else{cpt_img=0}images_loaded[cpt_img].setStyle("visibility","visible");
images_loaded[cpt_img].fade("1.0")}};
var changeImagePage=function(){
if($("image_page_"+cpt_image_page)){
$("image_page_"+cpt_image_page).fade("0.0");
$("image_page_"+cpt_image_page).style.display="none"}var a=cpt_image_page;
if(cpt_image_page<cpt_image_page_total-1){
cpt_image_page++}else{cpt_image_page=0}if($("image_page_"+cpt_image_page)){
$("image_page_"+cpt_image_page).fade("1.0");
$("image_page_"+cpt_image_page).style.display="block"}};
var smartHoverBox=function(d,e,c,g,i){
var h=$(document.body).getElements("[id$="+g+"]");
var f=$(document.body).getElements("."+i);
var a=function(){
h.setStyle("display","none")};
f.addEvent("click",function(){
a()}).setStyle("cursor","pointer");
var b=0;
h.each(function(k){
var j=k.getProperty("id");
j=j.replace(""+g+"","");
$(j).addEvent("mouseleave",function(){
b=a.delay(d)});
k.addEvent("mouseleave",function(){
b=a.delay(d)});
$(j).addEvent("mouseenter",function(){
if($defined(b)){
$clear(b)}});
k.addEvent("mouseenter",function(){
if($defined(b)){
$clear(b)}});
k.setStyle("margin","0");
$(j).addEvent("mouseenter",function(){
h.setStyle("display","none");
k.setStyles({display:"block",position:"absolute"}).setStyle("z-index","1000000");
var u=$(window).getSize();
var w=$(window).getScroll();
var s=u.y/2;
var t=u.x/2;
var n=k.getSize();
var m=$(j).getCoordinates();
var v=$(j).getPosition();
var p=$(j).getSize();
var x=m.top+p.y;
var r=x-w.y;
var o=m.left+e;
var q=m.right;
var l=v.x+e;
if(s<r){
k.setStyle("top",m.top-n.y-c);
if(o<t){
k.setStyle("left",l)}else{k.setStyle("left",(m.right-n.x)-e)}}else{k.setStyle("top",x+c);
if(o<t){
k.setStyle("left",l)}else{k.setStyle("left",(m.right-n.x)-e)}}}).setStyle("cursor","pointer")})};
window.addEvent("domready",function(){
	new DropdownMenu($("menu"));
	alaune.periodical(PERIODE_ACTUS);
	smartHoverBox(1000,30,0,"_smarthbox","smarthbox_close");
	}
	);
