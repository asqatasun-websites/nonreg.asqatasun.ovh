/* ------------ SCRIPT D ALTERNANCE DES DIV ------------ */
/* VARIABLE 1 : ID DU DIV A ALTERNER */
function altern(id) {
	eval ("count"+id+" = 1;");
	var totalencart = 0;
	var i = 0;
	while(document.getElementById(id+i) != null){
		document.getElementById(id+i).style.visibility = "hidden";
		document.getElementById(id+i).style.position = "absolute";
		totalencart++;
		i++;
	}
	document.getElementById(id+0).style.visibility = "visible";
	document.getElementById(id+0).style.position = "relative";
	self.setInterval('changeencart(\''+id+'\',\''+totalencart+'\');', 4000)

}

function changeencart(encart,totalencart) {
	//eval ("alert(encart+' - '+count"+encart+"+' - '+totalencart)");
	for (i=0;i<totalencart;i++) {
		document.getElementById(encart+i).style.visibility = "hidden";
		document.getElementById(encart+i).style.position = "absolute";
	}
	
	eval ("if (count"+encart+">totalencart-1) count"+encart+" = 0;");
	eval ("x = count"+encart); 
	document.getElementById(encart + "" + x).style.visibility = "visible";
	document.getElementById(encart + "" + x).style.position = "relative";
	eval ("count"+encart+"++;");

}


/* ------------ FONCTION DE CONTROLE CLE DE LUHN ------------ */
/* VARIABLE 1 : NUMERO A TESTER */
function isLuhnKey(key){
	var result = false;
	var multiplicateur = 2;
	var tempCalcul = 0;
	var temp2 = 0;
	for (i=key.length-2;i>=0;i--){
	var temp = parseInt(key.substring(i,i+1));
	temp2 = temp * multiplicateur;
	if (temp2>9) {temp2 = temp2+1;}
	tempCalcul = tempCalcul + temp2;
	if (multiplicateur==2) {multiplicateur=1;} else {multiplicateur=2;}
	}
	result = ((( 10-(tempCalcul % 10) ) % 10) == parseInt(key.substring(key.length - 1,key.length)));
	return result;
}


/* ------------ RETOURNE LA DATE DU JOUR D APRES L ORDINATEUR CLIENT ------------ */
/* PAS DE VARIABLE */
function getdateday () {
	var maDate = new Date();
	var mois=maDate.getMonth()+1;
	var jour=maDate.getDate();
	var annee=maDate.getYear();
	var heures=maDate.getHours();
	var minutes=maDate.getMinutes();
	day = jour + "/" + mois + "/" + annee + " - " + heures + "h" + minutes;
	return day;
}

/* ------------ VERIFIE SI L EMAIL EST VALIDE ------------ */
/* VARIABLE 1 : VALEUR A TESTER */
function ismail(valueto) {
	if ((valueto=='') || (valueto.indexOf('.')<1) || (valueto.indexOf ('@', 0) < 2))
	   	return false;
	else
	   	return true;
}

/* ------------ VERIFIE SI CHAMPS OBLIGATOIRE REMPLI ------------ */
/* VARIABLE 1 : VALEUR A TESTER */
function isempty(valueto) {
	if (valueto=='')
	   	return true;
	else
	   	return false;
}

/* ------------ VERIFIE SI NUMERO FID CORRECT ------------ */
/* VARIABLE 1 : VALEUR A TESTER */
function isfid(valueto) {
	var cardnum = valueto.split("913572");
	if ((cardnum.length>0 && cardnum.length != 13)&& !isLuhnKey(valueto)) 
		return false;
	else
		return true;
}

/* ------------ VERIFIE SI NUMERO PASS CORRECT ------------ */
/* VARIABLE 1 : VALEUR A TESTER */
function ispass(valueto) {
	var cardnum = valueto.split("503200");
	if (cardnum[1].length>0 && cardnum[1].length != 11)
		return false;
	else
		return true;
	}

/* ------------ VERIFIE SI NUMERO PASS CORRECT ------------ */
/* VARIABLE 1 : VALEUR A TESTER */
function isvisa(valueto) {
var cardnum = valueto.slice(3,15);
	if (cardnum.length>0 && cardnum.length != 12) 
		return false;
	else
		return true;}

/* ------------ VERIFIE SI BON NOMBRE DE CARACTERES ------------ */
/* VARIABLE 1 : VALEUR A TESTER -- VARIABLE 2 : NOMBRE DE CARACTERES REQUIS */
function hasxchar(valueto, x) {
	if (valueto.length==x)
		return true;
	else
		return false;
	}

/* ------------ VERIFIE SI C EST UN NUMERO ET BON NOMBRE DE CHIFFRES ------------ */
function hasxnum(valueto, x) {
/* VARIABLE 1 : NOMBRE A TESTER -- VARIABLE 2 : NOMBRE DE CHIFFRES REQUIS */
	 if (!(isNaN(valueto))&&(valueto.length==x))
	
		return true;
	else
		return false;
}

/* ------------ VERIFIE SI C EST UN NUMERO ------------ */
function isnum(valueto) {
/* VARIABLE 1 : NOMBRE A TESTER  */
	if (!(isNaN(valueto))) 
		return true;
	else
		return false;
}



/* ------------ VERIFIE SI UNE CASE EST COCHEE ------------ */
/* VARIABLE 1 : CASE A COCHER A TESTER */
function ischecked(valueto) {
	if (valueto.checked)
		return true;
	else
		return false;
}

/* ------------ VERIFIE QUELLE OPTION EST SELECTIONNEE DANS UNE LISTE ------------ */
/* VARIABLE 1 : LISTE A TESTER */
function whitchisselected(valueto) {
	if (valueto!=undefined)	{
		return valueto.options[valueto.selectedIndex].value;
		}
        else return 0;
}

/* ------------ VERIFIE QUEL BOUTON RADIO EST COCHE ------------ */
/* VARIABLE 1 : BOUTON RADIO A TESTER */
function whitchischecked(valueto) {
	if (valueto!=undefined)	{
		for (var i=0; i<valueto.length;i++) {
			if (valueto[i].checked)
				return valueto[i].value;
        	}
        }
        else return 0;
}

/* ------------ VERIFIE si un bouton RADIO EST COCHE ------------ */
/* VARIABLE 1 : BOUTON RADIO A TESTER */
function whichischecked(valueto) {
	if (valueto!=undefined)	{
		for (var i=0; i<valueto.length;i++) {
			if (valueto[i].checked)
				return valueto[i].value;
        	}
        }
        return -1;
}


function changevisaquiz (carte,form) {
	switch (carte) {
		case "classique" :
			form.R76.value="0250";
			form.R77.value="";
			break;
		case "premier" :
			form.R76.value="0150";
			form.R77.value="";
			break;
		case "electron" :
			form.R76.value="0350";
			form.R77.value="";
			break;
	}
}


function changecartequiz (carte, form) {
	switch (carte) {
		case "fidelite" :
			document.getElementById('typevisa').style.visibility="hidden";
			document.getElementById('typevisa').style.position="absolute";
			document.getElementById('typecarte').style.visibility="visible";
			form.R76.value="913572";
			form.R77.value="";
			break;
		case "pass" :
			document.getElementById('typevisa').style.visibility="hidden";
			document.getElementById('typevisa').style.position="absolute";
			document.getElementById('typecarte').style.visibility="visible";
			form.R76.value="503200";
			form.R77.value="";
			break;
		case "visapass" :
			document.getElementById('typevisa').style.visibility="visible";
			document.getElementById('typevisa').style.position="relative";
			document.getElementById('typecarte').style.visibility="visible";
			form.R74[0].checked=true;
			form.R76.value="0150";
			form.R77.value="";
			break;
	}
}

function changecartequizz (carte, form) {
	switch (carte) {
		case "fidelite" :
			document.getElementById('typevisa').style.display="none";
			document.getElementById('typecarte').style.display="block";
			form.R76.value="913572";
			form.R77.value="";
			break;
		case "pass" :
			document.getElementById('typevisa').style.display="none";
			document.getElementById('typecarte').style.display="block";
			form.R76.value="503200";
			form.R77.value="";
			break;
		case "visapass" :
			document.getElementById('typevisa').style.display="block";
			document.getElementById('typecarte').style.display="block";
			form.R74[0].checked=true;
			form.R76.value="0150";
			form.R77.value="";
			break;
	}
}


function changevisa(carte,form) {
	switch (carte) {
		case "classique" :
			form.cardnumber1.value="0250";
			form.cardnumber2.value="";
			break;
		case "premier" :
			form.cardnumber1.value="0150";
			form.cardnumber2.value="";
			break;
		case "electron" :
			form.cardnumber1.value="0350";
			form.cardnumber2.value="";
			break;
	}
}


function changecarte(carte, form) {
	switch (carte) {
		case "fidelite" : 
			document.getElementById('typevisa').style.visibility="hidden";
			document.getElementById('typevisa').style.position="absolute";
			document.getElementById('typecarte').style.visibility="visible";
			form.cardnumber1.value="913572";
			form.cardnumber2.value="";
			break;
		case "pass" : 
			document.getElementById('typevisa').style.visibility="hidden";
			document.getElementById('typevisa').style.position="absolute";
			document.getElementById('typecarte').style.visibility="visible";
			form.cardnumber1.value="503200";
			form.cardnumber2.value="";
			break;
		case "visapass" : 
			document.getElementById('typevisa').style.visibility="visible";
			document.getElementById('typevisa').style.position="relative";
			document.getElementById('typecarte').style.visibility="visible";
			form.visa[0].checked=true;
			form.cardnumber1.value="0150";
			form.cardnumber2.value="";
			break;
	}
}

