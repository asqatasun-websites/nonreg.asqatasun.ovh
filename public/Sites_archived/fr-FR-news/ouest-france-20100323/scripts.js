
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
function pluriel(id) // Utilisé dans la page notation des joueurs
{
 var nombre = document.getElementById(id).innerHTML ;
 if (nombre >= 2)
 {
  document.write("s");
 }
 if (nombre == 0)
 {
  document.getElementById(id).innerHTML="Aucun";
 }
}
function Pluriel(Nbre,libZero,accord)
{
 var nombre = document.getElementById(Nbre).innerHTML ;
 if (nombre == 0)
 {
  document.getElementById(Nbre).innerHTML = libZero;
 }
 if (nombre >= 2)
 {
  document.write(accord);
 }
}

function cacheBloc(ident,hauteurMini) //Pour cacher les blocs de pubs(ou autre) vide, mais qui affichent tout de même une marge...
{
 var BLOC = document.getElementById(ident) ;
 hBLOC = BLOC.offsetHeight;
// alert(hBLOC);
 if ( hBLOC < hauteurMini )
 {
  BLOC.style.display = 'none' ;
 }
}

function cachePubVide(idDiv)
{
 // /!\ ne peut pas s'appliquer aux pubs top et megabanner quand elles sont dans le même div
 var imgs,i;
 var isDisplay;
 isDisplay = true;
 dedansDiv = document.getElementById(idDiv);
 imgs      = dedansDiv.getElementsByTagName('img');

 // Présence de scripts
 scripts = dedansDiv.getElementsByTagName('script') ;
 presenceScripts = false ;

 if(scripts.length>2){
   presenceScripts = true ;
 }
 
 // on affiche la Pub si il y a du contenu Flash (dans un objet)
 if (dedansDiv.getElementsByTagName('object').length==0 && !presenceScripts)
 {
  for(i=0;i<imgs.length;i++)
  {
	 // on n'affiche pas si c'est une image vide  
	 if( (/empty.gif/.test(imgs[i].src)) || (/vide.gif/.test(imgs[i].src)) || (/vide1.gif/.test(imgs[i].src)) || (/vide2.gif/.test(imgs[i].src)) ) // nom du fichier pub vide
   {isDisplay = false;}
  }
 }
 if (!isDisplay) {dedansDiv.style.display = "none";}
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function taillePhoto(ident,tailleMax)
//vérifie la taille des photos affichés et les retailles si nécessaire.
{
 var PHOTO = document.getElementById(ident) ;
 hPHOTO = PHOTO.offsetHeight; //On peut aussi utiliser PHOTO.height
 lPHOTO = PHOTO.offsetWidth;
// test du format de la photo (paysage)
 if ( lPHOTO > hPHOTO )
 {
  if (lPHOTO > tailleMax )
  {
   PHOTO.style.width=tailleMax+'px'; 
  }
 }
// test du format de la photo (portrait)
 else if ( hPHOTO > lPHOTO )
 {
  if (lPHOTO > tailleMax )
  {
   PHOTO.style.width=tailleMax+'px'; 
  }
 }
// test du format de la photo (carré)
 else
 {
  if (lPHOTO > tailleMax )
  {
   PHOTO.style.width=tailleMax+'px'; 
  }
 }
}
function AffEff(id)
{
	el = document.getElementById(id);
	var display = el.style.display ? '' : 'none';
	el.style.display = display;
}

function cacheNePasAfficher(idDiv)
{
 phrase = document.getElementById(idDiv);
 cherche = new RegExp (/NePasAfficher/gi);
 resultat = cherche.test(phrase.innerHTML);
 if (resultat == true)
 {
  phrase.style.display = "none";
 }
}

function compteBloc(truc,tag) {
	var navroot = document.getElementById(truc);
	if ( navroot ) {
		var lis = navroot.getElementsByTagName(tag);
		var ok = 0; var nok = 0;
		for ( i = 0; i < lis.length; i++ ) {
			if ( lis[i].parentNode == navroot )
				ok++;
        var hrefbloc = lis[i].href;
        chaineID = "idBloc"+ok+" = '"+lis[i].id+"';"
        eval(chaineID);
        document.getElementById('idOngletDossier'+ok).href=hrefbloc;
		}
    blocDossier = ok ;
    Afficher();
	}
}

//blocDossier = 3;
Temps = 3000;
Dessus = false;

var Increment = 1;
var Actuelle = 1;

function Afficher()
{
 if (!Dessus)
 {
  Precedente = Increment;
  Actuelle = Increment + 1;
  if (Actuelle > blocDossier) Actuelle = 1;
  diaporama();
 }
 if (Increment < blocDossier)
 {
  Increment = Increment + 1;
 }
 else
 {
  Increment = 1;
 }
 setTimeout("Afficher()",Temps);
}

function changeOpac(opacity, object)
{ 
// var object = document.getElementById(id).style; 
 object.opacity = (opacity / 100); 
 object.MozOpacity = (opacity / 100); 
 object.KhtmlOpacity = (opacity / 100); 
 object.filter = "alpha(opacity=" + opacity + ")"; 
}

function diaporama()
{
 if(Precedente == undefined) Precedente = 1;
 diapoPrecedente = document.getElementById(eval('idBloc'+Precedente)).style;
 diapoActuelle = document.getElementById(eval('idBloc'+Actuelle)).style;

 diapoPrecedente.zIndex='0';
 diapoActuelle.zIndex='99';
 diapoActuelle.display='block';

 changeOpac('10',diapoActuelle);                          // Je devrai pouvoir ajouter une fonction pour ça !
 setTimeout("changeOpac('50',diapoActuelle);",100);       //
 setTimeout("changeOpac('100',diapoActuelle);",200);      //

 setTimeout("diapoPrecedente.display='none';",200);

 document.getElementById('idOngletDossier'+Precedente).className = "unOngletDossier";
 document.getElementById('idOngletDossier'+Actuelle).className = "unOngletDossierHover";
}

function monteDiapo(affiche,cache1,cache2,cache3)
{
 document.getElementById(eval('idBloc'+affiche)).style.display= "block";
 document.getElementById(eval('idBloc'+cache1)).style.display = "none";
 document.getElementById(eval('idBloc'+cache2)).style.display = "none";
 document.getElementById(eval('idBloc'+cache3)).style.display = "none";

 document.getElementById('idOngletDossier'+affiche).className = "unOngletDossierHover";
 document.getElementById('idOngletDossier'+cache1).className = "unOngletDossier";
 document.getElementById('idOngletDossier'+cache2).className = "unOngletDossier";
 document.getElementById('idOngletDossier'+cache3).className = "unOngletDossier";
}

function changeCSS(id) //sans recup url/target
{
 var leBloc,nomClass;

 if (id.className==undefined) leBloc = document.getElementById(id); // cas où id est l'id du bloc
 else leBloc = id;                                                  // cas où id est this

 nomClass=leBloc.className;

 var rechercheCSS=new RegExp("Hover.*$");

 if(nomClass.match(rechercheCSS))
 {
  nomClass = nomClass.replace('Hover','');
  leBloc.className=nomClass;
 }
 else 
 {
  leBloc.className=nomClass+'Hover';
 }
}


function tailleTitre(hDispo,lDispo,blocTitre,tailleTexte)
{
blocTitre.style.fontSize = "16px";
 for( j=tailleTexte ; blocTitre.offsetHeight<=hDispo && tailleTexte<30 ; j++ ) // tant que le bloc titre rentre dans la hauteur dispo on agrandi la taille du texte de 1px jusqu'a 30px 
 {
  tailleTexte = j-1;
  blocTitre.style.fontSize = j+"px";
 }

 blocTitre.style.fontSize = tailleTexte-1 +"px"; // ici on enlève 1px à la taille du texte car dans le test(for) précédent on peut dépasser la hauteur dispo.

 for (j=tailleTexte ; blocTitre.offsetWidth>=lDispo ; j--) // on s'occupe maintenant de la largeur du texte, certains mots pouvant être trop large
 {
  tailleTexte = j;
  blocTitre.style.fontSize = j+"px";
 }
}

function deploy(idBloc,idImage,idText)
{
el = document.getElementById(idBloc);
image = document.getElementById(idImage);
text = document.getElementById(idText);
if (el.style.display == 'none')
{
el.style.display = '';
image.src = '/design/images/btnFlechFerm.gif';
text.innerHTML = 'Fermer';
}
else
{
el.style.display = 'none';
image.src = '/design/images/btnFlechOuvr.gif';
text.innerHTML = 'Afficher';
}
}


function tailPhotoDemiColUne(id)
{
 var sPhotoUne = document.getElementById(id);				// c'est le 'bloc' qui contient la photo 
 if ( sPhotoUne.offsetWidth > 300 )							// si il fait moins de 300 pixels de large
 {
  var urlPhotoUne = sPhotoUne.style.backgroundImage;		// on récupère l'url de l'image de fond

  if(urlPhotoUne.indexOf('_apx_')!=-1) var sIndex = '_apx_';

  if(urlPhotoUne.indexOf('_px_')!=-1) var sIndex = '_px_';

  if(urlPhotoUne.indexOf('_px_')==-1 && urlPhotoUne.indexOf('_apx_')==-1) var sIndex = '.jpg';

  if(urlPhotoUne.indexOf('_w_')==-1)
  {
   urlPhotoUne = urlPhotoUne.substring(0,urlPhotoUne.indexOf(sIndex)) + '_px_' + sPhotoUne.offsetWidth + '_.jpg)';
															// et on la change pour que l'image tienne dans la largeur du bloc
  }
  else
  {
   var watermark = urlPhotoUne.substring(0,urlPhotoUne.indexOf('_w_'), urlPhotoUne.length);
   urlPhotoUne = urlPhotoUne.substring(0,urlPhotoUne.indexOf(sIndex)) + '_px_' + sPhotoUne.offsetWidth + '_' + watermark + '.jpg)';
  }
  document.getElementById(id).style.backgroundImage = urlPhotoUne;			// ensuite on la ré-injecte dans le fond du bloc
 }
}

function retaillePhoto(ca)
{
 var format,sIndex;
 var urlPhoto = ca.parentNode.style.backgroundImage;
 var fermeture = urlPhoto.substring(urlPhoto.lastIndexOf('.jpg'));
 var ratioPhoto = ca.width/ca.height;
 if(urlPhoto.indexOf('_apx_')!=-1) sIndex = '_apx_';
 if(urlPhoto.indexOf('_px_')!=-1) sIndex = '_px_';
 if(urlPhoto.indexOf('_px_')==-1 && urlPhoto.indexOf('_apx_')==-1) sIndex = '.jpg';
 if ( ca.width>ca.height && ratioPhoto>=(4/3) )
 {
  if(urlPhoto.indexOf('_w_')==-1)
  {
   urlPhoto = urlPhoto.substring(0,urlPhoto.indexOf(sIndex)) + '_px_' + ca.parentNode.offsetWidth + '_' + fermeture;
  }
  else
  {
   var watermark = urlPhoto.substring(urlPhoto.lastIndexOf('_w_'), urlPhoto.length);
   urlPhoto = urlPhoto.substring(0,urlPhoto.indexOf(sIndex)) + '_px_' + ca.parentNode.offsetWidth + '_' + watermark;
  }
 }
 else
 {
  ca.parentNode.style.backgroundPosition = "center center";
  if (ca.width > ca.height) {
   var tailleApx = Math.floor(ca.parentNode.offsetHeight*ratioPhoto);
  }
  else tailleApx = ca.parentNode.offsetHeight;

  if(urlPhoto.indexOf('_w_')==-1)
  {
   urlPhoto = urlPhoto.substring(0,urlPhoto.indexOf(sIndex)) + '_apx_' + tailleApx + '_' + fermeture;
  }
  else
  {
   var watermark = urlPhoto.substring(urlPhoto.lastIndexOf('_w_'), urlPhoto.length);
   urlPhoto = urlPhoto.substring(0,urlPhoto.indexOf(sIndex)) + '_apx_' + tailleApx + '_' + watermark;
  }
 }
 ca.parentNode.style.backgroundImage = urlPhoto;
}

function li1OnClick(lUrl,bloc) {
// ce script donne le lien du onclick en cas d'adresse exterieur
// dans cette version le bloc ne peut accepter un autre lien en son sein.
 var rechHTTP = new RegExp("http","i");
 var rechMVI = new RegExp("maville.com","i");
 if (lUrl.match(rechHTTP)) {
  if (lUrl.match(rechMVI)) {
//        alert('Y\'a http et maville.com dedans. ->location');
   document.location.href=(lUrl);
  }
  else {
//        alert('Y\'a que http dedans. ->open');
//   bloc.onClick="window.open("+lUrl+");return false;"; // return false si on clique sur le lien avec le target, pour que ça n'ouvre pas plusieurs fenêtres.
   window.open(lUrl);
  }
 }
 else {
//        alert('Y\'a pas http dedans. ->location');
  document.location.href=(lUrl);
 }
}