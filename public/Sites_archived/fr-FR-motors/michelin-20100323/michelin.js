// JavaScript Document
var isOpera = navigator.userAgent.indexOf("Opera") > -1;
var isIE = navigator.userAgent.indexOf("MSIE") > 1 && !isOpera;
var isMoz = navigator.userAgent.indexOf("Mozilla/5.") == 0 && !isOpera;

//Surcharge de la methode loadXML de mozilla
if (isMoz) {
    
  //add the loadXML() method to the Document class
  Document.prototype.loadXML = function(strXML) {
    //create a DOMParser
    var objDOMParser = new DOMParser();
    //create new document from string
    var objDoc = objDOMParser.parseFromString(strXML, "text/xml");
    return objDoc;
  }
}

function rollOverMenu(id, nbMenu,e) {
	for (i=1;i<=nbMenu;i++) {
        if( i != id ) {
	        clearMenu(i);
	    }
    }
    try{
        document.getElementById("menu_"+id).className='menuOn';
		var cName = document.getElementById("sous_menu_"+id).className;
		if( cName.indexOf('Affiche') < 0 ) {
        	document.getElementById("sous_menu_"+id).className=document.getElementById("sous_menu_"+id).className + 'Affiche';
        }
    }catch(e){}
}

function clearMenu(id) {
	document.getElementById("menu_"+id).className='menu bordureMenu';
    try{
		if (id!=6) {document.getElementById("sous_menu_"+id).className='sous_menu';}
		else	{document.getElementById("sous_menu_"+id).className='sous_menu_fin';}
	}catch(e){}
}

function rollOutMenu(id, nbMenu, e) {
	 clearMenu(id);
}


/*function rollOverMenu(id) {
	for (i=1;i<=6;i++) {
		rollOutMenu(i);
	}
	try{
		document.getElementById("menu_"+id).className='menuOn';
		document.getElementById("sous_menu_"+id).style.display='block';
	}catch(e){}
	
}

function rollOutMenu(id) {
	try{
		document.getElementById("menu_"+id).className='menu bordureMenu';
		document.getElementById("sous_menu_"+id).style.display='none';
	}catch(e){}
}
*/
function rollOverTab(id) {
	document.getElementById("outilLien_"+id).style.backgroundColor='#e2e2ed';
	document.getElementById("outilNum_"+id).className='outil_numOn';
	/*document.getElementById("outilNumG_"+id).className='outil_numOn';*/
}

function rollOutTab(id) {
	document.getElementById("outilLien_"+id).style.backgroundColor='#f5f4f5';	
	document.getElementById("outilNum_"+id).className='outil_numOff';
	/*document.getElementById("outilNumG_"+id).className='outil_numOff';*/
}


function releaseQuestion(id) {
	if(document.getElementById("q_"+id).className=='support_reponse_hide') {
		document.getElementById("q_"+id).className='support_reponse_show';
	}else{
		document.getElementById("q_"+id).className='support_reponse_hide';
	}
	return false;
}

function HideQuestion() {
	var tab = document.getElementsByTagName('div');
	for(i = 0; i < tab.length ; i++ ) {
		var elem = tab[i];
		var elemId = elem.id;
		if( elemId.indexOf('q_') == 0 ) {
			elem.className='support_reponse_hide';
		}
	}
}

function popupBack(url, titre) {
	w = window.open(url,titre,'');	
	w.focus();
}


function redirectRubrique(codeRubrique,lang)
{
document.location.href="AfficheServlet?Rubrique=" + codeRubrique + "&Langue=" + lang;
}

function redirectOrga(lang)
{
organisation = document.form_deplacement.groupe.value;
redirectRubrique(organisation,lang);

}

function redirectGroupe(lang)
{
groupe= document.form_groupe.groupe.value;
redirectRubrique(groupe,lang);

}

function redirectContactMichelin(codeRubrique,lang)
{
document.form_contact.action="AfficheServlet?Rubrique=" + codeRubrique + "&Langue=" + lang + "&method=change";
document.form_contact.submit();

}

function redirectEtapeOnWay(codeRubrique,lang,method)
{
document.form_onway.action="AfficheServlet?Rubrique=" + codeRubrique + "&Langue=" + lang + "&method=" + method;
document.form_onway.submit();

}

function resetKeywords() {
	document.getElementById('recherche_destination_txt').value='';
}

function getDestinations(idGroupe,lang)
{
	resetKeywords();
	var url = "AjaxServletCG";
    var pars = "method=getDestinations&Langue=" +lang+ "&idGroupe=" +idGroupe;
    var myAjax = new Ajax.Request(
            url,
    {
        method: 'get',
        parameters: pars,
        onComplete: setDestinations
    });
}

function setDestinations(originalRequest)
{
    try {
	    result = originalRequest.responseXML;
	    //alert(originalRequest.responseText);
	    populateComboBoxDestination(result.documentElement);
    }
    catch (e) {
        alert( e );
        return;
    }
}

function populateComboBoxDestination(node)
{
	try {
		id = node.getAttribute("type");
	    
	    target = $(id);
	    target.focus();
	    // vidage de la combobox
	    target.options.length = 0;
	
	    // remplissage dynamique de la combobox
	    var noeuds = node.firstChild;
		var j = 0;
	    while (noeuds!=null) {
		  
		  if(noeuds.nodeName == 'value') {
			  var tex = noeuds.firstChild.nodeValue;
		      var val = noeuds.getAttribute("id");
		      //alert(tex + ' ' + val );
		      target.options[j] = new Option(urldecode(tex),val);
			  j++;
		  }
		  noeuds = noeuds.nextSibling;
		}
	   
	    target.disabled = false;
	
	    //Si un seul �l�ment, on le s�lectionne
	    if(j == 1){
	        target.options.selectedIndex = 0;
	    }
    }catch(e) {
	    alert( e );
    }
}


function urldecode(ch) {
    if(ch=='') return ch;
    try{
	    ch = ch.replace(/[+]/g, " ");
    	return unescape(ch);
    }
    catch (e) {
        alert( e );
        return "";
    }
}

var wdw = null;
function OpenWindowByTarget(target,url,width,height,top,left)
{
	var options='directories=no,location=no,menubar=no,toolbar=no,resizable=yes,scrollbars=yes,status=no';
	wdw = window.open(url,target,"top="+top+",left="+left+",width="+width+",height="+height+","+options);
	wdw.focus();
	return false;
}

/**
* fonction clickMenuConseil 
*    BUT   : Permet de, � la fois, cacher tout les sous menu
*            et de d�rouler le sous menu pass� en param�tre
*    PARAM : Les seuls param�tres � passer sont des entiers, l'id du menu � d�rouler et le nombre de menus 
*
**/
function clickMenuConseil(idMenu, nbMenu){
	cacheAllMenuConseil(nbMenu);
	
	//on change l'image de la petite fl�che
	//alert(idMenu)
	var chaine = "<img src=\"images/puce_02.gif\" alt=\"\" />";
	document.getElementById("img_fleche_"+idMenu).innerHTML = chaine;
	document.getElementById("menuGauche0"+idMenu).className='outil_bib_txt';
	document.getElementById("titreh2_"+idMenu).className='outil_bib_titre';
	
}

/**
* fonction clickMenuConseil 
*    BUT   : Cache tous les sous-menus.
*    PARAM : le nombre de menu 
*
**/
function cacheAllMenuConseil(nbMenu){
	//alert('On cache les sous menu.');
	for (i=1;i<=nbMenu;i++) {
		//on cache les sous menus
		document.getElementById("menuGauche0"+i).className='outil_bib_txt_hide';
		
		//on applique le bon style au "header" de chaque sous menu
		document.getElementById("titreh2_"+i).className='outil_bib_titre_off';
		
		//on change l'image de la petite fl�che
		var chaine = "<img src=\"images/puce_03.gif\" alt=\"\" />";
		document.getElementById("img_fleche_"+i).innerHTML=chaine;
	}
}

/** Script for change mode in backoffice **/
function setDesignMode(){
	if(document.getElementById('designMode').checked){
		self.location=getLocation()+ getSeparator() + 'designMode=Y';
	}else{
		self.location=getLocation()+ getSeparator() + 'designMode=N';
	}
}

function getSeparator() {
	var pos = location.href.indexOf("?");
	if(pos > 0 ) {
        if (location.href.indexOf("?designMode=") > 0) {
                return '?';
        } else {
                return '&';
        }
	} else {
		return '?';
	}
}

function getLocation(){
	var pos = location.href.indexOf("designMode=");
	var newlocation = location.href;
	if(pos > 0 ) {
		newlocation = location.href.substring(0,pos-1);
	}
	return newlocation;
}

function goToAnchor(a) {
	var pos = location.href.indexOf("#");
	var newlocation = location.href;
	if(pos > 0 ) {
		newlocation = location.href.substring(0,pos);
	}
	self.location = newlocation + '#' + a;
}

// d�finition de la taille maximun pour un textarea
function ismaxlength(obj){
var mlength=obj.getAttribute? parseInt(obj.getAttribute("maxlength")) : ""
if (obj.getAttribute && obj.value.length>mlength)
obj.value=obj.value.substring(0,mlength)
}