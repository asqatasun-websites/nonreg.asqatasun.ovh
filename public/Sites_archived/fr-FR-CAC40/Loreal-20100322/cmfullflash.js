﻿(function($) {

	var W = window, D = W.document;
	
	if(!W.LOC) {
		W.LOC = {};
	}
	
	if(!W.cmSetProduction || 
	!W.cmCreatePageviewTag || 
	!W.cmCreateProductviewTag || 
	!W.cmCreateRegistrationTag || 
	!W.cmCreateErrorTag || 
	!W.cmCreatePageElementTag || 
	!W.cmCreateConversionEventTag) {
		return;
	}
	
	// define ClientID as global var
	cm_ClientID = "90299333";
	
	// set working environment
	var reDevHost = /\.(validation|contentmanager2|web[\d]{2})\.e-loreal\./i;
	//alert("location.hostname: " + location.hostname + " && reDevHost.test(location.hostname): " + reDevHost.test(location.hostname));
	if(!reDevHost.test(location.hostname)) {
		W.cmSetProduction();
	}

})(jQuery);