		var xhr = null; 

		function getXhr(){
			if(window.XMLHttpRequest) // Firefox et autres
			   xhr = new XMLHttpRequest(); 
			else if(window.ActiveXObject){ // Internet Explorer 
				   try {
			                xhr = new ActiveXObject("Msxml2.XMLHTTP");
			            } catch (e) {
			                xhr = new ActiveXObject("Microsoft.XMLHTTP");
			            }
			}
			else { // XMLHttpRequest non support� par le navigateur 
				   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
				   xhr = false; 
			} 
		}

		function incStats(myTarget,myId){
			getXhr();
			xhr.onreadystatechange = function(){
				if(xhr.readyState == 4 && xhr.status == 200){
				//	document.getElementById(myId).innerHTML=(xhr.responseText);
			}
		}
		xhr.open("GET",myTarget,true);
		xhr.send(null);
		}


		function displayElement(myTarget,myId){
			getXhr()
			xhr.onreadystatechange = function(){
				if(xhr.readyState == 4 && xhr.status == 200){
					document.getElementById(myId).innerHTML=(xhr.responseText);
					scripts.widthProg();
					sideInit();
			}
		}
		xhr.open("GET",myTarget,true);
		xhr.send(null);
		}

		function displayVotes(myTarget,myId){
			getXhr()
			xhr.onreadystatechange = function(){
				if(xhr.readyState == 4 && xhr.status == 200){
					document.getElementById(myId).innerHTML=(xhr.responseText);
					scripts.videoSanders();
					scripts.videoMerci();
					scripts.videoEmission();
					scripts.videoManu();
					scripts.videoMax();
					scripts.videoSaints();
					scripts.videoFontenay();
					scripts.videoBravo();
					scripts.videoNousAppartient();
					scripts.videoTimsit();
                                        scripts.videoPublivores();
                                        scripts.videoQcmm();
                                        scripts.videoArthur();
                                        scripts.videoSoireeArthur();
                                        scripts.videoDroleDeJobs();
			}
		}
		xhr.open("GET",myTarget,true);
		xhr.send(null);
		}


		function changeVideo(idVideo,urlVideo){
			getXhr()
			xhr.onreadystatechange = function(){
				if(xhr.readyState == 4 && xhr.status == 200){
					document.getElementById("contenu_lecture_en_cours").innerHTML=(xhr.responseText);
					testChange(urlVideo);
			}
		}
		xhr.open("GET","webboxes/videos/playerVideo.html?VIDE_ID="+idVideo,true);
		xhr.send(null);

		}
		function testChange(url){
			if(url.indexOf('.flv')>0){
				var fo = new FlashObject("swf/player_FLV.swf", "player_ba", "300", "185", "8","#000");
				fo.addParam("quality","high");
				fo.addParam("wmode", "transparent");
				fo.addParam("scale","noscale");
				fo.addParam("salign","TL");
				fo.addParam("allowScriptAccess", "sameDomain");
				fo.addVariable("flvPath","http://vod-flash.mth.net/COMEDIE/PROGRESSIF/"+url);
				fo.write("player_vid");
			}
		}


		function afficheSaison(numSaison,idserie){
			getXhr()
			xhr.onreadystatechange = function(){
				if(xhr.readyState == 4 && xhr.status == 200){
					document.getElementById("contenu_serie_5").innerHTML=(xhr.responseText);
					scripts.conteneurBoxSerie();
					scripts.appli_combo_serie();

					// Ascenseur perso sur la page S�rie onglet episotheque
					$('#liste_episodes').jScrollPane({scrollbarOnLeft:true, showArrows:true});
					$('#desc_episode').jScrollPane({scrollbarOnLeft:true, showArrows:true});
					$('#contenu_serie_5 .jScrollPaneContainer:first').addClass('first');
			}
		}
		xhr.open("GET","serie/episotheque.html?num_saison="+numSaison+"&serieid="+idserie,true);
		xhr.send(null);
		}

		function afficheEpisode(numEpisode){
			getXhr()
			xhr.onreadystatechange = function(){
				if(xhr.readyState == 4 && xhr.status == 200){
					document.getElementById("desc_episode").innerHTML=(xhr.responseText);
			}
		}
		xhr.open("GET","serie/descepisode?num_epi="+numEpisode,true);
		xhr.send(null);
		}

function viewResult(){
	displayVotes('webboxes/sondages/resultBox.html?VOTE_ID='+document.getElementById('vote').value,'boxsondage');
}

function viewSond(id){
	displayVotes('webboxes/sondages/sondageBox.html?VOTE_ID='+id,'boxsondage');
}

function displayResult(){
if(document.getElementById('ans1')){
	if(document.getElementById('ans1').checked){
		displayVotes('webboxes/sondages/resultBox.html?VOTE_ID='+document.getElementById('vote').value+'&QUEST=ANSW1','boxsondage');
	}
}
if(document.getElementById('ans2')){
	if(document.getElementById('ans2').checked){
		displayVotes('webboxes/sondages/resultBox.html?VOTE_ID='+document.getElementById('vote').value+'&QUEST=ANSW2','boxsondage');
	}
}
if(document.getElementById('ans3')){
	if(document.getElementById('ans3').checked){
		displayVotes('webboxes/sondages/resultBox.html?VOTE_ID='+document.getElementById('vote').value+'&QUEST=ANSW3','boxsondage');
	}
}
if(document.getElementById('ans4')){
	if(document.getElementById('ans4').checked){
		displayVotes('webboxes/sondages/resultBox.html?VOTE_ID='+document.getElementById('vote').value+'&QUEST=ANSW4','boxsondage');
	}
}
if(document.getElementById('ans5')){
	if(document.getElementById('ans5').checked){
		displayVotes('webboxes/sondages/resultBox.html?VOTE_ID='+document.getElementById('vote').value+'&QUEST=ANSW5','boxsondage');
	}
}
if(document.getElementById('ans6')){
	if(document.getElementById('ans6').checked){
		displayVotes('webboxes/sondages/resultBox.html?VOTE_ID='+document.getElementById('vote').value+'&QUEST=ANSW6','boxsondage');
	}
}
if(document.getElementById('ans7')){
	if(document.getElementById('ans7').checked){
		displayVotes('webboxes/sondages/resultBox.html?VOTE_ID='+document.getElementById('vote').value+'&QUEST=ANSW7','boxsondage');
	}
}
if(document.getElementById('ans8')){
	if(document.getElementById('ans8').checked){
		displayVotes('webboxes/sondages/resultBox.html?VOTE_ID='+document.getElementById('vote').value+'&QUEST=ANSW8','boxsondage');
	}
}

}
